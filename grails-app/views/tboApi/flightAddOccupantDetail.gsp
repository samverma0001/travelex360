<!DOCTYPE html>
<html lang="en" ng-app>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>.:: Travelex360 ::.</title>

    <link href="${resource(dir: 'css', file: 'bootstrap.css')}" rel="stylesheet">

    <link href="${resource(dir: 'css', file: 'datepicker3.css')}" rel="stylesheet">
    <link type="/text/css" rel="stylesheet" href="${resource(dir: 'css', file: 'jquery.selectBoxIt.css')}"/>
    <link href="${resource(dir: 'css', file: 'owl.carousel.css')}" rel="stylesheet">
    <link href="${resource(dir: 'css', file: 'owl.theme.css')}" rel="stylesheet">
    <link href="${resource(dir: 'css', file: 'jquery.bootstrap-touchspin.css')}" rel="stylesheet" type="text/css"
          media="all">
    <link href="${resource(dir: 'css', file: 'bootstrap-slider.css')}" rel="stylesheet">
    <link href="${resource(dir: 'css', file: 'style.css')}" rel="stylesheet">
    <link href="${resource(dir: 'css', file: 'theme.css')}" rel="stylesheet">
    <link href="${resource(dir: 'css', file: 'media.css')}" rel="stylesheet">
    <script src="${resource(dir: 'js', file: 'ie8-responsive-file-warning.js')}"></script>
    <script src="${resource(dir: 'js', file: 'ie-emulation-modes-warning.js')}"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
</head>

<body class="theme-red">
<div class="page-wrapper">

<header class="main-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-sm-7 col-xs-6">
                <div class="logo pull-left">
                    <a href="${createLink(uri: "/")}">
                        <img src="../images/logo.png" width="190px">
                    </a>
                </div>

                <div class="main-icons-container-header pull-left">
                    <g:render template="/appTemplate/menuTemplate"/>

                </div>
            </div>
            <!-- <div class="col-md-7 col-sm-8 col-xs-7"> -->
            <!-- </div> -->
            <div class="col-md-2 col-sm-3 col-xs-6 text-right">
                <div class="header-right">
                    <ul class="header-icon-nav">
                        <li><a href="${createLink(controller: 'signIn', action: 'index')}"><img
                                src="../images/register-icon.png"></a></li>
                        <li><a href="${createLink(controller: 'login', action: 'auth')}"><img
                                src="../images/login-icon.png"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>


<div class="listing-page-wrapper">

<div class="content-wrapper">
<div class="container">
<div class="checkout-process-container">
<div class="checkout-process-header">
    <span class="num-circle">1</span> <span class="checkout-process-title">Review your Flight details</span>
</div>
<g:form controller="tboFlightBooking" id="validateOccupantForm"
        class="form parsley-form"
        params="[flightCode: flightCode, flightSessionId: flightForBooking?.sessionId, child: flightSearchVo?.children,
                 adult     : flightSearchVo?.adult]">
<div class="row">
    <div class="col-md-8">
        <div class="content-box">
            <div class="listing-box">
                <g:each in="${flightForBooking}" var="flightInstance" status="i">
                    <h3>${flightInstance?.origin} → ${flightInstance?.destination} <small
                            class="text-muted">
                        <g:if test="${flightInstance.tripIndicator == 1}">
                            ${flightSearchVo?.journeyDate}
                        </g:if>
                        <g:else test="${flightInstance.tripIndicator == 2}">
                            ${flightSearchVo?.journeyReturnDate}
                        </g:else>
                    </small>
                    </h3>

                    <div class="flight-list-item">
                        <table class="table airline-listing-table">
                            <tbody>
                            <tr>
                                <td class="col-airline">
                                    <span>
                                        <g:if test="${flightInstance.flightSegmentVO?.first()?.airLineName == "JetLite"}">
                                            <img src="${resource(dir: "/images/flightImages", file: "jetlite1.jpg")}"
                                                 height="30"
                                                 width="60"
                                                 class="img-rounded">
                                        </g:if>
                                        <g:elseif
                                                test="${flightInstance.flightSegmentVO?.first()?.airLineName == "Jet Airways"}">
                                            <img src="${resource(dir: "images", file: "jetairways.png")}">

                                        </g:elseif>
                                        <g:elseif
                                                test="${flightInstance.flightSegmentVO?.first()?.airLineName == "Air India"}">
                                            <img src="${resource(dir: "images", file: "AIRINDIA.png")}">

                                        </g:elseif>
                                        <g:elseif
                                                test="${flightInstance.flightSegmentVO?.first()?.airLineName == "SpiceJet"}">
                                            <img src="${resource(dir: "images", file: "SpiceJet.png")}">

                                        </g:elseif>
                                        <g:elseif
                                                test="${flightInstance.flightSegmentVO?.first()?.airLineName == "IndiGo"}">
                                            <img src="${resource(dir: "images", file: "IndiGo.png")}">
                                        </g:elseif>
                                        <g:elseif
                                                test="${flightInstance.flightSegmentVO?.first()?.airLineName == "GoAir"}">
                                            <img src="${resource(dir: "images", file: "GoAir.png")}">
                                        </g:elseif>
                                        <g:elseif
                                                test="${flightInstance.flightSegmentVO?.first()?.airLineName == "Air Asia"}">
                                            <img src="${resource(dir: "images", file: "airasia.png")}">
                                        </g:elseif>
                                        <g:elseif
                                                test="${flightInstance.flightSegmentVO?.first()?.airLineName == "Fly Dubai"}">
                                            <img src="${resource(dir: "images", file: "FLYDUBAI.png")}">
                                        </g:elseif>
                                        <g:elseif
                                                test="${flightInstance.flightSegmentVO?.first()?.airLineName == "Air India Express"}">
                                            <img src="${resource(dir: "images", file: "AirIndiaExpress.png")}">
                                        </g:elseif>
                                        <br>
                                        ${flightInstance?.flightSegmentVO?.first()?.airLineName}&nbsp;&nbsp;
                                    </span>

                                    <p><small>${flightInstance?.flightSegmentVO?.first()?.airLineName}</small></p>
                                </td>
                                <td class="col-dapart">
                                    <span class="text-large">${(flightInstance?.flightSegmentVO?.departureTime?.first()?.tokenize("T")?.last())}</span>
                                    <small class="text-muted">${flightInstance?.flightSegmentVO?.originAirPortName?.first()}</small>
                                </td>
                                <td class="col-arrive">
                                    <span class="text-large">${(flightInstance?.flightSegmentVO?.arrivalTime?.first()?.tokenize("T")?.last())}</span>
                                    <small class="text-muted">${flightInstance?.flightSegmentVO?.destinationAirPortName?.first()}</small>
                                </td>
                                <td class="col-duration">
                                    <span>${flightInstance?.ibDuration}</span>
                                    <small class="text-muted">${flightInstance?.flightSegmentVO?.size() - 1} Stop</small>
                                </td>
                                <td class="col-price">
                                    <div class="price">
                                        <span class="text-large color-default">Rs. ${flightInstance?.flightFare?.offeredFare}</span>
                                    </div>
                                    <a href="#" class="btn btn-default">Book</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <ul id="myTab" class="flight-list-tab-link" role="tablist">
                            <li class="">
                                <a href="#flight${i}"
                                   onclick="flightItineary('${i}', '${flightInstance}', '${flightInstance?.tripIndicator}')"
                                   role="tab" data-toggle="tab" aria-controls="home"
                                   aria-expanded="true">Flight Details</a>
                            </li>
                            <li class="">
                                <a aria-expanded="false" href="#fare${i}" role="tab"
                                   onclick="showFlightFairRule('${i}', '${flightInstance}', '${flightInstance?.tripIndicator}')"
                                   data-toggle="tab"
                                   aria-controls="profile">Fare Details</a>
                            </li>
                            <li class="">
                                <a aria-expanded="false" href="#baggage${i}" role="tab"
                                   onclick="showFlightSSR('${i}', '${flightInstance}', '${flightInstance?.tripIndicator}')"
                                   data-toggle="tab"
                                   aria-controls="profile">Baggage</a>
                            </li>
                        </ul>

                        <div class="tab-content flight-list-tab-content" style="display: inline-block">
                            <div class="tab-content-close"><i class="glyphicon glyphicon-remove"></i></div>

                            <div role="tabpanel" class="tab-pane fade active in" id="flight${i}"
                                 aria-labelledby=""></div>

                            <div role="tabpanel" class="tab-pane fade" id="fare${i}" aria-labelledby=""></div>

                            <div role="tabpanel" class="tab-pane fade" id="baggage${i}" aria-labelledby=""></div>

                        </div>
                    </div>
                </g:each>


                <div class="text-center">
                    <g:link controller="tboFlightBooking" action="searchFlights"
                            class="btn btn-lg btn-default">Modify Details</g:link>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="content-box">
            <h3>Fare Details</h3>
            <table class="table table-striped">
                <tbody>
                <tr>
                    <td>Adult x 1</td>
                    <td class="text-right">Rs. 1200</td>
                </tr>
                <tr>
                    <td>Children x 2</td>
                    <td class="text-right">Rs. 6,464</td>
                </tr>
                <tr>
                    <td>Infants x 2</td>
                    <td class="text-right">Rs. 2,464</td>
                </tr>
                </tbody>
            </table>
            <h4>Fee &amp; Surcharges</h4>
            <table class="table table-striped">
                <tbody>
                <tr>
                    <td>Airline Fuel Charges</td>
                    <td class="text-right">Rs. 3000</td>
                </tr>
                <tr>
                    <td>Service Tax</td>
                    <td class="text-right">Rs. 200</td>
                </tr>
                <tr>
                    <td>Other Charges</td>
                    <td class="text-right">Rs. 300</td>
                </tr>
                </tbody>
            </table>

            <div class="price text-right">
                <h4 class="color-default">(-)Special Discount :: Rs.1222</h4>

                <h2><strong>You Pay<br>
                    Rs. 10000</strong></h2>
            </div>
        </div>
    </div>
</div>

<div class="checkout-process-header">
    <span class="num-circle">2</span> <span class="checkout-process-title">Ticketing Email</span>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="content-box">
            <br>

            <div class="form-horizontal">
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Your Email</label>

                    <div class="col-sm-7">
                        <input type="email" name="adult1Email" class="form-control parsley-validated"
                               id="adult1Email" required="true"/>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-7">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="toggle-passowrd-box"> I have Travelex 360 Password
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group passowrd-box">
                    <label for="" class="col-sm-3 control-label">Login Password</label>

                    <div class="col-sm-7">
                        <input type="password" class="form-control" id="" placeholder="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="checkout-process-header">
    <span class="num-circle">3</span> <span class="checkout-process-title">Traveller details</span>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="content-box">
            <div class="alert alert-danger text-center" role="alert">
                Please Enter the Name &amp; DOB according to Passport Details.
            </div>

            <div class="tvx-from form-horizontal traveller-detail-form">
                <g:set var="adultNo" value="${flightSearchVo?.adult?.toInteger()}"/>
                <g:each in="${(0..<adultNo)}" var="adult" status="ad">
                    <g:if test="${ad == 0}">
                        <h3>Adult ${ad}</h3>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Name</label>

                            <div class="col-sm-2">
                                <div class="input-group col-md-12">
                                    <g:select name="adult${ad + 1}TName" class="form-control"
                                              from="${['Mr.', 'Mrs.', 'Miss.', 'Ms.', 'Master']}"/>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <input type="text" data-required="true" name="adult${ad + 1}FName" required
                                       id="adult${ad + 1}FName"
                                       class="form-control parsley-validated" placeholder="First Name">
                            </div>

                            <div class="col-sm-4">
                                <input type="text" data-required="true" name="adult${ad + 1}LName" required
                                       id="adult${ad + 1}LName"
                                       class="form-control parsley-validated" placeholder="Last Name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">DOB</label>

                            <div class="col-sm-2">
                                <input type="text" data-required="true" name="adult${ad + 1}DOB"
                                       id="adult${ad + 1}DOB" required
                                       class="form-control parsley-validated" placeholder="Date of birth">
                            </div>

                            <div class="col-sm-4">
                                <input type="text" data-required="true" name="adult${ad + 1}Age"
                                       id="adult${ad + 1}Age" required
                                       class="form-control parsley-validated" placeholder="Age">

                            </div>

                            <div class="col-sm-4">
                                <input type="tel" name="adult${ad + 1}Phone" required
                                       id="adult${ad + 1}Phone"
                                       class="form-control parsley-validated" placeholder="Contact Number">

                            </div>
                        </div>
                    </g:if>
                    <g:else>
                        <div class="portlet">
                            <div class="portlet-header">
                                <h3>
                                    <i class="fa fa-tasks"></i>
                                    Add ${ad + 1 == 1 ? (ad + 1) + "st" : (ad + 1) + "nd"} adult name
                                </h3>
                            </div>

                            <div class="portlet-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Title</label>

                                                    <g:select name="adult${ad + 1}TName" class="form-control"
                                                              from="${['Mr.', 'Mrs.', 'Miss.', 'Ms.', 'Master']}"/>
                                                </div>
                                            </div>

                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>First Name</label>

                                                    <p class="text-primary" style="display: inline">*</p>
                                                    <input type="text" data-required="true" name="adult${ad + 1}FName"
                                                           required
                                                           id="adult${ad + 1}FName"
                                                           class="form-control parsley-validated"/>
                                                </div>
                                            </div>

                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Last Name</label>

                                                    <p class="text-primary" style="display: inline">*</p>
                                                    <input type="text" data-required="true" name="adult${ad + 1}LName"
                                                           required
                                                           id="adult${ad + 1}LName"
                                                           class="form-control parsley-validated"/>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </g:else>
                </g:each>


                <g:if test="${flightSearchVo?.children}">
                    <g:set var="childNo" value="${flightSearchVo?.children?.toInteger()}"/>
                    <g:each in="${(0..<childNo)}" var="child" status="ch">
                        <h3>Child ${ch}</h3>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Name</label>

                            <div class="col-sm-2">
                                <div class="input-group col-md-12">
                                    <g:select name="child${ch + 1}TName" class="form-control parsley-validated"
                                              required=""
                                              from="${['Mr.', 'Mrs.', 'Miss.', 'Ms.', 'Master']}"
                                              id="child${ch + 1}TName"/>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <input type="text" data-required="true" name="child${ch + 1}FName"
                                       required
                                       id="child${ch + 1}FName" placeholder="First Name"
                                       class="form-control parsley-validated">
                            </div>

                            <div class="col-sm-4">
                                <input type="text" data-required="true" name="child${ch + 1}LName"
                                       required
                                       id="child${ch + 1}LName" placeholder="Last Name"
                                       class="form-control parsley-validated">
                            </div>
                        </div>
                    </g:each>
                </g:if>
                <hr>

                %{--<div class="form-group">--}%
                %{--<label for="" class="col-sm-3 control-label">Contact Number</label>--}%

                %{--<div class="col-sm-4">--}%
                %{--<div class="input-group col-md-12">--}%
                %{--<input type="text" class="form-control">--}%
                %{--</div>--}%
                %{--</div>--}%
                %{--</div>--}%

                <g:set var="lccOrNot" value="${flightForBooking?.isLcc}"/>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-4">
                        %{--<input value="Proceed To Payment" type="submit" class="btn btn-lg btn-success"/>--}%
                        <g:if test="${lccOrNot.contains(true)}">
                            <g:actionSubmit value="Proceed to book" action="bookTicket" class="btn btn-success btn-lg"/>
                        </g:if>
                        <g:else>
                            <g:actionSubmit action="bookTicket" value="Proceed to book" onclick="validateForm1()"
                                            class="btn btn-success btn-lg"/>
                            %{--<g:actionSubmit action="flightBooking" value="Proceed to hold" onclick="validateForm1()"--}%
                                            %{--class="btn btn-success btn-lg"/>--}%
                        </g:else>
                    </div>
                </div>
                <hr>
            </div>
            <img src="../images/secure-payment.jpg" class="img-max">
        </div>
    </div>
</div>
<g:hiddenField name="flightSearchResult" value="${flightForBooking}"/>
</g:form>
</div>
</div>

</div>
</div>


%{--<div class="modal fade" id="popup_traveller_info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"--}%
%{--aria-hidden="true">--}%
%{--<div class="modal-dialog">--}%
%{--<div class="modal-content">--}%
%{--<div class="modal-header">--}%
%{--<h3 class="modal-title" id="myModalLabel">Occupancy Details</h3>--}%
%{--</div>--}%

%{--<form class="form-horizontal">--}%
%{--<div class="modal-body traveller-form">--}%
%{--<h4>Guest 1</h4>--}%

%{--<div class="form-group">--}%
%{--<label for="" class="col-sm-2 control-label">Full Name</label>--}%

%{--<div class="col-sm-7">--}%
%{--<input type="text" class="form-control" id="" placeholder="">--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="form-group">--}%
%{--<label for="" class="col-sm-2 control-label">Age</label>--}%

%{--<div class="col-sm-3">--}%
%{--<input type="text" class="form-control" id="" placeholder="">--}%
%{--</div>--}%
%{--</div>--}%
%{--<h4>Guest 2</h4>--}%

%{--<div class="form-group">--}%
%{--<label for="" class="col-sm-2 control-label">Full Name</label>--}%

%{--<div class="col-sm-7">--}%
%{--<input type="text" class="form-control" id="" placeholder="">--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="form-group">--}%
%{--<label for="" class="col-sm-2 control-label">Age</label>--}%

%{--<div class="col-sm-3">--}%
%{--<input type="text" class="form-control" id="" placeholder="">--}%
%{--</div>--}%
%{--</div>--}%
%{--<h4>Guest 3</h4>--}%

%{--<div class="form-group">--}%
%{--<label for="" class="col-sm-2 control-label">Full Name</label>--}%

%{--<div class="col-sm-7">--}%
%{--<input type="text" class="form-control" id="" placeholder="">--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="form-group">--}%
%{--<label for="" class="col-sm-2 control-label">Age</label>--}%

%{--<div class="col-sm-3">--}%
%{--<input type="text" class="form-control" id="" placeholder="">--}%
%{--</div>--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="modal-footer">--}%
%{--<button type="button" class="btn btn-danger cancel-checkbox" data-dismiss="modal">Cancel</button>--}%
%{--<button type="button" class="btn btn-default">Preceed to Payment</button>--}%
%{--</div>--}%
%{--</form>--}%
%{--</div>--}%
%{--</div>--}%
%{--</div>--}%

</div>

<footer class="main-footer inner">
    <div class="container-fluid">
        <div class="footer-links">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <ul class="inline-list-links">
                        <li><a href="about.php">About Us</a></li>
                        <li><a href="how_it_works.php">How It Works</a></li>
                        <li><a href="terms.php">Terms Of Use</a></li>
                        <li><a href="contact.php">Contact Us</a></li>
                    </ul>
                </div>

                <div class="col-md-6 col-sm-6 text-right">
                    Copyright &copy; Travelex 360 Pvt.Ltd. 2014, All rights reserved
                </div>
            </div>
        </div>

        <div class="footer-rights">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <ul class="inline-list-links margin-top-5">
                        <li><a href="#">Post Your Complaint</a></li>
                        <li><a href="policy.php">Privacy Policy</a></li>
                        <li><a href="#">FAQ's</a></li>
                        <li><a href="#">Site Map</a></li>
                    </ul>
                </div>

                <div class="col-md-6 col-sm-6 text-right">
                    <img src="../images/payment-methods.jpg">
                </div>
            </div>
        </div>
    </div>
</footer>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="${resource(dir: 'js', file: 'script.min.js')}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28/angular.js"></script>
<script src="${resource(dir: 'js', file: 'ang_main.js')}"></script>
<script src="${resource(dir: 'js', file: 'ie10-viewport-bug-workaround.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery-ui.min.js')}"></script>
<script src="${resource(dir: 'js', file: 'datepicker.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery.selectBoxIt.min.js')}"></script>
<script src="${resource(dir: 'js', file: 'owl.carousel.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery.bootstrap-touchspin.js')}"></script>
<script src="${resource(dir: 'js', file: 'packages-map.js')}"></script>
<script src="${resource(dir: 'js', file: 'bootstrap-slider.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery.barrating.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery.raty.js')}"></script>
<script src="${resource(dir: 'js', file: 'main.js')}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".countspin-roomtype1").TouchSpin({
            min: 0,
            initval: '1',
            max: 14,
            buttondown_class: 'btn btn-default',
            buttonup_class: 'btn btn-default',
            step: 1
        });
        $(".countspin-roomtype2").TouchSpin({
            min: 0,
            initval: '1',
            max: 5,
            buttondown_class: 'btn btn-default',
            buttonup_class: 'btn btn-default',
            step: 1
        });
        $('#example-f').barrating({
            showSelectedRating: false,
            readonly: true
        });
        $.fn.raty.defaults.path = 'images';
        $('.star-rating.readonly').raty({
            readOnly: true,
            score: function () {
                return $(this).attr('data-score');
            }
        });
        $('.star-rating-overall').raty({
            score: function () {
                return $(this).attr('data-score');
            },
            target: '.rating-target1',
            targetKeep: true,
            targetText: 'Click to Rate'
        });
        $('.star-rating-service').raty({
            score: function () {
                return $(this).attr('data-score');
            },
            target: '.rating-target2',
            targetKeep: true,
            targetText: 'Click to Rate'
        });
        $('.star-rating-quality').raty({
            score: function () {
                return $(this).attr('data-score');
            },
            target: '.rating-target3',
            targetKeep: true,
            targetText: 'Click to Rate'
        });
        $('.star-rating-clean').raty({
            score: function () {
                return $(this).attr('data-score');
            },
            target: '.rating-target4',
            targetKeep: true,
            targetText: 'Click to Rate'
        });
    });

    function flightItineary(i, flightInstance, tripIndicator) {
        var flightDetailModal = "#flight" + i;
        var modalIndex = i;
        $.ajax({
            url: "${createLink(controller: "tboFlightBooking" , action: "findFlightItinerary")}",
            type: "POST",
            data: {flightSearchResultVO: flightInstance, modalIndex: modalIndex, tripIndicator: tripIndicator},
            success: function (data) {
                $(flightDetailModal).html(data);
            }
        });
    }
    function showFlightFairRule(i, flightSearchResultVO, tripIndicator) {
        var flightFareModal = "#fare" + i;
        var modalIndex = i;
        $.ajax({
            url: "${createLink(controller: "tboFlightBooking" , action: "flightFairDetail")}",
            type: "POST",
            data: {flightSearchResultVO: flightSearchResultVO, modalIndex: modalIndex, tripIndicator: tripIndicator},
            success: function (data) {
                $(flightFareModal).html(data);
            }
        });
    }
    function showFlightSSR(i, flightSearchResultVO, tripIndicator) {
        var baggageModal = "#baggage" + i;
        var modalIndex = i;
        $.ajax({
            url: "${createLink(controller: "tboFlightBooking" , action: "findFlightSSR")}",
            type: "POST",
            data: {flightSearchResultVO: flightSearchResultVO, modalIndex: modalIndex, tripIndicator: tripIndicator},
            success: function (data) {
                $(baggageModal).html(data);
            }
        });
    }
    function validateForm1() {
        if ($('#validateOccupantForm').validate().form()) {
            $('#validateOccupantForm').submit();
        }
    }
</script>
</body>
</html>