<%@ page import="org.codehaus.groovy.grails.plugins.web.taglib.CountryTagLib; java.math.RoundingMode" %>
%{--<%@ page import="com.travelex.socials.Enums.HotelType; serviceProvider.ServiceProvider" contentType="text/html;charset=UTF-8" %>--}%
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Profile | Service Provider</title>
</head>

<body>
<br>
<br>
<br>
<br>
<br>

<div class="container" style="margin-bottom: 100px">
<div class="row">
    <h4><strong>Room Details:</strong></h4>
</div>

<div class="row">
    <table class="table table-bordered table-stripped table-condensed">
        <tr>
            <th><strong>Room(s)</strong></th>
            <th><strong>Room Type</strong></th>
            <th><strong>Check In</strong></th>
            <th><strong>Check Out</strong></th>
            <th><strong>Adult(s)</strong></th>
            <th><strong>Child(s)</strong></th>
            <th><strong>No. of guest</strong></th>
            <th><strong>Room rate</strong></th>
            <th><strong>Tax</strong></th>

        </tr>
        <g:each in="${(0..<noOfRoom?.toInteger())}" var="room" status="r">
            <tr>
                <td>Room ${r + 1}</td>
                <td>${roomDetailVO?.roomTypeName}</td>
                <td>${checkIn}</td>
                <td>${checkOut}</td>
                <td><g:if test="${r == 0}">
                    <label>${adult} Adult(s)</label>
                </g:if>
                    <g:if test="${r == 1}">
                        <label>${peopleInRoomTwo} Adult(s)</label>
                    </g:if>
                    <g:if test="${r == 2}">
                        <label>${peopleInRoomThree} Adult(s)</label>
                    </g:if>
                    <g:if test="${r == 3}">
                        <label>${peopleInRoomFour} Adult(s)</label>
                    </g:if></td>
                <td><g:if test="${r == 0}">
                    <label>${child} Child(s)</label>
                </g:if>
                    <g:if test="${r == 1}">
                        <label>${childInRoomTwo} Child(s)</label>
                    </g:if>
                    <g:if test="${r == 2}">
                        <label>${childInRoomThree} Child(s)</label>
                    </g:if>
                    <g:if test="${r == 3}">
                        <label>${childInRoomFour} Child(s)</label>
                    </g:if></td>
                <td><g:if test="${r == 0}">
                    <label>${adult.toInteger() + child.toInteger()}</label>
                </g:if>
                    <g:if test="${r == 1}">
                        <label>${peopleInRoomTwo.toInteger() + childInRoomTwo.toInteger()}</label>
                    </g:if>
                    <g:if test="${r == 2}">
                        <label>${peopleInRoomThree.toInteger() + childInRoomThree.toInteger()}</label>
                    </g:if>
                    <g:if test="${r == 3}">
                        <label>${peopleInRoomFour.toInteger() + childInRoomFour.toInteger()}</label>
                    </g:if></td>
                <td>${roomDetailVO.hotelRateVO.totalRate.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP) + roomDetailVO.hotelRateVO.agentMarkUp.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP)}</td>
                <td>${roomDetailVO.hotelRateVO.totalTax.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP) + trvlx360Tax.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP)}</td>

            </tr>
        </g:each>
        <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><label>Total:</label>
        </td><td>${total}</td></tr>

    </table>
</div>
<g:form controller="hotelService" action="bookingHotel" id="occupantDetailFormId"
        params="[roomId: roomId, sessionId: sessionId, hotelIndex: hotelIndex, noOfRoom: noOfRoom, child: child, childInRoomTwo: childInRoomTwo, childInRoomThree: childInRoomThree, childInRoomFour: childInRoomFour,
                 adult : adult, peopleInRoomTwo: peopleInRoomTwo, peopleInRoomThree: peopleInRoomThree, peopleInRoomFour: peopleInRoomFour, checkOut: checkOut, checkIn: checkIn]">

<div class="row">
    <p>
    <h4 class="pull-left"><strong>Occupant Details:</strong></h4>

    <i class="pull-right"><strong>Select Country:</strong>

<g:countrySelect name="country"
                 optionKey="${{ CountryTagLib.ISO3166_3[it] }}" class="form-control" default="ind"/>
</i>
</p>
</div>
<br>

<div class="row">

<input type="hidden" value="${total}" name="totalAmmount">
<input type="hidden" value="${roomDetailVO.roomTypeName}" name="productName">
<g:each in="${(0..<noOfRoom.toInteger())}" var="room" status="i">

<g:if test="${i == 0}">
    <g:set var="adultNo" value="${adult.toInteger()}"/>
    <g:each in="${(0..<adultNo)}" var="adult" status="ad">
        <g:if test="${ad == 0}">
            <strong>Lead Occupant Room ${i + 1}</strong>
            <br>
            <br>

            <div class="row">
                <div class="row">
                    <div class="col-md-2"></div>

                    <div class="col-md-2">
                        <div class="col-md-4">
                            <label>Title<travelx:star/></label>
                        </div>

                        <div class="col-md-8">
                            <g:select id="adult${ad + 1}FNameRoom${i + 1}"
                                      from="['Mr.', 'Mrs.', 'Miss.', 'Ms.', 'Master']"
                                      name="adult${ad + 1}FNameRoom${i + 1}"
                                      class="form-control" style="width: 100px"/>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="col-md-4">
                            <label>First Name<travelx:star/></label>
                        </div>

                        <div class="col-md-8">
                            <input required type="text" name="adult${ad + 1}MNameRoom${i + 1}"
                                   id="adult${ad + 1}MNameRoom${i + 1}"
                                   class="form-control ">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="col-md-4">
                            <label>Last Name<travelx:star/></label>
                        </div>

                        <div class="col-md-8">
                            <input required type="text" name="adult${ad + 1}LNameRoom${i + 1}"
                                   id="adult${ad + 1}LNameRoom${i + 1}"
                                   class="form-control ">
                        </div>

                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-md-4"></div>

                    <div class="col-md-4">
                        <div class="col-md-4">
                            <label>Email<travelx:star/></label>
                        </div>

                        <div class="col-md-8">
                            <input required type="email" name="adult${ad + 1}EmailRoom${i + 1}"
                                   id="adult${ad + 1}EmailRoom${i + 1}"
                                   class="form-control ">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="col-md-4">
                            <label>Phone<travelx:star/></label>
                        </div>

                        <div class="col-md-8">
                            <input maxlength="10" required type="tel" name="adult${ad + 1}PhoneRoom${i + 1}"
                                   id="adult${ad + 1}PhoneRoom${i + 1}"
                                   class="form-control ">
                        </div>
                    </div>

                </div>
            </div>
        </g:if>
        <g:else>
            <strong>${ad + 1 == 1 ? (ad + 1) + "st" : (ad + 1) + "nd"} Adult Room ${i + 1}</strong>

            <div class="row">
                <div class="col-md-2"></div>

                <div class="col-md-2">
                    <div class="col-md-4">
                        <label>Title<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <g:select from="['Mr.', 'Mrs.', 'Miss.', 'Ms.', 'Master']"
                                  name="adult${ad + 1}FNameRoom${i + 1}"
                                  id="adult${ad + 1}FNameRoom${i + 1}"
                                  class="form-control " style="width: 100px"/>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-4">
                        <label>First Name<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <input required type="text" name="adult${ad + 1}MNameRoom${i + 1}"
                               id="adult${ad + 1}MNameRoom${i + 1}"
                               class="form-control ">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-4">
                        <label>Last Name<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <input required type="text" name="adult${ad + 1}LNameRoom${i + 1}"
                               id="adult${ad + 1}LNameRoom${i + 1}"
                               class="form-control ">
                    </div>
                </div>
            </div>
        </g:else>
    </g:each>
    <br>
    <br>
    <g:if test="${child}">
        <g:set var="childNo" value="${child.toInteger()}"/>
        <g:each in="${(0..<childNo)}" var="child" status="ch">
            <strong>${ch + 1 == 1 ? (ch + 1) + "st" : (ch + 1) + "nd"} Child Room ${i + 1}</strong>

            <div class="row">
                <div class="col-md-2"></div>

                <div class="col-md-2">
                    <div class="col-md-4">
                        <label>Title<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <g:select from="['Mr.', 'Mrs.', 'Miss.', 'Ms.', 'Master']"
                                  name="child${ch + 1}Room${i + 1}FName"
                                  id="child${ch + 1}Room${i + 1}FName"
                                  class="form-control" style="width: 100px"/>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-4">
                        <label>First Name<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <input required type="text" name="child${ch + 1}Room${i + 1}MName"
                               id="child${ch + 1}Room${i + 1}MName"
                               class="form-control ">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-4">
                        <label>Last Name<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <input required type="text" name="child${ch + 1}Room${i + 1}LName"
                               id="child${ch + 1}Room${i + 1}LName"
                               class="form-control ">
                    </div>
                </div>
            </div>
        </g:each>
        <br>
    </g:if>
</g:if>
<br>
<g:if test="${i == 1}">
    <g:set var="adultNo" value="${peopleInRoomTwo.toInteger()}"/>
    <g:each in="${(0..<adultNo)}" var="adult" status="ad2">
        <g:if test="${ad2 == 0}">
            <strong>Lead Occupant Room ${i + 1}</strong>
            <br>
            <br>

            <div class="row">
                <div class="col-md-2"></div>

                <div class="col-md-2">
                    <div class="col-md-4">
                        <label>Title<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <g:select from="['Mr.', 'Mrs.', 'Miss.', 'Ms.', 'Master.']"
                                  name="adult${ad2 + 1}FNameRoom${i + 1}"
                                  id="adult${ad2 + 1}FNameRoom${i + 1}"
                                  class="form-control" style="width: 100px"/>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-4">
                        <label>First Name<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <input required type="text" name="adult${ad2 + 1}MNameRoom${i + 1}"
                               id="adult${ad2 + 1}MNameRoom${i + 1}"
                               class="form-control ">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-4">
                        <label>Last Name<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <input required type="text" name="adult${ad2 + 1}LNameRoom${i + 1}"
                               id="adult${ad2 + 1}LNameRoom${i + 1}"
                               class="form-control ">
                    </div>
                </div>
            </div>
        </g:if>
        <g:else>
            <strong>${ad2 + 1 == 1 ? (ad2 + 1) + "st" : (ad2 + 1) + "nd"} Adult Room ${i + 1}</strong>

            <div class="row">
                <div class="col-md-2"></div>

                <div class="col-md-2">
                    <div class="col-md-4">
                        <label>Title<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <g:select from="['Mr.', 'Mrs.', 'Miss.', 'Ms.', 'Master.']"
                                  name="adult${ad2 + 1}FNameRoom${i + 1}"
                                  id="adult${ad2 + 1}FNameRoom${i + 1}"
                                  class="form-control" style="width: 100px"/>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-4">
                        <label>First Name<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <input required type="text" name="adult${ad2 + 1}MNameRoom${i + 1}"
                               id="adult${ad2 + 1}MNameRoom${i + 1}"
                               class="form-control ">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-4">
                        <label>Last Name<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <input required type="text" name="adult${ad2 + 1}LNameRoom${i + 1}"
                               id="adult${ad2 + 1}LNameRoom${i + 1}"
                               class="form-control ">
                    </div>
                </div>
            </div>
        </g:else>
    </g:each>
    <br>
    <g:if test="${childInRoomTwo}">
        <g:set var="childNo" value="${childInRoomTwo.toInteger()}"/>
        <g:each in="${(0..<childNo)}" var="child" status="ch2">
            <strong>${ch2 + 1 == 1 ? (ch2 + 1) + "st" : (ch2 + 1) + "nd"} Child Room ${i + 1}</strong>

            <div class="row">
                <div class="col-md-2"></div>

                <div class="col-md-2">
                    <div class="col-md-4">
                        <label>Title<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <g:select from="['Mr.', 'Mrs.', 'Miss.', 'Ms.', 'Master.']"
                                  name="child${ch2 + 1}Room${i + 1}FName"
                                  id="child${ch2 + 1}Room${i + 1}FName"
                                  class="form-control" style="width: 100px"/>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-4">
                        <label>First Name<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <input required type="text" name="child${ch2 + 1}Room${i + 1}MName"
                               id="child${ch2 + 1}Room${i + 1}MName"
                               class="form-control ">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-4">
                        <label>Last Name<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <input required type="text" name="child${ch2 + 1}Room${i + 1}LName"
                               id="child${ch2 + 1}Room${i + 1}LName"
                               class="form-control ">
                    </div>
                </div>
            </div>
        </g:each>
        <br>
    </g:if>
</g:if>
<g:if test="${i == 2}">
    <g:set var="adultNo" value="${peopleInRoomThree?.toInteger()}"/>
    <g:each in="${(0..<adultNo)}" var="adult" status="ad3">
        <g:if test="${ad3 == 0}">
            <strong>Lead Occupant Room ${i + 1}</strong>
            <br>
            <br>

            <div class="row">
                <div class="col-md-2"></div>

                <div class="col-md-2">
                    <div class="col-md-4">
                        <label>Title<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <g:select from="['Mr.', 'Mrs.', 'Miss.', 'Ms.', 'Master.']"
                                  name="adult${ad3 + 1}FNameRoom${i + 1}"
                                  id="adult${ad3 + 1}FNameRoom${i + 1}"
                                  class="form-control" style="width: 100px"/>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-4">
                        <label>First Name<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <input required type="text" name="adult${ad3 + 1}MNameRoom${i + 1}"
                               id="adult${ad3 + 1}MNameRoom${i + 1}"
                               class="form-control ">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-4">
                        <label>Last Name<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <input required type="text" name="adult${ad3 + 1}LNameRoom${i + 1}"
                               id="adult${ad3 + 1}LNameRoom${i + 1}"
                               class="form-control ">
                    </div>
                </div>
            </div>
        </g:if>
        <g:else>
            <strong>${ad3 + 1 == 1 ? (ad3 + 1) + "st" : (ad3 + 1) + "nd"} Adult Room ${i + 1}
            <div class="row">
                <div class="col-md-2"></div>

                <div class="col-md-2">
                    <div class="col-md-4">
                        <label>Title<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <g:select from="['Mr.', 'Mrs.', 'Miss.', 'Ms.', 'Master.']"
                                  name="adult${ad3 + 1}FNameRoom${i + 1}"
                                  id="adult${ad3 + 1}FNameRoom${i + 1}"
                                  class="form-control" style="width: 100px"/>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-4">
                        <label>First Name<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <input required type="text" name="adult${ad3 + 1}MNameRoom${i + 1}"
                               id="adult${ad3 + 1}MNameRoom${i + 1}"
                               class="form-control ">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-4">
                        <label>Last Name<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <input required type="text" name="adult${ad3 + 1}LNameRoom${i + 1}"
                               id="adult${ad3 + 1}LNameRoom${i + 1}"
                               class="form-control ">
                    </div>
                </div>
            </div>
        </g:else>
    </g:each>
    <br>
    <g:if test="${childInRoomThree}">
        <g:set var="childNo" value="${childInRoomThree?.toInteger()}"/>
        <g:each in="${(0..<childNo)}" var="child" status="ch3">
            <strong>${ch3 + 1 == 1 ? (ch3 + 1) + "st" : (ch3 + 1) + "nd"} Child Room ${i + 1}</strong>

            <div class="row">
                <div class="col-md-2"></div>

                <div class="col-md-2">
                    <div class="col-md-4">
                        <label>Title<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <g:select from="['Mr.', 'Mrs.', 'Miss.', 'Ms.', 'Master.']"
                                  name="child${ch3 + 1}Room${i + 1}FName"
                                  id="child${ch3 + 1}Room${i + 1}FName"
                                  class="form-control" style="width: 100px"/>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-4">
                        <label>First Name<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <input required type="text" name="child${ch3 + 1}Room${i + 1}MName"
                               id="child${ch3 + 1}Room${i + 1}MName"
                               class="form-control ">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-4">
                        <label>Last Name<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <input required type="text" name="child${ch3 + 1}Room${i + 1}LName"
                               id="child${ch3 + 1}Room${i + 1}LName"
                               class="form-control ">
                    </div>
                </div>
            </div>
        </g:each>
        <br>
    </g:if>
</g:if>
<g:if test="${i == 3}">
    <g:set var="adultNo" value="${peopleInRoomFour?.toInteger()}"/>
    <g:each in="${(0..<adultNo)}" var="adult" status="ad4">
        <g:if test="${ad4 == 0}">
            <strong>Lead Occupant Room ${i + 1}</strong>

            <div class="row">
                <div class="col-md-2"></div>

                <div class="col-md-2">
                    <div class="col-md-4">
                        <label>Title<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <g:select from="['Mr.', 'Mrs.', 'Miss.', 'Ms.', 'Master.']"
                                  name="adult${ad4 + 1}FNameRoom${i + 1}"
                                  id="adult${ad4 + 1}FNameRoom${i + 1}"
                                  class="form-control" style="width: 100px"/>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-4">
                        <label>First Name<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <input required type="text" name="adult${ad4 + 1}MNameRoom${i + 1}"
                               id="adult${ad4 + 1}MNameRoom${i + 1}"
                               class="form-control ">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-4">
                        <label>Last Name<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <input required type="text" name="adult${ad4 + 1}LNameRoom${i + 1}"
                               id="adult${ad4 + 1}LNameRoom${i + 1}"
                               class="form-control ">
                    </div>
                </div>
            </div>
        </g:if>
        <g:else>
            <strong>${ad4 + 1 == 1 ? (ad4 + 1) + "st" : (ad4 + 1) + "nd"} Adult Room ${i + 1}</strong>

            <div class="row">
                <div class="col-md-2"></div>

                <div class="col-md-2">
                    <div class="col-md-4">
                        <label>Title<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <g:select from="['Mr.', 'Mrs.', 'Miss.', 'Ms.', 'Master.']"
                                  name="adult${ad4 + 1}FNameRoom${i + 1}"
                                  id="adult${ad4 + 1}FNameRoom${i + 1}"
                                  class="form-control" style="width: 100px"/>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-4">
                        <label>First Name<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <input required type="text" name="adult${ad4 + 1}MNameRoom${i + 1}"
                               id="adult${ad4 + 1}MNameRoom${i + 1}"
                               class="form-control ">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-4">
                        <label>Last Name<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <input required type="text" name="adult${ad4 + 1}LNameRoom${i + 1}"
                               id="adult${ad4 + 1}LNameRoom${i + 1}"
                               class="form-control ">
                    </div>
                </div>
            </div>
        </g:else>
    </g:each>

    <g:if test="${childInRoomFour}">
        <g:set var="childNo" value="${childInRoomFour?.toInteger()}"/>
        <g:each in="${(0..<childNo)}" var="child" status="ch4">
            <strong>${ch4 + 1 == 1 ? (ch4 + 1) + "st" : (ch4 + 1) + "nd"} Child Room ${i + 1}</strong>

            <div class="row">
                <div class="col-md-2"></div>

                <div class="col-md-2">
                    <div class="col-md-4">
                        <label>Title<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <g:select from="['Mr.', 'Mrs.', 'Miss.', 'Ms.', 'Master.']"
                                  name="child${ch4 + 1}Room${i + 1}FName"
                                  id="child${ch4 + 1}Room${i + 1}FName"
                                  class="form-control" style="width: 100px"/>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-4">
                        <label>First Name<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <input required type="text" name="child${ch4 + 1}Room${i + 1}MName"
                               id="child${ch4 + 1}Room${i + 1}MName"
                               class="form-control ">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-4">
                        <label>Last Name<travelx:star/></label>
                    </div>

                    <div class="col-md-8">
                        <input required type="text" name="child${ch4 + 1}Room${i + 1}LName"
                               id="child${ch4 + 1}Room${i + 1}LName"
                               class="form-control ">
                    </div>
                </div>
            </div>
        </g:each>
    </g:if>
</g:if>
</g:each>
<br>
<input type="submit" value="Proceed To Booking" id="proceedId" class=" col-md-offset-4 btn btn-success btn-lg">
</div>
</g:form>

</div>
<script>
    $("#proceedId").click(function () {
        $("#occupantDetailFormId").validate({
            debug: true,
            rules: {
                adult1FNameRoom1: {
                    required: true
                },
                adult1AgeRoom1: {
                    required: true
                }
            },
            messages: {
                adult1FNameRoom1: {
                    required: "Adult 1 name cannot be left blank"
                },
                adult1AgeRoom1: {
                    required: "Adult 1 age cannot be left blank"
                }
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    })
</script>
</body>
</html>