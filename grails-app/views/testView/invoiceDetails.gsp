%{--<%@ page import="com.travelex.socials.Enums.HotelType; serviceProvider.ServiceProvider" contentType="text/html;charset=UTF-8" %>--}%
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Profile | Service Provider</title>
    <style>body {
        background-color: snow
    }</style>
    %{--<script src="${resource(dir: '/js/multiDate', file: 'jquery-1.7.2.js')}"></script>--}%
    %{--<script src="${resource(dir: '/js/multiDate', file: 'jquery.ui.core.js')}"></script>--}%
    <link rel="stylesheet" href="${resource(dir: '/css', file: 'bootstrap-formhelpers.css')}" type="text/css"/>
    <link rel="stylesheet" href="${resource(dir: '/css', file: 'bootstrap-formhelpers.min.css')}" type="text/css"/>
    <script src="${resource(dir: '/js', file: 'bootstrap-formhelpers.js')}"></script>
    <script src="${resource(dir: '/js', file: 'bootstrap-formhelpers-countries.js')}"></script>




    %{--<script src="/assets/js/plugins/parsley/parsley.js"></script>--}%

</head>

<body>
<br>
<br>

<div id="wrapper">

    <nav id="top-bar" class="collapse top-bar-collapse">

    </nav> <!-- /#top-bar -->
    <div id="content1">

        <div id="content-header">
            <div class="col-md-3">
                <h1>Add Occupant Details</h1>
            </div>

            <div class="col-md-4">
                <br>
                <g:link controller="home" action="index"
                        class="btn btn-lg btn-warning">&nbsp; &nbsp;Back &nbsp;&nbsp;</g:link>
            </div>
        </div> <!-- #content-header -->


    <div id="content-container">
        %{--<g:hasErrors bean="${tboSearchParamsCO}">--}%
        %{--<ul class="errors alert alert-error" role="alert">--}%
        %{--<g:eachError bean="${tboSearchParamsCO}" var="error">--}%
        %{--<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message--}%
        %{--error="${error}"/></li>--}%
        %{--</g:eachError>--}%
        %{--</ul>--}%
        %{--</g:hasErrors>--}%
        <g:form controller="hotelService" action="bookingHotel" id="validate-basic1" data-validate="parsley"
                class="form parsley-form"
                params="[roomId: roomId, sessionId: sessionId, hotelIndex: hotelIndex, noOfRoom: noOfRoom, child: child, childInRoomTwo: childInRoomTwo, childInRoomThree: childInRoomThree, childInRoomFour: childInRoomFour,
                         adult : adult, peopleInRoomTwo: peopleInRoomTwo, peopleInRoomThree: peopleInRoomThree, peopleInRoomFour: peopleInRoomFour, checkOut: checkOut, checkIn: checkIn]">

            <div class="portlet">
                <div class="portlet-header">
                    <h3>
                        <i class="fa fa-tasks"></i>
                        Room Details
                    </h3>
                </div>

                <div class="portlet-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <h4><strong>Room No.</strong></h4>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <h4><strong>Room Type</strong></h4>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <h4><strong>No. Of Guest(s)</strong></h4>
                                    </div>
                                </div>



                                %{--<g:each in="${(0..<noOfRoom.toInteger())}" var="room" status="r">--}%
                                %{--<div class="col-sm-4">--}%
                                %{--<div class="form-group">--}%
                                %{--<label>Room ${r + 1}</label>--}%
                                %{--</div>--}%
                                %{--</div>--}%

                                %{--<div class="col-sm-4">--}%
                                %{--<div class="form-group">--}%
                                %{--<label>${roomDetailVO.roomTypeName}</label>--}%
                                %{--</div>--}%
                                %{--</div>--}%

                                %{--<div class="col-sm-4">--}%
                                %{--<div class="form-group">--}%
                                %{--<g:if test="${r == 0}">--}%
                                %{--<label>${adult} Adult(s)</label>--}%
                                %{--<label>${child} Child(s)</label>--}%
                                %{--</g:if>--}%
                                %{--<g:if test="${r == 1}">--}%
                                %{--<label>${peopleInRoomTwo} Adult(s)</label>--}%
                                %{--<label>${childInRoomTwo} Child(s)</label>--}%
                                %{--</g:if>--}%
                                %{--<g:if test="${r == 2}">--}%
                                %{--<label>${peopleInRoomThree} Adult(s)</label>--}%
                                %{--<label>${childInRoomThree} Child(s)</label>--}%
                                %{--</g:if>--}%
                                %{--<g:if test="${r == 3}">--}%
                                %{--<label>${peopleInRoomFour} Adult(s)</label>--}%
                                %{--<label>${childInRoomFour} Child(s)</label>--}%
                                %{--</g:if>--}%
                                %{--</div>--}%
                                %{--</div>--}%
                                %{--</g:each>--}%


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <h4>Room Type:- <p style="display: inline"
                                                           class="text-success"></p></h4>
                                        <g:set var="roomRate" value=""/>
                                        <g:set var="roomRate2"
                                               value=""/>

                                        <h5>Room Rate:- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p class="text-success"
                                                                                               style="display: inline"></p>
                                        </h5>
                                        <h5>No. of room(s):- <p class="text-success" style="display: inline"></p></h5>
                                        <g:set var="noOfRooms" value=""/>
                                        <g:set var="roomRateForTotal"
                                               value=""/>
                                        <g:set var="totalRate" value=""/>

                                        <h5>Total:-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <p
                                                class="text-success" style="display: inline">${totalRate}</p></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="submit" value="Proceed to booking" class="pull-right btn btn-success btn-lg">


            </div>
        </g:form>
    </div>

</div><!-- /.row -->

<script src="${resource(dir: '/js', file: 'bootstrap-formhelpers.js')}"></script>
<script src="${resource(dir: '/js', file: 'bootstrap-formhelpers-countries.js')}"></script>


%{----}%

<style>
#content1 {
    background: #FFF;
    width: auto;
    min-height: 800px;
    z-index: 18;
    padding-bottom: 25px;
    margin-left: 20px;
    margin-right: 20px;
    position: relative;
    left: auto;
    top: auto;
}</style>

<script type="text/javascript">
    $('#validate-basic1').validate();

</script>
</body>
</html>