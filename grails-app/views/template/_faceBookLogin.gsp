<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function () {
        FB.init({
//            appId      : '727532057290437',
            appId: '803054073093613',

            status: true,
            cookie: true,
            xfbml: true

        });
    };

    (function (d) {
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    }(document));

</script>

<div class="fb-login-button" style="display: inline" data-max-rows="1" data-size="medium" onlogin="checkLoginState()"
     data-show-faces="false" data-auto-logout-link="false"></div>

<div style="display: none">
    <g:form controller="signIn" action="signInWithFacebook">
        <input type="text" name="username" id="fbEmail">
        <input type="password" name="password" id="fbPassword">
        <input type="password" name="confirmPassword" id="fbConfPassword">
        <input type="text" name="name" id="fbName"/>
        <input type="text" name="profileId" id="fbProfileId"/>
        <input type="submit" id="facebookSubmit">

    </g:form>
</div>
<script>
    function statusChangeCallback(response) {
        console.log('statusChangeCallback');
        console.log(response);

        if (response.status === 'connected') {
            getUserInfo(response.authResponse.accessToken);
        } else if (response.status === 'not_authorized') {
            document.getElementById('status').innerHTML = 'Please log ' +
                    'into this app.';
        } else {

            document.getElementById('status').innerHTML = 'Please log ' +
                    'into Facebook.';
        }
    }

</script>
<script>


    function checkLoginState() {
        FB.getLoginStatus(function (response) {
            statusChangeCallback(response);
        });
    }


    function getUserInfo(accesstoken) {
        FB.api('/me', function (response) {
            console.log(JSON.stringify(response));
            $('#fbName').val(response.name);
            $('#fbEmail').val(response.email);
            $('#fbProfileId').val(response.id);
            $('#fbPassword').val(accesstoken);
            $('#fbConfPassword').val(accesstoken);
            setTimeout(function () {
                $('#facebookSubmit').click();
            }, 1000);
            //response.name       - User Full name
            //response.link       - User Facebook URL
            //response.username   - User name
            //response.id         - id
            //response.email      - User email

        });

    }
</script>