<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"><!--<![endif]-->
%{--<%@ page import="" %>--}%

<head>

    <meta name="layout" content="main">
    <title>Plan Activity-TravelX360</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="">
    <meta name="author" content=""/>
    %{--<link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'bootstrap.min.css')}" type="text/css">--}%

    <link rel="stylesheet" href="${resource(dir: '/css', file: 'jquery-ui.css')}" type="text/css"/>
    <link rel="stylesheet" href="${resource(dir: '/assets/js/plugins/fullcalendar', file: 'fullcalendar.css')}"
          type="text/css"/>


    <link rel="stylesheet"
          href="${resource(dir: '/assets', file: '/js/libs/css/ui-lightness/jquery-ui-1.9.2.custom.css')}"
          type="text/css"/>
    %{--<script type="text/javascript" src="${resource(dir: '/js', file: 'countries.js')}"></script>--}%
    %{--<script type="text/javascript" src="${resource(dir: '/js', file: 'Country.js')}"></script>--}%
    <script src="${resource(dir: '/assets/js/plugins/parsley', file: 'parsley.js')}"></script>



    %{--//        $(document).ready(function () {--}%
    %{--//            $('#full-calendar').fullCalendar({--}%
    %{--//                events: [--}%
    %{--//                    {--}%
    %{--//                        title: 'event1',--}%
    %{--//                        start: '2014-01-02'--}%
    %{--//                        color: "blue"--}%
    %{--//                    },--}%
    %{--//                    {--}%
    %{--//                        title: 'event2',--}%
    %{--//                        start: '2014-01-05',--}%
    %{--//                        end: '2014-01-07'--}%
    %{--//                        color: "blue"--}%
    %{--//--}%
    %{--//                    },--}%
    %{--//                    {--}%
    %{--//                        title: 'event3',--}%
    %{--//                        start: '2014-01-09 12:30:00',--}%
    %{--//                        color: "blue"--}%
    %{--//--}%
    %{--//                        allDay: false // will make the time show--}%
    %{--//                    }--}%
    %{--//                ]--}%
    %{--//            });--}%
    %{--//        }--}%
</head>

<body>

<div id="wrapper">

    <header id="header">

        <h1 id="site-logo">
            <a href="./index.html">
                <img src="./img/logos/logo.png" alt="Site Logo"/>
            </a>
        </h1>

        <a href="javascript:;" data-toggle="collapse" data-target=".top-bar-collapse" id="top-bar-toggle"
           class="navbar-toggle collapsed">
            <i class="fa fa-cog"></i>
        </a>

        <a href="javascript:;" data-toggle="collapse" data-target=".sidebar-collapse" id="sidebar-toggle"
           class="navbar-toggle collapsed">
            <i class="fa fa-reorder"></i>
        </a>

    </header> <!-- header -->




    <div id="content1">

        <div id="content-header">
            <h1>Calendar</h1>
        </div> <!-- #content-header -->


        <div id="content-container">

            <div class="row">

                <div class="col-md-12">

                    <div class="portlet">

                        <div class="portlet-header">

                            <h3>
                                <i class="fa fa-calendar"></i>
                                Full Calendar
                            </h3>

                        </div> <!-- /.portlet-header -->

                        <div class="portlet-content">
                            %{--<g:each in="${startDateList}" status="i" var="startDate">--}%
                                <div id="full-calendar"></div> <!-- /#full-calendar -->
                            %{--</g:each>--}%
                        </div> <!-- /.portlet-content -->

                    </div> <!-- /.portlet -->

                </div> <!-- /.col-md-8 -->

            </div> <!-- /.row -->

        </div> <!-- /#content-container -->

    </div> <!-- #content -->

</div> <!-- #wrapper -->

<footer id="footer">
    <ul class="nav pull-right">
        <li>
            Copyright &copy; 2014 Travelex 360.
        </li>
    </ul>
</footer>

<script src="${resource(dir: '/assets/js/libs', file: 'jquery-1.9.1.min.js')}"></script>
<script src="${resource(dir: '/assets/js/libs', file: 'bootstrap.min.js')}"></script>
<script src="${resource(dir: '/assets/js', file: 'App.js')}"></script>
<script src="${resource(dir: '/assets/js/plugins/fullcalendar', file: 'fullcalendar.min.js')}"></script>
%{--<script src="${resource(dir: '/assets/js/demos', file: 'calendar.js')}"></script>--}%
<script>
    $("#login-form").validator.form();
</script>
<script src="${resource(dir: '/assets/js/plugins/fullcalendar', file: 'fullcalendar.js')}"></script>

<style>
#content1 {
    background: #FFF;
    width: auto;
    min-height: 800px;
    z-index: 18;
    padding-bottom: 25px;
    margin-left: 20px;
    margin-right: 20px;
    position: relative;
    left: auto;
    top: auto;
}</style>

<script>
//    $('.fc-event').remove();
    //    $(document).ready(function(){
    %{--jQuery.each(${startDateList}, function(index, value) {--}%
    %{--jQuery.each(${startDateList},function(){--}%
    $(function () {

//        var $currentElem;
//    var i, l;
        %{--var date2=${startDate2};--}%
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        %{--$(${startDateList}).each(function() {--}%

        $('#full-calendar').fullCalendar({
            header: {
                left: 'prev,next',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
//            editable: true,
            droppable: true,
            startEditable:false,
//            drop: function (date, allDay) { // this function is called when something is dropped
//
//                // retrieve the dropped element's stored Event Object
//                var originalEventObject = $(this).data('eventObject');
//
//                // we need to copy it, so that multiple events don't have a reference to the same object
//                var copiedEventObject = $.extend({}, originalEventObject);
//
//                // assign it the date that was reported
//                copiedEventObject.start = date;
//                copiedEventObject.allDay = allDay;
//                copiedEventObject.className = $(this).attr("data-category");
//
//                // render the event on the calendar
//                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
//                $('#full-calendar').fullCalendar('renderEvent', copiedEventObject, true);
//
//                // is the "remove after drop" checkbox checked?
//                //if ($('#drop-remove').is(':checked')) {
//                // if so, remove the element from the "Draggable Events" list
//                $(this).remove();
//                //}
//
//            },

            %{--${startDateList}.each(a,function(){--}%
            %{--events: [--}%
            %{--{--}%
            %{--title: 'All Day Event2',--}%
            %{--start: a,--}%
            %{--end: new Date (y, m, 4),--}%
            %{--className: 'fc-red'--}%
            %{--}]})--}%

            %{--.each( ${startDateList}, function( i, l ){--}%
            %{--alert( "Index #" + i + ": " + l );--}%
            %{--});--}%

            %{--$('${startDateList}').each(function() {--}%
//        $(startDateList).each(function(i) {
//            events: [
//                {
//                    title: 'All Day Event2',
//                    start: new Date(y, m, 3),
//                    end: new Date (y, m, 4),
//                    className: 'fc-red'
//                }
//            alert(index + ': ' + $(this).attr("id"));
//        });

//        $.each( myArray, function( intValue, currentElement ) {
//
//                // Do work with currentElement
//
//            });

            %{--jQuery.each(${startDateList}, function(index, value) {--}%
            events: ${json}
//                    [



                %{--for( i = 0, l = ${startDateList}.length; i < l; i++) {--}%

//        {
//            title: 'All Day Event2',
//                    start : new Date(y, m, 3),
//                end: new Date (y, m, 4),
//                className: 'fc-red'
//        }
//    }


                %{--<g:each in="${startDateList}" status="i" var="startDate">--}%


                %{--{--}%

                %{--title: 'All Day Event2',--}%
                %{--start: new Date(y, m, 3),--}%
                %{--end: new Date (y, m, 4),--}%
                %{--className: 'fc-red'--}%
                %{--}--}%
                %{--</g:each>--}%

                %{--$.each( ${startDateList}, function() {--}%

                    %{--title: 'All Day Event2',--}%
                            %{--start: new Date(y, m, 3),--}%
                            %{--end: new Date(y, m, 4),--}%
                            %{--className: 'fc-red'--}%
                    %{--// Do work with currentElement--}%

                %{--})--}%


                %{--$(${startDateList}).each(function() {--}%



%{--//                    $currentElem = $(this);--}%
                    %{--{--}%

                        %{--title: 'All Day Event2',--}%

                                %{--start: new Date(y, m, 3),--}%

                            %{--end: new Date(y, m, 4),--}%

                            %{--className: 'fc-red'--}%
                    %{--}--}%
                    %{--// Do work with the current element--}%

                %{--})--}%


//        {
//
//                    title: 'All Day Event2',
//                    start: new Date(y, m, 3),
//                    end: new Date(y, m, 4),
//                    className: 'fc-red'
//                },
//                {
//                    title: 'Long Event2',
//                    start: new Date(y, m, d - 2),
//                    end: new Date(y, m, d - 1),
//                    className: 'fc-yellow'
//                },
//                {
//                    id: 999,
//                    title: 'Repeating Event2',
//                    start: new Date(y, m, d - 1, 16, 0),
//                    allDay: false,
//                    className: 'fc-grey'
//                },
//                {
//                    id: 999,
//                    title: 'Repeating Event2',
//                    start: new Date(y, m, d + 3, 14, 0),
//                    end: new Date(y, m, d + 4, 14, 0),
//                    allDay: false,
//                    className: 'fc-charcoal'
//                },
//                {
//                    title: 'Meeting2',
//                    start: new Date(y, m, d - 14, 11, 30),
//                    end: new Date(y, m, d - 12, 11, 30),
//                    allDay: false,
//                    className: 'fc-grey'
//                },
//                {
//                    title: 'Lunch2',
//                    start: new Date(y, m, d, 13, 0),
//                    end: new Date(y, m, d, 15, 0),
//                    allDay: false,
//                    className: 'fc-red'
//                }
//            ]
        });


    });
    //});

    //    }
</script>
</body>
</html>