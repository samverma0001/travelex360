<a href="#" class="main-icon-box">
    <img src="${resource(dir: 'images',file: 'ico1.png')}">
    <h3>PLAN TRAVEL</h3>
</a>
<a href="${createLink(controller: "hotelService", action: "bookHotel")}" class="main-icon-box">
    <img src="${resource(dir: 'images',file: 'ico2.png')}">
    <h3>BOOK HOTEL</h3>
</a>
<a href="#" class="main-icon-box">
    <img src="${resource(dir: 'images',file: 'ico3.png')}">
    <h3>ACTIVITY</h3>
</a>
<a href="${createLink(controller: "tboFlightBooking", action: "searchFlights")}" class="main-icon-box">
    <img src="${resource(dir: 'images',file: 'ico4.png')}">
    <h3>FLIGHT BOOKING</h3>
</a>
<a href="${createLink(controller: "serviceProvider", action: "chooseService")}" class="main-icon-box">
    <img src="${resource(dir: 'images',file: 'ico5.png')}">
    <h3>SERVICE PROVIDER</h3>
</a>
%{--<a href="${createLink(controller: "holiday", action: "showHolidayCategory")}" class="main-icon-box">--}%
    %{--<img src="${resource(dir: 'images',file: 'ico6.png')}">--}%
    %{--<h3>PACKAGES</h3>--}%
%{--</a>--}%
<a href="http://www.travelex360plus.net" class="main-icon-box">
    <img src="${resource(dir: 'images',file: 'ico6.png')}">
    <h3>PACKAGES</h3>
</a>