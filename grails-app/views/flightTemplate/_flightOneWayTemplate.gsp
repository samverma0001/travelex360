<g:if test="${flightSearchVOList}">
    <g:each in="${flightSearchVOList}" status="i" var="flightInstance">
        <div class="flight-list-item">
            <table class="table airline-listing-table">
                <tbody>
                <tr>
                    <td class="col-airline">
                        <span>
                            <g:if test="${flightInstance}">
                                <g:if test="${flightInstance.flightSegmentVO?.first()?.airLineName == "JetLite"}">
                                    <img src="${resource(dir: "/images/flightImages", file: "jetlite1.jpg")}"
                                         height="30"
                                         width="60"
                                         class="img-rounded">
                                </g:if>
                                <g:elseif
                                        test="${flightInstance.flightSegmentVO?.first()?.airLineName == "Jet Airways"}">
                                    <img src="${resource(dir: "images", file: "jetairways.png")}">
                                </g:elseif>
                                <g:elseif
                                        test="${flightInstance.flightSegmentVO?.first()?.airLineName == "Air India"}">
                                    <img src="${resource(dir: "images", file: "AIRINDIA.png")}">

                                </g:elseif>
                                <g:elseif
                                        test="${flightInstance.flightSegmentVO?.first()?.airLineName == "SpiceJet"}">
                                    <img src="${resource(dir: "images", file: "SpiceJet.png")}">

                                </g:elseif>
                                <g:elseif
                                        test="${flightInstance.flightSegmentVO?.first()?.airLineName == "IndiGo"}">
                                    <img src="${resource(dir: "images", file: "IndiGo.png")}">
                                </g:elseif>
                                <g:elseif
                                        test="${flightInstance.flightSegmentVO?.first()?.airLineName == "GoAir"}">
                                    <img src="${resource(dir: "images", file: "GoAir.png")}">
                                </g:elseif>
                                <g:elseif
                                        test="${flightInstance.flightSegmentVO?.first()?.airLineName == "Air Asia"}">
                                    <img src="${resource(dir: "images", file: "airasia.png")}">
                                </g:elseif>
                                <g:elseif
                                        test="${flightInstance.flightSegmentVO?.first()?.airLineName == "Fly Dubai"}">
                                    <img src="${resource(dir: "images", file: "FLYDUBAI.png")}">
                                </g:elseif>
                                <g:elseif
                                        test="${flightInstance.flightSegmentVO?.first()?.airLineName == "Air India Express"}">
                                    <img src="${resource(dir: "images", file: "AirIndiaExpress.png")}">
                                </g:elseif>
                                <br>
                                ${flightInstance?.flightSegmentVO?.first()?.airLineName}&nbsp;&nbsp;
                            </g:if>
                        </span>

                        <p><small>${flightInstance?.flightSegmentVO?.first()?.airLineCode}-${flightInstance?.flightSegmentVO?.first()?.flightNumber}</small>
                        </p>
                    </td>
                    <td class="col-dapart">
                        <g:set var="flightTimeOri"
                               value="${(flightInstance?.flightSegmentVO?.first()?.departureTime?.tokenize("T")?.last())}"/>
                        <span class="text-large">${flightTimeOri.substring(0, flightTimeOri.lastIndexOf(":"))}</span>
                        <small class="text-muted">${flightInstance?.flightSegmentVO?.first()?.originAirPortName}</small>
                    </td>
                    <td class="col-arrive">
                        <g:set var="flightTimeDes"
                               value="${(flightInstance?.flightSegmentVO?.first()?.arrivalTime?.tokenize("T")?.last())}"/>
                        <span class="text-large">${flightTimeDes.substring(0, flightTimeDes.lastIndexOf(":"))}</span>
                        <small class="text-muted">${flightInstance?.flightSegmentVO?.first()?.destinationAirPortName}</small>
                    </td>
                    <td class="col-duration">
                        <g:set var="durationOfFlight"
                               value="${flightInstance?.obDuration}"/>
                        <span>${durationOfFlight}</span>
                        <small class="text-muted"><g:if test="${flightInstance?.flightSegmentVO?.size() > 1}">
                            ${flightInstance?.flightSegmentVO?.size() - 1}
                        </g:if>
                        <g:else>
                            0
                        </g:else> Stop</small>
                    </td>
                    <td class="col-price">
                        <div class="price">
                            <span class="text-large color-default">Rs. ${flightInstance?.flightFare?.offeredFare}</span>
                        </div>
                        <g:if test="${flightInstance}">
                            <g:link class="btn btn-default"
                                    controller="tboFlightBooking"
                                    action="addOccupantForFlight"
                                    params="[flightInstanceId: flightInstance]"
                                    style="text-decoration: none;">Book Now
                            </g:link>
                        </g:if>

                    </td>
                </tr>
                </tbody>
            </table>

            <ul id="myTab" class="flight-list-tab-link" role="tablist">
                <li class="">
                    <a href="#flight${i}" role="tab" data-toggle="tab" aria-controls="home"
                       aria-expanded="true"
                       onclick="flightItineary('${i}', '${flightInstance}', '${flightInstance?.tripIndicator}')">Flight Details</a>
                </li>
                <li class="">
                    <a aria-expanded="false" href="#fare${i}" role="tab" data-toggle="tab"
                       aria-controls="profile"
                       onclick="showFlightFairRule('${i}', '${flightInstance}', '${flightInstance?.tripIndicator}')">Fare Details</a>
                </li>
                <li class="">
                    <a aria-expanded="false" href="#baggage${i}" role="tab" data-toggle="tab" aria-controls="profile"
                       onclick="showFlightSSR('${i}', '${flightInstance}', '${flightInstance?.tripIndicator}')">Baggage</a>
                </li>
            </ul>

            <div class="tab-content flight-list-tab-content">
                <div class="tab-content-close"><i class="glyphicon glyphicon-remove"></i></div>

                <div role="tabpanel" class="tab-pane fade active in" id="flight${i}" aria-labelledby=""></div>

                <div role="tabpanel" class="tab-pane fade" id="fare${i}" aria-labelledby=""></div>

                <div role="tabpanel" class="tab-pane fade" id="baggage${i}" aria-labelledby=""></div>
            </div>
            <util:remoteNonStopPageScroll action='flightSearchResultPaginated' controller="tboFlightBooking"
                                          total="${total}"
                                          update="updateFlightFilterList"/>
        </div>
    </g:each>
</g:if>
<g:else>
    <h3>
        Flight Not Found
    </h3>
</g:else>


%{--<script>--}%
%{--$(window).scroll(function () {--}%
%{--var max = 10;--}%
%{--var offset = ${offset} +max;--}%

%{--if ($(window).scrollTop() + $(window).height() == $(document).height()) {--}%
%{--$.ajax({--}%
%{--url: "${createLink(controller: "tboFlightBooking" , action: "flightSearchResultPaginated",absolute: true)}",--}%
%{--type: "POST",--}%
%{--data: {total: '${total}', offset: offset},--}%
%{--success: function (data) {--}%
%{--alert('--------data--------' + data);--}%
%{--//                    var temp = $('#updateFlightFilterList').html();--}%
%{--//                    $('#updateFlightFilterList').append(temp);--}%
%{--}--}%
%{--});--}%
%{--}--}%
%{--});--}%

%{--</script>--}%








%{--<ul class="pull-right pagination">--}%

%{--<g:paginate controller="tboFlightBooking" action="flightSearchResultPaginated"--}%
%{--total="${total}"--}%
%{--params="${params}"/>--}%

%{--</ul>--}%

%{--<style>--}%
%{--.pagination {--}%
%{--margin: 3px 0px 3px 0px;--}%
%{--}--}%

%{--.pagination a {--}%
%{--padding: 2px 4px 2px 4px;--}%
%{--background-color: #A4A4A4;--}%
%{--border: 1px solid #EEEEEE;--}%
%{--text-decoration: none;--}%
%{--font-size: 10pt;--}%
%{--font-variant: small-caps;--}%
%{--color: #EEEEEE;--}%
%{--}--}%

%{--.pagination a:hover {--}%
%{--text-decoration: underline;--}%
%{--background-color: #888888;--}%
%{--border: 1px solid #AA4444;--}%
%{--color: #FFFFFF;--}%
%{--}--}%
%{--</style>--}%