<div class="row" style="border: 2px solid">
    <h5 class="text-primary text-center"><a href=""
                                            onclick="window.print()">Print Invoice</a>
    </h5>

    <table class="table table-bordered table-condensed ">
        <tr>
            <td>INVOICE NO: TR/360/2321</td>
            <td>INVOICE DATE:12/12/2013</td>
            <td>PNR:&nbsp;&nbsp;&nbsp;${bookedFlight?.pnr}</td>
        </tr>

    </table>

    <div class="col-md-4">
        <addresss>
            <strong>Travelex 360 Pvt.Ltd.</strong>
            <br>
            25/60,&nbsp;A block,
            <br>
            Middle circle, Connaught Place
            <br>
            New Delhi,110001
        </addresss>
    </div>

    <div class="col-md-4">
        <address>
            <strong>To:-</strong>
            <br>
            Lead Pax :- Vipul Gupta
            <br>
            Nationality&nbsp;&nbsp;&nbsp;india
        </address>
    </div>

    <div class="col-md-4">
        <address>
            <strong>Journey Date:-11/12/2015</strong>
            <br>
            Service tax reg no&nbsp;&nbsp;&nbsp;---------
            <br>
            CI number&nbsp;&nbsp;&nbsp;--------
        </address>
    </div>
    <br>
    <br>
    <br>
    <table class="table table-bordered table-condensed">
        <tr>
            <td class="text-center"><strong>Sr. No.</strong></td>
            <td class="text-center"><strong>Title</strong></td>
            <td class="text-center"><strong>First Name</strong></td>
            <td class="text-center"><strong>Last Name</strong></td>
            <td class="text-center"><strong>Email</strong></td>
            <td class="text-center"><strong>Phone</strong></td>
            <td class="text-center"><strong>Type</strong></td>
            <td class="text-center"><strong>Date Of Birth</strong></td>
        </tr>
        <g:each in="${bookedFlightPassengerList}" var="bookedFlightPassenger">
            <tr>
                <td class="text-center">${bookedFlightPassenger?.id}</td>
                <td class="text-center">${bookedFlightPassenger?.title}</td>
                <td class="text-center">${bookedFlightPassenger?.firstName}</td>
                <td class="text-center">${bookedFlightPassenger?.lastName}</td>
                <td class="text-center">${bookedFlightPassenger?.email}</td>
                <td class="text-center">${bookedFlightPassenger?.phone}</td>
                <td class="text-center">${bookedFlightPassenger?.type}</td>
                <td class="text-center">${bookedFlightPassenger?.dateOfBirth}</td>
            </tr>
        </g:each>
    </table>
    <table class="table table-bordered table-condensed">
        <tr>
            <td class="text-center"><strong>Base Fare</strong></td>
            <td class="text-center"><strong>Tax</strong></td>
            <td class="text-center"><strong>Service Tax</strong></td>
            <td class="text-center"><strong>Other Charges</strong></td>
            <td class="text-center"><strong>Add. Txn Fee</strong></td>
            <td class="text-center"><strong>AL Trans Fee</strong></td>
            <td class="text-center"><strong>Offered Fare</strong></td>
            <td class="text-center"><strong>Published Fare</strong></td>
        </tr>
        <tr>
            <td class="text-center">${bookedFlightFare?.baseFare}</td>
            <td class="text-center">${bookedFlightFare?.tax}</td>
            <td class="text-center">${bookedFlightFare?.serviceTax}</td>
            <td class="text-center">${bookedFlightFare?.otherCharges}</td>
            <td class="text-center">${bookedFlightFare?.additionalTxnFee}</td>
            <td class="text-center">${bookedFlightFare?.airTransFee}</td>
            <td class="text-center">${bookedFlightFare?.offeredFare}</td>
            <td class="text-center">${bookedFlightFare?.publishedPrice}</td>
        </tr>
    </table>

</div>
