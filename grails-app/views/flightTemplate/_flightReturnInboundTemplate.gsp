<g:each in="${flightSearchVOListPaginatedInbound}" var="flightInstanceInbound" status="j">
    <g:if test="${flightInstanceInbound.tripIndicator == '2'}">
        <li xmlns="http://www.w3.org/1999/html">
            <div class="listItem" id="alreadySelected${j}">
                <g:set var="flightTimeDept"
                       value="${(flightInstanceInbound?.flightSegmentVO?.first()?.departureTime?.tokenize("T")?.last())}"/>
                <g:set var="flightTimeArr"
                       value="${(flightInstanceInbound?.flightSegmentVO?.last()?.arrivalTime?.tokenize("T")?.last())}"/>
                <label class="">
                    <table class="table airline-listing-table">
                        <tbody>
                        <tr>
                            <th class="col-empty">
                                <input id="arriveId${j}" type="radio" name="arrive" class="selectable-box"
                                       onclick="setCurrentSelectionForInbound('${flightInstanceInbound?.flightSegmentVO?.first()?.airLineName}', '${flightInstanceInbound?.flightSegmentVO?.first()?.originAirPortCode}', '${flightInstanceInbound?.flightSegmentVO?.first()?.destinationAirPortCode}', '${flightInstanceInbound?.flightSegmentVO?.first()?.flightNumber}', '${flightTimeDept}', '${flightTimeArr}', '${j}', '${flightInstanceInbound}')">
                            </th>
                            <td class="col-airline">
                                <span><g:if
                                        test="${flightInstanceInbound.flightSegmentVO?.first()?.airLineName == "JetLite"}">
                                    <img src="${resource(dir: "/images/flightImages", file: "jetlite1.jpg")}"
                                         height="30"
                                         width="60"
                                         class="img-rounded">

                                </g:if>
                                    <g:elseif
                                            test="${flightInstanceInbound.flightSegmentVO?.first()?.airLineName == "Jet Airways"}">
                                        <img src="${resource(dir: "images", file: "jetairways.png")}">

                                    </g:elseif>
                                    <g:elseif
                                            test="${flightInstanceInbound.flightSegmentVO?.first()?.airLineName == "Air India"}">
                                        <img src="${resource(dir: "images", file: "AIRINDIA.png")}">

                                    </g:elseif>
                                    <g:elseif
                                            test="${flightInstanceInbound.flightSegmentVO?.first()?.airLineName == "SpiceJet"}">
                                        <img src="${resource(dir: "images", file: "SpiceJet.png")}">

                                    </g:elseif>
                                    <g:elseif
                                            test="${flightInstanceInbound.flightSegmentVO?.first()?.airLineName == "IndiGo"}">
                                        <img src="${resource(dir: "images", file: "IndiGo.png")}">
                                    </g:elseif>
                                </span>
                            </td>
                            <td class="col-dapart">

                                <span class="">${(flightInstanceInbound?.flightSegmentVO?.first()?.departureTime?.tokenize("T")?.last())}</span>
                            </td>
                            <td class="col-arrive">

                                <span class="">${(flightInstanceInbound?.flightSegmentVO?.last()?.arrivalTime?.tokenize("T")?.last())}</span>
                            </td>
                            <td class="col-duration">
                                <g:set var="durationOfFlight"
                                       value="${flightInstanceInbound?.obDuration}"/>
                                <span>${durationOfFlight}</span>
                                <small class="text-muted"><g:if
                                        test="${flightInstanceInbound?.flightSegmentVO?.size() > 1}">
                                    ${flightInstanceInbound?.flightSegmentVO?.size() - 1}
                                </g:if>
                                <g:else>
                                    0
                                </g:else> Stop</small>
                            </td>
                            <td class="col-price">
                                <div class="price">
                                    <span class="color-default">Rs.</span><span class="color-default"
                                                                                id="fareInboundId${j}">${flightInstanceInbound?.flightFare?.offeredFare}</span>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </label>
            </div>
        </li>
        %{--<util:remoteNonStopPageScroll action='flightSearchResultPaginated' controller="tboFlightBooking"--}%
                                      %{--total="${total}"--}%
                                      %{--update="updateInboundPaginateList"/>--}%
    </g:if>
</g:each>
<script>
    $(document).ready(function () {
        $('#arriveId0').prop('checked', true);
        $('#alreadySelected0').addClass('selected');
        var airLineName = '${flightSearchVOListPaginatedInbound.first().flightSegmentVO?.first()?.airLineName}';
        var originAC = '${flightSearchVOListPaginatedInbound.first().flightSegmentVO?.first()?.originAirPortCode}';
        var flightNumber = '${flightSearchVOListPaginatedInbound.first().flightSegmentVO?.first()?.flightNumber}';
        var destAC = '${flightSearchVOListPaginatedInbound.first().flightSegmentVO?.first()?.destinationAirPortCode}';
        var deptTime = '${flightSearchVOListPaginatedInbound.first().flightSegmentVO?.first()?.departureTime?.tokenize("T")?.last()}';
        var arrTime = '${flightSearchVOListPaginatedInbound.first().flightSegmentVO?.first()?.arrivalTime?.tokenize("T")?.last()}';
        $('#flightCodeId2').html(flightNumber);
        $('#flightNameId2').html(airLineName);
        $('#flightRouteId2').html(originAC + ' to ' + destAC);
        $('#flightTimeId2').html(deptTime + ' - ' + arrTime);
        $('#inBoundFlightId').val('${flightSearchVOListPaginatedInbound?.first()}');
    });
    function setCurrentSelectionForInbound(airlineName, originAirport, destAirport, flightNumber, flightTimeDept, flightTimeArr, j, flightInstance) {
        var index = j;
        var ibId = "#fareInboundId" + index;
        var obRadio = $("input[name='depart']:checked").attr('id');
        $('#flightCodeId2').html(flightNumber);
        $('#flightNameId2').html(airlineName);
        $('#flightRouteId2').html(originAirport + ' to ' + destAirport);
        $('#flightTimeId2').html(flightTimeDept + ' - ' + flightTimeArr);
        var inBoundPrice = $(ibId).text();
        var extractChar = obRadio.slice(-1);
        var outBoundPrice = $("#fareOutboundId" + extractChar).text();
        var total = parseInt(inBoundPrice) + parseInt(outBoundPrice);
        $('#priceId').html(total);
        $('#inBoundFlightId').val(flightInstance);
    }

</script>
%{--<ul class="pull-left pagination">--}%

%{--<g:paginate controller="tboFlightBooking" action="flightSearchResultPaginated"--}%
%{--total="${totalInbound}"--}%
%{--params="${params}"/>--}%

%{--</ul>--}%
%{--<style>--}%
%{--.pagination {--}%
%{--margin: 3px 0px 3px 0px;--}%
%{--}--}%

%{--.pagination a {--}%
%{--padding: 2px 4px 2px 4px;--}%
%{--background-color: #A4A4A4;--}%
%{--border: 1px solid #EEEEEE;--}%
%{--text-decoration: none;--}%
%{--font-size: 10pt;--}%
%{--font-variant: small-caps;--}%
%{--color: #EEEEEE;--}%
%{--}--}%

%{--.pagination a:hover {--}%
%{--text-decoration: underline;--}%
%{--background-color: #888888;--}%
%{--border: 1px solid #AA4444;--}%
%{--color: #FFFFFF;--}%
%{--}--}%

%{--</style>--}%
