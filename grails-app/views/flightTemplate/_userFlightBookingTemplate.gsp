<%@ page import="com.travelex.booking.HotelForBooking" %>

<div class="modal fade bs-example-modal-lg" id="flightDetatil" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span
                        aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <img class="panel-title text-left"
                     src="${resource(dir: '/images/appimages', file: 'appLogo.png')}"
                     height="30">
            </div>

            <div class="modal-body" id="flightInvoiceTemplateId">
            </div>
        </div>
    </div>
</div>

<table class="table table-bordered table-stripped">
    <tr>
        <th>Flight PNR</th>
        <th>Booking id</th>
        <th>Reference id</th>
        <th>Date created</th>
        <th>Last updated</th>
        <th>Action</th>
    </tr>

    <g:each in="${bookedFlightList}" var="flightBooked">

        <tr>
            <td>${flightBooked?.pnr}</td>

            <td>${flightBooked?.bookingId}</td>

            <td>${flightBooked?.refId}</td>

            <td>${flightBooked?.dateCreated?.format('dd/MM/yyyy')}</td>
            <td>${flightBooked?.lastUpdated?.format('dd/MM/yyyy')}</td>
            <td>

                <button type="button" class="btn btn-xs btn-primary" data-target=".bs-example-modal-lg"
                        data-toggle="modal" onclick="findBookedFlightDetailFromTBO(${flightBooked?.id})"
                        data-target="#flightDetatil">Booking details</button>

                <button type="button" class="btn btn-xs btn-primary"
                        data-toggle="modal" onclick="cancelBookedFlightDetailFromTBO(${flightBooked?.id})"
                        >Cancel details</button>
            </td>
        </tr>
    </g:each>
</table>
<script>
    function findBookedFlightDetailFromTBO(flightBookId) {
        $.ajax({
            url: '${createLink(controller: 'home',action: 'flightDetails')}',
            data: {
                flightBookId: flightBookId
            },
            success: function (data) {
                $("#flightInvoiceTemplateId").html(data)
            }
        });
    }
    function cancelBookedFlightDetailFromTBO(flightBookId) {
        $.ajax({
            url: '${createLink(controller: 'home',action: 'cancelFlightBooking')}',
            data: {
                flightBookId: flightBookId
            },
        });
    }
</script>