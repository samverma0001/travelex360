<g:each in="${flightSearchVOList}" var="flightInstance" status="index">
    <g:if test="${flightInstance.tripIndicator == '1'}">
        <li>
            <g:set var="flightTimeDept"
                   value="${(flightInstance?.flightSegmentVO?.first()?.departureTime?.tokenize("T")?.last())}"/>
            <g:set var="flightTimeArr"
                   value="${(flightInstance?.flightSegmentVO?.last()?.arrivalTime?.tokenize("T")?.last())}"/>
            <div class="listItem" id="alreadySelected${index}">
                <label class="">
                    <table class="table airline-listing-table">
                        <tbody>
                        <tr>
                            <th class="col-empty">
                                <input id="departId${index}" type="radio" name="depart" class="selectable-box"
                                       onclick="setCurrentSelectionForOutbound('${flightInstance.flightSegmentVO?.first()?.airLineName}', '${flightInstance?.flightSegmentVO?.first()?.originAirPortCode}', '${flightInstance?.flightSegmentVO?.first()?.destinationAirPortCode}', '${flightInstance?.flightSegmentVO?.first()?.flightNumber}', '${flightTimeDept}', '${flightTimeArr}', '${index}', '${flightInstance}')">
                            </th>
                            <td class="col-airline">
                                <span>
                                    <g:if test="${flightInstance.flightSegmentVO?.first()?.airLineName == "JetLite"}">
                                        <img src="${resource(dir: "/images/flightImages", file: "jetlite1.jpg")}"
                                             height="30"
                                             width="60"
                                             class="img-rounded">

                                    </g:if>
                                    <g:elseif
                                            test="${flightInstance.flightSegmentVO?.first()?.airLineName == "Jet Airways"}">
                                        <img src="${resource(dir: "images", file: "jetairways.png")}">

                                    </g:elseif>
                                    <g:elseif
                                            test="${flightInstance.flightSegmentVO?.first()?.airLineName == "Air India"}">
                                        <img src="${resource(dir: "images", file: "AIRINDIA.png")}">

                                    </g:elseif>
                                    <g:elseif
                                            test="${flightInstance.flightSegmentVO?.first()?.airLineName == "SpiceJet"}">
                                        <img src="${resource(dir: "images", file: "SpiceJet.png")}">

                                    </g:elseif>
                                    <g:elseif
                                            test="${flightInstance.flightSegmentVO?.first()?.airLineName == "IndiGo"}">
                                        <img src="${resource(dir: "images", file: "IndiGo.png")}">
                                    </g:elseif>
                                </span>
                            </td>
                            <td class="col-dapart">

                                <span class="">${(flightInstance?.flightSegmentVO?.first()?.departureTime?.tokenize("T")?.last())}</span>
                            </td>
                            <td class="col-arrive">

                                <span class="">${(flightInstance?.flightSegmentVO?.last()?.arrivalTime?.tokenize("T")?.last())}</span>
                            </td>
                            <td class="col-duration">
                                <g:set var="durationOfFlight"
                                       value="${flightInstance?.obDuration}"/>
                                <span>${durationOfFlight}</span>
                                <small class="text-muted"><g:if
                                        test="${flightInstance?.flightSegmentVO?.size() > 1}">
                                    ${flightInstance?.flightSegmentVO?.size() - 1}
                                </g:if>
                                <g:else>
                                    0
                                </g:else> Stop</small>
                            </td>
                            <td class="col-price">
                                <div class="price">
                                    <span class="color-default">Rs.</span><span class="color-default"
                                                                                id="fareOutboundId${index}">${flightInstance.flightFare.offeredFare}</span>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </label>
            </div>
        </li>
        %{--<util:remoteNonStopPageScroll id="outBoundID" action='flightSearchResultPaginated' controller="tboFlightBooking"--}%
                                      %{--total="${total}"--}%
                                      %{--update="updateOutboundPaginateList" />--}%
    </g:if>
</g:each>

<script>

    $(document).ready(function () {
        $('#departId0').prop('checked', true);
        $('#alreadySelected0').addClass('selected');
        var airLineName = '${flightSearchVOList.first().flightSegmentVO?.first()?.airLineName}';
        var originAC = '${flightSearchVOList.first().flightSegmentVO?.first()?.originAirPortCode}';
        var flightNumber = '${flightSearchVOList.first().flightSegmentVO?.first()?.flightNumber}';
        var destAC = '${flightSearchVOList.first().flightSegmentVO?.first()?.destinationAirPortCode}';
        var deptTime = '${flightSearchVOList.first().flightSegmentVO?.first()?.departureTime?.tokenize("T")?.last()}';
        var arrTime = '${flightSearchVOList.first().flightSegmentVO?.first()?.arrivalTime?.tokenize("T")?.last()}';
        $('#flightCodeId1').html(flightNumber);
        $('#flightNameId1').html(airLineName);
        $('#flightRouteId1').html(originAC + ' to ' + destAC);
        $('#flightTimeId1').html(deptTime + ' - ' + arrTime);
        var inboundFare = $('#fareInboundId0').text();
        var total = parseInt('${flightSearchVOList.first().flightFare.offeredFare}') + parseInt(inboundFare);
        $('#priceId').html(total);
        $('#outBoundFlightId').val('${flightSearchVOList?.first()}');
    });

    function setCurrentSelectionForOutbound(airlineName, originAirport, destAirport, flightNumber, flightTimeDept, flightTimeArr, index, flightInstance) {
        var index = index;
        var obId = "#fareOutboundId" + index;
        var ibRadio = $("input[name='arrive']:checked").attr('id');
        $('#flightCodeId1').html(flightNumber);
        $('#flightNameId1').html(airlineName);
        $('#flightRouteId1').html(originAirport + ' to ' + destAirport);
        $('#flightTimeId1').html(flightTimeDept + ' - ' + flightTimeArr);
        var outBoundPrice = $(obId).text();
        var extractChar = ibRadio.slice(-1);
        var inBoundPrice = $("#fareInboundId" + extractChar).text();
        var total = parseInt(outBoundPrice) + parseInt(inBoundPrice);
        $('#priceId').html(total);
        $('#outBoundFlightId').val(flightInstance);
    }
</script>








%{--<ul class="pull-right pagination">--}%

%{--<g:paginate controller="tboFlightBooking" action="flightSearchResultPaginated"--}%
%{--total="${total}"--}%
%{--params="${params}"/>--}%

%{--</ul>--}%
%{--<style>--}%
%{--.pagination {--}%
%{--margin: 3px 0px 3px 0px;--}%
%{--}--}%

%{--.pagination a {--}%
%{--padding: 2px 4px 2px 4px;--}%
%{--background-color: #A4A4A4;--}%
%{--border: 1px solid #EEEEEE;--}%
%{--text-decoration: none;--}%
%{--font-size: 10pt;--}%
%{--font-variant: small-caps;--}%
%{--color: #EEEEEE;--}%
%{--}--}%

%{--.pagination a:hover {--}%
%{--text-decoration: underline;--}%
%{--background-color: #888888;--}%
%{--border: 1px solid #AA4444;--}%
%{--color: #FFFFFF;--}%
%{--}--}%

%{--</style>--}%