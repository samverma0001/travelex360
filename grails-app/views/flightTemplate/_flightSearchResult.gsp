<div class="col-md-12 col-sm-12 col-xs-12" xmlns="http://www.w3.org/1999/html">
    <div class="control list-group">
        <g:if test="${flightSearchVOList}">
            <g:each in="${flightSearchVOList}" status="i" var="flightInstance">
                <g:set var="flightSegment" value="${flightInstance.flightSegmentVO}"/>
                <div class="controls list-group-item">
                    <div class="row">
                        <div class="col-md-3">
                        %{--<g:each in="${hotelInstance?.searchHotelMedias}" var="media1">--}%
                            <g:if test="${flightInstance}">
                                <g:if test="${flightInstance.flightSegmentVO?.first()?.airLineName == "JetLite"}">
                                    <img src="${resource(dir: "/images/flightImages", file: "jetlite1.jpg")}"
                                         height="40"
                                         width="80"
                                         class="img-rounded">

                                </g:if>
                                <g:elseif
                                        test="${flightInstance.flightSegmentVO?.first()?.airLineName == "Jet Airways"}">
                                    <img src="${resource(dir: "/images/flightImages", file: "jetAir2.jpg")}"
                                         height="40"
                                         width="80"
                                         class="img-rounded">

                                </g:elseif>
                                <g:elseif
                                        test="${flightInstance.flightSegmentVO?.first()?.airLineName == "Air India"}">
                                    <img src="${resource(dir: "/images/flightImages", file: "airIndia.jpg")}"
                                         height="40"
                                         width="80"
                                         class="img-rounded">

                                </g:elseif>
                                <g:elseif
                                        test="${flightInstance.flightSegmentVO?.first()?.airLineName == "SpiceJet"}">
                                    <img src="${resource(dir: "/images/flightImages", file: "spiceJet.jpg")}"
                                         height="40"
                                         width="80"
                                         class="img-rounded">

                                </g:elseif>
                                <br>
                                ${flightInstance?.flightSegmentVO?.first()?.airLineName}&nbsp;&nbsp;
                                ${flightInstance?.flightSegmentVO?.first()?.airLineCode}-${flightInstance?.flightSegmentVO?.first()?.flightNumber}
                            </g:if>

                        %{--</g:each>--}%
                        </div>


                        <div class="col-md-2">
                            <g:set var="flightTimeDes"
                                   value="${(flightInstance?.flightSegmentVO?.first()?.arrivalTime?.tokenize("T")?.last())}"/>
                            <g:set var="flightTimeOri"
                                   value="${(flightInstance?.flightSegmentVO?.first()?.departureTime?.tokenize("T")?.last())}"/>
                            <div class="row">
                                ${flightInstance.origin}&nbsp;(${flightTimeOri.substring(0, flightTimeOri.lastIndexOf(":"))})
                            </div>

                            <div class="row">
                                ${flightInstance.destination}&nbsp;(${flightTimeDes.substring(0, flightTimeDes.lastIndexOf(":"))})
                            </div>
                            %{--<a href="${createLink(controller: 'tboFlightBooking', action: 'daa')}">--}%
                            %{--<button type="button"--}%
                            %{--class="btn btn-xs btn-default"--}%
                            %{--data-toggle="modal"--}%
                            %{--data-target="#flightDetailModal"--}%
                            %{--onclick="showFlightFairRule('${flightInstance}')">--}%
                            %{--Show details--}%
                            %{--</button>--}%
                            %{--</a>--}%
                            <div class="row">
                                <a style="cursor: pointer"
                                   onclick="flightItineary('${i}', '${flightInstance}', 'Itinerary')">Flight Itinerary</a>
                            </div>

                        </div>

                        <div class="col-md-2">
                            <g:set var="durationOfFlight"
                                   value="${flightInstance?.obDuration}"/>
                            <div class="row">
                                Duration:-${durationOfFlight}
                            </div>

                            <div class="row">
                                <g:if test="${flightInstance?.flightSegmentVO?.size() > 1}">
                                    Stops : ${flightInstance?.flightSegmentVO?.size() - 1}
                                </g:if>
                                <g:else>
                                    Stops : 0
                                </g:else>
                            </div>

                            <div class="row">
                                <a style="cursor: pointer"
                                   onclick="showFlightFairRule('${i}', '${flightInstance}', 'Fare Rule')">Fare Rule</a>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="row">
                                Publish Price :&nbsp;<strong style="color: brown">Rs
                                ${flightInstance.flightFare.publishedPrice}</strong>
                            </div>

                            <div class="row">
                                Offer price :&nbsp;<strong style="color: brown">Rs
                                ${flightInstance.flightFare.offeredFare}</strong>

                            </div>

                            <div class="row">
                                <a style="cursor: pointer"
                                   onclick="showFlightSSR('${i}', '${flightInstance}', 'Baggage')">Baggage Info</a>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <g:if test="${flightInstance}">
                            %{--<g:link class="btn btn-primary"--}%
                            %{--controller="test"--}%
                            %{--action="bookFlight"--}%
                            %{--params="[adult:adult,child:child,infant:infant,flightSessionId: flightInstance.sessionId, flightCode: flightInstance.flightSegmentVO?.first()?.flightNumber]"--}%
                            %{--style="text-decoration: none;">Book Now--}%
                            %{--</g:link>--}%
                            %{--<g:link class="btn btn-danger"--}%
                            %{--controller="tboFlightBooking"--}%
                            %{--action="addOccupantForFlight"--}%
                            %{--params="[adult: flightSearchVo?.adult, child: flightSearchVo?.children, infant: flightSearchVo?.infant, flightSessionId: flightInstance.sessionId, flightCode: flightInstance.flightSegmentVO?.first()?.flightNumber]"--}%
                            %{--style="text-decoration: none;">Book Now--}%
                            %{--</g:link>--}%
                                <div class="row" style="padding: 5px">
                                    <g:link class="btn btn-xs btn-danger"
                                            controller="tboFlightBooking"
                                            action="addOccupantForFlight"
                                            params="[flightInstanceId: flightInstance]"
                                            style="text-decoration: none;">Book Now
                                    </g:link>
                                </div>
                            </g:if>
                        </div>
                    </div>

                    <div id="itineraryModal${i}" class="row">
                    </div>

                    <button id="itineraryDataModal${i}" class="btn btn-link"
                            style="display: none"
                            data-toggle="modal"
                            data-target=".bs-example-modal-lg${i}">Small modal</button>
                </div>
            </g:each>
        </g:if>

        <g:else>
            <h3>
                Flight Not Found
            </h3>
        </g:else>
    </div>
    <ul class="pull-right pagination">

        <g:paginate controller="tboFlightBooking" action="flightSearchResultPaginated"
                    total="${total}"
                    params="${params}"/>

    </ul>
</div>

%{--<div id="cancellationModal" class="modalClass">--}%
<div id="spinner" style="display: none;">
    <img src="${createLinkTo(dir: '/assets/img/loaders', file: 'ajax-loader.gif')}" alt="Loading..." width="16"
         height="16"/>
</div>
%{--</div>--}%
<style>
.pagination {
    margin: 3px 0px 3px 0px;
}

.pagination a {
    padding: 2px 4px 2px 4px;
    background-color: #A4A4A4;
    border: 1px solid #EEEEEE;
    text-decoration: none;
    font-size: 10pt;
    font-variant: small-caps;
    color: #EEEEEE;
}

.pagination a:hover {
    text-decoration: underline;
    background-color: #888888;
    border: 1px solid #AA4444;
    color: #FFFFFF;
}

</style>