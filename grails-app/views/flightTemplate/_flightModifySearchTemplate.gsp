<div class="min-search-container">
<div class="container-fluid">
<div class="horiz-form-container">
<g:form class="tvx-from tvx-horiz-form" data-ng-controller="book_flight_control" controller="tboFlightBooking"
        action="searchFlightsForCity2">
<div class="row">
<div class="col-md-8">
<div class="row">
    <div class="col-md-9">
        <div class="form-group">
            <label for="">Search Summary</label>

            <div class="search-summary">
                <p><span class="location-summary">${flightSearchVo?.flightFromDom} <small>
                    <g:if test="${flightSearchVo?.journeyReturnDate}">
                        <i class="glyphicon glyphicon-transfer"></i>
                    </g:if>
                    <g:else>
                        <i class="glyphicon glyphicon-arrow-right"></i>
                    </g:else>
                </small> ${flightSearchVo?.flightToDom}</span> <span class="text-muted">|</span>
                    <span class="date-summary">${flightSearchVo?.journeyDate}</span>
                </p>
            </div>
        </div>

        <div class="modify-search-container">
            <div class="form-group">
                <label for="">Choose where you prefer to go</label>

                <div class="tick-box">
                    <input id="radio1" class="tick-radio flight-single" type="radio" name="timeTable" checked="true"
                           value="OneWay">
                    <label for="radio1"><span><span></span></span>One Way</label>
                    <input id="radio2" class="tick-radio flight-double" type="radio" name="timeTable"
                           value="Return"><label for="radio2"><span><span></span></span>Round Trip</label>
                    <input id="radio3" class="tick-radio flight-multi" type="radio" name="timeTable"
                           value="MultiWay"><label for="radio3"><span><span></span></span>Multi City</label>
                </div>
            </div>


            <div class="single-trip-container">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="">Leaving From</label>
                            <input type="text" class="form-control" name="flightFromDom"
                                   value="${flightSearchVo?.flightFromDom}"
                                   placeholder="Select Origin" id="leavingFrom">
                        </div>

                        <div class="col-md-6">
                            <label for="">Going To</label>
                            <input type="text" class="form-control" value="${flightSearchVo?.flightToDom}"
                                   name="flightToDom"
                                   placeholder="Select Destination" id="goingTo">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <label for="">Departure</label>

                            <div class="input-group date">
                                <input type="text" name="departDate" class="form-control"
                                       value="${flightSearchVo?.journeyDate}">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-4 ">
                            <div class="return_box">
                                <label for="">Return</label>

                                <div class="input-group date ">
                                    <input type="text" class="form-control" name="returnDate"
                                           value="${flightSearchVo?.journeyReturnDate}">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="multi-trip-container">
                <div class="multicity-flight-container">
                    <div class="multicity-header">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Leaving From</label>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Going To</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                        <label for="">Departure</label>
                                    </div>

                                    <div class="col-md-2 col-sm-2 col-xs-2 text-center">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="multicity-leg ">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="fromCity1"
                                               placeholder="">
                                    </div>

                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="toCity1"
                                               placeholder="">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                        <div class="input-group date">
                                            <input type="text" class="form-control" name="depature1">
                                            <span class="input-group-addon"><i
                                                    class="glyphicon glyphicon-calendar"></i></span>
                                        </div>
                                    </div>

                                    <div class="col-md-2 col-sm-2 col-xs-2 text-center">
                                        <div class="delete-city"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="multicity-leg ">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="fromCity1"
                                               placeholder="">
                                    </div>

                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="toCity1"
                                               placeholder="">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                        <div class="input-group date">
                                            <input type="text" class="form-control" name="depature1">
                                            <span class="input-group-addon"><i
                                                    class="glyphicon glyphicon-calendar"></i></span>
                                        </div>
                                    </div>

                                    <div class="col-md-2 col-sm-2 col-xs-2 text-center">
                                        <div class="delete-city"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="multicity-add-container">
                        <a href="#" id="addCity" class="multicity-add btn btn-default"><i
                                class=" glyphicon glyphicon-plus"></i> Add City <small>(max 5)</small></a>
                        <a href="#" id="removeCity" class="multicity-add btn btn-default"><i
                                class=" glyphicon glyphicon-minus"></i> Remove City <small>(min 2)</small></a>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group">
            <a href="#" class="btn img-full btn-default modify-search-toggle"><i
                    class="glyphicon glyphicon-chevron-down"></i> Modify Search</a>
        </div>
    </div>

</div>
</div>

<div class="col-md-4">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Guests</label>

                <div class="room-select-toggle">
                    {{TotalFlightGuest()}} Guests <span class="glyphicon glyphicon-chevron-down"></span>
                </div>

                <div class="room-select-dropdown-container">
                    <div class="dropdown-rooms-container">
                        <div class="form-group">
                            <label for="">Number of Adults</label>

                            <div class="input-group col-md-12">
                                <input type="text" id="adult" value="${flightSearchVo?.adult}"
                                       ng-model="flight_adult" name="adult"
                                       ng-init="flight_adult='1'" class="form-control countspin-flight-adult">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="">Number of Childrens</label>

                            <div class="input-group col-md-12">
                                <input type="text" id="child" ng-model="flight_children"
                                       value="${flightSearchVo?.children}" name="children"
                                       ng-init="flight_children='0'" class="form-control countspin-flight-children">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="">Number of Infants</label>

                            <div class="input-group col-md-12">
                                <input type="text" id="infant" ng-model="flight_infant"
                                       value="${flightSearchVo?.infant}" name="infant"
                                       ng-init="flight_infant='0'" class="form-control countspin-flight-infant">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <input type="submit" class="btn img-full btn-default" value="Search Again">
            </div>
        </div>
    </div>
</div>
</div>
</g:form>
</div>
</div>
</div>