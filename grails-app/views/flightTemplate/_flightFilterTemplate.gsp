<div class="filter-sidebar">

    <h3>Filter Your Search
    <!-- <a href="#" class="btn btn-danger btn-small pull-right"><i class="glyphicon glyphicon-remove"></i></a> -->
    </h3>

    <div class="filter-box">
        <div class="filter-box-header">
            <h4>Stops</h4>
        </div>

        <div class="filter-box-content">
            <div class="">
                <div class="checkbox-group">
                    <label class="">
                        <input type="checkbox" class="flightNoOfStop" autocomplete="off"
                               onclick="findFlightsByFilter()" value="1"> 1<br>
                    </label>
                    <label class="">
                        <input type="checkbox" class="flightNoOfStop" autocomplete="off"
                               onclick="findFlightsByFilter()" value="2"> 2<br>
                    </label>
                    <label class="">
                        <input type="checkbox" class="flightNoOfStop" autocomplete="off"
                               onclick="findFlightsByFilter()" value="3"> 3<br>
                    </label>
                    <label class="">
                        <input type="checkbox" class="flightNoOfStop" autocomplete="off"
                               onclick="findFlightsByFilter()" value="4"> 4<br>
                    </label>
                    <label class="">
                        <input type="checkbox" class="flightNoOfStop" autocomplete="off"
                               onclick="findFlightsByFilter()" value="5"> 5<br>
                    </label>
                </div>
            </div>
        </div>
    </div>

    <div class="filter-box">
        <div class="filter-box-header">
            <h4>Airlines <small></small></h4>
        </div>

        <div class="filter-box-content">
            <div class="filter-list-box">
                <div class="filter-list">
                    <label><input type="checkbox" class="airlineFilter"
                                  onclick="findFlightsByFilter()"
                                  value="Multiple Carrier"> Multiple Carrier
                    </label>
                </div>

                <div class="filter-list">
                    <label><input type="checkbox" class="airlineFilter"
                                  onclick="findFlightsByFilter()"
                                  value="Air Asia"> Air Asia</label>
                </div>

                <div class="filter-list">
                    <label><input type="checkbox" class="airlineFilter"
                                  onclick="findFlightsByFilter()"
                                  value="Jet Airways"> Jet Airways</label>
                </div>

                <div class="filter-list">
                    <label><input type="checkbox" class="airlineFilter"
                                  onclick="findFlightsByFilter()"
                                  value="Air India"> Air India
                    </label>
                </div>

                <div class="filter-list">
                    <label><input type="checkbox" class="airlineFilter"
                                  onclick="findFlightsByFilter()"
                                  value="SpiceJet"> Spice Jet</label>
                </div>

                <div class="filter-list">
                    <label><input type="checkbox" class="airlineFilter"
                                  onclick="findFlightsByFilter()"
                                  value="IndiGo"> IndiGo</label>
                </div>
            </div>
        </div>
    </div>


    <div class="filter-box">
        <div class="filter-box-header">
            <h4>Budget Range <small>(per pax)</small></h4>
        </div>

        <div class="filter-box-content">
            <div class="filter-list-box">
                <div class="filter-list">
                    <label><input type="checkbox" onclick="findFlightsByFilter()"
                                  value="4000" class="budgetRangeFilter"> Less than Rs.4,000
                    </label>
                </div>

                <div class="filter-list">
                    <label><input type="checkbox" onclick="findFlightsByFilter()"
                                  value="8000" class="budgetRangeFilter"> Rs.4,001 - Rs.8,000
                    </label>
                </div>

                <div class="filter-list">
                    <label><input type="checkbox" onclick="findFlightsByFilter()"
                                  value="12000" class="budgetRangeFilter"> Rs.8,001 - Rs.12,000</label>
                </div>

                <div class="filter-list">
                    <label><input type="checkbox" onclick="findFlightsByFilter()"
                                  value="16000" class="budgetRangeFilter"> Rs.12,001 - Rs.16,000</label>
                </div>

                <div class="filter-list">
                    <label><input type="checkbox" onclick="findFlightsByFilter()"
                                  value="more" class="budgetRangeFilter"> More than Rs.16,000
                    </label>
                </div>
            </div>
            <h5>Custom Price Range</h5>
            <input id="flightPriceRangeFilter" type="text" class="" value="" data-slider-min="${minPriceRange}"
                   data-slider-max="${maxPriceRange}"
                   data-slider-step="5" data-slider-value="[${minPriceRange},${maxPriceRange}]"/> <br>
            <h5>
                <span>INR ${minPriceRange}</span>
                <span class="pull-right">INR ${maxPriceRange}</span>
            </h5>
        </div>
    </div>
</div>
