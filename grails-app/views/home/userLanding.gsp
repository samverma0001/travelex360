<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <style>
    body {
        background-image: url('../images/searchBkgnd.jpg');
    }
    </style>
</head>

<body>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>


<div class="container" style="margin-bottom: 20%">
    <div class="col-md-12">
        <div class="col-md-1"></div>

        <div class="col-md-10">
            <div class="row col-md-offset-1">
                <div class="btn">
                    <a href="${createLink(controller: 'home', action: 'userLanding')}"
                       class="btn  btn-lg btn-danger" style="width: 180px;height: 80px"><br>My Profile</a>
                </div>

                <div class="btn">
                    <a href="${createLink(controller: "home", action: "userHotelBookingSummary")}"
                       class="btn btn-lg btn-warning" style="width: 180px;height: 80px;"><br>My Hotel</a>
                </div>


                <div class="btn">
                    <a href="${createLink(controller: 'home', action: 'userFlightBookingSummary')}"
                       class="btn btn-lg btn-success" style="width: 180px;height: 80px"><br>My Flight</a>
                </div>

            </div>
            <br>

            <div class="row col-md-offset-1">
                <div class="btn">
                    <a href="javascript:void(0)"
                       class="btn btn-lg btn-danger"
                       style="width: 180px;height: 80px"><br>My Activities</a>
                </div>

                <div class="btn">
                    <a href="javascript:void(0)"
                       class="btn btn-lg btn-warning" style="width: 180px;height: 80px"><br>Complain Form
                    </a>
                </div>

                <div class="btn">
                    <a href="javascript:void(0)"
                       class="btn btn-lg btn-success"
                       style="width: 180px;height: 80px"><br>My Payments</a>
                </div>
            </div>
            <br>

        </div>

        <div class="col-md-1"></div>

    </div>
</div>

</body>
</html>