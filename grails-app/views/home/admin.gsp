<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><g:layoutTitle default="Travelex"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'bootstrap.min.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'App.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'custom.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'font-awesome.min.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'Login.css')}" type="text/css">
    <g:layoutHead/>
    <r:layoutResources/>
    <style type="text/css">

    body {
        padding: 0px;
        margin: 0px;
    }

    .footer {
        width: 100%;
        bottom: 0px;
    }

    .footer1 {
        height: 40px;
        position: relative;
        z-index: 1;
    }
    </style>

</head>

<body>
<br>
<br>
<br>
<br>
<br>
<nav class="navbar navbar-inverse navbar-fixed-top " role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">TravelX360</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">

            <li><a href="/">TRAVELX 360</a></li>

        </ul>
        <ul class="nav navbar-nav navbar-right">
            <sec:ifLoggedIn>

                <li><g:link controller="home" ><i class="fa fa-user"></i>&nbsp;&nbsp;Profile</g:link></li>
                <li><g:link controller="plan" action="index" ><i class="fa fa-calendar"></i>&nbsp;&nbsp;Calender</g:link></li>
                <li><g:link controller="logout"><i class="fa fa-sign-out"></i>&nbsp;&nbsp;Logout</g:link></li>
            </sec:ifLoggedIn>
            <sec:ifNotLoggedIn>
                <li><g:link controller="signIn" action="index">SignUp</g:link></li>
                <li><g:link controller="login" action="auth">Login</g:link></li>
            </sec:ifNotLoggedIn>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>
%{--<div class="container">--}%
<g:layoutBody/>
</div>

<div class="footer" role="contentinfo"></div>

<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
<g:javascript library="application"/>
<r:layoutResources/>


<footer id="footer navbar-fixed-bottom" style="background-color: #111111">
    <ul class="nav pull-right navbar-fixed-bottom">
        <li>
            Copyright &copy; 2013, TravelX360.
        </li>
    </ul>
</footer>
<script src="${resource(dir: 'assets/js', file: 'libs/jquery-ui-1.9.2.custom.min.js')}"></script>
<script src="${resource(dir: 'assets/js', file: 'libs/bootstrap.min.js')}"></script>
<script src="${resource(dir: 'assets/js', file: 'App.js')}"></script>
<script src="${resource(dir: 'assets/js', file: 'demos/dashboard.js')}"></script>

<style>
#content1 {
    background: #FFF;
    width: auto;
    min-height: 800px;
    z-index: 18;
    padding-bottom: 25px;
    margin-left: 20px;
    margin-right: 20px;
    position: relative;
    left: auto;
    top: auto;
}</style>
</body>

</html>