<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Welcome To Travelex</title>
</head>

<body>
<br>
<br>
<br>

<div id="content1">
    <div id="content-header">
        <h4>&nbsp;</h4>

        <h3>&nbsp;&nbsp;Activation email sent</h3>
    </div>

    <div id="content-container">

        <h1 style="text-align: center">Activation link has been sent to service provider's email link</h1>
        <g:link class="btn btn-sm btn-primary" controller="admin" action="index">Go to home</g:link>

        %{--</div>--}%
    </div>
</div>

%{--<style>--}%
%{--#content1 {--}%
%{--background: #FFF;--}%
%{--width: auto;--}%
%{--min-height: 800px;--}%
%{--z-index: 18;--}%
%{--padding-bottom: 25px;--}%
%{--margin-left: 20px;--}%
%{--margin-right: 20px;--}%
%{--position: relative;--}%
%{--left: auto;--}%
%{--top: auto;--}%
%{--}</style>--}%
</body>
</html>