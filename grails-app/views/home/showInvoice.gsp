<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
</head>

<body>
<br>
<br>
<br>
<br>
<br>

<div class="container">
    <div class="row">
        <a href="${createLink(controller: 'home', action: 'userHotelBookingSummary')}"
           class="pull-right btn btn-primary">Back</a>
    </div>
</div>


<div class="container">

    <g:render template="/hotelTemplates/invoiceTemplate"
              model="[paymentInfo: paymentInfo, bookingHistory: bookingHistory, roomBookedList: roomBookedList, hotelForBooking: hotelForBooking]"/>

</div>
</body>
</html>