<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
</head>

<body>
<br>
<br>
<br>
<br>
<br>
<br>
<div class="container" style="margin-bottom: 40%">
    <g:hasErrors bean="${resetPasswordCo}">
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span
                    aria-hidden="true">&times;
            </span><span class="sr-only">Close</span></button>

            <div id="errors" class="alert alert-error">
                <g:renderErrors bean="${resetPasswordCo}"></g:renderErrors>
            </div>
        </div>
    </g:hasErrors>
    <div class="col-md-12">
        <g:form controller="home" action="updateCurrentPassword">
            <div class="row" style="padding: 2%">
                <div class="col-md-5 ">
                    <label><strong>Current Password</strong></label>
                </div>

                <div class="col-md-7 input-group">
                    <input type="password" class="form-control" name="currentPassword"/>

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row" style="padding: 2%">
                <div class="col-md-5">
                    <label><strong>New Password</strong></label>
                </div>

                <div class="col-md-7 input-group">

                    <input type="password" class="form-control" name="newPassword"/>

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row" style="padding: 2%">
                <div class="col-md-5">
                    <label><strong>Confirm New Password</strong></label>
                </div>

                <div class="col-md-7 input-group">
                    <input type="password" class="form-control" name="confirmPassword"/>

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row" style="padding: 2%">
                <div class="col-md-5">
                </div>

                <div class="col-md-7">
                    <input type="submit" class="btn btn-primary" value="Update Password"/>
                </div>
            </div>

        </g:form>
    </div>
</div>
</body>
</html>