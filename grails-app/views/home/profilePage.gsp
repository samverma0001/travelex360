<%@ page import="com.travelex.booking.HotelForBooking" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Welcome To Travelex 360</title>
</head>

<body>
<br>
<br>
<br>
<br>
<br>

<travelx:renderAlert/>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">My Profile</h3>
                </div>

                <div class="panel-body">
                    <div class="row">

                        <div class="col-md-4 col-lg-4 " align="center">
                            <g:form controller="user" action="saveProfilePicture" enctype="multipart/form-data">

                                <div style="margin-left: 40px;margin-top: 20px" class="fileupload fileupload-new"
                                     data-provides="fileupload"><input type="hidden" value="" name="">

                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">

                                        <g:if test="${user?.pictureName}">
                                            <img src="${createLink(controller: "user", action: "showProfileImage", params: [userId: user?.id])}"
                                                 alt="Placeholder" class="img-circle">
                                        </g:if>
                                        <g:else>
                                            <img src="${resource(dir: "/images", file: "picNotAvailable.jpeg")}"
                                                 class="img-circle" alt="Placeholder">
                                        </g:else>
                                    </div>

                                    <div class="fileupload-preview fileupload-exists thumbnail"
                                         style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>

                                    <div>
                                        <span class="btn btn-default btn-file"><span
                                                class="fileupload-new">Select image</span><span
                                                class="fileupload-exists">Change</span>
                                            <input type="file" name="profilePicture">
                                        </span>
                                        <a href="#" class="btn btn-default fileupload-exists"
                                           data-dismiss="fileupload">Remove</a>
                                    </div>
                                </div>
                                <input type="submit" name="update" class="btn btn-success" value="update">
                            </g:form>
                        </div>

                        <div class=" col-md-8 col-lg-8 ">
                            <table class="table table-user-information">
                                <tbody>
                                <tr>
                                    <td>First Name:</td>
                                    <td>${user?.firstName}</td>
                                </tr>
                                <tr>
                                    <td>Last Name:</td>
                                    <td>${user?.lastName}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td><a href="mailTo:">${user?.username}</a></td>
                                </tr>

                                <tr>
                                <tr>
                                    <td>Contact Info</td>
                                    <td>${user?.mobNo}</td>
                                </tr>
                                </tbody>
                            </table>

                            <a href="${createLink(controller: 'home', action: 'requestToChangePassword')}"
                               class="btn btn-primary">Change Password</a>

                            <a class="btn btn-primary" data-toggle="modal" data-target="#contact"
                               data-original-title>Edit Profile</a>

                            <g:link controller="plan" action="index" class="disabled btn btn-xs btn-primary pull-right"
                                    params="[userId: user?.id]">Calendar</g:link>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="contact" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="panel-title" id="contactLabel"><span
                        class="glyphicon glyphicon-info-sign"></span> Feel free to update...</h4>
            </div>
            <br>
            <g:hasErrors bean="${updateUserCO}">
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span
                            aria-hidden="true">&times;
                    </span><span class="sr-only">Close</span></button>

                    <div id="errors" class="alert alert-error">
                        <g:renderErrors bean="${updateUserCO}"></g:renderErrors>
                    </div>
                </div>
            </g:hasErrors>
            <g:form action="addProfile" controller="home" id="addProfile" accept-charset="utf-8">
                <div class="modal-body" style="padding: 5px;">
                    <div class="row">
                        <div class="input-group col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 10px;">
                            <input class="form-control" name="firstName" placeholder="Firstname" type="text"
                                   value="${user?.firstName}" autofocus/>

                            <div class="input-group-addon">
                                <travelx:star/>
                            </div>
                        </div>

                        <div class=" input-group col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 10px;">
                            <input class="form-control" value="${user?.lastName}" name="lastName" placeholder="Lastname"
                                   type="text"/>

                            <div class="input-group-addon">
                                <travelx:star/>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="input-group col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                            <input class="form-control" readonly value="${user?.username}" name="email"
                                   placeholder="E-mail"
                                   type="text"/>

                            <div class="input-group-addon">
                                <travelx:star/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-group col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                            <input class="form-control" value="${user?.mobNo}" name="mobNo" placeholder="Contact Number"
                                   type="text"/>

                            <div class="input-group-addon">
                                <travelx:star/>
                            </div>
                        </div>
                    </div>
                </div>
                <g:hiddenField name="notValid" value="notValid"/>
                <div class="panel-footer" style="margin-bottom:-14px;">
                    <input type="submit" class="btn btn-success" value="Save"/>
                    <button type="button" class="btn btn-danger btn-close"
                            data-dismiss="modal">Close</button>
                </div>
            </g:form>
        </div>
    </div>
</div>

%{--<script type="text/javascript">--}%
%{--function validateAddProfileForm() {--}%
%{--alert('1111111111111111')--}%
%{--$("#addProfile").validate({--}%
%{--rules: {--}%
%{--firstName: {--}%
%{--required: true--}%
%{--},--}%
%{--lastName: {--}%
%{--required: true--}%
%{--}--}%
%{--},--}%
%{--messages: {--}%
%{--firstName: {--}%
%{--required: "First Name is required"--}%
%{--},--}%
%{--lastName: {--}%
%{--required: "Last Name is required"--}%
%{--}--}%
%{--},--}%
%{--submitHandler: function (form) {--}%
%{--$("#addProfile").submit();--}%
%{--}--}%
%{--})--}%
%{--}--}%

%{--$.validator.setDefaults({--}%
%{--errorClass: 'custom_error',--}%
%{--errorElement: 'label'--}%
%{--});--}%
%{--</script>--}%


<script>
    $(document).ready(function () {
        if ('${notValid}') {
            $("#contact").modal('show');
        }
        else {
        }
    });
</script>

</body>
</html>