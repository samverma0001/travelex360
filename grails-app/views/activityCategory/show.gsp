<%@ page import="activity.ActivityCategory" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin"/>
</head>

<body>
<travelx:adminUserTab/>
<travelx:renderAlert/>
<div id="show-activityCategory" class="container content scaffold-show" role="main">
    <h3 style="margin-left: 5%">Show activity category</h3>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table class="table table-bordered table-condensed table-condensed">
        <thead>
        <tr>
            <th>Name</th>
            <th>Parent category</th>
            <th>Child category</th>
            <th>Date created</th>
            <th>Last updated</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>${activityCategoryInstance.name}</td>
            <td>${activityCategoryInstance?.parentCategory?.name}</td>
            <td>${activityCategoryInstance?.childCategory?.name}</td>
            <td>${activityCategoryInstance.dateCreated.format("MM-dd-yyyy")}</td>
            <td>${activityCategoryInstance.lastUpdated.format("MM-dd-yyyy")}</td>
            <td><g:form url="[resource: activityCategoryInstance, action: 'delete']" method="DELETE">
                <fieldset class="buttons">
                    <g:link class="edit btn btn-xs btn-primary" action="edit"
                            resource="${activityCategoryInstance}"><g:message
                            code="default.button.edit.label" default="Edit"/></g:link>
                    <g:actionSubmit class="delete btn btn-xs btn-danger" action="delete"
                                    value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                    onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                </fieldset>
            </g:form></td>
        </tr>
        </tbody>
    </table>

</div>
</body>
</html>
