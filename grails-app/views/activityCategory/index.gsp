<%@ page import="activity.ActivityCategory" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin"/>
</head>

<body>

<travelx:adminUserTab/>
<travelx:renderAlert/>
<div class="tab-content" style="margin-top: 1%">
    <div class="tab-pane active" id="holiday" style="margin-left: 5%">
        <ul class="nav nav-tabs">
            <li class="active"><a
                    href="${createLink(controller: 'holiday', action: 'listCategory')}">Activity Category</a></li>
        </ul>

        <div class="tab-content" style="margin-top: 1%">
            <div class="tab-pane active" id="editUser">
                <div class="col-md-12">
                    <div class="row">
                        <label>Activity List:-</label>
                        <a href="${createLink(controller: 'activityCategory', action: 'create')}"
                           class="btn btn-primary pull-right">Create</a>&nbsp;&nbsp;&nbsp;
                    </div>
                    <br>
                    <table class="table table-bordered table-condensed">
                        <tr>
                            <th class="text-center">Name</th>
                            <th class="text-center">Date created</th>
                            <th class="text-center">Last updated</th>
                            <th class="text-center">Parent category</th>
                            <th class="text-center">Child category</th>
                            <th class="text-center">Action</th>
                        </tr>
                        <g:each in="${activityCategoryInstanceList}" var="activityCategoryInstance">
                            <tr class="text-center">
                                <td><g:link action="show"
                                            id="${activityCategoryInstance.id}">${fieldValue(bean: activityCategoryInstance, field: "name")}</g:link></td>

                                <td><g:formatDate date="${activityCategoryInstance.dateCreated}"/></td>

                                <td><g:formatDate date="${activityCategoryInstance.lastUpdated}"/></td>

                                <td>${activityCategoryInstance?.parentCategory?.name}</td>
                                <td>${activityCategoryInstance?.childCategory?.name}</td>
                                <th><a href="${createLink(controller: 'activityCategory', action: 'edit', params: [activityCategoryInstanceId: activityCategoryInstance?.id])}"
                                       class="btn btn-xs btn-primary">Edit</a>
                                    <a href="${createLink(controller: 'activityCategory', action: 'delete', params: [activityCategoryInstanceId: activityCategoryInstance?.id])}"
                                       class="btn btn-xs btn-danger">Delete</a>
                                </th>
                            </tr>
                        </g:each>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
