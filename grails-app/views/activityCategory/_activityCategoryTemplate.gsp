<g:select id="childCategory" name="childCategory.id" from="${activityCategoryList}"
                          optionKey="id" optionValue="name" noSelection="['': 'Select child category']"
                          class="form-control many-to-one"/>