<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin"/>
</head>

<body>
<travelx:adminUserTab/>
<travelx:renderAlert/>

<div class="container-fluid">
    <h3 style="margin-left: 5%">Create activity category</h3>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:form action="save">
        <fieldset class="form">
            <g:render template="form"
                      model="[activityCategoryList: activityCategoryList, activityCategoryInstance: activityCategoryInstance]"/>
        </fieldset>
        <fieldset class="buttons">
            <g:submitButton name="create" class="col-md-offset-3 save btn btn-primary"
                            value="${message(code: 'default.button.create.label', default: 'Create')}"/>
        </fieldset>
    </g:form>
</div>
</body>
</html>
