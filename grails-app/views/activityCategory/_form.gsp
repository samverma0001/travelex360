<%@ page import="activity.ActivityCategory" %>

<div class="container">
    <div class="row" style="padding: 2%">
        <div class="col-md-6">
            <div class="col-md-4">
                <label for="name">
                    <g:message code="activityCategory.name.label" default="Name"/>
                    <span class="required-indicator">*</span>
                </label>
            </div>

            <div class="col-md-8">
                <g:textField required="" class="form-control" name="name" value="${activityCategoryInstance?.name}"/>
            </div>
        </div>
    </div>

    <div class="row" style="padding: 2%">
        <div class="col-md-6">
            <div class="col-md-4">
                <label for="parentCategory">
                    <g:message code="activityCategory.parentCategory.label" default="Parent Category"/>
                </label>
            </div>

            <div class="col-md-8">
                <g:select id="parentCategory" name="parentCategory.id" from="${activityCategoryList}"
                          optionKey="id" optionValue="name" noSelection="['': 'Select parent category']"
                          class="form-control many-to-one" onchange="findParentActivityCategory(this.value)"/>
            </div>
        </div>
    </div>

    <div id="childCat" class="row" style="padding: 2%">
        <div class="col-md-6">
            <div class="col-md-4">
                <label for="childCategory">
                    <g:message code="activityCategory.childCategory.label" default="Child Category"/>
                </label>
            </div>

            <div class="col-md-8" id="childCategory">
                <g:render template="activityCategoryTemplate"/>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#childCat").hide();
    });


    function findParentActivityCategory(id) {
        alert(id);
        if (id) {
            $("#childCat").show();
        }
        else {
            $("#childCat").hide();
        }
        $.ajax({
            url: "${createLink(controller: "activityCategory" , action: "findParentCategory",absolute: true)}",
            type: "POST",
            data: {parentCategoryId: id},
            success: function (data) {
                $('#childCategory').html(data);
            }
        });


    }
</script>
