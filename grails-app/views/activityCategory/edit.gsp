<%@ page import="activity.ActivityCategory" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin"/>
</head>

<body>

<travelx:adminUserTab/>
<travelx:renderAlert/>

<div class="container-fluid">
    <h3 style="margin-left: 5%">Edit activity category</h3>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${activityCategoryInstance}">
        <ul class="errors" role="alert">
            <g:eachError bean="${activityCategoryInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form url="[resource: activityCategoryInstance, action: 'update']" method="PUT">
        <g:hiddenField name="version" value="${activityCategoryInstance?.version}"/>
        <fieldset class="form">
            <g:render template="form"
                      model="[activityCategoryList: activityCategoryList, activityCategoryInstance: activityCategoryInstance]"/>
        </fieldset>
        <fieldset class="buttons">
            <g:actionSubmit class="save btn btn-primary col-md-offset-3" action="update"
                            value="${message(code: 'default.button.update.label', default: 'Update')}"/>
        </fieldset>
    </g:form>
</div>
</body>
</html>
