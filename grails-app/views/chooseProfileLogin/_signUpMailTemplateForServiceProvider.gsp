<h3>Your user Id is your email Id and password is ${pass}</h3>
<br>
<h3>Please click the following link to activate your account</h3>

<g:link controller="admin" action="activateServiceProviderAccount" absolute="true"
        params="[userToken: userToken]">Activate your account</g:link>
