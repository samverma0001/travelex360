Hi ${name},
<br>
<strong>Congrats !</strong> You have successfully registered with travelex360.
<br>

<h3>Please click the following link to activate your account</h3>

<g:link controller="signIn" action="activateAccount" absolute="true"
        params="[userToken: userToken]">Activate your account</g:link>
