<%@ page import="serviceProvider.ServiceProvider" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name='layout' content='main'/>
    <title>Choose Profile to log In</title>
</head>

<body>

<div id="login-container">

    <div id="logo">
        <a href="${createLink(controller: "signIn", action: "index")}">
            <img src="${resource(dir: "assets/img/logos", file: "logo-login.png")}" alt="Logo"/>
        </a>
    </div>

    <div id="login">

        <h3>Request for hotel registration</h3>

        <h5>Your account will be active after Admin approval</h5>

        <br/>


        <br/>

        <span class="text-muted">FILL UP FORM BELOW</span>
        <g:hasErrors bean="${signUpCO}">
            <ul class="errors alert alert-error" role="alert">
                <g:eachError bean="${signUpCO}" var="error">
                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                            error="${error}"/></li>
                </g:eachError>
            </ul>
        </g:hasErrors>
        <form id="login-form" action="${createLink(controller: "signIn", action: "signUpForServiceProvider")}"
              class="form">

            <div class="form-group ">
                <label class="col-md-2">Name of Hotel</label>
                <input class="form-control" type="text" name="nameOfHotel" placeholder="Name of hotel">
            </div>

            <div class="form-group">
                <label class="col-md-2">City</label>
                <input class="form-control" type="text" name="city" placeholder="City">
            </div>

            <div class="form-group">
                <label class="col-md-2">State</label>
                <input class="form-control" type="text" name="state" placeholder="State">
            </div>

            <div class="form-group">
                <label class="col-md-2">Address</label>
                <input class="form-control" type="text" name="address" placeholder="Address">
            </div>

            <div class="form-group">
                <label class="col-md-2">Property Type</label>
                <input class="form-control" type="text" name="propertyType"
                       placeholder="Property type">
            </div>

            <div class="form-group">
                <label class="col-md-2">Name of Representee</label>
                <input class="form-control" type="text" name="nameOfRepresentee"
                       placeholder="Name of Representee">
            </div>

            <div class="form-group">
                <label class="col-md-2">Designation of Representee</label>
                <input class="form-control" type="text" name="designationOfRepresentee"
                       placeholder="Designation of Representee">
            </div>

            <div class="form-group">
                <label class="col-md-2">Official Email</label>
                <input class="form-control" type="email" name="officialEmail" id="officialEmail" onblur="checkEmail()" onclick="clearPlaceHolder()"
                       placeholder="Official Email">
                <label class="hideNotification" style="color: red;">Email already exists</label>
            </div>

            <div class="form-group">
                <label class="col-md-2">Website</label>
                <input class="form-control" type="text" name="website"
                       placeholder="Website">
            </div>

            <div class="form-group">
                <label class="col-md-2">Mobile Number</label>
                <input class="form-control" type="tel" name="mobNo"
                       placeholder="Mobile Number">
            </div>

            <div class="form-group">
                <button type="submit" id="login-btn" class="btn btn-primary btn-block">Sign Up &nbsp; <i
                        class="fa fa-play-circle"></i></button>
            </div>
        </form>

    </div> <!-- /#login -->

</div> <!-- /#login-container -->

<script src="${resource(dir: 'assets/js', file: 'libs/jquery-1.9.1.min.js')}"></script>
<script src="${resource(dir: 'assets/js', file: 'libs/jquery-ui-1.9.2.custom.min.js')}"></script>
<script src="${resource(dir: 'assets/js', file: 'libs/bootstrap.min.js')}"></script>

<script src="${resource(dir: 'assets/js', file: 'App.js')}"></script>
<script src="${resource(dir: 'assets/js', file: 'demos/Login.js')}"></script>

<script>

    $(document.ready(function(){
        $(".hideNotification").hide();
    }))
    function checkEmail() {
        var emailCheck = $("#officialEmail").val()
        $.ajax({
            type: "POST",
            url: "${createLink(controller: 'admin',action: 'checkExistingEmail')}",
            data: {emailCheck:emailCheck},
            success: function (response) {
                if (response == "true") {
                    $(".hideNotification").show();
                    $("#officialEmail").val("");
                    $("#officialEmail").attr("placeHolder",emailCheck);
                }  else{
                    $(".hideNotification").hide();
                }
            }
        });
    }
    function clearPlaceHolder(){
        $("#officialEmail").attr("placeHolder","");
    }
</script>
</body>
</html>
