<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name='layout' content='headerFooterLayoutWithMenu'/>
</head>

<body>
<div class="full-height login-page">
    <div class="inner-page-container full-height">
        <!-- <div class="middle-container"> -->
        <div class="container-fluid">
            <div class="register-container">
                <div class="row">
                    <div class="col-md-7">
                        <h1>Register on Travelex 360</h1>
                        <g:hasErrors bean="${registerCO}">
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert"><span
                                        aria-hidden="true">&times;
                                </span><span class="sr-only">Close</span></button>

                                <div id="errors" class="alert alert-error">
                                    <g:renderErrors bean="${registerCO}"/>
                                </div>
                            </div>
                        </g:hasErrors>
                        <travelx:renderAlert/>
                        <form class="login-form" action="${createLink(controller: 'signIn', action: 'signUp')}">
                            <h4>Personal Details</h4>

                            <div class="form-group">
                                <input type="text" value="${registerCO?.firstName}" class="form-control"
                                       name="firstName" placeholder="Your First name">
                            </div>

                            <div class="form-group">
                                <input type="text" value="${registerCO?.lastName}" class="form-control"
                                       name="lastName" placeholder="Your last name">
                            </div>

                            <div class="form-group">
                                <g:textField type="tel" class="form-control" name="captcha"
                                             placeholder="Type letter in box"/>
                            </div>

                            <div class="form-group">
                                <input value="${registerCO?.username}" type="email" class="form-control" name="username"
                                       placeholder="Your Email Id">
                            </div>

                            <div class="form-group">
                                <input type="tel" value="${registerCO?.mobNo}" class="form-control" name="mobNo"
                                       placeholder="Mobile Number">
                            </div>


                            <h4>Your Password</h4>

                            <div class="form-group">
                                <input type="password" class="form-control" name="password"
                                       placeholder="Enter Password">
                            </div>

                            <div class="form-group">
                                <input type="password" class="form-control" name="confirmPassword"
                                       placeholder="Confirm Password">
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"
                                           name="termsCondition"> I accept <a
                                        href="${createLink(controller: 'home', action: 'termsOfUse')}">Terms &amp; Conditions</a> &amp;  <a
                                        href="${createLink(controller: 'home', action: 'privacyPolicy')}">Privacy Policy</a>

                                </label>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-success img-full">Register Now</button>
                            </div>

                            <div class="form-group text-left">
                                <a href="${createLink(controller: 'login', action: 'auth')}">Already Having Account? Login Now!</a>
                            </div>
                        </form>
                    </div>

                    <div class="col-md-5">
                        <div class="other-sign-in text-center">
                            <a href="${createLink(controller: 'signIn', action: 'connectGoogle')}"><img
                                    src="../images/google-login.png" class="img-full"></a>
                            <a href="${createLink(controller: 'signIn', action: 'connectFacebook')}"><img
                                    src="../images/facebook-login.png" class="img-full"></a>
                            <a href="${createLink(controller: 'signIn', action: 'connectLinkedIn')}"><img
                                    src="../images/linkedIn-login.jpg" class="img-full"></a>

                            <img src="${createLink(controller: 'simpleCaptcha', action: 'captcha')}"/>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- </div> -->
    </div>
</div>
</body>
</html>
