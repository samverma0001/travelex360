<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name='layout' content='main'/>
    <title>Choose Profile to log In</title>
</head>

<body>


<div id="login-container">

    <div id="logo">
        <a href="${createLink(controller: "signIn", action: "index")}">
            <img src="${resource(dir: "assets/img/logos", file: "logo-login.png")}" alt="Logo"/>
        </a>
    </div>

    <div id="login">

        <h3>Request for hotel registration</h3>

        <h5>Your account will be active after Admin approval</h5>

        <br/>


        <br/>

        <span class="text-muted">FILL UP FORM BELOW</span>
        <g:hasErrors bean="${signUpCO}">
            <ul class="errors alert alert-error" role="alert">
                <g:eachError bean="${signUpCO}" var="error">
                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                            error="${error}"/></li>
                </g:eachError>
            </ul>
        </g:hasErrors>
        <form id="login-form" action="${createLink(controller: "signIn",action: "signUpForServiceProvider")}" class="form">

            <div class="form-group">
                <label class="col-md-2">First Name</label>
                <input class="form-control" type="text" name="firstName" placeholder="First Name">
            </div>

            <div class="form-group">
                <label class="col-md-2">Last Name</label>
                <input class="form-control" type="text" name="lastName" placeholder="Last Name">
            </div>

            <div class="form-group">
                <label class="col-md-2">Email</label>
                <input class="form-control" type="email" name="username" placeholder="Email">
            </div>

            <div class="form-group">
                <label class="col-md-2">Hotel Name</label>
                <input class="form-control" type="text" name="hotelName" placeholder="Hotel Name">
            </div>

            <div class="form-group">
                <label class="col-md-2">Hotel Location</label>
                <input class="form-control" type="text" name="hotelLocation"
                       placeholder="Hotel Location">
            </div>
            <div class="form-group">
                <label class="col-md-2">Mobile Number</label>
                <input class="form-control" type="tel" name="mobNo"
                       placeholder="Mobile Number">
            </div>
            <div class="form-group">
                <label class="col-md-2">Hotel short description</label>
                <textarea class="form-control" name="hotelDescription" style="height: 100px"
                          placeholder="Hotel short description"></textarea>
            </div>
            <div class="form-group">
                <label class="col-md-2">Hotel property type</label>
                <input class="form-control" type="text" name="propertyType"
                       placeholder="Hotel property type">
            </div>

            <div class="form-group">
                <button type="submit" id="login-btn" class="btn btn-primary btn-block">Sign Up &nbsp; <i
                        class="fa fa-play-circle"></i></button>
            </div>
        </form>

    </div> <!-- /#login -->

</div> <!-- /#login-container -->

<script src="${resource(dir: 'assets/js', file: 'libs/jquery-1.9.1.min.js')}"></script>
<script src="${resource(dir: 'assets/js', file: 'libs/jquery-ui-1.9.2.custom.min.js')}"></script>
<script src="${resource(dir: 'assets/js', file: 'libs/bootstrap.min.js')}"></script>

<script src="${resource(dir: 'assets/js', file: 'App.js')}"></script>
<script src="${resource(dir: 'assets/js', file: 'demos/Login.js')}"></script>

%{--<br>--}%
%{--<br>--}%
%{--<br>--}%
%{--<br>--}%
%{--<br>--}%

%{--<div class="container">--}%
%{--<div class="row">--}%
%{--<div class="col-md-5">--}%
%{--<g:form class="form-horizontal">--}%

%{--<div class="control-group">--}%
%{--<div class="controls">--}%

%{--<g:link controller="signIn" action="forgetPassword">forget password</g:link>--}%
%{--</div>--}%
%{--</div>--}%
%{--<div class="control-group">--}%
%{--<div class="controls">--}%

%{--<g:link controller="signIn" action="forgetPassword">forget password</g:link>--}%
%{--</div>--}%
%{--</div>--}%



%{--<div class="form-group">--}%
%{--<div class="controls">--}%

%{--<a href="${createLink(controller: 'signIn', action: 'connectGoogle')}"--}%
%{--class="btn btn-primary">Google Sign Up</a>--}%
%{--<a class="btn btn-primary  "--}%
%{--href="${createLink(controller: 'signIn', action: 'connectFacebook')}">--}%
%{--<i class="fa fa-google-plus-square"></i>--}%
%{--&nbsp;&nbsp;Google Sign Up--}%
%{--</a>--}%
%{--<a href="${createLink(controller: 'signIn', action: 'connectFacebook')}"--}%
%{--class="btn btn-primary">Facebook Sign Up</a>--}%
%{--<a class="btn btn-primary btn-facebook"--}%
%{--href="${createLink(controller: 'signIn', action: 'connectFacebook')}">--}%
%{--<i class="fa fa-facebook"></i>--}%
%{--&nbsp;&nbsp;Login with Facebook--}%
%{--</a>--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="form-group">--}%
%{--<div class="controls">--}%
%{--<label><h1 style="text-align: right" class="label">Normal Sign In</h1></label>--}%
%{--<a href="${createLink(controller: 'login', action: 'auth')}"--}%
%{--class="btn btn-info" style="text-align:right">Sign In</a>--}%
%{--<g:link controller="signIn" action="forgetPassword">forget password</g:link>--}%
%{--</div>--}%
%{--</div>--}%
%{--</g:form>--}%
%{--</div>--}%

%{--<form>--}%
%{--<table >--}%
%{--<tr>--}%
%{--<td>--}%
%{----}%
%{--<a href="${createLink(controller: 'signIn', action: 'connectGoogle')}"--}%
%{--class="btn btn-large btn-primary">Google Sign Up</a>--}%
%{--<a href="${createLink(controller: 'signIn', action: 'connectFacebook')}"--}%
%{--class="btn btn-large btn-primary">Facebook Sign Up</a>--}%
%{--</td>--}%
%{--</tr>--}%
%{--</table>--}%
%{--</form>--}%


%{--<div class="col-md-7" id="login" >--}%
%{--<g:hasErrors bean="${signUpCO}">--}%
%{--<ul class="errors alert alert-error" role="alert">--}%
%{--<g:eachError bean="${signUpCO}" var="error">--}%
%{--<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message--}%
%{--error="${error}"/></li>--}%
%{--</g:eachError>--}%
%{--</ul>--}%
%{--</g:hasErrors>--}%
%{--<g:form class="form-horizontal" role="form" controller="signIn" action="signUp">--}%
%{--<div class="form-group">--}%

%{--<label class="col-md-2">First Name</label>--}%

%{--<div class="col-md-7">--}%
%{--<input class="form-control" type="text" name="firstName" placeholder="First Name">--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="form-group">--}%
%{--<label class="col-md-2">Last Name</label>--}%

%{--<div class="col-md-7">--}%
%{--<input class="form-control" type="text" name="lastName" placeholder="Last Name">--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="form-group">--}%
%{--<label class="col-md-2">Email</label>--}%

%{--<div class="col-md-7">--}%
%{--<input class="form-control" type="email" name="username" placeholder="Email">--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="form-group">--}%
%{--<label class="col-md-2">Password</label>--}%

%{--<div class="col-md-7">--}%
%{--<input class="form-control" type="password" name="password" placeholder="Password">--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="form-group">--}%
%{--<label class="col-md-2">Confirm Password</label>--}%

%{--<div class="col-md-7">--}%
%{--<input class="form-control" type="password" name="confirmPassword"--}%
%{--placeholder="Confirm Password">--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="form-group">--}%
%{--<div class="col-md-5">--}%
%{--<g:submitButton class="btn btn-primary" name="Sign Up"/>--}%
%{--</div>--}%
%{--</div>--}%

%{--</g:form>--}%

%{--</div>--}%
%{--</div>--}%
%{--</div>--}%
</body>
</html>
