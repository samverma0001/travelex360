<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name='layout' content='headerFooterLayout'/>
</head>

<body>
<div class="full-height login-page theme-green">
    <div class="inner-page-container full-height">
        <div class="container-fluid">
            <div class="login-container">
                <h1>Get your Password Now</h1>
                <travelx:renderAlert/>
                <g:form id="login-form" class="login-form form" method='POST' controller="signIn"
                        action="sendPasswordReset">
                    <div class="form-group">
                        <input type="email" class="form-control username-field" id=""
                               placeholder="Enter your Registered  Email ID " name="emailForReset">
                    </div>

                    <div class="form-group">
                        <button type="submit" id="login-btn"
                                class="btn btn-main btn-default img-full">Get Password Now</button>
                    </div>
                </g:form>
            </div>

        </div>
    </div>
</div>
</body>
</html>