
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name='layout' content='main'/>
    <title></title>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <g:form class="form-horizontal" controller="signIn" action="sendPasswordReset">
                <div class="control-group">
                    <div class="controls">
                        <h2>Enter new password</h2>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label" for="inputEmail">Email</label>
                    <div class="controls">
                        <input type="email" id="inputEmail" placeholder="Email">
                    </div>
                </div>

                 <div class="form-group">
                    <div class="controls">
                        <g:submitButton name="submit" value="Change Password"/>

                    </div>
                </div>

            </g:form>>
        </div>
    </div>
</div>

</body>
</html>