<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name='layout' content='main'/>
    <style>
    body {
        background-color: #525252;
    }

    .centered-form {
        margin-top: 60px;
    }

    .centered-form .panel {
        background: rgba(255, 255, 255, 0.8);
        box-shadow: rgba(0, 0, 0, 0.3) 20px 20px 20px;
    }
    </style>
</head>

<body>
<br>
<br>
<br>
<br>
<br>

<div class="container" style="margin-bottom: 25%">
    <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><img src="${resource(dir: "images/appimages", file: "appLogo.png")}"
                                                 alt="Logo" height="40"/>
                    </h3>
                </div>

                <div class="panel-body">
                    <travelx:renderAlert/>
                    <g:form id="login-form" controller="signIn" action="changePassword">
                        <div class="form-group input-group">
                            <input type="password" name="newPassword" class="form-control" id="login-username"
                                   placeholder="New Password">

                            <div class="input-group-addon">
                                <travelx:star/>
                            </div>
                        </div>

                        <div class="form-group input-group">
                            <input type="password" name="confirmPassword" class="form-control" id="login-password"
                                   placeholder="Confirm Password">

                            <div class="input-group-addon">
                                <travelx:star/>
                            </div>
                        </div>
                        <br>
                        <br>

                        <div class="form-group">
                            <button type="submit" id="login-btn"
                                    class="btn btn-primary btn-block">Update Password &nbsp; <i
                                    class="fa fa-play-circle"></i></button>

                        </div>
                        <g:hiddenField name="userToken" value="${userToken}"/>
                    </g:form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>