<div class="row">
    <div class="col-md-10 col-sm-7 col-xs-6">
        <div class="logo pull-left">
            <a href="/travelx">
                <g:img dir="images" file="logo.png" width="190px"/>
            </a>
        </div>

        <div class="main-icons-container-header pull-left">
            <a href="#" class="main-icon-box">
                <g:img dir="images" file="ico1.png"/>

                <h3>PLAN TRAVEL</h3>
            </a>
            <a href="${createLink(controller: "hotelService", action: "bookHotel")}" class="main-icon-box">
                <g:img dir="images" file="ico2.png"/>

                <h3>BOOK HOTEL</h3>
            </a>
            <a href="#" class="main-icon-box">
                <g:img dir="images" file="ico3.png"/>

                <h3>ACTIVITY</h3>
            </a>
            <a href="${createLink(controller: "tboFlightBooking", action: "searchFlights")}"
               class="main-icon-box">
                <g:img dir="images" file="ico4.png"/>

                <h3>FLIGHT BOOKING</h3>
            </a>
            <g:link controller="serviceProvider" action="chooseService" class="main-icon-box">
                <g:img dir="images" file="ico5.png"/>

                <h3>SERVICE PROVIDER</h3>
            </g:link>
            <a href="http://www.travelex360plus.net" class="main-icon-box">
                <g:img dir="images" file="ico6.png"/>

                <h3>PACKAGES</h3>
            </a>
        </div>
    </div>
    <!-- <div class="col-md-7 col-sm-8 col-xs-7"> -->
    <!-- </div> -->
    <div class="col-md-2 col-sm-3 col-xs-6 text-right">
        <div class="header-right">
            <ul class="header-icon-nav">
                <li><g:link controller="signIn" action="index">
                    <g:img dir="images" file="register-icon.png"/></g:link></li>
                <li><g:link controller="login" action="auth"><g:img dir="images" file="login-icon.png"/></g:link>
                </li>
            </ul>
        </div>
    </div>
</div>
