<%@ page import="com.travelex.Hotel.BookingHistory" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin"/>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="span12">
            <div id="tab" class="btn-group">
                <a href="${createLink(controller: 'user', action: 'index')}" id="userId"
                   class="btn btn-large btn-info  disableAll"
                   onclick="activeButton(this.id)">Users</a>
                <a href="${createLink(controller: 'bookingHistory', action: 'index')}" id="hotelId"
                   class="btn btn-large btn-info active disableAll"
                   onclick="activeButton(this.id)">Hotels</a>
                <a href="#Flights" id="flightId" class="btn btn-large btn-info disableAll" data-toggle="tab"
                   onclick="activeButton(this.id)">Flights</a>
                <a href="#Activity" id="activityId" class="btn btn-large btn-info disableAll" data-toggle="tab"
                   onclick="activeButton(this.id)">Activity</a>
                <a href="#serviceProvider" id="serviceProviderId" class="btn btn-large btn-info disableAll"
                   data-toggle="tab"
                   onclick="activeButton(this.id)">Service Provider</a>

            </div>


            <div class="tab-content" style="margin-top: 1%">

                <div class="tab-pane active" id="Hotels">

                    <ul class="nav nav-tabs">
                        <li class="active"><a href="${createLink(controller: 'bookingHistory', action: 'index')}"
                                              data-toggle="tab">Booking List</a></li>
                        <li><a href="${createLink(controller: 'coupon', action: 'index')}">Coupon List</a></li>
                        <li><a href="${createLink(controller: 'paymentInfo', action: 'index')}">Payment List</a></li>
                        <li><a href="${createLink(controller: 'setMargin', action: 'index')}">Margin List</a></li>
                    </ul>

                    <div class="tab-content" style="margin-top: 1%">
                        <div class="tab-pane active" id="hotelBookingList">
                            <div class="col-md-2">
                                <div class="list-group-item">
                                    &nbsp;<h4>Show type</h4>
                                    &nbsp;&nbsp;<input type="checkbox" id="allBooking" name="bookingStatus"
                                                       onclick="unCheckOtherBookingFilter();
                                                       searchBookingType()" value="allBooking"/>
                                    <label>All</label>
                                    <br>
                                    &nbsp;&nbsp;<input type="checkbox" name="bookingStatus"
                                                       onclick=" unCheckAllBookingFilter();
                                                       searchBookingType()"
                                                       id="booked" value="booked"/>
                                    <label>Booked</label>
                                    <br>
                                    &nbsp;&nbsp;<input type="checkbox" name="bookingStatus" id="cancelled"
                                                       onclick="unCheckAllBookingFilter();
                                                       searchBookingType()" value="cancelled"/>
                                    <label>Cancelled</label>
                                    <br>
                                    &nbsp;&nbsp;<input type="checkbox" name="bookingStatus" id="vouchered"
                                                       onclick="unCheckAllBookingFilter();
                                                       searchBookingType()" value="vouchered"/>
                                    <label>Vouchered</label>
                                    <br>
                                </div>
                            </div>

                            <div class="col-md-10" id="updateBookingSummary">
                                <g:render template="/bookingHistory/list"
                                          model="[bookingHistoryList: bookingHistoryList]"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function activeButton(id) {
        $(".disableAll").removeClass('active')
    }
</script>
<script>
    function unCheckOtherBookingFilter() {
        $("#booked").attr('checked', false);
        $("#cancelled").attr('checked', false);
        $("#vouchered").attr('checked', false);
    }
    function unCheckAllBookingFilter() {
        $("#allBooking").attr('checked', false)
    }

    $(document).ready(function () {
        $("#allBooking").attr('checked', true);
    })

    function searchBookingType() {
        var selectedCheckBox = new Array();
        $('input[name="bookingStatus" ]:checked').each(function () {
            selectedCheckBox.push(this.value);
        });
        $.ajax({
            url: "${createLink(controller: "bookingHistory" , action: "searchBookingType",absolute: true)}",
            type: "POST",
            data: {selectedCheckBox: selectedCheckBox},
            success: function (data) {
                $("#updateBookingSummary").html(data)
            }
        });
    }
</script>

</body>
</html>
