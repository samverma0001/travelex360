<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="admin">
</head>

<body>
<div class="container">
    <g:render template="/bookingHistory/voucherTemplate" model="[bookingHistory: bookingHistory]"/>
</div>
</body>
</html>