<%@ page import="com.travelex.booking.RoomBooked; com.travelex.booking.Adult; com.travelex.booking.Child" %>
<div class="row" style="margin-top: 20px">
    <table class="table table-bordered table-stripped table-condensed">
        <tr style="background-color: #dcdcdc;height: 50px">
            <td colspan="4" class="text-left"><img src="../images/appimages/appLogo.png" height="40"/></td>
            <td colspan="4" class="text-center"><h4>Hotel Voucher</h4></td>
            <td colspan="4" class="text-right"><b><a href="">Email /</a><a href="">Print Voucher</a></b></td>
        </tr>
        <tr>
            <td colspan="4" class="text-center"><strong>Booking Id</strong></td>
            <td colspan="4" class="text-center"><strong>Confirmation No</strong></td>
            <td colspan="4" class="text-center"><strong>Reference No</strong></td>
        </tr>
        <tr>
            <td colspan="4" class="text-center">${bookingHistory?.bookingIdTrvlx}</td>
            <td colspan="4" class="text-center">${bookingHistory?.confirmationIdTrvlx}</td>
            <td colspan="4" class="text-center">${bookingHistory?.bookId}</td>
        </tr>
        <tr>
            <td colspan="4"><strong
                    class="pull-left">Hotel Address:-</strong><strong
                    class="pull-right">${hotelForBooking?.hotelAddress}</strong></td>
            <td colspan="4"><strong class="pull-left">Hotel Name:-</strong><strong
                    class="pull-right">${hotelForBooking?.hotelName}</strong></td>
            <td colspan="4"
                class="text-center"><strong class="pull-left">Last Cancellation Date:-</strong><strong
                    class="pull-right">${RoomBooked.findByHotelForBooking(hotelForBooking)?.lastCancellationDate?.format('dd/MM/yyyyy')}</strong>
            </td>
        </tr>
    </table>
    <table class="table table-bordered" style="margin-top: -15px">
        <tr style="background-color: #dcdcdc;height: 30px">
            <td colspan="4" class="text-center"><strong>Room Type</strong></td>
            <td colspan="4" class="text-center"><strong>No. Of Guest</strong></td>
            <td colspan="4" class="text-center"><strong>Guest Name</strong></td>
        </tr>
        <g:each in="${roomBookedList}" var="roomBooked">
            <tr>
                <td colspan="4" class="text-center">${roomBooked?.roomCategory}</td>
                <td colspan="4"
                    class="text-center">${(Adult.findAllByRoomBooked(roomBooked).size() + Child.findAllByRoomBooked(roomBooked).size())}</td>
                <td colspan="4"
                    class="text-center">
                    <p><strong class="pull-left">Adult(s)</strong><i
                            class="pull-right">${Adult.findAllByRoomBooked(roomBooked).adultFirstName.join(',')}</i></p>
                    <br>

                    <p><strong class="pull-left">Child(s)</strong><i
                            class="pull-right">${Child.findAllByRoomBooked(roomBooked).childFirstName.join(',')}</i></p>
                </td>
            </tr>
        </g:each>
        <tr>
            <td colspan="4"></td>
            <td colspan="4"></td>
            <td colspan="4" class="text-center"><p><strong class="pull-left">Nationality</strong><i
                    class="pull-right">${bookingHistory?.counrtyName}</i></p>
            </td>
        </tr>
    </table>
    <table class="table table-bordered">
        <tr>
            <th>No. Of Night(s)</th>
            <td colspan="4">${bookingHistory?.checkOut - bookingHistory?.checkIn}</td>
            <th>No. Of Room(s)</th>
            <td colspan="4">${bookingHistory?.noOfRooms}</td>
            <th>Email</th>
            <td colspan="4">${bookingHistory?.user?.username}</td>
        </tr>
        <tr>
            <th>Check In</th>
            <td colspan="4">${bookingHistory?.checkIn?.format('dd/MM/yyyy')}</td>
            <th>Check Out</th>
            <td colspan="4">${bookingHistory?.checkOut?.format('dd/MM/yyyy')}</td>
            <th>Contact Info</th>
            <td colspan="4">${bookingHistory?.user?.mobNo}</td>
        </tr>
        <tr>
            <td colspan="12"><p><strong class="pul-left" style="color: dodgerblue">Cancellation Policy:</strong></p>

                <p>${hotelForBooking?.roomBooked?.cancelPolicy?.first()}</p>
            </td>
        </tr>
        <tr>
            <td colspan="12"><p><strong class="pul-left" style="color: dodgerblue">Remarks:</strong></p>

                <p>Please note that while your booking had been confirmed and is guaranteed, the rooming list with your name may not be adjusted in the hotel's reservation system until closer to arrival.
                </p>
            </td>
        </tr>


        <tr>
            <td colspan="12"><p><strong class="pul-left"
                                        style="color: dodgerblue">Booking Terms & Conditions:</strong></p>

                <p>You must present a photo ID at the time of check in. Hotel may ask for credit card or cash deposit for the extra services at the time of check in.</p>

                <p>All extra charges should be collected directly from clients prior to departure such as parking, phone calls, room service, city tax, etc.</p>

                <p>We don’t accept any responsibility for additional expenses due to the changes or delays in air, road, rail, sea or indeed of any other causes, all such expenses will have to be borne by passengers.</p>

                <p>In case of wrong residency & nationality selected by user at the time of booking; the supplement charges may be applicable and need to be paid to the hotel by guest on check in/ check out.</p>

                <p>Any special request for bed type, early check in, late check out, smoking rooms, etc are not guaranteed as subject to availability at the time of check in.</p>
            </td>
        </tr>
    </table>
</div>