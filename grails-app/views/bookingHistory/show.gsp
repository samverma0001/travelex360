<%@ page import="com.travelex.booking.Adult; com.travelex.booking.RoomBooked; com.travelex.booking.HotelForBooking; com.travelex.Hotel.BookingHistory" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin">
</head>

<body>

<div class="container">
    <div class="row">
        <div class="span12">
            <div id="tab" class="btn-group">
                <a href="${createLink(controller: 'user', action: 'index')}" id="userId"
                   class="btn btn-large btn-info active disableAll"
                   onclick="activeButton(this.id)">Users</a>
                <a href="${createLink(controller: 'bookingHistory', action: 'index')}" id="hotelId"
                   class="btn btn-large btn-info disableAll"
                   onclick="activeButton(this.id)">Hotels</a>
                <a href="#Flights" id="flightId" class="btn btn-large btn-info disableAll" data-toggle="tab"
                   onclick="activeButton(this.id)">Flights</a>
                <a href="#Activity" id="activityId" class="btn btn-large btn-info disableAll" data-toggle="tab"
                   onclick="activeButton(this.id)">Activity</a>
                <a href="#serviceProvider" id="serviceProviderId" class="btn btn-large btn-info disableAll"
                   data-toggle="tab"
                   onclick="activeButton(this.id)">Service Provider</a>

            </div>

            <div class="tab-content" style="margin-top: 1%">

                <div class="tab-pane active" id="Hotels">

                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#showBookingHistory" data-toggle="tab">Show Booking History</a></li>
                    </ul>

                    <table class="table table-bordered table-condensed">
                        <tr>
                            <th>Hotel Name</th>
                            <td>${HotelForBooking.findByBookingHistory(bookingHistoryInstance)?.hotelName}</td>
                            <th>Hotel Address</th>
                            <td>${HotelForBooking.findByBookingHistory(bookingHistoryInstance)?.hotelAddress}</td>
                        </tr>
                        <tr>
                            <th>Book Id</th>
                            <td>${bookingHistoryInstance?.bookId}</td>
                            <th>Confirmation No</th>
                            <td>${bookingHistoryInstance?.confirmationNo}</td>
                        </tr>
                        <tr>
                            <th>Reference No</th>
                            <td>${bookingHistoryInstance?.referenceNo}</td>
                            <th>Trip Id</th>
                            <td>${bookingHistoryInstance?.tripId}</td>
                        </tr>
                        <tr>
                            <th>Room Category</th>
                            <td>${RoomBooked.findByHotelForBooking(hotelForBooking)?.roomCategory}</td>
                            <th>Check In</th>
                            <td>${bookingHistoryInstance?.checkIn?.format('dd/MM/yyyy')}</td>
                        </tr>
                        <tr>
                            <th>Check Out</th>
                            <td>${bookingHistoryInstance?.checkOut?.format('dd/MM/yyyy')}</td>
                            <th>No Of Room(s)</th>
                            <td>${bookingHistoryInstance?.noOfRooms}</td>
                        </tr>
                        <tr>
                            <th>No Of Adult(s)</th>
                            <td>${1}</td>
                            <th>No Of Child(s)</th>
                            <td>${2}</td>
                        </tr>

                        <tr>
                            <th>Booking Id Travelex</th>
                            <td>${bookingHistoryInstance?.bookingIdTrvlx}</td>
                            <th>Confirmation Id Travelex</th>
                            <td>${bookingHistoryInstance?.confirmationIdTrvlx}</td>
                        </tr>
                        <tr>
                            <th>Cancel Id Travelex</th>
                            <td>${bookingHistoryInstance?.cancelIdTrvlx}</td>
                            <th>Booking Status</th>
                            <td>${bookingHistoryInstance?.bookingStatus}</td>
                        </tr>
                        <tr>
                            <th>Date Created</th>
                            <td>${bookingHistoryInstance?.dateCreated?.format('dd/MM/yyyy')}</td>
                            <th>Last Updated</th>
                            <td>${bookingHistoryInstance?.lastUpdated?.format('dd/MM/yyyy')}</td>
                        </tr>
                        <tr>
                            <th>User</th>
                            <td>${bookingHistoryInstance?.user?.username}</td>
                            <th>
                                <a href="${createLink(controller: 'bookingHistory', action: 'showVoucher', params: [bookingHistoryId: bookingHistoryInstance?.id])}"
                                   class="btn btn-xs btn-primary">View Voucher</a>
                                <a href="${createLink(controller: 'home', action: 'showInvoice', params: [bookingHistoryId: bookingHistoryInstance?.id])}"
                                   class="btn btn-xs btn-primary">View Invoice</a>
                            </th>

                            <td><g:form url="[resource: bookingHistoryInstance, action: 'delete']" method="DELETE">
                                <fieldset class="buttons">
                                    <g:link class="cancel btn btn-xs btn-primary"
                                            controller="bookingHistory" action="cancelBookingByAdmin"
                                            params="[bookRefNo: bookingHistoryInstance?.bookId, bookingHistoryId: bookingHistoryInstance?.id]">Cancel</g:link>
                                    <g:actionSubmit class="delete btn btn-xs btn-danger" action="delete"
                                                    value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                                    onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                                </fieldset>
                            </g:form>

                            </td>
                        </tr>

                    </table>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    function activeButton(id) {
        $(".disableAll").removeClass('active')
    }
</script>

</body>
</html>
