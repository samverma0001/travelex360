<%@ page import="com.travelex.booking.HotelForBooking" %>
<table class="table table-bordered table-condensed table-striped">
    <thead>
    <tr>
        <g:sortableColumn property="hotelName"
                          title="${message(code: 'bookingHistory.hotelName.label', default: 'Hotel Name')}"/>

        <g:sortableColumn property="bookingIdTrvlx"
                          title="${message(code: 'bookingHistory.bookingIdTrvlx.label', default: 'Travelex Booking Id')}"/>

        <g:sortableColumn property="confirmationIdTrvlx"
                          title="${message(code: 'bookingHistory.confirmationIdTrvlx.label', default: 'Travelex Conf. Id')}"/>

        <g:sortableColumn property="bookId"
                          title="${message(code: 'bookingHistory.bookId.label', default: 'Book Id')}"/>

        <g:sortableColumn property="checkIn"
                          title="${message(code: 'bookingHistory.checkIn.label', default: 'Check In')}"/>

        <g:sortableColumn property="checkOut"
                          title="${message(code: 'bookingHistory.checkOut.label', default: 'Check Out')}"/>

        <g:sortableColumn property="bookingStatus"
                          title="${message(code: 'bookingHistory.bookingStatus.label', default: 'Booking Status')}"/>

    </tr>
    </thead>
    <tbody>
    <g:each in="${bookingHistoryList}" var="bookingHistoryInstance">
        <tr>
            <td><g:link controller="bookingHistory" action="show"
                        id="${bookingHistoryInstance?.id}">${HotelForBooking.findByBookingHistory(bookingHistoryInstance)?.hotelName}</g:link></td>
            <td>${bookingHistoryInstance?.bookingIdTrvlx}</td>
            <td>${bookingHistoryInstance?.confirmationIdTrvlx}</td>
            <td>${bookingHistoryInstance?.bookId}</td>
            <td>${bookingHistoryInstance?.checkIn?.format("dd/MM/yyyy")}</td>
            <td>${bookingHistoryInstance?.checkOut?.format("dd/MM/yyyy")}</td>
            <td>${bookingHistoryInstance?.bookingStatus}</td>
        </tr>
    </g:each>
    </tbody>
</table>

<div class="pagination">
    <g:paginate total="${bookingHistoryInstanceCount ?: 0}"/>
</div>