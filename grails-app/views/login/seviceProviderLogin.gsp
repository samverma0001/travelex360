<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name='layout' content='headerFooterLayout'/>
</head>

<body>

<div class="inner-page-container content-page full-height theme-lime">
    <!-- <div class="middle-container"> -->
    <div class="container-fluid">
        <!-- <h1 class="main-title text-center">Plan a Travel for your Holiday</h1> -->
        <div class="service-login-container">
            <div class="larga-icon-container">
                <div class="main-icon-box">
                    <img src="../images/service-activity.png" height="120px">
                    <h4>ACTIVITY SERVICES LOGIN</h4>
                </div>
            </div>

            <div class="login-box text-center">
                <h3>Login to Access Activity Services</h3>

                <form class="tvx-from form" id="login-form" action="${postUrl}" method='POST' autocomplete='off'>
                    <div class="form-group text-left">
                        <label for="username"
                               class="col-sm-4 control-label control-label-side">User Name / Email</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" name='j_username' id="username"
                                   placeholder="Username">
                        </div>
                    </div>

                    <div class="form-group text-left">
                        <label for="password" class="col-sm-4 control-label control-label-side">Password</label>

                        <div class="col-sm-8">

                            <input type="password" class="form-control" name="j_password" id="password"
                                   placeholder="Password">
                        </div>
                    </div>

                    <div class="form-group text-left">
                        <div class="col-sm-offset-4 col-sm-8 text-left">
                            <button type="submit" class="btn btn-default">Sign In</button>
                            <a href="${createLink(controller: "signIn", action: "serviceProviderSignUp")}"
                               class="btn btn-default">
                                Register as service provider</a>
                        </div>

                    </div>

                    <div class="form-group">
                        <p>Forgot Password? <a
                                href="${createLink(controller: "signIn", action: "resetPassword")}">Click Here</a></p>
                        <input type='checkbox' class='chk' name='${rememberMeParameter}' id='remember_me'
                               <g:if test='${hasCookie}'>checked='checked'</g:if>/>&nbsp;
                        <label for='remember_me'><g:message code="springSecurity.login.remember.me.label"/></label>
                    </div>
                </form>

            </div>
        </div>

    </div>
    <!-- </div> -->
</div>

<script type='text/javascript'>
    <!--
    (function () {
        document.forms['loginForm'].elements['j_username'].focus();
    })();
    // -->
</script>
</body>
</html>
