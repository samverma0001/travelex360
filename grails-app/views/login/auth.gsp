<html>
<head>
    <meta name='layout' content='headerFooterLayoutWithMenu'/>
</head>


<body>
<div class="full-height login-page">
    <div class="inner-page-container full-height">
        <!-- <div class="middle-container"> -->
        <div class="container-fluid">
            <div class="login-container">
                <div class="row">
                    <div class="col-md-7">
                        <h1>Login into your Account</h1>

                        <form class="login-form form" id="login-from" autocomplete="off" action="${postUrl}"
                              method="POST">
                            <div class="form-group">
                                <input type="text" class="form-control username-field" id="username"
                                       placeholder="User Name / Email" name="j_username">
                            </div>

                            <div class="form-group">
                                <input type="password" class="form-control password-field" name="j_password"
                                       id="password"
                                       placeholder="Password">
                            </div>

                            <div class="form-group">
                                <button value='${message(code: "springSecurity.login.button")}' type="submit"
                                        class="btn btn-success img-full">Login</button>
                            </div>

                            <div class="form-group text-left">
                                <a href="${createLink(controller: "signIn", action: "resetPassword")}">Forgot Password?</a>
                            </div>
                            <div class="form-group">
                                <input type='checkbox' class='chk' name='${rememberMeParameter}' id='remember_me'
                                       <g:if test='${hasCookie}'>checked='checked'</g:if>/>
                                <label for='remember_me'><g:message
                                        code="springSecurity.login.remember.me.label"/></label>
                            </div>
                        </form>
                    </div>

                    <div class="col-md-5">
                        <div class="text-center">
                            <a href="${createLink(controller: "signIn", action: "index")}">
                                Dont' have an account? Register Now!</a>
                        </div>

                        <div class="other-sign-in text-center">
                            <a href="${createLink(controller: 'signIn', action: 'connectGoogle')}"><img
                                    src="../images/google-login.png" class="img-full"></a>
                            <a href="#"><img src="../images/facebook-login.png" class="img-full"></a>
                            <a href="${createLink(controller: 'signIn', action: 'connectLinkedIn')}"><img
                                    src="../images/linkedIn-login.jpg" class="img-full"></a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- </div> -->
    </div>
</div>
<script type='text/javascript'>
    <!--
    (function () {
        document.forms['loginForm'].elements['j_username'].focus();
    })();
    // -->
</script>
</body>
</html>
