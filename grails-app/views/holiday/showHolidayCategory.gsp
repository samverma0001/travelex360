<%@ page import="java.math.RoundingMode" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <style>
    .col-item {
        border: 1px solid #E1E1E1;
        border-radius: 5px;
        background: #FFF;
    }

    .col-item .photo img {
        margin: 0 auto;
        width: 100%;
    }

    .col-item .info {
        padding: 10px;
        border-radius: 0 0 5px 5px;
        margin-top: 1px;
    }

    .col-item .info .rating {
        color: #777;
    }

    .col-item .separator {
        border-top: 1px solid #E1E1E1;
    }

    .clear-left {
        clear: left;
    }

    .col-item .separator p {
        line-height: 20px;
        margin-bottom: 0;
        margin-top: 10px;
        text-align: center;
    }

    .col-item .separator p i {
        margin-right: 5px;
    }

    .col-item .btn-add {
        width: 50%;
        float: left;
    }

    .col-item .btn-add {
        border-right: 1px solid #E1E1E1;
    }

    .col-item .btn-details {
        width: 50%;
        float: left;
        padding-left: 10px;
    }

    .controls {
        margin-top: 20px;
    }

    </style>

</head>

<body>
<br>
<br>
<br>
<br>
<br>

<div class="modal fade" id="clickHere" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title"
                    id="myModalLabel"><strong>To Book Package's / Find Out More Detail Please Call 9810396839 Or <a
                        href="mailto:info@travelx360.com">Mail us</a> !</strong></h4>
            </div>

            <g:form controller="holiday" action="submitQuery">
                <div class="modal-body">
                    <p><h4><strong>You can also mail your query <a href="mailto:info@travelx360.com">info@travelx360.com</a> !</strong></h4></p>
                    <div class="row">
                        <div class="col-md-4"><label>Name</label></div>

                        <div class="col-md-8 input-group form-group">
                            <input type="text" class="form-control" name="name" required=""/>

                            <div class="input-group-addon">
                <travelx:star/>
                </div>
            </div>
        </div>

                <div class="row">
                    <div class="col-md-4"><label>Email</label></div>

                    <div class="col-md-8 input-group form-group">
                        <input type="email" class="form-control" name="email" required=""/>

                        <div class="input-group-addon">
                            <travelx:star/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4"><label>Contact No.</label></div>

                    <div class="col-md-8 input-group form-group">
                        <input type="text" class="form-control" name="contactNo" required=""/>

                        <div class="input-group-addon">
                            <travelx:star/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4"><label>Message</label></div>

                    <div class="col-md-8 input-group form-group">
                        <g:textArea name="message" class="form-control" required=""/>
                        <div class="input-group-addon">
                            <travelx:star/>
                        </div>
                    </div>
                </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success" value="Submit">
                    <input type="reset" class="btn btn-danger" value="Reset">
                </div>
            </g:form>
        </div>
    </div>
</div>

<div class="container" style="margin-bottom: 50%;">
    <travelx:renderAlert/>
    <div class="col-md-3 list-group-item">
        <div class="row ">
            <h4><strong>To Know More/Book Packages <a href="" data-toggle="modal" data-target="#clickHere">Click Here
            </a></strong></h4>

        </div>
    </div>

    <div class="col-md-9">
        <div class="carousel">
            <div class="item">
                <div class="row" style="margin-bottom: 2%">
                    <g:each in="${holidayCategoryList}" var="holidayCategory" status="i">
                        <a href="${createLink(controller: 'package', action: 'showPackages', params: [categoryId: holidayCategory?.id])}">
                            <div class="col-md-4 col-sm-4 col-xs-4" style="margin-top: 2%">
                                <div class="col-item text-center" style="background-color: #5D3126">
                                    <p><h5
                                        style="color: #ffffff;"><strong>${holidayCategory?.categoryName}</strong>
                                </h5>
                                </p>
                                    <div class="photo">
                                        <g:if test="${holidayCategory?.imageName}">
                                            <img src="${createLink(controller: "holiday", action: "showHolidayCategoryImage", params: [categoryId: holidayCategory?.id])}"
                                                 class="img-responsive" alt="a"
                                                 style="height: 100px;width: 250px"/>
                                        </g:if>
                                        <g:else>
                                            <img src="${resource(dir: '/images/appimages', file: '350x260.gif')}"
                                                 class="img-responsive" alt="a"
                                                 style="height: 100px;width: 250px"/>
                                        </g:else>
                                    </div>

                                    <div class="info">
                                        <p>
                                        <h5>
                                            <strong class="pull-left"
                                                    style="color: white">Starts from Rs.&nbsp;
                                            <g:each in="${rangeList}" var="priceRange" status="j">
                                                <g:if test="${i == j}">
                                                    ${priceRange?.setScale(0, RoundingMode.HALF_UP)}
                                                </g:if>

                                            </g:each>
                                            &nbsp;/-&nbsp;...</strong>
                                            <strong class="pull-right"
                                                    style="color: white"></strong>
                                        </h5>
                                    </p>

                                        %{--<div class="separator clear-left">--}%
                                        %{--<p class="btn-add">--}%
                                        %{--<a href="${createLink(controller: 'package', action: 'showPackages', params: [holidayId: holiday?.id])}"--}%
                                        %{--class="btn btn-xs btn-primary">--}%
                                        %{--More Details--}%
                                        %{--</a>--}%
                                        %{--</p>--}%
                                        %{--</div>--}%

                                        <div class="clearfix">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </g:each>
                </div>
            </div>
        </div>
    </div>

</div>
%{--<script>--}%

%{--function validateForm() {--}%
%{--alert('before validate')--}%
%{--$("#submitFormId").validate({--}%
%{--debug: true,--}%
%{--rules: {--}%
%{--name: "required",--}%
%{--email: {--}%
%{--required: true,--}%
%{--email: true--}%
%{--},--}%
%{--contactNo: "required",--}%
%{--message: "required"--}%
%{--},--}%
%{--messages: {--}%
%{--name: {--}%
%{--required: "Name cannot be left blank"--}%
%{--},--}%
%{--email: {--}%
%{--required: "Email cannot be left blank"--}%
%{--}--}%
%{--}--}%
%{--})--}%
%{--}--}%
%{--</script>--}%
</body>
</html>