<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="admin">
</head>

<body>

<div content="container">
    <travelx:renderAlert/>
    <g:form controller="holiday" action="updateCategory" enctype="multipart/form-data">
        <g:hasErrors bean="${holidayCategoryCO}">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span
                        aria-hidden="true">&times;
                </span><span class="sr-only">Close</span></button>

                <div id="errors" class="alert alert-error">
                    <g:renderErrors bean="${holidayCategoryCO}"/>
                </div>
            </div>
        </g:hasErrors>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-4">
                    <label>Title</label>
                </div>

                <div class="col-md-8 input-group form-group">
                    <input type="text" class="form-control" name="categoryName"
                           value="${holidayCategory?.categoryName}"/>

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <label>Category image</label>
                </div>

                <div class="col-md-8">
                    <input type="file" class="form-control" name="categoryImage"/>
                </div>
            </div>
            <br>

            <div class="row">
                <div class="col-md-4">
                    <strong>Current image</strong>
                </div>

                <div class="col-md-8">
                    <g:if test="${holidayCategory?.imageName}">
                        <img src="${createLink(controller: "holiday", action: "showHolidayCategoryImage", params: [categoryId: holidayCategory?.id])}"
                             class="img-responsive" alt="a" width="350" height="260"/>
                    </g:if>
                    <g:else>
                        <img src="${resource(dir: '/images/appimages', file: '350x260.gif')}"
                             class="img-responsive" alt="a" width="350" height="260"/>
                    </g:else>
                </div>
            </div>

        </div>
        <g:hiddenField name="categoryId" value="${holidayCategory?.id}"/>
        <input type="submit" class="btn btn-primary" value="Save"/>
    </g:form>
    <br>

    <div class="row col-md-offset-4">
        <a href="${createLink(controller: 'holiday', action: 'listCategory')}" class="btn btn-primary">Back</a>
    </div>
</div>
</body>
</html>