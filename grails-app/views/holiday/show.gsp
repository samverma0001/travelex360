<%@ page import="com.travelex.admin.HolidayImage" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="admin"/>
    <style>
    @import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);

    .col-item {
        border: 1px solid #E1E1E1;
        border-radius: 5px;
        background: #FFF;
    }

    .col-item .photo img {
        margin: 0 auto;
        width: 100%;
    }

    .col-item .info {
        padding: 10px;
        border-radius: 0 0 5px 5px;
        margin-top: 1px;
    }

    .col-item:hover .info {
        background-color: #F5F5DC;
    }

    .col-item .price {
        /*width: 50%;*/
        float: left;
        margin-top: 5px;
    }

    .col-item .price h5 {
        line-height: 20px;
        margin: 0;
    }

    .price-text-color {
        color: #219FD1;
    }

    .col-item .info .rating {
        color: #777;
    }

    .col-item .rating {
        /*width: 50%;*/
        float: left;
        font-size: 17px;
        text-align: right;
        line-height: 52px;
        margin-bottom: 10px;
        height: 52px;
    }

    .col-item .separator {
        border-top: 1px solid #E1E1E1;
    }

    .clear-left {
        clear: left;
    }

    .col-item .separator p {
        line-height: 20px;
        margin-bottom: 0;
        margin-top: 10px;
        text-align: center;
    }

    .col-item .separator p i {
        margin-right: 5px;
    }

    .col-item .btn-add {
        width: 50%;
        float: left;
    }

    .col-item .btn-add {
        border-right: 1px solid #E1E1E1;
    }

    .col-item .btn-details {
        width: 50%;
        float: left;
        padding-left: 10px;
    }
    </style>
</head>

<body>
<travelx:renderAlert/>

<div class="container" style="margin-bottom: 10%">
<table class="table table-bordered table-condensed">
    <tr>
        <th colspan="6">Category</th>
        <th colspan="6">${holidayPackage?.holidayCategory?.categoryName}</th>
        <th colspan="6">Holiday name</th>
        <th colspan="6">${holidayPackage?.holidayName}</th>
    </tr>
    <tr>
        <th colspan="6">Image name</th>
        <th colspan="6">${holidayPackage?.holidayImageName}</th>
        <th colspan="6">Image</th>
        <th colspan="6">
            <g:if test="${holidayPackage?.holidayImageName}">
                <img src="${createLink(controller: 'holiday', action: 'showHolidayPackageImage', params: [holidayPackageId: holidayPackage?.id])}"
                     class="img-responsive" alt="a" width="350" height="260"/>
            </g:if>
            <g:else>
                <img src="${resource(dir: '/images/appimages', file: '350x260.gif')}"
                     class="img-responsive" alt="a" width="350" height="260"/>
            </g:else>
        </th>
    </tr>
    <tr>
        <th colspan="6">Start date</th>
        <th colspan="6">${holidayPackage?.startDate?.format('dd/MM/yyyy')}</th>
        <th colspan="6">End date</th>
        <th colspan="6">${holidayPackage?.endDate?.format('dd/MM/yyyy')}</th></tr>
    <tr>
        <th colspan="6">Status</th>
        <th colspan="6">${holidayPackage?.holidayStatus}</th>
        <th colspan="6">Type</th>
        <th colspan="6">${holidayPackage?.holidayType}</th>
    </tr>
    <tr>
        <th colspan="6">Date created</th>
        <th colspan="6">${holidayPackage?.dateCreated?.format('dd/MM/yyyy')}</th>
        <th colspan="6">Last updated</th>
        <th colspan="6">${holidayPackage?.lastUpdated?.format('dd/MM/yyyy')}</th>
    </tr>
    <tr>
        <th colspan="6">Price range</th>
        <th colspan="6">${holidayPackage?.priceRange}</th>
        <th colspan="6">Total seat</th>
        <th colspan="6">${holidayPackage?.totalSeat}</th>
    </tr>
    <tr>
        <th colspan="6">Visa include</th>
        <th colspan="6">${holidayPackage?.visaIncluded}</th>
    </tr>
    <tr>
        <th colspan="6">Visa on arrival</th>
        <th colspan="6">${holidayPackage?.visaOnArrival}</th>
        <th colspan="6">Travel insurance</th>
        <th colspan="6">${holidayPackage?.travelInsurance}</th>
    </tr>
</table>

<div class="row">
    <a href="${createLink(controller: 'holiday', action: 'index')}"
       class="btn btn-primary">Back</a>
    <g:if test="${packageDetail}">

    </g:if>
    <g:else>
        <a href="${createLink(controller: 'package', action: 'addPackageDetail', params: [holidayPackageId: holidayPackage?.id])}"
           class="btn btn-success">Add detail</a>
    </g:else>
    <g:if test="${stayDetails}">
        <a href="${createLink(controller: 'package', action: 'addPackageItinerary', params: [holidayPackageId: holidayPackage?.id])}"
           class="btn btn-warning">Add itinerary</a>
    </g:if>
    <g:else>
    </g:else>
    <a href="${createLink(controller: 'package', action: 'createStay', params: [holidayPackageId: holidayPackage?.id])}"
       class="btn btn-warning">Add Stay</a>
    <a href="${createLink(controller: 'package', action: 'listFlight', params: [holidayPackageId: holidayPackage?.id])}"
       class="btn btn-warning">Add Flight</a>
</div>
<br>

<div class="row">
    <div class="col-md-2">
        <strong>Stay Detail(s)</strong>
    </div>

    <div class="col-md-10">
        <table class="table table-bordered table-condensed">
            <thead>
            <tr>
                <th>Country</th>
                <th>State</th>
                <th>City</th>
                <th>Location</th>
                <th>No of night</th>
                <th>Action</th>
            </tr>
            </thead>
            <g:if test="${stayDetails}">
                <g:each in="${stayDetails}" var="stayDetail">
                    <tr>
                        <td>${stayDetail?.country}</td>
                        <td>${stayDetail?.state}</td>
                        <td>${stayDetail?.city}</td>
                        <td>${stayDetail?.location}</td>
                        <td>${stayDetail?.noOfNight}</td>
                        <td><a class="btn btn-primary btn-xs"
                               href="${createLink(controller: "package", action: "editStayForPackage", params: [stayId: stayDetail?.id, holidayPackageId: holidayPackage?.id])}">Edit</a>
                            <a class="btn btn-danger btn-xs"
                               href="${createLink(controller: "package", action: "deleteStayForPackage", params: [stayId: stayDetail?.id, holidayPackageId: holidayPackage?.id])}">Delete</a>
                            <a href="${createLink(controller: "package", action: "listHotel", params: [stayId: stayDetail?.id])}"
                               class="btn btn-xs btn-success">Add hotel</a>

                        </td>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
            </g:else>

        </table>
    </div>
</div>

<div class="row">
    <div class="col-md-2">
        <strong>Detail(s)</strong>
    </div>

    <g:if test="${packageDetail}">
        <div class="col-md-10">
            <div class="row">
                <div id="carousel-example" class="carousel slide hidden-xs" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="item active">
                            <g:each in="${holidayImageList}" var="holidayImage">
                                <div class="col-md-3">
                                    <div class="col-item">
                                        <div class="photo">
                                            <img height="150" width="200"
                                                 src="${createLink(controller: 'package', action: 'showHolidayImage', params: [holidayImageId: holidayImage?.id])}"/>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <a href="${createLink(controller: 'package', action: 'deleteHolidayImage', params: [holidayImageId: holidayImage?.id, holidayPackageId: holidayPackage?.id])}"
                                                   class="btn btn-xs btn-danger pull-left">delete</a>
                                                <a href="${createLink(controller: 'package', action: 'editDetail', params: [holidayImageId: holidayImage?.id, detailId: packageDetail?.id, holidayPackageId: holidayPackage?.id])}"
                                                   class="btn btn-xs btn-warning pull-right">Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </g:each>
                        </div>
                    </div>
                </div>
            </div>
            <strong>Overview</strong>

            <p>${packageDetail?.overview}</p>
            <strong>Inclusion</strong>

            <p>${packageDetail?.inclusion}</p>
            <strong>Exclusion</strong>

            <p>${packageDetail?.exclusion}</p>
            <strong>Payment Policy</strong>

            <p>${packageDetail?.paymentPolicy}</p>
            <strong>Cancellation Policy</strong>

            <p>${packageDetail?.cancellationPolicy}</p>
            <strong>Terms & Cond.</strong>

            <p>${packageDetail?.termsCondition}</p>

            <p>
                <a href="${createLink(controller: 'package', action: 'editDetail', params: [detailId        : packageDetail?.id,
                                                                                            holidayPackageId: holidayPackage?.id])}"
                   class="btn btn-xs btn-primary">Edit</a>
                <a href="${createLink(controller: 'package', action: 'deleteDetail', params: [detailId        : packageDetail?.id,
                                                                                              holidayPackageId: holidayPackage?.id])}"
                   class="btn btn-xs btn-danger">Delete</a>
            </p>
        </div>

    </g:if>
    <g:else>
        <strong>Not available</strong>
    </g:else>
</div>
<br>

<div class="row">
    <div class="col-md-2">
        <strong>Itinerary(s)</strong>
    </div>

    <g:if test="${itineraryList}">
        <div class="col-md-10">
            <g:each in="${itineraryList}" var="packageItinerary" status="i">
                <strong>Heading</strong>

                <p>${packageItinerary?.dayHeading}</p>
                <strong>Meal</strong>

                <p>${packageItinerary?.dayMeal}</p>
                <strong>No of day</strong>

                <p>${packageItinerary?.dayNumber}</p>
                <strong>Description</strong>

                <p>${packageItinerary?.description}</p>

                <p>
                    <a href="${createLink(controller: 'package', action: 'editItinerary', params: [itineraryId     : packageItinerary?.id,
                                                                                                   holidayPackageId: holidayPackage?.id])}"
                       class="btn btn-xs btn-primary">Edit</a>
                    <a href="${createLink(controller: 'package', action: 'deleteItinerary', params: [itineraryId     : packageItinerary?.id,
                                                                                                     holidayPackageId: holidayPackage?.id])}"
                       class="btn btn-xs btn-danger">Delete</a>
                </p>
            </g:each>
        </div>
    </g:if>
    <g:else>
        <strong>Not available</strong>
    </g:else>
</div>
<br/>

<div class="row">
    <div class="col-md-2">
        <strong>Flight(s)</strong>
    </div>

    <g:if test="${holidayPackageFlightList}">
        <div class="col-md-10">
            <table class="table table-bordered table-condensed">
                <tr>
                    <th><strong>Flight name</strong></th>
                    <th><strong>Flight number</strong></th>
                    <th><strong>Dept. from</strong></th>
                    <th><strong>Dest. To</strong></th>
                    <th><strong>Dapt. date</strong></th>
                    <th><strong>Arrival date</strong></th>
                    <th><strong>Dept. time</strong></th>
                    <th><strong>Dest. time</strong></th>
                    <th><strong>Action</strong></th>
                </tr>
                <g:each in="${holidayPackageFlightList}" var="holidayPackageFlight">
                    <tr>
                        <td>${holidayPackageFlight?.flight?.flightName}</td>
                        <td>${holidayPackageFlight?.flight?.flightNumber}</td>
                        <td>${holidayPackageFlight?.flight?.departureFrom}</td>
                        <td>${holidayPackageFlight?.flight?.destinationTo}</td>
                        <td>${holidayPackageFlight?.flight?.departureDate?.format('MM/dd/yyyy')}</td>
                        <td>${holidayPackageFlight?.flight?.destinationDate?.format('MM/dd/yyyy')}</td>
                        <td>${holidayPackageFlight?.flight?.departureTime}</td>
                        <td>${holidayPackageFlight?.flight?.destinationTime}</td>
                        <td><a href="${createLink(controller: 'package', action: 'removeFlightFromPackage', params:
                                [holidayPackageFlightId: holidayPackageFlight?.id, holidayPackageId: holidayPackage?.id])}"
                               class="btn btn-xs btn-danger">Remove</a>
                        </td>
                    </tr>
                </g:each>
            </table>

        </div>
    </g:if>
    <g:else>
        <strong>Flight to be announced !</strong>
    </g:else>
</div>
</div>
</body>
</html>