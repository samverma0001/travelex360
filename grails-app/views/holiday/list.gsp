<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="admin"/>
</head>

<body>
<travelx:adminUserTab/>
<travelx:renderAlert/>
<div class="tab-content" style="margin-top: 1%">
    <div class="tab-pane active" id="holiday" style="margin-left: 5%">
        <ul class="nav nav-tabs">
            <li><a href="${createLink(controller: 'holiday', action: 'listCategory')}">Holiday Category</a></li>
            <li class="active"><a href="${createLink(controller: "holiday", action: "index")}">Holiday</a></li>
            <li><a href="${createLink(controller: 'package', action: 'listHotel')}">Holiday Hotel</a></li>
            <li><a href="${createLink(controller: "package", action: 'listFlight')}">Holiday Flight</a></li>
        </ul>

        <div class="tab-content" style="margin-top: 1%">
            <div class="tab-pane active" id="editUser">
                <div class="col-md-12">
                    <div class="row">
                        <label>Holiday List:-</label>
                        <a href="${createLink(controller: 'holiday', action: 'create')}"
                           class="btn btn-primary pull-right">Create</a>&nbsp;&nbsp;&nbsp;
                        <a href="${createLink(controller: 'holiday', action: 'listCategory')}"
                           class="btn btn-primary pull-right">Back</a>
                    </div>
                    <br>
                    <table class="table table-bordered table-condensed">
                        <tr>
                            <th class="text-center">Category</th>
                            <th class="text-center">Holiday name</th>
                            <th class="text-center">Start date</th>
                            <th class="text-center">End date</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Type</th>
                            <th class="text-center">Action</th>
                        </tr>
                        <g:each in="${holidayPackageList}" var="holidayPackage">
                            <tr class="text-center">
                                <td>${holidayPackage?.holidayCategory?.categoryName}
                                </td>
                                <td><a href="${createLink(controller: 'holiday', action: 'show', params: [holidayPackageId: holidayPackage?.id])}">${holidayPackage?.holidayName}</a>
                                </td>
                                <td>${holidayPackage?.startDate?.format('MM/dd/yyyy')}</td>
                                <td>${holidayPackage?.endDate?.format('MM/dd/yyyy')}</td>
                                <td>${holidayPackage?.holidayStatus}</td>
                                <td>${holidayPackage?.holidayType}</td>
                                <th><a href="${createLink(controller: 'holiday', action: 'edit', params: [holidayPackageId: holidayPackage?.id])}"
                                       class="btn btn-xs btn-primary">Edit</a>
                                    <a href="${createLink(controller: 'holiday', action: 'delete', params: [holidayPackageId: holidayPackage?.id])}"
                                       class="btn btn-xs btn-danger">Delete</a>
                                </th>
                            </tr>
                        </g:each>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>