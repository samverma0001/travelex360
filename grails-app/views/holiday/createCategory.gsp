<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="admin">
</head>

<body>

<div content="container">
    <travelx:renderAlert/>
    <g:form controller="holiday" action="saveCategory" enctype="multipart/form-data">
        <g:hasErrors bean="${holidayCategoryCO}">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span
                        aria-hidden="true">&times;
                </span><span class="sr-only">Close</span></button>

                <div id="errors" class="alert alert-error">
                    <g:renderErrors bean="${holidayCategoryCO}"/>
                </div>
            </div>
        </g:hasErrors>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-4">
                    <label>Title</label>
                </div>

                <div class="col-md-8 input-group form-group">
                    <input type="text" class="form-control" name="categoryName"/>

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <label>Category image</label>
                </div>

                <div class="col-md-8">
                    <input type="file" class="form-control" name="categoryImage"/>
                </div>
            </div>
        </div>
        <input type="submit" class="btn btn-primary" value="Save"/>
    </g:form>
</div>
</body>
</html>