<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="admin">
</head>

<body>
<travelx:renderAlert/>
<travelx:adminUserTab/>
<div class="tab-content" style="margin-top: 1%">
    <div class="tab-pane active" id="holiday" style="margin-left: 5%">
        <ul class="nav nav-tabs">
            <li class="active"><a href="${createLink(controller: 'user', action: 'index')}">Holiday Category</a></li>
            <li><a href="${createLink(controller: "holiday", action: "index")}">Holidays</a></li>
            <li><a href="${createLink(controller: "package", action: 'listHotel')}">Holiday Hotel</a></li>
            <li><a href="${createLink(controller: "package", action: 'listFlight')}">Holiday Flight</a></li>
        </ul>

        <div class="tab-content" style="margin-top: 1%">
            <div class="tab-pane active" id="editUser">
                <div class="col-md-12">
                    <div class="row">
                        <label>Category list:-</label>
                        <a href="${createLink(controller: 'holiday', action: 'createCategory')}"
                           class="btn btn-primary pull-right">Create Category</a>
                    </div>
                    <br>
                    <table class="table table-bordered table-condensed">

                        <tr>
                            <th>Category name</th>
                            <th>Image name</th>
                            <th>Date created</th>
                            <th>Last updated</th>
                            <th>Action</th>
                        </tr>
                        <g:each in="${holidayCategories}" var="category">
                            <tr>
                                <td>${category?.categoryName}</td>
                                <td>${category?.imageName}</td>
                                <td>${category?.dateCreated?.format('dd/MM/yyyy')}</td>
                                <td>${category?.lastUpdated?.format('dd/MM/yyyy')}</td>
                                <td><a href="${createLink(controller: 'holiday', action: 'editCategory', params: [categoryId: category?.id])}"
                                       class="btn btn-xs btn-primary">Edit</a>
                                    <a href="${createLink(controller: 'holiday', action: 'deleteCategory', params: [categoryId: category?.id])}"
                                       class="btn btn-xs btn-danger">Delete</a>
                                </td>
                            </tr>
                        </g:each>

                    </table>

                </div>

            </div>


            <div class="tab-pane" id="createNewUser">
                <p>Coming soon.</p>
            </div>

            <div class="tab-pane" id="tab3">
                <p>Coming soon.</p>
            </div>

            <div class="tab-pane" id="tab4">
                <p>Coming soon.</p>
            </div>
        </div>

    </div>

</div>

</body>
</html>