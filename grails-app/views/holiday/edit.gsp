<%@ page import="com.travelex.admin.holiday.HolidayType; com.travelex.admin.holiday.HolidayStatus" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="admin">
    <script src="${resource(dir: 'js', file: 'bootstrap-datepicker.js')}"></script>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'datepicker.css')}">
</head>

<body>

<div class="container" style="margin-bottom: 5%">
<g:form controller="holiday" action="update" enctype="multipart/form-data">
<g:hasErrors bean="${holidayPackageCO}">
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span
                aria-hidden="true">&times;
        </span><span class="sr-only">Close</span></button>

        <div id="errors" class="alert alert-error">
            <g:renderErrors bean="${holidayPackageCO}"/>
        </div>
    </div>
</g:hasErrors>
<div class="col-md-4">
    <div class="row">
        <div class="col-md-4">
            <label>Category</label>
        </div>

        <div class="col-md-8 form-group input-group">
            <g:select name="categoryId" from="${holidayCategoryList}" optionKey="id" optionValue="categoryName"
                      class="form-control" value="${holidayPackage?.holidayCategory?.categoryName}"/>
            <div class="input-group-addon">
                <travelx:star/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <label>Holiday name</label>
        </div>

        <div class="col-md-8 form-group input-group">
            <input type="text" class="form-control" name="holidayName" value="${holidayPackage?.holidayName}"/>

            <div class="input-group-addon">
                <travelx:star/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <label>Price range</label>
        </div>

        <div class="col-md-8 form-group input-group">
            <input type="text" class="form-control" name="priceRange" value="${holidayPackage?.priceRange}"/>

            <div class="input-group-addon">
                <travelx:star/>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-4">
            <label>Total seat</label>
        </div>

        <div class="col-md-8 form-group input-group">
            <input type="text" class="form-control" name="totalSeat" value="${holidayPackage?.totalSeat}"/>

            <div class="input-group-addon">
                <travelx:star/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <label>Holiday Image</label>
        </div>

        <div class="col-md-8">
            <input type="file" class="form-control" name="holidayImageName"/>
        </div>
    </div>
</div>

<div class="col-md-4">
    <div class="row">
        <div class="col-md-4">
            <label>Start Date</label>
        </div>

        <div class="col-md-8 form-group input-group">
            <input type="text" class="form-control" name="startDate" id="startDate" autocomplete="off"
                   onclick="showDatePicker('startDate')"
                   value="${holidayPackage?.startDate?.format('MM/dd/yyyy')}"/>

            <div class="input-group-addon">
                <travelx:star/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <label>End Date</label>
        </div>

        <div class="col-md-8 form-group input-group">
            <input type="text" class="form-control" name="endDate" id="endDate" autocomplete="off"
                   onclick="showDatePicker('endDate')"
                   value="${holidayPackage?.endDate?.format('MM/dd/yyyy')}"/>

            <div class="input-group-addon">
                <travelx:star/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <label>Status</label>
        </div>

        <div class="col-md-8 form-group input-group">
            <g:select name="status" class="form-control" from="${HolidayStatus.list()}"
                      value="${holidayPackage?.holidayStatus}"/>

            <div class="input-group-addon">
                <travelx:star/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <label>Holiday Type</label>
        </div>

        <div class="col-md-8 form-group input-group">
            <g:select name="type" class="form-control" from="${HolidayType.list()}"
                      value="${holidayPackage?.holidayType}"/>

            <div class="input-group-addon">
                <travelx:star/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">
            <label>Visa Includes</label>
        </div>

        <div class="col-md-7 ">
            <g:if test="${holidayPackage?.visaIncluded}">
                <input type="checkbox" name="visaIncluded" checked="true">
            </g:if>
            <g:else>
                <input type="checkbox" name="visaIncluded">
            </g:else>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">
            <label>Visa On Arrival</label>
        </div>

        <div class="col-md-7">
            <g:if test="${holidayPackage?.visaOnArrival}">
                <input type="checkbox" name="visaOnArrival" checked="true"/>
            </g:if>
            <g:else>
                <input type="checkbox" name="visaOnArrival"/>
            </g:else>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">
            <label>Travel Insurance</label>
        </div>

        <div class="col-md-7">
            <g:if test="${holidayPackage?.travelInsurance}">
                <input type="checkbox" name="travelInsurance" checked="true"/>
            </g:if>
            <g:else>
                <input type="checkbox" name="travelInsurance"/>
            </g:else>
        </div>
    </div>

</div>
<g:hiddenField name="holidayPackageId" value="${holidayPackage?.id}"/>

<input type="submit" class="btn btn-primary" value="Update"/>
</g:form>
<br>
<g:link controller="holiday" action="index" class="btn btn-primary">Back</g:link>

</div>
<script>
    $(document).ready(function () {

        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#startDate').datepicker({
            onRender: function (date) {
                return date.valueOf() < now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function (ev) {
            if (ev.date.valueOf() > checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
            }
            checkin.hide();
            $('#endDate')[0].focus();
        }).data('datepicker');
        var checkout = $('#endDate').datepicker({
            endDate: '+0d',
            onRender: function (date) {
                return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function (ev) {
            if (ev.date.valueOf() > checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
            }
            checkout.hide();
        }).data('datepicker');
    })
    function showDatePicker(type) {
        if (type == 'startDate') {
            $("#startDate").datepicker('show');
        }
        if (type == 'endDate') {
            $("#endDate").datepicker('show');
        }
    }
</script>

</body>
</html>