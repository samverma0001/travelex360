<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'bootstrap.min.css')}" type="text/css">
    <link rel="shortcut icon" href="${resource(dir: '/images/appimages', file: 'loginImage.png')}" type="text/x-icon">
    <title><g:layoutTitle default="Travelex 360"/></title>
    <g:layoutHead/>
    <r:layoutResources/>
    <style type="text/css">

    body {
        padding: 0px;
        margin: 0px;
    }

    .footer {
        width: 100%;
        bottom: 0px;
    }

    .footer1 {
        height: 40px;
        position: relative;
        z-index: 1;
    }
    </style>

</head>

<body>
<br>
<br>
<br>
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="${createLink(uri: '/')}"><img
                src="${resource(dir: 'images/appimages', file: 'appLogo.png')}" height="50px"/>
        </a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
            <sec:ifLoggedIn>
                <br>
                <li>
                    %{--<a href="${createLink(controller: 'home', action: 'index')}"><i--}%
                    %{--class="fa fa-user"></i>&nbsp;&nbsp;Profile</a>--}%
                    <a href="${createLink(controller: 'home', action: 'index')}" id="homeId" data-toggle="tooltip"
                       data-placement="top" title="My Account"><i
                            class="glyphicon glyphicon-home"></i></a>
                </li>
            %{--<li><g:link controller="plan" action="index"><i class="fa fa-calendar"></i>&nbsp;&nbsp;Calender</g:link>--}%
            %{--</li>--}%
                <li><g:link controller="logout" data-toggle="tooltip" data-placement="top"
                            title="Log Out"><span
                            class="glyphicon glyphicon-log-out"></span></g:link></li>
            </sec:ifLoggedIn>
            <sec:ifNotLoggedIn>
                <br>
                <li><g:link controller="signIn" action="index" data-toggle="tooltip" data-placement="top"
                            title="Register/Sign Up"><span
                            class="glyphicon glyphicon-registration-mark"></span></g:link></li>
                <li><g:link controller="login" action="auth" data-toggle="tooltip" data-placement="top" title="Log In"
                            id="loginForm"><span
                            class="glyphicon glyphicon-log-in"></span></g:link></li>
            </sec:ifNotLoggedIn>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>
<g:layoutBody/>

<g:javascript library="application"/>
<r:layoutResources/>

</body>

<div class="footer" role="contentinfo">
    <nav class="navbar navbar-default" role="navigation" style="margin-bottom: -25px;">
        <table class="table  table-responsive table-condensed table-stripped table-bordered">
            <tr>
                <td class="pull-left"><a href="${createLink(controller: 'home', action: 'aboutUs')}"
                                         style="color: #000000"><strong>About Us</strong></a></td>
                <td class="pull-left"><a href="${createLink(controller: 'home', action: 'howItWork')}"
                                         style="color: #000000"><strong>How It Work's</strong></a>
                </td>
                <td class="pull-left"><a href="${createLink(controller: 'home', action: 'termsOfUse')}"
                                         style="color: #000000"><strong>Terms Of Use</strong></a>
                </td>
                <td class="pull-left"><a href="${createLink(controller: 'home', action: 'contactUs')}"
                                         style="color: #000000"><strong>Contact Us</strong></a>
                </td>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <td class="pull-right"><strong>Copyright &copy; Travelex 360 Pvt.Ltd. 2014, All rights reserved.</strong>
                </td>
                <td class="pull-right"><a
                        href="${createLink(controller: 'home', action: 'privacyPolicy')}"
                        style="color: #000000"><strong>Privacy Policy</strong></a></td>
                <td class="pull-right"><a href="" style="color: #000000"><strong>Post Your Complain</strong></a></td>

            </tr>
        </table>
        <img src="${resource(dir: 'images/appimages', file: 'Securedby_Type-2.jpg')}" height="60"/>

    </nav>
</div>

</html>