<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title><g:layoutTitle default=".:: Travelex360 ::."/></title>
    <link href="${resource(dir: 'css', file: 'mainNew.css')}" rel="stylesheet">
    <link href="${resource(dir: 'css', file: 'datepicker3.css')}" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="${resource(dir: 'css', file: 'jquery.selectBoxIt.css')}"/>


    <link href="${resource(dir: 'css', file: 'owl.carousel.css')}" rel="stylesheet">
    <link href="${resource(dir: 'css', file: 'owl.theme.css')}" rel="stylesheet">
    <link href="${resource(dir: 'css', file: 'jquery.bootstrap-touchspin.css')}" rel="stylesheet">
    <link href="${resource(dir: 'css', file: 'slider.css')}" rel="stylesheet">

    <link href="${resource(dir: 'css', file: 'style.css')}" rel="stylesheet">
    <link href="${resource(dir: 'css', file: 'theme.css')}" rel="stylesheet">
    <link href="${resource(dir: 'css', file: 'media.css')}" rel="stylesheet">
    <script src="${resource(dir: 'js', file: 'ie8-responsive-file-warning.js')}"></script>
    <script src="${resource(dir: 'js', file: 'ie-emulation-modes-warning.js')}"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <g:layoutHead/>
    <r:layoutResources/>
</head>

<body class="full-height">
<header class="main-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-sm-7 col-xs-6">
                <div class="logo pull-left">
                    <a href="${createLink(uri: '/')}">
                        <img src="${resource(dir: 'images', file: 'logo.png')}" width="190px">
                    </a>
                </div>
                <div class="main-icons-container-header pull-left">
                    <g:render template="/appTemplate/menuTemplate"/>
                </div>
            </div>

            <div class="col-md-2 col-sm-3 col-xs-6 text-right">
                <div class="header-right">
                    <ul class="header-icon-nav">
                        <sec:ifLoggedIn>
                            <li>
                                <a href="${createLink(controller: 'home', action: 'index')}" id="homeId"
                                   data-toggle="tooltip"
                                   data-placement="top" title="My Account"><i
                                        class="glyphicon glyphicon-home"></i></a>
                            </li>
                            <li><g:link controller="logout" data-toggle="tooltip" data-placement="top"
                                        title="Log Out"><span
                                        class="glyphicon glyphicon-log-out"></span></g:link></li>
                        </sec:ifLoggedIn>
                        <sec:ifNotLoggedIn>
                            <li><a href="${createLink(controller: 'signIn', action: 'index')}"><img
                                    src="${resource(dir: 'images', file: 'register-icon.png')}"></a></li>
                            <li><a href="${createLink(controller: 'login', action: 'auth')}"><img
                                    src="${resource(dir: 'images', file: 'login-icon.png')}"></a></li>
                        </sec:ifNotLoggedIn>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<g:layoutBody/>
</body>
<g:javascript library="application"/>
<r:layoutResources/>
<footer class="main-footer">
    <div class="container-fluid">
        <div class="footer-links">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <ul class="inline-list-links">
                        <li><a href="${createLink(controller: 'home', action: 'aboutUs')}">About Us</a></li>
                        <li><a href="${createLink(controller: 'home', action: 'howItWork')}">How It Work's</a></li>
                        <li><a href="${createLink(controller: 'home', action: 'termsOfUse')}">Terms Of Use</a></li>
                        <li><a href="${createLink(controller: 'home', action: 'contactUs')}">Contact Us</a></li>
                    </ul>
                </div>

                <div class="col-md-6 col-sm-6 text-right">
                    Copyright &copy; Travelex 360 Pvt.Ltd. 2014, All rights reserved
                </div>
            </div>
        </div>

        <div class="footer-rights">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <ul class="inline-list-links margin-top-5">
                        <li><a href="#">Post Your Complaint</a></li>
                        <li><a href="${createLink(controller: 'home', action: 'privacyPolicy')}">Privacy Policy</a></li>
                        <li><a href="#">FAQ's</a></li>
                        <li><a href="#">Site Map</a></li>
                    </ul>
                </div>

                <div class="col-md-6 col-sm-6 text-right">
                    <img src="${resource(dir: 'images', file: 'payment-methods.jpg')}">
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- // <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> -->
<script src="${resource(dir: 'js', file: 'script.min.js')}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28/angular.js"></script>
<script src="${resource(dir: 'js', file: 'ang_main.js')}"></script>
<script src="${resource(dir: 'js', file: 'ie10-viewport-bug-workaround.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery-ui.min.js')}"></script>
<script src="${resource(dir: 'js', file: 'datepicker.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery.selectBoxIt.min.js')}"></script>
<script src="${resource(dir: 'js', file: 'owl.carousel.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery.bootstrap-touchspin.js')}"></script>
<script src="${resource(dir: 'js', file: 'packages-map.js')}"></script>
<script src="${resource(dir: 'js', file: 'bootstrap-slider.js')}"></script>
<script src="${resource(dir: 'js', file: 'main.js')}"></script>
</html>
