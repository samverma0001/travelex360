<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><g:layoutTitle default="Travelex"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'bootstrap.min.css')}" type="text/css">
    <script src="${resource(dir: 'js', file: 'jquery-1.10.2.js')}"></script>
    <script src="${resource(dir: 'js', file: 'jquery-ui.js')}"></script>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'jquery-ui.css')}">

    <link rel="stylesheet" href="${resource(dir: 'assets/css', file: 'font-awesome.min.css')}" type="text/css">
    <link rel="shortcut icon" href="${resource(dir: '/images/appimages', file: 'loginImage.png')}" type="text/x-icon">
    <title><g:layoutTitle default="Travelex 360"/></title>
    <g:layoutHead/>
    <r:layoutResources/>
</head>

<body>
<div class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${createLink(uri: '/')}"><img
                    src="${resource(dir: 'images/appimages', file: 'appLogo.png')}" height="40px"/>
            </a>
        </div>

        <div class="navbar-collapse collapse">
            %{--<ul class="nav navbar-nav">--}%
            %{--<li><a  href="${createLink(controller: 'bookingHistory', action: 'index')}">Booking List</a></li>--}%
            %{--<li><a href="${createLink(controller: 'coupon', action: 'index')}">Coupon List</a></li>--}%
            %{--<li><a href="${createLink(controller: "paymentInfo", action: "index")}">Payment List</a></li>--}%
            %{--<li><a href="${createLink(controller: "setMargin", action: "index")}">Margin List</a></li>--}%
            %{--</ul>--}%
            <ul class="nav navbar-nav navbar-right">
                <sec:ifLoggedIn>

                    <li><g:link controller="home"><i class="fa fa-user"></i>&nbsp;&nbsp;Profile</g:link></li>
                    <li><g:link controller="plan" action="index"><i
                            class="fa fa-calendar"></i>&nbsp;&nbsp;Calender</g:link>
                    </li>
                    <li><g:link controller="logout"><i class="fa fa-sign-out"></i>&nbsp;&nbsp;Logout</g:link></li>
                </sec:ifLoggedIn>
                <sec:ifNotLoggedIn>
                    <li><g:link controller="signIn" action="index">SignUp</g:link></li>
                    <li><g:link controller="login" action="auth">Login</g:link></li>
                </sec:ifNotLoggedIn>
            </ul>
        </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
</div>
<g:layoutBody/>
</body>
<g:javascript library="application"/>
<r:layoutResources/>


<div class="footer" role="contentinfo">
    <nav class="navbar navbar-default navbar-fixed-bottom" role="navigation" style="margin-bottom: -25px;">
        <table class="table  table-responsive table-condensed table-stripped table-bordered">
            <tr>
                <td class="pull-left"><a href="${createLink(controller: 'home', action: 'aboutUs')}">About Us</a></td>
                <td class="pull-left"><a href="${createLink(controller: 'home', action: 'howItWork')}">How It Work's</a>
                </td>
                <td class="pull-left"><a href="${createLink(controller: 'home', action: 'termsOfUse')}">Terms Of Use</a>
                </td>
                <td class="pull-left"><a href="${createLink(controller: 'home', action: 'contactUs')}">Contact Us</a>
                </td>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <td class="pull-right">Copyright &copy; Travelex 360 Pvt.Ltd. 2014, All rights reserved.
                </td>
                <td class="pull-right"><a
                        href="${createLink(controller: 'home', action: 'privacyPolicy')}">Privacy Policy</a></td>
                <td class="pull-right"><a href="">Post Your Complain</a></td>

            </tr>
        </table>
    </nav>
    <script src="${resource(dir: 'assets/js', file: 'libs/jquery-ui-1.9.2.custom.min.js')}"></script>
    <script src="${resource(dir: 'assets/js', file: 'libs/bootstrap.min.js')}"></script>
    %{--<script src="${resource(dir: 'assets/js', file: 'demos/dashboard.js')}"></script>--}%
</div>
</html>