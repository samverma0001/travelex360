<%@ page import="com.travelex.admin.SetMargin" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin">
</head>

<body>

<div class="container">
    <div class="row">
        <div class="span12">
            <div id="tab" class="btn-group">
                <a href="${createLink(controller: 'user', action: 'index')}" id="userId"
                   class="btn btn-large btn-info  disableAll"
                   onclick="activeButton(this.id)">Users</a>
                <a href="${createLink(controller: 'bookingHistory', action: 'index')}" id="hotelId"
                   class="btn btn-large btn-info active disableAll"
                   onclick="activeButton(this.id)">Hotels</a>
                <a href="#Flights" id="flightId" class="btn btn-large btn-info disableAll" data-toggle="tab"
                   onclick="activeButton(this.id)">Flights</a>
                <a href="#Activity" id="activityId" class="btn btn-large btn-info disableAll" data-toggle="tab"
                   onclick="activeButton(this.id)">Activity</a>
                <a href="#serviceProvider" id="serviceProviderId" class="btn btn-large btn-info disableAll"
                   data-toggle="tab"
                   onclick="activeButton(this.id)">Service Provider</a>

            </div>


            <div class="tab-content" style="margin-top: 1%">

                <div class="tab-pane active" id="Hotels">

                    <ul class="nav nav-tabs">
                        <li><a
                                href="${createLink(controller: 'bookingHistory', action: 'index')}">Booking List</a>
                        </li>
                        <li><a
                                href="${createLink(controller: 'coupon', action: 'index')}">Coupon List</a></li>
                        <li><a
                                href="${createLink(controller: 'paymentInfo', action: 'index')}">Payment List</a></li>
                        <li class="active"><a
                                href="${createLink(controller: 'setMargin', action: 'index')}">Margin List</a></li>
                    </ul>

                    <div class="tab-content" style="margin-top: 1%">
                        <div class="tab-pane active">
                            <table class="table table-bordered table-condensed">
                                <thead>
                                <tr>
                                    <th><strong>Dom. Hotel Margin</strong></th>
                                    <th><strong>Valid From</strong></th>
                                    <th><strong>Valid From</strong></th>
                                    <th><strong>Amount Type</strong></th>
                                    <th><strong>Margin Type</strong></th>
                                    <th><strong>PayUMoney Stake</strong></th>
                                    <th><strong>Status</strong></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>${setMargin?.marginMoney}</td>
                                    <td>${setMargin?.validFrom?.format('dd/MM/yyyy')}</td>
                                    <td>${setMargin?.validTo?.format('dd/MM/yyyy')}</td>
                                    <td>${setMargin?.amountType}</td>
                                    <td>${setMargin?.marginType}</td>
                                    <td>${setMargin?.payUMoneyStake}</td>
                                    <td>${setMargin?.status}</td>
                                </tr>
                                </tbody>
                            </table>

                            <div class="row">
                                <g:form url="[resource: setMargin, action: 'delete']" method="DELETE">
                                    <fieldset class="buttons">
                                        <g:link class="edit col-md-offset-4 btn btn-primary" action="edit"
                                                resource="${setMargin}"><g:message
                                                code="default.button.edit.label" default="Edit"/></g:link>
                                        <g:actionSubmit class="delete  btn btn-danger" action="delete"
                                                        value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                                        onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                                    </fieldset>
                                </g:form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
