<%@ page import="com.travelex.admin.AmountType; com.travelex.admin.Status; com.travelex.admin.MarginType; com.travelex.admin.SetMargin" %>
<div class="col-md-2"></div>

<div class="col-md-8">
    <div class="col-md-6">
        <div class="row" style="margin-top: 2%">
            <div class="col-md-4">
                <strong>Margin<travelx:star/></strong>
            </div>

            <div class="col-md-8">
                <g:textField name="marginMoney" class="form-control"
                             value="${domHotelMarginVO?.marginMoney}"/>
            </div>
        </div>

        <div class="row" style="margin-top: 2%">
            <div class="col-md-4">
                <strong>Valid From<travelx:star/></strong>
            </div>

            <div class="col-md-8">
                <input class="form-control" autocomplete="off"
                       name="validFrom" type="text"
                       id="dpStart" value="${domHotelMarginVO?.validFrom?.format('dd/MM/yyyy')}"
                       onclick="showDatePicker('validFrom')"/>
            </div>
        </div>

        <div class="row" style="margin-top: 2%">
            <div class="col-md-4">
                <strong>Valid Upto<travelx:star/></strong>
            </div>

            <div class="col-md-8">
                <input class="form-control" autocomplete="off"
                       name="validTo" type="text"
                       id="dpEnd" value="${domHotelMarginVO?.validTo?.format('dd/MM/yyyy')}"
                       onclick="showDatePicker('validTo')"/>
            </div>
        </div>

        <div class="row" style="margin-top: 2%">
            <div class="col-md-4">
                <strong>PayUMoney Stake<travelx:star/></strong>
            </div>

            <div class="col-md-8 input-group">
                <g:textField name="payUMoneyStake" class="form-control"
                             value="${domHotelMarginVO?.payUMoneyStake}"/>
                <div class="input-group-addon">
                    %
                </div>
            </div>

        </div>
    </div>

    <div class="col-md-6">
        <div class="row" style="margin-top: 2%">
            <div class="col-md-4">
                <strong>Amount Type</strong>
            </div>

            <div class="col-md-8">
                <g:select name="amountType" from="${AmountType?.values()}"
                          keys="${AmountType.values()*.name()}" required=""
                          value="${domHotelMarginVO?.amountType}" class="form-control"/>
            </div>
        </div>

        <div class="row" style="margin-top: 2%">
            <div class="col-md-4">
                <strong>Status</strong>
            </div>

            <div class="col-md-8">
                <g:select name="status" from="${Status?.values()}"
                          keys="${Status.values()*.name()}" required=""
                          value="${domHotelMarginVO?.status}" class="form-control"/>
            </div>
        </div>

        <div class="row" style="margin-top: 2%">
            <div class="col-md-4">
                <strong>Margin Type</strong>
            </div>

            <div class="col-md-8">
                <g:select name="marginType" from="${MarginType.values()}"
                          keys="${MarginType.values()*.name()}" required=""
                          value="${domHotelMarginVO?.marginType}" class="form-control"/>
            </div>
        </div>
    </div>
</div>

<div class="col-md-2"></div>
<script>
    function showDatePicker(type) {
        if (type == 'validFrom') {
            $("#dpStart").datepicker('show');
        }
        if (type == 'validTo') {
            $("#dpEnd").datepicker('show');
        }
    }
</script>