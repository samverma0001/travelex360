<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin">
    <script src="${resource(dir: 'js', file: 'bootstrap-datepicker.js')}"></script>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'datepicker.css')}">
</head>

<body>

<div class="container">
    <div class="row">
        <div class="span12">
            <div id="tab" class="btn-group">
                <a href="${createLink(controller: 'user', action: 'index')}" id="userId"
                   class="btn btn-large btn-info  disableAll"
                   onclick="activeButton(this.id)">Users</a>
                <a href="${createLink(controller: 'bookingHistory', action: 'index')}" id="hotelId"
                   class="btn btn-large btn-info active disableAll"
                   onclick="activeButton(this.id)">Hotels</a>
                <a href="#Flights" id="flightId" class="btn btn-large btn-info disableAll" data-toggle="tab"
                   onclick="activeButton(this.id)">Flights</a>
                <a href="#Activity" id="activityId" class="btn btn-large btn-info disableAll" data-toggle="tab"
                   onclick="activeButton(this.id)">Activity</a>
                <a href="#serviceProvider" id="serviceProviderId" class="btn btn-large btn-info disableAll"
                   data-toggle="tab"
                   onclick="activeButton(this.id)">Service Provider</a>

            </div>


            <div class="tab-content" style="margin-top: 1%">

                <div class="tab-pane active" id="Hotels">

                    <ul class="nav nav-tabs">
                        <li class="active"><a
                                href="${createLink(controller: 'setMargin', action: 'create')}">Create New</a>
                        </li>
                        <li><a
                                href="${createLink(controller: 'setMargin', action: 'index')}">Margin List</a></li>
                    </ul>

                    <div class="tab-content" style="margin-top: 1%">
                        <div class="tab-pane active">
                            <g:hasErrors bean="${domHotelMarginVO}">
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert"><span
                                            aria-hidden="true">&times;
                                    </span><span class="sr-only">Close</span></button>

                                    <div id="errors" class="alert alert-error">
                                        <g:renderErrors bean="${domHotelMarginVO}"></g:renderErrors>
                                    </div>
                                </div>
                            </g:hasErrors>

                            <g:form url="[resource: setMarginInstance, action: 'save']">
                                <fieldset class="form">
                                    <g:render template="form" model="[domHotelMarginVO: domHotelMarginVO]"/>
                                </fieldset>

                                <div class="col-md-4"></div>

                                <div class="col-md-8">

                                    <fieldset class="buttons" style="margin-top: 2%">
                                        <g:submitButton name="create"
                                                        class="save btn btn-primary col-md-offset-6 col-md-3 "
                                                        value="${message(code: 'default.button.create.label', default: 'Create')}"/>
                                    </fieldset>
                                </div>
                            </g:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#dpStart').datepicker({
            onRender: function (date) {
                return date.valueOf() < now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function (ev) {
            if (ev.date.valueOf() > checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
            }
            checkin.hide();
            $('#dpEnd')[0].focus();
        }).data('datepicker');
        var checkout = $('#dpEnd').datepicker({
            onRender: function (date) {
                return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function (ev) {
            checkout.hide();
        }).data('datepicker');
    });
</script>
</body>
</html>
