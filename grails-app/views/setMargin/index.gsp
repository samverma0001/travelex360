<%@ page import="com.travelex.admin.SetMargin" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin">
</head>

<body>

<div class="container">
    <div class="row">
        <div class="span12">
            <div id="tab" class="btn-group">
                <a href="${createLink(controller: 'user', action: 'index')}" id="userId"
                   class="btn btn-large btn-info  disableAll"
                   onclick="activeButton(this.id)">Users</a>
                <a href="${createLink(controller: 'bookingHistory', action: 'index')}" id="hotelId"
                   class="btn btn-large btn-info active disableAll"
                   onclick="activeButton(this.id)">Hotels</a>
                <a href="#Flights" id="flightId" class="btn btn-large btn-info disableAll" data-toggle="tab"
                   onclick="activeButton(this.id)">Flights</a>
                <a href="#Activity" id="activityId" class="btn btn-large btn-info disableAll" data-toggle="tab"
                   onclick="activeButton(this.id)">Activity</a>
                <a href="#serviceProvider" id="serviceProviderId" class="btn btn-large btn-info disableAll"
                   data-toggle="tab"
                   onclick="activeButton(this.id)">Service Provider</a>

            </div>


            <div class="tab-content" style="margin-top: 1%">

                <div class="tab-pane active" id="Hotels">

                    <ul class="nav nav-tabs">
                        <li><a
                                href="${createLink(controller: 'bookingHistory', action: 'index')}">Booking List</a>
                        </li>
                        <li><a
                                href="${createLink(controller: 'coupon', action: 'index')}">Coupon List</a></li>
                        <li><a
                                href="${createLink(controller: 'paymentInfo', action: 'index')}">Payment List</a></li>
                        <li class="active"><a
                                href="${createLink(controller: 'setMargin', action: 'index')}">Margin List</a></li>
                    </ul>

                    <div class="tab-content" style="margin-top: 1%">
                        <div class="tab-pane active">
                            <p><a href="${createLink(controller: 'setMargin', action: 'create')}"
                                  class="btn btn-primary pull-right">Create New Margin</a></p>
                            <br>
                            <br>
                            <br>

                            <table class="table table-bordered table-condensed">
                                <thead>
                                <tr>
                                    <g:sortableColumn property="marginMoneyOnHotel"
                                                      title="${message(code: 'setMargin.marginMoneyOnHotel.label', default: 'Margin Money')}"/>
                                    <g:sortableColumn property="amountType"
                                                      title="${message(code: 'setMargin.amountType.label', default: 'Amount Type')}"/>
                                    <g:sortableColumn property="marginType"
                                                      title="${message(code: 'setMargin.marginType.label', default: 'Margin Type')}"/>
                                    <g:sortableColumn property="status"
                                                      title="${message(code: 'setMargin.status.label', default: 'Status')}"/>
                                    <g:sortableColumn property="validFrom"
                                                      title="${message(code: 'setMargin.validFrom.label', default: 'Valid From')}"/>
                                    <g:sortableColumn property="validTo"
                                                      title="${message(code: 'setMargin.validTo.label', default: 'Valid To')}"/>
                                    <g:sortableColumn property="payUMoneyStake"
                                                      title="${message(code: 'setMargin.payUMoneyStake.label', default: 'Pay U Money Stake')}"/>
                                </tr>
                                </thead>
                                <tbody>
                                <g:each in="${setMarginList}" var="setMarginInstance">
                                    <tr>
                                        <td><g:link action="show" controller="setMargin"
                                                    params="[setMarginInstance: setMarginInstance?.id]">${fieldValue(bean: setMarginInstance, field: "marginMoney")}</g:link></td>
                                        <td>${fieldValue(bean: setMarginInstance, field: "amountType")}</td>
                                        <td>${fieldValue(bean: setMarginInstance, field: "marginType")}</td>
                                        <td>${fieldValue(bean: setMarginInstance, field: "status")}</td>
                                        <td>${setMarginInstance?.validFrom?.format('dd/MM/yyyy')}</td>
                                        <td>${setMarginInstance?.validTo?.format('dd/MM/yyyy')}</td>
                                        <td>${setMarginInstance?.payUMoneyStake}</td>
                                    </tr>
                                </g:each>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
