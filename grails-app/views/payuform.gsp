<html>
<head>
    <meta name="layout" content="main"/>
</head>

<body>
<br>
<br>
<br>
<br>
<br>

<div class="row" style="margin-bottom: 30%">
    <div class="col-md-1"></div>

    <div class="col-md-10">
        %{--<form class="postForm" action="https://test.payu.in/_payment" method="post" name="payuForm">--}%
        %{--<input type="hidden" name="key" value="JBZaLc"/>--}%
        <form class="postForm" action="https://secure.payu.in/_payment" method="post" name="payuForm">
            <input type="hidden" name="key" value="wO0qly"/>

            <input type="hidden" name="txnid"
                   value="${tranId}"/>
            <table class="table table-bordered table-stripped table-condensed" style="border-color: lightskyblue">
                <tr>
                    <td colspan="12" style="background-color: dodgerblue"
                        class="text-center"><b><span12>Booking summary</span12></b></td>
                </tr>
                <g:hiddenField name="amount" value="${total}"/>
                <g:hiddenField name="firstname" value="${fName}"/>
                <g:hiddenField name="email" value="${email}"/>
                <g:hiddenField name="phone" value="${phone}"/>
                <g:hiddenField name="productinfo" value="${productInfo}"/>
                <g:hiddenField name="surl" value="${serverUrl}/hotelService/processBooking" size="64"/>
                <g:hiddenField name="furl" value="${serverUrl}" size="64"/>
                <g:hiddenField name="service_provider" value="payu_paisa" size="64"/>

                <tr>
                    <td><strong>Amount:</strong></td>
                    <td><strong>${totalAmount}</strong></td>
                    <td><strong>Payment Gateway Service Charge</strong></td>
                    <td><strong>${payUMoneyStake}</strong></td>
                </tr>
                <tr>
                    <td>First Name:</td>
                    <td id="firstname">${fName}</td>
                    <td>Last Name</td>
                    <td>${lName}</td>
                </tr>

                <tr>
                    <td>Email:</td>
                    <td id="email">${email}</td>
                    <td>Phone:</td>
                    <td>${phone}</td>
                </tr>
                <tr>
                    <td>Booking Info:</td>
                    <td>${productInfo}&nbsp;&nbsp; * &nbsp;&nbsp;${noOfRoom} Room(s)</td>
                    <td><strong>Total Pay</strong></td>
                    <td><strong>${total}</strong></td>
                </tr>
                <input type="hidden" name="hash" value="${hash}"/>

                <tr>
                    <td colspan="4"><input type="submit" id="submitForm" value="Pay Now"
                                           class="btn col-md-offset-9 col-md-2 btn-xs btn-primary"/>
                    </td>
                </tr>
            </table>
        </form>

    </div>

    <div class="col-md-1"></div>

</div>
</body>
</html>