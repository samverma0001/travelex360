<%@ page import="com.travelex.User.User" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin"/>
</head>

<body>
<travelx:renderAlert/>
<travelx:adminUserTab/>
<div class="tab-content" style="margin-top: 1%">

    <div class="tab-pane active" id="Users" style="margin-left: 5%">

        <ul class="nav nav-tabs">
            <li><a href="${createLink(controller: 'user', action: 'index')}">User List</a></li>
            <li><a href="${createLink(controller: 'user', action: 'create')}">Create New</a></li>
            <li class="active"><a href="#showUser" data-toggle="tab">Details</a></li>

        </ul>

        <div class="tab-content" style="margin-top: 1%">
            <div class="tab-pane active" id="showUser">
                <table class="table table-bordered table-condensed">
                    <tr>
                        <th>User Name</th>
                        <td>${userInstance?.username}</td>
                    </tr>
                    <tr>
                        <th>First Name</th>
                        <td>${userInstance?.firstName}</td>
                    </tr>
                    <tr>
                        <th>Last Name</th>
                        <td>${userInstance?.lastName}</td>
                    </tr>
                    <tr>
                        <th>Contact No.</th>
                        <td>${userInstance?.mobNo}</td>
                    </tr>
                    <tr>
                        <th>Email Verified</th>
                        <td>${userInstance?.hasVerifiedEmail}</td>
                    </tr>
                    <tr>
                        <th>Enabled</th>
                        <td>${userInstance?.enabled}</td>
                    </tr>
                    <tr>
                        <th>Account Expired</th>
                        <td>${userInstance?.accountExpired}</td>
                    </tr>
                    <tr>
                        <th>Account Locked</th>
                        <td>${userInstance?.accountLocked}</td>
                    </tr>
                    <tr>
                        <th>Password Expired</th>
                        <td>${userInstance?.passwordExpired}</td>
                    </tr>
                    <tr>
                        <th>Ip Address</th>
                        <td>${userInstance?.ipAddress}</td>
                    </tr>
                    <tr>
                        <th>Profile Pic Name</th>
                        <td>${userInstance?.pictureName}</td>
                    </tr>
                    <tr>
                        <th>Role</th>
                        <td>${role}</td>
                    </tr>
                    <tr>
                        <g:form>
                            <fieldset class="buttons">
                                <g:hiddenField name="id" value="${userInstance?.id}"/>
                                <g:link class="btn btn-primary edit pull-right" action="edit"
                                        id="${userInstance?.id}"><g:message
                                        code="default.button.edit.label"
                                        default="Edit"/></g:link>
                                <g:actionSubmit class="btn btn-danger delete pull-right" action="delete"
                                                value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                            </fieldset>
                        </g:form>
                    </tr>
                    <br>
                </table>
            </div>


            <div class="tab-pane" id="createNewUser">
                <p>Coming soon.</p>
            </div>

            <div class="tab-pane" id="tab3">
                <p>Coming soon.</p>
            </div>

            <div class="tab-pane" id="tab4">
                <p>Coming soon.</p>
            </div>
        </div>

    </div>


    <div class="tab-pane" id="Hotels">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#hotelBookingList" data-toggle="tab">Booking List</a></li>
            <li><a href="#hotelPaymentList" data-toggle="tab">Payment List</a></li>
            <li><a href="#hotelCouponList" data-toggle="tab">Coupon List</a></li>
            <li><a href="#hotelMarginList" data-toggle="tab">Margin List</a></li>

        </ul>
    </div>
</div>
</body>
</html>
