<%@ page import="com.travelex.User.User" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin"/>
</head>

<body>
<travelx:adminUserTab/>
%{--<div class="container">--}%
    %{--<div class="row">--}%
        %{--<div class="span12">--}%
            %{--<div id="tab" class="btn-group">--}%
                %{--<a href="${createLink(controller: 'user', action: 'index')}" id="userId"--}%
                   %{--class="btn btn-large btn-info active disableAll"--}%
                   %{--onclick="activeButton(this.id)">Users</a>--}%
                %{--<a href="${createLink(controller: 'bookingHistory', action: 'index')}" id="hotelId"--}%
                   %{--class="btn btn-large btn-info disableAll"--}%
                   %{--onclick="activeButton(this.id)">Hotels</a>--}%
                %{--<a href="#Flights" id="flightId" class="btn btn-large btn-info disableAll"--}%
                   %{--onclick="activeButton(this.id)">Flights</a>--}%
                %{--<a href="#Activity" id="activityId" class="btn btn-large btn-info disableAll"--}%
                   %{--onclick="activeButton(this.id)">Activity</a>--}%
                %{--<a href="#serviceProvider" id="serviceProviderId" class="btn btn-large btn-info disableAll"--}%
                   %{--onclick="activeButton(this.id)">Service Provider</a>--}%
            %{--</div>--}%

            <div class="tab-content" style="margin-top: 1%">

                <div class="tab-pane active" id="Users" style="margin-left: 5%">

                    <ul class="nav nav-tabs">
                        <li><a href="${createLink(controller: 'user', action: 'index')}">User List</a></li>
                        <li><a href="${createLink(controller: 'user', action: 'create')}">Create New</a></li>
                        <li class="active"><a href="#editUser" data-toggle="tab">Edit User</a></li>
                    </ul>

                    <div class="tab-content" style="margin-top: 1%">
                        <div class="tab-pane active" id="editUser">
                            <div class="col-md-12">
                                <g:hasErrors bean="${signUpCO}">
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert"><span
                                                aria-hidden="true">&times;
                                        </span><span class="sr-only">Close</span></button>

                                        <div id="errors" class="alert alert-error">
                                            <g:renderErrors bean="${signUpCO}"></g:renderErrors>
                                        </div>
                                    </div>
                                </g:hasErrors>
                                <g:form method="post">
                                    <g:hiddenField name="id" value="${userInstance?.id}"/>
                                    <g:hiddenField name="version" value="${userInstance?.version}"/>
                                    <fieldset class="form">
                                        <g:render template="form" model="[userInstance: userInstance, role: role]"/>
                                    </fieldset>
                                    <fieldset class="buttons">
                                        <g:actionSubmit class="save btn btn-primary" action="update"
                                                        value="${message(code: 'default.button.update.label', default: 'Update')}"/>
                                        <g:actionSubmit class="delete btn btn-danger" action="delete"
                                                        value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                                        formnovalidate=""
                                                        onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                                    </fieldset>
                                </g:form>
                            </div>
                        </div>


                        <div class="tab-pane" id="createNewUser">
                            <p>Coming soon.</p>
                        </div>

                        <div class="tab-pane" id="tab3">
                            <p>Coming soon.</p>
                        </div>

                        <div class="tab-pane" id="tab4">
                            <p>Coming soon.</p>
                        </div>
                    </div>

                </div>


                <div class="tab-pane" id="Hotels">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#hotelBookingList" data-toggle="tab">Booking List</a></li>
                        <li><a href="#hotelPaymentList" data-toggle="tab">Payment List</a></li>
                        <li><a href="#hotelCouponList" data-toggle="tab">Coupon List</a></li>
                        <li><a href="#hotelMarginList" data-toggle="tab">Margin List</a></li>

                    </ul>
                </div>

                <div class="tab-pane" id="Flights">
                    <h4>Coming soon !</h4>
                </div>

                <div class="tab-pane" id="Activity">
                    <h4>Coming soon !</h4>
                </div>

                <div class="tab-pane" id="serviceProvider">
                    <h4>Coming soon !</h4>
                </div>
            </div>
        %{--</div>--}%
    %{--</div>--}%
%{--</div>--}%


%{--<script>--}%
    %{--function activeButton(id) {--}%
        %{--$(".disableAll").removeClass('active')--}%
    %{--}--}%
%{--//</script>--}%
</body>
</html>
