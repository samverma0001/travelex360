<%@ page import="com.travelex.User.User" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin">
</head>

<body>
<travelx:renderAlert/>
<travelx:adminUserTab/>
%{--<div class="container">--}%
%{--<div class="row">--}%
%{--<div class="span12">--}%
%{--<div id="tab" class="btn-group">--}%
%{--<a href="${createLink(controller: 'user', action: 'index')}" id="userId"--}%
%{--class="btn btn-large btn-info active disableAll"--}%
%{--onclick="activeButton(this.id)">Users</a>--}%
%{--<a href="${createLink(controller: 'bookingHistory', action: 'index')}" id="hotelId"--}%
%{--class="btn btn-large btn-info disableAll"--}%
%{--onclick="activeButton(this.id)">Hotels</a>--}%
%{--<a href="#Flights" id="flightId" class="btn btn-large btn-info disableAll" data-toggle="tab"--}%
%{--onclick="activeButton(this.id)">Flights</a>--}%
%{--<a href="#Activity" id="activityId" class="btn btn-large btn-info disableAll" data-toggle="tab"--}%
%{--onclick="activeButton(this.id)">Activity</a>--}%
%{--<a href="#serviceProvider" id="serviceProviderId" class="btn btn-large btn-info disableAll"--}%
%{--data-toggle="tab"--}%
%{--onclick="activeButton(this.id)">Service Provider</a>--}%

%{--</div>--}%


<div class="tab-content" style="margin-top: 1%">

    <div class="tab-pane active" id="Users" style="margin-left: 5%">

        <ul class="nav nav-tabs" >
            <li class="active"><a href="#list" data-toggle="tab">User List</a></li>
            <li><a href="${createLink(controller: 'user', action: 'create')}">Create New</a></li>
        </ul>

        <div class="tab-content" style="margin-top: 1%">
            <div class="tab-pane active" id="list">
                <table class="table table-bordered table-condensed">
                    <thead>
                    <tr>
                        <g:sortableColumn property="srNo" class="text-center"
                                          title="${message(code: 'user.srNo.label', default: 'Serial No.')}"/>
                        <g:sortableColumn property="username" class="text-center"
                                          title="${message(code: 'user.username.label', default: 'Username')}"/>

                        <g:sortableColumn property="firstName" class="text-center"
                                          title="${message(code: 'user.firstName.label', default: 'First Name')}"/>

                        <g:sortableColumn property="lastName" class="text-center"
                                          title="${message(code: 'user.lastName.label', default: 'Last Name')}"/>

                        <g:sortableColumn property="mobNo" class="text-center"
                                          title="${message(code: 'user.mobNo.label', default: 'Contact No.')}"/>

                        <g:sortableColumn property="hasVerifiedEmail" class="text-center"
                                          title="${message(code: 'user.hasVerifiedEmail.label', default: 'Email Verify')}"/>

                        <g:sortableColumn property="coustId" class="text-center"
                                          title="${message(code: 'user.coustId.label', default: 'Travelex User Id')}"/>


                        <g:sortableColumn property="action" class="text-center"
                                          title="${message(code: 'user.action.label', default: 'Action')}"/>

                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${userList}" status="i" var="userInstance">
                        <tr>
                            <td class="text-center"><strong>${i}</strong></td>

                            <td class="text-center"><g:link controller="user" action="show"
                                                            id="${userInstance?.id}">${fieldValue(bean: userInstance, field: "username")}</g:link></td>

                            <td class="text-center">${fieldValue(bean: userInstance, field: "firstName")}</td>

                            <td class="text-center">${fieldValue(bean: userInstance, field: "lastName")}</td>
                            <td class="text-center">${fieldValue(bean: userInstance, field: "mobNo")}</td>
                            <td class="text-center">${fieldValue(bean: userInstance, field: "hasVerifiedEmail")}</td>
                            <td class="text-center">${fieldValue(bean: userInstance, field: "coustId")}</td>
                            <td class="text-center"><a href="" class="btn btn-xs btn-success"
                                                       data-toggle="modal" data-target="#myModal">
                                Send Mail
                            </a>
                            </td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
                <g:form controller="user" action="sendMailToUser">
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close"
                                            data-dismiss="modal"><span
                                            aria-hidden="true">&times;</span><span
                                            class="sr-only">Close</span>
                                    </button>
                                    <h4 class="modal-title"
                                        id="myModalLabel">Compose Mail</h4>
                                </div>

                                <div class="modal-body">
                                    <div class="row" style="padding: 1%">
                                        <div class="col-md-4">
                                            <strong>Receiver<travelx:star/>
                                            </strong>
                                        </div>

                                        <div class="col-md-8">
                                            <input type="text" class="form-control"
                                                   name="receiver" required=""/>
                                        </div>
                                    </div>

                                    <div class="row" style="padding: 1%">
                                        <div class="col-md-4">
                                            <strong>Subject<travelx:star/>
                                            </strong>
                                        </div>

                                        <div class="col-md-8">
                                            <input type="text" required="true"
                                                   class="form-control"
                                                   name="subject"/>
                                        </div>
                                    </div>

                                    <div class="row" style="padding: 1%">
                                        <div class="col-md-4">
                                            <strong><label>Body<travelx:star/>
                                            </label></strong>
                                        </div>

                                        <div class="col-md-8">
                                            <textarea required="true" name="body"
                                                      style="width:100%"></textarea>
                                        </div>
                                    </div>

                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">Close</button>
                                    <input type="submit"
                                           class="btn btn-primary" value="Send">
                                </div>
                            </div>
                        </div>
                    </div>
                </g:form>
            </div>

            <div class="tab-pane" id="createNewUser">
                <p>Coming soon.</p>
            </div>

            <div class="tab-pane" id="tab3">
                <p>Coming soon.</p>
            </div>

            <div class="tab-pane" id="tab4">
                <p>Coming soon.</p>
            </div>
        </div>
    </div>
</div>
%{--</div>--}%
%{--</div>--}%
%{--</div>--}%
%{--<script>--}%
%{--function activeButton(id) {--}%
%{--$(".disableAll").removeClass('active')--}%
%{--}--}%
%{--//</script>--}%
</body>
</html>
