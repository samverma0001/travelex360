<%@ page import="com.travelex.User.User" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin"/>
</head>

<body>
<travelx:adminUserTab/>

            <div class="tab-content" style="margin-top: 1%;">

                <div class="tab-pane active" id="Users" style="margin-left: 5%">

                    <ul class="nav nav-tabs" >
                        <li><a href="${createLink(controller: 'user', action: 'index')}">User List</a></li>
                        <li class="active"><a href="${createLink(controller: 'user', action: 'create')}">Create New</a>
                        </li>
                    </ul>

                    <div class="tab-content" style="margin-top: 1%">
                        <div class="tab-pane active" id="editUser">
                            <div class="col-md-12">
                                <g:hasErrors bean="${signUpCO}">
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert"><span
                                                aria-hidden="true">&times;
                                        </span><span class="sr-only">Close</span></button>

                                        <div id="errors" class="alert alert-error">
                                            <g:renderErrors bean="${signUpCO}"/>
                                        </div>
                                    </div>
                                </g:hasErrors>
                                <g:form action="save">
                                    <fieldset class="form">
                                        <g:render template="form" model="[userInstance: signUpCO]"/>
                                    </fieldset>
                                    <fieldset class="buttons">
                                        <g:submitButton name="create" class="save btn btn-primary"
                                                        value="${message(code: 'default.button.create.label', default: 'Create')}"/>
                                    </fieldset>
                                </g:form>
                            </div>

                        </div>


                        <div class="tab-pane" id="createNewUser">
                            <p>Coming soon.</p>
                        </div>

                        <div class="tab-pane" id="tab3">
                            <p>Coming soon.</p>
                        </div>

                        <div class="tab-pane" id="tab4">
                            <p>Coming soon.</p>
                        </div>
                    </div>

                </div>


                <div class="tab-pane" id="Hotels">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#hotelBookingList" data-toggle="tab">Booking List</a></li>
                        <li><a href="#hotelPaymentList" data-toggle="tab">Payment List</a></li>
                        <li><a href="#hotelCouponList" data-toggle="tab">Coupon List</a></li>
                        <li><a href="#hotelMarginList" data-toggle="tab">Margin List</a></li>

                    </ul>
                </div>

                <div class="tab-pane" id="Flights">
                    <h4>Coming soon !</h4>
                </div>

                <div class="tab-pane" id="Activity">
                    <h4>Coming soon !</h4>
                </div>

                <div class="tab-pane" id="serviceProvider">
                    <h4>Coming soon !</h4>
                </div>
            </div>
        %{--</div>--}%
    %{--</div>--}%
%{--</div>--}%
%{--<script>--}%
    %{--function activeButton(id) {--}%
        %{--$(".disableAll").removeClass('active')--}%
    %{--}--}%
%{--//</script>--}%

</body>
</html>
