<%@ page import="com.travelex.User.User" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="btn btn-toolbar">
    <a href="${createLink(controller: 'user', action: 'create')}" class="btn btn-primary">Create User</a>
</div>

<div id="list-user" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>

            <g:sortableColumn property="username" title="${message(code: 'user.username.label', default: 'Username')}"/>

            <g:sortableColumn property="password" title="${message(code: 'user.password.label', default: 'Password')}"/>

            <g:sortableColumn property="ipAddress"
                              title="${message(code: 'user.ipAddress.label', default: 'Ip Address')}"/>

            <g:sortableColumn property="firstName"
                              title="${message(code: 'user.firstName.label', default: 'First Name')}"/>

            <g:sortableColumn property="lastName"
                              title="${message(code: 'user.lastName.label', default: 'Last Name')}"/>

            <g:sortableColumn property="token" title="${message(code: 'user.token.label', default: 'Token')}"/>

        </tr>
        </thead>
        <tbody>
        <g:each in="${userInstanceList}" status="i" var="userInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td><g:link action="show"
                            id="${userInstance?.id}">${fieldValue(bean: userInstance, field: "username")}</g:link></td>

                <td>${fieldValue(bean: userInstance, field: "password")}</td>

                <td>${fieldValue(bean: userInstance, field: "ipAddress")}</td>

                <td>${fieldValue(bean: userInstance, field: "firstName")}</td>

                <td>${fieldValue(bean: userInstance, field: "lastName")}</td>

                <td>${fieldValue(bean: userInstance, field: "token")}</td>

            </tr>
        </g:each>
        </tbody>
    </table>

    <div class="pagination">
        <g:paginate total="${userInstanceCount ?: 0}"/>
    </div>
</div>
</body>
</html>
