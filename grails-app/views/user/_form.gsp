<%@ page import="com.travelex.User.User" %>

<div class="col-md-4">

    <div class="row" style="margin-top: 2%">
        <div class="col-md-4">
            <strong>Username<travelx:star/></strong>
        </div>

        <div class="col-md-8">
            <g:textField class="form-control" name="username" value="${userInstance?.username}"/>
        </div>
    </div>

    <div class="row" style="margin-top: 2%">
        <div class="col-md-4">
            <strong>Password<travelx:star/></strong>
        </div>

        <div class="col-md-8">
            <input type="password" class="form-control" name="password"/>
        </div>
    </div>

    <div class="row" style="margin-top: 2%">
        <div class="col-md-4">
            <strong >Confirm Password<travelx:star/></strong>
        </div>

        <div class="col-md-8">
            <input type="password" class="form-control" name="confirmPassword"/>
        </div>
    </div>

    <div class="row" style="margin-top: 2%">
        <div class="col-md-4">
            <strong>First Name<travelx:star/></strong>
        </div>

        <div class="col-md-8">
            <g:textField class="form-control" name="firstName" value="${userInstance?.firstName}"/>
        </div>
    </div>

    <div class="row" style="margin-top: 2%">
        <div class="col-md-4">
            <strong>Last Name<travelx:star/></strong>
        </div>

        <div class="col-md-8">
            <g:textField class="form-control" name="lastName" value="${userInstance?.lastName}"/>

        </div>
    </div>

    <div class="row" style="margin-top: 2%">
        <div class="col-md-4">
            <strong>Contact Number</strong>
        </div>

        <div class="col-md-8">
            <g:textField class="form-control" name="mobNo" value="${userInstance?.mobNo}"/>

        </div>
    </div>

    %{--<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'socialProfileList', 'error')} ">--}%
    %{--<tr>--}%

    %{--<td>--}%
    %{--<label for="socialProfileList">--}%
    %{--<g:message code="user.socialProfileList.label" default="Social Profile List"/>--}%

    %{--</label>--}%
    %{--</td>--}%
    %{--<td>--}%

    %{--<ul class="one-to-many">--}%
    %{--<g:each in="${userInstance?.socialProfileList ?}" var="s">--}%
    %{--<li><g:link controller="socialProfile" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>--}%
    %{--</g:each>--}%
    %{--<li class="add">--}%
    %{--<g:link controller="socialProfile" action="create"--}%
    %{--params="['user.id': userInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'socialProfile.label', default: 'SocialProfile')])}</g:link>--}%
    %{--</li>--}%
    %{--</ul>--}%
    %{--</td>--}%
    %{--</tr>--}%

    %{--</div>--}%
</div>

<div class="col-md-4">

    <div class="row" style="margin-top: 2%">
        <div class="col-md-4">
            <strong>Account Expired</strong>
        </div>

        <div class="col-md-8">
            <g:checkBox name="accountExpired" value="${userInstance?.accountExpired}"/>

        </div>
    </div>

    <div class="row" style="margin-top: 2%">
        <div class="col-md-4">
            <strong>Account Locked</strong>
        </div>

        <div class="col-md-8">
            <g:checkBox name="accountLocked" value="${userInstance?.accountLocked}"/>

        </div>
    </div>


    <div class="row" style="margin-top:2%">
        <div class="col-md-4">
            <strong>Has Verified Email</strong>
        </div>

        <div class="col-md-8">
            <g:checkBox name="hasVerifiedEmail" value="${userInstance?.hasVerifiedEmail}"/>

        </div>
    </div>


    <div class="row" style="margin-top: 2%">
        <div class="col-md-4">
            <strong>Password Expired</strong>
        </div>

        <div class="col-md-8">
            <g:checkBox name="passwordExpired" value="${userInstance?.passwordExpired}"/>

        </div>
    </div>

    <div class="row" style="margin-top: 2%">
        <div class="col-md-4">
            <strong>Enabled</strong>
        </div>

        <div class="col-md-8">
            <g:checkBox name="enabled" value="${userInstance?.enabled}"/>

        </div>
    </div>

    <div class="row" style="margin-top: 2%">
        <div class="col-md-4">
            <strong>ROLE_ADMIN</strong>
        </div>

        <div class="col-md-8">
            <g:radio name="selectRole" id="ROLE_ADMIN" value="ROLE_ADMIN"/>
        </div>
    </div>

    <div class="row" style="margin-top: 2%">
        <div class="col-md-4">
            <strong>ROLE_USER</strong>
        </div>

        <div class="col-md-8">
            <g:radio name="selectRole" id="ROLE_USER" value="ROLE_USER"/>
        </div>
    </div>

    <div class="row" style="margin-top: 2%">
        <div class="col-md-4">
            <strong>ROLE_PROVIDER</strong>
        </div>

        <div class="col-md-8">
            <g:radio name="selectRole" id="ROLE_PROVIDER" value="ROLE_PROVIDER"/>
        </div>
    </div>

</div>

<div class="col-md-4"></div>
<script>
    $(document).ready(function () {
        if (${role=="ROLE_ADMIN"}) {
            $("#ROLE_ADMIN").attr('checked', true);
        }
        else if (${role=="ROLE_USER"}) {
            $("#ROLE_USER").attr('checked', true);
        }
        else if (${role=="ROLE_PROVIDER"}) {
            $("#ROLE_PROVIDER").attr('checked', true);
        }
    });
</script>