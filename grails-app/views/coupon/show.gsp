<%@ page import="com.travelex.admin.Coupon" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin"/>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="span12">
            <div id="tab" class="btn-group">
                <a href="${createLink(controller: 'user', action: 'index')}" id="userId"
                   class="btn btn-large btn-info  disableAll"
                   onclick="activeButton(this.id)">Users</a>
                <a href="${createLink(controller: 'bookingHistory', action: 'index')}" id="hotelId"
                   class="btn btn-large btn-info active disableAll"
                   onclick="activeButton(this.id)">Hotels</a>
                <a href="#Flights" id="flightId" class="btn btn-large btn-info disableAll" data-toggle="tab"
                   onclick="activeButton(this.id)">Flights</a>
                <a href="#Activity" id="activityId" class="btn btn-large btn-info disableAll" data-toggle="tab"
                   onclick="activeButton(this.id)">Activity</a>
                <a href="#serviceProvider" id="serviceProviderId" class="btn btn-large btn-info disableAll"
                   data-toggle="tab"
                   onclick="activeButton(this.id)">Service Provider</a>

            </div>

            <div class="tab-content" style="margin-top: 1%">

                <div class="tab-pane active" id="Hotels">

                    <ul class="nav nav-tabs">
                        <li><a
                                href="${createLink(controller: 'coupon', action: 'index')}">Coupon List</a></li>
                        <li><a
                                href="${createLink(controller: 'coupon', action: 'create')}">New Coupon</a></li>
                        <li class="active"><a
                                href="${createLink(controller: 'coupon', action: 'edit', params: [couponInstance: couponInstance?.id])}">Edit Coupon</a>
                        </li>
                    </ul>

                    <div class="tab-content" style="margin-top: 1%">
                        <div class="tab-pane active">
                            <div class="col-md-9">

                                <div id="show-coupon" class="content scaffold-show" role="main">
                                    <table class="table table-bordered table-condensed">
                                        <thead>
                                        <th>Amount</th>
                                        <th>Amount Type</th>
                                        <th>Coupon Code</th>
                                        <th>Valid No. Of Day</th>
                                        <th>Status</th>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>${couponInstance?.amount}</td>
                                            <td>${couponInstance?.amountType}</td>
                                            <td>${couponInstance?.couponCode}</td>
                                            <td>${couponInstance?.validNoOfDays}</td>
                                            <td>${couponInstance?.status}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <g:form url="[resource: couponInstance, action: 'delete']" method="DELETE">
                                        <fieldset class="buttons">
                                            <g:link class="edit btn btn-success" action="edit"
                                                    resource="${couponInstance}"><g:message
                                                    code="default.button.edit.label" default="Edit"/></g:link>
                                            <g:actionSubmit class="delete btn btn-primary" action="delete"
                                                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                                        </fieldset>
                                    </g:form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
