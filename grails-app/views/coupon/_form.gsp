<%@ page import="com.travelex.admin.Coupon" %>


<table>
    <tr><div class="fieldcontain ${hasErrors(bean: couponInstance, field: 'amount', 'error')} ">
        <td><label for="amount">
            <g:message code="coupon.amount.label" default="Amount"/>

        </label></td><td>
            <g:textField name="amount" value="${couponInstance?.amount}"/>
        </td>
    </div>

        <div class="fieldcontain ${hasErrors(bean: couponInstance, field: 'amountType', 'error')} required">
            <td><label for="amountType">
                <g:message code="coupon.amountType.label" default="Amount Type"/>
                <span class="required-indicator">*</span>
            </label></td><td>
            <g:select name="amountType" from="${com.travelex.admin.AmountType?.values()}"
                      keys="${com.travelex.admin.AmountType.values()*.name()}" required=""
                      value="${couponInstance?.amountType?.name()}"/>
        </td>
        </div>

        <div class="fieldcontain ${hasErrors(bean: couponInstance, field: 'validNoOfDays', 'error')} ">
            <td><label for="validNoOfDays">
                <g:message code="coupon.validNoOfDays.label" default="Valid Days"/>

            </label></td><td>
            <g:textField name="validNoOfDays" value="${couponInstance?.validNoOfDays}"/>
        </td>
        </div>
    </tr>

    <tr><div class="fieldcontain ${hasErrors(bean: couponInstance, field: 'couponCode', 'error')} ">
        <td><label for="couponCode">
            <g:message code="coupon.couponCode.label" default="Coupon Code"/>

        </label></td><td>
            <g:textField name="couponCode" value="${couponInstance?.couponCode}"/>
        </td>
    </div>

        <div class="fieldcontain ${hasErrors(bean: couponInstance, field: 'status', 'error')} required">
            <td><label for="status">
                <g:message code="coupon.status.label" default="Status"/>
                <span class="required-indicator">*</span>
            </label></td><td>
            <g:select name="status" from="${com.travelex.admin.Status?.values()}"
                      keys="${com.travelex.admin.Status.values()*.name()}" required=""
                      value="${couponInstance?.status?.name()}"/>
        </td>
        </div>
    </tr>
</table>
