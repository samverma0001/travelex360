<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <g:set var="entityName" value="${message(code: 'coupon.label', default: 'Coupon')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="span12">
            <div id="tab" class="btn-group">
                <a href="${createLink(controller: 'user', action: 'index')}" id="userId"
                   class="btn btn-large btn-info  disableAll"
                   onclick="activeButton(this.id)">Users</a>
                <a href="${createLink(controller: 'bookingHistory', action: 'index')}" id="hotelId"
                   class="btn btn-large btn-info active disableAll"
                   onclick="activeButton(this.id)">Hotels</a>
                <a href="#Flights" id="flightId" class="btn btn-large btn-info disableAll" data-toggle="tab"
                   onclick="activeButton(this.id)">Flights</a>
                <a href="#Activity" id="activityId" class="btn btn-large btn-info disableAll" data-toggle="tab"
                   onclick="activeButton(this.id)">Activity</a>
                <a href="#serviceProvider" id="serviceProviderId" class="btn btn-large btn-info disableAll"
                   data-toggle="tab"
                   onclick="activeButton(this.id)">Service Provider</a>

            </div>


            <div class="tab-content" style="margin-top: 1%">

                <div class="tab-pane active" id="Hotels">

                    <ul class="nav nav-tabs">
                        <li><a
                                href="${createLink(controller: 'coupon', action: 'index')}">Coupon List</a></li>
                        <li class="active"><a
                                href="${createLink(controller: 'coupon', action: 'create')}">New Coupon</a></li>
                    </ul>

                    <div class="tab-content" style="margin-top: 1%">
                        <div class="tab-pane active">
                            <div class="col-md-12">
                                <div id="create-coupon" class="content scaffold-create" role="main">
                                    <h1><g:message code="default.create.label" args="[entityName]"/></h1>
                                    <g:if test="${flash.message}">
                                        <div class="message" role="status">${flash.message}</div>
                                    </g:if>
                                    <g:hasErrors bean="${couponVO}">
                                        <div class="alert alert-danger alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert"><span
                                                    aria-hidden="true">&times;
                                            </span><span class="sr-only">Close</span></button>

                                            <div id="errors" class="alert alert-error">
                                                <g:renderErrors bean="${couponVO}"/>
                                            </div>
                                        </div>
                                    </g:hasErrors>
                                    <g:form url="[resource: couponInstance, action: 'save']">
                                        <fieldset class="form">
                                            <g:render template="form"/>
                                        </fieldset>
                                        <fieldset class="buttons">
                                            <g:submitButton name="create" class="save btn btn-success"
                                                            value="${message(code: 'default.button.create.label', default: 'Create')}"/>
                                        </fieldset>
                                    </g:form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
