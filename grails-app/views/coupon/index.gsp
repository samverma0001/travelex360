<%@ page import="com.travelex.admin.Status; com.travelex.admin.Coupon" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin"/>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="span12">
            <div id="tab" class="btn-group">
                <a href="${createLink(controller: 'user', action: 'index')}" id="userId"
                   class="btn btn-large btn-info  disableAll"
                   onclick="activeButton(this.id)">Users</a>
                <a href="${createLink(controller: 'bookingHistory', action: 'index')}" id="hotelId"
                   class="btn btn-large btn-info active disableAll"
                   onclick="activeButton(this.id)">Hotels</a>
                <a href="#Flights" id="flightId" class="btn btn-large btn-info disableAll" data-toggle="tab"
                   onclick="activeButton(this.id)">Flights</a>
                <a href="#Activity" id="activityId" class="btn btn-large btn-info disableAll" data-toggle="tab"
                   onclick="activeButton(this.id)">Activity</a>
                <a href="#serviceProvider" id="serviceProviderId" class="btn btn-large btn-info disableAll"
                   data-toggle="tab"
                   onclick="activeButton(this.id)">Service Provider</a>

            </div>


            <div class="tab-content" style="margin-top: 1%">

                <div class="tab-pane active" id="Hotels">

                    <ul class="nav nav-tabs">
                        <li><a
                                href="${createLink(controller: 'bookingHistory', action: 'index')}">Booking List</a>
                        </li>
                        <li class="active"><a
                                href="${createLink(controller: 'coupon', action: 'index')}">Coupon List</a></li>
                        <li><a href="${createLink(controller: 'paymentInfo', action: 'index')}">Payment List</a></li>
                        <li><a href="${createLink(controller: 'setMargin', action: 'index')}">Margin List</a></li>
                    </ul>

                    <div class="tab-content" style="margin-top: 1%">
                        <div class="tab-pane active">
                            <p class="pull-right"><a class="btn btn-primary"
                                                     href="${createLink(controller: 'coupon', action: 'create')}">Create Coupon</a>
                            </p>
                            <table class="table table-bordered table-condensed">
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Coupon Code</th>
                                    <th class="text-center">Amount</th>
                                    <th class="text-center">Amount Type</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Valid No Of Days</th>
                                    <th class="text-center">Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                <g:each in="${couponInstanceList}" status="i" var="couponInstance">
                                    <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                                        <td class="text-center">${fieldValue(bean: couponInstance, field: 'id')}</td>
                                        <td class="text-center">${fieldValue(bean: couponInstance, field: "couponCode")}</td>


                                        <td class="text-center"><g:link action="show"
                                                                        id="${couponInstance.id}">${fieldValue(bean: couponInstance, field: "amount")}</g:link></td>

                                        <td class="text-center">${fieldValue(bean: couponInstance, field: "amountType")}</td>


                                        <td class="text-center">${fieldValue(bean: couponInstance, field: "status")}</td>
                                        <td class="text-center">${fieldValue(bean: couponInstance, field: "validNoOfDays")}</td>

                                        <g:if test="${couponInstance?.status == Status.ACTIVE}">
                                            <td class="text-center"><a
                                                    href="${createLink(controller: "coupon", action: "changeStatusDeactivate", params: [couponId: couponInstance?.id])}"
                                                    class="btn btn-success">Deactivate</a></td>
                                        </g:if>
                                        <g:else>
                                            <td class="text-center"><a
                                                    href="${createLink(controller: "coupon", action: "changeStatusActivate", params: [couponId: couponInstance?.id])}"
                                                    class="btn btn-danger">Activate</a></td>
                                        </g:else>
                                    </tr>
                                </g:each>

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
