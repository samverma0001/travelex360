<g:if test="${searchFor.equals("Itenerary")}">
    <div class="itinerary">
        <h3>${flightSearchResultVOFound?.flightSegmentVO?.originCityName?.first()} → ${flightSearchResultVOFound?.flightSegmentVO?.destinationCityName?.first()} <small>${flightSearchResultVOFound?.flightFareRule?.departureDate?.first()}</small>
        </h3>
        <g:each in="${flightSearchResultVOFound?.flightSegmentVO}" var="segment">

            <ul class="itinerarySummary">
                <li class="start">
                    <p>${segment?.originCityName} <br>
                        <small>${segment?.originAirPortName}</small></p>
                </li>
                <li class="detail">
                    <p class='text-large'><g:set var="flightTimeOrig"
                                                 value="${(segment.departureTime?.tokenize("T")?.last())}"/>
                    ${flightTimeOrig.substring(0, flightTimeOrig.lastIndexOf(":"))}
                    </p>
                    <small>${flightSearchResultVOFound?.flightFareRule?.departureDate?.first()}</small>
                </li>
                <li class="vendor">
                    <div class="air-logo">
                        %{--<img src="images/airasia.png">--}%
                    </div>

                    <div class="air-detail">
                        <div>${segment?.airLineName}</div>

                        <div><small>${segment?.airLineCode}</small></div>

                        <div class="color-default">${segment.duration}</div>
                    </div>
                </li>
                <li class="detail">
                    <p class='text-large'>
                        <g:set var="flightTimeDes"
                               value="${(segment.arrivalTime?.tokenize("T")?.last())}"/>
                        ${flightTimeDes.substring(0, flightTimeDes.lastIndexOf(":"))}</p>
                    <small>${flightSearchResultVOFound?.flightFareRule?.departureDate?.first()}</small>
                </li>
                <li class="end">
                    <p>${segment?.destinationCityName}<br>
                        <small>${segment?.destinationAirPortName}</small></p>
                </li>
            </ul>

            <div class="connector">
                <small class="layOver">Layover : -----------</small>
            </div>
        </g:each>
    </div>
</g:if>
<g:elseif test="${searchFor.equals('FareRule')}">
    <div class="fare-detail-container">
        <h3>Fare Break up</h3>

        <div class="row">
            <div class="col-sm-4">
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <td>Base fare</td>
                        <td class="text-right">${flightSearchResultVO?.flightFare?.baseFare}</td>
                    </tr>
                    <tr>
                        <td colspan="2"><h4>Taxes &amp; fees</h4></td>
                    </tr>
                    <tr>
                        <td>Tax</td>
                        <td class="text-right">Rs.${flightSearchResultVO?.flightFare?.tax}</td>
                    </tr>
                    <tr>
                        <td>Add. Txn Fee</td>
                        <td class="text-right">Rs.${flightSearchResultVO?.flightFare?.additionalTxnFee}</td>
                    </tr>
                    <tr>
                        <td>Airline Txn fee</td>
                        <td class="text-right">Rs.${flightSearchResultVO?.flightFare?.airTransFee}</td>
                    </tr>
                    <tr>
                        <td>Other Charges</td>
                        <td class="text-right">Rs.${flightSearchResultVO?.flightFare?.otherCharges}</td>
                    </tr>
                    <tr>
                        <td>Govt. Service Tax</td>
                        <td class="text-right">Rs.${flightSearchResultVO?.flightFare?.serviceTax}</td>
                    </tr>
                    <tr>
                        <td>Total</td>
                        <td class="text-right">Rs.${flightSearchResultVO?.flightFare?.offeredFare}</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-sm-4">
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <td colspan="2"><h4>Cancellation <small>Per Person</small></h4></td>
                    </tr>
                    <tr>
                        <td>Airline fee</td>
                        <td class="text-right">Rs.3,412</td>
                    </tr>
                    <tr>
                        <td>Cleartrip fee</td>
                        <td class="text-right">Rs.250</td>
                    </tr>

                    </tbody>
                </table>
            </div>

            <div class="col-sm-4">
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <td colspan="2"><h4>Change flight <small>Per Person</small></h4></td>
                    </tr>
                    <tr>
                        <td>Airline fee</td>
                        <td class="text-right">Rs.3,412</td>
                    </tr>
                    <tr>
                        <td>Cleartrip fee</td>
                        <td class="text-right">Rs.200</td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>

        <p class="text-muted"><small>Note: The airline fee may, at times, be calculated on a per-flight basis. Cancellation/Flight change charges are indicative. Airlines stop accepting cancellation/change requests 3 hours before flight departure.</small>
        </p>
    </div>
</g:elseif>
<g:elseif test="${searchFor.equals('Baggage')}">
%{--<h3>Coimbatore → Kuala Lumpur <small>Baggage</small></h3>--}%

%{--<div class="row">--}%
%{--<div class="col-md-3">--}%
%{--<div class="air-logo">--}%
%{--<img src="images/jetairways.png">--}%
%{--</div>--}%

%{--<div class="air-detail">--}%
%{--<div>Jet Airways</div>--}%

%{--<div><small>CJB - BLR</small></div>--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="col-md-4">--}%
%{--<table class="table table-bordered table-condensed">--}%
%{--<tr>--}%
%{--<td>Check-in</td>--}%
%{--<td class-"text-right">15 KG/person</td>--}%
%{--</tr>--}%
%{--<tr>--}%
%{--<td>Cabin</td>--}%
%{--<td class-"text-right">7 KG/person</td>--}%
%{--</tr>--}%
%{--</table>--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="row">--}%
%{--<div class="col-md-3">--}%
%{--<div class="air-logo">--}%
%{--<img src="images/airasia.png">--}%
%{--</div>--}%

%{--<div class="air-detail">--}%
%{--<div>Air Asia</div>--}%

%{--<div><small>BLR - KLP</small></div>--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="col-md-4">--}%
%{--<table class="table table-bordered table-condensed">--}%
%{--<tr>--}%
%{--<td>Check-in</td>--}%
%{--<td class-"text-right">15 KG/person</td>--}%
%{--</tr>--}%
%{--<tr>--}%
%{--<td>Cabin</td>--}%
%{--<td class-"text-right">7 KG/person</td>--}%
%{--</tr>--}%
%{--</table>--}%
%{--</div>--}%
%{--</div>--}%
    <div class="row">
        ${description}
    </div>
</g:elseif>
