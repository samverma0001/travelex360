<!DOCTYPE html>
<html lang="en" ng-app>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>.:: Travelex360 ::.</title>

    %{--<g:javascript library="jquery"/>--}%
    <link href="${resource(dir: 'css', file: 'jquery-ui.css')}" rel="stylesheet"/>
    <script src="${resource(dir: 'js', file: 'jquery-1.10.2.js')}"></script>
    <script src="${resource(dir: 'js', file: 'jquery-ui.js')}"></script>
    <link href="${resource(dir: 'css', file: 'bootstrap.css')}" rel="stylesheet"/>
    <link href="${resource(dir: 'css', file: 'datepicker3.css')}" rel="stylesheet">
    <link type="/text/css" rel="stylesheet" href="${resource(dir: 'css', file: 'jquery.selectBoxIt.css')}"/>
    <link href="${resource(dir: 'css', file: 'owl.carousel.css')}" rel="stylesheet"/>
    <link href="${resource(dir: 'css', file: 'owl.theme.css')}" rel="stylesheet"/>
    <link href="${resource(dir: 'css', file: 'jquery.bootstrap-touchspin.css')}" rel="stylesheet" type="text/css"
          media="all"/>
    <link href="${resource(dir: 'css', file: 'bootstrap-slider.css')}" rel="stylesheet"/>
    <link href="${resource(dir: 'css', file: 'style.css')}" rel="stylesheet"/>
    <link href="${resource(dir: 'css', file: 'theme.css')}" rel="stylesheet"/>
    <link href="${resource(dir: 'css', file: 'media.css')}" rel="stylesheet"/>

    <script src="${resource(dir: 'js', file: 'ie8-responsive-file-warning.js')}"></script>
    <script src="${resource(dir: 'js', file: 'ie-emulation-modes-warning.js')}"></script>
    <script src="${resource(dir: 'js', file: 'jquery.min.js')}"></script>
    <script src="${resource(dir: 'js', file: 'script.min.js')}"></script>

    <script type="text/javascript" src="${resource(dir: 'js', file: 'remoteNonStopPageScroll.js')}"></script>
    <g:javascript plugin="remote-pagination" library="remoteNonStopPageScroll"/>
</head>

<body class="theme-red">
<div class="page-wrapper">

<header class="main-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-sm-7 col-xs-6">
                <div class="logo pull-left">
                    <a href="${createLink(uri: "/")}">
                        <img src="../images/logo.png" width="190px">
                    </a>
                </div>

                <div class="main-icons-container-header pull-left">
                    <g:render template="/appTemplate/menuTemplate"/>
                </div>
            </div>
            <!-- <div class="col-md-7 col-sm-8 col-xs-7"> -->
            <!-- </div> -->
            <div class="col-md-2 col-sm-3 col-xs-6 text-right">
                <div class="header-right">
                    <ul class="header-icon-nav">
                        <li><a href="${createLink(controller: 'signIn', action: 'index')}"><img
                                src="../images/register-icon.png"></a></li>
                        <li><a href="${createLink(controller: 'login', action: 'auth')}"><img
                                src="../images/login-icon.png"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>


<div class="listing-page-wrapper">
    <g:render template="/flightTemplate/flightModifySearchTemplate"
              model="[flightSearchVo: flightSearchVo]"/>
    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="filter-sidebar-container">
                        <g:render template="/flightTemplate/flightFilterTemplate"
                                  model="[minPriceRange: minPriceRange, maxPriceRange: maxPriceRange]"/>
                    </div>
                </div>

                <div class="col-md-9">
                    <g:if test="${flightSearchVo?.journeyReturnDate == null}">
                        <div class="result-category-container">
                            <a href="javascript:void(0)" class="result-cat-box bg-brown active" id="allFlightCategory">
                                <div class="result-category-title">All Flights</div>

                                <div class="result-category-count">${total}</div>
                            </a>
                            <a href="javascript:void(0)" class="result-cat-box bg-voilet" id="nonStopFlightCategory">
                                <div class="result-category-title">Non-Stop Flight</div>

                                <div class="result-category-count">${nonStopCount}</div>
                            </a>
                            <a href="javascript:void(0)" class="result-cat-box bg-pink" id="redEyesFlightCategory">
                                <div class="result-category-title">Red-Eye Flights</div>

                                <div class="result-category-count">${redEyesCount}</div>
                            </a>
                            <a href="javascript:void(0)" class="result-cat-box bg-dark" id="budgetFlightCategory">
                                <div class="result-category-title">Budget Flights</div>

                                <div class="result-category-count">${budgetFlightCount}</div>
                            </a>
                        </div>

                        <div class="listing-content-container">
                            <div class="flight-list-sort-header">
                                <table class="table airline-listing-table">
                                    <thead>
                                    <tr>
                                        <th class="col-airline">Airline</th>
                                        <th class="col-dapart">Depart</th>
                                        <th class="col-arrive">Arrive</th>
                                        <th class="col-duration">Duration</th>
                                        <th class="col-price">Price</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>

                            <div id="updateFlightFilterList">
                                <g:render template="/flightTemplate/flightOneWayTemplate"
                                          model="[flightSearchVOList: flightSearchVOList, total: total]"/>
                            </div>
                        </div>
                    </g:if>
                    <g:else>
                        <div class="current-selection">
                            <h3 class="inner-title">Round Trip Current Selection</h3>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="selected-flight-container">
                                                <span class="flight-logo">
                                                    %{--<img id="flightImage" src="images/airasia.png">--}%
                                                    <span id="flightCodeId1" class="flight-code"></span>
                                                </span>
                                                <span class="flight-details">
                                                    <span id="flightNameId1" class="flight-name"></span>
                                                    <span id="flightRouteId1" class="flight-route"></span>
                                                    <span id="flightTimeId1" class="flight-timings"></span>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="selected-flight-container">
                                                <span class="flight-logo">
                                                    %{--<img src="images/airasia.png">--}%
                                                    <span id="flightCodeId2" class="flight-code"></span>
                                                </span>
                                                <span class="flight-details">
                                                    <span id="flightNameId2" class="flight-name"></span>
                                                    <span id="flightRouteId2" class="flight-route"></span>
                                                    <span id="flightTimeId2" class="flight-timings"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="total-fare-container text-center">
                                        <p class="total-fare">
                                            Rs. <span id="priceId"></span>
                                        </p>
                                    </div>
                                </div>

                                <div class="col-md-3 text-right">
                                    <g:form controller="tboFlightBooking" action="addOccupantForFlight">
                                        <input type="submit" class="btn btn-default full-width" value="Book Now"/>
                                        <g:hiddenField id="outBoundFlightId" name="outBoundFlight"/>
                                        <g:hiddenField id="inBoundFlightId" name="inBoundFlight"/>
                                    </g:form>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="listing-content-header">
                                    <span>1</span> ${flightSearchVo?.flightFromDom} &rarr; ${flightSearchVo?.flightToDom} <small
                                        class="text-muted">Sat, 14 Mar</small>
                                </div>

                                <div class="listing-content-container">
                                    <div class="flight-list-sort-header">
                                        <table class="table airline-listing-table">
                                            <thead>
                                            <tr>
                                                <th class="col-empty">&nbsp;&nbsp;&nbsp;</th>
                                                <th class="col-airline">Airline</th>
                                                <th class="col-dapart">Depart</th>
                                                <th class="col-arrive">Arrive</th>
                                                <th class="col-duration">Duration</th>
                                                <th class="col-price">Price</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <ul class="listView flights" id="updateOutboundPaginateList">
                                        <g:render template="/flightTemplate/flightReturnOutboundTemplate"
                                                  model="[flightSearchVOList: flightSearchVOList, total: total]"/>
                                    </ul>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="listing-content-header">
                                    <span>2</span> ${flightSearchVo?.flightToDom} &rarr; ${flightSearchVo?.flightFromDom} <small
                                        class="text-muted">Sat, 15 Mar</small>
                                </div>

                                <div class="listing-content-container">
                                    <div class="flight-list-sort-header">
                                        <table class="table airline-listing-table">
                                            <thead>
                                            <tr>
                                                <th class="col-empty">&nbsp;</th>
                                                <th class="col-airline">Airline</th>
                                                <th class="col-dapart">Depart</th>
                                                <th class="col-arrive">Arrive</th>
                                                <th class="col-duration">Duration</th>
                                                <th class="col-price">Price</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <ul class="listView flights" id="updateInboundPaginateList">
                                        <g:render template="/flightTemplate/flightReturnInboundTemplate"
                                                  model="[flightSearchVOListPaginatedInbound: flightSearchVOListPaginatedInbound, total: totalInbound]"/>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </g:else>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<footer class="main-footer inner">
    <div class="container-fluid">
        <div class="footer-links">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <ul class="inline-list-links">
                        <li><a href="about.php">About Us</a></li>
                        <li><a href="how_it_works.php">How It Works</a></li>
                        <li><a href="terms.php">Terms Of Use</a></li>
                        <li><a href="contact.php">Contact Us</a></li>
                    </ul>
                </div>

                <div class="col-md-6 col-sm-6 text-right">
                    Copyright &copy; Travelex 360 Pvt.Ltd. 2014, All rights reserved
                </div>
            </div>
        </div>

        <div class="footer-rights">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <ul class="inline-list-links margin-top-5">
                        <li><a href="#">Post Your Complaint</a></li>
                        <li><a href="policy.php">Privacy Policy</a></li>
                        <li><a href="#">FAQ's</a></li>
                        <li><a href="#">Site Map</a></li>
                    </ul>
                </div>

                <div class="col-md-6 col-sm-6 text-right">
                    <img src="../images/payment-methods.jpg">
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28/angular.js"></script>
<script src="${resource(dir: 'js', file: 'ang_main.js')}"></script>
<script src="${resource(dir: 'js', file: 'ie10-viewport-bug-workaround.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery-ui.min.js')}"></script>
<script src="${resource(dir: 'js', file: 'datepicker.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery.selectBoxIt.min.js')}"></script>
<script src="${resource(dir: 'js', file: 'owl.carousel.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery.bootstrap-touchspin.js')}"></script>
<script src="${resource(dir: 'js', file: 'bootstrap-slider.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery.barrating.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery.raty.js')}"></script>

<script src="${resource(dir: 'js', file: 'main.js')}"></script>


<script type="text/javascript">
    $(document).ready(function () {
        $(".countspin-roomtype1").TouchSpin({
            min: 0,
            initval: '1',
            max: 14,
            buttondown_class: 'btn btn-default',
            buttonup_class: 'btn btn-default',
            step: 1
        });
        $(".countspin-roomtype2").TouchSpin({
            min: 0,
            initval: '1',
            max: 5,
            buttondown_class: 'btn btn-default',
            buttonup_class: 'btn btn-default',
            step: 1
        });
        $('#example-f').barrating({
            showSelectedRating: false,
            readonly: true
        });
        $.fn.raty.defaults.path = 'images';
        $('.star-rating.readonly').raty({
            readOnly: true,
            score: function () {
                return $(this).attr('data-score');
            }
        });
        $('.star-rating-overall').raty({
            score: function () {
                return $(this).attr('data-score');
            },
            target: '.rating-target1',
            targetKeep: true,
            targetText: 'Click to Rate'
        });
        $('.star-rating-service').raty({
            score: function () {
                return $(this).attr('data-score');
            },
            target: '.rating-target2',
            targetKeep: true,
            targetText: 'Click to Rate'
        });
        $('.star-rating-quality').raty({
            score: function () {
                return $(this).attr('data-score');
            },
            target: '.rating-target3',
            targetKeep: true,
            targetText: 'Click to Rate'
        });
        $('.star-rating-clean').raty({
            score: function () {
                return $(this).attr('data-score');
            },
            target: '.rating-target4',
            targetKeep: true,
            targetText: 'Click to Rate'
        });

        $("#leavingFrom").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '${createLink(controller: 'tboFlightBooking',action: 'findCityForFlight')}',
                    dataType: 'json',
                    data: {
                        term: request.term
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            },
            %{--source: "${createLink(controller: "hotelService", action: "findCityForHotel" ,absolute: true)}"--}%
        });
        $("#leavingFrom").autocomplete({ minLength: 2 });
        $("#goingTo").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '${createLink(controller: 'tboFlightBooking',action: 'findCityForFlight')}',
                    dataType: 'json',
                    data: {
                        term: request.term
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            },
            %{--source: "${createLink(controller: "hotelService", action: "findCityForHotel" ,absolute: true)}"--}%
        });
        $("#goingTo").autocomplete({ minLength: 2 });
    });

    function flightItineary(i, flightInstance, tripIndicator) {
        var flightDetailModal = "#flight" + i;
        var modalIndex = i;
        $.ajax({
            url: "${createLink(controller: "tboFlightBooking" , action: "findFlightItinerary")}",
            type: "POST",
            data: {flightSearchResultVO: flightInstance, modalIndex: modalIndex, tripIndicator: tripIndicator},
            success: function (data) {
                $(flightDetailModal).html(data);
            }
        });
    }
    function showFlightFairRule(i, flightInstance, tripIndicator) {
        var flightFareModal = "#fare" + i;
        var modalIndex = i;
        $.ajax({
            url: "${createLink(controller: "tboFlightBooking" , action: "flightFairDetail")}",
            type: "POST",
            data: {flightSearchResultVO: flightInstance, modalIndex: modalIndex, tripIndicator: tripIndicator},
            success: function (data) {
                $(flightFareModal).html(data);
            }
        });
    }
    function showFlightSSR(i, flightInstance, tripIndicator) {
        var baggageModal = "#baggage" + i;
        var modalIndex = i;
        $.ajax({
            url: "${createLink(controller: "tboFlightBooking" , action: "findFlightSSR")}",
            type: "POST",
            data: {flightSearchResultVO: flightInstance, modalIndex: modalIndex, tripIndicator: tripIndicator},
            success: function (data) {
                $(baggageModal).html(data);
            }
        });
    }

    function findFlightsByFilter() {
        var checkedAirlineNames = [];
        $(".airlineFilter:checked").each(function () {
            checkedAirlineNames.push(this.value);
        });
        var checkedNoOfStop = [];
        $(".flightNoOfStop:checked").each(function () {
            checkedNoOfStop.push(this.value);
        });
        alert('-----checkedNoOfStop-----' + checkedNoOfStop);
        var checkedBudgetRangeFilter = [];
        $(".budgetRangeFilter:checked").each(function () {
            checkedBudgetRangeFilter.push(this.value);
        });
        alert('-----checkedBudgetRangeFilter-----' + checkedBudgetRangeFilter);
        var sliderPriceRange = $("#flightPriceRangeFilter").slider('getValue');
        alert('-----sliderPriceRange-----' + sliderPriceRange);

        $.ajax({
            url: "${createLink(controller: "tboFlightBooking" , action: "searchFlightForFilter",absolute: true)}",
            type: "POST",
            data: {checkedAirlineNames: checkedAirlineNames, checkedNoOfStop: checkedNoOfStop, checkedBudgetRangeFilter: checkedBudgetRangeFilter, sliderPriceRange: sliderPriceRange},
            success: function (data) {
                $("#updateFlightFilterList").html(data);
            }
        });

    }


    function flightNoOfStop() {
        var selectedCheckBox = new Array();
        $('input[name="flightNoOfStop"]:checked').each(function () {
            selectedCheckBox.push(this.value);
        });
        $.ajax({
            url: "${createLink(controller: "tboFlightBooking" , action: "searchFlightForFilter")}",
            type: "POST",
            data: {selectedCheckBox: selectedCheckBox},
            success: function (data) {
                $("#updateFlightFilterList").html(data);
            }
        });
    }
    function findFilterForAirline() {
        var selectedCheckBox = new Array();
        $('input[name="airlineFilter"]:checked').each(function () {
            selectedCheckBox.push(this.value);
        });
        $.ajax({
            url: "${createLink(controller: "tboFlightBooking" , action: "searchFlightForFilter")}",
            type: "POST",
            data: {selectedCheckBox: selectedCheckBox, searchFor: 'Airline'},
            success: function (data) {
                $("#updateFlightFilterList").html(data);
            }
        });
    }

    var sliderPriceRange;
    $("#flightPriceRangeFilter").slider().on('slide', function (ev) {
        sliderPriceRange = $("#flightPriceRangeFilter").slider('getValue');
    })
    $("#flightPriceRangeFilter").slider().on('slideStop', function (ev) {
        $.ajax({
            url: "${createLink(controller: "tboFlightBooking" , action: "searchFlightForFilter")}",
            type: "POST",
            data: {sliderPriceRange: sliderPriceRange, searchFor: "flightPriceRange"},
            success: function (data) {
                $("#updateFlightFilterList").html(data);
            }
        });


    });

    function findFlightByBudgetRange() {
        var selectedCheckBox = new Array();
        $('input[name="budgetRangeFilter"]:checked').each(function () {
            selectedCheckBox.push(this.value);
        });
        $.ajax({
            url: "${createLink(controller: "tboFlightBooking" , action: "searchFlightForFilter")}",
            type: "POST",
            data: {selectedCheckBox: selectedCheckBox, searchFor: 'budgetRange'},
            success: function (data) {
                $("#updateFlightFilterList").html(data);
            }
        });
    }
    $('#allFlightCategory').click(function () {
        $.ajax({
            url: "${createLink(controller: "tboFlightBooking" , action: "searchFlightByCategory")}",
            type: "POST",
            data: {flightCategory: 'allFlightCategory'},
            success: function (data) {
                $("#updateFlightFilterList").html(data);
            }
        });
    });
    $('#nonStopFlightCategory').click(function () {
        $.ajax({
            url: "${createLink(controller: "tboFlightBooking" , action: "searchFlightByCategory")}",
            type: "POST",
            data: {flightCategory: 'nonStopFlightCategory'},
            success: function (data) {
                $("#updateFlightFilterList").html(data);
            }
        });

    });
    $('#redEyesFlightCategory').click(function () {
        $.ajax({
            url: "${createLink(controller: "tboFlightBooking" , action: "searchFlightByCategory")}",
            type: "POST",
            data: {flightCategory: 'redEyesFlightCategory'},
            success: function (data) {
                $("#updateFlightFilterList").html(data);
            }
        });
    });
    $('#budgetFlightCategory').click(function () {
        $.ajax({
            url: "${createLink(controller: "tboFlightBooking" , action: "searchFlightByCategory")}",
            type: "POST",
            data: {flightCategory: 'budgetFlightCategory'},
            success: function (data) {
                $("#updateFlightFilterList").html(data);
            }
        });
    })
</script>
</body>
</html>