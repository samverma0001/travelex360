<!DOCTYPE html>
<html lang="en" ng-app>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <title>.:: Travelex360 ::.</title>
    <link href="${resource(dir: 'css', file: 'jquery-ui.css')}" rel="stylesheet"/>
    <script src="${resource(dir: 'js', file: 'jquery-1.10.2.js')}"></script>
    <script src="${resource(dir: 'js', file: 'jquery-ui.js')}"></script>
    <link href="${resource(dir: 'css', file: 'bootstrap.css')}" rel="stylesheet"/>
    <link href="${resource(dir: 'css', file: 'datepicker3.css')}" rel="stylesheet"/>
    <link type="/text/css" rel="stylesheet" href="${resource(dir: 'css', file: 'jquery.selectBoxIt.css')}"/>
    <!-- Owl Carousel Assets -->
    <link href="${resource(dir: 'css', file: 'owl.carousel.css')}" rel="stylesheet"/>
    <link href="${resource(dir: 'css', file: 'owl.theme.css')}" rel="stylesheet"/>
    <link href="${resource(dir: 'css', file: 'jquery.bootstrap-touchspin.css')}" rel="stylesheet" type="text/css"
    media="all"/>
    <link href="${resource(dir: 'css', file: 'bootstrap-slider.css')}" rel="stylesheet"/>
    <link href="${resource(dir: 'css', file: 'style.css')}" rel="stylesheet"/>
    <link href="${resource(dir: 'css', file: 'theme.css')}" rel="stylesheet"/>
    <link href="${resource(dir: 'css', file: 'media.css')}" rel="stylesheet"/>
    <script src="${resource(dir: 'js', file: 'ie8-responsive-file-warning.js')}"></script>
    <script src="${resource(dir: 'js', file: 'ie-emulation-modes-warning.js')}"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <style>
    .ui-autocomplete {
        max-height: 100px;
        overflow-y: auto;
        /* prevent horizontal scrollbar */
        overflow-x: hidden;
    }

    /* IE 6 doesn't support max-height
     * we use height instead, but this forces the menu to always be this tall
     */
    * html .ui-autocomplete {
        height: 100px;
    }
    </style>

</head>

<body class="full-height flight-booking-page theme-red ">
<header class="main-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="logo">
                    <a href="${createLink(uri: "/")}">
                        <img src="../images/logo.png" width="190px">
                    </a>
                </div>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                <div class="header-right">
                    <ul class="header-icon-nav">
                        <li><a href="${createLink(controller: 'signIn', action: 'index')}"><img
                                src="../images/register-icon.png"></a></li>
                        <li><a href="${createLink(controller: 'login', action: 'auth')}"><img
                                src="../images/login-icon.png"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>


<div class="inner-page-container full-height">
<!-- <div class="middle-container"> -->
<div class="container-fluid">
<!-- <h1 class="main-title text-center">Plan a Travel for your Holiday</h1> -->
<div class="main-search-container">
<div class="main-icons-container-vertical">
    <a href="#" class="main-icon-box">
        <img src="${resource(dir: 'images', file: 'ico1.png')}">

        <h3>PLAN TRAVEL</h3>
    </a>
    <a href="${createLink(controller: "hotelService", action: "bookHotel")}" class="main-icon-box ">
        <img src="${resource(dir: 'images', file: 'ico2.png')}">

        <h3>BOOK HOTEL</h3>
    </a>
    <a href="#" class="main-icon-box">
        <img src= ${resource(dir: 'images', file: 'ico3.png')}>

        <h3>ACTIVITY</h3>
    </a>
    <a href="${createLink(controller: "tboFlightBooking", action: "searchFlights")}" class="main-icon-box active">
        <img src= ${resource(dir: 'images', file: 'ico4.png')}>

        <h3>FLIGHT BOOKING</h3>
    </a>
    <a href="${createLink(controller: "serviceProvider", action: "chooseService")}" class="main-icon-box ">
        <img src= ${resource(dir: 'images', file: 'ico5.png')}>

        <h3>SERVICE PROVIDER</h3>
    </a>
    <a href="${createLink(controller: "holiday", action: "showHolidayCategory")}" class="main-icon-box">
        <img src= ${resource(dir: 'images', file: 'ico6.png')}>

        <h3>PACKAGES</h3>
    </a>
</div>

<div class="main-search-container-content">
<h1 class="box-title">Book Flights</h1>

<g:form class="tvx-from" controller="tboFlightBooking"
        action="searchFlightsForCity2">
<div class="form-group">
    <label for="">Choose where you prefer to go</label>

    <div class="tick-box">
        <input id="radio1" class="tick-radio flight-single" type="radio" name="timeTable" checked="true" value="OneWay">
        <label for="radio1"><span><span></span></span>One Way</label>
        <input id="radio2" class="tick-radio flight-double" type="radio" name="timeTable" value="Return"><label
            for="radio2"><span><span></span></span>Round Trip</label>
        <input id="radio3" class="tick-radio flight-multi" type="radio" name="timeTable" value="MultiWay"><label
            for="radio3"><span><span></span></span>Multi City</label>
    </div>
</div>


<div class="single-trip-container">
    <div class="form-group">
        <div class="row">
            <div class="col-md-6">
                <label for="">Leaving From</label>
                <input type="text" class="form-control" name="flightFromDom" id="leavingFrom"
                       placeholder="Select Origin">
            </div>

            <div class="col-md-6">
                <label for="">Going To</label>
                <input type="text" class="form-control" id="goingTo" name="flightToDom"
                       placeholder="Select Destination">
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4">
                <label for="">Departure</label>

                <div class="input-group date">
                    <input type="text" name="departDate" class="form-control" autocomplete="off">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-4 ">
                <div class="return_box">
                    <label for="">Return</label>

                    <div class="input-group date ">
                        <input type="text" name="returnDate" class="form-control" autocomplete="off">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="multi-trip-container">
    <div class="multicity-flight-container">
        <div class="multicity-header">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="">Leaving From</label>
                        </div>

                        <div class="col-md-6">
                            <label for="">Going To</label>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-10">
                            <label for="">Departure</label>
                        </div>

                        <div class="col-md-2 col-sm-2 col-xs-2 text-center">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="multicity-leg ">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="w" name="fromCity1" placeholder="">
                        </div>

                        <div class="col-md-6">
                            <input type="text" class="form-control" id="e" name="toCity1" placeholder="">
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-10">
                            <div class="input-group date">
                                <input type="text" class="form-control" name="depature1">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>

                        <div class="col-md-2 col-sm-2 col-xs-2 text-center">
                            <div class="delete-city"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="multicity-leg ">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="" name="fromCity1" placeholder="">
                        </div>

                        <div class="col-md-6">
                            <input type="text" class="form-control" id="" name="toCity1" placeholder="">
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-10">
                            <div class="input-group date">
                                <input type="text" class="form-control" name="depature1">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>

                        <div class="col-md-2 col-sm-2 col-xs-2 text-center">
                            <div class="delete-city"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="multicity-add-container">
            <a href="#" id="addCity" class="multicity-add btn btn-default"><i
                    class=" glyphicon glyphicon-plus"></i> Add City <small>(max 5)</small></a>
            <a href="#" id="removeCity" class="multicity-add btn btn-default"><i
                    class=" glyphicon glyphicon-minus"></i> Remove City <small>(min 2)</small></a>
        </div>

    </div>

</div>


<div class="form-group">

    <h4>Traveller Details</h4>

    <div class="row">

        <div class="col-md-3 col-sm-3 col-xs-3">
            <label for="">Adults <small>(12+ yrs)</small></label>

            <div class="input-group col-md-12">
                <g:select from="${1..9}"
                          class="form-control peopleInFlight"
                          name="adult"
                          id="noOfAdult"/>
            </div>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-3">
            <label for="">Child <small>(2-11 yrs)</small></label>

            <div class="input-group col-md-12">
                <g:select noSelection="${['0': 'Select Children']}" from="${0..8}"
                          class="form-control childInFlight"
                          id="noOfChild"
                          name="children"/>
            </div>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-3">
            <label for="">Infant <small>(0-2 yrs)</small></label>

            <div class="input-group col-md-12">
                <g:select noSelection="${['0': 'Select Infants']}" from="${0..8}"
                          class="form-control childInFlight"
                          id="noOfChild"
                          name="infant"/>
            </div>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-3">
            <label for="">Class</label>

            <div class="input-group col-md-12">
                <select name="flightClass" class="form-control">
                    <option>Economy</option>
                    <option>Business</option>
                    <option>First Class</option>
                </select>
            </div>
        </div>

    </div>

</div>

<div class="form-footer">
    <div class="row">
        <div class="col-md-7 col-sm-7">
            <!-- <div class="form-group"> -->
            <div class="checkbox">
                <label>
                    <input type="checkbox"> My dates are Flexible [+/- 3 days]
                </label>
                <label>
                    <input type="checkbox"> Include Nearby Airports
                </label>
            </div>
            <!-- </div> -->
        </div>

        <div class="col-md-5 col-sm-5">
            <div class="form-group text-right">
                <input type="submit" value="Search Flights" class="btn btn-main btn-default"/>
            </div>
        </div>
    </div>
</div>
</g:form>
</div>
</div>
</div>
</div>

<footer class="main-footer">
    <div class="container-fluid">
        <div class="footer-links">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <ul class="inline-list-links">
                        <li><a href="about.php">About Us</a></li>
                        <li><a href="how_it_works.php">How It Works</a></li>
                        <li><a href="terms.php">Terms Of Use</a></li>
                        <li><a href="contact.php">Contact Us</a></li>
                    </ul>
                </div>

                <div class="col-md-6 col-sm-6 text-right">
                    Copyright &copy; Travelex 360 Pvt.Ltd. 2014, All rights reserved
                </div>
            </div>
        </div>

        <div class="footer-rights">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <ul class="inline-list-links margin-top-5">
                        <li><a href="#">Post Your Complaint</a></li>
                        <li><a href="policy.php">Privacy Policy</a></li>
                        <li><a href="#">FAQ's</a></li>
                        <li><a href="#">Site Map</a></li>
                    </ul>
                </div>

                <div class="col-md-6 col-sm-6 text-right">
                    <img src="../images/payment-methods.jpg">
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- // <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> -->
<script src="${resource(dir: 'js', file: 'script.min.js')}"></script>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28/angular.js"></script>
<script src="${resource(dir: 'js', file: 'ang_main.js')}"></script>
<script src="${resource(dir: 'js', file: 'ie10-viewport-bug-workaround.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery-ui.min.js')}"></script>
<script src="${resource(dir: 'js', file: 'datepicker.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery.selectBoxIt.min.js')}"></script>
<script src="${resource(dir: 'js', file: 'owl.carousel.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery.bootstrap-touchspin.js')}"></script>
<script src="${resource(dir: 'js', file: 'packages-map.js')}"></script>
<script src="${resource(dir: 'js', file: 'bootstrap-slider.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery.barrating.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery.raty.js')}"></script>
<script src="${resource(dir: 'js', file: 'main.js')}"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".countspin-roomtype1").TouchSpin({
            min: 0,
            initval: '1',
            max: 14,
            buttondown_class: 'btn btn-default',
            buttonup_class: 'btn btn-default',
            step: 1
        });
        $(".countspin-roomtype2").TouchSpin({
            min: 0,
            initval: '1',
            max: 5,
            buttondown_class: 'btn btn-default',
            buttonup_class: 'btn btn-default',
            step: 1
        });
        $('#example-f').barrating({
            showSelectedRating: false,
            readonly: true
        });
        $.fn.raty.defaults.path = 'images';
        $('.star-rating.readonly').raty({
            readOnly: true,
            score: function () {
                return $(this).attr('data-score');
            }
        });
        $('.star-rating-overall').raty({
            score: function () {
                return $(this).attr('data-score');
            },
            target: '.rating-target1',
            targetKeep: true,
            targetText: 'Click to Rate'
        });
        $('.star-rating-service').raty({
            score: function () {
                return $(this).attr('data-score');
            },
            target: '.rating-target2',
            targetKeep: true,
            targetText: 'Click to Rate'
        });
        $('.star-rating-quality').raty({
            score: function () {
                return $(this).attr('data-score');
            },
            target: '.rating-target3',
            targetKeep: true,
            targetText: 'Click to Rate'
        });
        $('.star-rating-clean').raty({
            score: function () {
                return $(this).attr('data-score');
            },
            target: '.rating-target4',
            targetKeep: true,
            targetText: 'Click to Rate'
        });

        $("#leavingFrom").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '${createLink(controller: 'tboFlightBooking',action: 'findCityForFlight')}',
                    dataType: 'json',
                    data: {
                        term: request.term
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            },
            %{--source: "${createLink(controller: "hotelService", action: "findCityForHotel" ,absolute: true)}"--}%
        });
        $("#leavingFrom").autocomplete({ minLength: 2 });


        $("#goingTo").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '${createLink(controller: 'tboFlightBooking',action: 'findCityForFlight')}',
                    dataType: 'json',
                    data: {
                        term: request.term
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            },
            %{--source: "${createLink(controller: "hotelService", action: "findCityForHotel" ,absolute: true)}"--}%
        });
        $("#goingTo").autocomplete({ minLength: 2 });


    });
</script>
</body>
</html>





