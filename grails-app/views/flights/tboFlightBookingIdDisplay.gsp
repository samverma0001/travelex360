<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title>Flight booked confirmation</title>
</head>

<body>
<br>
<br>
<br>
<br>

<div class="container" style="margin-bottom: 40%">
    <div class="row">
        <h4><p><strong>${description}</strong></p></h4>

        <g:if test="${bookedFlight}">
            <p><strong>Ticket has been booked.Please login for more details.Your PNR number is ${bookedFlight?.pnr} &amp;booking id is ${bookedFlight?.bookingId}</strong>
            </p>
        </g:if>
        <g:else>
            <p><strong>Unable to book flight,Please try again.</strong>
            </p>
        </g:else>
    </div>
</div>
</body>
</html>