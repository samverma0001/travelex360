<%@ page import="java.math.RoundingMode; com.travelex.booking.Adult" %>
<br>

<div class="row" style="margin-bottom: 100px">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <img class="panel-title text-left" src="${resource(dir: '/images/appimages', file: 'appLogo.png')}"
                 height="35">

            <h4 class="panel-title text-center" style="margin-top: -15px"><strong>Invoice Detail</strong></h4>

            <h4 class="panel-title text-right" style="margin-top: -15px"><a href=""
                                                                            onclick="window.print()">Print Invoice</a>
            </h4>

        </div>

        <div class="panel-body">
            <div class="row">

                <div class="col-md-4">
                    <addresss>
                        <strong>Travelex 360 Pvt.Ltd.</strong>
                        <br>
                        25/60,&nbsp;A block,
                        <br>
                        Middle circle, Connaught Place
                        <br>
                        New Delhi,110001
                    </addresss>
                </div>

                <div class="col-md-4">
                    <address>
                        <strong>To:-</strong>
                        <br>
                        <g:each in="${Adult.findAllByRoomBooked(roomBookedList.first())}" var="adult">
                            <g:if test="${adult.laedPassanger}">
                                ${adult.adultFirstName}&nbsp;&nbsp;${adult.adultLastName}
                            </g:if>
                        </g:each>
                        <br>
                        <strong>Nationality&nbsp;&nbsp;&nbsp;${bookingHistory?.counrtyName}</strong>
                    </address>
                </div>

                <div class="col-md-4">
                    <p><strong>Check-In</strong>&nbsp;&nbsp;${bookingHistory?.checkIn?.format('dd/MM/yyyy')}</p>

                    <p><strong>Check-Out</strong>&nbsp;&nbsp;${bookingHistory?.checkOut?.format('dd/MM/yyyy')}</p>

                </div>
            </div>
            <br>
            <table class="table table-bordered table-condensed">
                <tr>
                    <td class="text-center">Hotel Name</td>
                    <td class="text-center">Room Type</td>
                    <td class="text-center">Pax Name</td>
                    <td class="text-center">Room(s)</td>
                    <td class="text-center">Night(s)</td>
                    <td class="text-center">Rate</td>
                    <td class="text-center">Tax</td>
                    <td class="text-center">Total</td>
                </tr>
                <g:each in="${roomBookedList}" var="roomBooked">
                    <tr>
                        <td class="text-center">${hotelForBooking?.hotelName}</td>
                        <td class="text-center">${roomBooked?.roomCategory}</td>
                        <td class="text-center">${roomBooked?.adults?.first()?.adultFirstName}&nbsp;${roomBooked?.adults?.first()?.adultLastName}</td>
                        <td class="text-center">${1}</td>
                        <td class="text-center">${bookingHistory?.checkOut - bookingHistory?.checkIn}</td>
                        <td class="text-center">${paymentInfo?.amount}</td>
                        <td class="text-center">${paymentInfo?.totalTax?.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP)}</td>
                        <td class="text-center">${paymentInfo?.amount?.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP) + paymentInfo?.totalTax?.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP)}</td>
                    </tr>
                </g:each>
                <tr>
                    <td class="text-center" colspan="6">Payment Gateway Service Charge</td>
                    <td colspan="6">${paymentInfo?.payUMoneyStake}</td>
                </tr>
                <tr>
                    <td class="text-center" colspan="6">Total</td>
                    <td colspan="6">${((paymentInfo.amount.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP) * (bookingHistory?.noOfRooms?.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP))) + (paymentInfo.totalTax.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP) * (bookingHistory?.noOfRooms?.toBigDecimal()))) + paymentInfo.payUMoneyStake.toBigDecimal()}</td>
                </tr>

            </table>
            <tr><strong style="color: blue">Remarks</strong></tr>

            <p>
                Please note that while your booking had been confirmed and is guaranteed, the rooming list with your name may not be adjusted in the hotel's reservation system until closer to arrival.
            </p>
            <tr><strong style="color: blue">Booking Terms & Conditions:</strong></tr>

            <p>
                You must present a photo ID at the time of check in. Hotel may ask for credit card or cash deposit for the extra services at the time of check in.

                All extra charges should be collected directly from clients prior to departure such as parking, phone calls, room service, city tax, etc.

                We don’t accept any responsibility for additional expenses due to the changes or delays in air, road, rail, sea or indeed of any other causes, all such expenses will have to be borne by passengers.

                In case of wrong residency & nationality selected by user at the time of booking; the supplement charges may be applicable and need to be paid to the hotel by guest on check in/ check out.
            </p>
        </div>
    </div>
</div>