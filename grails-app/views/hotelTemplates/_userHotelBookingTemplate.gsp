<%@ page import="com.travelex.booking.HotelForBooking" %>
<table class="table table-bordered table-stripped">
    <tr>
        <th>Book Reference No</th>
        <th>Room Category</th>
        <th>No of rooms booked</th>
        <th>Checked In</th>
        <th>Checked Out</th>
        <th>Action</th>
    </tr>

    <g:each in="${bookingHistoryList}" var="booking">

        <tr>
            <td>${booking?.bookId}</td>

            <td>${HotelForBooking.findByBookingHistory(booking)?.roomBooked?.roomCategory?.first()}</td>

            <td>${booking?.noOfRooms}</td>

            <td>${booking?.checkIn?.format('dd/MM/yyyy')}</td>
            <td>${booking?.checkOut?.format('dd/MM/yyyy')}</td>
            <td><g:if test="${!booking?.bookingCancelId}">
                <a href="${createLink(controller: "home", action: "cancelRoomBooking", params: [bookRefNo: booking?.bookId, bookingHistoryId: booking?.id])}"
                   class="btn btn-xs btn-primary pull-right ui-popover" data-toggle="tooltip"
                   data-placement="top"
                   title="Cancel Booking">
                    Cancel Booking
                </a>
            </g:if>
                <g:else>
                    <a href="#" class="btn btn-xs btn-danger disabled">Cancelled</a>
                </g:else>

                <a href="${createLink(controller: 'home', action: 'showVoucher', params: [bookingHistoryId: booking?.id])}"
                   class="btn btn-xs btn-primary">View Voucher</a>
                <a href="${createLink(controller: 'home', action: 'showInvoice', params: [bookingHistoryId: booking?.id])}"
                   class="btn btn-xs btn-primary">View Invoice</a>
            </td>
        </tr>
    </g:each>
</table>
