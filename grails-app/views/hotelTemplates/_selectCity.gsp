<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
<g:select name="place" from="${citiesList}" optionKey="id" optionValue="name" onkeyup="findCity()"
          class="chosen-select form-control" value="${cityId}"
          noSelection="${['': 'Select Location....']}"/>

%{--<input type="text" class="form-control" value="${cityList?.name}" id="selectCity" name="place" onkeyup="findCity()">--}%


%{--<script>--}%
    %{--function findCity() {--}%
        %{--var tempCity = $("#selectCity").val();--}%
        %{--alert(tempCity);--}%
        %{--$.ajax({--}%
            %{--url: "${createLink(controller: 'hotelService',action: 'findCityForHotel')}",--}%
            %{--type: "POST",--}%
            %{--data: {place: tempCity},--}%
            %{--success: function (data) {--}%
                %{--alert(data);--}%
                %{--$("#selectCity").html(data);--}%
            %{--}--}%
        %{--});--}%
    %{--}--}%
%{--</script>--}%