%{--HI   ${total}--}%
<g:javascript plugin="remote-pagination" library="remoteNonStopPageScroll"/>
<script type="text/javascript" src="${resource(dir: 'js', file: 'remoteNonStopPageScroll.js')}"></script>
<g:each in="${hotelList}" status="i" var="hotelInstance">

<div class="controls list-group-item" style=" margin-left: 10px">
<div class="row">
<div class="col-md-3">
%{--<g:each in="${hotelInstance?.searchHotelMedias}" var="media1">--}%
    <g:if test="${hotelInstance}">
        <img src="${hotelInstance?.searchHotelMedias ? hotelInstance?.searchHotelMedias?.first()?.mediaSrc : defaultUrl}"
             height="100"
             width="150"
             class="img-rounded">
    </g:if>
    <g:else>
        <img src="${defaultUrl}" height="100"
             width="150"
             class="img-rounded">
    </g:else>

%{--</g:each>--}%
    <g:if test="${(hotelInstance?.hotelRating == "OneStar" || "TwoStar" || "ThreeStar" || "FourStar" || "FiveStar" || "SixStar" || "SevenStar")}">
        <g:if test="${hotelInstance?.hotelRating == 'OneStar' || hotelInstance?.hotelRating == '1'}">
            <h5><small>Rating:</small>&nbsp;<g:each
                    in="${1..1}"
                    var="a"><img
                        src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
            </h5>
        </g:if>
        <g:elseif test="${hotelInstance?.hotelRating == 'TwoStar' || hotelInstance?.hotelRating == '2'}">
            <h5><small>Rating:</small>&nbsp;<g:each
                    in="${1..2}"
                    var="a"><img
                        src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
            </h5>
        </g:elseif>
        <g:elseif test="${hotelInstance?.hotelRating == 'ThreeStar' || hotelInstance?.hotelRating == '3'}">
            <h5><small>Rating:</small>&nbsp;<g:each
                    in="${1..3}"
                    var="a"><img
                        src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
            </h5>
        </g:elseif>
        <g:elseif test="${hotelInstance?.hotelRating == 'FourStar' || hotelInstance?.hotelRating == '4'}">
            <h5><small>Rating:</small>&nbsp;<g:each
                    in="${1..4}"
                    var="a"><img
                        src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
            </h5>
        </g:elseif>

        <g:elseif test="${hotelInstance?.hotelRating == 'FiveStar' || hotelInstance?.hotelRating == '5'}">
            <h5><small>Rating:</small>&nbsp;<g:each
                    in="${1..5}"
                    var="a"><img
                        src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
            </h5>
        </g:elseif>
        <g:elseif test="${hotelInstance?.hotelRating == 'SixStar' || hotelInstance?.hotelRating == '6'}">
            <h5><small>Rating:</small>&nbsp;<g:each
                    in="${1..6}"
                    var="a"><img
                        src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
            </h5>
        </g:elseif>
        <g:elseif test="${hotelInstance?.hotelRating == 'SevenStar' || hotelInstance?.hotelRating == '7'}">
            <h5><small>Rating:</small>&nbsp;<g:each
                    in="${1..7}"
                    var="a"><img
                        src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
            </h5>
        </g:elseif>

    </g:if>

    <g:elseif
            test="${(hotelInstance?.hotelRating == "0" || "1" || "2" || "3" || "4" || "5" || "6" || "7")}">
        <h5><small>Rating:</small>&nbsp;
            <g:each
                    in="${1..(hotelInstance?.hotelRating as Integer)}"
                    var="a"><img
                    src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
        </h5>
    </g:elseif>
    <g:else>
        Rating :&nbsp;   ${hotelInstance?.hotelRating}

    </g:else>

    <small>Deals /Offer / Special offer</small>
</div>

<div class="col-md-6">
    <g:if test="${hotelInstance}">
        <g:if test="${hotelInstance.sessionId}">
            <g:link controller="hotelService" action="hotelDetail"
                    style="text-decoration: none;"
                    params="[sessionId: hotelInstance?.sessionId, hotelIndex: hotelInstance?.hotelIndex, hotelInstanceList: hotelList,
                            noOfRoom: searchParametersCO?.noOfRooms, adult: searchParametersCO?.adult,
                            peopleInRoomTwo: searchParametersCO?.peopleInRoomTwo, peopleInRoomThree: searchParametersCO?.peopleInRoomThree, peopleInRoomFour: searchParametersCO?.peopleInRoomFour, child: searchParametersCO?.children, checkIn: searchParametersCO?.checkIn,
                            childInRoomTwo: searchParametersCO?.childInRoomTwo, childInRoomThree: searchParametersCO?.childInRoomThree, childInRoomFour: searchParametersCO?.childInRoomFour,
                            checkOut: searchParametersCO?.checkOut, firstChildAgeRoomOne: searchParametersCO.firstChildAgeRoomOne,
                            secondChildAgeRoomOne: searchParametersCO?.secondChildAgeRoomOne, thirdChildAgeRoomOne: searchParametersCO?.thirdChildAgeRoomOne, fourthChildAgeRoomOne: searchParametersCO?.fourthChildAgeRoomOne, firstChildAgeRoomTwo: searchParametersCO.firstChildAgeRoomTwo,
                            secondChildAgeRoomTwo: searchParametersCO?.secondChildAgeRoomTwo, thirdChildAgeRoomTwo: searchParametersCO?.thirdChildAgeRoomTwo, fourthChildAgeRoomTwo: searchParametersCO?.fourthChildAgeRoomTwo, firstChildAgeRoomThree: searchParametersCO.firstChildAgeRoomThree,
                            secondChildAgeRoomThree: searchParametersCO?.secondChildAgeRoomThree, thirdChildAgeRoomThree: searchParametersCO?.thirdChildAgeRoomThree, fourthChildAgeRoomThree: searchParametersCO?.fourthChildAgeRoomThree, firstChildAgeRoomFour: searchParametersCO.firstChildAgeRoomFour,
                            secondChildAgeRoomFour: searchParametersCO?.secondChildAgeRoomFour, thirdChildAgeRoomFour: searchParametersCO?.thirdChildAgeRoomFour, fourthChildAgeRoomFour: searchParametersCO?.fourthChildAgeRoomFour, supplierId: hotelInstance?.supplierId]">${hotelInstance?.hotelName}
            </g:link>
        </g:if>
        <g:else>
            <h4><g:link controller="hotelService" action="hotelDetail"
                        style="text-decoration: none;"
                        params="[id: hotelInstance?.hotelId, hotelInstanceList: hotelList,
                                noOfRoom: searchParametersCO?.noOfRooms, adult: searchParametersCO?.adult,
                                peopleInRoomTwo: searchParametersCO?.peopleInRoomTwo, peopleInRoomThree: searchParametersCO?.peopleInRoomThree, peopleInRoomFour: searchParametersCO?.peopleInRoomFour, child: searchParametersCO?.children, checkIn: searchParametersCO?.checkIn,
                                childInRoomTwo: searchParametersCO?.childInRoomTwo, childInRoomThree: searchParametersCO?.childInRoomThree, childInRoomFour: searchParametersCO?.childInRoomFour,
                                checkOut: searchParametersCO?.checkOut, firstChildAgeRoomOne: searchParametersCO.firstChildAgeRoomOne,
                                secondChildAgeRoomOne: searchParametersCO?.secondChildAgeRoomOne, thirdChildAgeRoomOne: searchParametersCO?.thirdChildAgeRoomOne, fourthChildAgeRoomOne: searchParametersCO?.fourthChildAgeRoomOne, firstChildAgeRoomTwo: searchParametersCO.firstChildAgeRoomTwo,
                                secondChildAgeRoomTwo: searchParametersCO?.secondChildAgeRoomTwo, thirdChildAgeRoomTwo: searchParametersCO?.thirdChildAgeRoomTwo, fourthChildAgeRoomTwo: searchParametersCO?.fourthChildAgeRoomTwo, firstChildAgeRoomThree: searchParametersCO.firstChildAgeRoomThree,
                                secondChildAgeRoomThree: searchParametersCO?.secondChildAgeRoomThree, thirdChildAgeRoomThree: searchParametersCO?.thirdChildAgeRoomThree, fourthChildAgeRoomThree: searchParametersCO?.fourthChildAgeRoomThree, firstChildAgeRoomFour: searchParametersCO.firstChildAgeRoomFour,
                                secondChildAgeRoomFour: searchParametersCO?.secondChildAgeRoomFour, thirdChildAgeRoomFour: searchParametersCO?.thirdChildAgeRoomFour, fourthChildAgeRoomFour: searchParametersCO?.fourthChildAgeRoomFour, supplierId: hotelInstance?.supplierId]">${hotelInstance?.hotelName}
            </g:link>
            </h4>
        </g:else>
    </g:if>

    <div class="row">
        <div class="col-md-8">

            <h4>
                ${hotelInstance?.location}/India</h4>
            <small>${hotelInstance?.shortDescription ? hotelInstance?.shortDescription?.tokenize("<b>")?.first() : ""}</small>

            <g:if test="${hotelInstance}">
                <g:if test="${hotelInstance.sessionId}">
                    <g:link controller="hotelService" action="hotelDetail"
                            style="text-decoration: none;"
                            params="[sessionId: hotelInstance?.sessionId, hotelIndex: hotelInstance?.hotelIndex,
                                    noOfRoom: searchParametersCO?.noOfRooms, adult: searchParametersCO?.adult,
                                    peopleInRoomTwo: searchParametersCO?.peopleInRoomTwo, peopleInRoomThree: searchParametersCO?.peopleInRoomThree, peopleInRoomFour: searchParametersCO?.peopleInRoomFour, child: searchParametersCO?.children, checkIn: searchParametersCO?.checkIn,
                                    childInRoomTwo: searchParametersCO?.childInRoomTwo, childInRoomThree: searchParametersCO?.childInRoomThree, childInRoomFour: searchParametersCO?.childInRoomFour,
                                    checkOut: searchParametersCO?.checkOut, firstChildAgeRoomOne: searchParametersCO.firstChildAgeRoomOne,
                                    secondChildAgeRoomOne: searchParametersCO?.secondChildAgeRoomOne, thirdChildAgeRoomOne: searchParametersCO?.thirdChildAgeRoomOne, fourthChildAgeRoomOne: searchParametersCO?.fourthChildAgeRoomOne, firstChildAgeRoomTwo: searchParametersCO.firstChildAgeRoomTwo,
                                    secondChildAgeRoomTwo: searchParametersCO?.secondChildAgeRoomTwo, thirdChildAgeRoomTwo: searchParametersCO?.thirdChildAgeRoomTwo, fourthChildAgeRoomTwo: searchParametersCO?.fourthChildAgeRoomTwo, firstChildAgeRoomThree: searchParametersCO.firstChildAgeRoomThree,
                                    secondChildAgeRoomThree: searchParametersCO?.secondChildAgeRoomThree, thirdChildAgeRoomThree: searchParametersCO?.thirdChildAgeRoomThree, fourthChildAgeRoomThree: searchParametersCO?.fourthChildAgeRoomThree, firstChildAgeRoomFour: searchParametersCO.firstChildAgeRoomFour,
                                    secondChildAgeRoomFour: searchParametersCO?.secondChildAgeRoomFour, thirdChildAgeRoomFour: searchParametersCO?.thirdChildAgeRoomFour, fourthChildAgeRoomFour: searchParametersCO?.fourthChildAgeRoomFour, supplierId: hotelInstance?.supplierId]">More Details..
                    </g:link>
                </g:if>
                <g:else>

                    <g:link controller="hotelService" action="hotelDetail"
                            style="text-decoration: none;"
                            params="[
                                    id: hotelInstance?.hotelId, hotelInstanceList: hotelList,
                                    noOfRoom: searchParametersCO?.noOfRooms, adult: searchParametersCO?.adult,
                                    peopleInRoomTwo: searchParametersCO?.peopleInRoomTwo, peopleInRoomThree: searchParametersCO?.peopleInRoomThree, peopleInRoomFour: searchParametersCO?.peopleInRoomFour, child: searchParametersCO?.children, checkIn: searchParametersCO?.checkIn,
                                    childInRoomTwo: searchParametersCO?.childInRoomTwo, childInRoomThree: searchParametersCO?.childInRoomThree, childInRoomFour: searchParametersCO?.childInRoomFour,
                                    checkOut: searchParametersCO?.checkOut, firstChildAgeRoomOne: searchParametersCO.firstChildAgeRoomOne,
                                    secondChildAgeRoomOne: searchParametersCO?.secondChildAgeRoomOne, thirdChildAgeRoomOne: searchParametersCO?.thirdChildAgeRoomOne, fourthChildAgeRoomOne: searchParametersCO?.fourthChildAgeRoomOne, firstChildAgeRoomTwo: searchParametersCO.firstChildAgeRoomTwo,
                                    secondChildAgeRoomTwo: searchParametersCO?.secondChildAgeRoomTwo, thirdChildAgeRoomTwo: searchParametersCO?.thirdChildAgeRoomTwo, fourthChildAgeRoomTwo: searchParametersCO?.fourthChildAgeRoomTwo, firstChildAgeRoomThree: searchParametersCO.firstChildAgeRoomThree,
                                    secondChildAgeRoomThree: searchParametersCO?.secondChildAgeRoomThree, thirdChildAgeRoomThree: searchParametersCO?.thirdChildAgeRoomThree, fourthChildAgeRoomThree: searchParametersCO?.fourthChildAgeRoomThree, firstChildAgeRoomFour: searchParametersCO.firstChildAgeRoomFour,
                                    secondChildAgeRoomFour: searchParametersCO?.secondChildAgeRoomFour, thirdChildAgeRoomFour: searchParametersCO?.thirdChildAgeRoomFour, fourthChildAgeRoomFour: searchParametersCO?.fourthChildAgeRoomFour, supplierId: hotelInstance?.supplierId]">More Details..
                    </g:link>
                </g:else>
            </g:if>


            <br>
        </div>
    </div>
</div>

<div class="col-md-3">
    Hotel Type:${hotelInstance?.category ? hotelInstance?.category : hotelInstance?.hotelRating}&nbsp;

    <div class="row">
        <div class="col-md-10">
            <h5>
                <h3><dt>Rs${hotelInstance ? hotelInstance.hotelRoomRatesVOs ? hotelInstance?.hotelRoomRatesVOs?.first() ? hotelInstance?.hotelRoomRatesVOs?.first()?.roomRateType?.first() ? hotelInstance?.hotelRoomRatesVOs?.first()?.roomRateType?.first()?.roomRate : "" : "" : "" : ""}</dt>
                </h3>
                <h6><small><strong>Average Price Per Night</strong></small>
                </h6>
            </h5>

        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <h5>
                <g:if test="${hotelInstance}">
                    <g:if test="${hotelInstance.sessionId}">
                        <g:link class="btn btn-primary" controller="hotelService" action="hotelDetail"
                                style="text-decoration: none;"
                                params="[sessionId: hotelInstance?.sessionId, hotelIndex: hotelInstance?.hotelIndex,
                                        noOfRoom: searchParametersCO?.noOfRooms, adult: searchParametersCO?.adult,
                                        peopleInRoomTwo: searchParametersCO?.peopleInRoomTwo, peopleInRoomThree: searchParametersCO?.peopleInRoomThree, peopleInRoomFour: searchParametersCO?.peopleInRoomFour, child: searchParametersCO?.children, checkIn: searchParametersCO?.checkIn,
                                        childInRoomTwo: searchParametersCO?.childInRoomTwo, childInRoomThree: searchParametersCO?.childInRoomThree, childInRoomFour: searchParametersCO?.childInRoomFour,
                                        checkOut: searchParametersCO?.checkOut, firstChildAgeRoomOne: searchParametersCO.firstChildAgeRoomOne,
                                        secondChildAgeRoomOne: searchParametersCO?.secondChildAgeRoomOne, thirdChildAgeRoomOne: searchParametersCO?.thirdChildAgeRoomOne, fourthChildAgeRoomOne: searchParametersCO?.fourthChildAgeRoomOne, firstChildAgeRoomTwo: searchParametersCO.firstChildAgeRoomTwo,
                                        secondChildAgeRoomTwo: searchParametersCO?.secondChildAgeRoomTwo, thirdChildAgeRoomTwo: searchParametersCO?.thirdChildAgeRoomTwo, fourthChildAgeRoomTwo: searchParametersCO?.fourthChildAgeRoomTwo, firstChildAgeRoomThree: searchParametersCO.firstChildAgeRoomThree,
                                        secondChildAgeRoomThree: searchParametersCO?.secondChildAgeRoomThree, thirdChildAgeRoomThree: searchParametersCO?.thirdChildAgeRoomThree, fourthChildAgeRoomThree: searchParametersCO?.fourthChildAgeRoomThree, firstChildAgeRoomFour: searchParametersCO.firstChildAgeRoomFour,
                                        secondChildAgeRoomFour: searchParametersCO?.secondChildAgeRoomFour, thirdChildAgeRoomFour: searchParametersCO?.thirdChildAgeRoomFour, fourthChildAgeRoomFour: searchParametersCO?.fourthChildAgeRoomFour, supplierId: hotelInstance?.supplierId]">Continue To Book
                        </g:link>
                    </g:if>
                    <g:else>

                        <g:link class="btn btn-primary" controller="hotelService" action="hotelDetail"
                                style="text-decoration: none;"
                                params="[
                                        id: hotelInstance?.hotelId, hotelInstanceList: hotelList,
                                        noOfRoom: searchParametersCO?.noOfRooms, adult: searchParametersCO?.adult,
                                        peopleInRoomTwo: searchParametersCO?.peopleInRoomTwo, peopleInRoomThree: searchParametersCO?.peopleInRoomThree, peopleInRoomFour: searchParametersCO?.peopleInRoomFour, child: searchParametersCO?.children, checkIn: searchParametersCO?.checkIn,
                                        childInRoomTwo: searchParametersCO?.childInRoomTwo, childInRoomThree: searchParametersCO?.childInRoomThree, childInRoomFour: searchParametersCO?.childInRoomFour,
                                        checkOut: searchParametersCO?.checkOut, firstChildAgeRoomOne: searchParametersCO.firstChildAgeRoomOne,
                                        secondChildAgeRoomOne: searchParametersCO?.secondChildAgeRoomOne, thirdChildAgeRoomOne: searchParametersCO?.thirdChildAgeRoomOne, fourthChildAgeRoomOne: searchParametersCO?.fourthChildAgeRoomOne, firstChildAgeRoomTwo: searchParametersCO.firstChildAgeRoomTwo,
                                        secondChildAgeRoomTwo: searchParametersCO?.secondChildAgeRoomTwo, thirdChildAgeRoomTwo: searchParametersCO?.thirdChildAgeRoomTwo, fourthChildAgeRoomTwo: searchParametersCO?.fourthChildAgeRoomTwo, firstChildAgeRoomThree: searchParametersCO.firstChildAgeRoomThree,
                                        secondChildAgeRoomThree: searchParametersCO?.secondChildAgeRoomThree, thirdChildAgeRoomThree: searchParametersCO?.thirdChildAgeRoomThree, fourthChildAgeRoomThree: searchParametersCO?.fourthChildAgeRoomThree, firstChildAgeRoomFour: searchParametersCO.firstChildAgeRoomFour,
                                        secondChildAgeRoomFour: searchParametersCO?.secondChildAgeRoomFour, thirdChildAgeRoomFour: searchParametersCO?.thirdChildAgeRoomFour, fourthChildAgeRoomFour: searchParametersCO?.fourthChildAgeRoomFour, supplierId: hotelInstance?.supplierId]">Continue To Book
                        </g:link>
                    </g:else>
                </g:if>

            </h5>

        </div>
    </div>
</div>
</div>

</div>
<hr>
</g:each>
<util:remoteNonStopPageScroll action='hotelListBasedOnRatingSubmit' controller="hotelService" onFailure="showError()" onSuccess="showSuccess()"
                              params="${params}"
                              total="${total}" update="pagingContainer"/>

<script>

    function showError() {
//        alert("errorPaging")
    }
    function showSuccess() {
//        alert("success")
    }
</script>

<div id="spinner" style="display: none;">
    <img src="${resource(dir: '/assets/img/loaders', file: 'ajax-loader.gif')}" alt="Loading..." width="16"
         height="16"/>
</div>

<script>
    function cancelPolicy(indexNo, ratePlanCode, roomTypeCode, sessionId) {
        var cancelModal = "#cancellationModal" + indexNo
        var cancelModalTrigger = "#triggerCancelModal" + indexNo
        $.ajax({
            url: "${createLink(controller: "hotelService" , action: "getCancelPolicy")}",
            type: "POST",
            data: {index: indexNo, ratePlanCode: ratePlanCode, roomTypeCode: roomTypeCode, sessionId: sessionId},
            success: function (success) {
                $(cancelModal).html(success);
                $(cancelModalTrigger).click();
                $(".modal-backdrop").removeClass('modal-backdrop fade in');

            }

        });

    }


    function displayRoomDetail(hotelIndex) {
        var hotelInd = "#hotelRoom" + hotelIndex
        $(hotelInd).toggle();
    }
</script>