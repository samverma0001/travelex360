<%@ page import="java.math.RoundingMode" %>
<style>
/**** BASE ****/
body {
    color: #888;
}

a {
    color: #03a1d1;
    text-decoration: none !important;
}

/**** LAYOUT ****/
.list-inline > li {
    padding: 0 10px 0 0;
}

.container-pad {
    padding: 30px 15px;
}

/**** MODULE ****/
.bgc-fff {
    background-color: #fff !important;
}

.box-shad {
    -webkit-box-shadow: 1px 1px 0 rgba(0, 0, 0, .2);
    box-shadow: 1px 1px 0 rgba(0, 0, 0, .2);
}

.brdr {
    border: 1px solid #ededed;
}

/* Font changes */
.fnt-smaller {
    font-size: .9em;
}

.fnt-lighter {
    color: #bbb;
}

/* Padding - Margins */
.pad-10 {
    padding: 10px !important;
}

.mrg-0 {
    margin: 0 !important;
}

.btm-mrg-10 {
    margin-bottom: 10px !important;
}

.btm-mrg-20 {
    margin-bottom: 20px !important;
}

/* Color  */
.clr-535353 {
    color: #535353;
}

/**** MEDIA QUERIES ****/
@media only screen and (max-width: 991px) {
    #property-listings .property-listing {
        padding: 5px !important;
    }

    #property-listings .property-listing a {
        margin: 0;
    }

    #property-listings .property-listing .media-body {
        padding: 10px;
    }
}

@media only screen and (min-width: 992px) {
    #property-listings .property-listing img {
        max-width: 180px;
    }
}

</style>
<g:if test="${hotelList}">
<g:each in="${hotelList}" status="i" var="hotelInstance">
<div class="row">
<div class="col-md-12" id="updatePaginateList">
<!-- Begin Listing: 609 W GRAVERS LN-->
<div class="brdr bgc-fff pad-10 box-shad btm-mrg-20 property-listing">
<div class="media">
<a class="pull-left">
    <g:if test="${hotelInstance}">
        <img src="${hotelInstance?.searchHotelMedias ? hotelInstance?.searchHotelMedias?.first()?.mediaSrc : defaultUrl}"
             alt="image" width="150" height="100" class="img-responsive">
    </g:if>
    <g:else>
        Image not available
    </g:else>

</a>

<div class="clearfix visible-sm"></div>

<div class="media-body fnt-smaller">
    <a href="#" target="_parent"></a>

    <h4 class="media-heading">
        <a href="#"><strong>Hotel Name:-</strong><g:if test="${hotelInstance}">
            <g:if test="${hotelInstance.sessionId}">
                <g:link controller="hotelService" action="hotelDetail"
                        style="text-decoration: none;"
                        params="[sessionId              : hotelInstance?.sessionId, hotelIndex: hotelInstance?.hotelIndex, hotelInstanceList: hotelList,
                                 noOfRoom               : searchParametersCO?.noOfRooms, adult: searchParametersCO?.adult,
                                 peopleInRoomTwo        : searchParametersCO?.peopleInRoomTwo, peopleInRoomThree: searchParametersCO?.peopleInRoomThree, peopleInRoomFour: searchParametersCO?.peopleInRoomFour, child: searchParametersCO?.children, checkIn: searchParametersCO?.checkIn,
                                 childInRoomTwo         : searchParametersCO?.childInRoomTwo, childInRoomThree: searchParametersCO?.childInRoomThree, childInRoomFour: searchParametersCO?.childInRoomFour,
                                 checkOut               : searchParametersCO?.checkOut, firstChildAgeRoomOne: searchParametersCO.firstChildAgeRoomOne,
                                 secondChildAgeRoomOne  : searchParametersCO?.secondChildAgeRoomOne, firstChildAgeRoomTwo: searchParametersCO.firstChildAgeRoomTwo,
                                 secondChildAgeRoomTwo  : searchParametersCO?.secondChildAgeRoomTwo, firstChildAgeRoomThree: searchParametersCO.firstChildAgeRoomThree,
                                 secondChildAgeRoomThree: searchParametersCO?.secondChildAgeRoomThree, firstChildAgeRoomFour: searchParametersCO.firstChildAgeRoomFour,
                                 secondChildAgeRoomFour : searchParametersCO?.secondChildAgeRoomFour, supplierId: hotelInstance?.supplierId]">${hotelInstance?.hotelName}
                </g:link>
            </g:if>
            <g:else>
                <h4><g:link controller="hotelService" action="hotelDetail"
                            style="text-decoration: none;"
                            params="[id                     : hotelInstance?.hotelId, hotelInstanceList: hotelList,
                                     noOfRoom               : searchParametersCO?.noOfRooms, adult: searchParametersCO?.adult,
                                     peopleInRoomTwo        : searchParametersCO?.peopleInRoomTwo, peopleInRoomThree: searchParametersCO?.peopleInRoomThree, peopleInRoomFour: searchParametersCO?.peopleInRoomFour, child: searchParametersCO?.children, checkIn: searchParametersCO?.checkIn,
                                     childInRoomTwo         : searchParametersCO?.childInRoomTwo, childInRoomThree: searchParametersCO?.childInRoomThree, childInRoomFour: searchParametersCO?.childInRoomFour,
                                     checkOut               : searchParametersCO?.checkOut, firstChildAgeRoomOne: searchParametersCO.firstChildAgeRoomOne,
                                     secondChildAgeRoomOne  : searchParametersCO?.secondChildAgeRoomOne, firstChildAgeRoomTwo: searchParametersCO.firstChildAgeRoomTwo,
                                     secondChildAgeRoomTwo  : searchParametersCO?.secondChildAgeRoomTwo, firstChildAgeRoomThree: searchParametersCO.firstChildAgeRoomThree,
                                     secondChildAgeRoomThree: searchParametersCO?.secondChildAgeRoomThree, firstChildAgeRoomFour: searchParametersCO.firstChildAgeRoomFour,
                                     secondChildAgeRoomFour : searchParametersCO?.secondChildAgeRoomFour, supplierId: hotelInstance?.supplierId]">${hotelInstance?.hotelName}
                </g:link>
                </h4>
            </g:else>
        </g:if>
            <small class="pull-right">
                <g:if test="${hotelInstance}">
                    <g:if test="${hotelInstance.sessionId}">
                        <g:link class="btn btn-primary" controller="hotelService" action="hotelDetail"
                                style="text-decoration: none;"
                                params="[sessionId                                             : hotelInstance?.sessionId, hotelIndex: hotelInstance?.hotelIndex,
                                         noOfRoom                                              : searchParametersCO?.noOfRooms, adult: searchParametersCO?.adult,
                                         peopleInRoomTwo                                       : searchParametersCO?.peopleInRoomTwo, peopleInRoomThree:
                                        searchParametersCO?.peopleInRoomThree, peopleInRoomFour: searchParametersCO?.peopleInRoomFour,
                                         child                                                 : searchParametersCO?.children, checkIn: searchParametersCO?.checkIn,
                                         childInRoomTwo                                        : searchParametersCO?.childInRoomTwo, childInRoomThree: searchParametersCO?.childInRoomThree, childInRoomFour: searchParametersCO?.childInRoomFour,
                                         checkOut                                              : searchParametersCO?.checkOut, firstChildAgeRoomOne:
                                        searchParametersCO.firstChildAgeRoomOne,
                                         secondChildAgeRoomOne                                 : searchParametersCO?.secondChildAgeRoomOne, firstChildAgeRoomTwo:
                                        searchParametersCO.firstChildAgeRoomTwo,
                                         secondChildAgeRoomTwo                                 : searchParametersCO?.secondChildAgeRoomTwo, firstChildAgeRoomThree:
                                        searchParametersCO.firstChildAgeRoomThree,
                                         secondChildAgeRoomThree                               : searchParametersCO?.secondChildAgeRoomThree,
                                         firstChildAgeRoomFour                                 : searchParametersCO.firstChildAgeRoomFour,
                                         secondChildAgeRoomFour                                : searchParametersCO?.secondChildAgeRoomFour, supplierId:
                                        hotelInstance?.supplierId, cityId                      : cityId]">Proceed To Book
                        </g:link>
                    </g:if>
                    <g:else>

                        <g:link class="btn btn-primary" controller="hotelService" action="hotelDetail"
                                style="text-decoration: none;"
                                params="[
                                        id                     : hotelInstance?.hotelId, hotelInstanceList: hotelList,
                                        noOfRoom               : searchParametersCO?.noOfRooms, adult: searchParametersCO?.adult,
                                        peopleInRoomTwo        : searchParametersCO?.peopleInRoomTwo, peopleInRoomThree: searchParametersCO?.peopleInRoomThree, peopleInRoomFour: searchParametersCO?.peopleInRoomFour, child: searchParametersCO?.children, checkIn: searchParametersCO?.checkIn,
                                        childInRoomTwo         : searchParametersCO?.childInRoomTwo, childInRoomThree: searchParametersCO?.childInRoomThree, childInRoomFour: searchParametersCO?.childInRoomFour,
                                        checkOut               : searchParametersCO?.checkOut, firstChildAgeRoomOne: searchParametersCO.firstChildAgeRoomOne,
                                        secondChildAgeRoomOne  : searchParametersCO?.secondChildAgeRoomOne, firstChildAgeRoomTwo: searchParametersCO.firstChildAgeRoomTwo,
                                        secondChildAgeRoomTwo  : searchParametersCO?.secondChildAgeRoomTwo, firstChildAgeRoomThree: searchParametersCO.firstChildAgeRoomThree,
                                        secondChildAgeRoomThree: searchParametersCO?.secondChildAgeRoomThree, firstChildAgeRoomFour: searchParametersCO.firstChildAgeRoomFour,
                                        secondChildAgeRoomFour : searchParametersCO?.secondChildAgeRoomFour, supplierId: hotelInstance?.supplierId, cityId: cityId]">Proceed To Book
                        </g:link>
                    </g:else>
                </g:if>
            </small>
        </a>
    </h4>

    <ul class="list-inline mrg-0 btm-mrg-10 clr-535353">
        <li><g:if test="${hotelInstance}">
            <g:if test="${hotelInstance.sessionId}">
                <g:link controller="hotelService" action="hotelDetail"
                        style="text-decoration: none;"
                        params="[sessionId              : hotelInstance?.sessionId, hotelIndex: hotelInstance?.hotelIndex,
                                 noOfRoom               : searchParametersCO?.noOfRooms, adult: searchParametersCO?.adult,
                                 peopleInRoomTwo        : searchParametersCO?.peopleInRoomTwo, peopleInRoomThree: searchParametersCO?.peopleInRoomThree, peopleInRoomFour: searchParametersCO?.peopleInRoomFour, child: searchParametersCO?.children, checkIn: searchParametersCO?.checkIn,
                                 childInRoomTwo         : searchParametersCO?.childInRoomTwo, childInRoomThree: searchParametersCO?.childInRoomThree, childInRoomFour: searchParametersCO?.childInRoomFour,
                                 checkOut               : searchParametersCO?.checkOut, firstChildAgeRoomOne: searchParametersCO.firstChildAgeRoomOne,
                                 secondChildAgeRoomOne  : searchParametersCO?.secondChildAgeRoomOne, firstChildAgeRoomTwo: searchParametersCO.firstChildAgeRoomTwo,
                                 secondChildAgeRoomTwo  : searchParametersCO?.secondChildAgeRoomTwo, firstChildAgeRoomThree: searchParametersCO.firstChildAgeRoomThree,
                                 secondChildAgeRoomThree: searchParametersCO?.secondChildAgeRoomThree, firstChildAgeRoomFour: searchParametersCO.firstChildAgeRoomFour,
                                 secondChildAgeRoomFour : searchParametersCO?.secondChildAgeRoomFour, supplierId: hotelInstance?.supplierId]">More Details..
                </g:link>
            </g:if>
            <g:else>

                <g:link controller="hotelService" action="hotelDetail"
                        style="text-decoration: none;"
                        params="[
                                id                     : hotelInstance?.hotelId, hotelInstanceList: hotelList,
                                noOfRoom               : searchParametersCO?.noOfRooms, adult: searchParametersCO?.adult,
                                peopleInRoomTwo        : searchParametersCO?.peopleInRoomTwo, peopleInRoomThree: searchParametersCO?.peopleInRoomThree, peopleInRoomFour: searchParametersCO?.peopleInRoomFour, child: searchParametersCO?.children, checkIn: searchParametersCO?.checkIn,
                                childInRoomTwo         : searchParametersCO?.childInRoomTwo, childInRoomThree: searchParametersCO?.childInRoomThree, childInRoomFour: searchParametersCO?.childInRoomFour,
                                checkOut               : searchParametersCO?.checkOut, firstChildAgeRoomOne: searchParametersCO.firstChildAgeRoomOne,
                                secondChildAgeRoomOne  : searchParametersCO?.secondChildAgeRoomOne, firstChildAgeRoomTwo: searchParametersCO.firstChildAgeRoomTwo,
                                secondChildAgeRoomTwo  : searchParametersCO?.secondChildAgeRoomTwo, firstChildAgeRoomThree: searchParametersCO.firstChildAgeRoomThree,
                                secondChildAgeRoomThree: searchParametersCO?.secondChildAgeRoomThree, firstChildAgeRoomFour: searchParametersCO.firstChildAgeRoomFour,
                                secondChildAgeRoomFour : searchParametersCO?.secondChildAgeRoomFour, supplierId: hotelInstance?.supplierId]">More Details..
                </g:link>
            </g:else>
        </g:if>
        </li>

        <li style="list-style: none">|</li>

        <li>Deals /Offer / Special offer</li>

        <li style="list-style: none">|</li>

        <li><g:if
                test="${(hotelInstance?.hotelRating == "OneStar" || "TwoStar" || "ThreeStar" || "FourStar" || "FiveStar" || "SixStar" || "SevenStar")}">
            <g:if test="${hotelInstance?.hotelRating == 'OneStar' || hotelInstance?.hotelRating == '1'}">
                <h5><small>Rating:</small>&nbsp;<g:each
                        in="${1..1}"
                        var="a"><img
                            src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                </h5>
            </g:if>
            <g:elseif test="${hotelInstance?.hotelRating == 'TwoStar' || hotelInstance?.hotelRating == '2'}">
                <h5><small>Rating:</small>&nbsp;<g:each
                        in="${1..2}"
                        var="a"><img
                            src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                </h5>
            </g:elseif>
            <g:elseif test="${hotelInstance?.hotelRating == 'ThreeStar' || hotelInstance?.hotelRating == '3'}">
                <h5><small>Rating:</small>&nbsp;<g:each
                        in="${1..3}"
                        var="a"><img
                            src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                </h5>
            </g:elseif>
            <g:elseif test="${hotelInstance?.hotelRating == 'FourStar' || hotelInstance?.hotelRating == '4'}">
                <h5><small>Rating:</small>&nbsp;<g:each
                        in="${1..4}"
                        var="a"><img
                            src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                </h5>
            </g:elseif>

            <g:elseif test="${hotelInstance?.hotelRating == 'FiveStar' || hotelInstance?.hotelRating == '5'}">
                <h5><small>Rating:</small>&nbsp;<g:each
                        in="${1..5}"
                        var="a"><img
                            src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                </h5>
            </g:elseif>
            <g:elseif test="${hotelInstance?.hotelRating == 'SixStar' || hotelInstance?.hotelRating == '6'}">
                <h5><small>Rating:</small>&nbsp;<g:each
                        in="${1..6}"
                        var="a"><img
                            src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                </h5>
            </g:elseif>
            <g:elseif test="${hotelInstance?.hotelRating == 'SevenStar' || hotelInstance?.hotelRating == '7'}">
                <h5><small>Rating:</small>&nbsp;<g:each
                        in="${1..7}"
                        var="a"><img
                            src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                </h5>
            </g:elseif>

        </g:if>

            <g:elseif
                    test="${(hotelInstance?.hotelRating == "0" || "1" || "2" || "3" || "4" || "5" || "6" || "7")}">
                <h5><small>Rating:</small>&nbsp;
                    <g:each
                            in="${1..(hotelInstance?.hotelRating as Integer)}"
                            var="a"><img
                            src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                </h5>
            </g:elseif>
            <g:else>
                Rating :&nbsp;   ${hotelInstance?.hotelRating}

            </g:else></li>
    </ul>

    <p class="hidden-xs">${hotelInstance?.shortDescription ? hotelInstance?.shortDescription?.tokenize("<b>")?.first() : ""}
    ...</p><span
        class="fnt-arial">${hotelInstance?.location}India</span>
    <span class="pull-right media-body fnt-smaller media-heading">
        <h5><strong>Total Rs ${hotelInstance?.hotelRoomDetailVOArrayList?.hotelRateVO?.totalRate?.min()?.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP) + hotelInstance?.hotelRoomDetailVOArrayList?.hotelRateVO?.agentMarkUp?.first()?.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP)}</strong>
        </h5></span>
</div>

</div>
</div><!-- End Listing-->
<util:remoteNonStopPageScroll action='remotePaginateHotel' controller="hotelService"
                              params="[checkIn          : searchParametersCO?.checkIn, checkOut: searchParametersCO?.checkOut, priceRangeMax: maxPrice, minRangePrice:
                                      minPrice, noOfRoom: searchParametersCO?.noOfRooms, adult: searchParametersCO?.adult, children: searchParametersCO?.children]"
                              total="${total}"
                              update="updatePaginateList"/>
</div><!-- End row -->
</div><!-- End container -->
</g:each>
</g:if>
<g:else>
    Hotel not available
</g:else>


