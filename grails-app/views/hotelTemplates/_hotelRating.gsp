<g:if test="${(hotelInstance?.hotelRating == "OneStar" || "TwoStar" || "ThreeStar" || "FourStar" || "FiveStar" || "SixStar" || "SevenStar")}">
    <g:if test="${hotelInstance?.hotelRating == 'OneStar' || hotelInstance?.hotelRating == '1'}">
        <g:each in="${1..1}" var="a"><img
                src="${resource(dir: '/images', file: 'star1.png')}"></g:each>
    </g:if>
    <g:elseif
            test="${hotelInstance?.hotelRating == 'TwoStar' || hotelInstance?.hotelRating == '2'}">
        <g:each in="${1..2}" var="a"><img
                src="${resource(dir: '/images', file: 'star1.png')}"></g:each>
    </g:elseif>
    <g:elseif
            test="${hotelInstance?.hotelRating == 'ThreeStar' || hotelInstance?.hotelRating == '3'}">
        <g:each in="${1..3}" var="a"><img
                src="${resource(dir: '/images', file: 'star1.png')}"></g:each>
    </g:elseif>
    <g:elseif
            test="${hotelInstance?.hotelRating == 'FourStar' || hotelInstance?.hotelRating == '4'}">
        <g:each in="${1..4}"
                var="a"><img
                src="${resource(dir: '/images', file: 'star1.png')}"></g:each>
    </g:elseif>

    <g:elseif
            test="${hotelInstance?.hotelRating == 'FiveStar' || hotelInstance?.hotelRating == '5'}">
        <g:each in="${1..5}" var="a"><img
                src="${resource(dir: '/images', file: 'star1.png')}"></g:each>
    </g:elseif>
    <g:elseif
            test="${hotelInstance?.hotelRating == 'SixStar' || hotelInstance?.hotelRating == '6'}">
        <g:each in="${1..6}" var="a"><img
                src="${resource(dir: '/images', file: 'star1.png')}"></g:each>
    </g:elseif>
    <g:elseif
            test="${hotelInstance?.hotelRating == 'SevenStar' || hotelInstance?.hotelRating == '7'}">
        <g:each in="${1..7}" var="a"><img
                src="${resource(dir: '/images', file: 'star1.png')}"></g:each>
    </g:elseif>
</g:if>

<g:elseif
        test="${(hotelInstance?.hotelRating == "0" || "1" || "2" || "3" || "4" || "5" || "6" || "7")}">
    <g:each
            in="${1..(hotelInstance?.hotelRating as Integer)}"
            var="a"><img
            src="${resource(dir: '/images', file: 'star1.png')}"></g:each>
</g:elseif>
<g:else>
    ${hotelInstance?.hotelRating}
</g:else>