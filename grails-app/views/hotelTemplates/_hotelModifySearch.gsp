<div class="min-search-container">
    <div class="container-fluid">
        <div class="horiz-form-container">
            <form class="tvx-from tvx-horiz-form" data-ng-controller="book_hotel_control">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Going to</label>
                            <input type="text" class="form-control" id="destination"
                                   placeholder="Select City, Area or Hotel...">
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Check in</label>

                            <div class="input-group date">
                                <input type="text" class="form-control">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Check out</label>

                            <div class="input-group date">
                                <input type="text" class="form-control">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Check out</label>

                            <div class="room-select-toggle">
                                {{no_of_rooms}} Room / {{TotalGuest()}} Guests <span
                                    class="glyphicon glyphicon-chevron-down"></span>
                            </div>

                            <div class="room-select-dropdown-container">
                                <div class="dropdown-rooms-container">
                                    <div class="form-group">
                                        <label for="">Number of Rooms</label>

                                        <div class="input-group col-md-12">
                                            <select id="select_room_count" ng-model="no_of_rooms"
                                                    ng-init="no_of_rooms='1'" class="form-control">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="rooms-container">
                                        <div class="form-group room-container room1">
                                            <h4>Rooms1</h4>

                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <label for="">Adults</label>

                                                            <div class="input-group col-md-12">
                                                                <select class="form-control"
                                                                        ng-model="room1_adult"
                                                                        ng-init="room1_adult='0'">
                                                                    <option value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-6">
                                                            <label for="">Childrens</label>

                                                            <div class="input-group col-md-12">
                                                                <select class="form-control children_count"
                                                                        ng-model="room1_child"
                                                                        ng-init="room1_child='0'">
                                                                    <option>0</option>
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-6 child1">
                                                            <label for="">Child 1 Age</label>

                                                            <div class="input-group col-md-12">
                                                                <select class="form-control">
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                    <option>3</option>
                                                                    <option>4</option>
                                                                    <option>5</option>
                                                                    <option>6</option>
                                                                    <option>7</option>
                                                                    <option>8</option>
                                                                    <option>9</option>
                                                                    <option>10</option>
                                                                    <option>11</option>
                                                                    <option>12</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-6 child2">
                                                            <label for="">Child 2 Age</label>

                                                            <div class="input-group col-md-12">
                                                                <select class="form-control">
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                    <option>3</option>
                                                                    <option>4</option>
                                                                    <option>5</option>
                                                                    <option>6</option>
                                                                    <option>7</option>
                                                                    <option>8</option>
                                                                    <option>9</option>
                                                                    <option>10</option>
                                                                    <option>11</option>
                                                                    <option>12</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group room-container room2">
                                            <h4>Rooms2</h4>

                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <label for="">Adults</label>

                                                            <div class="input-group col-md-12">
                                                                <select class="form-control"
                                                                        ng-model="room2_adult"
                                                                        ng-init="room2_adult='0'">
                                                                    <option value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-6">
                                                            <label for="">Childrens</label>

                                                            <div class="input-group col-md-12">
                                                                <select class="form-control children_count"
                                                                        ng-model="room2_child"
                                                                        ng-init="room2_child='0'">
                                                                    <option value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-6 child1">
                                                            <label for="">Child 1 Age</label>

                                                            <div class="input-group col-md-12">
                                                                <select class="form-control">
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                    <option>3</option>
                                                                    <option>4</option>
                                                                    <option>5</option>
                                                                    <option>6</option>
                                                                    <option>7</option>
                                                                    <option>8</option>
                                                                    <option>9</option>
                                                                    <option>10</option>
                                                                    <option>11</option>
                                                                    <option>12</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-6 child2">
                                                            <label for="">Child 2 Age</label>

                                                            <div class="input-group col-md-12">
                                                                <select class="form-control">
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                    <option>3</option>
                                                                    <option>4</option>
                                                                    <option>5</option>
                                                                    <option>6</option>
                                                                    <option>7</option>
                                                                    <option>8</option>
                                                                    <option>9</option>
                                                                    <option>10</option>
                                                                    <option>11</option>
                                                                    <option>12</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group room-container room3">
                                            <h4>Rooms3</h4>

                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <label for="">Adults</label>

                                                            <div class="input-group col-md-12">
                                                                <select class="form-control"
                                                                        ng-model="room3_adult"
                                                                        ng-init="room3_adult='0'">
                                                                    <option value="0">0</option>
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                    <option>3</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-6">
                                                            <label for="">Childrens</label>

                                                            <div class="input-group col-md-12">
                                                                <select class="form-control children_count"
                                                                        ng-model="room3_child"
                                                                        ng-init="room3_child='0'">
                                                                    <option value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-6 child1">
                                                            <label for="">Child 1 Age</label>

                                                            <div class="input-group col-md-12">
                                                                <select class="form-control">
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                    <option>3</option>
                                                                    <option>4</option>
                                                                    <option>5</option>
                                                                    <option>6</option>
                                                                    <option>7</option>
                                                                    <option>8</option>
                                                                    <option>9</option>
                                                                    <option>10</option>
                                                                    <option>11</option>
                                                                    <option>12</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-6 child2">
                                                            <label for="">Child 2 Age</label>

                                                            <div class="input-group col-md-12">
                                                                <select class="form-control">
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                    <option>3</option>
                                                                    <option>4</option>
                                                                    <option>5</option>
                                                                    <option>6</option>
                                                                    <option>7</option>
                                                                    <option>8</option>
                                                                    <option>9</option>
                                                                    <option>10</option>
                                                                    <option>11</option>
                                                                    <option>12</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group room-container room4">
                                            <h4>Rooms4</h4>

                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <label for="">Adults</label>

                                                            <div class="input-group col-md-12">
                                                                <select class="form-control"
                                                                        ng-model="room4_adult"
                                                                        ng-init="room4_adult='0'">
                                                                    <option value="0">0</option>
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                    <option>3</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-6">
                                                            <label for="">Childrens</label>

                                                            <div class="input-group col-md-12">
                                                                <select class="form-control children_count"
                                                                        ng-model="room4_child"
                                                                        ng-init="room4_child='0'">
                                                                    <option value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-6 child1">
                                                            <label for="">Child 1 Age</label>

                                                            <div class="input-group col-md-12">
                                                                <select class="form-control">
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                    <option>3</option>
                                                                    <option>4</option>
                                                                    <option>5</option>
                                                                    <option>6</option>
                                                                    <option>7</option>
                                                                    <option>8</option>
                                                                    <option>9</option>
                                                                    <option>10</option>
                                                                    <option>11</option>
                                                                    <option>12</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-6 child2">
                                                            <label for="">Child 2 Age</label>

                                                            <div class="input-group col-md-12">
                                                                <select class="form-control">
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                    <option>3</option>
                                                                    <option>4</option>
                                                                    <option>5</option>
                                                                    <option>6</option>
                                                                    <option>7</option>
                                                                    <option>8</option>
                                                                    <option>9</option>
                                                                    <option>10</option>
                                                                    <option>11</option>
                                                                    <option>12</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group room-container room5">
                                            <h4>Rooms5</h4>

                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <label for="">Adults</label>

                                                            <div class="input-group col-md-12">
                                                                <select class="form-control"
                                                                        ng-model="room5_adult"
                                                                        ng-init="room5_adult='0'">
                                                                    <option value="0">0</option>
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                    <option>3</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-6">
                                                            <label for="">Childrens</label>

                                                            <div class="input-group col-md-12">
                                                                <select class="form-control children_count"
                                                                        ng-model="room5_child"
                                                                        ng-init="room5_child='0'">
                                                                    <option value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-6 child1">
                                                            <label for="">Child 1 Age</label>

                                                            <div class="input-group col-md-12">
                                                                <select class="form-control">
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                    <option>3</option>
                                                                    <option>4</option>
                                                                    <option>5</option>
                                                                    <option>6</option>
                                                                    <option>7</option>
                                                                    <option>8</option>
                                                                    <option>9</option>
                                                                    <option>10</option>
                                                                    <option>11</option>
                                                                    <option>12</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-6 child2">
                                                            <label for="">Child 2 Age</label>

                                                            <div class="input-group col-md-12">
                                                                <select class="form-control">
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                    <option>3</option>
                                                                    <option>4</option>
                                                                    <option>5</option>
                                                                    <option>6</option>
                                                                    <option>7</option>
                                                                    <option>8</option>
                                                                    <option>9</option>
                                                                    <option>10</option>
                                                                    <option>11</option>
                                                                    <option>12</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <button type="submit" class="btn img-full btn-default">Search Hotel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
