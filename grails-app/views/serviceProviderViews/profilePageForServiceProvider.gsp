<%@ page import="serviceProvider.ServiceProvider" contentType="text/html;charset=UTF-8" %>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="main"/>
    <title>Profile | Service Provider</title>
    <script src="${resource(dir: "js", file: "jquery.MultiFile.js")}"></script>

    <style>body {
        background-color: #ffffff;
    }

    .user-row {
        margin-bottom: 14px;
    }

    .user-row:last-child {
        margin-bottom: 0;
    }

    .dropdown-user {
        margin: 13px 0;
        padding: 5px;
        height: 100%;
    }

    .dropdown-user:hover {
        cursor: pointer;
    }

    .table-user-information > tbody > tr {
        border-top: 1px solid rgb(221, 221, 221);
    }

    .table-user-information > tbody > tr:first-child {
        border-top: 0;
    }

    .table-user-information > tbody > tr > td {
        border-top: 0;
    }

    i.list-group-item {
        height: auto;
        min-height: 220px;
    }

    i.list-group-item.active small {
        color: #fff;
    }
    </style>
</head>

<body>
<br>
<br>
<br>
<br>
<br>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 ">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Service provider profile</h3>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <g:form controller="serviceProvider" action="saveServiceProviderImage"
                                enctype="multipart/form-data">
                            <div class="col-md-3 col-lg-3 " align="center">
                                <g:if test="${user?.pictureName}">
                                    <img alt="User Pic"
                                         src="${createLink(controller: "serviceProvider", action: 'showServiceProviderImage',
                                                 params: [userId: user?.id])}"
                                         height="175px" width="250px"
                                         class="img-circle">
                                </g:if>
                                <g:else>
                                    <img alt="User Pic"
                                         src="${resource(dir: 'images/appimages', file:
                                                 'serviceProviderDefaultImage.png')}"
                                         height="175px"
                                         class="img-circle">
                                </g:else>
                                <br>
                                <br>
                                <br>

                                <div class="row">
                                    <input type="file" name="serviceProviderProfileImage"
                                           class="pull-left btn btn-xs btn-primary"
                                           style="width: 180px">

                                    <input type="submit" name="update" class="btn btn-xs btn-success" value="Submit">
                                </div>
                            </div>

                        </g:form>
                        <div class=" col-md-9 col-lg-9 ">
                            <table class="table table-user-information">
                                <tbody>
                                <tr>
                                    <td>Service provider email</td>
                                    <td>${serviceProvider?.officialEmail}</td>
                                </tr>
                                <tr>
                                    <td>Service Provider website</td>
                                    <td>${serviceProvider?.website}</td>
                                </tr>
                                <tr>
                                    <td>City</td>
                                    <td>${serviceProvider?.city}</td>
                                </tr>

                                <tr>
                                <tr>
                                    <td>Contact Number</td>
                                    <td>${serviceProvider?.mobNo}</td>
                                </tr>
                                <tr>
                                    <td>Service type</td>
                                    <td>${serviceProvider?.propertyType}</td>
                                </tr>
                                </tbody>
                            </table>

                            <a href="#" class="btn btn-primary">Edit profile</a>
                            <a href="${createLink(controller: 'serviceProvider', action: 'addHotel', params: [serviceProviderId: serviceProvider?.id])}"
                               class="btn btn-primary">Add Hotel</a>
                        </div>
                    </div>
                </div>

                <div class="panel-footer">
                </div>

            </div>
        </div>
    </div>
</div>


<div class="container" style="margin-bottom: 10%">
    <div class="row">
        <div class="well">
            <h1 class="text-center">My Hotel list</h1>

            <div class="list-group">

                <g:each status="i" in="${serviceProvider?.hotel}" var="hotel">
                    <g:if test="${(i % 2) == 0}">
                        <hr>
                    </g:if>

                    <i class="list-group-item">
                        <div class="media col-md-3">
                            <figure class="pull-left">
                                <img class="media-object img-rounded img-responsive" src="http://placehold.it/350x250"
                                     alt="placehold.it/350x250">
                            </figure>
                        </div>

                        <div class="col-md-6">
                            <h4 class="list-group-item-heading">${hotel?.name}</h4>

                            <p class="list-group-item-text">${hotel?.summary}
                            </p>
                        </div>

                        <div class="col-md-3">
                            <g:link controller="serviceProvider" action="editHotel"
                                    params="[hotelId: hotel?.id, serviceProviderId:
                                            serviceProvider?.id]" class="btn btn-xs btn-primary">Edit</g:link>
                        </div>
                    </i>
                </g:each>
            </div>
        </div>
    </div>
</div>

<script src="${resource(dir: '/assets/js/libs', file: 'jquery-1.9.1.min.js')}"></script>
<script src="${resource(dir: '/assets/js/libs', file: 'jquery-ui-1.9.2.custom.min.js')}"></script>
<script src="${resource(dir: '/assets/js/libs', file: 'bootstrap.min.js')}"></script>
<script src="${resource(dir: '/assets/js/plugins/magnific', file: 'jquery.magnific-popup.min.js')}"></script>
%{--<script src="${resource(dir: '/assets/js', file: 'App.js')}"></script>--}%
<script type="text/javascript">
    $(document).ready(function () {

    });

    function showPopUp() {
        alert("Are you sure to delete this summary")

    }
</script>

<script type="text/javascript">
</script>
</body>
</html>