<%@ page import="com.travelex.socials.Enums.FacilityFor; com.travelex.common.Facility; com.travelex.socials.Enums.HotelType; serviceProvider.ServiceProvider" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Profile | Service Provider</title>
    <style>body {
        background-color: snow
    }</style>

    %{--<link rel="stylesheet" href="${resource(dir: 'css', file: 'jquery.ui.css')}" type="text/css">--}%
    %{--<link rel="stylesheet" href="${resource(dir: 'css', file: 'prettify.css')}" type="text/css">--}%
    <script src="${resource(dir: '/js/multiDate', file: 'jquery-1.7.2.js')}"></script>
    <script src="${resource(dir: '/js/multiDate', file: 'jquery.ui.core.js')}"></script>
    <script src="${resource(dir: '/js/multiDate', file: 'jquery.ui.datepicker.js')}"></script>
    <script src="${resource(dir: '/js/multiDate', file: 'jquery-ui.multidatespicker.js')}"></script>
    %{--<link rel="stylesheet" href="${resource(dir: 'css', file: 'mdp.css')}" type="text/css">--}%
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'pepper-ginder-custom.css')}" type="text/css">
    <script src="${resource(dir: '/js/multiDate', file: 'prettify.js')}"></script>
    <script src="${resource(dir: '/js/multiDate', file: 'lang-css.js')}"></script>
</head>

<body>
<br>
<br>

<div id="wrapper">

<nav id="top-bar" class="collapse top-bar-collapse">

</nav> <!-- /#top-bar -->





<div id="content1">

<div id="content-header">
    <div class="col-md-2">
        <h1>Edit Hotel Details</h1>
    </div>

    <div class="col-md-4">
        <br>
        <g:link controller="home" action="index" class="btn btn-lg btn-warning">&nbsp; &nbsp;Back &nbsp;&nbsp;</g:link>
    </div>
</div> <!-- #content-header -->


<div id="content-container">
<g:form controller="serviceProvider" action="updateHotel" enctype="multipart/form-data"
        params="[serviceProviderId: serviceProviderId,
                 hotelId          : hotel?.id,
                 addressId        : hotel?.address?.id,
                 hotelRoomId      : hotel?.roomTypes?.first()?.id,
                 hotelPicId       : hotel?.hotelPictures?.first() ? hotel?.hotelPictures?.first()?.id : null,
                 roomPicId        : hotel?.roomTypes?.first()?.hotel?.roomTypes?.first()?.id,
                 roomPictureOneId : hotel?.roomTypes[0]?.id
        ]">
<div class="portlet">
    <div class="portlet-header">

        <h3>
            <i class="fa fa-tasks"></i>
            Edit Hotel details
        </h3>

    </div>

    <div class="portlet-content">
        <div class="row">

            <div class="col-md-12">

                <div class="row">

                    <div class="col-sm-4">

                        <div class="form-group">
                            <label for="text-input">Hotel Name</label>
                            <input type="text" name="hotelName" id="text-input" value="${hotel.name}"
                                   class="form-control">
                        </div>

                        <div class="form-group">
                            <label>Hotel Summary(not more than 100 words)</label>
                            <textarea name="hotelSummary"
                                      cols="10"
                                      rows="3"
                                      class="form-control"
                                      onKeyDown="limitText(this.form.hotelSummary, this.form.countdown, 200);"
                                      onKeyUp="limitText(this.form.hotelSummary, this.form.countdown, 200);">${hotel.summary}</textarea>
                            <h5>(Maximum characters: 200)<br>
                                You have <input style="width: 50px;display: inline" class="form-control" readonly
                                                type="text" name="countdown"
                                                size="3" value="200"> characters left.</h5>
                        </div>

                    </div>

                    <div class="col-sm-2">&nbsp;</div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="text-input1">Hotel Type</label>
                            <g:select name="hotelType" id="text-input1" class="form-control" from="${HotelType.list()}"
                                      optionKey="key"
                                      optionValue="value" value="${hotel.type}"/>
                        </div>
                    </div>
                </div>

            </div>
            <hr>

            <div class="col-md-12">
                <h4>Edit Hotel Pictures</h4>

                <div class="row">
                    <g:each in="${hotel?.hotelPictures}" var="hotelPicture" status="i">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="text-input">Hotel Picture${i}</label>

                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="fileupload-new thumbnail" style="width: 60px; height: 40px;">
                                        <img src="${hotelPicture?.hotelPictureUrl ? hotelPicture?.hotelPictureUrl : "http://www.placehold.it/50x50/EFEFEF/AAAAAA"}"
                                             alt="Placeholder">
                                    </div>

                                    <div class="fileupload-preview fileupload-exists thumbnail"
                                         style="width: 60px; height: 40px;">
                                    </div>
                                    <span class="btn btn-default btn-file">
                                        <span class="fileupload-new">Select image</span>
                                        <span class="fileupload-exists">Change</span>
                                        <input type="hidden" name="pictureId${i}" value="${hotelPicture?.id}">
                                        <input type="file" name="hotelPicture${i}" accept="png|jpg|jpeg|gif">
                                    </span>
                                    <a href="#" class="btn btn-default fileupload-exists"
                                       data-dismiss="fileupload">Remove</a>
                                </div>

                            </div>
                        </div>
                    </g:each>

                    <div class="col-sm-2">&nbsp;</div>
                </div>
            </div>
            <hr>

            <div class="col-md-12">
                <h4>Edit Hotel Facilities separated by comma</h4>

                <div class="row">
                    <div class="col-sm-4">

                        <div class="form-group">
                            <label>Hotel Facilities</label>
                            <textarea name="hotelFacilities"
                                      cols="10" rows="3"
                                      class="form-control">${facilityList ? facilityList?.first()?.name : ""}</textarea>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>

<div class="portlet">
    <div class="portlet-header">

        <h3>
            <i class="fa fa-tasks"></i>
            Edit Hotel Address
        </h3>

    </div>

    <div class="portlet-content">
        <div class="row">

            <div class="col-md-12">

                <div class="row">

                    <div class="col-sm-4">

                        <div class="form-group">
                            <label for="text-input">Address line one</label>
                            <input type="text" name="addressOne" id="text-input2"
                                   value="${hotel?.address ? hotel?.address?.address1 : null}" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="text-input">Address line two</label>
                            <input type="text" name="addressTwo" id="text-input4"
                                   value="${hotel?.address ? hotel?.address?.address2 : null}" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="text-input">City</label>
                            <input type="text" name="addressCity" id="text-input5"
                                   value="${hotel?.address ? hotel?.address?.city : null}" class="form-control">
                        </div>

                    </div>

                    <div class="col-sm-2">&nbsp;</div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="text-input">State</label>
                            <input type="text" name="addressState" id="text-input6"
                                   value="${hotel?.address ? hotel?.address?.state : null}" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="text-input">Zip</label>
                            <input type="text" name="addressZip" id="text-input7"
                                   value="${hotel?.address ? hotel?.address?.zip : null}" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="text-input">Country</label>
                            <input type="text" name="addressCountry" id="text-input8"
                                   value="${hotel?.address ? hotel?.address?.country : null}" class="form-control">
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<div class="portlet">
    <div class="portlet-header">

        <h3>
            <i class="fa fa-tasks"></i>
            Edit Hotel Room
        </h3>

    </div>

    <div class="portlet-content">
    <div class="row">
        <g:each in="${hotel?.roomTypes}" var="room">
            <div class="col-md-12">

                <div class="row">

                    <div class="col-sm-4">

                        <div class="form-group">
                            <label for="text-input">Room Name</label>
                            <input type="text" name="roomOneName"
                                   value="${room ? room?.name : null}"
                                   class="form-control">
                        </div>

                        <div class="form-group">
                            <label>Room Description</label>
                            <textarea name="roomOneDescription"
                                      cols="10" rows="3"
                                      class="form-control"
                                      onKeyDown="limitText(this.form.roomOneDescription, this.form.countdown1, 200);"
                                      onKeyUp="limitText(this.form.roomOneDescription, this.form.countdown1, 200);">${room ? room?.description : null}</textarea>
                            <h5>(Maximum characters: 200)<br>
                                You have <input style="width: 50px;display: inline" class="form-control" readonly
                                                type="text" name="countdown1" size="3" value="200"> characters left.
                            </h5>
                        </div>

                    </div>

                    <div class="col-sm-2">&nbsp;</div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="text-input">Price</label>
                            <input type="text" name="roomOneMinimumPrice"
                                   value="${room ? room?.price : null}"
                                   class="form-control">
                        </div>
                    </div>
                </div>

            </div>
            <hr>

            <div class="col-md-12">
                <h4>Add room Pictures</h4>

                <div class="row">
                    <g:each in="${room.roomPicture}" var="roomPicture" status="i">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="text-input">Room Picture ${i}</label>

                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="fileupload-new thumbnail" style="width: 60px; height: 40px;">
                                        <img src="${roomPicture?.roomPictureUrl ? roomPicture?.roomPictureUrl : "http://www.placehold.it/50x50/EFEFEF/AAAAAA"}"
                                             alt="Placeholder">

                                        %{--<img src="${roomPicture?.roomPictureName ? createLink(controller: "serviceProvider", action: "showRoomImage", params: [roomId: room?.id, roomPictureName: roomPicture?.roomPictureName]) : "http://www.placehold.it/50x50/EFEFEF/AAAAAA"}"--}%
                                        %{--alt="Placeholder">--}%
                                    </div>

                                    <div class="fileupload-preview fileupload-exists thumbnail"
                                         style="width: 60px; height: 40px;">
                                    </div>
                                    <span class="btn btn-default btn-file">
                                        <span class="fileupload-new">Select image</span>
                                        <span class="fileupload-exists">Change</span>
                                        <input type="hidden" name="pictureId${i}" value="${roomPicture?.id}">
                                        <input type="file" name="roomPicture${i}" accept="png|jpg|jpeg|gif">
                                    </span>
                                    <a href="#" class="btn btn-default fileupload-exists"
                                       data-dismiss="fileupload">Remove</a>
                                </div>
                            </div>
                        </div>
                    </g:each>

                </div>

                <div class="col-sm-2">&nbsp;</div>

                %{--<div class="col-sm-4">--}%
                %{--<div class="form-group">--}%
                %{--<label for="text-input">Room Picture content type</label>--}%
                %{--<input type="text" name="roomOnePictureContentType"--}%
                %{--value="${room ? room?.pictures?.first()?.contentType : null}"--}%
                %{--class="form-control">--}%
                %{--</div>--}%

                %{--</div>--}%

            </div>
            </div>
            <hr>

            <div class="col-md-12">
                <h4>Add Room Facilities separated by comma</h4>

                <div class="row">
                    <div class="col-sm-4">

                        <div class="form-group">
                            <label>Room Facilities</label>
                            <g:set var="roomFacility"
                                   value="${Facility.createCriteria().list {
                                       eq("hotelRoomId", room.id) eq("facilityFor", FacilityFor.HOTEL_ROOM)
                                   }}"/>
                            <textarea name="roomOneFacilities"
                                      cols="10" rows="3"
                                      class="form-control">${roomFacility ? roomFacility?.first()?.name : null}</textarea>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-12">
                <h4>Add Room Booking details</h4>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="col-sm-6">
                            <input class="dpstart form-control bookingStart" type="text" placeholder="Start date"
                                   id="bookingStart"
                                   name="bookingStartRoomOne" data-date-format="mm-dd-yyyy"
                                   data-date-autoclose="false">
                            <span class="help-block">mm-dd-yyyy</span>
                        </div>
                    </div>
                </div>
            </div>
        </g:each>
    </div>
</div>
<br>
<g:submitButton name="Update Hotel" value="Update Hotel" class="btn btn-lg btn-success"/>
<g:link controller="home" action="index" class="btn btn-lg btn-primary">Cancel</g:link>

</div>
</g:form>
</div>
</div><!-- /.row -->
</div> <!-- /#content-container -->
</div> <!-- #content -->
<style>
#content1 {
    background: #FFF;
    width: auto;
    min-height: 800px;
    z-index: 18;
    padding-bottom: 25px;
    margin-left: 20px;
    margin-right: 20px;
    position: relative;
    left: auto;
    top: auto;
}</style>
<script type="text/javascript">

    $(document).ready(function () {
        $(".bookingStart").multiDatesPicker({ minDate: 0});
    });

    function showPopUp() {
        alert("Are you sure to delete this summary")
    }

</script>

<script type="text/javascript">
    function limitText(limitField, limitCount, limitNum) {
        if (limitField.value.length > limitNum) {
            limitField.value = limitField.value.substring(0, limitNum);
        } else {
            limitCount.value = limitNum - limitField.value.length;
        }
    }
</script>


<style>
.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
    background: #F8F7F6 url('/images/multiDatePick/ui-bg_fine-grain_10_f8f7f6_60x60.png') 50% 50% repeat;
}

table.ui-datepicker-calendar {
    border-collapse: separate;
}

.ui-datepicker-calendar td {
    border: 1px solid transparent;
}

.ui-datepicker .ui-datepicker-calendar .ui-state-highlight a {
    background: #743620 none;
    color: white;
}

#ui-datepicker-div {
    display: none;
}
</style>
</body>
</html>