<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="headerFooterLayout"/>
</head>

<body>

<div class="inner-page-container flight-booking-page full-height theme-blue">
    <!-- <div class="middle-container"> -->
    <div class="container-fluid">
        <div class="main-search-container">
            <div class="main-icons-container-vertical">
                <g:render template="/appTemplate/menuTemplate"/>
            </div>

            <div class="main-search-container-content text-center">
                <h2>What Service Do you Provide?</h2>

                <div class="row border-bottom">
                    <div class="col-md-6">
                        <a href="${createLink(controller: 'hotelService', action: 'index')}">
                            <img src="../images/service-hotel.png" height="120px">
                            <h4>HOTEL SERVICES</h4>
                        </a>
                        <a href="${createLink(controller: 'hotelService', action: 'index')}"
                           class="btn-ico btn-default"><i class=""><img
                                src="../images/login-icon-white.png"></i><span>Login to Hotel Service</span></a>
                    </div>

                    <div class="col-md-6">
                        <a href="${createLink(controller: 'login', action: 'serviceProviderLogin')}">
                            <img src="../images/service-activity.png" height="120px">
                            <h4>ACTIVITY SERVICES</h4>
                        </a>
                        <a href="${createLink(controller: 'login', action: 'serviceProviderLogin')}"
                           class="btn-ico btn-default"><i class=""><img
                                src="../images/login-icon-white.png"></i><span>Login to Activity Service</span></a>
                    </div>
                </div>

                <div class="button-container text-center">
                </div>
            </div>
        </div>
    </div>
    <!-- </div> -->
</div>
</body>
</html>