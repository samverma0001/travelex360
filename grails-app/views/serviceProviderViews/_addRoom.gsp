<div class="portlet-content">
    <div class="row">

        <div class="col-md-12">

            <div class="row">

                <div class="col-sm-4">

                    <div class="form-group">
                        <label for="text-input">Room ${roomNumber} Name</label>
                        <input type="text" name="room${roomNumber}Name" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Room Description</label>
                        <textarea name="room${roomNumber}Description" cols="10" rows="3"
                                  class="form-control"
                                  onKeyDown="limitText(this.form.room${roomNumber}Description, this.form.countdown${roomNumber}, 100);"
                                  onKeyUp="limitText(this.form.room${roomNumber}Description, this.form.countdown${roomNumber}, 100);"></textarea>
                        <h5>(Maximum characters: 100)<br>
                            You have <input style="width: 50px;display: inline" class="form-control" readonly
                                            type="text" name="countdown${roomNumber}" size="3"
                                            value="100"> characters left.
                        </h5>
                    </div>

                </div>

                <div class="col-sm-2">&nbsp;</div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="text-input">Price</label>
                        <input type="text" name="room${roomNumber}Price" class="form-control">
                    </div>

                </div>
            </div>

        </div>
        <hr>

        <div class="col-md-12">
            <h4>Add room Pictures</h4>

            <div class="row">
                <div class="col-sm-4">

                    <div class="form-group">
                        <label for="text-input">Room Picture</label>

                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 50px; height: 50px;">
                                <img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" alt="Placeholder">
                            </div>

                            <div class="fileupload-preview fileupload-exists thumbnail"
                                 style="width: 60px; height: 40px;">
                            </div>
                            <span class="btn btn-default btn-file">
                                <span class="fileupload-new">Select image</span>
                                <span class="fileupload-exists">Change</span>
                                <input type="file" name="room${roomNumber}Picture1" accept="png|jpg|jpeg|gif">
                            </span>
                            <a href="#" class="btn btn-default fileupload-exists"
                               data-dismiss="fileupload">Remove</a>
                        </div>


                        %{--<input type="text" name="roomOnePictureUrl" class="form-control">--}%
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="text-input">&nbsp;</label>

                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 50px; height: 50px;">
                                <img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" alt="Placeholder">
                            </div>

                            <div class="fileupload-preview fileupload-exists thumbnail"
                                 style="width: 60px; height: 40px;">
                            </div>
                            <span class="btn btn-default btn-file">
                                <span class="fileupload-new">Select image</span>
                                <span class="fileupload-exists">Change</span>
                                <input type="file" name="room${roomNumber}Picture2" accept="png|jpg|jpeg|gif">
                            </span>
                            <a href="#" class="btn btn-default fileupload-exists"
                               data-dismiss="fileupload">Remove</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="text-input">&nbsp;</label>

                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 50px; height: 50px;">
                                <img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" alt="Placeholder">
                            </div>

                            <div class="fileupload-preview fileupload-exists thumbnail"
                                 style="width: 60px; height: 40px;">
                            </div>
                            <span class="btn btn-default btn-file">
                                <span class="fileupload-new">Select image</span>
                                <span class="fileupload-exists">Change</span>
                                <input type="file" name="room${roomNumber}Picture3" accept="png|jpg|jpeg|gif">
                            </span>
                            <a href="#" class="btn btn-default fileupload-exists"
                               data-dismiss="fileupload">Remove</a>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <hr>

        <div class="col-md-12">
            <h4>Add Room Facilities separated by comma</h4>

            <div class="row">
                <div class="col-sm-4">

                    <div class="form-group">
                        <label>Room Facilities</label>
                        <textarea name="room${roomNumber}Facilities" cols="10" rows="3"
                                  class="form-control"></textarea>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-md-12">
            <h4>Add Room Booking details</h4>

            <div class="row">
                <div class="col-sm-6">
                    <div class="col-sm-6">
                        <input class="dpstart form-control bookingStart" type="text" placeholder="Start date"
                               id="bookingStart"
                               name="bookingStartRoom${roomNumber}" data-date-format="mm-dd-yyyy"
                               data-date-autoclose="false">
                        <span class="help-block">mm-dd-yyyy</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="button" onclick="addRoom('${roomNumber.toInteger()+1}')" id="room${roomNumber}AddButton"
           class="btn btn-lg btn-tertiary pull-right addRoomButton"
           value="Add Room">
</div>
<br>
<script>
    $(document).ready(function () {
        $(".bookingStart").multiDatesPicker({ minDate: 0});
    });
</script>