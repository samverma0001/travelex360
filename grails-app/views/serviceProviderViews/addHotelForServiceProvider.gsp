<%@ page import="com.travelex.socials.Enums.HotelType; serviceProvider.ServiceProvider" contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="cl" uri="http://cloudinary.com/jsp/taglib" %>

%{--<%@ taglib uri="http://cloudinary.com/jsp/taglib" prefix="cl" %>--}%
%{--<cl:jsinclude/>--}%
%{--<cl:jsconfig/>--}%
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="main"/>
    <title>Profile | Service Provider</title>
    <style>body {
        background-color: #ffffff;
    }</style>
    <script src="${resource(dir: '/js/multiDate', file: 'jquery-1.7.2.js')}"></script>
    <script src="${resource(dir: '/js/multiDate', file: 'jquery.ui.core.js')}"></script>
    <script src="${resource(dir: '/js/multiDate', file: 'jquery.ui.datepicker.js')}"></script>
    <script src="${resource(dir: '/js/multiDate', file: 'jquery-ui.multidatespicker.js')}"></script>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'pepper-ginder-custom.css')}" type="text/css">
    <script src="${resource(dir: '/js/multiDate', file: 'prettify.js')}"></script>
    <script src="${resource(dir: '/js/multiDate', file: 'lang-css.js')}"></script>
    <script src="${resource(dir: '/js', file: 'jquery.ui.widget.js')}"></script>
    <script src="${resource(dir: '/js', file: 'jquery.iframe-transport.js')}"></script>
    <script src="${resource(dir: '/js', file: 'jquery.fileupload.js')}"></script>
    <script src="${resource(dir: '/js', file: 'cloudinary.js')}"></script>
    <script src="${resource(dir: '/css', file: 'bootstrap-formhelpers.min.css')}"></script>
    <script src="${resource(dir: "js", file: "jquery.MultiFile.js")}"></script>

    <script type='text/javascript'>

        $.cloudinary.config({
            "cloud_name": "dv83vfnvq",
            "api_key": "478778589577545",
            "private_cdn": false,
            "cdn_subdomain": false
        });



    </script>
    <script>
        var data = { "timestamp":  <%= timestamp %>,
            "callback": "http://YOUR_DOMAIN/cloudinary_cors.html",
            "signature": "<%= expected_signature %>",
            "api_key": "YOUR API KEY" };
        $('#uploadinput').attr('data-form-data', JSON.stringify(data));
    </script>
</head>

<body>
<br>
<br>
<br>
<br>
<br>

<g:hasErrors bean="${addHotelCO}">
    <ul class="errors alert alert-error" role="alert">
        <g:eachError bean="${addHotelCO}" var="error">
            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                    error="${error}"/></li>
        </g:eachError>
    </ul>
</g:hasErrors>
<div class="row">
<g:form controller="serviceProvider" action="addNewHotel" enctype="multipart/form-data">

<div class="col-md-12">
<div class="col-md-4">

    <h4>Add Hotel Details</h4>
    <br>

    <div class="row">
        <div class="col-md-4">
            <strong>Hotel Name<travelx:star/></strong>
        </div>

        <div class="col-md-8">
            <input type="text" name="hotelName" id="text-input" class="form-control"/>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-4">
            <strong>Summary <travelx:star/></strong>
        </div>

        <div class="col-md-8">
            <textarea name="hotelSummary" cols="10" rows="3"
                      class="form-control autosize-animate"
                      onKeyDown="limitText(this.form.hotelSummary, this.form.countdown, 200);"
                      onKeyUp="limitText(this.form.hotelSummary, this.form.countdown, 200);"></textarea>
            <h5>(Maximum characters: 200)<br>
                %{--You have <input style="width: 50px;display: inline" class="form-control" readonly--}%
                %{--type="text" name="countdown" size="3" value="200"> characters left.</h5>--}%
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-4">
            <strong for="text-input1">Hotel Type<travelx:star/></strong>
        </div>

        <div class="col-md-8">
            <g:select name="hotelType" id="text-input1" class="form-control" from="${HotelType.list()}"
                      optionKey="key" optionValue="value" value="${HotelType.BED_AND_BREAKFAST.key}"/>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-4">
            <strong>Hotel Facilities<travelx:star/></strong>
        </div>

        <div class="col-md-8">
            <textarea name="hotelFacilities" cols="10" rows="3"
                      class="form-control autosize-animate"></textarea>
        </div>

    </div>
    <br>

    <div class="row">
        <div class="col-md-4">
            <strong>Hotel Images<travelx:star/></strong>
        </div>

        <div class="col-md-8">
            <input type="file" name="hotelImages" class="form-control multi"/>
        </div>
    </div>
</div>

<div class="col-md-4">
    <h4>Add Room Details</h4>
    <br>

    <div class="row">
        <div class="col-md-4">
            <strong>Room Type<travelx:star/></strong>

        </div>

        <div class="col-md-8">
            <input type="text" name="roomOneName" class="form-control">
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-4">
            <strong>Room Description<travelx:star/></strong>
        </div>

        <div class="col-md-8">
            <textarea name="roomOneDescription" cols="10" rows="3"
                      class="form-control autosize-animate"
                      onKeyDown="limitText(this.form.roomOneDescription, this.form.countdown1, 200);"
                      onKeyUp="limitText(this.form.roomOneDescription, this.form.countdown1, 200);"></textarea>
            <h5>(Maximum characters: 200)<br>
                %{--You have <input style="width: 50px;display: inline" class="form-control" readonly--}%
                %{--type="text" name="countdown1" size="3" value="200"> characters left.--}%
            </h5>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-4">
            <strong>Price<travelx:star/></strong>
        </div>

        <div class="col-md-8">
            <input type="text" name="roomOnePrice" class="form-control"/>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-4">
            <strong>Room Facilities<travelx:star/></strong>

        </div>

        <div class="col-md-8">
            <textarea name="roomOneFacilities" cols="10" rows="3"
                      class="form-control autosize-animate"></textarea>
        </div>

    </div>
    <br>

    <div class="row">
        <div class="col-md-4">
            <strong>Add Room Booking details<travelx:star/></strong>
        </div>

        <div class="col-md-8">
            <input class="dpstart form-control bookingStart" type="text" placeholder="Start date"
                   id="bookingStart"
                   name="bookingStartRoomOne" data-date-format="mm-dd-yyyy"
                   data-date-autoclose="false">
            <span class="help-block">mm-dd-yyyy</span>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-4">
            <strong>Room Images<travelx:star/></strong>
        </div>

        <div class="col-md-8">
            <input type="file" name="roomImages" class="form-control multi"/>
        </div>
    </div>



    %{--<div class="col-md-12">--}%
    %{--<h4>Add room Pictures</h4>--}%

    %{--<div class="row">--}%
    %{--<div class="col-sm-4">--}%

    %{--<div class="form-group">--}%
    %{--<label for="text-input">Room Picture</label>--}%

    %{--<div class="fileupload fileupload-new" data-provides="fileupload">--}%
    %{--<div class="fileupload-new thumbnail" style="width: 50px; height: 50px;">--}%
    %{--<img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" alt="Placeholder">--}%
    %{--</div>--}%

    %{--<div class="fileupload-preview fileupload-exists thumbnail"--}%
    %{--style="width: 60px; height: 40px;">--}%
    %{--</div>--}%
    %{--<span class="btn btn-default btn-file">--}%
    %{--<span class="fileupload-new">Select image</span>--}%
    %{--<span class="fileupload-exists">Change</span>--}%
    %{--<input type="file" name="roomPicture1" accept="png|jpg|jpeg|gif">--}%
    %{--</span>--}%
    %{--<a href="#" class="btn btn-default fileupload-exists"--}%
    %{--data-dismiss="fileupload">Remove</a>--}%
    %{--</div>--}%


    %{--<input type="text" name="roomOnePictureUrl" class="form-control">--}%
    %{--</div>--}%
    %{--</div>--}%

    %{--<div class="col-sm-4">--}%
    %{--<div class="form-group">--}%
    %{--<label for="text-input">&nbsp;</label>--}%

    %{--<div class="fileupload fileupload-new" data-provides="fileupload">--}%
    %{--<div class="fileupload-new thumbnail" style="width: 50px; height: 50px;">--}%
    %{--<img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" alt="Placeholder">--}%
    %{--</div>--}%

    %{--<div class="fileupload-preview fileupload-exists thumbnail"--}%
    %{--style="width: 60px; height: 40px;">--}%
    %{--</div>--}%
    %{--<span class="btn btn-default btn-file">--}%
    %{--<span class="fileupload-new">Select image</span>--}%
    %{--<span class="fileupload-exists">Change</span>--}%
    %{--<input type="file" name="roomPicture2" accept="png|jpg|jpeg|gif">--}%
    %{--</span>--}%
    %{--<a href="#" class="btn btn-default fileupload-exists"--}%
    %{--data-dismiss="fileupload">Remove</a>--}%
    %{--</div>--}%
    %{--</div>--}%
    %{--</div>--}%

    %{--<div class="col-sm-4">--}%
    %{--<div class="form-group">--}%
    %{--<label for="text-input">&nbsp;</label>--}%

    %{--<div class="fileupload fileupload-new" data-provides="fileupload">--}%
    %{--<div class="fileupload-new thumbnail" style="width: 50px; height: 50px;">--}%
    %{--<img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" alt="Placeholder">--}%
    %{--</div>--}%

    %{--<div class="fileupload-preview fileupload-exists thumbnail"--}%
    %{--style="width: 60px; height: 40px;">--}%
    %{--</div>--}%
    %{--<span class="btn btn-default btn-file">--}%
    %{--<span class="fileupload-new">Select image</span>--}%
    %{--<span class="fileupload-exists">Change</span>--}%
    %{--<input type="file" name="roomPicture3" accept="png|jpg|jpeg|gif">--}%
    %{--</span>--}%
    %{--<a href="#" class="btn btn-default fileupload-exists"--}%
    %{--data-dismiss="fileupload">Remove</a>--}%
    %{--</div>--}%
    %{--</div>--}%

    %{--</div>--}%

    %{--</div>--}%
    %{--</div>--}%


    %{--<g:link onclick="addRoomTwo()" class="btn btn-lg btn-primary">Add Room</g:link>--}%

</div>

<div class="col-md-4">
    <h4>Add Address</h4>
    <br>

    <div class="row">
        <div class="col-md-4">
            <strong>Line one<travelx:star/></strong>
        </div>

        <div class="col-md-8">
            <input type="text" name="addressOne" id="text-input2" class="form-control">
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-4">
            <strong>Line two</strong>
        </div>

        <div class="col-md-8">
            <input type="text" name="addressTwo" id="text-input4" class="form-control"/>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-4">
            <strong>City/Distt.<travelx:star/></strong>
        </div>

        <div class="col-md-8">
            <input type="text" name="addressCity" id="text-input5" class="form-control"/>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-4">
            <strong>State<travelx:star/></strong>
        </div>

        <div class="col-md-8">
            <input type="text" name="addressState" id="text-input6" class="form-control"/>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-4">
            <strong>Pincode<travelx:star/></strong>
        </div>

        <div class="col-md-8">
            <input type="text" name="addressZip" id="text-input7" class="form-control"/>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-4">
            <strong>Country<travelx:star/></strong>
        </div>

        <div class="col-md-8">
            <input type="text" name="addressCountry" id="text-input8" class="form-control">
        </div>
    </div>
</div>
</div>

<div class="row">
<div id="addRoomTwo"></div>
<br>
<input type="hidden" name="serviceProviderId" value="${serviceProviderId}">

<g:submitButton name="Add Hotel" value="Add Hotel" class="btn btn-lg btn-success"/>
<g:link controller="home" action="index" class="btn btn-lg btn-primary">Cancel</g:link>

</g:form>
<div class="row">
    <g:link controller="home" action="index"
            class="btn btn-lg btn-warning">&nbsp; &nbsp;Back &nbsp;&nbsp;</g:link>
</div>
</div>
<input type="button" onclick="addRoom('2')" id="room1AddButton"
       class="btn btn-lg btn-tertiary pull-right addRoomButton"
       value="Add Room">
</div>


%{--hotel images--}%

%{--<div class="row">--}%
%{--<div class="col-sm-4">--}%
%{--<div class="form-group">--}%
%{--<label for="text-input">Hotel Picture</label>--}%

%{--<div class="fileupload fileupload-new" data-provides="fileupload">--}%
%{--<div class="fileupload-new thumbnail" style="width: 50px; height: 50px;">--}%
%{--<img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" alt="Placeholder">--}%
%{--</div>--}%

%{--<div class="fileupload-preview fileupload-exists thumbnail"--}%
%{--style="width: 60px; height: 40px;">--}%
%{--</div>--}%
%{--<span class="btn btn-default btn-file">--}%
%{--<span class="fileupload-new">Select image</span>--}%
%{--<span class="fileupload-exists">Change</span>--}%
%{--<input type="file" name="hotelPicture" accept="png|jpg|jpeg|gif">--}%
%{--</span>--}%
%{--<a href="#" class="btn btn-default fileupload-exists"--}%
%{--data-dismiss="fileupload">Remove</a>--}%
%{--</div>--}%
%{--</div>--}%
%{--</div>--}%



%{--<input type="text" name="hotelPictureUrl" class="form-control">--}%

%{--<div class="col-sm-4">--}%
%{--<div class="form-group">--}%
%{--<label for="text-input">&nbsp;</label>--}%

%{--<div class="fileupload fileupload-new" data-provides="fileupload">--}%
%{--<div class="fileupload-new thumbnail" style="width: 50px; height: 50px;">--}%
%{--<img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" alt="Placeholder">--}%
%{--</div>--}%

%{--<div class="fileupload-preview fileupload-exists thumbnail"--}%
%{--style="width: 60px; height: 40px;">--}%
%{--</div>--}%
%{--<span class="btn btn-default btn-file">--}%
%{--<span class="fileupload-new">Select image</span>--}%
%{--<span class="fileupload-exists">Change</span>--}%
%{--<input type="file" name="hotelPicture2" accept="png|jpg|jpeg|gif">--}%
%{--</span>--}%
%{--<a href="#" class="btn btn-default fileupload-exists"--}%
%{--data-dismiss="fileupload">Remove</a>--}%
%{--</div>--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="col-sm-4">--}%
%{--<div class="form-group">--}%
%{--<label for="text-input">&nbsp;</label>--}%

%{--<div class="fileupload fileupload-new" data-provides="fileupload">--}%
%{--<div class="fileupload-new thumbnail" style="width: 50px; height: 50px;">--}%
%{--<img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" alt="Placeholder">--}%
%{--</div>--}%

%{--<div class="fileupload-preview fileupload-exists thumbnail"--}%
%{--style="width: 60px; height: 40px;">--}%
%{--</div>--}%
%{--<span class="btn btn-default btn-file">--}%
%{--<span class="fileupload-new">Select image</span>--}%
%{--<span class="fileupload-exists">Change</span>--}%
%{--<input type="file" name="hotelPicture3" accept="png|jpg|jpeg|gif">--}%
%{--</span>--}%
%{--<a href="#" class="btn btn-default fileupload-exists"--}%
%{--data-dismiss="fileupload">Remove</a>--}%
%{--</div>--}%
%{--</div>--}%
%{--</div>--}%

%{--</div>--}%

%{--</div>--}%



%{----}%
<script src="${resource(dir: '/js', file: 'bootstrap-formhelpers.js')}"></script>

<style>
#content1 {
    background: #FFF;
    width: auto;
    min-height: 800px;
    z-index: 18;
    padding-bottom: 25px;
    margin-left: 20px;
    margin-right: 20px;
    position: relative;
    left: auto;
    top: auto;
}</style>
<script type="text/javascript">

    $(document).ready(function () {
        $(".bookingStart").multiDatesPicker({ minDate: 0});
    });

    function showPopUp() {
        alert("Are you sure to delete this summary");

    }
    function addRoom(roomNumber) {

        var roomNum = roomNumber;
        if (roomNum == "6") {
            alert("Not allowed to add more than 5 rooms per hotel")
        }
        else {
            $(".addRoomButton").hide();
            $.ajax({
                url: "${createLink(controller: "serviceProvider" , action: "addRoom")}",
                data: {roomNumber: roomNum},
                type: "POST",
                success: function (success) {
//                alert(success)
                    $('#addRoomTwo').append(success);
                }
            });
            var showButton = "#room" + roomNumber + "AddButton";
            $(showButton).hide();
        }

    }


    $('.cloudinary-fileupload').bind('cloudinarydone', function (e, data) {
        $('.preview').html(
                $.cloudinary.image(data.result.public_id,
                        { format: data.result.format, version: data.result.version,
                            crop: 'fill', width: 150, height: 100 })
        );
        $('.image_public_id').val(data.result.public_id);
        return true;
    });
</script>

<script type="text/javascript">
    function limitText(limitField, limitCount, limitNum) {
        if (limitField.value.length > limitNum) {
            limitField.value = limitField.value.substring(0, limitNum);
        } else {
            limitCount.value = limitNum - limitField.value.length;
        }
    }
</script>

<style>

.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
    background: #F8F7F6 url('/images/multiDatePick/ui-bg_fine-grain_10_f8f7f6_60x60.png') 50% 50% repeat;
}

/* begin: jQuery UI Datepicker moving pixels fix */
table.ui-datepicker-calendar {
    border-collapse: separate;
}

.ui-datepicker-calendar td {
    border: 1px solid transparent;
}

/* end: jQuery UI Datepicker moving pixels fix */

/* begin: jQuery UI Datepicker emphasis on selected dates */
.ui-datepicker .ui-datepicker-calendar .ui-state-highlight a {
    background: #743620 none;
    color: white;
}

/* end: jQuery UI Datepicker emphasis on selected dates */

/* begin: jQuery UI Datepicker hide datepicker helper */
#ui-datepicker-div {
    display: none;
}
</style>
</body>
</html>