<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="headerFooterLayout"/>
</head>

<body>
<div class="content-page theme-orange full-height">
    <div class="inner-page-container full-height">
        <div class="container-fluid">
            <div class="service-login-container">
                <div class="larga-icon-container">
                    <div class="main-icon-box">
                        <img src="../images/service-hotel.png" height="120px">
                        <h4>HOTEL SERVICES LOGIN</h4>
                    </div>
                </div>

                <div class="login-box text-center">
                    <h3>Login to Access Hotel Services</h3>

                    <form class="tvx-from">
                        <div class="form-group text-left">
                            <label for="username"
                                   class="col-sm-4 control-label control-label-side">User Name / Email</label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="username" placeholder="">
                            </div>
                        </div>

                        <div class="form-group text-left">
                            <label for="password" class="col-sm-4 control-label control-label-side">Password</label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="password" placeholder="">
                            </div>
                        </div>

                        <div class="form-group text-left">
                            <div class="col-sm-offset-4 col-sm-8 text-left">
                                <button type="submit" class="btn btn-default">Sign in</button>
                            </div>
                        </div>

                        <div class="form-group">
                            <p>Forgot Password? <a href="#">Click Here</a></p>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <!-- </div> -->
        <!-- </div> -->
    </div>
</div>

</body>
</html>