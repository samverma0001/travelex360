<%@ page import="java.math.RoundingMode" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" href="../../favicon.ico">

    <title>.:: Travelex360 ::.</title>
    <link rel="stylesheet" href="../css/jquery-ui.css">
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../css/datepicker3.css" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="../css/jquery.selectBoxIt.css"/>

    <!-- Owl Carousel Assets -->
    %{--
        <link href="plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
        <link href="plugins/owl-carousel/owl.theme.css" rel="stylesheet">

        <link href="plugins/touchspin/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" media="all">
    --}%


    <link href="../css/bootstrap-slider.css" rel="stylesheet">

    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/theme.css" rel="stylesheet">
    <link href="../css/media.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28/angular.js"></script>

    <!-- // <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> -->

    <!-- // <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->
    <script src="../js/script.min.js"></script>

    <script src="../js/ang_main.js"></script>

    <script src="../js/ie10-viewport-bug-workaround.js"></script>
    <script src="../js/jquery-ui.min.js"></script>
    <script src="../js/datepicker.js"></script>
    <script src="../js/jquery.selectBoxIt.min.js"></script>

    %{--<script src="/plugins/owl-carousel/owl.carousel.js"></script>
    <script src="/plugins/touchspin/jquery.bootstrap-touchspin.js"></script>--}%

    %{--<script type="text/javascript" src="../js/packages-map.js"></script>--}%
    <script src="../js/bootstrap-slider.js"></script>
    <script src="../js/jquery.barrating.js"></script>
    <script src="../js/jquery.raty.js"></script>
    <script src="../js/jquery.validate.js"></script>
    <script src="../js/main.js"></script>


    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../js/ie8-responsive-file-warning.js"></script><![endif]-->

    <script src="../js/ie-emulation-modes-warning.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body class="theme-orange" ng-app>
<div class="page-wrapper">

    <header class="main-header">
        <div class="container-fluid">
            <g:render template="/navbar/standardNav"/>
        </div>
    </header>


    <div class="listing-page-wrapper">
        <g:render template="/hotelTemplates/hotelModifySearch"/>
        <div class="content-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="filter-sidebar-container">
                            <div class="filter-sidebar">

                                <h3>Filter Your Search
                                    <a href="#" class="btn btn-default btn-small pull-right"><i
                                            class="glyphicon glyphicon-refresh"></i></a>
                                </h3>

                                <div class="filter-box">
                                    <div class="filter-box-header">
                                        <h4>Find by Hotel Name</h4>
                                    </div>

                                    <div class="filter-box-content">
                                        <input type="text" class="form-control" placeholder="Search Hotel...">
                                    </div>
                                </div>

                                <div class="filter-box">
                                    <div class="filter-box-header">
                                        <h4>Rating</h4>
                                    </div>

                                    <div class="filter-box-content">
                                        <div class="rating-buttons">
                                            <div class="btn-group" data-toggle="buttons">
                                                <!-- <label class="btn btn-white active">
                          <input type="checkbox"  autocomplete="off" checked> All
                          <br> <small class="text-muted">(700)</small>
                        </label> -->
                                                <label class="btn btn-white">
                                                    <input type="checkbox" autocomplete="off"> 1 <i
                                                        class="glyphicon glyphicon-star"></i> <br> <small
                                                        class="text-muted">(700)</small>
                                                </label>
                                                <label class="btn btn-white">
                                                    <input type="checkbox" autocomplete="off"> 2 <i
                                                        class="glyphicon glyphicon-star"></i> <br> <small
                                                        class="text-muted">(700)</small>
                                                </label>
                                                <label class="btn btn-white">
                                                    <input type="checkbox" autocomplete="off"> 3 <i
                                                        class="glyphicon glyphicon-star"></i> <br> <small
                                                        class="text-muted">(700)</small>
                                                </label>
                                                <label class="btn btn-white">
                                                    <input type="checkbox" autocomplete="off"> 4 <i
                                                        class="glyphicon glyphicon-star"></i> <br> <small
                                                        class="text-muted">(700)</small>
                                                </label>
                                                <label class="btn btn-white">
                                                    <input type="checkbox" autocomplete="off"> 5 <i
                                                        class="glyphicon glyphicon-star"></i> <br> <small
                                                        class="text-muted">(700)</small>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="filter-box">
                                    <div class="filter-box-header">
                                        <h4>Budget Range <small>(per day)</small></h4>
                                    </div>

                                    <div class="filter-box-content">
                                        <div class="filter-list-box">
                                            <div class="filter-list">
                                                <label><input type="checkbox"> Less than Rs.5,000</label>
                                            </div>

                                            <div class="filter-list">
                                                <label><input type="checkbox"> Rs.5,001 - Rs.10,000</label>
                                            </div>

                                            <div class="filter-list">
                                                <label><input type="checkbox"> Rs.10,001 - Rs.15,000</label>
                                            </div>

                                            <div class="filter-list">
                                                <label><input type="checkbox"> Rs.15,001 - Rs.20,000</label>
                                            </div>

                                            <div class="filter-list">
                                                <label><input type="checkbox"> More than Rs.25,000</label>
                                            </div>
                                        </div>
                                        <h5>Custom Price Range</h5>
                                        <input id="ex2" type="text" class="" value="" data-slider-min="1"
                                               data-slider-max="25000" data-slider-step="5"
                                               data-slider-value="[0,4000]"/> <br>
                                        <h5>
                                            <span>INR 1</span>
                                            <span class="pull-right">INR 25,000</span>
                                        </h5>
                                    </div>
                                </div>

                                <div class="filter-box">
                                    <div class="filter-box-header">
                                        <h4>Property Type<small></small></h4>
                                    </div>


                                    <div class="filter-box-content">
                                        <div class="filter-list-box">
                                            <div class="filter-list">
                                                &nbsp;&nbsp;<input type="checkbox" name="hotelTypeFilter"
                                                                   id="allHotelType"
                                                                   onclick="unCheckOtherHotelTypeFilter();
                                                                   searchHotelType()" value="allHotelType"/>
                                                All

                                            </div>


                                            <div class="filter-list">
                                                &nbsp;&nbsp;<input type="checkbox" name="hotelTypeFilter"
                                                                   onclick=" unCheckAllHotelTypeFilter();
                                                                   searchHotelType()"
                                                                   id="innType" value="Inn"/>
                                                Inn
                                            </div>

                                            <div class="filter-list">

                                                &nbsp;&nbsp;<input type="checkbox" name="hotelTypeFilter"
                                                                   id="holidayType"
                                                                   onclick=" unCheckAllHotelTypeFilter();
                                                                   searchHotelType()" value="Holiday"/>
                                                Holiday Hotel
                                            </div>

                                            <div class="filter-list">

                                                &nbsp;&nbsp;<input type="checkbox" name="hotelTypeFilter"
                                                                   id="resortType"
                                                                   onclick=" unCheckAllHotelTypeFilter();
                                                                   searchHotelType()" value="Resort"/>
                                                Resort
                                            </div>

                                            <div class="filter-list">

                                                &nbsp;&nbsp;<input type="checkbox" name="hotelTypeFilter" id="hotelType"
                                                                   onclick=" unCheckAllHotelTypeFilter();
                                                                   searchHotelType()" value="Hotel"/>
                                                Hotel
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="filter-box">
                                    <div class="filter-box-header">
                                        <h4>Hotel Facilities</h4>
                                    </div>

                                    <div class="filter-box-content">
                                        <div class="filter-list-box">
                                            <div class="filter-list with-icon">
                                                <label><input type="checkbox"> Internet / Wi-Fi</label>
                                                <span><img src="../images/faci-wifi.png"></span>
                                            </div>

                                            <div class="filter-list with-icon">
                                                <label><input type="checkbox"> Swimming Pool</label>
                                                <span><img src="../images/faci-pool.png"></span>
                                            </div>

                                            <div class="filter-list with-icon">
                                                <label><input type="checkbox"> Integerated Gym</label>
                                                <span><img src="../images/faci-gym.png"></span>
                                            </div>

                                            <div class="filter-list with-icon">
                                                <label><input type="checkbox"> Air Conditioned</label>
                                                <span><img src="../images/faci-ac.png"></span>
                                            </div>

                                            <div class="filter-list with-icon">
                                                <label><input type="checkbox"> Restaurent Facility</label>
                                                <span><img src="../images/faci-rest.png"></span>
                                            </div>

                                            <div class="filter-list with-icon">
                                                <label><input type="checkbox"> Room Service</label>
                                                <span><img src="../images/faci-room.png"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    %{--------------------------}%

                    <div class="col-md-9">
                        <h1 class="main-title">${total} Hotels Available</h1>

                        <div class="result-category-container">
                            <a href="#" class="result-cat-box bg-brown active">
                                <div class="result-category-title">All Hotels</div>

                                <div class="result-category-count">${total}</div>
                            </a>
                            <a href="#" class="result-cat-box bg-green">
                                <div class="result-category-title">Budget Hotels</div>

                                <div class="result-category-count">500</div>
                            </a>
                            <a href="#" class="result-cat-box bg-voilet">
                                <div class="result-category-title">Business Hotels</div>

                                <div class="result-category-count">1000</div>
                            </a>
                            <a href="#" class="result-cat-box bg-pink">
                                <div class="result-category-title">Luxury Hotels</div>

                                <div class="result-category-count">12</div>
                            </a>
                            <a href="#" class="result-cat-box bg-dark">
                                <div class="result-category-title">Best Deals</div>

                                <div class="result-category-count">50</div>
                            </a>
                        </div>

                        <div class="listing-content-container">
                            <div class="listing-sort-container">
                                SORT BY : <a href="#" class="active">Most Popular</a> <a href="#">Lowest Price</a> <a
                                    href="#">Top Rated</a>
                            </div>

                            <g:if test="${hotelList}">
                                <g:each in="${hotelList}" status="i" var="hotelInstance">

                                    <div class="listing-content-outer">
                                        <div class="listing-box">
                                            <div class="row">
                                                <div class="col-md-3" id="updatePaginateList">
                                                    <div class="listing-image">
                                                        <a class="pull-left">
                                                            <g:if test="${hotelInstance}">
                                                                <img src="${hotelInstance?.searchHotelMedias ? hotelInstance?.searchHotelMedias?.first()?.mediaSrc : defaultUrl}"
                                                                     alt="image" width="150" height="100"
                                                                     class="img-responsive">
                                                            </g:if>
                                                            <g:else>
                                                                Image not available
                                                            </g:else>
                                                        </a>
                                                    </div>
                                                </div>

                                                <div class="col-md-9">
                                                    <div class="listing-content">
                                                        <div class="row">
                                                            <div class="col-md-9">
                                                                <h3 class="listing-title">
                                                                    <a href="#">
                                                                        <g:if test="${hotelInstance}">
                                                                            <g:if test="${hotelInstance.sessionId}">
                                                                                <g:link controller="hotelService"
                                                                                        action="hotelDetail"
                                                                                        style="text-decoration: none;"
                                                                                        params="[sessionId              : hotelInstance?.sessionId, hotelIndex: hotelInstance?.hotelIndex, hotelInstanceList: hotelList,
                                                                                                 noOfRoom               : searchParametersCO?.noOfRooms, adult: searchParametersCO?.adult,
                                                                                                 peopleInRoomTwo        : searchParametersCO?.peopleInRoomTwo, peopleInRoomThree: searchParametersCO?.peopleInRoomThree, peopleInRoomFour: searchParametersCO?.peopleInRoomFour, child: searchParametersCO?.children, checkIn: searchParametersCO?.checkIn,
                                                                                                 childInRoomTwo         : searchParametersCO?.childInRoomTwo, childInRoomThree: searchParametersCO?.childInRoomThree, childInRoomFour: searchParametersCO?.childInRoomFour,
                                                                                                 checkOut               : searchParametersCO?.checkOut, firstChildAgeRoomOne: searchParametersCO.firstChildAgeRoomOne,
                                                                                                 secondChildAgeRoomOne  : searchParametersCO?.secondChildAgeRoomOne, firstChildAgeRoomTwo: searchParametersCO.firstChildAgeRoomTwo,
                                                                                                 secondChildAgeRoomTwo  : searchParametersCO?.secondChildAgeRoomTwo, firstChildAgeRoomThree: searchParametersCO.firstChildAgeRoomThree,
                                                                                                 secondChildAgeRoomThree: searchParametersCO?.secondChildAgeRoomThree, firstChildAgeRoomFour: searchParametersCO.firstChildAgeRoomFour,
                                                                                                 secondChildAgeRoomFour : searchParametersCO?.secondChildAgeRoomFour, supplierId: hotelInstance?.supplierId]">${hotelInstance?.hotelName}
                                                                                </g:link>
                                                                            </g:if>
                                                                            <g:else>
                                                                                <h4><g:link controller="hotelService"
                                                                                            action="hotelDetail"
                                                                                            style="text-decoration: none;"
                                                                                            params="[id                     : hotelInstance?.hotelId, hotelInstanceList: hotelList,
                                                                                                     noOfRoom               : searchParametersCO?.noOfRooms, adult: searchParametersCO?.adult,
                                                                                                     peopleInRoomTwo        : searchParametersCO?.peopleInRoomTwo, peopleInRoomThree: searchParametersCO?.peopleInRoomThree, peopleInRoomFour: searchParametersCO?.peopleInRoomFour, child: searchParametersCO?.children, checkIn: searchParametersCO?.checkIn,
                                                                                                     childInRoomTwo         : searchParametersCO?.childInRoomTwo, childInRoomThree: searchParametersCO?.childInRoomThree, childInRoomFour: searchParametersCO?.childInRoomFour,
                                                                                                     checkOut               : searchParametersCO?.checkOut, firstChildAgeRoomOne: searchParametersCO.firstChildAgeRoomOne,
                                                                                                     secondChildAgeRoomOne  : searchParametersCO?.secondChildAgeRoomOne, firstChildAgeRoomTwo: searchParametersCO.firstChildAgeRoomTwo,
                                                                                                     secondChildAgeRoomTwo  : searchParametersCO?.secondChildAgeRoomTwo, firstChildAgeRoomThree: searchParametersCO.firstChildAgeRoomThree,
                                                                                                     secondChildAgeRoomThree: searchParametersCO?.secondChildAgeRoomThree, firstChildAgeRoomFour: searchParametersCO.firstChildAgeRoomFour,
                                                                                                     secondChildAgeRoomFour : searchParametersCO?.secondChildAgeRoomFour, supplierId: hotelInstance?.supplierId]">${hotelInstance?.hotelName}
                                                                                </g:link>
                                                                                </h4>
                                                                            </g:else>
                                                                        </g:if>
                                                                    </a>

                                                                    %{--_hotelRating template for the rating the hotel--}%
                                                                    <g:render template="../hotelTemplates/hotelRating"
                                                                              model="[hotelInstance: hotelInstance]"/>

                                                                </h3>

                                                                %{--More Details--}%
                                                                <ul class="list-inline mrg-0 btm-mrg-10 clr-535353">
                                                                    <li>
                                                                        <a class="moreDetails" href="#hee"
                                                                           onclick="hideDiv()"
                                                                           style="text-decoration: none">More Details..</a>
                                                                    </li>
                                                                </ul>

                                                                <div class="address" style="display:none;">

                                                                    <p class="hidden-xs">${hotelInstance?.shortDescription ? hotelInstance?.shortDescription?.tokenize("<b>")?.first() : ""}
                                                                    </p><span
                                                                        class="fnt-arial">${hotelInstance?.location}India</span>
                                                                </div>


                                                                <h4>Facilities</h4>

                                                                <div class="facilities-container">
                                                                    <ul class="facilities-list">
                                                                        <li><span><img src="../images/faci-wifi.png"
                                                                                       alt="Internet / Wifi"
                                                                                       title="Internet /Wifi"></span>
                                                                        </li>
                                                                        <li><span><img src="../images/faci-ac.png"
                                                                                       alt="Air Conditioned"
                                                                                       title="Air Conditioned"></span>
                                                                        </li>
                                                                        <li><span><img src="../images/faci-room.png"
                                                                                       alt="Room Service"
                                                                                       title="Room Service">
                                                                        </span></li>
                                                                        <li><span><img src="../images/faci-pool.png"
                                                                                       alt="Swimming Pool"
                                                                                       title="Swimming Pool"></span>
                                                                        </li>
                                                                        <li><span><img src="../images/faci-rest.png"
                                                                                       alt="Restaurent"
                                                                                       title="Restaurent">
                                                                        </span></li>
                                                                        <li><span><img src="../images/faci-gym.png"
                                                                                       alt="Gym"
                                                                                       title="Gym"></span></li>
                                                                    </ul>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="price-box">
                                                                    <h3 class="price">
                                                                        <small style="font-style:italic;font-size: medium"><b>Price starts -</b>
                                                                        </small>
                                                                        <g:set var="total"
                                                                               value="${Double.parseDouble(travelx.calculatePerNightRoomCost(noOfNights: "${noOfNights}", totalAmount: "${hotelInstance?.hotelRoomDetailVOArrayList?.hotelRateVO?.totalRate?.min()?.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP) + hotelInstance?.hotelRoomDetailVOArrayList?.hotelRateVO?.agentMarkUp?.first()?.toBigDecimal()?.setScale(2, java.math.RoundingMode.HALF_UP)}").toString())}"/>
                                                                        <span class="old">Rs.${2000 + total?.toBigDecimal()?.setScale(2, java.math.RoundingMode.HALF_UP)}</span>
                                                                        <span class="new color-default"><b>Rs.${travelx.calculatePerNightRoomCost(noOfNights: "${noOfNights}", totalAmount: "${hotelInstance?.hotelRoomDetailVOArrayList?.hotelRateVO?.totalRate?.min()?.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP) + hotelInstance?.hotelRoomDetailVOArrayList?.hotelRateVO?.agentMarkUp?.first()?.toBigDecimal()?.setScale(2, java.math.RoundingMode.HALF_UP)}")}</b>
                                                                        </span>

                                                                        <p><small>Per Room Per Night</small></p>


                                                                        <g:if test="${hotelInstance}">
                                                                            <g:if test="${hotelInstance.sessionId}">
                                                                                <g:link class="btn btn-default"
                                                                                        controller="hotelService"
                                                                                        action="hotelDetail"
                                                                                        style="text-decoration: none;"
                                                                                        params="[sessionId                                                      : hotelInstance?.sessionId, hotelIndex: hotelInstance?.hotelIndex,
                                                                                                 noOfRoom                                                       : searchParametersCO?.noOfRooms, adult: searchParametersCO?.adult,
                                                                                                 peopleInRoomTwo                                                : searchParametersCO?.peopleInRoomTwo, peopleInRoomThree:
                                                                                                         searchParametersCO?.peopleInRoomThree, peopleInRoomFour: searchParametersCO?.peopleInRoomFour,
                                                                                                 child                                                          : searchParametersCO?.children, checkIn: searchParametersCO?.checkIn,
                                                                                                 childInRoomTwo                                                 : searchParametersCO?.childInRoomTwo, childInRoomThree: searchParametersCO?.childInRoomThree, childInRoomFour: searchParametersCO?.childInRoomFour,
                                                                                                 checkOut                                                       : searchParametersCO?.checkOut, firstChildAgeRoomOne:
                                                                                                         searchParametersCO.firstChildAgeRoomOne,
                                                                                                 secondChildAgeRoomOne                                          : searchParametersCO?.secondChildAgeRoomOne, firstChildAgeRoomTwo:
                                                                                                         searchParametersCO.firstChildAgeRoomTwo,
                                                                                                 secondChildAgeRoomTwo                                          : searchParametersCO?.secondChildAgeRoomTwo, firstChildAgeRoomThree:
                                                                                                         searchParametersCO.firstChildAgeRoomThree,
                                                                                                 secondChildAgeRoomThree                                        : searchParametersCO?.secondChildAgeRoomThree,
                                                                                                 firstChildAgeRoomFour                                          : searchParametersCO.firstChildAgeRoomFour,
                                                                                                 secondChildAgeRoomFour                                         : searchParametersCO?.secondChildAgeRoomFour, supplierId:
                                                                                                         hotelInstance?.supplierId, cityId                      : cityId]">
                                                                                    Select
                                                                                </g:link>
                                                                            </g:if>
                                                                            <g:else>
                                                                                <g:link class="btn btn-default"
                                                                                        controller="hotelService"
                                                                                        action="hotelDetail"
                                                                                        params="[
                                                                                                id                     : hotelInstance?.hotelId, hotelInstanceList: hotelList,
                                                                                                noOfRoom               : searchParametersCO?.noOfRooms, adult: searchParametersCO?.adult,
                                                                                                peopleInRoomTwo        : searchParametersCO?.peopleInRoomTwo, peopleInRoomThree: searchParametersCO?.peopleInRoomThree, peopleInRoomFour: searchParametersCO?.peopleInRoomFour, child: searchParametersCO?.children, checkIn: searchParametersCO?.checkIn,
                                                                                                childInRoomTwo         : searchParametersCO?.childInRoomTwo, childInRoomThree: searchParametersCO?.childInRoomThree, childInRoomFour: searchParametersCO?.childInRoomFour,
                                                                                                checkOut               : searchParametersCO?.checkOut, firstChildAgeRoomOne: searchParametersCO.firstChildAgeRoomOne,
                                                                                                secondChildAgeRoomOne  : searchParametersCO?.secondChildAgeRoomOne, firstChildAgeRoomTwo: searchParametersCO.firstChildAgeRoomTwo,
                                                                                                secondChildAgeRoomTwo  : searchParametersCO?.secondChildAgeRoomTwo, firstChildAgeRoomThree: searchParametersCO.firstChildAgeRoomThree,
                                                                                                secondChildAgeRoomThree: searchParametersCO?.secondChildAgeRoomThree, firstChildAgeRoomFour: searchParametersCO.firstChildAgeRoomFour,
                                                                                                secondChildAgeRoomFour : searchParametersCO?.secondChildAgeRoomFour, supplierId: hotelInstance?.supplierId, cityId: cityId]">
                                                                                    Select
                                                                                </g:link>
                                                                            </g:else>
                                                                        </g:if>
                                                                    </h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </g:each>
                            </g:if>
                        </div>
                        <util:remoteNonStopPageScroll action='remotePaginateHotel' controller="hotelService"
                                                      params="[checkIn          : searchParametersCO?.checkIn, checkOut: searchParametersCO?.checkOut, priceRangeMax: maxPrice, minRangePrice:
                                                              minPrice, noOfRoom: searchParametersCO?.noOfRooms, adult: searchParametersCO?.adult, children: searchParametersCO?.children]"
                                                      total="${total}"
                                                      update="updatePaginateList"/>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="main-footer inner">
    <div class="container-fluid">
        <div class="footer-links">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <ul class="inline-list-links">
                        <li><a href="about.php">About Us</a></li>
                        <li><a href="how_it_works.php">How It Works</a></li>
                        <li><a href="terms.php">Terms Of Use</a></li>
                        <li><a href="contact.php">Contact Us</a></li>
                    </ul>
                </div>

                <div class="col-md-6 col-sm-6 text-right">
                    Copyright &copy; Travelex 360 Pvt.Ltd. 2014, All rights reserved
                </div>
            </div>
        </div>

        <div class="footer-rights">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <ul class="inline-list-links margin-top-5">
                        <li><a href="#">Post Your Complaint</a></li>
                        <li><a href="policy.php">Privacy Policy</a></li>
                        <li><a href="#">FAQ's</a></li>
                        <li><a href="#">Site Map</a></li>
                    </ul>
                </div>

                <div class="col-md-6 col-sm-6 text-right">
                    <img src="../images/payment-methods.jpg">
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript">

    function hideDiv() {
        var value = $(".address").css("display");
        if (value == "none") {
            $(".address").css("display", "inline");
            $(".moreDetails").text("Hide Details..");
        }
        else if (value == "inline") {
            $(".moreDetails").text("More Details..");
            $(".address").css("display", "none");
        }
    }
    $(document).ready(function () {
        $(".countspin-roomtype1").TouchSpin({
            min: 0,
            initval: '1',
            max: 14,
            buttondown_class: 'btn btn-default',
            buttonup_class: 'btn btn-default',
            step: 1
        });
        $(".countspin-roomtype2").TouchSpin({
            min: 0,
            initval: '1',
            max: 5,
            buttondown_class: 'btn btn-default',
            buttonup_class: 'btn btn-default',
            step: 1
        });
        $('#example-f').barrating({
            showSelectedRating: false,
            readonly: true
        });
        $.fn.raty.defaults.path = 'images';
        $('.star-rating.readonly').raty({
            readOnly: true,
            score: function () {
                return $(this).attr('data-score');
            }
        });
        $('.star-rating-overall').raty({
            score: function () {
                return $(this).attr('data-score');
            },
            target: '.rating-target1',
            targetKeep: true,
            targetText: 'Click to Rate'
        });
        $('.star-rating-service').raty({
            score: function () {
                return $(this).attr('data-score');
            },
            target: '.rating-target2',
            targetKeep: true,
            targetText: 'Click to Rate'
        });
        $('.star-rating-quality').raty({
            score: function () {
                return $(this).attr('data-score');
            },
            target: '.rating-target3',
            targetKeep: true,
            targetText: 'Click to Rate'
        });
        $('.star-rating-clean').raty({
            score: function () {
                return $(this).attr('data-score');
            },
            target: '.rating-target4',
            targetKeep: true,
            targetText: 'Click to Rate'
        });
    });
</script>
</body>
</html>