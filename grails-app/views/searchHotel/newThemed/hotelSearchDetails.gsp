<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" xmlns="http://www.w3.org/1999/html"><!--<![endif]-->
<head>
    <meta name="layout" content="main">
    <title>Hotel Detail-TravelX360</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="">
    <meta name="author" content=""/>
    <link rel="stylesheet" href=""${resource(dir: '/css', file: 'jquery-ui.css')}"
    <link rel="stylesheet" href="${resource(dir: '/assets/css', file: 'custom.css')}" type="text/css"/>
    <link rel="stylesheet" href="${resource(dir: '/assets/css', file: 'App.css')}" type="text/css">
    %{--<link rel="stylesheet" href="${resource(dir: '/css', file: '')}" type="text/css">--}%
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53a949532e39605e"></script>
</head>

<body>

<div id="wrapper">
<br>
<br>
<br>

<div id="content1">
<div id="content-header">
    <h2 style="margin-top: 3px;margin-left: 5px">${hotelVOInstance?.name}</h2>
    %{--<g:link style="margin-left: 5px" url="${searchUrl}">Go back to search</g:link>--}%
</div> <!-- #content-header -->
<div id="content-container">
<div class="row">
%{--<div class="col-md-9">--}%
<div class="col-md-9 col-sm-9">
<div class="portlet">
<div class="portlet-header">

    <h3>
        <i class="fa fa-bar-chart-o"></i>

    </h3>

</div>

<div class="portlet-content">
<div class="row">

<div class="col-md-12">
<div class="control list-group">

<div class="controls" style=" margin-left: 20px">
<div class="list-group-item">
    <div class="row">
        <div class="col-md-3">
            <h4>${hotelVOInstance?.name}</h4>
            <h6>Hotel Detailed address:-(${location},${locality}/${hotelVOInstance?.state})</h6>
            %{--<h6>Phone No.| Email Id</h6>--}%
            <h6>Phone No:-${hotelVOInstance?.contacts?.first()?.phone}&nbsp;
                <br>
                Mobile No:-${hotelVOInstance?.contacts?.first()?.mobile}
                <br>
                e-mail:-${hotelVOInstance?.contacts?.first()?.email}</h6>
        </div>

        <div class="col-md-6"></div>

        <div class="col-md-3">
            %{--<small>Type:-<strong>${hotelVOInstance?.categoryName}</strong></small>--}%

            <div class="row">
                <div class="col-md-9">
                    %{--<g:each in="${hotelInstance1}" var="h">--}%
                    %{--<h5>${h?.roomRate}</h5>--}%
                    %{--<h5><p class="text-info">${hotelInstance1?.hotelRoomRatesVOs[0] ? hotelInstance1?.hotelRoomRatesVOs[0]?.roomRateType[0]?.roomRate : null}&nbsp;INR</p>--}%
                </h5>
                    <h6><small><strong>Average Price</strong></small></h6>
                </div>
            </div>
        </div>
    </div>
</div>

%{--<div class="row">--}%
%{--<div class="col-md-9">--}%
%{--<small>......................................................................................................................................................................................................................................................................................</small>--}%
%{--</div>--}%
%{--</div>--}%
<div class="list-group-item">
<div class="row">
    <div class="col-md-3">
        <g:if test="${hotelVOInstance?.hotelPictures}">

            <img src="${hotelVOInstance?.getMediaInfo()?.src}" width="180"
                 alt="Profile Picture">

        </g:if>

        <g:else>
            <img src="http://www.durhamld.com/images/Taj_01.jpg" width="180"
                 class="img-rounded">
        </g:else>
        <div>
            <small>Ratings ******** Reviews</small>
        </div>


    </div>

    <div class="col-md-9">
        <h4><button
                class="btn btn-small btn-success" onclick="loadHotelPics(${hotelVOInstance?.hotelId})">Photos</button>&nbsp;
            <button
                    class="btn btn-small btn-success">Reviews</button>&nbsp;
            <button
                    class="btn btn-small btn-success">Map/Directions</button></h4>

        <p>${hotelVOInstance?.description}</p>

        <div class="col-md-10">
            <small>Share this hotel:</small>

            <a href="https://www.facebook.com/sharer/sharer.php?u" class="btn btn-info">Facebook</a>
            &nbsp;
            <br>
            <br>
            %{--<h5><small>Hotel Rating:</small>&nbsp;<g:each--}%
            %{--in="${1..(hotelInstance1?.hotelRating as Integer)}"--}%
            %{--var="a"><img--}%
            %{--src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>--}%
            %{--</h5>--}%
        </div>

        <div class="col-md-7">&nbsp;</div>

        %{--<div class="col-md-7 pull-right">--}%
        %{--<button class="btn btn-small btn-primary">Book Now</button>--}%
        %{--<button class="btn btn-small btn-primary"><small>Add To</small>Travel Summary--}%
        %{--</button>--}%

        %{--</div>--}%
    </div>
</div>
%{--<div id="hotelPics" class="list-group-item">--}%
<div class="row">

    <div class="col-md-2">&nbsp;</div>

    <div id="hotelPics" class="col-md-9">
        %{--<g:render template="/hotelTemplates/loadHotelPictures" model="[hotelVOInstance:hotelVOInstance]"/>--}%
    </div>
</div>
%{--</div>--}%

<div class="list-group-item">
<g:each in="${hotelVOInstance?.roomCategory}" status="i" var="hotelRoomInstance">
<div class="row">

<div class="col-md-2">
    <g:if test="${hotelRoomInstance?.roomPictures}">

    %{--<img src="${resource(dir: 'images/hotel', file: 'hotelImage.jpg')}" class="img-rounded">--}%
    %{--<img src="http://www.durhamld.com/images/Taj_01.jpg" class="img-rounded">--}%
        <img src="${hotelRoomInstance?.roomMedia?.roomMediaSrc}" width="125"
             alt="Profile Picture">
    </g:if>

    <g:else>
        <img src="http://www.1320am.com/wp-content/uploads/2013/04/no-photo-available-hi-300x300.png"
             width="125"
             alt="Profile Picture">
    </g:else>
    <h6><small style="color: #080808">Deals/Offer/Special offer</small>
    </h6>

</div>

<div class="col-md-10">
    <div class="col-md-5">
        %{--<h5>Room Types Available</h5>--}%
    %{--<g:if test="${hotelRoomInstance?.roomCategoryId}"--}%
        <g:if test="${i == 0}">
            <h6>Room Type:<small><strong>${hotelInstance1?.hotelRoomRatesVOs[0]?.roomTypeId == "1" ? "Single" : hotelInstance1?.hotelRoomRatesVOs[0]?.roomTypeId == "2" ? "Double" :
                hotelInstance1?.hotelRoomRatesVOs[0]?.roomTypeId == "3" ? "Triple" : hotelInstance1?.hotelRoomRatesVOs[0]?.roomTypeId == "4" ? "Quad" : ""}
            </strong></small></h6>
        </g:if>

        <g:elseif test="${i == 1}">
            <h6>Room Type:<small><strong>${hotelInstance1?.hotelRoomRatesVOs[1]?.roomTypeId == "1" ? "Single" : hotelInstance1?.hotelRoomRatesVOs[1]?.roomTypeId == "2" ? "Double" :
                hotelInstance1?.searchHotelRoomType[1]?.roomTypeId == "3" ? "Triple" : hotelInstance1?.hotelRoomRatesVOs[1]?.roomTypeId == "4" ? "Quad" : ""}
            </strong></small></h6>
        </g:elseif>
        <g:elseif test="${i == 2}">
            <h6>Room Type:<small><strong>${hotelInstance1?.hotelRoomRatesVOs[2]?.roomTypeId == "1" ? "Single" : hotelInstance1?.hotelRoomRatesVOs[2]?.roomTypeId == "2" ? "Double" :
                hotelInstance1?.hotelRoomRatesVOs[2]?.roomTypeId == "3" ? "Triple" : hotelInstance1?.hotelRoomRatesVOs[2]?.roomTypeId == "4" ? "Quad" : ""}
            </strong></small></h6>
        </g:elseif>
        <g:elseif test="${i == 3}">
            <h6>Room Type:<small><strong>${hotelInstance1?.hotelRoomRatesVOs[3]?.roomTypeId == "1" ? "Single" : hotelInstance1?.hotelRoomRatesVOs[3]?.roomTypeId == "2" ? "Double" :
                hotelInstance1?.hotelRoomRatesVOs[3]?.roomTypeId == "3" ? "Triple" : hotelInstance1?.hotelRoomRatesVOs[3]?.roomTypeId == "4" ? "Quad" : ""}
            </strong></small></h6>
        </g:elseif>
        <g:elseif test="${i == 4}">
            <h6>Room Type:<small><strong>${hotelInstance1?.searchHotelRoomType[4]?.roomTypeId == "1" ? "Single" : hotelInstance1?.searchHotelRoomType[4]?.roomTypeId == "2" ? "Double" :
                hotelInstance1?.searchHotelRoomType[4]?.roomTypeId == "3" ? "Triple" : hotelInstance1?.searchHotelRoomType[4]?.roomTypeId == "4" ? "Quad" : ""}
            </strong></small></h6>
        </g:elseif>

        <strong><p class="text-info">Hotel Room Facilities</p></strong>
        <g:if test="${roomFacilityList}">
            <g:each in="${roomFacilityList}" var="roomFacilities">
                <small>${roomFacilities?.name}</small>,
            </g:each>
        </g:if>
        <g:else>
            ${hotelRoomInstance.roomFacilities}
        </g:else>
    </div>

    <div class="col-md-5 pull-right">
        <h6><small
                style="color: #080808">Room Class:&nbsp;<strong>${hotelRoomInstance?.name}</strong></small>
        </h6>

        %{--<g:render template="/hotelTemplates/setValueForHotelDetail"--}%
        %{--model="[hotelInstance1: hotelInstance1]"/>--}%

        <g:set var="roomTypeOneAopPrice"
               value="${hotelInstance1?.hotelRoomRatesVOs[0] ? hotelInstance1?.hotelRoomRatesVOs[0]?.roomRateType[0]?.roomRate : null}"/>
        <g:set var="roomTypeTwoAopPrice"
               value="${hotelInstance1?.hotelRoomRatesVOs[1] ? hotelInstance1?.hotelRoomRatesVOs[1]?.roomRateType[0]?.roomRate : null}"/>
        <g:set var="roomTypeThreeAopPrice"
               value="${hotelInstance1?.hotelRoomRatesVOs[2] ? hotelInstance1?.hotelRoomRatesVOs[2]?.roomRateType[0]?.roomRate : null}"/>
        <g:set var="roomTypeFourAopPrice"
               value="${hotelInstance1?.hotelRoomRatesVOs[3] ? hotelInstance1?.hotelRoomRatesVOs[3]?.roomRateType[0]?.roomRate : null}"/>


        <g:set var="roomTypeOneExtraBed"
               value="${hotelInstance1?.hotelRoomRatesVOs[0] ? hotelInstance1?.hotelRoomRatesVOs[0]?.roomRateType[2]?.roomRate : null}"/>
        <g:set var="roomTypeTwoExtraBed"
               value="${hotelInstance1?.hotelRoomRatesVOs[0] ? hotelInstance1?.hotelRoomRatesVOs[0]?.roomRateType[2]?.roomRate : null}"/>
        <g:set var="roomTypeThreeExtraBed"
               value="${hotelInstance1?.hotelRoomRatesVOs[0] ? hotelInstance1?.hotelRoomRatesVOs[0]?.roomRateType[2]?.roomRate : null}"/>
        <g:set var="roomTypeFourExtraBed"
               value="${hotelInstance1?.hotelRoomRatesVOs[0] ? hotelInstance1?.hotelRoomRatesVOs[0]?.roomRateType[2]?.roomRate : null}"/>

        <g:set var="roomTypeOneKidPrice"
               value="${hotelInstance1?.hotelRoomRatesVOs[0] ? hotelInstance1?.hotelRoomRatesVOs[0]?.roomRateType[3]?.roomRate : null}"/>
        <g:set var="roomTypeTwoKidPrice"
               value="${hotelInstance1?.hotelRoomRatesVOs[0] ? hotelInstance1?.hotelRoomRatesVOs[0]?.roomRateType[3]?.roomRate : null}"/>
        <g:set var="roomTypeThreeKidPrice"
               value="${hotelInstance1?.hotelRoomRatesVOs[0] ? hotelInstance1?.hotelRoomRatesVOs[0]?.roomRateType[3]?.roomRate : null}"/>
        <g:set var="roomTypeFourKidPrice"
               value="${hotelInstance1?.hotelRoomRatesVOs[0] ? hotelInstance1?.hotelRoomRatesVOs[0]?.roomRateType[3]?.roomRate : null}"/>


        <g:set var="mealPlanRoomOne"
               value="${hotelInstance1?.hotelRoomRatesVOs[0] ? hotelInstance1?.hotelRoomRatesVOs[0]?.mealPlan[0]?.mealPlan : null}"/>
        <g:set var="mealPlanRoomTwo"
               value="${hotelInstance1?.hotelRoomRatesVOs[0] ? hotelInstance1?.hotelRoomRatesVOs[0]?.mealPlan[0]?.mealPlan : null}"/>
        <g:set var="mealPlanRoomThree"
               value="${hotelInstance1?.hotelRoomRatesVOs[0] ? hotelInstance1?.hotelRoomRatesVOs[0]?.mealPlan[0]?.mealPlan : null}"/>
        <g:set var="mealPlanRoomFour"
               value="${hotelInstance1?.hotelRoomRatesVOs[0] ? hotelInstance1?.hotelRoomRatesVOs[0]?.mealPlan[0]?.mealPlan : null}"/>

        <g:set var="roomOneRefundPolicyMinDay"
               value="${hotelInstance1?.hotelRoomRatesVOs[0] ? hotelInstance1?.hotelRoomRatesVOs[0].refundPolicy[0]?.dayMin : null}"/>
        <g:set var="roomTwoRefundPolicyMinDay"
               value="${hotelInstance1?.hotelRoomRatesVOs[1] ? hotelInstance1?.hotelRoomRatesVOs[1].refundPolicy[0]?.dayMin : null}"/>
        <g:set var="roomOneRefundPolicyMaxDay"
               value="${hotelInstance1?.hotelRoomRatesVOs[0] ? hotelInstance1?.hotelRoomRatesVOs[0].refundPolicy[0]?.dayMax : null}"/>
        <g:set var="roomTwoRefundPolicyMaxDay"
               value="${hotelInstance1?.hotelRoomRatesVOs[1] ? hotelInstance1?.hotelRoomRatesVOs[1].refundPolicy[0]?.dayMax : null}"/>

        <g:set var="roomOneRefundPolicy"
               value="${hotelInstance1?.hotelRoomRatesVOs[0] ? hotelInstance1?.hotelRoomRatesVOs[0].refundPolicy[0]?.deduction : null}"/>
        <g:set var="roomTwoRefundPolicy"
               value="${hotelInstance1?.hotelRoomRatesVOs[1] ? hotelInstance1?.hotelRoomRatesVOs[1].refundPolicy[0]?.deduction : null}"/>
        <g:set var="roomOneRefundPolicyTwo"
               value="${hotelInstance1?.hotelRoomRatesVOs[0] ? hotelInstance1?.hotelRoomRatesVOs[0].refundPolicy[1]?.deduction : null}"/>
        <g:set var="roomOneRefundPolicyTwoMinDay"
               value="${hotelInstance1?.hotelRoomRatesVOs[0] ? hotelInstance1?.hotelRoomRatesVOs[0].refundPolicy[1]?.dayMin : null}"/>
        <g:set var="roomOneRefundPolicyTwoMaxDay"
               value="${hotelInstance1?.hotelRoomRatesVOs[1] ? hotelInstance1?.hotelRoomRatesVOs[1].refundPolicy[1]?.dayMax : null}"/>

        <small style="display: block;color: #24AA7A">Price:&nbsp;${i == 0 ? roomTypeOneAopPrice : i == 1 ? roomTypeTwoAopPrice : i == 2 ? roomTypeThreeAopPrice : i == 3 ? roomTypeFourAopPrice : null}&nbsp;INR</small>
        <small style="display: block;color: #24AA7A">ExtraBed Charges:&nbsp;${i == 0 ? roomTypeOneExtraBed : i == 1 ? roomTypeTwoExtraBed : i == 2 ? roomTypeThreeExtraBed : i == 3 ? roomTypeFourExtraBed : null}&nbsp;INR</small>
        <small style="display: block;color: #24AA7A">Kids Charges:&nbsp;${i == 0 ? roomTypeOneKidPrice : i == 1 ? roomTypeTwoKidPrice : i == 2 ? roomTypeThreeKidPrice : i == 3 ? roomTypeFourKidPrice : null}&nbsp;INR</small>
        <small style="display: block;color: #24AA7A">Meal Plan:&nbsp;${i == 0 ? mealPlanRoomOne : i == 1 ? mealPlanRoomTwo : i == 2 ? mealPlanRoomThree : i == 3 ? mealPlanRoomFour : null}&nbsp;</small>

        <g:if test="${i == 0}">
            <g:each in="${hotelInstance1?.hotelRoomRatesVOs[0]}"
                    var="refundPolicy1">

                <g:each in="${refundPolicy1?.refundPolicy}" var="deduction1" status="c">
                    <g:if test="${c == 0 || c == 1 || c == 2}">
                        <small style="display: block;color: #24AA7A">Cancellation charge before day:&nbsp;${deduction1.dayMin}-${deduction1.dayMax} is ${deduction1.deduction}</small>
                    </g:if>
                </g:each>
            </g:each>

        </g:if>
        <g:if test="${i == 1}">
            <g:each in="${hotelInstance1?.hotelRoomRatesVOs[1]}"
                    var="refundPolicy1">
                <g:each in="${refundPolicy1?.refundPolicy}" var="deduction1" status="c">
                    <g:if test="${c == 0 || c == 1 || c == 2}">
                        <small style="display: block;color: #24AA7A">Cancellation charge before day:&nbsp;${deduction1.dayMin}-${deduction1.dayMax} is ${deduction1.deduction}</small>
                    </g:if>
                </g:each>
            </g:each>
        </g:if>
        <h6>Per Room Per Night</h6>
    </div>

    <div class="col-md-9">
    </div>

    <div>&nbsp;</div>

    <div class="col-md-10">
        Rating&nbsp;N Reviews:
        <button class="btn btn-xs pull-right"><small>Deal/Offer/Special Offer</small>
        </button>
    </div>

    <div class="col-md-10">&nbsp;</div>

    <div>&nbsp;</div>


    <div class="col-md-7 pull-right">
        <sec:ifLoggedIn>
            <g:form controller="hotelService" action="hotelBooking" style="display: inline">

                <input type="hidden" name="childAgeRoomFour" value="${childAgeRoomFour}">
                <input type="hidden" name="childAgeRoomThree" value="${childAgeRoomThree}">
                <input type="hidden" name="childAgeRoomTwo" value="${childAgeRoomTwo}">
                <input type="hidden" name="childAgeRoomOne" value="${childAgeRoomOne}">
                <input type="hidden" name="hotelName" value="${hotelVOInstance?.name}">
                <input type="hidden" name="hotelId" value="${hotelVOInstance?.hotelId}">
                <input type="hidden" name="childInRoomTwo" value="${childInRoomTwo}">
                <input type="hidden" name="childInRoomThree" value="${childInRoomThree}">
                <input type="hidden" name="childInRoomFour" value="${childInRoomFour}">
                <input type="hidden" name="peopleInRoomTwo" value="${peopleInRoomTwo}">
                <input type="hidden" name="peopleInRoomThree" value="${peopleInRoomThree}">
                <input type="hidden" name="peopleInRoomFour" value="${peopleInRoomFour}">
                <input type="hidden" name="noOfRoom" value="${noOfRoom}">
                <input type="hidden" name="hotelRoomCatId" value="${hotelRoomInstance?.roomCategoryId}">
                <input type="hidden" name="checkOut" value="${checkOut}">
                <input type="hidden" name="checkIn" value="${checkIn}">
                <input type="hidden" name="child" value="${child}">
                <input type="hidden" name="adult" value="${adult}">
                <input type="hidden" name="supplierId" value="${supplierId}">
                <input type="hidden" name="roomType"
                       value="${hotelRoomInstance.roomCategoryId == "891" ? "4" : hotelRoomInstance.roomCategoryId == "892" ? "2" : hotelRoomInstance.roomCategoryId == "10623" ? "2" :
                           hotelRoomInstance.roomCategoryId == "10622" ? "2" : hotelRoomInstance.roomCategoryId == "5389" ? "4" : hotelRoomInstance.roomCategoryId == "5388" ? "2" : hotelRoomInstance.roomCategoryId == "32" ? "2" :
                               hotelRoomInstance.roomCategoryId == "6619" ? "Quad" : hotelRoomInstance.roomCategoryId == "6612" ? "Double" : ""}">
                <g:submitButton name="BookNow" class="btn btn-small btn-info"/>
            </g:form>

        </sec:ifLoggedIn>
        <sec:ifNotLoggedIn>
            <g:link controller="login" action="auth" id="loginForm" onclick="setRedirectUrl()"
                    class="btn btn-small btn-info">BOOK NOW</g:link>
        </sec:ifNotLoggedIn>

    %{--<g:isLoggedIn>--}%
        <g:link class="btn btn-small btn-info" controller="hotel"
                action="addToSummary"
                params="[hotelId: hotelVOInstance?.hotelId, hotelPlace: hotelVOInstance?.state, hotelName: hotelVOInstance?.name, hotelRoomCategoryId: hotelRoomInstance?.roomCategoryId, noOfRoom: noOfRoom, roomType: hotelRoomInstance?.name, checkIn: checkIn, checkOut: checkOut, child: child, adult: adult, price: price]">Add To Travel Summary
        </g:link>
    </div>
    <br>
</div>
</div>
</g:each>
</div>
</div>

</div>
</div>
</div>
</div>

</div>
</div>
</div>

<div class="col-md-3 col-sm-3 col-sidebar-right">

    <h4>Advertisement</h4>


    <div class="list-group">

        <a href="javascript:;" class="list-group-item"><h3 class="pull-right"><i class="fa fa-eye"></i></h3>
            <h4 class="list-group-item-heading">Advertisement</h4>

            <p class="list-group-item-text">Advertisement Views</p>

        </a>

        <a href="javascript:;" class="list-group-item"><h3 class="pull-right"><i class="fa fa-facebook-square"></i></h3>
            <h4 class="list-group-item-heading">Advertisement</h4>

            <p class="list-group-item-text">Facebook Advertisement Likes</p>

        </a>

        <a href="javascript:;" class="list-group-item"><h3 class="pull-right"><i class="fa fa-twitter-square"></i></h3>
            <h4 class="list-group-item-heading">Advertisement</h4>

            <p class="list-group-item-text">Twitter Advertisement Followers</p>

        </a>
    </div> <!-- /.list-group -->

    <br/>


    <div class="well">
        <h4>Recent Activity</h4>
        <ul class="icons-list text-md">
            <li>
                <i class="icon-li fa fa-location-arrow"></i>
                <strong>Admin</strong> uploaded 6 files.
                <br/>
                <small>about 4 hours ago</small>
            </li>

            <li>
                <i class="icon-li fa fa-location-arrow"></i>
                <strong>Admin</strong> followed the research interest: <a
                    href="javascript:;">Open Access Books in Linguistics</a>.
                <br/>
                <small>about 23 hours ago</small>
            </li>
            <li>
                <i class="icon-li fa fa-location-arrow"></i>
                <strong>Admin</strong> added 51 papers.
                <br/>
                <small>2 days ago</small>
            </li>
        </ul>

    </div>
</div>

</div> <!-- /.row -->

</div> <!-- /#content-container -->

</div> <!-- #content -->

</div> <!-- #wrapper -->

<script src="${resource(dir: '/assets/js/libs', file: 'jquery-1.9.1.min.js')}"></script>
<script src="${resource(dir: '/assets/js/libs', file: 'jquery-ui-1.9.2.custom.min.js')}"></script>
<script src="${resource(dir: '/assets/js/libs', file: 'bootstrap.min.js')}"></script>

<script src="${resource(dir: '/assets/js/plugins', file: 'magnific/jquery.magnific-popup.min.js')}"></script>

<script src="${resource(dir: '/assets/js', file: 'App.js')}"></script>


<script src="${resource(dir: 'js', file: 'prism.js')}"></script>
<script src="${resource(dir: 'js', file: 'chosen.jquery.js')}"></script>
<script src="${resource(dir: '/assets/js/libs', file: 'jquery-1.9.1.min.js')}"></script>
<script src="${resource(dir: 'js', file: 'chosen.jquery.js')}"></script>
<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


</script>


<script>
    function setRedirectUrl() {
        $.ajax({
            url: "${createLink(controller: "hotel" , action: "savePreviousUrl")}",
            type: "POST",
            success: function (data, textStatus) {
//                 alert(data);
            }
        });
    }

    function loadHotelPics(hotelId) {
        alert("hi")
        var hotelIdData = hotelId
        $.ajax({
            url: "${createLink(controller: "parseData" , action: "loadHotelPictures")}",
            type: "POST",
            data: {hotelIdData:hotelIdData},
            success: function (success) {
//                alert(success)
                $('#hotelPics').html(success);
            }
        });
    }
</script>
<style>
#content1 {
    background: #FFF;
    width: auto;
    min-height: 800px;
    z-index: 18;
    padding-bottom: 25px;
    margin-left: 20px;
    margin-right: 20px;
    position: relative;
    left: auto;
    top: auto;
}</style>
<script src="${resource(dir: '/assets/js', file: 'App.js')}"></script>
<script src="${resource(dir: '/assets/js', file: 'jCarousel.js')}"></script>

</body>
</html>