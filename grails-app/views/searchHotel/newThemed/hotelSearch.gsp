<!DOCTYPE html>
<html lang="en" ng-app>
<head>
    %{--<meta name="layout" content="headerFooterLayout"/>--}%
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Travelex360</title>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'jquery-ui.css')}"/>
    <script src="${resource(dir: 'js', file: 'jquery-1.10.2.js')}"></script>
    <script src="${resource(dir: 'js', file: 'jquery-ui.js')}"></script>
    <link href="${resource(dir: 'css', file: 'mainNew.css')}" rel="stylesheet"/>
    <link href="${resource(dir: 'css', file: 'datepicker3.css')}" rel="stylesheet"/>
    <link type="text/css" rel="stylesheet" href="${resource(dir: 'css', file: 'jquery.selectBoxIt.css')}"/>

    <link href="${resource(dir: 'css', file: 'style.css')}" rel="stylesheet"/>
    <link href="${resource(dir: 'css', file: 'theme.css')}" rel="stylesheet"/>
    <link href="${resource(dir: 'css', file: 'media.css')}" rel="stylesheet"/>
    <script src="${resource(dir: 'js', file: 'ie8-responsive-file-warning.js')}"></script>
    <script src="${resource(dir: 'js', file: 'ie-emulation-modes-warning.js')}"></script>
    <script src="${resource(dir: 'js/RequiredHotelJs', file: 'generateOccupantTag.js')}"></script>

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <!--<script src="/js/ie8-responsive-file-warning.js"></script>-->
    %{--<![endif]-->--}%
    %{--<script src="/js/ie-emulation-modes-warning.js"></script>--}%
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <style>
    .ui-autocomplete {
        max-height: 100px;
        overflow-y: auto;
        /* prevent horizontal scrollbar */
        overflow-x: hidden;
    }

    /* IE 6 doesn't support max-height
     * we use height instead, but this forces the menu to always be this tall
     */
    * html .ui-autocomplete {
        height: 100px;
    }
    </style>
</head>


<body class="book-hotel-page theme-orange">
<div class="page-wrapper full-height">

<header class="main-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="logo">
                    <a href="${createLink(uri: "/")}">
                        <img src="${resource(dir: 'images', file: 'logo.png')}" width="190px">
                    </a>
                </div>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                <div class="header-right">
                    <ul class="header-icon-nav">
                        <li><a href="${createLink(controller: 'signIn', action: 'index')}"><img
                                src= ${resource(dir: 'images', file: 'register-icon.png')}></a></li>
                        <li><a href="${createLink(controller: 'login', action: 'auth')}"><img
                                src= ${resource(dir: 'images', file: 'login-icon.png')}></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>


<div class="inner-page-container">
<!-- <div class="middle-container"> -->
<div class="container-fluid">
<div class="main-search-container">
<div class="main-icons-container-vertical">
    <a href="#" class="main-icon-box">
        <img src="${resource(dir: 'images', file: 'ico1.png')}">

        <h3>PLAN TRAVEL</h3>
    </a>
    <a href="${createLink(controller: "hotelService", action: "bookHotel")}" class="main-icon-box active">
        <img src="${resource(dir: 'images', file: 'ico2.png')}">

        <h3>BOOK HOTEL</h3>
    </a>
    <a href="#" class="main-icon-box">
        <img src= ${resource(dir: 'images', file: 'ico3.png')}>

        <h3>ACTIVITY</h3>
    </a>
    <a href="${createLink(controller: "tboFlightBooking", action: "searchFlights")}" class="main-icon-box">
        <img src= ${resource(dir: 'images', file: 'ico4.png')}>

        <h3>FLIGHT BOOKING</h3>
    </a>
    <a href="${createLink(controller: "serviceProvider", action: "chooseService")}" class="main-icon-box">
        <img src= ${resource(dir: 'images', file: 'ico5.png')}>

        <h3>SERVICE PROVIDER</h3>
    </a>
    <a href="${createLink(controller: "holiday", action: "showHolidayCategory")}" class="main-icon-box">
        <img src= ${resource(dir: 'images', file: 'ico6.png')}>

        <h3>PACKAGES</h3>
    </a>
</div>

<div class="main-search-container-content">
<h1 class="box-title">Book your Hotel</h1>
<g:hasErrors bean="${hotelSearchParamsVO}">
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;
        </span><span class="sr-only">Close</span></button>

        <div id="errors" class="alert alert-error">
            <g:renderErrors bean="${hotelSearchParamsVO}"/>
        </div>
    </div>
</g:hasErrors>
<g:form class="tvx-from" data-ng-controller="book_hotel_control" controller="hotelService" action="searchData">
<div class="form-group">
    <label for="">Choose where you prefer to go</label>
    <!-- <div>
                <label class="radio-inline">
                  <input type="radio" name="inlineRadioOptions" id="" value=""> International
                </label>
                <label class="radio-inline">
                  <input type="radio" name="inlineRadioOptions" id="" value=""> Domestic
                </label>
              </div> -->
    <div>
        <div class="tick-box">
            <input id="interLocation" class="tick-radio" type="radio" name="location"
                   value="international"
                   checked="checked">
            <label for="interLocation"><span><span></span></span>International</label>
            <input id="domLocation" class="tick-radio" type="radio" name="location"
                   value="domestic"><label
                for="domLocation"><span><span></span></span>Domestic</label>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="city">Going to</label>
    <input type="text" class="form-control" id="city" name="place"
           placeholder="Select City, Area or Hotel..." value="${hotelSearchParamsVO?.place}">
</div>

<div class="form-group">
    <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-4">
            <label for="dpStart">Check in</label>

            <div class="input-group date">
                <input id="dpStart" type="text" class="form-control" autocomplete="off"
                       style="cursor: pointer" name="checkIn" value="${hotelSearchParamsVO?.checkIn}">
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-4">
            <label for="dpEnd">Check out</label>

            <div class="input-group date">
                <input id="dpEnd" type="text" class="form-control" autocomplete="off"
                       name="checkOut"
                       style="cursor: pointer" value="${hotelSearchParamsVO?.checkOut}">
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-4">
            <label for="numberOfRooms">Number of Rooms</label>

            <div class="input-group col-md-12">
                <g:select from="${['1', '2', '3', '4']}" name="noOfRooms" onchange="showMoreInputField()"
                          class="form-control" id="numberOfRooms" value="${hotelSearchParamsVO?.noOfRooms}"/>
            </div>
        </div>
    </div>
</div>

<div class="peopleInRooms" id="peopleInOneRoom">
    <div class="form-group">
        <h4>Rooms1</h4>

        <div class="row">
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label for="noOfAdult">Adults</label>

                        <div class="input-group col-md-12">
                            <select class="form-control peopleInRoom" name="adult"
                                    id="noOfAdult" onchange="checkTotalAdultGuest()">
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label for="noOfChild">Childrens</label>

                        <div class="input-group col-md-12">
                            <select class="form-control childInRoom" id="noOfChild"
                                    onchange="showChildrenAgeRoomOne();checkTotalChildrenGuest()"
                                    name="children">
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-3 firstRoomChildrens firstChildInRoomOne">
                        <label for="firstChildAgeRoomOne">Child 1 Age</label>

                        <div class="col-md-12">
                            <select class="form-control childAgeInRoomOne" name="firstChildAgeRoomOne"
                                    id="firstChildAgeRoomOne">
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-3 firstRoomChildrens secondChildInRoomOne">
                        <label for="secondChildAgeRoomOne">Child 2 Age</label>

                        <div class="col-md-12">
                            <select class="form-control childAgeInRoomOne" id="secondChildAgeRoomOne"
                                    name="secondChildAgeRoomOne">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="peopleInRooms" id="peopleInTwoRooms">
    <div class="form-group">
        <h4>Rooms2</h4>

        <div class="row">
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label for="peopleInRoomOne">Adults</label>

                        <div class="input-group col-md-12">
                            <select id="peopleInRoomOne"
                                    class="form-control peopleInRoom peopleInTwoRooms"
                                    name="peopleInRoomTwo" onchange="checkTotalAdultGuest()">
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label for="childInRoomTwo">Childrens</label>

                        <div class="input-group col-md-12">
                            <select id="childInRoomTwo" onchange="showChildrenAgeRoomTwo();checkTotalChildrenGuest()"
                                    class="form-control childInRoom childInTwoRooms"
                                    name="childInRoomTwo">
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-3 secondRoomChildrens firstChildInRoomTwo">
                        <label for="firstChildAgeRoomTwo">Child 1 Age</label>

                        <div class="input-group col-md-12">
                            <select class="form-control childAgeInRoomTwo" name="firstChildAgeRoomTwo"
                                    id="firstChildAgeRoomTwo">
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-3 secondRoomChildrens secondChildInRoomTwo">
                        <label for="secondChildAgeRoomTwo">Child 2 Age</label>

                        <div class="input-group col-md-12">
                            <select class="form-control childAgeInRoomTwo" id="secondChildAgeRoomTwo"
                                    name="secondChildAgeRoomTwo">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="peopleInRooms" id="peopleInThreeRooms">
    <div class="form-group">
        <h4>Rooms3</h4>

        <div class="row">
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label for="peopleInRoomThree">Adults</label>

                        <div class="input-group col-md-12">
                            <select id="peopleInRoomThree"
                                    class="form-control peopleInRoom peopleInThreeRooms"
                                    name="peopleInRoomThree" onchange="checkTotalAdultGuest()">
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label for="childInRoomThree">Childrens</label>

                        <div class="input-group col-md-12">
                            <select id="childInRoomThree" onchange="showChildrenAgeRoomThree();checkTotalChildrenGuest()"
                                    class="form-control childInRoom childInThreeRooms"
                                    name="childInRoomThree">
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-3 thirdRoomChildrens firstChildInRoomThree">
                        <label for="firstChildAgeRoomThree">Child 1 Age</label>

                        <div class="input-group col-md-12">
                            <select class="form-control childAgeInRoomThree"
                                    name="firstChildAgeRoomThree"
                                    id="firstChildAgeRoomThree">
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-3 thirdRoomChildrens secondChildInRoomThree">
                        <label for="secondChildAgeRoomThree">Child 2 Age</label>

                        <div class="input-group col-md-12">
                            <select class="form-control childAgeInRoomThree"
                                    id="secondChildAgeRoomThree"
                                    name="secondChildAgeRoomThree">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="peopleInRooms" id="peopleInFourRooms">
    <div class="form-group">
        <h4>Rooms4</h4>

        <div class="row">
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label for="peopleInRoomFour">Adults</label>

                        <div class="input-group col-md-12">
                            <select id="peopleInRoomFour"
                                    class="form-control peopleInRoom peopleInFourRooms"
                                    name="peopleInRoomFour" onchange="checkTotalAdultGuest()">
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label for="childInRoomFour">Childrens</label>

                        <div class="input-group col-md-12">
                            <select id="childInRoomFour" onchange="showChildrenAgeRoomFour();checkTotalChildrenGuest()"
                                    class="form-control childInRoom childInFourRooms"
                                    name="childInRoomFour">
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-3 fourthRoomChildrens firstChildInRoomFour">
                        <label for="firstChildAgeRoomFour">Child 1 Age</label>

                        <div class="input-group col-md-12">
                            <select class="form-control childAgeInRoomFour"
                                    name="firstChildAgeRoomFour"
                                    id="firstChildAgeRoomFour">
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-3 fourthRoomChildrens secondChildInRoomFour">
                        <label for="secondChildAgeRoomFour">Child 2 Age</label>

                        <div class="input-group col-md-12">
                            <select class="form-control childAgeInRoomFour" id="secondChildAgeRoomFour"
                                    name="secondChildAgeRoomFour">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="form-footer">
    <div class="row">
        <div class="col-md-7 col-sm-7">
            <div class="form-group">
                <p>Total Rooms : <strong id="totalRoom"></strong></p>

                <p>Total Guests : <strong id="totalAdults"></strong>&nbsp;Adults/<strong
                        id="totalChildren"></strong>&nbsp;Childrens
                </p>
            </div>
        </div>

        <div class="col-md-5 col-sm-5">
            <div class="form-group text-right">
                <button type="submit" class="btn btn-main btn-default">Search Hotel</button>
            </div>
        </div>
    </div>
</div>
</g:form>
</div>

<h2 class="sub-title text-center">Popular Destinations</h2>

<div class="transparent-buttons-container">
    <ul>
        <li><a href="#">Darjeeling</a></li>
        <li><a href="#">Kovalam Beach</a></li>
        <!-- <li><a href="#">Taj Mahal</a></li>
          <li><a href="#">Ajanta and Ellora Cave</a></li> -->
        <li><a href="#">Khajuraho Temples</a></li>
        <li><a href="#">Kashmir</a></li>
        <li><a href="#">Goa</a></li>
        <li><a href="#">Kerala Backwaters</a></li>
        <li><a href="#">Jaipur</a></li>
        <li><a href="#">Munnar</a></li>
        <li><a href="#">Udaipur</a></li>
        <li><a href="#">Jaisalmer</a></li>
        <li><a href="#">Leh and Ladakh</a></li>
        <li><a href="#">Kullu and Manali</a></li>
        <li><a href="#">Shimla</a></li>
        <li><a href="#">Gangtok</a></li>
    </ul>

</div>

</div>
</div>
<!-- </div> -->
</div>

</div>

<footer class="main-footer">
    <div class="container-fluid">
        <div class="footer-links">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <ul class="inline-list-links">
                        <li><a href="${createLink(controller: 'home', action: 'aboutUs')}">About Us</a></li>
                        <li><a href="${createLink(controller: 'home', action: 'howItWork')}">How It Work's</a></li>
                        <li><a href="${createLink(controller: 'home', action: 'termsOfUse')}">Terms Of Use</a></li>
                        <li><a href="${createLink(controller: 'home', action: 'contactUs')}">Contact Us</a></li>
                    </ul>
                </div>

                <div class="col-md-6 col-sm-6 text-right">
                    Copyright &copy; Travelex 360 Pvt.Ltd. 2014, All rights reserved
                </div>
            </div>
        </div>

        <div class="footer-rights">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <ul class="inline-list-links margin-top-5">
                        <li><a href="#">Post Your Complaint</a></li>
                        <li><a href="${createLink(controller: 'home', action: 'privacyPolicy')}">Privacy Policy</a></li>
                        <li><a href="#">FAQ's</a></li>
                        <li><a href="#">Site Map</a></li>
                    </ul>
                </div>

                <div class="col-md-6 col-sm-6 text-right">
                    <img src="${resource(dir: 'images', file: 'payment-methods.jpg')}">
                </div>
            </div>
        </div>
    </div>
</footer>


<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- // <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> -->
<script src="${resource(dir: 'js', file: 'script.min.js')}"></script>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28/angular.js"></script>
<script src="${resource(dir: 'js', file: 'ang_main.js')}"></script>
<script src="${resource(dir: 'js', file: 'ie10-viewport-bug-workaround.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery-ui.min.js')}"></script>
%{--<script src="${resource(dir: 'js', file: 'main.js')}"></script>--}%
<script src="${resource(dir: 'js', file: 'datepicker.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery.selectBoxIt.min.js')}"></script>
<script>

    $(document).ready(function () {
        $("#totalRoom").html($("#numberOfRooms").val());
        if (${searchFor.equals("international")}) {
            $("#interLocation").prop('checked', true)
        }
        else {
            $("#domLocation").prop('checked', true)
        }
        $('.input-group.date').datepicker({
            startDate: "today",
            autoclose: true,
            todayHighlight: true
        });
        var noOfRooms = $("#numberOfRooms").val()
        if ($("#numberOfRooms").val()) {
            if ($("#numberOfRooms").val() == "1") {
                $(".peopleInRooms").hide();
                $("#peopleInOneRoom").show();
                $(".firstRoomChildrens").hide();
                for (var i = 1; i <= 3; i++) {
                    $("#noOfAdult").append('<option value=' + i + '>' + i + '</option>');
                }
                for (var i = 0; i <= 2; i++) {
                    $("#noOfChild").append('<option value=' + i + '>' + i + '</option>');
                }
                if ($("#noOfChild").val() == "1") {
                    $(".ChildrensAgeRoomOne").show();
                    $(".firstChildInRoomOne").show();
                    for (var i = 1; i <= 9; i++) {
                        $("#firstChildAgeRoomOne").append('<option value=' + i + '>' + i + '</option>');
                    }
                    $("#firstChildAgeRoomOne").val('${hotelSearchParamsVO?.firstChildAgeRoomOne}');
                    $(".secondChildInRoomOne").hide()
                }
                else if ($("#noOfChild").val() == "2") {
                    $(".ChildrensAgeRoomOne").show();
                    $(".firstChildInRoomOne").show();
                    $(".secondChildInRoomOne").show();
                    for (var i = 1; i <= 9; i++) {
                        $("#firstChildAgeRoomOne").append('<option value=' + i + '>' + i + '</option>');
                        $("#secondChildAgeRoomOne").append('<option value=' + i + '>' + i + '</option>');
                    }
                    $("#firstChildAgeRoomOne").val('${hotelSearchParamsVO?.firstChildAgeRoomOne}');
                    $("#secondChildAgeRoomOne").val('${hotelSearchParamsVO?.secondChildAgeRoomOne}');
                }
                else if ($("#noOfChild").val() == "0") {
                    $(".ChildrensAgeRoomOne").hide()
                }
            }
            else if ($("#numberOfRooms").val() == "2") {
                $(".peopleInRooms").hide();
                $("#peopleInOneRoom").show();
                $("#peopleInTwoRooms").show();
                $(".peopleInRoom").html("");
                $(".childInRoom").html("");
                for (var i = 1; i <= 3; i++) {
                    $("#noOfAdult").append('<option value=' + i + '>' + i + '</option>');
                    $(".peopleInTwoRooms").append('<option value=' + i + '>' + i + '</option>');
                }
                for (var i = 0; i <= 2; i++) {
                    $("#noOfChild").append('<option value=' + i + '>' + i + '</option>');
                    $(".childInTwoRooms").append('<option value=' + i + '>' + i + '</option>');
                }
                if ('${hotelSearchParamsVO?.adult}') {
                    $("#noOfAdult").val('${hotelSearchParamsVO?.adult}');
                    $("#noOfChild").val('${hotelSearchParamsVO?.children}');
                }
                else {
                }
                if ($("#noOfChild").val() == "1") {
                    $(".ChildrensAgeRoomOne").show();
                    $(".firstChildInRoomOne").show();
                    $(".childAgeInRoomOne").html("");
                    for (var i = 1; i <= 9; i++) {
                        $("#firstChildAgeRoomOne").append('<option value=' + i + '>' + i + '</option>');
                    }
                    $("#firstChildAgeRoomOne").val('${hotelSearchParamsVO?.firstChildAgeRoomOne}');
                    $(".secondChildInRoomOne").hide()
                }
                else if ($("#noOfChild").val() == "2") {
                    $(".ChildrensAgeRoomOne").show();
                    $(".firstChildInRoomOne").show();
                    $(".secondChildInRoomOne").show();
                    $(".childAgeInRoomOne").html("");
                    for (var i = 1; i <= 9; i++) {
                        $("#firstChildAgeRoomOne").append('<option value=' + i + '>' + i + '</option>');
                        $("#secondChildAgeRoomOne").append('<option value=' + i + '>' + i + '</option>');
                    }
                    $("#firstChildAgeRoomOne").val('${hotelSearchParamsVO?.firstChildAgeRoomOne}');
                    $("#secondChildAgeRoomOne").val('${hotelSearchParamsVO?.secondChildAgeRoomOne}');
                }
                else if ($("#noOfChild").val() == "0") {
                    $(".ChildrensAgeRoomOne").hide()
                }

                if ('${hotelSearchParamsVO?.peopleInRoomTwo}') {
                    $("#peopleInRoomOne").val('${hotelSearchParamsVO?.peopleInRoomTwo}');
                    $("#childInRoomTwo").val('${hotelSearchParamsVO?.childInRoomTwo}')
                }
                else {
                }
                if ($("#childInRoomTwo").val() == "1") {
                    $(".secondRoomChildrens").hide();
                    $(".firstChildInRoomTwo").show();
                    $(".childAgeInRoomTwo").html("");
                    for (var i = 1; i <= 9; i++) {
                        $("#firstChildAgeRoomTwo").append('<option value=' + i + '>' + i + '</option>');

                    }
                    $("#firstChildAgeRoomTwo").val('${hotelSearchParamsVO?.firstChildAgeRoomTwo}')
                }
                else if ($("#childInRoomTwo").val() == "2") {
                    $(".secondRoomChildrens").hide();
                    $(".firstChildInRoomTwo").show();
                    $(".secondChildInRoomTwo").show();
                    $(".childAgeInRoomTwo").html("");
                    for (var i = 1; i <= 9; i++) {
                        $("#firstChildAgeRoomTwo").append('<option value=' + i + '>' + i + '</option>');
                        $("#secondChildAgeRoomTwo").append('<option value=' + i + '>' + i + '</option>');
                    }
                    $("#firstChildAgeRoomTwo").val('${hotelSearchParamsVO?.firstChildAgeRoomTwo}')
                    $("#secondChildAgeRoomTwo").val('${hotelSearchParamsVO?.secondChildAgeRoomTwo}');

                }
                if ($("#childInRoomTwo").val() == "0") {
                    $(".secondRoomChildrens").hide();
                }
            }
            else if ($("#numberOfRooms").val() == "3") {
                $(".peopleInRooms").hide();
                $("#peopleInOneRoom").show();
                $("#peopleInTwoRooms").show();
                $("#peopleInThreeRooms").show();

                $(".peopleInRoom").html("");
                $(".childInRoom").html("");
                for (var i = 1; i <= 3; i++) {
                    $("#noOfAdult").append('<option value=' + i + '>' + i + '</option>');
                    $(".peopleInTwoRooms").append('<option value=' + i + '>' + i + '</option>');
                    $(".peopleInThreeRooms").append('<option value=' + i + '>' + i + '</option>');
                }
                for (var i = 0; i <= 2; i++) {
                    $("#noOfChild").append('<option value=' + i + '>' + i + '</option>');
                    $(".childInTwoRooms").append('<option value=' + i + '>' + i + '</option>');
                    $(".childInThreeRooms").append('<option value=' + i + '>' + i + '</option>');
                }
                if ('${hotelSearchParamsVO?.adult}') {
                    $("#noOfAdult").val('${hotelSearchParamsVO?.adult}');
                    $("#noOfChild").val('${hotelSearchParamsVO?.children}');
                }
                else {
                }

                if ($("#noOfChild").val() == "1") {
                    $(".ChildrensAgeRoomOne").show();
                    $(".firstChildInRoomOne").show();
                    $(".childAgeInRoomOne").html("");
                    for (var i = 1; i <= 9; i++) {
                        $("#firstChildAgeRoomOne").append('<option value=' + i + '>' + i + '</option>');
                    }
                    $("#firstChildAgeRoomOne").val('${hotelSearchParamsVO?.firstChildAgeRoomOne}');
                    $(".secondChildInRoomOne").hide()
                }
                else if ($("#noOfChild").val() == "2") {
                    $(".ChildrensAgeRoomOne").show();
                    $(".firstChildInRoomOne").show();
                    $(".secondChildInRoomOne").show();
                    $(".childAgeInRoomOne").html("");
                    for (var i = 1; i <= 9; i++) {
                        $("#firstChildAgeRoomOne").append('<option value=' + i + '>' + i + '</option>');
                        $("#secondChildAgeRoomOne").append('<option value=' + i + '>' + i + '</option>');
                    }
                    $("#firstChildAgeRoomOne").val('${hotelSearchParamsVO?.firstChildAgeRoomOne}');
                    $("#secondChildAgeRoomOne").val('${hotelSearchParamsVO?.secondChildAgeRoomOne}');
                }
                else if ($("#noOfChild").val() == "0") {
                    $(".ChildrensAgeRoomOne").hide()
                }

                if ('${hotelSearchParamsVO?.peopleInRoomTwo}') {
                    $("#peopleInRoomOne").val('${hotelSearchParamsVO?.peopleInRoomTwo}');
                    $("#childInRoomTwo").val('${hotelSearchParamsVO?.childInRoomTwo}');
                }
                else {
                }

                if ($("#childInRoomTwo").val() == "1") {
                    $(".secondRoomChildrens").hide();
                    $(".firstChildInRoomTwo").show();
                    $(".childAgeInRoomTwo").html("");
                    for (var i = 1; i <= 9; i++) {
                        $("#firstChildAgeRoomTwo").append('<option value=' + i + '>' + i + '</option>');

                    }
                    $("#firstChildAgeRoomTwo").val('${hotelSearchParamsVO?.firstChildAgeRoomTwo}')
                }
                else if ($("#childInRoomTwo").val() == "2") {
                    $(".secondRoomChildrens").hide();
                    $(".firstChildInRoomTwo").show();
                    $(".secondChildInRoomTwo").show();
                    $(".childAgeInRoomTwo").html("");
                    for (var i = 1; i <= 9; i++) {
                        $("#firstChildAgeRoomTwo").append('<option value=' + i + '>' + i + '</option>');
                        $("#secondChildAgeRoomTwo").append('<option value=' + i + '>' + i + '</option>');
                    }
                    $("#firstChildAgeRoomTwo").val('${hotelSearchParamsVO?.firstChildAgeRoomTwo}')
                    $("#secondChildAgeRoomTwo").val('${hotelSearchParamsVO?.secondChildAgeRoomTwo}');

                }
                if ($("#childInRoomTwo").val() == "0") {
                    $(".secondRoomChildrens").hide();
                }

                if ('${hotelSearchParamsVO?.peopleInRoomThree}') {
                    $("#peopleInRoomThree").val('${hotelSearchParamsVO?.peopleInRoomThree}');
                    $("#childInRoomThree").val('${hotelSearchParamsVO?.childInRoomThree}');
                }
                else {
                }

                if ($("#childInRoomThree").val() == "1") {
                    $(".ChildrensAgeRoomThree").show();
                    $(".thirdRoomChildrens").hide();
                    $(".firstChildInRoomThree").show();
                    $(".childAgeInRoomThree").html("");
                    for (var i = 1; i <= 9; i++) {
                        $("#firstChildAgeRoomThree").append('<option value=' + i + '>' + i + '</option>');

                    }
                    $("#firstChildAgeRoomThree").val('${hotelSearchParamsVO?.firstChildAgeRoomThree}')
                }
                else if ($("#childInRoomThree").val() == "2") {
                    $(".ChildrensAgeRoomThree").show();
                    $(".thirdRoomChildrens").hide();
                    $(".firstChildInRoomThree").show();
                    $(".secondChildInRoomThree").show();
                    $(".childAgeInRoomThree").html("");
                    for (var i = 1; i <= 9; i++) {
                        $("#firstChildAgeRoomThree").append('<option value=' + i + '>' + i + '</option>');
                        $("#secondChildAgeRoomThree").append('<option value=' + i + '>' + i + '</option>');
                    }
                    $("#firstChildAgeRoomThree").val('${hotelSearchParamsVO?.firstChildAgeRoomThree}');
                    $("#secondChildAgeRoomThree").val('${hotelSearchParamsVO?.secondChildAgeRoomThree}');
                }
                else if ($("#childInRoomThree").val() == "0") {
                    $(".ChildrensAgeRoomThree").hide();
                }
            }
            else if ($("#numberOfRooms").val() == "4") {
                $(".peopleInRooms").hide();
                $("#peopleInOneRoom").show();
                $("#peopleInTwoRooms").show();
                $("#peopleInThreeRooms").show();
                $("#peopleInFourRooms").show();
                $(".peopleInRoom").html("");
                $(".childInRoom").html("");
                for (var i = 1; i <= 3; i++) {
                    $("#noOfAdult").append('<option value=' + i + '>' + i + '</option>');
                    $(".peopleInTwoRooms").append('<option value=' + i + '>' + i + '</option>');
                    $(".peopleInThreeRooms").append('<option value=' + i + '>' + i + '</option>');
                    $(".peopleInFourRooms").append('<option value=' + i + '>' + i + '</option>');
                }
                for (var i = 0; i <= 2; i++) {
                    $("#noOfChild").append('<option value=' + i + '>' + i + '</option>');
                    $(".childInTwoRooms").append('<option value=' + i + '>' + i + '</option>');
                    $(".childInThreeRooms").append('<option value=' + i + '>' + i + '</option>');
                    $(".childInFourRooms").append('<option value=' + i + '>' + i + '</option>');
                }

                if ('${hotelSearchParamsVO?.adult}') {
                    $("#noOfAdult").val('${hotelSearchParamsVO?.adult}');
                    $("#noOfChild").val('${hotelSearchParamsVO?.children}');
                }
                else {
                }


                if ($("#noOfChild").val() == "1") {
                    $(".ChildrensAgeRoomOne").show();
                    $(".firstChildInRoomOne").show();
                    $(".childAgeInRoomOne").html("");
                    for (var i = 1; i <= 9; i++) {
                        $("#firstChildAgeRoomOne").append('<option value=' + i + '>' + i + '</option>');
                    }
                    $("#firstChildAgeRoomOne").val('${hotelSearchParamsVO?.firstChildAgeRoomOne}');
                    $(".secondChildInRoomOne").hide()
                }
                else if ($("#noOfChild").val() == "2") {
                    $(".ChildrensAgeRoomOne").show();
                    $(".firstChildInRoomOne").show();
                    $(".secondChildInRoomOne").show();
                    $(".childAgeInRoomOne").html("");
                    for (var i = 1; i <= 9; i++) {
                        $("#firstChildAgeRoomOne").append('<option value=' + i + '>' + i + '</option>');
                        $("#secondChildAgeRoomOne").append('<option value=' + i + '>' + i + '</option>');
                    }
                    $("#firstChildAgeRoomOne").val('${hotelSearchParamsVO?.firstChildAgeRoomOne}');
                    $("#secondChildAgeRoomOne").val('${hotelSearchParamsVO?.secondChildAgeRoomOne}');
                }
                else if ($("#noOfChild").val() == "0") {
                    $(".ChildrensAgeRoomOne").hide()
                }

                if ('${hotelSearchParamsVO?.peopleInRoomTwo}') {
                    $("#peopleInRoomOne").val('${hotelSearchParamsVO?.peopleInRoomTwo}');
                    $("#childInRoomTwo").val('${hotelSearchParamsVO?.childInRoomTwo}');
                }
                else {
                }

                if ($("#childInRoomTwo").val() == "1") {
                    $(".secondRoomChildrens").hide();
                    $(".firstChildInRoomTwo").show();
                    $(".childAgeInRoomTwo").html("");
                    for (var i = 1; i <= 9; i++) {
                        $("#firstChildAgeRoomTwo").append('<option value=' + i + '>' + i + '</option>');

                    }
                    $("#firstChildAgeRoomTwo").val('${hotelSearchParamsVO?.firstChildAgeRoomTwo}')
                }
                else if ($("#childInRoomTwo").val() == "2") {
                    $(".secondRoomChildrens").hide();
                    $(".firstChildInRoomTwo").show();
                    $(".secondChildInRoomTwo").show();
                    $(".childAgeInRoomTwo").html("");
                    for (var i = 1; i <= 9; i++) {
                        $("#firstChildAgeRoomTwo").append('<option value=' + i + '>' + i + '</option>');
                        $("#secondChildAgeRoomTwo").append('<option value=' + i + '>' + i + '</option>');
                    }
                    $("#firstChildAgeRoomTwo").val('${hotelSearchParamsVO?.firstChildAgeRoomTwo}')
                    $("#secondChildAgeRoomTwo").val('${hotelSearchParamsVO?.secondChildAgeRoomTwo}');

                }
                if ($("#childInRoomTwo").val() == "0") {
                    $(".secondRoomChildrens").hide();
                }

                if ('${hotelSearchParamsVO?.peopleInRoomThree}') {
                    $("#peopleInRoomThree").val('${hotelSearchParamsVO?.peopleInRoomThree}');
                    $("#childInRoomThree").val('${hotelSearchParamsVO?.childInRoomThree}');
                }
                else {
                }

                if ($("#childInRoomThree").val() == "1") {
                    $(".ChildrensAgeRoomThree").show();
                    $(".thirdRoomChildrens").show();
                    $(".firstChildInRoomThree").show();
                    $(".secondChildInRoomThree").hide();
                    $(".childAgeInRoomThree").html("");
                    for (var i = 1; i <= 9; i++) {
                        $("#firstChildAgeRoomThree").append('<option value=' + i + '>' + i + '</option>');
                    }
                    $("#firstChildAgeRoomThree").val('${hotelSearchParamsVO?.firstChildAgeRoomThree}')
                }
                else if ($("#childInRoomThree").val() == "2") {
                    $(".ChildrensAgeRoomThree").show();
                    $(".thirdRoomChildrens").show();
                    $(".firstChildInRoomThree").show();
                    $(".secondChildInRoomThree").show();
                    $(".childAgeInRoomThree").html("");
                    for (var i = 1; i <= 9; i++) {
                        $("#firstChildAgeRoomThree").append('<option value=' + i + '>' + i + '</option>');
                        $("#secondChildAgeRoomThree").append('<option value=' + i + '>' + i + '</option>');
                    }
                    $("#firstChildAgeRoomThree").val('${hotelSearchParamsVO?.firstChildAgeRoomThree}');
                    $("#secondChildAgeRoomThree").val('${hotelSearchParamsVO?.secondChildAgeRoomThree}');
                }
                else if ($("#childInRoomThree").val() == "0") {
                    $(".ChildrensAgeRoomThree").hide();
                }

                if ('${hotelSearchParamsVO?.peopleInRoomThree}') {
                    $("#peopleInRoomFour").val('${hotelSearchParamsVO?.peopleInRoomFour}')
                    $("#childInRoomFour").val('${hotelSearchParamsVO?.childInRoomFour}')
                }
                else {
                }


                if ($("#childInRoomFour").val() == "1") {
                    $(".ChildrensAgeRoomFour").show();
                    $(".fourthRoomChildrens").show();
                    $(".firstChildInRoomFour").show();
                    $(".secondChildInRoomFour").hide();
                    $(".childAgeInRoomFour").html("");
                    for (var i = 1; i <= 9; i++) {
                        $("#firstChildAgeRoomFour").append('<option value=' + i + '>' + i + '</option>');
                    }
                    $("#firstChildAgeRoomFour").val('${hotelSearchParamsVO?.firstChildAgeRoomFour}')
                }
                else if ($("#childInRoomFour").val() == "2") {
                    $(".ChildrensAgeRoomFour").show();
                    $(".fourthRoomChildrens").show();
                    $(".firstChildInRoomFour").show();
                    $(".secondChildInRoomFour").show();
                    $(".childAgeInRoomFour").html("");
                    for (var i = 1; i <= 9; i++) {
                        $("#firstChildAgeRoomFour").append('<option value=' + i + '>' + i + '</option>');
                        $("#secondChildAgeRoomFour").append('<option value=' + i + '>' + i + '</option>');
                    }
                    $("#firstChildAgeRoomFour").val('${hotelSearchParamsVO?.firstChildAgeRoomFour}');
                    $("#secondChildAgeRoomFour").val('${hotelSearchParamsVO?.secondChildAgeRoomFour}');
                }
                else if ($("#childInRoomFour").val() == "0") {
                    $(".ChildrensAgeRoomFour").hide();
                }
            }
            else {
                $(".peopleInRooms").hide();
                $(".ChildrensAgeRoomOne").hide();
                $(".ChildrensAgeRoomTwo").hide();
                $(".ChildrensAgeRoomThree").hide();
                $(".ChildrensAgeRoomFour").hide();
            }
        }

        var total = 0;
        var adultRoom1 = $("#noOfAdult").val();
        if (adultRoom1) {
            total = parseInt(total)+parseInt(adultRoom1)
        }
        var adultRoom2 = $("#peopleInRoomOne").val();
        if (adultRoom2) {
            total = parseInt(total)+parseInt(adultRoom2)
        }
        var adultRoom3 = $("#peopleInRoomThree").val();
        if (adultRoom3) {
            total = parseInt(total)+parseInt(adultRoom3)
        }
        var adultRoom4 = $("#peopleInRoomFour").val();
        if (adultRoom4) {
            total = parseInt(total)+parseInt(adultRoom4)
        }
        $("#totalAdults").html(total);

        var total1 = 0;
        var childRoom1 = $("#noOfChild").val();
        if (childRoom1) {
            total1 = parseInt(total1)+parseInt(childRoom1)
        }
        var childRoom2 = $("#childInRoomTwo").val();
        if (childRoom2) {
            total1 = parseInt(total1)+parseInt(childRoom2)
        }
        var childRoom3 = $("#childInRoomThree").val();
        if (childRoom3) {
            total1 = parseInt(total1)+parseInt(childRoom3)
        }
        var childRoom4 = $("#childInRoomFour").val();
        if (childRoom4) {
            total1 = parseInt(total1)+parseInt(childRoom4)
        }
        $("#totalChildren").html(total1);

        $("#city").autocomplete({
            source: function (request, response) {
                var dom = $("#domLocation").val();
                var inter = $("#interLocation").val();
                var selectedCheckBox = new Array();
                $('input[name="location"]:checked').each(function () {
                    selectedCheckBox.push(this.value);
                });
                $.ajax({
                    url: '${createLink(controller: 'hotelService',action: 'findCityForHotel')}',
                    dataType: 'json',
                    data: {
                        term: request.term,
                        city: selectedCheckBox
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            },
            %{--source: "${createLink(controller: "hotelService", action: "findCityForHotel" ,absolute: true)}"--}%
        });
        $("#city").autocomplete({ minLength: 2 })
    });
</script>

</body>
</html>
