<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">

    <title></title>
</head>

<body>
<div id="wrapper">

    <div id="content1">
        <div id="content-header">
            <h3>&nbsp;&nbsp;</h3>

            <h2>&nbsp;Confirm Booking</h2>
        </div>

        %{--<div id="content-container">--}%
        <div class="container">
            <div class="row">
                <div id="content-container">

                    <div class="row">

                        <div class="col-md-12">

                            <div class="error-container">

                                <!-- /.error-code -->

                                <div class="error-details">
                                    <div class="error-code">
                                        <h4><p><strong>${bookingDescription}</strong></p></h4>

                                        <g:if test="${bookingId && bookingId != '0'}">
                                            <g:link class="btn btn-primary btn-sm" controller="hotelService"
                                                    action="confirmBooking"
                                                    params="[bookingId: bookingId, totalRate: totalRate]">Show Booking Details
                                            </g:link>
                                            <h5><p><strong> please save this booking id for future reference</strong>
                                            </p></h5>

                                        </g:if>
                                        <g:else>
                                            <h5><p><strong>Unable to Book the Hotel</strong>

<a class="btn btn-info"
   href="${createLink(controller: "home")}">Click to return Home</a>
                                        </g:else>

                                    </div>
                                    <h4>${bookingId}</h4>

                                </div> <!-- /.error-details -->

                            </div> <!-- /.error -->

                        </div> <!-- /.col-md-12 -->

                    </div> <!-- /.row -->

                </div>
            </div>
        </div>
    </div>
</div>
<style>
#content1 {
    background: #FFF;
    width: auto;
    min-height: 800px;
    z-index: 18;
    padding-bottom: 25px;
    margin-left: 20px;
    margin-right: 20px;
    position: relative;
    left: auto;
    top: auto;
}</style>
</body>
</html>