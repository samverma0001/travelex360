<div id="styledModal" class="modal modal-styled fade" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h3 class="modal-title">Hotel Details - ${hotelInstance1?.hotelName}</h3>
            </div>

            <div class="modal-body">
                <p>${hotelVOInstance?.description}</p>

                <p> <h5><small>Hotel Rating:</small>&nbsp;
                <g:if test="${hotelInstance1?.hotelRating == 'OneStar'}">
                    <h5><small>Rating:</small>&nbsp;<g:each
                            in="${1..1}"
                            var="a"><img
                                src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                    </h5>
                </g:if>
                <g:elseif test="${hotelInstance1?.hotelRating == 'TwoStar'}">
                    <h5><small>Rating:</small>&nbsp;<g:each
                            in="${1..2}"
                            var="a"><img
                                src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                    </h5>
                </g:elseif>
                <g:elseif test="${hotelInstance1?.hotelRating == 'ThreeStar'}">
                    <h5><small>Rating:</small>&nbsp;<g:each
                            in="${1..3}"
                            var="a"><img
                                src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                    </h5>
                </g:elseif>
                <g:elseif test="${hotelInstance1?.hotelRating == 'FourStar'}">
                    <h5><small>Rating:</small>&nbsp;<g:each
                            in="${1..4}"
                            var="a"><img
                                src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                    </h5>
                </g:elseif>

                <g:elseif test="${hotelInstance1?.hotelRating == 'FiveStar'}">
                    <h5><small>Rating:</small>&nbsp;<g:each
                            in="${1..5}"
                            var="a"><img
                                src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                    </h5>
                </g:elseif>
                <g:elseif test="${hotelInstance1?.hotelRating == 'SixStar'}">
                    <h5><small>Rating:</small>&nbsp;<g:each
                            in="${1..6}"
                            var="a"><img
                                src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                    </h5>
                </g:elseif>
                <g:elseif test="${hotelInstance1?.hotelRating == 'SevenStar'}">
                    <h5><small>Rating:</small>&nbsp;<g:each
                            in="${1..7}"
                            var="a"><img
                                src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                    </h5>
                </g:elseif>
            </h5>
            </p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-tertiary" data-dismiss="modal">Close</button>
                %{--<button type="button" class="btn btn-primary">Save changes</button>--}%
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>