<div class="col-md-8">
    <table class="table table-bordered">
        <tr>
            <td><strong>Check-In</strong></td>

            <th>${orderDetails?.checkInDate}</th>

            <td><strong>Check-Out</strong></td>
            <th>${orderDetails?.checkOutDate}</th>
        </tr>

        <tr>
            <td><strong>Order No</strong></td>
            <td>${orderDetails?.orderNumber}</td>
            <td><strong>Book Reference No</strong></td>
            <td>${orderDetails?.bookingRefNo}</td>
        </tr>

        <tr>
            <td><strong>Room Category</strong></td>
            <td>${bookingHistory?.roomCategory}</td>
            <td><strong>No. Of Rooms</strong></td>
            <td>${bookingHistory?.noOfRooms}</td>
        </tr>

        <tr>
            <td><strong>Booking Date</strong></td>
            <td>${orderDetails?.bookingDate}</td>
            <td><strong>Hotel Name</strong></td>
            <td>${orderDetails?.hotelName}</td>
        </tr>
        <tr>
            <td><strong>Guest Name</strong></td>
            <td>${orderDetails?.guestName}</td>
            <td><strong>Last Name</strong></td>
            <td>${orderDetails?.guestEmail}</td>
        </tr>
        <tr>
            <td><strong>Guest Contact</strong></td>
            <td>${orderDetails?.guestPhoneNo}</td>
        </tr>
        <tr>
            <td><strong>Booking Cancellation Policy</strong></td>
            <td>${orderDetails?.hotelCancelPolicy}</td>
        </tr>
    </table>
</div>