<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Booking Status-Travelex 360</title>
</head>

<body>
<br>
<br>
<br>
<br>
<br>

<div class="container">
    <div class="row">
        <h4>For more detail you can login</h4>

    </div>

    <div class="row">
        <div class="col-md-12">
            <g:render template="/searchHotel/newThemed/forTBO/pdfForHotelBookingSummary"
                      model="[bookingHistory: bookingHistory, amount: amount, hotelForBooking: hotelForBooking, roomBookedList: roomBookedList]"/>

        </div>
    </div>
</div>
</body>
</html>