<div class="modal-dialog fade modal-sm bs-example-modal-lg${indexNo}">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

            <h3 class="modal-title">Cancellation Policy</h3>
        </div>

        <div class="modal-body">
            <g:if test="${cancelPolicy}">
                <p>

                <strong>${cancelPolicy}</strong></p>
            </g:if>
            <g:else>
                <p>

                    <strong>This booking is not refundable</strong></p>
            </g:else>

        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-tertiary" data-dismiss="modal">Close</button>
        </div>
    </div><!-- /.modal-content -->
</div>