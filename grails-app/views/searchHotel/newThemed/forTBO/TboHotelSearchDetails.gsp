<%@ page import="java.math.RoundingMode" %>
<!DOCTYPE html>
<html lang="en" ng-app>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>.:: Travelex360 ::.</title>
    <link rel="stylesheet" href="../css/jquery-ui.css">
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../css/datepicker3.css" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="../css/jquery.selectBoxIt.css" />

    <!-- Owl Carousel Assets -->
    <link href="../plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="../plugins/owl-carousel/owl.theme.css" rel="stylesheet">

    <link href="../plugins/touchspin/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" media="all">


    <link href="../css/bootstrap-slider.css" rel="stylesheet">

    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/theme.css" rel="stylesheet">
    <link href="../css/media.css" rel="stylesheet">


    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../js/ie-emulation-modes-warning.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>


<body class="theme-orange">
<div class="page-wrapper">
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-528db7470c3800d0" async="async"></script>

    <header class="main-header">
        <div class="container-fluid">
           <g:render template="/navbar/standardNav"/>
        </div>
    </header>

    <div class="listing-page-wrapper">
    <g:render template="/hotelTemplates/hotelModifySearch"/>
    <div class="content-wrapper">
        <div class="container">
            <div class="main-hotel-details-container">
                <div class="hotel-header">
                    <h1 class="inner-title color-default">
                        <strong>${hotelInstance1?.hotelName}</strong>
                        <g:render template="../hotelTemplates/hotelRating"
                                  model="[hotelInstance: hotelInstance1]"/>
                    </h1>

                    <p>${hotelVOInstance?.address}, ${place}.</p>
                </div>

                <div class="row">
                    <div class="col-sm-9">
                        <div class="hotel-detail-box">
                            <h3 class="listing-title">Choose Your Rooms</h3>


                            <g:each in="${hotelInstance1?.hotelRoomDetailVOArrayList}" status="i"
                                    var="hotelRoomInstance">

                                <div class="room-option-container">
                                    <div class="row">
                                        <div class="col-sm-12 room-selection-edit">
                                            <div class="room-option">
                                                <div class="row">
                                                    <div class="col-sm-9">
                                                        <div class="room-details">
                                                            <div style="float: left">
                                                                <img height="150" width="200"
                                                                     style="margin-top: 2%;padding-right: 10px"
                                                                     src="${hotelVOInstance?.hotelPictures[i] ? hotelVOInstance?.hotelPictures[i].src : ""}"/>
                                                            </div>


                                                            <div style="padding-top:10px">
                                                                <g:set var="type"
                                                                       value="${hotelRoomInstance.roomTypeName.split(" ")}"/>


                                                                <h4><strong>${type[0]}&nbsp;${type[1]}</strong></h4>

                                                                <p><small>Includes :
                                                                    <g:each in="${hotelRoomInstance?.amenities}"
                                                                            var="roomFacilities">
                                                                        ${roomFacilities?.name},
                                                                    </g:each></small></p>
                                                                <a href="#"
                                                                   style="text-decoration: none;color:darkblue;font-size:small"
                                                                   onclick="showCancelPolicy('${hotelRoomInstance?.roomIndex}',
                                                                           '${hotelRoomInstance?.ratePlanCOde}', '${hotelRoomInstance?.roomTypeCode}')">
                                                                    <u>Cancellation Policy</u></a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <div class="room-manipulate">
                                                            <div class="form-group">
                                                                <div style="margin-top: 10%">
                                                                    <span style="font-size: medium;margin-left: 10%"><b>Total Price&nbsp;:</b>
                                                                    </span><h4><span class="color-default"
                                                                                     style="margin-left: 10%"><b>${(hotelRoomInstance?.hotelRateVO?.totalRate?.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP)) + (hotelRoomInstance?.hotelRateVO?.agentMarkUp?.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP))}</b>&nbsp;INR
                                                                </span></h4>
                                                                </div>
                                                                <g:link class="btn btn-default"
                                                                        controller="hotelService"
                                                                        action="takeOccupantInfo"
                                                                        id="loginForm"
                                                                        params="[hotelIndex       : hotelIndex, roomId: hotelRoomInstance?.roomIndex,
                                                                                 childInRoomTwo   : childInRoomTwo, childInRoomThree: childInRoomThree,
                                                                                 childInRoomFour  : childInRoomFour, peopleInRoomTwo: peopleInRoomTwo,
                                                                                 peopleInRoomThree: peopleInRoomThree, peopleInRoomFour: peopleInRoomFour,
                                                                                 checkOut         : checkOut, checkIn: checkIn, child: child, adult: adult,
                                                                                 noOfRoom         : noOfRoom]"
                                                                        onclick="setRedirectUrl()"
                                                                        style="text-decoration: none;margin-top: 10%;margin-left: 15%;">Book Now</g:link>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </g:each>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                            <div class="photo-gallery-container">
                                <g:if test="${hotelInstance1?.searchHotelMedias}">
                                    <div id="slider-large" class="owl-carousel">
                                        <div class="item">
                                            <img width="400" height="900"
                                                 src="${hotelVOInstance?.hotelPictures[0]?.src}"/>
                                        </div>

                                        <div class="item">
                                            <img width="300" height="300"
                                                 src="${hotelVOInstance?.hotelPictures[1]?.src}"/>
                                        </div>

                                        <div class="item">
                                            <img width="300" height="300"
                                                 src="${hotelVOInstance?.hotelPictures[2]?.src}"/>
                                        </div>

                                        <div class="item">
                                            <img width="300" height="300"
                                                 src="${hotelVOInstance?.hotelPictures[3] ? hotelVOInstance?.hotelPictures[3].src : ""}"/>
                                        </div>

                                        <div class="item">
                                            <img width="300" height="300"
                                                 src="${hotelVOInstance?.hotelPictures[4] ? hotelVOInstance?.hotelPictures[4].src : ""}"/>
                                        </div>

                                        <div class="item">
                                            <img width="300" height="300"
                                                 src="${hotelVOInstance?.hotelPictures[5] ? hotelVOInstance?.hotelPictures[5].src : ""}"/>
                                        </div>

                                        <div class="item">

                                            <img width="300" height="300"
                                                 src="${hotelVOInstance?.hotelPictures[6] ? hotelVOInstance?.hotelPictures[6].src : ""}"/>
                                        </div>

                                        <div class="item">
                                            <img width="300" height="300"
                                                 src="${hotelVOInstance?.hotelPictures[7] ? hotelVOInstance?.hotelPictures[7].src : ""}"/>
                                        </div>

                                        <div class="item">
                                            <img width="300" height="300"
                                                 src="${hotelVOInstance?.hotelPictures[8] ? hotelVOInstance?.hotelPictures[8].src : ""}"/>
                                        </div>
                                    </div>

                                    <div class="slider-thumb-container">
                                        <div id="slider-thumb" class="owl-carousel">
                                            <div class="item">
                                                <img width="50" height="55"
                                                     src="${hotelVOInstance?.hotelPictures[0]?.src}"/>
                                            </div>

                                            <div class="item">
                                                <img width="50" height="55"
                                                     src="${hotelVOInstance?.hotelPictures[1]?.src}"/>
                                            </div>

                                            <div class="item">
                                                <img width="50" height="55"
                                                     src="${hotelVOInstance?.hotelPictures[2]?.src}"/>
                                            </div>

                                            <div class="item">
                                                <img width="50" height="55"
                                                     src="${hotelVOInstance?.hotelPictures[3] ? hotelVOInstance?.hotelPictures[3].src : ""}"/>
                                            </div>

                                            <div class="item">
                                                <img width="50" height="55"
                                                     src="${hotelVOInstance?.hotelPictures[4] ? hotelVOInstance?.hotelPictures[4].src : ""}"/>
                                            </div>

                                            <div class="item">
                                                <img width="50" height="55"
                                                     src="${hotelVOInstance?.hotelPictures[5] ? hotelVOInstance?.hotelPictures[5].src : ""}"/>
                                            </div>

                                            <div class="item">

                                                <img width="50" height="55"
                                                     src="${hotelVOInstance?.hotelPictures[6] ? hotelVOInstance?.hotelPictures[6].src : ""}"/>
                                            </div>

                                            <div class="item">
                                                <img width="50" height="55"
                                                     src="${hotelVOInstance?.hotelPictures[7] ? hotelVOInstance?.hotelPictures[7].src : ""}"/>
                                            </div>

                                            <div class="item">
                                                <img width="50" height="55"
                                                     src="${hotelVOInstance?.hotelPictures[8] ? hotelVOInstance?.hotelPictures[8].src : ""}"/>
                                            </div>
                                        </div>

                                        <div class="slider-thumb-nav-container">
                                            <div class="slider-thumb-nav thumb-next"><i
                                                    class="glyphicon glyphicon-chevron-right"></i></div>

                                            <div class="slider-thumb-nav thumb-prev"><i
                                                    class="glyphicon glyphicon-chevron-left"></i></div>
                                        </div>
                                    </div>
                                    </div>
                                </g:if>
                                <g:else>
                                    <img alt="image" class="img-responsive"
                                         src="http://images.prd.mris.com/image/V2/1/Yu59d899Ocpyr_RnF0-8qNJX1oYibjwp9TiLy-bZvU9vRJ2iC1zSQgFwW-fTCs6tVkKrj99s7FFm5Ygwl88xIA.jpg">
                                </g:else>

                            </div>

                            <div class="col-md-6">
                                <h4>Hotel Overview</h4>
                                <g:set var="descriptionLength"
                                       value="${hotelVOInstance?.description?.length() / 4}"/>

                                <div style="height: 300px;overflow:auto">
                                    ${hotelVOInstance?.description}
                                </div>

                            </div>
                        </div>

                        <div class="hotel-information-container">
                            <h3>Hotel Information</h3>
                            <table class="table hotel-info-table">
                                <tbody>
                                <tr>
                                    <td>
                                        <h4>Amenities</h4>
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            <li>Parking Facilities Available</li>
                                            <li>Lift / Elevator</li>
                                            <li>Front Desk</li>
                                            <li>Laundry Service Available</li>
                                            <li>Internet Access - Surcharge</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h4>Rules</h4>
                                    </td>
                                    <td>
                                        <p>Check In: <strong>From 12:00 hours</strong></p>

                                        <p>Check Out: <strong>Until 12:00 hours</strong></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h4>Hotel Policy</h4>
                                    </td>
                                    <td>
                                        <p>The standard check-in time is 12:00 PM and the standard check-out time is 12:00 PM. Early check-in or late check-out is strictly subjected to availability and may be chargeable by the hotel. Any early check-in or late check-out request must be directed and reconfirmed with hotel directly</p>

                                        <p>Terms and Conditions:</p>

                                        <p>It is mandatory for guests to present valid photo identification at the time of check-in.</p>
                                    </td>
                                <tr>
                                    <td>
                                        <h4>Terms and Conditions</h4>
                                    </td>
                                    <td>
                                        <p>It is mandatory for guests to present valid photo identification at the time of check-in.</p>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>

                    </div>

                    <div class="col-sm-3">
                        <div class="hotel-detail-box">
                            <h4 class="listing-title">Share</h4>

                            <div class="facilities-container">
                                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                                <div class="addthis_sharing_toolbox"></div>
                            </div>
                        </div>

                        <div class="hotel-detail-box">

                            <h4 class="listing-title">Facilities</h4>

                            <div class="facilities-container">
                                <ul class="facilities-list">
                                    <li><span><img src="../images/faci-wifi.png" alt="Internet / Wifi"
                                                   title="Internet /Wifi"></span></li>
                                    <li><span><img src="../images/faci-ac.png" alt="Air Condtioned"
                                                   title="Air Conditioned"></span></li>
                                    <li><span><img src="../images/faci-room.png" alt="Room Service"
                                                   title="Room Service"></span></li>
                                    <li><span><img src="../images/faci-rest.png" alt="Restaurent"
                                                   title="Restaurent"></span></li>
                                </ul>
                            </div>
                        </div>

                        <div class="hotel-detail-box">
                            <h4 class="listing-title">Location</h4>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7831.523146952806!2d77.0703710848363!3d11.056494236283742!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x76e6375967faeedc!2sClarion+Hotel!5e0!3m2!1sen!2sin!4v1424244728876"
                                    width="100%" height="200" frameborder="0" style="border:0"
                                    scrollwheel="false"></iframe>
                        </div>

                        <div class="hotel-detail-box">
                            <h4 class="listing-title">Landmarks Nearby</h4>

                            <div class="">
                                <ul class="detail-list">
                                    <li>Anamalai Wildlife Sanctuary - 65Km</li>
                                    <li>Coimbatore Airport - 4.0Km</li>
                                    <li>Railway Station - 15.0Km</li>
                                    <li>City centre - 15.0Km</li>
                                    <li>Perur Patteeswarar Temple - 10Km</li>
                                    <li>Pollachi - 44.8Km</li>
                                    <li>Thirumoorthy Temple - 93.3Km</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>

</div>

<footer class="main-footer inner">
    <div class="container-fluid">
        <div class="footer-links">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <ul class="inline-list-links">
                        <li><a href="about.php">About Us</a></li>
                        <li><a href="how_it_works.php">How It Works</a></li>
                        <li><a href="terms.php">Terms Of Use</a></li>
                        <li><a href="contact.php">Contact Us</a></li>
                    </ul>
                </div>

                <div class="col-md-6 col-sm-6 text-right">
                    Copyright &copy; Travelex 360 Pvt.Ltd. 2014, All rights reserved
                </div>
            </div>
        </div>

        <div class="footer-rights">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <ul class="inline-list-links margin-top-5">
                        <li><a href="#">Post Your Complaint</a></li>
                        <li><a href="policy.php">Privacy Policy</a></li>
                        <li><a href="#">FAQ's</a></li>
                        <li><a href="#">Site Map</a></li>
                    </ul>
                </div>

                <div class="col-md-6 col-sm-6 text-right">
                    <img src="../images/payment-methods.jpg">
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- // <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> -->

<!-- // <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->
<script src="../js/script.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28/angular.js"></script>
<script src="../js/ang_main.js"></script>

<script src="../js/ie10-viewport-bug-workaround.js"></script>
<script src="../js/jquery-ui.min.js"></script>
<script src="../js/datepicker.js"></script>
<script src="../js/jquery.selectBoxIt.min.js"></script>
<script src="../plugins/owl-carousel/owl.carousel.js"></script>
<script src="../plugins/touchspin/jquery.bootstrap-touchspin.js"></script>

<script type="text/javascript" src="../js/packages-map.js"></script>
<script src="../js/bootstrap-slider.js"></script>
<script src="../js/jquery.barrating.js"></script>
<script src="../js/jquery.raty.js"></script>
<script src="../js/jquery.validate.js"></script>
<script src="../js/main.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28/angular.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".countspin-roomtype1").TouchSpin({
            min: 0,
            initval: '1',
            max: 14,
            buttondown_class: 'btn btn-default',
            buttonup_class: 'btn btn-default',
            step: 1
        });
        $(".countspin-roomtype2").TouchSpin({
            min: 0,
            initval: '1',
            max: 5,
            buttondown_class: 'btn btn-default',
            buttonup_class: 'btn btn-default',
            step: 1
        });
        $('#example-f').barrating({
            showSelectedRating: false,
            readonly: true
        });
        $.fn.raty.defaults.path = 'images';
        $('.star-rating.readonly').raty({
            readOnly: true,
            score: function () {
                return $(this).attr('data-score');
            }
        });
        $('.star-rating-overall').raty({
            score: function () {
                return $(this).attr('data-score');
            },
            target: '.rating-target1',
            targetKeep: true,
            targetText: 'Click to Rate'
        });
        $('.star-rating-service').raty({
            score: function () {
                return $(this).attr('data-score');
            },
            target: '.rating-target2',
            targetKeep: true,
            targetText: 'Click to Rate'
        });
        $('.star-rating-quality').raty({
            score: function () {
                return $(this).attr('data-score');
            },
            target: '.rating-target3',
            targetKeep: true,
            targetText: 'Click to Rate'
        });
        $('.star-rating-clean').raty({
            score: function () {
                return $(this).attr('data-score');
            },
            target: '.rating-target4',
            targetKeep: true,
            targetText: 'Click to Rate'
        });
    });

</script>
</body>
</html>