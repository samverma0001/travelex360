<!DOCTYPE html>
<html lang="en" ng-app>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>.:: Travelex360 ::.</title>

    <link href="${resource(dir: 'css',file: 'bootstrap.css')}" rel="stylesheet">
    <link href="${resource(dir: 'css',file: 'bootstrap.css.map')}" rel="stylesheet">
    <link href="${resource(dir: 'css',file: 'datepicker3.css')}" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="${resource(dir: 'css',file: 'jquery.selectBoxIt.css')}" />

    <!-- Owl Carousel Assets -->
    <link href="${resource(dir: 'css',file: 'owl.carousel.css')}" rel="stylesheet">
    <link href="${resource(dir: 'css',file: 'owl.theme.css')}" rel="stylesheet">

    <link href="${resource(dir: 'css',file: 'jquery.bootstrap-touchspin.css')}" rel="stylesheet" type="text/css" media="all">


    <link href="${resource(dir: 'css',file: 'bootstrap-slider.css')}" rel="stylesheet">

    <link href="${resource(dir: 'css',file: 'style.css')}" rel="stylesheet">
    <link href="${resource(dir: 'css',file: 'theme.css')}" rel="stylesheet">
    <link href="${resource(dir: 'css',file: 'media.css')}" rel="stylesheet">


    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <script src="${resource(dir: 'js', file: 'ie8-responsive-file-warning.js')}"></script>

    <script src="${resource(dir: 'js', file: 'ie-emulation-modes-warning.js')}"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="theme-orange">
<div class="page-wrapper">
<header class="main-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-sm-7 col-xs-6">
                <div class="logo pull-left">
                    <a href="index.php">
                        <img src="../images/logo.png" width="190px">
                    </a>
                </div>
                <div class="main-icons-container-header pull-left">
                    <a href="plan_travel.php" class="main-icon-box">
                        <img src="../images/ico1.png">
                        <h3>PLAN TRAVEL</h3>
                    </a>
                    <a href="book_hotel.php" class="main-icon-box">
                        <img src="../images/ico2.png">
                        <h3>BOOK HOTEL</h3>
                    </a>
                    <a href="activity.php" class="main-icon-box">
                        <img src="../images/ico3.png">
                        <h3>ACTIVITY</h3>
                    </a>
                    <a href="flight_booking.php" class="main-icon-box">
                        <img src="../images/ico4.png">
                        <h3>FLIGHT BOOKING</h3>
                    </a>
                    <a href="service-provider.php" class="main-icon-box">
                        <img src="../images/ico5.png">
                        <h3>SERVICE PROVIDER</h3>
                    </a>
                    <a href="packages.php" class="main-icon-box">
                        <img src="../images/ico6.png">
                        <h3>PACKAGES</h3>
                    </a>
                </div>
            </div>
            <!-- <div class="col-md-7 col-sm-8 col-xs-7"> -->
            <!-- </div> -->
            <div class="col-md-2 col-sm-3 col-xs-6 text-right">
                <div class="header-right">
                    <ul class="header-icon-nav">
                        <li><a href="register.php"><img src="../images/register-icon.png"></a></li>
                        <li><a href="login.php"><img src="../images/login-icon.png"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>


<div class="listing-page-wrapper">
<div class="min-search-container">
<div class="container-fluid">
<div class="horiz-form-container">
<form class="tvx-from tvx-horiz-form" data-ng-controller="book_hotel_control">
<div class="row">
<div class="col-md-4">
    <div class="form-group">
        <label for="">Going to</label>
        <input type="text" class="form-control" id="" placeholder="Select City, Area or Hotel...">
    </div>
</div>
<div class="col-md-2">
    <div class="form-group">
        <label for="">Check in</label>
        <div class="input-group date">
            <input type="text" class="form-control">
            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
        </div>
    </div>
</div>
<div class="col-md-2">
    <div class="form-group">
        <label for="">Check out</label>
        <div class="input-group date">
            <input type="text" class="form-control">
            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
        </div>
    </div>
</div>
<div class="col-md-2">
<div class="form-group">
<label for="">Guests</label>
<div class="room-select-toggle">
    {{no_of_rooms}} Room / {{TotalGuest()}} Guests <span class="glyphicon glyphicon-chevron-down"></span>
</div>
<div class="room-select-dropdown-container">
<div class="dropdown-rooms-container">
<div class="form-group">
    <label for="">Number of Rooms</label>
    <div class="input-group col-md-12">
        <select id="select_room_count" ng-model="no_of_rooms" ng-init="no_of_rooms='1'" class="form-control">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
        </select>
    </div>
</div>
<div class="rooms-container">
<div class="form-group room-container room1">
    <h4>Rooms1</h4>
    <div class="row">
        <div class="col-md-10">
            <div class="row">
                <div class="col-xs-6">
                    <label for="">Adults</label>
                    <div class="input-group col-md-12">
                        <select class="form-control" ng-model="room1_adult" ng-init="room1_adult='0'" >
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-6">
                    <label for="">Childrens</label>
                    <div class="input-group col-md-12">
                        <select class="form-control children_count" ng-model="room1_child" ng-init="room1_child='0'" >
                            <option>0</option>
                            <option>1</option>
                            <option>2</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-6 child1">
                    <label for="">Child 1 Age</label>
                    <div class="input-group col-md-12">
                        <select class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                            <option>11</option>
                            <option>12</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-6 child2">
                    <label for="">Child 2 Age</label>
                    <div class="input-group col-md-12">
                        <select class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                            <option>11</option>
                            <option>12</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="form-group room-container room2">
    <h4>Rooms2</h4>
    <div class="row">
        <div class="col-md-10">
            <div class="row">
                <div class="col-xs-6">
                    <label for="">Adults</label>
                    <div class="input-group col-md-12">
                        <select class="form-control" ng-model="room2_adult" ng-init="room2_adult='0'" >
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-6">
                    <label for="">Childrens</label>
                    <div class="input-group col-md-12">
                        <select class="form-control children_count" ng-model="room2_child" ng-init="room2_child='0'" >
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-6 child1">
                    <label for="">Child 1 Age</label>
                    <div class="input-group col-md-12">
                        <select class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                            <option>11</option>
                            <option>12</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-6 child2">
                    <label for="">Child 2 Age</label>
                    <div class="input-group col-md-12">
                        <select class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                            <option>11</option>
                            <option>12</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="form-group room-container room3">
    <h4>Rooms3</h4>
    <div class="row">
        <div class="col-md-10">
            <div class="row">
                <div class="col-xs-6">
                    <label for="">Adults</label>
                    <div class="input-group col-md-12">
                        <select class="form-control" ng-model="room3_adult" ng-init="room3_adult='0'" >
                            <option value="0">0</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-6">
                    <label for="">Childrens</label>
                    <div class="input-group col-md-12">
                        <select class="form-control children_count" ng-model="room3_child" ng-init="room3_child='0'" >
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-6 child1">
                    <label for="">Child 1 Age</label>
                    <div class="input-group col-md-12">
                        <select class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                            <option>11</option>
                            <option>12</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-6 child2">
                    <label for="">Child 2 Age</label>
                    <div class="input-group col-md-12">
                        <select class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                            <option>11</option>
                            <option>12</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="form-group room-container room4">
    <h4>Rooms4</h4>
    <div class="row">
        <div class="col-md-10">
            <div class="row">
                <div class="col-xs-6">
                    <label for="">Adults</label>
                    <div class="input-group col-md-12">
                        <select class="form-control" ng-model="room4_adult" ng-init="room4_adult='0'" >
                            <option value="0">0</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-6">
                    <label for="">Childrens</label>
                    <div class="input-group col-md-12">
                        <select class="form-control children_count" ng-model="room4_child" ng-init="room4_child='0'" >
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-6 child1">
                    <label for="">Child 1 Age</label>
                    <div class="input-group col-md-12">
                        <select class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                            <option>11</option>
                            <option>12</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-6 child2">
                    <label for="">Child 2 Age</label>
                    <div class="input-group col-md-12">
                        <select class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                            <option>11</option>
                            <option>12</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="form-group room-container room5">
    <h4>Rooms5</h4>
    <div class="row">
        <div class="col-md-10">
            <div class="row">
                <div class="col-xs-6">
                    <label for="">Adults</label>
                    <div class="input-group col-md-12">
                        <select class="form-control" ng-model="room5_adult" ng-init="room5_adult='0'" >
                            <option value="0">0</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-6">
                    <label for="">Childrens</label>
                    <div class="input-group col-md-12">
                        <select class="form-control children_count" ng-model="room5_child" ng-init="room5_child='0'" >
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-6 child1">
                    <label for="">Child 1 Age</label>
                    <div class="input-group col-md-12">
                        <select class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                            <option>11</option>
                            <option>12</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-6 child2">
                    <label for="">Child 2 Age</label>
                    <div class="input-group col-md-12">
                        <select class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                            <option>11</option>
                            <option>12</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-2">
    <button type="submit" class="btn img-full btn-default">Search Hotel</button>
</div>
</div>
</form>
</div>
</div>
</div>
<div class="content-wrapper">
<div class="container">
<div class="row">
<div class="col-md-3">
    <div class="filter-sidebar-container">
        <div class="filter-sidebar">

            <h3>Filter Your Search
                <a href="#" class="btn btn-default btn-small pull-right"><i class="glyphicon glyphicon-refresh"></i></a>
            </h3>

            <div class="filter-box">
                <div class="filter-box-header">
                    <h4>Find by Hotel Name</h4>
                </div>
                <div class="filter-box-content">
                    <input type="text" class="form-control" placeholder="Search Hotel...">
                </div>
            </div>
            <div class="filter-box">
                <div class="filter-box-header">
                    <h4>Rating</h4>
                </div>
                <div class="filter-box-content">
                    <div class="rating-buttons">
                        <div class="btn-group" data-toggle="buttons">
                            <!-- <label class="btn btn-white active">
                          <input type="checkbox"  autocomplete="off" checked> All
                          <br> <small class="text-muted">(700)</small>
                        </label> -->
                            <label class="btn btn-white">
                                <input type="checkbox" autocomplete="off"> 1 <i class="glyphicon glyphicon-star"></i> <br> <small class="text-muted">(700)</small>
                            </label>
                            <label class="btn btn-white">
                                <input type="checkbox" autocomplete="off"> 2 <i class="glyphicon glyphicon-star"></i> <br> <small class="text-muted">(700)</small>
                            </label>
                            <label class="btn btn-white">
                                <input type="checkbox" autocomplete="off"> 3 <i class="glyphicon glyphicon-star"></i> <br> <small class="text-muted">(700)</small>
                            </label>
                            <label class="btn btn-white">
                                <input type="checkbox"autocomplete="off"> 4 <i class="glyphicon glyphicon-star"></i> <br> <small class="text-muted">(700)</small>
                            </label>
                            <label class="btn btn-white">
                                <input type="checkbox"  autocomplete="off"> 5 <i class="glyphicon glyphicon-star"></i> <br> <small class="text-muted">(700)</small>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="filter-box">
                <div class="filter-box-header">
                    <h4>Budget Range <small>(per day)</small></h4>
                </div>
                <div class="filter-box-content">
                    <div class="filter-list-box">
                        <div class="filter-list">
                            <label> <input type="checkbox"> Less than Rs.5,000 </label>
                        </div>
                        <div class="filter-list">
                            <label> <input type="checkbox"> Rs.5,001 - Rs.10,000 </label>
                        </div>
                        <div class="filter-list">
                            <label> <input type="checkbox"> Rs.10,001 - Rs.15,000 </label>
                        </div>
                        <div class="filter-list">
                            <label> <input type="checkbox"> Rs.15,001 - Rs.20,000 </label>
                        </div>
                        <div class="filter-list">
                            <label> <input type="checkbox"> More than Rs.25,000 </label>
                        </div>
                    </div>
                    <h5>Custom Price Range</h5>
                    <input id="ex2" type="text" class="" value="" data-slider-min="1" data-slider-max="25000" data-slider-step="5" data-slider-value="[0,4000]"/> <br>
                    <h5>
                        <span>INR 1</span>
                        <span class="pull-right">INR 25,000</span>
                    </h5>
                </div>
            </div>

            <div class="filter-box">
                <div class="filter-box-header">
                    <h4>Hotel Location <small></small></h4>
                </div>
                <div class="filter-box-content">
                    <div class="filter-list-box">
                        <div class="filter-list">
                            <a href="#">Bardez <small class="text-muted">(5)</small> </a>
                        </div>
                        <div class="filter-list">
                            <a href="#">Calangute Beach <small class="text-muted">(15)</small> </a>
                        </div>
                        <div class="filter-list">
                            <a href="#">Calangute <small class="text-muted">(12)</small> </a>
                        </div>
                        <div class="filter-list">
                            <a href="#">South Goa <small class="text-muted">(45)</small> </a>
                        </div>
                        <div class="filter-list">
                            <a href="#">Panjim <small class="text-muted">(45)</small> </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="filter-box">
                <div class="filter-box-header">
                    <h4>Hotel Facilities</h4>
                </div>
                <div class="filter-box-content">
                    <div class="filter-list-box">
                        <div class="filter-list with-icon">
                            <label> <input type="checkbox"> Internet / Wi-Fi </label>
                            <span><img src="../images/faci-wifi.png"></span>
                        </div>
                        <div class="filter-list with-icon">
                            <label> <input type="checkbox"> Swimming Pool </label>
                            <span><img src="../images/faci-pool.png"></span>
                        </div>
                        <div class="filter-list with-icon">
                            <label> <input type="checkbox"> Integerated Gym </label>
                            <span><img src="../images/faci-gym.png"></span>
                        </div>
                        <div class="filter-list with-icon">
                            <label> <input type="checkbox"> Air Conditioned</label>
                            <span><img src="../images/faci-ac.png"></span>
                        </div>
                        <div class="filter-list with-icon">
                            <label> <input type="checkbox"> Restaurent Facility </label>
                            <span><img src="../images/faci-rest.png"></span>
                        </div>
                        <div class="filter-list with-icon">
                            <label> <input type="checkbox"> Room Service </label>
                            <span><img src="../images/faci-room.png"></span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="col-md-9">
<h1 class="main-title">10450 Hotels Available</h1>
<div class="result-category-container">
    <a href="#" class="result-cat-box bg-brown active">
        <div class="result-category-title"> All Hotels </div>
        <div class="result-category-count"> 10450 </div>
    </a>
    <a href="#" class="result-cat-box bg-green">
        <div class="result-category-title"> Budget Hotels </div>
        <div class="result-category-count"> 500 </div>
    </a>
    <a href="#" class="result-cat-box bg-voilet">
        <div class="result-category-title"> Business Hotels </div>
        <div class="result-category-count"> 1000 </div>
    </a>
    <a href="#" class="result-cat-box bg-pink">
        <div class="result-category-title"> Luxury Hotels </div>
        <div class="result-category-count"> 12 </div>
    </a>
    <a href="#" class="result-cat-box bg-dark">
        <div class="result-category-title"> Best Deals </div>
        <div class="result-category-count"> 50 </div>
    </a>
</div>
<div class="listing-content-container">
<div class="listing-sort-container">
    SORT BY : <a href="#" class="active">Most Popular</a> <a href="#">Lowest Price</a> <a href="#">Top Rated</a>
</div>
<div class="listing-content-outer">
<div class="listing-box">
    <div class="row">
        <div class="col-md-3">
            <div class="listing-image">
                <a href="book-hotel-main.php">
                    <img src="../images/listing-image.jpg" class="img-full">
                </a>
            </div>
        </div>
        <div class="col-md-9">
            <div class="listing-content">
                <div class="row">
                    <div class="col-md-9">
                        <h3 class="listing-title">
                            <a href="book-hotel-main.php">
                                Fortune Select Regina
                            </a>
                            <span class="star-rating readonly" data-score="4"></span>
                        </h3>
                        <h5>376, Off Fort Aguada Road, Candolim, Bardez, Goa - 403515.</h5>
                        <h4>Facilities</h4>
                        <div class="facilities-container">
                            <ul class="facilities-list">
                                <li><span><img src="../images/faci-wifi.png" alt="Internet / Wifi" title="Internet /Wifi"></span></li>
                                <li><span><img src="../images/faci-ac.png" alt="Air Conditioned" title="Air Conditioned"></span></li>
                                <li><span><img src="../images/faci-room.png" alt="Room Service" title="Room Service"></span></li>
                                <li><span><img src="../images/faci-pool.png" alt="Swimming Pool" title="Swimming Pool"></span></li>
                                <li><span><img src="../images/faci-rest.png" alt="Restaurent" title="Restaurent"></span></li>
                                <li><span><img src="../images/faci-gym.png" alt="Gym" title="Gym"></span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="price-box">
                            <h3 class="price">
                                <span class="old">Rs.8,999</span>
                                <span class="new color-default">Rs.6,999</span>
                                <p><small>per night(1 Room)</small></p>
                                <a href="book-hotel-main.php" class="btn btn-default">Select</a>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="listing-box">
    <div class="row">
        <div class="col-md-3">
            <div class="listing-image">
                <a href="book-hotel-main.php">
                    <img src="../images/listing-image.jpg" class="img-full">
                </a>
            </div>
        </div>
        <div class="col-md-9">
            <div class="listing-content">
                <div class="row">
                    <div class="col-md-9">
                        <a href="book-hotel-main.php">
                            <h3 class="listing-title">
                                <a href="book-hotel-main.php">
                                    Fortune Select Regina
                                </a>
                                <span class="star-rating readonly" data-score="4"></span>
                            </h3>
                        </a>
                        <h5>376, Off Fort Aguada Road, Candolim, Bardez, Goa - 403515.</h5>
                        <h4>Facilities</h4>
                        <div class="facilities-container">
                            <ul class="facilities-list">
                                <li><span><img src="../images/faci-wifi.png" alt="Internet / Wifi" title="Internet /Wifi"></span></li>
                                <li><span><img src="../images/faci-ac.png" alt="Air Conditioned" title="Air Conditioned"></span></li>
                                <li><span><img src="../images/faci-room.png" alt="Room Service" title="Room Service"></span></li>
                                <li><span><img src="../images/faci-pool.png" alt="Swimming Pool" title="Swimming Pool"></span></li>
                                <li><span><img src="../images/faci-rest.png" alt="Restaurent" title="Restaurent"></span></li>
                                <li><span><img src="../images/faci-gym.png" alt="Gym" title="Gym"></span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="price-box">
                            <h3 class="price">
                                <span class="old">Rs.8,999</span>
                                <span class="new color-default">Rs.6,999</span>
                                <p><small>per night(1 Room)</small></p>
                                <a href="book-hotel-main.php" class="btn btn-default">Select</a>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="listing-box">
    <div class="row">
        <div class="col-md-3">
            <div class="listing-image">
                <a href="book-hotel-main.php">
                    <img src="../images/listing-image.jpg" class="img-full">
                </a>
            </div>
        </div>
        <div class="col-md-9">
            <div class="listing-content">
                <div class="row">
                    <div class="col-md-9">
                        <a href="book-hotel-main.php">
                            <h3 class="listing-title">
                                <a href="book-hotel-main.php">
                                    Fortune Select Regina
                                </a>
                                <span class="star-rating readonly" data-score="4"></span>
                            </h3>
                        </a>
                        <h5>376, Off Fort Aguada Road, Candolim, Bardez, Goa - 403515.</h5>
                        <h4>Facilities</h4>
                        <div class="facilities-container">
                            <ul class="facilities-list">
                                <li><span><img src="../images/faci-wifi.png" alt="Internet / Wifi" title="Internet /Wifi"></span></li>
                                <li><span><img src="../images/faci-ac.png" alt="Air Conditioned" title="Air Conditioned"></span></li>
                                <li><span><img src="../images/faci-room.png" alt="Room Service" title="Room Service"></span></li>
                                <li><span><img src="../images/faci-pool.png" alt="Swimming Pool" title="Swimming Pool"></span></li>
                                <li><span><img src="../images/faci-rest.png" alt="Restaurent" title="Restaurent"></span></li>
                                <li><span><img src="../images/faci-gym.png" alt="Gym" title="Gym"></span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="price-box">
                            <h3 class="price">
                                <span class="old">Rs.8,999</span>
                                <span class="new color-default">Rs.6,999</span>
                                <p><small>per night(1 Room)</small></p>
                                <a href="book-hotel-main.php" class="btn btn-default">Select</a>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="listing-box">
    <div class="row">
        <div class="col-md-3">
            <div class="listing-image">
                <a href="book-hotel-main.php">
                    <img src="../images/listing-image.jpg" class="img-full">
                </a>
            </div>
        </div>
        <div class="col-md-9">
            <div class="listing-content">
                <div class="row">
                    <div class="col-md-9">
                        <a href="book-hotel-main.php">
                            <h3 class="listing-title">
                                <a href="book-hotel-main.php">
                                    Fortune Select Regina
                                </a>
                                <span class="star-rating readonly" data-score="4"></span>
                            </h3>
                        </a>
                        <h5>376, Off Fort Aguada Road, Candolim, Bardez, Goa - 403515.</h5>
                        <h4>Facilities</h4>
                        <div class="facilities-container">
                            <ul class="facilities-list">
                                <li><span><img src="../images/faci-wifi.png" alt="Internet / Wifi" title="Internet /Wifi"></span></li>
                                <li><span><img src="../images/faci-ac.png" alt="Air Conditioned" title="Air Conditioned"></span></li>
                                <li><span><img src="../images/faci-room.png" alt="Room Service" title="Room Service"></span></li>
                                <li><span><img src="../images/faci-pool.png" alt="Swimming Pool" title="Swimming Pool"></span></li>
                                <li><span><img src="../images/faci-rest.png" alt="Restaurent" title="Restaurent"></span></li>
                                <li><span><img src="../images/faci-gym.png" alt="Gym" title="Gym"></span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="price-box">
                            <h3 class="price">
                                <span class="old">Rs.8,999</span>
                                <span class="new color-default">Rs.6,999</span>
                                <p><small>per night(1 Room)</small></p>
                                <a href="book-hotel-main.php" class="btn btn-default">Select</a>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="listing-box">
    <div class="row">
        <div class="col-md-3">
            <div class="listing-image">
                <a href="book-hotel-main.php">
                    <img src="../images/listing-image.jpg" class="img-full">
                </a>
            </div>
        </div>
        <div class="col-md-9">
            <div class="listing-content">
                <div class="row">
                    <div class="col-md-9">
                        <a href="book-hotel-main.php">
                            <h3 class="listing-title">
                                <a href="book-hotel-main.php">
                                    Fortune Select Regina
                                </a>
                                <span class="star-rating readonly" data-score="4"></span>
                            </h3>
                        </a>
                        <h5>376, Off Fort Aguada Road, Candolim, Bardez, Goa - 403515.</h5>
                        <h4>Facilities</h4>
                        <div class="facilities-container">
                            <ul class="facilities-list">
                                <li><span><img src="../images/faci-wifi.png" alt="Internet / Wifi" title="Internet /Wifi"></span></li>
                                <li><span><img src="../images/faci-ac.png" alt="Air Conditioned" title="Air Conditioned"></span></li>
                                <li><span><img src="../images/faci-room.png" alt="Room Service" title="Room Service"></span></li>
                                <li><span><img src="../images/faci-pool.png" alt="Swimming Pool" title="Swimming Pool"></span></li>
                                <li><span><img src="../images/faci-rest.png" alt="Restaurent" title="Restaurent"></span></li>
                                <li><span><img src="../images/faci-gym.png" alt="Gym" title="Gym"></span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="price-box">
                            <h3 class="price">
                                <span class="old">Rs.8,999</span>
                                <span class="new color-default">Rs.6,999</span>
                                <p><small>per night(1 Room)</small></p>
                                <a href="book-hotel-main.php" class="btn btn-default">Select</a>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




</div>
</div>
</div>
</div>
</div>

</div>
</div>



</div>

<footer class="main-footer inner">
    <div class="container-fluid">
        <div class="footer-links">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <ul class="inline-list-links">
                        <li><a href="about.php">About Us</a></li>
                        <li><a href="how_it_works.php">How It Works</a></li>
                        <li><a href="terms.php">Terms Of Use</a></li>
                        <li><a href="contact.php">Contact Us</a></li>
                    </ul>
                </div>
                <div class="col-md-6 col-sm-6 text-right">
                    Copyright &copy; Travelex 360 Pvt.Ltd. 2014, All rights reserved
                </div>
            </div>
        </div>
        <div class="footer-rights">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <ul class="inline-list-links margin-top-5">
                        <li><a href="#">Post Your Complaint</a></li>
                        <li><a href="policy.php">Privacy Policy</a></li>
                        <li><a href="#">FAQ's</a></li>
                        <li><a href="#">Site Map</a></li>
                    </ul>
                </div>
                <div class="col-md-6 col-sm-6 text-right">
                    <img src="../images/payment-methods.jpg">
                </div>
            </div>
        </div>
    </div>
</footer>


<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- // <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> -->
<script src="${resource(dir: 'js', file: 'script.min.js')}"></script>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28/angular.js"></script>
<script src="${resource(dir: 'js', file: 'ang_main.js')}"></script>

<script src="${resource(dir: 'js', file: 'ie10-viewport-bug-workaround.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery-ui.min.js')}"></script>
<script src="${resource(dir: 'js', file: 'datepicker.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery.selectBoxIt.min.js')}"></script>
<script src="${resource(dir: 'js', file: 'owl.carousel.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery.bootstrap-touchspin.js')}"></script>

<script type="text/javascript" src="${resource(dir: 'js', file: 'packages-map.js')}"></script>
<script src="${resource(dir: 'js', file: 'bootstrap-slider.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery.barrating.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery.raty.js')}"></script>
<script src="${resource(dir: 'js', file: 'main.js')}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".countspin-roomtype1").TouchSpin({
            min: 0,
            initval: '1',
            max: 14,
            buttondown_class: 'btn btn-default',
            buttonup_class: 'btn btn-default',
            step: 1
        });
        $(".countspin-roomtype2").TouchSpin({
            min: 0,
            initval: '1',
            max: 5,
            buttondown_class: 'btn btn-default',
            buttonup_class: 'btn btn-default',
            step: 1
        });
        $('#example-f').barrating({
            showSelectedRating:false,
            readonly:true
        });
        $.fn.raty.defaults.path = 'images';
        $('.star-rating.readonly').raty({
            readOnly: true,
            score: function() {
                return $(this).attr('data-score');
            }
        });
        $('.star-rating-overall').raty({
            score: function() {
                return $(this).attr('data-score');
            },
            target : '.rating-target1',
            targetKeep : true,
            targetText : 'Click to Rate'
        });
        $('.star-rating-service').raty({
            score: function() {
                return $(this).attr('data-score');
            },
            target : '.rating-target2',
            targetKeep : true,
            targetText : 'Click to Rate'
        });
        $('.star-rating-quality').raty({
            score: function() {
                return $(this).attr('data-score');
            },
            target : '.rating-target3',
            targetKeep : true,
            targetText : 'Click to Rate'
        });
        $('.star-rating-clean').raty({
            score: function() {
                return $(this).attr('data-score');
            },
            target : '.rating-target4',
            targetKeep : true,
            targetText : 'Click to Rate'
        });
    });
</script>
</body>
</html>
