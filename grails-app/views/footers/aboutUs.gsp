<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="headerFooterLayout"/>
</head>

<body>
<div class="content-page page-wrapper full-height">
    <div class="inner-page-container">
        <!-- <div class="middle-container"> -->
        <div class="container-fluid">
            <div class="main-content-container">
                <div class="main-icons-container-vertical">
                    <g:render template="/appTemplate/menuTemplate"/>
                </div>

                <div class="main-content-container-content">
                    <h1>About Travelex 360</h1>

                    <p>Travel has increased multifold in the contemporary life. Whether such travel is for corporate events, family vacations, or private retreats, there has been an unmatched growth of the tourism industry in India. However, amidst this growth, there has been an unmet need of customer satisfaction and primacy. In recognizing and experiencing this, the idea of Travelex was born and the company created.</p>

                    <p>Travelex is an exclusive company that runs at the forefront of its competitors to serve the myriad travel needs of its customers. With services ranging from arranging corporate events to fashioning private retreats, Travelex commits to satisfying these necessities of contemporary urban life. We at Travelex treasure and place supreme the specific requirements of our diverse clients in every aspect.</p>

                    <p>We understand the uniqueness of each of our clients and to serve their diversified nature, the Travelex philosophy dictates the principle of customization. Every aspect of the travel uncompromised for our customers- how they imagine their travel through and through. Individual choices and opinions take precedence over all other factors. Travelex provides quality service and superior craftsmanship in fashioning travel requests into works of art.</p>

                    <p>We do this through our team of dedicated, talented and expert players who have a domain experience of more than 10 years, but an even more intensive experience in catering to human needs and desires. This core team, an amalgamation of talent, skill and knowledge creates a dynamic of enormous magnitude, a dynamic channeled towards the creation of travel solutions, towards the creation of Travelex.</p>

                    <p></p>
                </div>
            </div>
        </div>
        <!-- </div> -->
    </div>
</div>
</body>
</html>