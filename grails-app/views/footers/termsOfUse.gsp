<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="headerFooterLayout">
</head>

<body>

<div class="content-page page-wrapper full-height">
    <div class="inner-page-container">
        <!-- <div class="middle-container"> -->
        <div class="container-fluid">
            <div class="main-content-container">
                <div class="main-icons-container-vertical">
                    <g:render template="/appTemplate/menuTemplate"/>
                </div>

                <div class="main-content-container-content">
                    <h1>Terms of Services</h1>

                    <p class="text-muted">Terms of Service binding users for the use of websites and applications offered by <a
                            href="${createLink(controller: 'home',action: 'landingPage')}">www.travelex360.com</a> <br> <strong>Last updated on:</strong> 11th August 2014
                    </p>

                    <p>The below mentioned terms of services are applicable on any user/visitor (the "User") using any services, websites and applications offered by Travelex 360 Pvt. Ltd. , a travel technology company incorporated and existing in India in accordance with the Companies Act, 1956. Travelex360 .com is a travel website owned and operated by Travelex, which inclusive of its all its affiliated domains and various other sub-domains and country sites, is together referred to as "Travelex 360"</p>

                    <p>The terms "we", "us", and "website" refer to Travelex 360 and the terms "you" and "your" refer to you, as a user of Travelex 360 . The term "personal information" means information that you provide to us which personally identifies you to be contacted or identified, such as your name, phone number, email address, and any other data that is tied to such information.</p>

                    <p>As a user, these Terms of Service (<strong>"Terms"</strong>) govern your access to and use of the services, websites, and applications offered by Travelex 360 (the <strong>"Service"</strong>). Your access to and use of the Service offered by Travelex360 is conditioned on your acceptance of and compliance with the Terms and conditions mentioned below. By accessing or using the Service you agree to be bound by these Terms.
                    </p>

                    <p><strong>By using Travelex 360 , you agree to be bound by terms of this agreement. If you do not agree with these terms, please do not use Travelex 360 .</strong>
                    </p>

                    <p></p><h4><strong>Use of the Service</strong></h4>

                    <p></p>

                    <p>As a user, you agree to comply with these Terms and all applicable local, state, national, and international laws, rules and regulations. Also, as a user, you must provide us accurate information, including your real name, when you create your account on Travelex 360 .</p>

                    <p>Travelex 360 is free to change the service, stop the service or feature of the service, create usage limits for the Service at any given point in time, to you or to users at large without giving any prior notice/notification.</p>

                    <p>As a user, you are responsible for safeguarding the password that you use to access the Service and for any activities or actions under your password. We encourage you to use "strong" passwords (that use a combination of upper and lower case letters, numbers and symbols) with your account. Travelex  360 will not be liable for any loss or damage arising from your failure to comply with this instruction.</p>

                    <p></p><h4><strong>Information we collect</strong></h4>

                    <p></p>

                    <p>We record information relating to your use of Travelex 360 such as the searches you undertake, the pages you view, your browser type, IP address, location, requested URL, referring URL, and timestamp information. We use this type of information to administer Travelex360 and provide the highest possible level of service to you. We also use this information in the aggregate to perform statistical analyses of user behaviour and characteristics in order to measure interest in and use of the various areas of Travelex 360. You cannot be identified from this aggregate information.</p>

                    <p></p><h4><strong>Travelex 360 as a Metasearch Engine</strong></h4>

                    <p></p>

                    <p>The Website allows you to search across multiple providers of travel services. Based on your search criteria, Travelex 360 sends out requests to multiple vendors and displays the results returned. You are allowed to click through to these third party supplier's websites and make bookings directly on their site. Furthermore, Travelex360 website displays many travel deals offered by third parties. Based on your selection, Travelex360 links you to these third party suppliers’ websites where you can make your bookings. From time to time, you may also receive travel deal information from third party suppliers if you agree to receive our email newsletters. Any selection of these deals would again link you to the third party supplier's website where you can fulfill your booking. Your access and use of the Service and Travelex360 Web site and any content made available through the Service or Travelex 360 Web site constitutes your agreement to these Terms and to the <a
                            href="${createLink(controller: 'home', action: 'privacyPolicy')}">Travelex360 Privacy Policy</a>.</p>

                    <p></p><h4><strong>Content and its usage</strong></h4>

                    <p></p>

                    <p><strong>"Content"</strong> means any information, text, graphics, or other materials uploaded, downloaded or appearing on the Service.

                    </p>

                    <p>Types of Content:</p>

                    <p>Broadly, the Site Content on travelex 360 can be categorized as follows:</p>

                    <p><strong>a) User Generated Content (“UGC”):</strong> UGC can be described as any form of content such as text, video, blogs, discussion form posts, digital images, audio files, and other forms of media that are created by you or other users of Travelex 360 .
                    </p>

                    <p><strong>b) Proprietary Content:</strong>  Proprietary Content is the content developed, produced and owned by Travelex360 (i.e. by its employees, consultants, partners or service providers).

                    </p>

                    <p><strong>c) Third-Party Content:</strong> Third-Party Content is the content which ithetraveler does not own but has sourced from third parties under appropriate licensing.
                    </p>

                    <p></p><h4><strong>Licensing Arrangement</strong></h4>

                    <p></p>

                    <p><strong>a) Your License to Travelex360</strong></p>

                    <p>You retain ownership of all the Content you submit, post, display, or otherwise make available on the Service.</p>

                    <p>By submitting, posting or displaying Content on or through the Service, you grant us a worldwide, non-exclusive, royalty-free license (with the right to sublicense) to use, copy, reproduce, process, adapt, modify, publish, transmit, display and distribute such Content in any and all media or distribution methods (now known or later developed).</p>

                    <p>You agree that this license includes the right for other users of the Service to modify your Content, and for Travelex360 to make your Content available to others for the publication, distribution, syndication, or broadcast of such Content on other media and services, subject to our terms and conditions for such Content use. Such additional uses by Travelex 360 or others may be made with no compensation paid to you with respect to the Content that you submit, post, transmit or otherwise make available through the Service.</p>

                    <p>We may modify or adapt your Content in order to transmit, display or distribute it over computer networks and in various media and/or make changes to your Content as are necessary to conform and adapt that Content to any requirements or limitations of any networks, devices (Including but not limited to Mobile devices), services or media.</p>

                    <p>Additionally, by uploading or writing such content on Travelex360 , you warrant, represent and agree that you have the right to grant Travelex360 the license described above. You also represent, warrant and agree that you have not and will not contribute any Content that (a) infringes, violates or otherwise interferes with any copyright or trademark of another party, (b) reveals any trade secret, unless the trade secret belongs to you or you have the owner's permission to disclose it, (c) infringes any intellectual property right of another or the privacy or publicity rights of another, (d) is libelous, defamatory, abusive, threatening, harassing, hateful, offensive or otherwise violates any law or right of any third party, (e) creates an impression that you know is incorrect, misleading, or deceptive, including by impersonating others or otherwise misrepresenting your affiliation with a person or entity; (f) contains other people's private or personally identifiable information without their express authorization and permission, and/or (g) contains or links to a virus, trojan horse, worm, time bomb or other computer programming routine or engine that is intended to damage, detrimentally interfere with, surreptitiously intercept or expropriate any system, data or information.</p>

                    <p>Travelex360 reserves the right in its discretion to remove any Content from the Site, suspend or terminate your account at any time, or pursue any other remedy or relief available under equity or law.</p>

                    <p></p><h4><strong>b) Travelex 360 's Licenses to You</strong></h4>

                    <p></p>

                    <p>Travelex360 grants you a non-exclusive, limited, non sub-licensable license to access and use the Site and to view, copy and print portions of the Site Content. Such license is subject to these Site Terms, and specifically conditioned upon the following: (i) you may only view, copy and print portions of the Site Content for your own informational, personal and non-commercial use; (ii) you may not reverse engineer, modify or otherwise make derivative uses of the Site or the Site Content, or any portion thereof; (iii) any displays or printouts of Site Content must be marked "Copyright owned by Travelex360 .com -  All rights reserved.” (iv) you may not remove or modify any copyright, trademark, or other proprietary notices that have been placed in the Site Content; (v) you may not use any data mining, robots or similar data gathering or extraction methods; (vi) you may not use the Site or the Site Content other than for its intended purpose; and (vii) you may not reproduce, prepare derivative works from, distribute or display the Site or any Site Content (except for page caching), except as provided herein. Except as expressly permitted above, any use of any portion of the Site or Site Content without the prior written permission of Travelex360 is strictly prohibited and will terminate the license granted herein.</p>

                    <p>Travelex360 owns all right, title and interest in and to the Proprietary Content it provides on Service and any trademark, trade name, service mark, patent or copyright on the Proprietary Content will be and remain vested with Travelex360 .</p>

                    <p>Any unauthorized use may also violate applicable laws, including without limitation copyright and trademark laws and applicable communications regulations. Unless explicitly stated herein, nothing in these Site Terms may be construed as conferring any license to intellectual property rights, whether by estoppel, implication or otherwise. This license is revocable at any time without notice.
                    You represent and warrant that your use of the Site and the Site Content will be consistent with this license and will not infringe or violate the rights of any other party or breach any contract or legal duty to any other parties, or violate any applicable law. You expressly agree to indemnify Travelex360 against any liability to any person arising out of your use of Site Content not in accordance with these Site Terms.
                    You may only use the attribution required by this Section in the manner set out above. In exercising these rights, you may not implicitly or explicitly assert or imply any connection with, sponsorship or endorsement by Travelex360 , or any Travelex360 user, of you or your use of the work, without the separate, express prior written permission of Travelex360 or the Travelex360 user.</p>

                    <p>We reserve the right at all times (but will not have an obligation) to remove or refuse to distribute any Content on the Service and to terminate users or reclaim usernames. We also reserve the right to access, read, preserve, and disclose any information as we reasonably believe is necessary to (i) satisfy any applicable law, regulation, legal process or governmental request, (ii) enforce the Terms, including investigation of potential violations hereof, (iii) detect, prevent, or otherwise address fraud, security or technical issues, (iv) respond to user support requests, or (v) protect the rights, property or safety of Travelex360 , its users and the public..</p>

                    <p></p><h4><strong>c) Third-Party Sites</strong></h4>

                    <p></p>

                    <p>The Site may contain links to third-party Web sites ("Third-Party Sites") and Third-Party Content. You use links to Third-Party Sites, or any Third-Party Content therein, at your own risk. Travelex360 does not monitor or have any control over, and makes no claim or representation regarding, Third-Party Content or Third-Party Sites. Travelex360 provides such content and links only as a convenience, and Third-Party Content or links to Third-Party Sites does not necessarily imply Travelex360 's endorsement, adoption or sponsorship of, or affiliation with such Third-Party. Travelex360 accepts no responsibility for reviewing changes or updates to, or the quality, content, policies, nature or reliability of, Third-Party Content, Third-Party Sites, or Web sites linking to the Site. When you leave the Site, our terms and policies no longer govern. You should review applicable terms and policies, including privacy and data gathering practices, of any Third-Party Site, and should make whatever investigation you feel necessary or appropriate before proceeding with any transaction with any Third-Party.</p>

                    <p></p><h4><strong>Hyperlink</strong></h4>

                    <p></p>

                    <p>You are granted a limited, non-exclusive right to create a text hyperlink to the Site for non-commercial purposes, provided such link does not portray ithetraveler or any of its products or services in a false, misleading, derogatory or otherwise defamatory manner and provided further that the linking site does not contain any adult or illegal material or any material that is offensive, harassing or otherwise objectionable. Ithetraveler is free to revoke this limited right at any time. You may not use a ithetraveler logo or other proprietary graphic of ithetraveler to link to the Site without the express written permission of Travelex360 . Further, you may not use, frame or utilize framing techniques to enclose any ithetraveler trademark, logo or other proprietary information, including the images found at the Site, the content of any text or the layout/design of any page or form contained on a page on the Site without Travelex360 ’s express written consent. Except as noted above, you are not conveyed any right or license by implication, estoppel or otherwise in or under any patent, trademark, copyright or proprietary right of ithetraveler or any third party. Nothing contained on the Site may be construed as granting, by implication, estoppel or otherwise, any license or right to use any patent, trademark, copyright or other proprietary right of Travelex360 or any third party without the prior written permission of Travelex360 or such other party that may own such patent, trademark, copyright or other proprietary right(s).</p>

                    <p></p><h4><strong>Rules of Usage</strong></h4>

                    <p></p>

                    <p>You must not do any of the following while accessing or using the Service: (i) use the Service for any unlawful purposes or for promotion of illegal activities; (ii) upload or post any Content (as defined above) in violation of the provisions contained in the "Your License to Travelex360 " section of these terms; (iii) use the Service for the purpose of spamming anyone; (iv) access or tamper with non-public areas of the Service, Travelex360 's computer systems, or the technical delivery systems of Travelex360 's providers; (v) probe, scan, or test the vulnerability of any system or network or breach or circumvent any security or authentication measures; (vi) access or search or attempt to access or search the Service by any means (automated or otherwise) other than through the currently available, published interfaces that are provided by Travelex360 (and only pursuant to those terms and conditions), unless you have been specifically allowed to do so in a separate agreement with Travelex360 (crawling the Service is permissible in accordance with these Terms, but scraping the Service without the prior consent of Travelex360 except as permitted by these Terms is expressly prohibited); (vii) forge any TCP/IP packet header or any part of the header information in any email or posting, or in any way use the Service to send altered, deceptive or false source-identifying information; or (viii) interfere with or disrupt (or attempt to do so) the access of any user, host or network, including, without limitation, sending a virus, overloading, flooding, spamming, mail-bombing the Service, or by scripting the creation of Content in such a manner as to interfere with or create an undue burden on the Service.</p>

                    <p>We may make available one or more APIs for interacting with the Service. Your use of any ithetraveler API is subject to these terms and the ithetraveler API Rules, which will be posted before we make these APIs available (as part of these Terms).</p>

                    <p></p><h4><strong>Proprietary Rights</strong></h4>

                    <p></p>

                    <p>All right, title, and interest in and to the Service (excluding UGC) are and will remain the exclusive property of Travelex360 and its licensors. The Service is protected by copyright, trademark, and other Indian and International laws. Except as expressly provided herein, nothing in the Terms gives you a right to use the Travelex360 name or any of the Travelex360 trademarks, logos, domain names, and other distinctive brand features. Any feedback, comments, or suggestions you may provide regarding the Service is entirely voluntary and we will be free to use such feedback, comments or suggestions as we see fit and without any obligation to you.</p>

                    <p>The Service may include advertisements, which may be targeted to the Content or information on the Service, queries made through the Service, or other information. The types and extent of advertising by Travelex360 on the Service are subject to change. In consideration for Travelex360 granting you access to and use of the Service, you agree that Travelex360 and its third party providers and partners may place such advertising on the Service or in connection with the display of Content or information from the Service whether submitted by you or others.</p>

                    <p></p><h4><strong>Copyright Policy</strong></h4>

                    <p></p>

                    <p>Travelex360 respects the intellectual property rights of others and expects users of the Service to do the same. We will respond to notices of alleged copyright infringement that comply with applicable law and are properly provided to us. If you believe that your Content has been copied in a way that constitutes copyright infringement, please provide our copyright agent with the following information in accordance with the Copyright and Information Technology Acts of India: (i) a physical or electronic signature of the copyright owner or a person authorized to act on their behalf; (ii) identification of the copyrighted work claimed to have been infringed; (iii) identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and information reasonably sufficient to permit us to locate the material; (iv) your contact information, including your address, telephone number, and an email address; (v) a statement by you that you have a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law; and (vi) a statement that the information in the notification is accurate, and, under penalty of perjury, that you are authorized to act on behalf of the copyright owner.</p>

                    <p>You can send the copyright notice at the following address:</p>
                    <address>
                        Travelex 360 Pvt. Ltd.
                        <br>
                        25/60 , Connaught Place,
                        <br>
                        A Block , New Delhi- 110001.
                        <br>
                        Email- <a href="mailto:info@travelx360.com">info@travelx360.com</a>
                    </address>

                    <p>We reserve the right to remove Content alleged to be infringing or otherwise illegal without prior notice and at our sole discretion. In appropriate circumstances, Travelex360 will also terminate a user's account if the user is determined to be a repeat infringer.</p>

                    <p></p><h4><strong>Privacy</strong></h4>

                    <p></p>

                    <p>Travelex360 values your privacy. Please review our <a
                            href="${createLink(controller: 'home',action: 'privacyPolicy')}">Privacy Policy</a> to learn more about how we collect and use information about you via the Service. By using the Service you consent to the transfer of your information for storage, processing and use by Travelex 360 .
                    </p>

                    <p></p><h4><strong>Disclaimers; Indemnity</strong></h4>

                    <p></p>

                    <p>You access and use the Service or any Content available on Travelex360 at your own risk. You understand and agree that the Service is provided to you on an "AS IS" and "AS AVAILABLE" basis. Without limiting the foregoing, TRAVELEX360 AND ITS PARTNERS DISCLAIM ANY WARRANTIES, EXPRESS OR IMPLIED, OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.</p>

                    <p>We make no warranty and disclaim all responsibility and liability for the completeness, accuracy, availability, timeliness, security or reliability of the Service or any content thereon. Travelex360 will not be responsible or liable for any harm to your computer system, loss of data, or other harm that results from your access to or use of the Service, or any Content. You also agree that Travelex360 has no responsibility or liability for the deletion of, or the failure to store or to transmit, any Content and other communications maintained by the Service. We make no warranty that the Service will meet your requirements or be available on an uninterrupted, secure, or error-free basis. No advice or information, whether oral or written, obtained from Travelex360 or through the Service, will create any warranty not expressly made herein.</p>

                    <p>TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, TRAVELEX360  AND ITS AFFILIATES, OFFICERS, EMPLOYEES, AGENTS, PARTNERS AND LICENSORS WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR PUNITIVE DAMAGES, INCLUDING WITHOUT LIMITATION, LOSS OF PROFITS, DATA, USE, GOOD-WILL, OR OTHER INTANGIBLE LOSSES, RESULTING FROM (i) YOUR ACCESS TO OR USE OF OR INABILITY TO ACCESS OR USE THE SERVICE; (ii) ANY CONDUCT OR CONTENT OF ANY THIRD PARTY ON THE SERVICE, INCLUDING WITHOUT LIMITATION, ANY DEFAMATORY, OFFENSIVE OR ILLEGAL CONDUCT OF OTHER USERS OR THIRD PARTIES; (iii) ANY CONTENT OBTAINED FROM THE SERVICE; AND (iv) UNAUTHORIZED ACCESS, USE OR ALTERATION OF YOUR TRANSMISSIONS OR CONTENT, WHETHER BASED ON WARRANTY, CONTRACT, TORT (INCLUDING NEGLIGENCE) OR ANY OTHER LEGAL THEORY, WHETHER OR NOT TRAVELEX360 HAS BEEN INFORMED OF THE POSSIBILITY OF SUCH DAMAGE, AND EVEN IF A REMEDY SET FORTH HEREIN IS FOUND TO HAVE FAILED OF ITS ESSENTIAL PURPOSE.</p>

                    <p>If anyone brings a claim against us related to your actions or Content on the Service, or actions or Content by or from someone using your account, you will indemnify and hold us harmless from and against all damages, losses, and expenses of any kind (including reasonable legal fees and costs) related to such claim.</p>

                    <p></p><h4><strong>General Terms</strong></h4>

                    <p></p>

                    <p></p><h5><strong>Jurisdiction:</strong></h5>

                    <p></p>

                    <p>This agreement and any disputes relating thereto shall be construed under the laws of India to be settled at a court of jurisdiction in New Delhi. Unless submitted to arbitration as set forth in the following paragraph, all claims, legal proceedings or litigation arising in connection with the Service will be brought to the court of jurisdiction in New Delhi.</p>

                    <p></p><h5><strong>Revision of terms and its binding</strong></h5>

                    <p></p>

                    <p>We may revise these Terms from time to time. The most current version will always be on this page (or such other page as the Service may indicate). If the revision, in our sole discretion, is material we will notify you via posting to our website or e-mail to the email associated with your account. By continuing to access or use the Service after those revisions become effective, you agree to be bound by the revised Terms.</p>

                    <p>The Service is operated and provided by Travelex 360 Pvt. Ltd. For any further query please write to us at info@travelx360 .com</p>

                    <p></p>
                </div>
            </div>
        </div>
        <!-- </div> -->
    </div>

</div>
</body>
</html>