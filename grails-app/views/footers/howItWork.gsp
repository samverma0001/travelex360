<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="headerFooterLayout">
</head>

<body>
<div class="content-page page-wrapper full-height">
    <div class="inner-page-container">
        <!-- <div class="middle-container"> -->
        <div class="container-fluid">
            <div class="main-content-container">
                <div class="main-icons-container-vertical">
                    <g:render template="/appTemplate/menuTemplate"/>
                </div>

                <div class="main-content-container-content">
                    <h1>Welcome to Travelex 360</h1>

                    <p>Conceived from an idea to journey the Indian voyager from the obsolete to the contemporary era of travel, Travelex is a revolutionizing endeavor in the industry. Our identity and integrity rests on the principles of choice and customization that we provide to the travellers of today. Couple that with an unbending resolve to serve and cater, Travelex has emerged a breath of fresh air in the travel scene in India.</p>

                    <div class="text-center">
                        <h3>What we do</h3>

                        <p>In reflecting the diverse travel needs of the Indian audience, Travelex has divided its work into three arenas:</p>

                        <div class="row">
                            <div class="col-md-4 text-left">
                                <div class="row step-box">
                                    <div class="col-md-4">
                                        <img src="../images/step1.png" class="img-full">
                                    </div>

                                    <div class="col-md-8">
                                        <h4>Arena 1</h4>

                                        <h3>We Understand</h3>
                                    </div>
                                </div>

                                <p>We at Travelex understand the needs and desires of our clients even before they utter it. We have spent years training ourselves to be great listeners and motivators.</p>
                            </div>

                            <div class="col-md-4 text-left">
                                <div class="row step-box">
                                    <div class="col-md-4">
                                        <img src="../images/step2.png" class="img-full">
                                    </div>

                                    <div class="col-md-8">
                                        <h4>Arena 2</h4>

                                        <h3>We Provide</h3>
                                    </div>
                                </div>

                                <p>We at Travelex provide our clients with unbiased options and recommendations so that there is no dearth of voyages to choose from. We provide ways for you to fulfill your desires- none too small, none too big.</p>
                            </div>

                            <div class="col-md-4 text-left">
                                <div class="row step-box">
                                    <div class="col-md-4">
                                        <img src="../images/step3.png" class="img-full">
                                    </div>

                                    <div class="col-md-8">
                                        <h4>Arena 3</h4>

                                        <h3>We Create</h3>
                                    </div>
                                </div>

                                <p>We at Travelex use the raw materials of your needs, your desires, your choices and sculpt them into marvelous works of art. Each work unique, special and exclusive just like you!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- </div> -->
    </div>
</div>

</body>
</html>