<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="headerFooterLayout">

</head>

<body>
<div class="content-page page-wrapper full-height">
    <div class="inner-page-container">
        <!-- <div class="middle-container"> -->
        <div class="container-fluid">
            <div class="main-content-container">
                <div class="main-icons-container-vertical">
                    <g:render template="/appTemplate/menuTemplate"/>
                </div>

                <div class="main-content-container-content">
                    <h1>Contact us</h1>

                    <p>We at Travelex are always interested to hear what you have to say. If you have a comment, a query, or even a conundrum for us to solve, reach out at the references below.</p>

                    <div class="row">
                        <div class="col-md-5 col-sm-6">
                            <h3>Visit Us At:</h3>

                            <p>25/60, A block,<br>
                                Middle Circle, Connaught Place,<br>
                                New Delhi- 110001.</p>
                        </div>

                        <div class="col-md-5 col-sm-6">
                            <h3>Contact us at:</h3>

                            <p><span class="glyphicon glyphicon-earphone"></span> +91 8512866161</p>

                            <p><span class="glyphicon glyphicon-envelope"></span>  info@travelex360.in</p>

                            <p><span class="glyphicon glyphicon-globe"></span> www.travelex360.com</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- </div> -->
    </div>

</div>

</body>
</html>