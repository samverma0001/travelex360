<html lang="en">
<head>
    <title>Create Activity</title>
    <style>
    .customLabel{
        text-align: left;
        font-weight: bold;
        margin-bottom: 5px;
    }
    .customText{
        margin-bottom: 5px;
    }
    .customSubmit{
        align-content: center;
    }
    .form-control {
        width: 250px !important;
    }
    </style>
    <g:link rel="stylesheet" url="${resource(dir: 'css',file: 'bootstrap.css')}"/>
    <g:link rel="stylesheet" url="bootstrapValidator"/>
    <g:javascript src="jquery-1.10.2.js"/>
    <g:javascript src="bootstrap.min.js" />
    %{--<g:javascript src="bootstrapValidator.js" />--}%
</head>

<body>
<div class="container">
    <div class="row">
        <div class="span12">
            <div id="tab" class="btn-group">
                <div id="tab" class="btn-group">
                    <a href="${createLink(controller: 'admin', action: 'createActivityTemp')}" id="createAct"
                       class="btn btn-large btn-info  disableAll"
                       onclick="activeButton(this.id)" style="  border-top-left-radius: 4px;border-bottom-left-radius: 4px;">Create</a>
                    <a href="${createLink(controller: 'admin', action: 'editActivity')}" id="editAct"
                       class="btn btn-large btn-info" style="border-top-right-radius: 4px;border-bottom-right-radius: 4px;" onclick="activeButton(this.id)">Edit</a>
                </div>
            </div>
            <div class="tab-content" style="margin-top: 1%">

                <div class="tab-pane active" id="createActivityTab">

                    <g:form id="" enctype="multipart/form-data" name="createActivityForm" action="saveActivity" style="margin-bottom: 75px;">


                        <div class="form-group col-md-12">
                            <div class="col-md-3 customLabel">
                                Name
                            </div>
                            <div class="col-md-9 customText">
                                <input type="text" class="form-control col-md-9" name="name"/>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="col-md-3 customLabel">
                                Activity Type
                            </div>
                            <div class="col-md-9 customText">
                                <select name="activityType" class="form-control col-md-9">
                                    <option value="EATING_EXOTIC">Eating And Exotic</option>
                                    <option value="ADVENTURE_SPORTS">Adventure Sports</option>
                                    <option value="ARTS_CRAFTS">Arts And Crafts</option>
                                    <option value="DISPLAYS_SHOWS">Display And Shows</option>
                                    <option value="BODY_WELLNESS">Body And Wellness</option>
                                    <option value="SHOPPING">Shopping</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="col-md-3 customLabel">
                                Location
                            </div>
                            <div class="col-md-9 customText">
                                <input type="text" class="form-control  col-md-9"    name="location"/>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="col-md-3 customLabel">
                                Duration
                            </div>
                            <div class="col-md-9 customText">
                                <input type="text" class="form-control  col-md-9"    name="duration"/>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="col-md-3 customLabel">
                                Primary Interest
                            </div>
                            <div class="col-md-9 customText">
                                <input type="text" class="form-control col-md-9"    name="primaryInterest"/>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="col-md-3 customLabel">
                                Secondary Interest
                            </div>
                            <div class="col-md-9 customText">
                                <input type="text" class="form-control col-md-9"   name="secondaryInterest"/>
                            </div>
                        </div>


                        <div class="form-group col-md-12">
                            <div class="col-md-3 customLabel">
                                Activity Company Name
                            </div>
                            <div class="col-md-9 customText">
                                <input type="text" class="form-control col-md-9"   name="activityCompanyName"/>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="col-md-3 customLabel">
                               Service Provider Name
                            </div>
                            <div class="col-md-9 customText">
                                <input type="text" class="form-control col-md-9" id="serviceProviderName" name="serviceProviderName"/>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="col-md-3 customLabel">
                                Company Authorised PersonalName
                            </div>
                            <div class="col-md-9 customText">
                                <input type="text" class="form-control col-md-9"   name="companyAuthorisedPersonalName"/>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="col-md-3 customLabel">
                                Phone Number
                            </div>
                            <div class="col-md-9 customText">
                                <input type="text" class="numericOnly form-control col-md-9"   maxlength="10" name="phoneNumber"/>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="col-md-3 customLabel">
                                Email
                            </div>
                            <div class="col-md-9 customText">
                                <input type="email" class="form-control col-md-9" name="email"/>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="col-md-3 customLabel">
                                Activity Image
                            </div>
                            <div class="col-md-9 customText">
                                <input type="file" id="profileImage" name="profileImage"  value="">
                            </div>
                        </div>

                            <button type="submit" class="btn btn-primary">Submit</button>

                    </g:form>
                </div>

            </div>
        </div>
    </div>
</div>
<script>
    function activeButton(id) {
        $("#id").addClass('active')
    }
    $(document).ready(function(){
        $("#createAct").addClass('active')
    });
    //        var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    //        filter.test(sEmail)
    $(".numericOnly").keypress(function (e) {
        if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
    });

//    $('#createActivityForm').bootstrapValidator({
//        fields: {
//            name: {
//                validators: {
//                    notEmpty: {
//                        message: '&nbsp &nbsp &nbsp Activity name is Required '
//                    }
//                }
//            },
//            location: {
//                validators: {
//                    notEmpty: {
//                        message: '&nbsp &nbsp &nbsp  Location is Required '
//                    }
//                }
//            },
//            primaryInterest: {
//                validators: {
//                    notEmpty: {
//                        message: '&nbsp &nbsp &nbsp  Primary Interest is Required '
//                    }
//                }
//            },
//            secondaryInterest: {
//                validators: {
//                    notEmpty: {
//                        message: '&nbsp &nbsp &nbsp Secondary Interest is Required '
//                    }
//                }
//            },
//            activityCompanyName: {
//                validators: {
//                    notEmpty: {
//                        message: '&nbsp &nbsp &nbsp Company Name is Required '
//                    }
//                }
//            },
//            companyAuthorisedPersonalName: {
//                validators: {
//                    notEmpty: {
//                        message: '&nbsp &nbsp &nbsp Authorised Personal Name is Required '
//                    }
//                }
//            },
//            phoneNumber: {
//                validators: {
//                    notEmpty: {
//                        message: '&nbsp &nbsp &nbsp Phone number is Required '
//                    }
//                }
//            },
//            email: {
//                validators: {
//                    notEmpty: {
//                        message: '&nbsp &nbsp &nbsp email is Required '
//                    }
//                }
//            }
//
//
//        }
//    });


//    $('#serviceProviderName').autocomplete({
//        source:'fetchServiceProvider',
//        minLength:1,
//        focus: function(event, ui) {
//            event.preventDefault();
//            $(this).val(ui.item.label);
//        },
//        select: function(event, ui) {
//            event.preventDefault();
//        }
//    });
</script>


</body>
</html>