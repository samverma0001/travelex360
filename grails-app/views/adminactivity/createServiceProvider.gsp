<html lang="en">
<head>
    <title>Create Service Provider</title>
    <style>
    .customLabel{
        text-align: left;
        font-weight: bold;
        margin-bottom: 5px;
    }
    .customText{
        margin-bottom: 5px;
    }
    .customSubmit{
        align-content: center;
    }
    .form-control {
        width: 250px !important;
    }
    </style>
    <g:link rel="stylesheet" url="bootstrap"/>
    <g:link rel="stylesheet" url="bootstrapValidator"/>
    <g:javascript src="jquery-1.10.2.js"/>
    <g:javascript src="${resource(dir: 'js',file: 'bootstrap.min.js')}"/>
    %{--<g:javascript src="/bootstrapValidator.js" />--}%
</head>

<body>
<div class="container">
    <div class="row">
        <div class="span12">
            <div id="tab" class="btn-group">
                <div id="tab" class="btn-group">
                    <a href="${createLink(controller: 'admin', action: 'createServiceProvider')}" id="createSer"
                       class="btn btn-large btn-info  disableAll"
                       onclick="activeButton(this.id)" style="  border-top-left-radius: 4px;border-bottom-left-radius: 4px;">Create</a>
                    <a href="${createLink(controller: 'admin', action: 'editServiceProvider')}" id="editSer"
                       class="btn btn-large btn-info" style="border-top-right-radius: 4px;border-bottom-right-radius: 4px;" onclick="activeButton(this.id)">Edit</a>
                </div>
            </div>
            <div class="tab-content" style="margin-top: 1%">

                <div class="tab-pane active" id="createServiceProviderTab">

                    <g:form enctype="multipart/form-data"  id="" name="createServiceProviderForm" action="saveServiceProvider" style="margin-bottom: 75px;">


                        <div class="form-group col-md-12">
                            <div class="col-md-3 customLabel">
                                Name
                            </div>
                            <div class="col-md-9 customText">
                                <input type="text" class="form-control col-md-9" name="name"/>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="col-md-3 customLabel">
                                Location
                            </div>
                            <div class="col-md-9 customText">
                                <input type="text" class="form-control  col-md-9"    name="location"/>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="col-md-3 customLabel">
                                Authorised PersonalName
                            </div>
                            <div class="col-md-9 customText">
                                <input type="text" class="form-control col-md-9"   name="authorisedPersonalName"/>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="col-md-3 customLabel">
                                Phone Number
                            </div>
                            <div class="col-md-9 customText">
                                <input type="text" class="numericOnly form-control col-md-9"   maxlength="10" name="phoneNumber"/>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="col-md-3 customLabel">
                                Email
                            </div>
                            <div class="col-md-9 customText">
                                <input type="email" class="form-control col-md-9"   name="email"/>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="col-md-3 customLabel">
                                Service Provider Image
                                </div>
                            <div class="col-md-9 customText">
                                <input type="file" id="profileImage" name="profileImage"  value="">
                            </div>
                        </div>


                        <button type="submit" class="btn btn-primary">Submit</button>

                    </g:form>
                </div>

            </div>
        </div>
    </div>
</div>
<script>
    function activeButton(id) {
        $("#id").addClass('active')
    }
    $(document).ready(function(){
        $("#createSer").addClass('active')
    });
    //        var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    //        filter.test(sEmail)
    $(".numericOnly").keypress(function (e) {
        if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
    });


//    $('#createActivityForm').bootstrapValidator({
//        fields: {
//            name: {
//                validators: {
//                    notEmpty: {
//                        message: '&nbsp &nbsp &nbsp Activity name is Required '
//                    }
//                }
//            },
//            location: {
//                validators: {
//                    notEmpty: {
//                        message: '&nbsp &nbsp &nbsp  Location is Required '
//                    }
//                }
//            },
//            primaryInterest: {
//                validators: {
//                    notEmpty: {
//                        message: '&nbsp &nbsp &nbsp  Primary Interest is Required '
//                    }
//                }
//            },
//            secondaryInterest: {
//                validators: {
//                    notEmpty: {
//                        message: '&nbsp &nbsp &nbsp Secondary Interest is Required '
//                    }
//                }
//            },
//            activityCompanyName: {
//                validators: {
//                    notEmpty: {
//                        message: '&nbsp &nbsp &nbsp Company Name is Required '
//                    }
//                }
//            },
//            companyAuthorisedPersonalName: {
//                validators: {
//                    notEmpty: {
//                        message: '&nbsp &nbsp &nbsp Authorised Personal Name is Required '
//                    }
//                }
//            },
//            phoneNumber: {
//                validators: {
//                    notEmpty: {
//                        message: '&nbsp &nbsp &nbsp Phone number is Required '
//                    }
//                }
//            },
//            email: {
//                validators: {
//                    notEmpty: {
//                        message: '&nbsp &nbsp &nbsp email is Required '
//                    }
//                }
//            }
//
//
//        }
//    });
</script>


</body>
</html>