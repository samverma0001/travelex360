
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="../css/tableSorterStyle.css" type="text/css" id="" media="print, projection, screen" />
    <script type="text/javascript" src="../js/jquery.tablesorter.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".table").tablesorter();
        });
        function renderModal(activityId){
            $.ajax({
                url : 'editActivityModal',
                type : 'POST',
                data: {activityId: activityId},
                success : function(data) {
                    $(".modal-body").html(data);
                    $("#myModal").modal('show');
                },
                failure : function(d) {
                    console.log(d);
                },
                complete : function(d) {
                    console.log(d);
                }
            });
        }
        function updateActivity(){
            $.ajax({
                url : 'updateActivityModal',
                type : 'POST',
                data: {activityId: $("#activityId").val(),activityName: $("#acitivityName").val(), type:$("#activityType").val(),location:$('location').val(),duration: $("#duration").val(),primaryInterest: $("#primaryInterest").val(),secondaryInterest: $('secondaryInterest').val(),activityCompanyName: $("#activityCompanyName").val(),serviceProviderName:$('serviceProviderName').val(),companyAuthorisedPersonalName: $("companyAuthorisedPersonalName").val(),phoneNumber: $("phoneNumber").val(),email:$("email").val()},
                success : function(data) {
                  alert("Successfully Saved.");
                    $("#myModal").modal('hide');
                },
                failure : function(d) {
                    console.log(d);
                },
                complete : function(d) {
                    console.log(d);
                }
            });
        }
    </script>
    <style>
    .alignLeft{
        text-align:left;
    }
    .alignCenter{
        text-align:center;
    }
    .modal-dialog {
        width: 90% !important;
    }
    .modal-footer {
        margin-top: 225px !important;
        text-align: center !important;;
    }
    </style>
</head>

<body>
    <table cellspacing="1" class="tablesorter" width="100%">
        <thead>
        <tr>
            <th class="alignLeft">Name of Activity</th>
            <th class="alignLeft">Name of Service Provider</th>
            <th class="alignLeft">Location</th>
            <th class="alignLeft">Duration</th>
            <th class="alignLeft">Activity Type</th>
            <th class="alignLeft">Edit Activity</th>
        </tr>
        </thead>

        <tbody>
            <g:each in="${activitiesList}">
                <tr>
                    <td class="alignLeft">${it.name}</td>
                    <td class="alignLeft">name</td>
                    <td class="alignLeft">${it.location}</td>
                    <td class="alignLeft">${it.duration}</td>
                    <td class="alignLeft">${it.type}</td>
                    <th class="alignCenter" onclick="renderModal(${it.id})" style="cursor: pointer">Edit</th>
                </tr>
            </g:each>
        </tbody>
    </table>
%{--<form id="imageform" method="post" enctype="multipart/form-data" action='${createLink(controller:'admin',action:'updateProfileImage')}'>
    <input type="file" name="profileImage" id="profileImage" />
</form>
<div id='preview' style="margin-top: 25px;">
    <img height="100px" src="${createLink(controller:'user', action:'renderProfileImage')}" alt="" />
</div>--}%

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Activity</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" onclick="updateActivity()" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


</body>
</html>