

        <input type="hidden" value="22" id="activityId"/>
        <div class="form-group col-md-12">
            <div class="col-md-3 customLabel">
                Name
            </div>
            <div class="col-md-9 customText">
                <input type="text" class="form-control col-md-9" id="acitivityName" name="name" value="22"/>
            </div>
        </div>

        <div class="form-group col-md-12">
            <div class="col-md-3 customLabel">
                Activity Type
            </div>
            <div class="col-md-9 customText">
                <select name="activityType" id="activityType" class="form-control col-md-9">
                    <option <g:if test="${'Eating And Exotic' == 'Eating And Exotic'}"> selected="selected" </g:if> value="EATING_EXOTIC">Eating And Exotic</option>
                    <option <g:if test="${'Adventure Sports' == 'Adventure Sports'}"> selected="selected" </g:if> value="ADVENTURE_SPORTS">Adventure Sports</option>
                    <option <g:if test="${'Arts And Crafts' == 'Arts And Crafts'}"> selected="selected" </g:if> value="ARTS_CRAFTS">Arts And Crafts</option>
                    <option <g:if test="${'Display And Shows' == 'Display And Shows'}"> selected="selected" </g:if> value="DISPLAYS_SHOWS">Display And Shows</option>
                    <option <g:if test="${'Body And Wellness' == 'Body And Wellness'}"> selected="selected" </g:if> value="BODY_WELLNESS">Body And Wellness</option>
                    <option <g:if test="${'Shopping' == 'Shopping'}"> selected="selected" </g:if> value="SHOPPING">Shopping</option>
                </select>
            </div>
        </div>

        <div class="form-group col-md-12">
            <div class="col-md-3 customLabel">
                Location
            </div>
            <div class="col-md-9 customText">
                <input type="text" id="location" class="form-control  col-md-9" value="location" name="location"/>
            </div>
        </div>

        <div class="form-group col-md-12">
            <div class="col-md-3 customLabel">
                Duration
            </div>
            <div class="col-md-9 customText">
                <input type="text" class="form-control  col-md-9"  value="duration" id="duration" name="duration"/>
            </div>
        </div>

        <div class="form-group col-md-12">
            <div class="col-md-3 customLabel">
                Primary Interest
            </div>
            <div class="col-md-9 customText">
                <input type="text" class="form-control col-md-9"  value="primaryInterest" id="primaryInterest" name="primaryInterest"/>
            </div>
        </div>

        <div class="form-group col-md-12">
            <div class="col-md-3 customLabel">
                Secondary Interest
            </div>
            <div class="col-md-9 customText">
                <input type="text" class="form-control col-md-9"  value="activityInstance.secondaryInterest" id="secondaryInterest" name="secondaryInterest"/>
            </div>
        </div>


        <div class="form-group col-md-12">
            <div class="col-md-3 customLabel">
                Activity Company Name
            </div>
            <div class="col-md-9 customText">
                <input type="text" class="form-control col-md-9"  value="activityInstance.activityCompanyName" id="activityCompanyName" name="activityCompanyName"/>
            </div>
        </div>

        <div class="form-group col-md-12">
            <div class="col-md-3 customLabel">
                Service Provider Name
            </div>
            <div class="col-md-9 customText">
                <input type="text" class="form-control col-md-9" id="serviceProviderName" value="name" name="serviceProviderName"/>
            </div>
        </div>

        <div class="form-group col-md-12">
            <div class="col-md-3 customLabel">
                Authorised PersonalName
            </div>
            <div class="col-md-9 customText">
                <input type="text" class="form-control col-md-9" value="name" id="companyAuthorisedPersonalName" name="companyAuthorisedPersonalName"/>
            </div>
        </div>

        <div class="form-group col-md-12">
            <div class="col-md-3 customLabel">
                Phone Number
            </div>
            <div class="col-md-9 customText">
                <input type="text" class="numericOnly form-control col-md-9"  value="name" id="phoneNumber" maxlength="10" name="phoneNumber"/>
            </div>
        </div>

        <div class="form-group col-md-12">
            <div class="col-md-3 customLabel">
                Email
            </div>
            <div class="col-md-9 customText">
                <input type="email" class="form-control col-md-9" value="name" id="email" name="email"/>
            </div>
        </div>

        <div class="form-group col-md-12">
            <div class="col-md-3 customLabel">
                Activity Image
            </div>
            <div class="col-md-9 customText">
                <input type="file" id="profileImage" name="profileImage"  value="">
            </div>
        </div>