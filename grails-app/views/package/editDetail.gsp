<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="admin"/>
    %{--<script src="${resource(dir: "js", file: "jquery.MultiFile.js")}"></script>--}%

</head>

<body>
<div class="container" style="margin-bottom: 10%">
    <g:hasErrors bean="${packageDetailCO}">
        <div class="alert alert-danger alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;
            </span><span class="sr-only">Close</span></button>

            <div id="errors" class="alert alert-error">
                <g:renderErrors bean="${packageDetailCO}"/>
            </div>
        </div>
    </g:hasErrors>
    <p><strong>Edit detail(s) to this package</strong></p>
    <g:form controller="package" action="updateDetailForPackage" enctype="multipart/form-data" method="post">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <label>Images</label>
                </div>

                <div class="col-md-9">
                    <input type="file" class="form-control" name="holidayImages"/>

                </div>
            </div>
            <br/>

            <div class="row">
                <div class="col-md-3">
                    <label>Overview</label>
                </div>

                <div class="col-md-9 form-group input-group">
                    <g:textArea name="overview" value="${packageDetail?.overview}" class="form-control">

                    </g:textArea>
                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <label>Inclusions</label>
                </div>

                <div class="col-md-9 form-group input-group">
                    <g:textArea name="inclusion" value="${packageDetail?.inclusion}" class="form-control">
                    </g:textArea>

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <label>Exclusion</label>
                </div>

                <div class="col-md-9 form-group input-group">
                    <g:textArea name="exclusion" value="${packageDetail?.exclusion}" class="form-control">
                    </g:textArea>

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <label>Payment Policy</label>
                </div>

                <div class="col-md-9 form-group input-group">
                    <g:textArea name="paymentPolicy" value="${packageDetail?.paymentPolicy}" class="form-control">
                    </g:textArea>

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <label>Cancellation Policy</label>
                </div>

                <div class="col-md-9 form-group input-group">
                    <g:textArea name="cancellationPolicy" value="${packageDetail?.cancellationPolicy}"
                                class="form-control">
                    </g:textArea>

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <label>Terms & Cond.</label>
                </div>

                <div class="col-md-9 form-group input-group">
                    <g:textArea name="termsCondition" value="${packageDetail?.termsCondition}" class="form-control">
                    </g:textArea>

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>
        </div>
        <g:hiddenField name="holidayPackageId" value="${holidayPackage?.id}"/>
        <g:hiddenField name="detailId" value="${packageDetail?.id}"/>
        <g:hiddenField name="holidayImageId" value="${holidayImageId}"/>
        <div class="row">
            <input type="submit" name="updateDetail" class="btn btn-primary" value="Update detail"/>
        </div>
    </g:form>
</div>

</body>
</html>