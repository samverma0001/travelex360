<%@ page import="com.travelex.admin.HolidayHotelStayDetail; com.travelex.admin.StayDetail; com.travelex.admin.PackageHotelType" %>
<style>
/**** BASE ****/
body {
    color: #888;
}

a {
    color: #03a1d1;
    text-decoration: none !important;
}

/**** LAYOUT ****/
.list-inline > li {
    padding: 0 10px 0 0;
}

.container-pad {
    padding: 30px 15px;
}

/**** MODULE ****/
.bgc-fff {
    background-color: #fff !important;
}

.box-shad {
    -webkit-box-shadow: 1px 1px 0 rgba(0, 0, 0, .2);
    box-shadow: 1px 1px 0 rgba(0, 0, 0, .2);
}

.brdr {
    border: 1px solid #ededed;
}

/* Font changes */
.fnt-smaller {
    font-size: .9em;
}

.fnt-lighter {
    color: #bbb;
}

/* Padding - Margins */
.pad-10 {
    padding: 10px !important;
}

.mrg-0 {
    margin: 0 !important;
}

.btm-mrg-10 {
    margin-bottom: 10px !important;
}

.btm-mrg-20 {
    margin-bottom: 20px !important;
}

/* Color  */
.clr-535353 {
    color: #535353;
}

/**** MEDIA QUERIES ****/
@media only screen and (max-width: 991px) {
    #property-listings .property-listing {
        padding: 5px !important;
    }

    #property-listings .property-listing a {
        margin: 0;
    }

    #property-listings .property-listing .media-body {
        padding: 10px;
    }
}

@media only screen and (min-width: 992px) {
    #property-listings .property-listing img {
        max-width: 180px;
    }
}
</style>

<div class="container-fluid" style="background-color:#e8e8e8;margin-bottom: 15%">
    <div class="container container-pad" id="property-listings">

        <div class="row">
            <div class="col-md-12">
            </div>
        </div>

        <g:if test="${stayDetailList}">
            <g:each in="${stayDetailList}" var="stayDetail">
                <div class="row">
                    <div class="col-md-12">
                        <nav class="navbar navbar-default" role="navigation">
                            <div class="container-fluid">
                                <div class="navbar-header">
                                    <a class="navbar-brand">
                                        <strong>${stayDetail?.city}&nbsp;&nbsp;Hotel(s)</strong>
                                    </a>
                                </div>
                            </div>
                        </nav>

                        <g:set var="holidayHotel"
                               value="${HolidayHotelStayDetail.findAllByStayDetail(stayDetail).holidayHotel}"/>

                        <g:each in="${holidayHotel}" var="hotel">
                            <div class="brdr bgc-fff pad-10 box-shad btm-mrg-20 property-listing">
                                <div class="media">
                                    <a class="pull-left" href="#">
                                        <img alt="image" class="img-responsive" width="200" height="300"
                                             style="height: 175px"
                                             src="${createLink(controller: 'package', action: 'showHolidayHotelImage', params: [holidayHotelId: hotel?.id])}">
                                    </a>

                                    <div class="clearfix visible-sm"></div>

                                    <div class="media-body fnt-smaller">
                                        <a href="#" target="_parent"></a>

                                        <h4 class="media-heading">
                                            <a>${hotel?.hotelName}<strong><small
                                                    class="pull-right"
                                                    style="color: lightskyblue">${hotel?.type}</small>
                                            </strong></a></h4>


                                        <ul class="list-inline mrg-0 btm-mrg-10 clr-535353">
                                            <li><g:if
                                                    test="${(hotel?.packageHotelType == PackageHotelType.ONESTAR)}">
                                                <h5><small>Rating:</small>&nbsp;<g:each
                                                        in="${1..1}"
                                                        var="a"><img
                                                            src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                                                </h5>
                                            </g:if>
                                                <g:elseif
                                                        test="${(hotel?.packageHotelType == PackageHotelType.TWOSTAR)}">
                                                    <h5><small>Rating:</small>&nbsp;<g:each
                                                            in="${1..2}"
                                                            var="a"><img
                                                                src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                                                    </h5>
                                                </g:elseif>
                                                <g:elseif
                                                        test="${(hotel?.packageHotelType == PackageHotelType.THREESTAR)}">
                                                    <h5><small>Rating:</small>&nbsp;<g:each
                                                            in="${1..3}"
                                                            var="a"><img
                                                                src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                                                    </h5>
                                                </g:elseif>
                                                <g:elseif
                                                        test="${(hotel?.packageHotelType == PackageHotelType.FOURSTAR)}">
                                                    <h5><small>Rating:</small>&nbsp;<g:each
                                                            in="${1..4}"
                                                            var="a"><img
                                                                src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                                                    </h5>
                                                </g:elseif>
                                                <g:elseif
                                                        test="${(hotel?.packageHotelType == PackageHotelType.FIVESTAR)}">
                                                    <h5><small>Rating:</small>&nbsp;<g:each
                                                            in="${1..5}"
                                                            var="a"><img
                                                                src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                                                    </h5>
                                                </g:elseif>
                                            </li>

                                            %{--<li style="list-style: none">|</li>--}%

                                            %{--<li>${hotel?.type}</li>--}%

                                            %{--<li style="list-style: none">|</li>--}%

                                            <li></li>
                                        </ul>

                                        <p class="hidden-xs"><strong>Overview:</strong><strong class="col-md-offset-1"><br>${hotel.overview}</strong>
                                        </p><span
                                            class="fnt-smaller fnt-lighter fnt-arial"></span>
                                    </div>
                                </div>
                            </div><!-- End Listing-->
                            <hr>
                        </g:each>
                    </div>
                </div><!-- End row -->
            </g:each>
        </g:if>
        <g:else>
            <h3>No Hotel Found</h3>
        </g:else>
    </div><!-- End container -->
</div>