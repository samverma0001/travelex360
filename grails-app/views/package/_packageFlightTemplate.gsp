<g:if test="${packageFlightList}" style="margin-bottom: 15%">
    <g:each in="${packageFlightList}" var="packageFlight">
        <div class="nav navbar navbar-default" role="navigation" >
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="#"><strong
                                style="color: dodgerblue">${packageFlight?.flight?.departureFrom}</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
                                class="glyphicon glyphicon-plane"></span></a>
                        </li>
                        <li><a href="#"><strong
                                style="color: dodgerblue">${packageFlight?.flight?.destinationTo}</strong></a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row" >
            <p>
            <div class="col-md-3">${packageFlight?.flight?.flightName}</div>
        <div class="col-md-3">${packageFlight?.flight?.flightNumber}</div>

        <div class="col-md-3">${packageFlight?.flight?.departureFrom}</div>

        <div class="col-md-3" style="float: right">${packageFlight?.flight?.destinationTo}</div>
        </p>

        <p>

        <div class="col-md-3">${packageFlight?.flight?.departureDate?.format('MM/dd/yyyy')}</div>

        <div class="col-md-3">${packageFlight?.flight?.destinationDate?.format('MM/dd/yyyy')}</div>

        <div class="col-md-3" style="float: right">${packageFlight?.flight?.departureTime}</div>

        <div class="col-md-3" style="float: right">${packageFlight?.flight?.destinationTime}</div>
        </p>
        </div>
        <br>
    </g:each>
</g:if>
<g:else>
    <strong>Flight combination to be announced..</strong>
</g:else>