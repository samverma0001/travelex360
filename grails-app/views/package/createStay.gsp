<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="admin">
</head>

<body>
<div class="container">
    <travelx:renderAlert/>
    <g:hasErrors bean="${stayDetailCO}">
        <div class="alert alert-danger alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;
            </span><span class="sr-only">Close</span></button>

            <div id="errors" class="alert alert-error">
                <g:renderErrors bean="${stayDetailCO}"/>
            </div>
        </div>
    </g:hasErrors>
    <p><strong>Create stay for hotel</strong></p>
    <g:form controller="package" action="saveStayForPackage">
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-4">
                    <label>Country</label>
                </div>

                <div class="col-md-8 form-group input-group">
                    <input type="text" class="form-control" name="country">

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <label>State</label>
                </div>

                <div class="col-md-8">
                    <input type="text" class="form-control" name="state">
                </div>
            </div>

            <div class="row" style="padding-top: 4%">
                <div class="col-md-4">
                    <label>City</label>
                </div>

                <div class="col-md-8 input-group form-group">
                    <input type="text" class="form-control" name="city">

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">

            <div class="row">
                <div class="col-md-3">
                    <label>Location</label>
                </div>

                <div class="col-md-9 form-group input-group">
                    <input type="text" class="form-control" name="location">

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <label>No of night</label>
                </div>

                <div class="col-md-9 form-group input-group">
                    <input type="text" class="form-control" name="noOfNight">

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>
        </div>
        <g:hiddenField name="holidayPackageId" value="${holidayPackageId}"/>
        <div class="row">
            <input type="submit" name="saveAddress" class="btn btn-primary" value="Save Stay"/>
        </div>
    </g:form>
</div>

</body>
</html>