<%@ page import="com.travelex.admin.HolidayImage" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script src="${resource(dir: 'js', file: 'jquery.bxslider.min.js')}"></script>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'jquery.bxslider.css')}">
</head>

<body>
<br>
<br>
<br>
<br>
<br>

<div class="container" style="margin-bottom: 15%;color: #000000">
    <div class="row">
        <div class="span12">
            <div class="tab-content" style="margin-top: 1%">
                <div class="tab-pane active" id="Users">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#details" data-toggle="tab"><strong>Package Detail(s)</strong></a>
                        </li>
                        <li><a href="#list" data-toggle="tab"><strong>Hotel(s)</strong></a></li>
                        <li><a href="#flights" data-toggle="tab"><strong>Flight(s)</strong></a></li>
                        <li><a href="#itinerary" data-toggle="tab"><strong>Itinerary</strong></a></li>
                    </ul>

                    <div class="tab-content" style="margin-top: 1%">
                        <div class="tab-pane active" id="details">
                            <br/>

                            <div class="row">
                                <div class="col-md-2">
                                    <h4><strong>Gallery &rarr;</strong></h4>
                                </div>

                                <div class="col-md-4">
                                    <g:if test="${holidayImageList}">

                                        <ul class="bxslider">
                                            <g:each in="${holidayImageList}" var="holidayImage">
                                                <li><img height="200" width="400"
                                                         src="${createLink(controller: 'package', action: 'showHolidayImage', params: [holidayImageId: holidayImage?.id])}"/>
                                                </li>
                                            </g:each>

                                        </ul>
                                    </g:if>
                                    <g:else>
                                        <strong>Gallery not availbale</strong>
                                    </g:else>
                                </div>

                                <div class="col-md-6"></div>
                            </div>

                            <h4><strong>Overview :-</strong></h4>
                            <g:if test="${packageDetail?.overview}">
                                <pre class="col-md-offset-1">${packageDetail?.overview}</pre>
                            </g:if>
                            <g:else>
                                <pre>Overview not available</pre>
                            </g:else>
                            <br>

                            <h4><strong>Inclusion :-</strong></h4>
                            <g:if test="${packageDetail?.overview}">
                                <pre class="col-md-offset-1">${packageDetail?.inclusion}</pre>
                            </g:if>
                            <g:else>
                                <pre>Inclusions not available</pre>
                            </g:else>
                            <br>
                            <h4><strong>Exclusion :-</strong></h4>
                            <g:if test="${packageDetail?.overview}">
                                <pre class="col-md-offset-1">${packageDetail?.exclusion}</pre>
                            </g:if>
                            <g:else>
                                <pre>Exclusions not available</pre>
                            </g:else>
                            <br>
                            <h4><strong>Payment Policy :-</strong></h4>
                            <g:if test="${packageDetail?.overview}">
                                <pre class="col-md-offset-1">${packageDetail?.paymentPolicy}</pre>
                            </g:if>
                            <g:else>
                                <pre>Payment policy not available</pre>
                            </g:else>
                            <br>
                            <h4><strong>Cancellation Policy :-</strong></h4>
                            <g:if test="${packageDetail?.overview}">
                                <pre class="col-md-offset-1">${packageDetail?.cancellationPolicy}</pre>
                            </g:if>
                            <g:else>
                                <pre>Cancellation policy not available</pre>
                            </g:else>
                            <br>
                            <h4><strong>Terms & Conditions :-</strong></h4>
                            <g:if test="${packageDetail?.overview}">
                                <pre class="col-md-offset-1">${packageDetail?.termsCondition}</pre>
                            </g:if>
                            <g:else>
                                <pre>Terms & Conditions not available</pre>
                            </g:else>
                        </div>

                        <div class="tab-pane" id="list">
                            <g:render template="/package/packageHotelTemplate"
                                      model="[stayDetailList: stayDetailList]"/>
                        </div>

                        <div class="tab-pane" id="flights">
                            <g:render template="/package/packageFlightTemplate"
                                      model="[packageFlightList: packageFlightList]"/>
                        </div>

                        <div class="tab-pane" id="itinerary">
                            <g:render template="/package/packageItineraryTemplate"
                                      model="[itineraryList: itineraryList]"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.bxslider').bxSlider({
            auto: true,
            autoControls: true
        });
    });
</script>
</body>
</html>