<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="admin">
</head>

<body>
<div class="container">
    <travelx:renderAlert/>
    <g:hasErrors bean="${itineraryCO}">
        <div class="alert alert-danger alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;
            </span><span class="sr-only">Close</span></button>

            <div id="errors" class="alert alert-error">
                <g:renderErrors bean="${itineraryCO}"/>
            </div>
        </div>
    </g:hasErrors>
    <p><strong>Edit itinerary to this package</strong></p>
    <g:form controller="package" action="updateItineraryForPackage">
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-4">
                    <label>Day Heading</label>
                </div>

                <div class="col-md-8 form-group input-group">
                    <input type="text" class="form-control" name="dayHeading" value="${itinerary?.dayHeading}">

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <label>Day Meal</label>
                </div>

                <div class="col-md-8 form-group input-group">
                    <input type="text" class="form-control" name="dayMeal" value="${itinerary?.dayMeal}">

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <label>Day Number</label>
                </div>

                <div class="col-md-8">
                    <g:set var="noOfItinerary" value="${tempNight.toInteger()}"/>
                    <g:select class="form-control" from="${1..noOfItinerary + 1}" name="dayNumber"/>
                </div>
            </div>
        </div>

        <div class="col-md-8">

            <div class="row">
                <div class="col-md-3">
                    <label>Description</label>
                </div>

                <div class="col-md-9 form-group input-group">
                    <g:textArea name="description" value="${itinerary?.description}" class="form-control">

                    </g:textArea>

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>
        </div>
        <g:hiddenField name="itineraryId" value="${itinerary?.id}"/>
        <g:hiddenField name="holidayPackageId" value="${holidayPackage?.id}"/>

        <div class="row">
            <input type="submit" name="update" class="btn btn-primary col-md-offset-4" value="Update itinerary"/>
        </div>
    </g:form>
</div>

</body>
</html>