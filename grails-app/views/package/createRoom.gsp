<%@ page import="com.travelex.admin.holiday.HolidayHotelRoomBedType" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="admin">
</head>

<body>
<div content="container" style="margin-bottom: 5%">
    <g:hasErrors bean="${holidayHotelRoomVO}">
        <div class="alert alert-danger alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span></button>

            <div id="errors" class="alert alert-error">
                <g:renderErrors bean="${holidayHotelRoomVO}"/>
            </div>
        </div>
    </g:hasErrors>
    <g:form controller="package" action="saveRoom">
        <div class="col-md-4">
            <div class="row" style="padding: 1%">
                <div class="col-md-4">
                    <strong>Room category</strong>
                </div>

                <div class="col-md-8 input-group form-group">
                    <input type="text" class="form-control" name="roomCategory" value="${holidayHotelRoomVO?.roomCategory}"/>

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row" style="padding: 1%">
                <div class="col-md-4">
                    <strong>Room Price</strong>
                </div>

                <div class="col-md-8 form-group input-group">
                    <input type="text" class="form-control" name="roomPrice" value="${holidayHotelRoomVO?.roomPrice}"/>

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row" style="padding: 1%">
                <div class="col-md-4">
                    <strong>Bed type</strong>
                </div>

                <div class="col-md-8 input-group form-group">
                    <g:select name="bedType" class="form-control" from="${HolidayHotelRoomBedType.list()}"
                              value="${holidayHotelRoomVO?.holidayHotelRoomBedType}" optionKey="key"
                              optionValue="value"/>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="row">
                <div class="col-md-2">
                    <strong>Room amenities</strong>
                </div>

                <div class="col-md-10">
                    <g:textArea name="roomAmenities" class="form-control" value="${holidayHotelRoomVO?.roomAmenities}"
                                style="height: 150px"/>
                </div>
            </div>
        </div>
        <g:hiddenField name="holidayHotelId" value="${holidayHotelId}"/>
        <input type="submit" class="btn btn-primary" value="Save room">
    </g:form>
</div>
</body>
</html>