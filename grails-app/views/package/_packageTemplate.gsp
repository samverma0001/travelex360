<%@ page import="java.math.RoundingMode; com.travelex.admin.StayDetail; com.travelex.admin.HolidayHotelStayDetail" %>
<style>
/**** BASE ****/
body {
    color: #888;
}

a {
    color: #03a1d1;
    text-decoration: none !important;
}

/**** LAYOUT ****/
.list-inline > li {
    padding: 0 10px 0 0;
}

.container-pad {
    padding: 30px 15px;
}

/**** MODULE ****/
.bgc-fff {
    background-color: #fff !important;
}

.box-shad {
    -webkit-box-shadow: 1px 1px 0 rgba(0, 0, 0, .2);
    box-shadow: 1px 1px 0 rgba(0, 0, 0, .2);
}

.brdr {
    border: 1px solid #ededed;
}

/* Font changes */
.fnt-smaller {
    font-size: .9em;
}

.fnt-lighter {
    color: #bbb;
}

/* Padding - Margins */
.pad-10 {
    padding: 10px !important;
}

.mrg-0 {
    margin: 0 !important;
}

.btm-mrg-10 {
    margin-bottom: 10px !important;
}

.btm-mrg-20 {
    margin-bottom: 20px !important;
}

/* Color  */
.clr-535353 {
    color: #535353;
}

/**** MEDIA QUERIES ****/
@media only screen and (max-width: 991px) {
    #property-listings .property-listing {
        padding: 5px !important;
    }

    #property-listings .property-listing a {
        margin: 0;
    }

    #property-listings .property-listing .media-body {
        padding: 10px;
    }
}

@media only screen and (min-width: 992px) {
    #property-listings .property-listing img {
        max-width: 180px;
    }
}
</style>

<div class="container-fluid" style="background-color:#e8e8e8;border: 2px solid;color: #000000">
    <div class="container container-pad" id="property-listings">

        <g:if test="${holidayPackageList}">
            <g:each in="${holidayPackageList}" var="holidayPackage">
                <div class="row">
                    <div class="brdr bgc-fff pad-10 box-shad btm-mrg-20 property-listing">
                        <div class="media">
                            <a class="pull-left" href="#">
                                <img alt="image" class="img-responsive" width="260px" style="height: 175px"
                                     src="${createLink(controller: 'holiday', action: 'showHolidayPackageImage', params: [holidayPackageId: holidayPackage?.id])}">
                            </a>

                            <div class="clearfix visible-sm"></div>

                            <div class="media-body fnt-smaller">
                                <h4 class="media-heading">
                                    <a class="pull-left">${holidayPackage?.holidayName}</a>
                                    <h4><a href="${createLink(controller: 'package', action: 'moreDetails', params: [holidayPackageId: holidayPackage?.id])}"
                                           class="btn btn-primary pull-right">View details</a></h4>
                                </h4>
                                <br>
                                %{--<g:set var="totalNights"--}%
                                %{--value="${StayDetail.findByHolidayPackage(holidayPackage).noOfNight}"/>--}%
                                %{--<g:set var="noOfNight" value="${totalNights}"/>--}%

                                <p class="hidden-xs"><strong
                                        class="badge">&nbsp;&nbsp;Nights &rarr;</strong>&nbsp;|
                                    <g:set var="stayDetails"
                                           value="${StayDetail.findAllByHolidayPackage(holidayPackage)}"/>
                                    <g:each in="${stayDetails}" var="stayDetail">
                                        <strong>${stayDetail?.city}&nbsp;(${stayDetail.noOfNight}N)</strong>&nbsp;&nbsp;
                                    </g:each>
                                </p>


                                <p>
                                    <strong class="pull-left">Category : ${holidayPackage?.holidayCategory?.categoryName}</strong>


                                </p>
                                <br>
                                <br>
                                <br>
                                <br>

                                <p><strong class="pull-left">Inclusions:
                                    <span class="badge" style="background-color: cadetblue"><i
                                            class="glyphicon glyphicon-ok-circle"></i>&nbsp;Hotel</span>
                                    <span class="badge" style="background-color: cadetblue"><i
                                            class="glyphicon glyphicon-plane"></i>&nbsp;Flight</span>
                                    <span class="badge" style="background-color: cadetblue"><i
                                            class="glyphicon glyphicon-cutlery"></i>&nbsp;Food</span>
                                    <span class="badge" style="background-color: cadetblue"><i
                                            class="glyphicon glyphicon-facetime-video"></i>&nbsp;Sightseeing</span>
                                </strong>
                                    <strong class="pull-right"><h4
                                            style="color: dodgerblue">INR ${holidayPackage?.priceRange?.toBigDecimal()?.setScale(0, RoundingMode.HALF_UP)}</h4>

                                    </strong>
                                </p>
                                <br>
                                <br>

                                <p><strong class="pull-right" style="margin-top: -15px">Per Person</strong></p>
                            </div>
                        </div>
                    </div><!-- End Listing-->
                </div><!-- End row -->
            </g:each>
        </g:if>
        <g:else>
            <h3>Packages not available yet .</h3>
        </g:else>
    </div><!-- End container -->
</div>
