<%@ page import="com.travelex.socials.Enums.HotelType; com.travelex.admin.PackageHotelType" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="admin"/>
</head>

<body>
<div class="container" style="margin-bottom: 5%">
    <g:hasErrors bean="${packageHotelCO}">
        <div class="alert alert-danger alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;
            </span><span class="sr-only">Close</span></button>

            <div id="errors" class="alert alert-error">
                <g:renderErrors bean="${packageHotelCO}"/>
            </div>
        </div>
    </g:hasErrors>
    <p>

    <h3><strong>Create Hotel</strong></h3></p>
    <g:form controller="package" action="saveHotelForPackage" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-4">
                <div class="row" style="padding: 1%">
                    <div class="col-md-4">
                        <label>Hotel Name</label>
                    </div>

                    <div class="col-md-8 form-group input-group">
                        <input type="text" class="form-control" name="hotelName" value="${packageHotelCO?.hotelName}"/>

                        <div class="input-group-addon">
                            <travelx:star/>
                        </div>
                    </div>
                </div>
                <div class="row" style="padding: 1%">
                    <div class="col-md-4">
                        <label>Hotel Type</label>
                    </div>

                    <div class="col-md-8 form-group input-group">
                        <g:select name="packageHotelType" from="${PackageHotelType?.values()}"
                                  keys="${PackageHotelType.values()*.name()}"
                                  class="form-control"/>
                        <div class="input-group-addon">
                            <travelx:star/>
                        </div>
                    </div>
                </div>

                <div class="row" style="padding: 1%">
                    <div class="col-md-4">
                        <label>Category</label>
                    </div>

                    <div class="col-md-8 form-group input-group">
                        <g:select name="type" from="${HotelType?.values()}"
                                  keys="${HotelType.values()*.name()}"
                                  class="form-control"/>
                        <div class="input-group-addon">
                            <travelx:star/>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="row" style="padding: 1%">
                    <div class="col-md-4">
                        <label>Hotel Image</label>
                    </div>

                    <div class="col-md-8">
                        <input type="file" class="form-control" name="mainImageName"/>
                    </div>
                </div>
            </div>

            <div class="col-md-4">

            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <label>Overview</label>
            </div>

            <div class="col-md-10 form-group input-group">
                <g:textArea class="form-control" name="overview" value="${packageHotelCO?.overview}"/>

                <div class="input-group-addon">
                    <travelx:star/>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <label>Facilities</label>
            </div>

            <div class="col-md-10 form-group input-group">
                <g:textArea class="form-control" name="facilities" value="${packageHotelCO?.facilities}"/>

                <div class="input-group-addon">
                    <travelx:star/>
                </div>
            </div>
        </div>
        <br>

        <div class="row">
            <input type="submit" name="saveHotel" class="col-md-offset-2 btn btn-primary" value="Save Hotel"/>
        </div>
    </g:form>
</div>
</body>
</html>