<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="admin">
</head>

<body>
<travelx:adminUserTab/>
<travelx:renderAlert/>
<br>

<div class="tab-content" style="margin-top: 1%">
    <div class="tab-pane active" id="holiday" style="margin-left: 5%">
        <ul class="nav nav-tabs">
            <li><a href="${createLink(controller: 'holiday', action: 'listCategory')}">Holiday Category</a></li>
            <li><a href="${createLink(controller: "holiday", action: "index")}">Holiday</a></li>
            <li><a href="${createLink(controller: 'package', action: 'listHotel')}">Holiday Hotel</a></li>
            <li class="active"><a href="${createLink(controller: 'package', action: 'listFlight')}">Holiday Flight</a>
            </li>
        </ul>

        <div class="tab-content" style="margin-top: 1%">
            <div class="tab-pane active" id="editUser">
                <div class="col-md-12">
                    <div class="row">
                        <label>Flight List:-</label>
                        <a href="${createLink(controller: 'package', action: 'createFlight')}"
                           class="btn btn-warning pull-right">New flight</a>
                    </div>
                    <br>
                    <table class="table table-condensed table-bordered">
                        <thead>
                        <tr>
                            <th>Flight name</th>
                            <th>Flight No.</th>
                            <th>Departure from</th>
                            <th>Destination to</th>
                            <th>Departure date</th>
                            <th>Destination date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <g:if test="${flightList}">
                            <g:each in="${flightList}" var="flight">
                                <tbody>
                                <tr>
                                    <td><a href="${createLink(controller: 'package', action: 'showFlight', params: [flightId: flight?.id])}">${flight?.flightName}</a>
                                    </td>
                                    <td>${flight?.flightNumber}</td>
                                    <td>${flight?.departureFrom}</td>
                                    <td>${flight?.destinationTo}</td>
                                    <td>${flight?.departureDate?.format('MM/dd/yyyy')}</td>
                                    <td>${flight?.destinationDate?.format('MM/dd/yyyy')}</td>
                                    <td><a href="${createLink(controller: 'package', action: 'editFlight', params: [flightId: flight?.id])}"
                                           class="btn btn-warning">Edit</a>
                                        <a href="${createLink(controller: 'package', action: 'deleteFlight', params: [flightId: flight?.id])}"
                                           class="btn btn-danger">Delete</a>
                                        <g:if test="${holidayPackageId}">
                                            <a href="${createLink(controller: 'package', action: 'addFlightToPackage', params: [flightId: flight?.id, holidayPackageId: holidayPackageId])}"
                                               class="btn btn-success">Add to package</a>
                                        </g:if>
                                        <g:else>
                                        </g:else>
                                    </td>
                                </tr>
                                </tbody>
                            </g:each>
                        </g:if>
                        <g:else>
                            <strong>No hotel found</strong>
                        </g:else>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>