<%@ page import="com.travelex.admin.PackageHotelType" %>
<html>
<head>
    <meta name="layout" content="main"/>
</head>

<body>
<br>
<br>
<br>
<br>
<br>

<div class="container" style="margin-bottom: 20%;color: #000000">
    <div class="col-md-3">
        <h4>
            <strong>Filter Package</strong>
        </h4>

        <div class=" portlet-content">
            <g:form id="login-form" controller="hotelService" onsubmit="setValue()"
                    action="hotelListBasedOnRatingSubmit">
                <div class="row list-group-item">
                    %{--<h4>Rating Star</h4>--}%
                    <input type="checkbox" onclick="unCheckOther();
                    starRatingSubmit()" name="hotelStar" value="all" checked="true"
                           id="all"> &nbsp;All

                    <br>
                    <input type="checkbox" onclick="unCheckAll();
                    starRatingSubmit()" name="hotelStar" value="fiveStar" id="fiveStar">
                    <g:each in="${1..5}" var="a"><img
                            src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                    <br>
                    <input type="checkbox" onclick="unCheckAll();
                    starRatingSubmit()" name="hotelStar" value="fourStar" id="fourStar">
                    <g:each in="${1..4}" var="a"><img
                            src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                    <br>
                    <input type="checkbox" onclick="unCheckAll();
                    starRatingSubmit()" name="hotelStar" value="threeStar" id="threeStar">
                    <g:each in="${1..3}" var="a"><img
                            src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                    <br>

                    <input type="checkbox" onclick="unCheckAll();
                    starRatingSubmit()" name="hotelStar" value="twoStar" id="twoStar">
                    <g:each in="${1..2}" var="a"><img
                            src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                    <br>

                    <input type="checkbox" onclick="unCheckAll();
                    starRatingSubmit()" name="hotelStar" value="oneStar" id="oneStar">
                    <g:each in="${1..1}" var="a"><img
                            src="${resource(dir: '/assets/img', file: 'star1.png')}"></g:each>
                    <br>

                    <div>&nbsp;</div>
                    <input style="display: none" value="Modify Search" id="hotelStarList" type="submit"
                           class="btn btn-xs btn-success">
                </div>
                <!-- /.list-group -->
            </g:form>
            <div>&nbsp;</div>
        </div>

        <div class="row list-group-item">
            <h4><strong>To Know More/Book Packages <a href="" data-toggle="modal"
                                                      data-target="#clickHere">Click Here
            </a></strong></h4>

        </div>

    </div> <!-- /.list-group -->
    <div class="col-md-9">
        <g:render template="/package/packageTemplate" model="[holidayPackageList: holidayPackageList]"/>
    </div>
</div>

<div class="modal fade" id="clickHere" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="color: #000000">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title"
                    id="myModalLabel"><strong>To Book Package's / Find Out More Detail Please Call 9810396839 Or <a
                        href="mailto:info@travelx360.com">Mail us</a> !</strong></h4>
            </div>

            <g:form controller="holiday" action="submitQuery">
                <div class="modal-body" >
                    <p><h4><strong>You can also mail your query <a href="mailto:info@travelx360.com">info@travelx360.com</a> !</strong></h4></p>
                    <div class="row">
                        <div class="col-md-4"><label>Name</label></div>

                        <div class="col-md-8 input-group form-group">
                            <input type="text" class="form-control" name="name" required=""/>

                            <div class="input-group-addon">
                <travelx:star/>
                </div>
            </div>
        </div>

                <div class="row">
                    <div class="col-md-4"><label>Email</label></div>

                    <div class="col-md-8 input-group form-group">
                        <input type="email" class="form-control" name="email" required=""/>

                        <div class="input-group-addon">
                            <travelx:star/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4"><label>Contact No.</label></div>

                    <div class="col-md-8 input-group form-group">
                        <input type="text" class="form-control" name="contactNo" required=""/>

                        <div class="input-group-addon">
                            <travelx:star/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4"><label>Message</label></div>

                    <div class="col-md-8 input-group form-group">
                        <g:textArea name="message" class="form-control" required=""/>
                        <div class="input-group-addon">
                            <travelx:star/>
                        </div>
                    </div>
                </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success" value="Submit">
                    <input type="reset" class="btn btn-danger" value="Reset">

                </div>

            </g:form>
        </div>
    </div>
</div>

</body>
</html>