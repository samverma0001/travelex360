<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="admin">
</head>

<body>
<travelx:adminUserTab/>
<travelx:renderAlert/>
<br>

<p><a href="${createLink(controller: "package", action: "listHotel")}" class="btn btn-warning pull-right">Back</a></p>

<div class="container" style="margin-bottom: 10%">
    <table class="table table-bordered table-condensed">
        <tr style="background-color: #808080"><th colspan="12"><h4><strong
                class="text-center col-md-offset-5">Hotel detail(s)</strong></h4></th></tr>
        <tr>
            <th colspan="3">Hotel name</th>
            <td colspan="3">${holidayHotel?.hotelName}</td>
            <th colspan="3">Image</th>
            <th colspan="3">
                <g:if test="${holidayHotel?.imageName}">
                    <img src="${createLink(controller: 'package', action: 'showHolidayHotelImage', params: [holidayHotelId: holidayHotel?.id])}"
                         alt="hotel image"/>
                </g:if>
                <g:else>
                    Image not available
                </g:else>
            </th>
        </tr>
        <tr>
            <th colspan="3">Image type</th>
            <td colspan="3">${holidayHotel?.imageType}</td>
            <th colspan="3">Image name</th>
            <td colspan="3">${holidayHotel?.imageName}</td>
        </tr>
        <tr>
            <th colspan="3">Type</th>
            <td colspan="3">${holidayHotel?.type}</td>
            <th colspan="3">Category</th>
            <td colspan="3">${holidayHotel?.packageHotelType}</td>
        </tr>
        <tr>
            <th colspan="3">Date created</th>
            <td colspan="3">${holidayHotel?.dateCreated?.format('MM/dd/yyyy')}</td>
            <th colspan="3">Last updated</th>
            <td colspan="3">${holidayHotel?.lastUpdated?.format("MM/dd/yyyy")}</td>
        </tr>
    </table>

    <div class="row">
        <strong><h4><strong class="pull-left">Room detail(s)</strong></h4></strong>
        <a class="pull-right btn btn-primary"
           href="${createLink(controller: 'package', action: 'createRoom', params: [holidayHotelId: holidayHotel?.id])}">New Room</a>
    </div>
    <br>
    <g:if test="${holidayHotelRoomList}">
        <table class="table table-condensed table-bordered">
            <tr>
                <th>Room type</th>
                <th>Room price</th>
                <th>Bed type</th>
                <th>Date created</th>
                <th>Last updated</th>
                <th>Action</th>
            </tr>
            <g:each in="${holidayHotelRoomList}" var="hotelRoom">
                <tr>
                    <td>${hotelRoom?.roomCategory}</td>
                    <td>${hotelRoom?.roomPrice}</td>
                    <td>${hotelRoom?.holidayHotelRoomBedType}</td>
                    <td>${hotelRoom?.dateCreated?.format('MM/dd/yyyy')}</td>
                    <td>${hotelRoom?.lastUpdated?.format('MM/dd/yyyy')}</td>
                    <td><a href="${createLink(controller: 'package', action: 'editRoom', params: [roomId: hotelRoom?.id])}"
                           class="btn btn-xs btn-success">Edit</a>
                        <a href="${createLink(controller: 'package', action: 'deleteRoom', params: [roomId: hotelRoom?.id])}"
                           class="btn btn-xs btn-danger">Delete</a></td>
                </tr>
            </g:each>
        </table>
    </g:if>
    <g:else>
        <strong><h4>Room not availabel yet !</h4></strong>

    </g:else>

</div>
</body>
</html>