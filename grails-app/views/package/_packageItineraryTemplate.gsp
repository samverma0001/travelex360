<div class="row">
    <div class="col-md-12">
        <g:if test="${itineraryList}">
            <g:each in="${itineraryList}" var="packageItinerary">
                <p><strong class="pull-left"
                           style="color: dodgerblue">Day ${packageItinerary?.dayNumber}</strong>&nbsp;&nbsp;<strong>${packageItinerary?.dayHeading}</strong>
                </p>

                <p><strong class="pull-left"
                           style="color: dodgerblue">Meal:-</strong>&nbsp;&nbsp;${packageItinerary?.dayMeal}</p>

                <p class="col-md-offset-1">${packageItinerary?.description}</p>
                <hr>
            </g:each>
        </g:if>
        <g:else>
        </g:else>
    </div>
</div>