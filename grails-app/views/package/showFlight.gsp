<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="admin">
</head>

<body>
<travelx:adminUserTab/>
<travelx:renderAlert/>

<p><a href="${createLink(controller: "package", action: "listFlight")}" class="btn btn-warning pull-right">Back</a></p>

<div class="container">
    <table class="table table-bordered table-condensed">
        <tr style="background-color: #808080"><th colspan="12"><h4><strong
                class="text-center col-md-offset-5">Flight detail(s)</strong></h4></th></tr>
        <tr>
            <th colspan="3">Flight name</th>
            <td colspan="3">${flight?.flightName}</td>
            <th colspan="3">Flight number</th>
            <td colspan="3">${flight?.flightNumber}</td>
        </tr>
        <tr>
            <th colspan="3">Departure from</th>
            <td colspan="3">${flight?.departureFrom}</td>
            <th colspan="3">Destination to</th>
            <td colspan="3">${flight?.destinationTo}</td>
        </tr>
        <tr>
            <th colspan="3">departureDate</th>
            <td colspan="3">${flight?.departureDate?.format("MM/dd/yyyy")}</td>
            <th colspan="3">destinationDate</th>
            <td colspan="3">${flight?.destinationDate?.format("MM/dd/yyyy")}</td>
        </tr>
        <tr>
            <th colspan="3">departureTime</th>
            <td colspan="3">${flight?.departureTime}</td>
            <th colspan="3">destinationTime</th>
            <td colspan="3">${flight?.destinationTime}</td>
        </tr>
        <tr>
            <th colspan="3">dateCreated</th>
            <td colspan="3">${flight?.dateCreated?.format("MM/dd/yyyy")}</td>
            <th colspan="3">lastUpdated</th>
            <td colspan="3">${flight?.lastUpdated?.format("MM/dd/yyyy")}</td>
        </tr>

    </table>
</div>

</body>
</html>