<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="admin">
</head>

<body>
<travelx:adminUserTab/>
<travelx:renderAlert/>
<br>

<div class="tab-content" style="margin-top: 1%;margin-bottom: 10%">
    <div class="tab-pane active" id="holiday" style="margin-left: 5%">
        <ul class="nav nav-tabs">
            <li><a href="${createLink(controller: 'holiday', action: 'listCategory')}">Holiday Category</a></li>
            <li><a href="${createLink(controller: "holiday", action: "index")}">Holiday</a></li>
            <li class="active"><a href="${createLink(controller: 'package', action: 'listHotel')}">Holiday Hotel</a>
            </li>
            <li><a href="${createLink(controller: "package", action: 'listFlight')}">Holiday Flight</a>
            </li>

        </ul>

        <div class="tab-content" style="margin-top: 1%">
            <div class="tab-pane active" id="editUser">
                <div class="col-md-12">
                    <div class="row">
                        <label>Hotel List:-</label>
                        <a href="${createLink(controller: 'package', action: 'createHotel')}"
                           class="btn btn-warning pull-right">New hotel</a>
                    </div>
                    <br>
                    <table class="table table-condensed table-bordered">
                        <thead>
                        <tr>
                            <th>Hotel name</th>
                            <th>Type</th>
                            <th>Category</th>
                            <th>Date created</th>
                            <th>Last updated</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <g:if test="${holidayHotelList}">
                            <g:each in="${holidayHotelList}" var="holidayHotel">
                                <tbody>
                                <tr>
                                    <td><a href="${createLink(controller: 'package', action: 'showHotel', params: [holidayHotelId: holidayHotel?.id])}">${holidayHotel?.hotelName}</a>
                                    </td>
                                    <td>${holidayHotel?.type}</td>
                                    <td>${holidayHotel?.packageHotelType}</td>
                                    <td>${holidayHotel?.dateCreated?.format("MM/dd/yyyy")}</td>
                                    <td>${holidayHotel?.lastUpdated?.format("MM/dd/yyyy")}</td>
                                    <td><a href="${createLink(controller: 'package', action: 'editHotel', params: [holidayHotelId: holidayHotel?.id])}"
                                           class="btn btn-warning">Edit</a>
                                        <a href="${createLink(controller: 'package', action: 'deleteHotel', params: [holidayHotelId: holidayHotel?.id])}"
                                           class="btn btn-danger">Delete</a>
                                        <g:if test="${stayId}">
                                            <a href="${createLink(controller: 'package', action: 'addHotelToPackage', params: [holidayHotelId: holidayHotel?.id, stayId: stayId])}"
                                               class="btn btn-success">Add to package</a>
                                        </g:if>
                                        <g:else>
                                        </g:else>
                                        <a class="btn btn-primary"
                                           href="${createLink(controller: 'package', action: 'createRoom', params: [holidayHotelId: holidayHotel?.id])}">Create Room</a>
                                    </td>

                                </tr>
                                </tbody>
                            </g:each>
                        </g:if>
                        <g:else>
                            <strong>No hotel found</strong>
                        </g:else>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>