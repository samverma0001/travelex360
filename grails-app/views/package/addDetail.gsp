<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="admin">
    <script src="${resource(dir: "js", file: "jquery.MultiFile.js")}"></script>

</head>

<body>
<div class="container" style="margin-bottom: 10%">
    <g:hasErrors bean="${packageDetailCO}">
        <div class="alert alert-danger alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;
            </span><span class="sr-only">Close</span></button>

            <div id="errors" class="alert alert-error">
                <g:renderErrors bean="${packageDetailCO}"/>
            </div>
        </div>
    </g:hasErrors>
    <p><strong>Add detail(s) to this package</strong></p>

    <g:form controller="package" action="saveDetailForPackage" enctype="multipart/form-data" method="post">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <label>Images</label>
                </div>

                <div class="col-md-9">
                    <input type="file" class="form-control multi" name="holidayImages" maxlength="5"/>
                </div>
            </div>
            <br/>

            <div class="row">
                <div class="col-md-3">
                    <label>Overview</label>
                </div>

                <div class="col-md-9 form-group input-group">
                    <g:textArea class="form-control" name="overview" value="${packageDetailCO?.overview}"/>

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <label>Inclusions</label>
                </div>

                <div class="col-md-9 form-group input-group">
                    <g:textArea class="form-control" name="inclusion" value="${packageDetailCO?.inclusion}"/>


                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <label>Exclusion</label>
                </div>

                <div class="col-md-9 form-group input-group">
                    <g:textArea class="form-control" name="exclusion" value="${packageDetailCO?.exclusion}"/>


                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <label>Payment Policy</label>
                </div>

                <div class="col-md-9 form-group input-group">
                    <g:textArea class="form-control" name="paymentPolicy" value="${packageDetailCO?.paymentPolicy}"/>


                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <label>Cancellation Policy</label>
                </div>

                <div class="col-md-9 form-group input-group">
                    <g:textArea class="form-control" name="cancellationPolicy"
                                value="${packageDetailCO?.cancellationPolicy}"/>


                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <label>Terms & Cond.</label>
                </div>

                <div class="col-md-9 form-group input-group">
                    <g:textArea class="form-control" name="termsCondition" value="${packageDetailCO?.termsCondition}"/>


                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>
        </div>
        <g:hiddenField name="holidayPackageId" value="${holidayPackage?.id}"/>
        <div class="row">
            <input type="submit" name="saveDetail" class="btn btn-primary" value="Save detail"/>
        </div>
    </g:form>
</div>
</body>
</html>