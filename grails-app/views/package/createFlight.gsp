<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="admin">
    <script src="${resource(dir: 'js', file: 'bootstrap-datepicker.js')}"></script>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'datepicker.css')}">
</head>

<body>

<div class="container">
    <g:hasErrors bean="${flightCO}">
        <div class="alert alert-danger alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;
            </span><span class="sr-only">Close</span></button>

            <div id="errors" class="alert alert-error">
                <g:renderErrors bean="${flightCO}"></g:renderErrors>
            </div>
        </div>
    </g:hasErrors>
    <p><strong>Add flight to this package</strong></p>
    <g:form controller="package" action="saveFlightForPackage">
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-4">
                    <label>Flight Name</label>
                </div>

                <div class="col-md-8 form-group input-group">
                    <input type="text" class="form-control" name="flightName" value="${flightCO?.flightName}"/>

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <label>Flight Number</label>
                </div>

                <div class="col-md-8 form-group input-group">
                    <input type="text" class="form-control" name="flightNumber" value="${flightCO?.flightNumber}"/>

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <label>Departure From</label>
                </div>

                <div class="col-md-8 form-group input-group">
                    <input type="text" class="form-control" name="departureFrom" value="${flightCO?.departureFrom}"/>

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <label>Destination To</label>
                </div>

                <div class="col-md-8 form-group input-group">
                    <input type="text" class="form-control" name="destinationTo" value="${flightCO?.destinationTo}"/>

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-4">
            <div class="row">
                <div class="col-md-4">
                    <label>Departure Date</label>
                </div>

                <div class="col-md-8 form-group input-group">
                    <input class="form-control" autocomplete="off" style="cursor: pointer" name="departureDate"
                           type="text" value="${flightCO?.departureDate}" id="departureDate"
                           onclick="showDatePicker('departureDate')"/>

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-4">
                    <label>Destination Date</label>
                </div>

                <div class="col-md-8 form-group input-group">
                    <input type="text" class="form-control" name="destinationDate" autocomplete="off"
                           style="cursor: pointer" id="destinationDate" onclick="showDatePicker('destinationDate')"
                           value="${flightCO?.destinationDate}"/>

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-4">
                    <label>Departure Time</label>
                </div>

                <div class="col-md-8 form-group input-group">
                    <input type="text" class="form-control" name="departureTime" value="${flightCO?.departureTime}"/>

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-4">
                    <label>Destination Time</label>
                </div>

                <div class="col-md-8 form-group input-group">
                    <input type="text" class="form-control" name="destinationTime"
                           value="${flightCO?.destinationTime}"/>

                    <div class="input-group-addon">
                        <travelx:star/>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
        </div>
        <div class="row">
            <input type="submit" name="saveFlight" class="btn btn-primary" value="Save Flight"/>
        </div>
    </g:form>
</div>
<script>
    function showDatePicker(type) {
        if (type == 'departureDate') {
            $("#departureDate").datepicker('show');
        }
        if (type == 'destinationDate') {
            $("#destinationDate").datepicker('show');
        }
    }

    $(document).ready(function () {
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#departureDate').datepicker({
            onRender: function (date) {
                return date.valueOf() < now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function (ev) {
            if (ev.date.valueOf() > checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
            }
            checkin.hide();
            $('#destinationDate')[0].focus();
        }).data('datepicker');
        var checkout = $('#destinationDate').datepicker({
            endDate: '+0d',
            onRender: function (date) {
                return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function (ev) {
            if (ev.date.valueOf() > checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
            }
            checkout.hide();
        }).data('datepicker');
    })


</script>
</body>
</html>