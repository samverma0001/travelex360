<div class="container">
    <div class="row">
        <div class="span12">
            <div id="tab" class="btn-group">
                <a href="${createLink(controller: 'user', action: 'index')}" id="userId"
                   class="btn btn-large btn-info  disableAll"
                   onclick="activeButton(this.id)">Users</a>
                <a href="${createLink(controller: 'holiday', action: 'listCategory')}" id="holidayId"
                   class="btn btn-large btn-info"
                   onclick="activeButton(this.id)">Holidays</a>
                <a href="${createLink(controller: 'bookingHistory', action: 'index')}" id="hotelId"
                   class="btn btn-large btn-info disableAll"
                   onclick="activeButton(this.id)">Hotels</a>
                <a href="#Flights" id="flightId" class="btn btn-large btn-info disableAll" data-toggle="tab"
                   onclick="activeButton(this.id)">Flights</a>
                <a href="${createLink(controller: 'activityCategory', action: 'index')}" id="activityId"
                   class="btn btn-large btn-info  disableAll"
                   onclick="activeButton(this.id)">Activity</a>

                <a href="#serviceProvider" id="serviceProviderId" class="btn btn-large btn-info disableAll"
                   data-toggle="tab"
                   onclick="activeButton(this.id)">Service Provider</a>
            </div>
        </div>
    </div>
</div>
<script>
    function activeButton(id) {
        $("#" + id).addClass('active');
    }
</script>