<%@ page import="com.travelex.User.PaymentInfo" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin"/>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="span12">
            <div id="tab" class="btn-group">
                <a href="${createLink(controller: 'user', action: 'index')}" id="userId"
                   class="btn btn-large btn-info  disableAll"
                   onclick="activeButton(this.id)">Users</a>
                <a href="${createLink(controller: 'bookingHistory', action: 'index')}" id="hotelId"
                   class="btn btn-large btn-info active disableAll"
                   onclick="activeButton(this.id)">Hotels</a>
                <a href="#Flights" id="flightId" class="btn btn-large btn-info disableAll" data-toggle="tab"
                   onclick="activeButton(this.id)">Flights</a>
                <a href="#Activity" id="activityId" class="btn btn-large btn-info disableAll" data-toggle="tab"
                   onclick="activeButton(this.id)">Activity</a>
                <a href="#serviceProvider" id="serviceProviderId" class="btn btn-large btn-info disableAll"
                   data-toggle="tab"
                   onclick="activeButton(this.id)">Service Provider</a>

            </div>


            <div class="tab-content" style="margin-top: 1%">

                <div class="tab-pane active" id="Hotels">

                    <ul class="nav nav-tabs">
                        <li class="active"><a
                                href="${createLink(controller: 'paymentInfo', action: 'index')}">Detail</a></li>
                    </ul>

                    <div class="tab-content" style="margin-top: 1%">
                        <div class="tab-pane active">
                            <table class="table table-bordered table-condensed">
                                <thead class="text-center" style="background-color: #808080"><tr><td
                                        colspan="12"><h4>Show Details</h4></td>
                                </tr>
                                </thead>
                                <tr>
                                    <th>Payu Money Id</th>
                                    <td>${paymentInfoInstance?.payuMoneyId}</td>
                                    <th>Payment Mode</th>
                                    <td>${paymentInfoInstance?.paymentMode}</td>
                                </tr>
                                <tr>
                                    <th>Payment Status</th>
                                    <td>${paymentInfoInstance?.paymentStatus}</td>
                                    <th>Merchant Key</th>
                                    <td>${paymentInfoInstance?.merchantKey}</td>
                                </tr>
                                <tr>
                                    <th>Payment Trancation Id</th>
                                    <td>${paymentInfoInstance?.paymentTxnId}</td>
                                    <th>Amount</th>
                                    <td>${paymentInfoInstance?.amount}</td>
                                </tr>
                                <tr>
                                    <th>Discount</th>
                                    <td>${paymentInfoInstance?.discount}</td>
                                    <th>First Name</th>
                                    <td>${paymentInfoInstance?.fName}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>${paymentInfoInstance?.email}</td>
                                    <th>User Card Number</th>
                                    <td>${paymentInfoInstance?.userCardNumber}</td>
                                </tr>
                                <tr>
                                    <th>Mihpayid</th>
                                    <td>${paymentInfoInstance?.mihpayid}</td>
                                    <th>Product Info</th>
                                    <td>${paymentInfoInstance?.productinfo}</td>
                                </tr>
                                <tr>
                                    <th>Booking History</th>
                                    <td>${paymentInfoInstance?.bookingHistory}</td>
                                    <th>Date Created</th>
                                    <td>${paymentInfoInstance?.dateCreated?.format("dd/MM/yyyy")}</td>
                                </tr>
                                <tr>
                                    <th>Last Updated</th>
                                    <td>${paymentInfoInstance?.lastUpdated?.format("dd/MM/yyyy")}</td>
                                    <th></th>
                                    <td><g:form url="[resource: paymentInfoInstance, action: 'delete']" method="DELETE">
                                        <fieldset class="buttons">
                                            <g:actionSubmit class="delete btn btn-xs btn-danger" action="delete"
                                                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                                        </fieldset>
                                    </g:form>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
