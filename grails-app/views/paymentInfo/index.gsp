<%@ page import="com.travelex.User.PaymentInfo" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin">
</head>

<body>

<div class="container">
    <div class="row">
        <div class="span12">
            <div id="tab" class="btn-group">
                <a href="${createLink(controller: 'user', action: 'index')}" id="userId"
                   class="btn btn-large btn-info  disableAll"
                   onclick="activeButton(this.id)">Users</a>
                <a href="${createLink(controller: 'bookingHistory', action: 'index')}" id="hotelId"
                   class="btn btn-large btn-info active disableAll"
                   onclick="activeButton(this.id)">Hotels</a>
                <a href="#Flights" id="flightId" class="btn btn-large btn-info disableAll" data-toggle="tab"
                   onclick="activeButton(this.id)">Flights</a>
                <a href="#Activity" id="activityId" class="btn btn-large btn-info disableAll" data-toggle="tab"
                   onclick="activeButton(this.id)">Activity</a>
                <a href="#serviceProvider" id="serviceProviderId" class="btn btn-large btn-info disableAll"
                   data-toggle="tab"
                   onclick="activeButton(this.id)">Service Provider</a>

            </div>


            <div class="tab-content" style="margin-top: 1%">

                <div class="tab-pane active" id="Hotels">

                    <ul class="nav nav-tabs">
                        <li><a
                                href="${createLink(controller: 'bookingHistory', action: 'index')}">Booking List</a>
                        </li>
                        <li><a
                                href="${createLink(controller: 'coupon', action: 'index')}">Coupon List</a></li>
                        <li class="active"><a
                                href="${createLink(controller: 'paymentInfo', action: 'index')}">Payment List</a></li>
                        <li><a href="${createLink(controller: 'setMargin', action: 'index')}">Margin List</a></li>
                    </ul>

                    <div class="tab-content" style="margin-top: 1%">
                        <div class="tab-pane active">
                            <table class="table table-bordered table-condensed">
                                <thead>
                                <tr>

                                    <g:sortableColumn property="payuMoneyId"
                                                      title="${message(code: 'paymentInfo.payuMoneyId.label', default: 'Payu Money Id')}"/>

                                    <g:sortableColumn property="paymentMode"
                                                      title="${message(code: 'paymentInfo.paymentMode.label', default: 'Payment Mode')}"/>

                                    <g:sortableColumn property="paymentStatus"
                                                      title="${message(code: 'paymentInfo.paymentStatus.label', default: 'Payment Status')}"/>

                                    <g:sortableColumn property="merchantKey"
                                                      title="${message(code: 'paymentInfo.merchantKey.label', default: 'Merchant Key')}"/>

                                    <g:sortableColumn property="paymentTxnId"
                                                      title="${message(code: 'paymentInfo.paymentTxnId.label', default: 'Payment Txn Id')}"/>

                                    <g:sortableColumn property="amount"
                                                      title="${message(code: 'paymentInfo.amount.label', default: 'Amount')}"/>

                                </tr>
                                </thead>
                                <tbody>
                                <g:each in="${paymentInfoInstanceList}" status="i" var="paymentInfoInstance">
                                    <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                                        <td><g:link action="show"
                                                    id="${paymentInfoInstance.id}">${fieldValue(bean: paymentInfoInstance, field: "payuMoneyId")}</g:link></td>

                                        <td>${fieldValue(bean: paymentInfoInstance, field: "paymentMode")}</td>

                                        <td>${fieldValue(bean: paymentInfoInstance, field: "paymentStatus")}</td>

                                        <td>${fieldValue(bean: paymentInfoInstance, field: "merchantKey")}</td>

                                        <td>${fieldValue(bean: paymentInfoInstance, field: "paymentTxnId")}</td>

                                        <td>${fieldValue(bean: paymentInfoInstance, field: "amount")}</td>

                                    </tr>
                                </g:each>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
