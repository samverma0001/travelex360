package com.travelex.admin

class HolidayQuery {

    String name
    String email
    String contactNo
    String message
    Date dateCreated
    Date lastUpdated

    static mapping = {
        message type: 'text'
    }

    static constraints = {
        name(nullable: false, blank: false)
        email(nullable: false, blank: false)
        contactNo(nullable: false, blank: false)
        message(nullable: false, blank: false)
    }


}
