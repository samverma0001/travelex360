package com.travelex.admin

class Itinerary {

    String dayHeading
    String dayMeal
    String description
    String dayNumber
    Date dateCreated
    Date lastUpdated


    static belongsTo = [holidayPackage: HolidayPackage]


    static mapping = {
        description type: 'text'
    }


    static constraints = {
        holidayPackage(nullable: false)
        dayHeading(nullable: false, blank: false)
        dayMeal(nullable: false, blank: false)
        description(nullable: false, blank: false)
        dayNumber(nullable: false, blank: false)
    }


}
