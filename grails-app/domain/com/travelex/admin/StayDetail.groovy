package com.travelex.admin

class StayDetail {

    String country
    String state
    String city
    String location
    String noOfNight
    Date dateCreated
    Date lastUpdated

    static hasMany = [holidayHotel: HolidayHotel]
    static belongsTo = [holidayPackage: HolidayPackage]

    static constraints = {
        holidayPackage(nullable: false)
        country(nullable: false, blank: false)
        state(nullable: false, blank: false)
        city(nullable: false, blank: false)
        location(nullable: false, blank: false)
        noOfNight(nullable: false, blank: false)

    }


}
