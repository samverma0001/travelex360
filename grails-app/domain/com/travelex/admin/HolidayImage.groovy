package com.travelex.admin


class HolidayImage {

    String imageName
    String imageType
    Date dateCreated
    Date lastUpdated

    static belongsTo = [holidayPackage: HolidayPackage]

    static constraints = {
        imageName(nullable: false, blank: false)
        imageType(nullable: false, blank: false)
        holidayPackage(nullable: false)
    }


}
