package com.travelex.admin

class PackageDetail {

    String overview
    String inclusion
    String exclusion
    String paymentPolicy
    String cancellationPolicy
    String termsCondition

    Date dateCreated
    Date lastUpdated


    static mapping = {
        overview type: 'text'
        inclusion type: 'text'
        exclusion type: 'text'
        paymentPolicy type: 'text'
        cancellationPolicy type: 'text'
        termsCondition type: 'text'
    }


    static belongsTo = [holidayPackage: HolidayPackage]


    static constraints = {

        overview(nullable: false, blank: false)
        inclusion(nullable: true, blank: true)
        exclusion(nullable: true, blank: true)
        paymentPolicy(nullable: false, blank: false)
        cancellationPolicy(nullable: false, blank: false)
        termsCondition(nullable: false, blank: false)


    }


}
