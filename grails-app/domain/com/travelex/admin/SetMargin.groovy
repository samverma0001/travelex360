package com.travelex.admin

class SetMargin {

    String marginMoney
    String payUMoneyStake
    Status status = Status.ACTIVE
    AmountType amountType = AmountType.AMOUNT
    MarginType marginType = MarginType.DOMHOTEL

    Date validFrom
    Date validTo

    Date dateCreated
    Date lastUpdated

    static constraints = {

        marginMoney(nullable: false, blank: false)
        validFrom(nullable: false, blank: false)
        validTo(nullable: false, blank: false)
        payUMoneyStake(nullable: false, blank: false)

    }
}
