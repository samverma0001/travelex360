package com.travelex.admin

import com.travelex.admin.holiday.HolidayStatus
import com.travelex.admin.holiday.HolidayType

class HolidayPackage {

    String holidayName
    String holidayImageName
    String holidayImageType
    BigDecimal priceRange
    String totalSeat
    Boolean visaIncluded = false
    Boolean visaOnArrival = false
    Boolean travelInsurance = false
    Date startDate
    Date endDate
    Date dateCreated
    Date lastUpdated
    HolidayStatus holidayStatus = HolidayStatus.ACTIVE
    HolidayType holidayType = HolidayType.DOMESTIC

    static hasMany = [stayDetails: StayDetail, holidayFlight: Flight, itinerary: Itinerary, holidayImages: HolidayImage]

    static belongsTo = [holidayCategory: HolidayCategory]

    static hasOne = [packageDetail: PackageDetail]

    static constraints = {
        packageDetail(nullable: true)
        holidayCategory(nullable: false)
        holidayName(nullable: false, blank: false)
        holidayImageName(nullable: true, blank: true)
        holidayImageType(nullable: true, blank: true)
        priceRange(nullable: false)
        totalSeat(nullable: false, blank: false)
        startDate(nullable: false)
        endDate(nullable: false)
    }


}
