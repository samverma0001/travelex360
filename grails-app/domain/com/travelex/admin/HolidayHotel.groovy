package com.travelex.admin

import com.travelex.socials.Enums.HotelType

class HolidayHotel {

    String hotelName
    String imageName
    String imageType
    HotelType type = HotelType.RESORTS_AND_PLACES
    PackageHotelType packageHotelType = PackageHotelType.THREESTAR
    String overview
    String facilities
    Date dateCreated
    Date lastUpdated

    static constraints = {
        hotelName(nullable: false, blank: false)
        overview(nullable: false, blank: false)
        facilities(nullable: false, blank: false)
        imageName(nullable: true)
        imageType(nullable: true)
    }

    static hasMany = [holidayHotelRoom:HolidayHotelRoom]



    static mapping = {
        overview type: 'text'
        facilities type: 'text'
    }


}
