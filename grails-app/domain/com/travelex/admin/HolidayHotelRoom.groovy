package com.travelex.admin

import com.travelex.admin.holiday.HolidayHotelRoomBedType

class HolidayHotelRoom {

    String roomCategory
    BigDecimal roomPrice
    String roomAmenities
    HolidayHotelRoomBedType holidayHotelRoomBedType
    Date dateCreated
    Date lastUpdated
    static belongsTo = [holidayHotel: HolidayHotel]


    static constraints = {
        holidayHotel(nullable: true)
        roomCategory(nullable: false, blank: false)
        roomPrice(nullable: false, blank: false)
        holidayHotelRoomBedType(nullable: false)
        roomAmenities(nullable: true, blank: true)
    }

    static mapping = {
        roomAmenities type: 'text'
    }


}
