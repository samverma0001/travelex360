package com.travelex.admin

class Coupon {
    String couponCode
    AmountType amountType = AmountType.AMOUNT
    Status status = Status.ACTIVE
    String amount
    Date dateCreated
    Date lastUpdated
    String validNoOfDays

    static constraints = {


    }
}
