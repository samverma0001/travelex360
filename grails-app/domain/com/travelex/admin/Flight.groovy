package com.travelex.admin

class Flight {

    String flightName
    String flightNumber
    String departureFrom
    String destinationTo
    Date departureDate
    Date destinationDate
    String departureTime
    String destinationTime

    Date dateCreated
    Date lastUpdated

    static constraints = {

        flightName(nullable: false, blank: false)
        flightNumber(nullable: false, blank: false)
        departureFrom(nullable: false, blank: false)
        destinationTo(nullable: false, blank: false)
        departureDate(nullable: false, blank: false)
        destinationDate(nullable: false, blank: false)
        departureTime(nullable: false, blank: false)
        destinationTime(nullable: false, blank: false)

    }


}
