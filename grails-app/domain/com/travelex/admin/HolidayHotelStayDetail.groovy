package com.travelex.admin

class HolidayHotelStayDetail {

    HolidayHotel holidayHotel
    StayDetail stayDetail
    Date dateCreated
    Date lastUpdated

    static constraints = {
        holidayHotel(nullable: false)
        stayDetail(nullable: false)
    }


}
