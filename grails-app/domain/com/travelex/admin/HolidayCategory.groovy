package com.travelex.admin

class HolidayCategory {

    String categoryName
    String imageName
    String imageType
    Date dateCreated
    Date lastUpdated


    static hasMany = [holidayPackage: HolidayPackage]

    static constraints = {
        holidayPackage(nullable: true)
        categoryName(nullable: false, blank: false)
        imageName(nullable: true, blank: true)
        imageType(nullable: true, blank: true)
    }


}
