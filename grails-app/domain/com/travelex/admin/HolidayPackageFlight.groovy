package com.travelex.admin

class HolidayPackageFlight {

    HolidayPackage holidayPackage
    Flight flight
    Date dateCreated
    Date lastUpdated

    static constraints = {
        holidayPackage(nullable: false)
        flight(nullable: false)
    }

}
