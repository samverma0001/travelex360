package com.travelex.booking

import com.travelex.Hotel.BookingHistory

class Adult {

    String adultTitle
    String adultFirstName
    String adultLastName
    Boolean laedPassanger

    Date dateCreated
    Date lastUpdated

    static belongsTo = [roomBooked: RoomBooked]

    static constraints = {

        laedPassanger(nullable: false)
        adultTitle(nullable: false, blank: false)
        adultFirstName(nullable: false, blank: false)
        adultLastName(nullable: false, blank: false)
        roomBooked(nullable: false, blank: false)

    }


}
