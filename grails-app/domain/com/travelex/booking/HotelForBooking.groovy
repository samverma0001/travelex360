package com.travelex.booking

import com.travelex.Hotel.BookingHistory

import java.text.SimpleDateFormat


class HotelForBooking {

    String hotelName
    String hotelAddress
    String map
    String rating
    Date dateCreated
    Date lastUpdated
    BookingHistory bookingHistory

    static hasMany = [roomBooked: RoomBooked]

    static constraints = {

        hotelName(nullable: false, blank: false)
        hotelAddress(nullable: false, blank: false)
        map(nullable: false, blank: false)
        rating(nullable: false, blank: false)

    }

    def saveHotelDetails(HotelForBooking hotelForBooking, def bookingResult) {

        hotelForBooking.hotelName = bookingResult.HotelName
        hotelForBooking.hotelAddress = bookingResult.HotelAddress1
        hotelForBooking.map = bookingResult.Map
        hotelForBooking.rating = bookingResult.Rating
        return hotelForBooking
    }


}
