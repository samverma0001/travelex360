package com.travelex.booking

import com.travelex.Hotel.BookingHistory


class RoomBooked {

    String roomCategory
    String cancelPolicy
    Date lastCancellationDate

    Date dateCreated
    Date lastUpdated

    static belongsTo = [hotelForBooking: HotelForBooking]
    static hasMany = [adults: Adult, child: Child]

    static constraints = {

        roomCategory(nullable: false, blank: false)
        cancelPolicy(nullable: false, blank: false)
        lastCancellationDate(nullable: false)

    }


}
