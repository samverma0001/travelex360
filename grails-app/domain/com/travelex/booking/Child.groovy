package com.travelex.booking


class Child {

    String childTitle
    String childFirstName
    String childLastName

    Date dateCreated
    Date lastUpdated

    static belongsTo = [roomBooked: RoomBooked]

    static constraints = {

        childTitle(nullable: false, blank: false)
        childFirstName(nullable: false, blank: false)
        childLastName(nullable: false, blank: false)
        roomBooked(nullable: false, blank: false)

    }


}
