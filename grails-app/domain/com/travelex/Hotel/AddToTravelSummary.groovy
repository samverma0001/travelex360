package com.travelex.Hotel

import com.travelex.User.User

class AddToTravelSummary {
    String hotelName
    String hotelId
    String address
    String roomType
    String travelStartDate
    String travelEndDate
    String addedOnDate
    String addedByUser
    String noOfRooms
    String roomCatId
    String occupantAdult
    String occupantChild
    String price


    static belongsTo = [user: User]

    static constraints = {
        hotelName(nallable: false)
        hotelId(nallable: false)
        address(nallable: true)
        roomType(nallable: false)
        travelStartDate(nallable: false, blank: false)
        travelEndDate(nallable: false, blank: false)
        addedByUser(nallable: false, blank: false)
        addedOnDate(nallable: false, blank: false)

    }

}
