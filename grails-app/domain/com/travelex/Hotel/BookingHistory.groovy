package com.travelex.Hotel

import com.travelex.User.User
import com.travelex.admin.BookingStatusType
import com.travelex.admin.margin.BookingType
import com.travelex.booking.HotelForBooking
import com.travelex.booking.RoomBooked
import com.travelex.hotelFunctionality.TboHotelBookingVO
import org.apache.commons.lang.RandomStringUtils

import java.text.SimpleDateFormat

class BookingHistory {

    String bookId
    String bookingCancelId
    String confirmationNo
    String referenceNo
    String tripId
    Date checkIn
    Date checkOut
    String counrtyName
    String noOfRooms
    BookingStatusType bookingStatus = BookingStatusType.Booked
    BookingType bookingType
    String bookingIdTrvlx
    String confirmationIdTrvlx
    String cancelIdTrvlx
    Date dateCreated
    Date lastUpdated


    static belongsTo = [user: User]

    static constraints = {

        bookId(nullable: false, blank: false)
        bookingCancelId(nullable: true, blank: true)
        confirmationNo(nullable: false, blank: false)
        referenceNo(nullable: false, blank: false)
        tripId(nullable: false, blank: false)
        checkIn(nullable: false, blank: false)
        checkOut(nullable: false, blank: false)
        noOfRooms(nullable: false, blank: false)
        bookingIdTrvlx(nullable: false, blank: false)
        confirmationIdTrvlx(nullable: false, blank: false)
        cancelIdTrvlx(nullable: true, blank: true)
        bookingStatus(nullable: false, blank: false)
        counrtyName(nullable: false, blank: false)
    }

    void saveBookingHistory(def bookingResult, TboHotelBookingVO tboHotelBookingVO) {
        this.bookId = bookingResult.BookingId
        print('1111111111111111111111111111111111111' + bookingResult.BookingId)
        this.confirmationNo = bookingResult.ConfirmationNo
        print('2222222222222222222222222222222222' + bookingResult.ConfirmationNo)
        this.referenceNo = bookingResult.BookingRefNo
        print('3333333333333333333333333333333333' + bookingResult.BookingRefNo)
        this.tripId = bookingResult.BookingRefNo
        String randomStringUtils = RandomStringUtils.randomAlphanumeric(8)
        Date today = new Date()
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat('dd/MM/yyyy')
        String[] date = simpleDateFormat.format(today).split('/')
        this.bookingIdTrvlx = "TR" + "BK" + "${randomStringUtils.toUpperCase()}" + "${date[0]}" + "${date[1]}" + "${date[2]}"
        String randomStringUtilsConfirm = RandomStringUtils.randomAlphanumeric(8)
        Date todayConfirm = new Date()
        String[] dateConfirm = simpleDateFormat.format(todayConfirm).split('/')
        this.confirmationIdTrvlx = "TR" + "CF" + "${randomStringUtilsConfirm.toUpperCase()}" + "${dateConfirm[0]}" + "${dateConfirm[1]}" + "${dateConfirm[2]}"
        this.checkIn = simpleDateFormat.parse(tboHotelBookingVO?.checkIn)
        this.checkOut = simpleDateFormat.parse(tboHotelBookingVO?.checkOut)
        this.noOfRooms = bookingResult.NoOfRooms.toString()
        this.bookingType = BookingType.HOTEL
        this.counrtyName = tboHotelBookingVO.countryName
    }


}
