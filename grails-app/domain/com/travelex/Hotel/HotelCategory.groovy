package com.travelex.Hotel

class HotelCategory {
    String categoryId
    String name
    static constraints = {
        categoryId(nullable: false)
        name(nullable: false)


    }

    HotelCategory(){

    }

    HotelCategory(def hotelCategory){
        this.categoryId=hotelCategory.attribute("id")
        this.name=hotelCategory.name.text()

    }
}
