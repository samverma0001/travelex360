package com.travelex.Hotel

import com.travelex.admin.PackageHotelType
import com.travelex.common.Address
import com.travelex.common.Facility
import com.travelex.socials.Enums.HotelType

class Hotel {

    String name
    String summary
    String contactNo
    String website
    String mainImageName
    String mainImageType
    HotelType type = HotelType.RESORTS_AND_PLACES
    PackageHotelType packageHotelType = PackageHotelType.THREESTAR
    List<Facility> hotelFacilities = []
    List<HotelRoom> hotelRooms = []
    List<HotelPicture> hotelPictures = []
    Date dateCreated
    Date lastUpdated


    static transients = ['getPictureUrl', 'getCity']
    static hasMany = [hotelPictures: HotelPicture, hotelRooms: HotelRoom]
//    ServiceProvider serviceProvider

    static constraints = {
        name(nullable: false, blank: false)
        summary(nullable: false, blank: false)
        contactNo(nullable: true, blank: true)
        website(nullable: true, blank: true)
        mainImageName(nullable: true, blank: true)
        mainImageType(nullable: true, blank: true)
        type(nullable: false)
        packageHotelType(nullable: false)
    }

    static mapping = {
        summary type: 'text'
    }

    public Facility getFacility() {
        return hotelFacilities.first()
    }

//    public HotelRoom getRoomTypes(){
//        return roomTypes.maxPrice
//    }

//    public Address getCity() {
//        return address.city
//    }

}
