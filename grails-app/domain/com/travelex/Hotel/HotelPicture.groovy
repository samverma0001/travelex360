package com.travelex.Hotel

class HotelPicture {

    String hotelPictureName
    String hotelPictureType
    String hotelPictureUrl
    Hotel hotel


    static constraints = {
        hotelPictureUrl(nullable: false, blank: false)
        hotelPictureName(nullable: false, blank: false)
        hotelPictureType(nullable: false, blank: false)
        hotel(nullable: true)
    }

}
