package com.travelex.Hotel

import com.travelex.User.User

class AddToActivitySummary {
    String actId
    String name
    String price
    String type
    String address
    String addedOn
    static belongsTo = [user: User]

    static constraints = {
        actId(nullable: false)
        name(nullable: false)
        price(nullable: false)
        type(nullable: false)
        address(nullable: false)
    }
}
