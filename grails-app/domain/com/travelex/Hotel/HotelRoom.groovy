package com.travelex.Hotel

class HotelRoom {

    String name
    String description
    Long price
//    Long maxPrice
    List<RoomFacility> roomFacilities = []
    List<Booking> booking = []
    List<RoomPicture> roomPicture = []

    static belongsTo = [hotel: Hotel]

    static transients = ['getRoomPictureUrl']
    static hasMany = [roomPicture: RoomPicture, roomFacilities: RoomFacility, booking: Booking]


    static constraints = {
        name(nullable: false, blank: false)
        description(nullable: false, blank: false)
        price(nullable: false)
//        maxPrice(nullable: false)
        booking(nullable: true)
        roomPicture(nullable: true)
    }
//    public Picture getRoomPictureUrl(){
//        return pictures.first()
//    }
}
