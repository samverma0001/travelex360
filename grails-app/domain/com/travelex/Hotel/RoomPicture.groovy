package com.travelex.Hotel

class RoomPicture {

    String roomPictureName
    String roomPictureType
    String roomPictureUrl

//    static belongsTo = [hotel:Hotel]
   HotelRoom hotelRoom
    static constraints = {
        roomPictureUrl(nullable: false, blank: false)
        roomPictureName(nullable: false, blank: false)
        roomPictureType(nullable: false, blank: false)
        hotelRoom(nullable: true)
    }
}
