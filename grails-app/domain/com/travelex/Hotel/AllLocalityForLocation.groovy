package com.travelex.Hotel

class AllLocalityForLocation {
    String locId
    String name

    static belongsTo = [city:City]

    static constraints = {
        locId(nullable: true)
        name(nullable: true)
    }
}
