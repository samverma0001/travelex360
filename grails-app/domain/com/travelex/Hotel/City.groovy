package com.travelex.Hotel

class City {

    String locationId
    String parentId
    Date dateCreated
    Date lastUpdated
    String countryCode
    String name

    static hasMany = [locality: AllLocalityForLocation]

    static constraints = {
        name(nullable: false)
    }

    City() {

    }

    City(def locationData) {
        this.locationId = locationData.attribute("id")
        this.parentId = locationData.attribute("parent_id")
        this.countryCode = locationData.attribute("countryCode")
        this.name = locationData.name.text()
    }
}
