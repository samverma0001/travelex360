package com.travelex.Hotel

import com.travelex.common.Facility

class RoomFacility {

    Facility facility

    static belongsTo = [room: HotelRoom]

    static constraints = {
        facility(nullable: false, blank: false)
    }
}
