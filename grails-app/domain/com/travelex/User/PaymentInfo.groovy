package com.travelex.User

import com.travelex.Hotel.BookingHistory

class PaymentInfo {

    Date dateCreated
    Date lastUpdated
    String payuMoneyId
    String paymentMode
    String paymentStatus
    String merchantKey
    String paymentTxnId
    String amount
    String totalTax
    String payUMoneyStake
    String discount
    String fName
    String email
    String userCardNumber
    String mihpayid
    String productinfo
    BookingHistory bookingHistory


    static constraints = {
        payuMoneyId(nullable: false, blank: false)
        paymentMode(nullable: false, blank: false)
        paymentStatus(nullable: false, blank: false)
        merchantKey(nullable: false, blank: false)
        paymentTxnId(nullable: false, blank: false)
        amount(nullable: false, blank: false)
        totalTax(nullable: false, blank: false)
        discount(nullable: false, blank: false)
        fName(nullable: false, blank: false)
        email(nullable: false, blank: false)
        userCardNumber(nullable: false, blank: false)
        mihpayid(nullable: false, blank: false)
        productinfo(nullable: false, blank: false)
        bookingHistory(nullable: false)
        payUMoneyStake(nullable: false, blank: false)
    }

    void savePaymentInfoFromPayUMoney(Map map) {
        this.payuMoneyId = map.payuMoneyId
        this.paymentMode = map.mode
        this.paymentStatus = map.status
        this.paymentTxnId = map.txnid
        this.discount = 'not available'
        this.fName = map.firstname
        this.email = map.email
        this.userCardNumber = map.cardnum
        this.mihpayid = map.mihpayid
        this.productinfo = map.productinfo
        this.merchantKey = map.key

    }


}
