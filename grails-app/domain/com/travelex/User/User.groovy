package com.travelex.User

import com.travelex.Hotel.BookingHistory
import com.travelex.flight.BookedFlight
import com.travelex.hotelFunctionality.TboHotelBookingVO
import com.travelex.social.SocialProfile
import org.apache.commons.lang.RandomStringUtils

import java.text.SimpleDateFormat

class User {

    transient springSecurityService

    Date dateCreated
    Date lastUpdated

    String firstName
    String lastName
    String username
    String password
    String mobNo
    String token = UUID.randomUUID().toString()
    String coustId
    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired

    String pictureUrl
    String pictureName
    String pictureType
    Boolean hasVerifiedEmail = false
    String ipAddress

    List<SocialProfile> socialProfileList = []

    static constraints = {

        username blank: false, unique: true
        password blank: false
        ipAddress(blank: true, nullable: true)
        firstName(blank: true, nullable: true)
        lastName(blank: true, nullable: true)
        token(blank: true, nullable: true)
        pictureUrl(blank: true, nullable: true)
        mobNo(blank: true, nullable: true)
        pictureName(blank: true, nullable: true)
        pictureType(blank: true, nullable: true)
        coustId(nullable: false, blank: false)
    }

    static mapping = {
        password column: '`password`'
    }

    static hasMany = [socialProfileList: SocialProfile, bookingHistory: BookingHistory, bookedFlights: BookedFlight]

    Set<Role> getAuthorities() {
        UserRole.findAllByUser(this).collect { it.role } as Set
    }

    def beforeInsert() {
        encodePassword()
    }

    def beforeUpdate() {
        if (isDirty('password')) {
            encodePassword()
        }
    }

    protected void encodePassword() {
        password = springSecurityService.encodePassword(password)
    }


    void createOrCheckUserWhileBookHotel(TboHotelBookingVO tboHotelBookingVO) {
        this.firstName = tboHotelBookingVO.adult1MNameRoom1
        this.lastName = tboHotelBookingVO.adult1LNameRoom1
        this.username = tboHotelBookingVO.adult1EmailRoom1
        String randomAlphanumeric = RandomStringUtils.randomAlphanumeric(8)
        Date today = new Date()
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat('dd/MM/yyyy')
        String[] date = simpleDateFormat.format(today).split('/')
        this.coustId = "TR" + "CS" + randomAlphanumeric.toUpperCase() + "${date[0]}" + "${date[1]}" + "${date[2]}"
        this.accountExpired = false
        this.accountLocked = false
        this.hasVerifiedEmail = true
        this.passwordExpired = false
        this.enabled = true
        this.pictureName = "picNotAvailable"
        this.pictureType = "jpeg"
        if (!this.validate()) {
            this.errors.allErrors.each {
                log.info '--------error is--------' + it
            }
        }
    }


}
