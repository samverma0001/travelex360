package com.travelex.common

class FacilityFromMGH {

    String name
    String facilityId
    Boolean forHotel = Boolean.TRUE
    Boolean forRoom = Boolean.FALSE
    Boolean showInLeftPanel = Boolean.FALSE


    static constraints = {
        name(nullable: false, blank: false)
        facilityId(nullable: true)
        forHotel(nullable: true)
        forRoom(nullable: true)
        showInLeftPanel(nullable: true)
    }
}
