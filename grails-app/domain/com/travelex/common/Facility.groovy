package com.travelex.common

import com.travelex.socials.Enums.FacilityFor

class Facility {

    String name
    FacilityFor facilityFor = FacilityFor.HOTEL
    Long hotelId
    Long hotelRoomId

    static constraints = {
        name(nullable: false, blank: false)
        facilityFor(nullable: true)
        hotelId(nullable: true)
        hotelRoomId(nullable: true)
    }
}
