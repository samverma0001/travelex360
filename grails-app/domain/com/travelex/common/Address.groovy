package com.travelex.common

import com.travelex.Hotel.Hotel

class Address {

    String address1
    String address2
    String location
    String city
    String zipCode
    String country
    Hotel hotel

    static constraints = {

        address1(nullable: false, blank: false)
        address2(nullable: true, blank: true)
        city(nullable: false, blank: false)
        zipCode(nullable: false, blank: false)
        country(nullable: false, blank: false)
        hotel(nullable: false)
        location(nullable: false, blank: false)

    }
}
