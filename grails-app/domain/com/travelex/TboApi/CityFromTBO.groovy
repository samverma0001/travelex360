package com.travelex.TboApi

class CityFromTBO {

    Date dateCreated
    Date lastUpdated
    String locationId
    String name
    static belongsTo = [countryFromTbo: CountryFromTbo]
    static constraints = {
    }

    CityFromTBO() {

    }

    CityFromTBO(def locationData, CountryFromTbo countryFromApi) {
        this.locationId = locationData.CityCode
        this.name = locationData.CityName
        this.countryFromTbo = countryFromApi
    }
}
