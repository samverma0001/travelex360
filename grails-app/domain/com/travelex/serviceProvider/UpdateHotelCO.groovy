package com.travelex.serviceProvider

import grails.validation.Validateable

@Validateable

class UpdateHotelCO {
    String serviceProviderId
    String hotelId
    String addressId
    String hotelPicId
    String roomPicId
    String hotelFacilityId

    String hotelName
    String hotelSummary
    String hotelType
    String hotelPictureUrl
    String hotelPictureContentType
    String hotelFacilities

    String addressOne
    String addressTwo
    String addressCity
    String addressState
    String addressZip
    String addressCountry

    String roomOneName
    String roomOneDescription
    String roomOneMinimumPrice
    String roomOneMaximumPrice
    String roomOnePictureUrl
    String roomOnePictureContentType
    String roomOneFacilities

    static constraints = {
        serviceProviderId(nullable: true,blank: true)
        hotelId(nullable: true,blank: true)
        addressId(nullable: true,blank: true)
        hotelPicId(nullable: true,blank: true)
        roomPicId(nullable: true,blank: true)
        hotelFacilityId(nullable: true,blank: true)

        hotelName(nullable: false,blank: false)
        hotelSummary(nullable: false,blank: false)
        hotelType(nullable: false,blank: false)
        hotelPictureUrl(nullable: false,blank: false)
        hotelPictureContentType(nullable: false,blank: false)
        hotelFacilities(nullable: false,blank: false)
        addressOne(nullable: false,blank: false)
        addressTwo(nullable: false,blank: false)
        addressCity(nullable: false,blank: false)
        addressState(nullable: false,blank: false)
        addressZip(nullable: false,blank: false)
        addressCountry(nullable: false,blank: false)
        roomOneName(nullable: false,blank: false)
        roomOneDescription(nullable: false,blank: false)
        roomOneMinimumPrice(nullable: false,blank: false)
        roomOneMaximumPrice(nullable: false,blank: false)
        roomOnePictureUrl(nullable: false,blank: false)
        roomOnePictureContentType(nullable: false,blank: false)
        roomOneFacilities(nullable: false,blank: false)
    }
}
