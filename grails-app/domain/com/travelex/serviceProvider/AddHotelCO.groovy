package com.travelex.serviceProvider

import grails.validation.Validateable

@Validateable

class AddHotelCO {
    String hotelName
    String hotelSummary
    String hotelType
//    String hotelPicture
//    String hotelPictureContentType
    String hotelFacilities

    String addressOne
    String addressTwo
    String addressCity
    String addressState
    String addressZip
    String addressCountry

    String roomOneName
    String roomOneDescription
//    String roomOneMinimumPrice
//    String roomOneMaximumPrice
//    String roomOnePictureUrl
//    String roomOnePictureContentType
    String roomOneFacilities

    static constraints = {
        hotelName(nullable: false,blank: false)
        hotelSummary(nullable: false,blank: false)
        hotelType(nullable: false,blank: false)
//        hotelPicture(nullable: false,blank: false)
//        hotelPictureContentType(nullable: false,blank: false)
        hotelFacilities(nullable: false,blank: false)
        addressOne(nullable: false,blank: false)
        addressTwo(nullable: false,blank: false)
        addressCity(nullable: false,blank: false)
        addressState(nullable: false,blank: false)
        addressZip(nullable: false,blank: false)
        addressCountry(nullable: false,blank: false)
        roomOneName(nullable: false,blank: false)
        roomOneDescription(nullable: false,blank: false)
//        roomOneMinimumPrice(nullable: false,blank: false)
//        roomOneMaximumPrice(nullable: false,blank: false)
//        roomOnePictureUrl(nullable: false,blank: false)
//        roomOnePictureContentType(nullable: false,blank: false)
        roomOneFacilities(nullable: false,blank: false)
    }
}
