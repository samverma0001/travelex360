package com.travelex.order

class OrderDetails {
    String guestName
    String guestPhoneNo
    String guestEmail
    String checkInDate
    String checkOutDate
    String hotelName
    String numberOfRoomsBooked
    String bookingId
    String bookingRefNo
    String confirmationNo
    String bookingDate
    String hotelCancelPolicy
    String orderNumber




    static constraints = {
    }
}
