package com.travelex.flight

import com.travelex.TboApi.CountryFromTbo

class BookedFlightPassenger {

    String title
    String firstName
    String lastName
    String type
    String gender
    Date dateOfBirth
    String passportNumber
    String passportExpiry
    String country
    String phone
    String pincode
    String address1
    String address2
    String email
    String meal
    String seat
    BookedFlight bookedFlight

    static constraints = {

        meal nullable: true,blank: true
        seat nullable: true,blank: true
        gender nullable: true,blank: true

    }
}
