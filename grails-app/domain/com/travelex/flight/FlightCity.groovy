package com.travelex.flight

class FlightCity {

    String cityCode
    String cityName
    String countryCode


    static constraints = {
        cityCode(nullable: true,blank: true)
        countryCode(nullable: true,blank: true)
    }
}
