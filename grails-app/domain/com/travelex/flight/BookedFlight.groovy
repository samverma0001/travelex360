package com.travelex.flight

import com.travelex.User.User

class BookedFlight {

    String origin
    String pnr
    String bookingId
    String destination
    String fareType
    String refId
    String source
    String status
    Boolean isRefundable
    Date dateCreated
    Date lastUpdated

    static belongsTo = [user: User]

    static hasOne = [bookedFlightFare: BookedFlightFare]
    static hasMany = [bookedFlightTicket: BookedFlightTicket,bookedFlightPassanger:BookedFlightPassenger]

    static constraints = {
        bookedFlightFare nullable: true
        origin nullable: true, blank: true
        destination nullable: true, blank: true
        fareType nullable: true, blank: true
        source nullable: true, blank: true
        status nullable: true, blank: true
        isRefundable nullable: true
    }


}
