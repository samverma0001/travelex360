package com.travelex.flight

class BookedFlightTicket {

    Integer ticketId
    String ticketNumber
    String title
    String firstName
    String lastName
    String passengerType
    Date issueDate
    String tourCode
    BookedFlight bookedFlight
    Date dateCreated
    Date lastUpdated

    static constraints = {
    }
}
