package com.travelex.flight


class BookedFlightFare {

    BigDecimal additionalTxnFee
    BigDecimal agentCommission
    BigDecimal tdsOnCommission
    BigDecimal airTransFee
    BigDecimal baseFare
    BigDecimal tax
    BigDecimal otherCharges
    BigDecimal fuelSurcharge
    BigDecimal discount
    BigDecimal serviceTax
    String currency
    BigDecimal plb
    BigDecimal tdsOnPLB
    BigDecimal incentiveEarned
    BigDecimal tdsOnIncentive
    BigDecimal offeredFare
    BigDecimal agentServiceCharge
    BigDecimal publishedPrice

    static belongsTo = [bookedFlight: BookedFlight]

    static constraints = {


    }
}
