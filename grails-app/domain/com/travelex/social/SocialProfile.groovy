package com.travelex.social


import com.travelex.User.User
import com.travelex.socials.FacebookSignupVO
import com.travelex.socials.GoogleSignupVO
import com.travelex.socials.LinkedInSignupVO
import com.travelex.socials.SocialProfileType

class SocialProfile {

    String accessToken
    String accessSecret
    String profileId
    String userName
    String name
    String profileUrl
    String alternateEmail
    SocialProfileType type = SocialProfileType.FACEBOOK

    String location
    String summary
    String specialities
    String gender
    String hometown
    String birthday

    static belongsTo = [user: User]

    static constraints = {
        accessToken(blank: false, nullable: false)
        accessSecret(blank: true, nullable: true)
        profileId(blank: false, nullable: false)
        userName(blank: false, nullable: false)
        name(blank: false, nullable: false)
        location(blank: true, nullable: true)
        summary(blank: true, nullable: true, maxSize: 2000, type: 'longtext')
        specialities(blank: true, nullable: true)
        gender(blank: true, nullable: true)
        hometown(blank: true, nullable: true)
        birthday(blank: true, nullable: true)
        profileUrl(blank: true, nullable: true)
        alternateEmail(nullable: true)
    }

    static mapping = {
    }

    SocialProfile() {
    }

    SocialProfile(User user, FacebookSignupVO facebookSignupVO) {
        this.user = user
        this.type = SocialProfileType.FACEBOOK
        this.profileId = facebookSignupVO.profileId
        this.accessToken = facebookSignupVO.accessToken
        this.name = facebookSignupVO.name
        this.userName = facebookSignupVO.facebookUserName
        this.hometown = facebookSignupVO.hometown
        this.gender = facebookSignupVO.gender
        this.profileUrl = facebookSignupVO.link
        this.birthday = facebookSignupVO.birthday
        this.location = facebookSignupVO.currentLocation
    }

    SocialProfile(User user, LinkedInSignupVO linkedInSignupVO) {
        this.user = user
        this.type = SocialProfileType.LINKED_IN
        this.profileId = linkedInSignupVO?.profileId
        this.accessToken = linkedInSignupVO?.accessToken
        this.accessSecret = linkedInSignupVO?.accessSecret
        this.name = linkedInSignupVO?.name
        this.userName = linkedInSignupVO?.profileId
        this.gender = linkedInSignupVO?.gender
        this.location = linkedInSignupVO?.location
        this.summary = linkedInSignupVO?.summary
        this.birthday = linkedInSignupVO?.dateOfBirth
    }

    SocialProfile(User user, GoogleSignupVO googleSignupVO) {
        this.user = user
        this.type = SocialProfileType.GOOGLE
        this.profileId = googleSignupVO.profileId
        this.accessToken = googleSignupVO.accessToken
        this.accessSecret = googleSignupVO.accessSecret
        this.name = googleSignupVO.name
        this.userName = googleSignupVO.profileId
    }

}
