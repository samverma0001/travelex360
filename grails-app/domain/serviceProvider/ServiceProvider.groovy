package serviceProvider

import activity.Activity
import com.travelex.Hotel.Hotel
import com.travelex.User.User

class ServiceProvider {
    String nameOfHotel
    String city
    String state
    String address
    String propertyType
    String nameOfRepresentee
    String designationOfRepresentee
    String officialEmail
    String website
    String mobNo
    Boolean approved = false
    Date lastUpdated
    Date dateCreated
    static  belongsTo = [user:User]
    static hasMany = [hotel: Hotel,activities:Activity]

    static constraints = {
        nameOfHotel(nullable: false, blank: false)
        city(nullable: false, blank: false)
        state(nullable: false, blank: false)
        address(nullable: true, blank: false)
        propertyType(nullable: true, blank: false)
        nameOfRepresentee(nullable: true, blank: false)
        designationOfRepresentee(nullable: true, blank: false)
        officialEmail(nullable: true, blank: false)
        website(nullable: true, blank: false)
        mobNo(nullable: true, blank: false)


    }
}
