package activity

class ActivitiesHistory {

	String userId
	String activityId
    String activitiesCancelId
    String activitiesBookingNo
    String activitiesBookingStatus
    String activitiesConfirmationNo
    String activitiesReferenceNo
    Date dateCreated
    Date lastUpdated

    static constraints = {
    }
}
