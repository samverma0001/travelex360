package activity
import java.sql.Timestamp
class ActivitiesServiceProviders {

    String name
    String location
    String authorisedPersonalName
    String phoneNumber
    String email
    String rating
//    String puchasedBy
    Timestamp createdDate
    Timestamp editedDate
    byte[] serviceProviderImage
//    Activities  activity
//    static hasMany = [relatedActivities: Activities]

    static constraints = {
        name  blank: true, nullable: true
        location  blank: true, nullable: true
        authorisedPersonalName  blank: true, nullable: true
        phoneNumber  blank: true, nullable: true
        email  blank: true, nullable: true
        rating  blank: true, nullable: true
        createdDate  blank: true, nullable: true
        editedDate  blank: true, nullable: true
        serviceProviderImage(nullable:true, maxSize: 1000*1024*10);
//        activity nullable:true
//        relatedActivities blank: true, nullable: true
    }
}
