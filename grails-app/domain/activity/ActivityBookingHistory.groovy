package activity

import com.travelex.User.User

class ActivityBookingHistory {

    String activityId
    String actStart
    String actEnd
    String activityName
    String price
    String information
    String address
    String activityType
    String bookedOn
    static belongsTo = [user:User]

    static constraints = {

        activityId(nullable: false, blank: false)
        activityName(nullable: false, blank: false)
        information(nullable: true)
        activityType(nullable: false,blank: false)
        address(nullable: true)
        activityType(nullable: true)
    }
}
