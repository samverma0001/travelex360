package activity


class ActivityCategory {

    String name
    ActivityCategory parentCategory
    ActivityCategory childCategory
    Date dateCreated
    Date lastUpdated

    static constraints = {

        name nullable: false, blank: false
        parentCategory nullable: true
        childCategory nullable: true

    }


}
