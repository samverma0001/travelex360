package activity

import com.travelex.socials.Enums.ActivityType

import java.sql.Timestamp

class Activity {

    String name
    String location
    String primaryInterest
    String secondaryInterest
    String activityCompanyName
    String companyAuthorisedPersonalName
    String phoneNumber
    String email
    String rating
    String duration
    String puchasedBy
    Timestamp createdDate
    Timestamp editedDate
    ActivityType type
    byte[] activityImage
    ActivitiesServiceProviders  serviceProvider
//    static belongsTo = ActivitiesServiceProviders

    static constraints = {
        name  blank: true, nullable: true
        location  blank: true, nullable: true
        primaryInterest  blank: true, nullable: true
        secondaryInterest  blank: true, nullable: true
        activityCompanyName  blank: true, nullable: true
        companyAuthorisedPersonalName  blank: true, nullable: true
        phoneNumber  blank: true, nullable: true
        email  blank: true, nullable: true
        rating  blank: true, nullable: true
        puchasedBy  blank: true, nullable: true
        createdDate  blank: true, nullable: true
        editedDate  blank: true, nullable: true
        type blank: true, nullable: true
        serviceProvider blank: true, nullable: true
        duration blank: true, nullable: true
        activityImage(nullable:true, maxSize: 1000*1024*10);
    }
    static mapping = {
        type defaultValue: 'ADVENTURE_SPORTS'
    }


}
