package activity

class ActivityPicture {
    String filePath
    String contentType

    static belongsTo = [activity:Activity]

    static constraints = {
        filePath(nullable: false, blank: false)
        contentType(nullable: false, blank: false)
    }
}
