package travelx

import com.travelex.Hotel.BookingHistory
import com.travelex.Hotel.City
import com.travelex.TboApi.CityFromTBO
import com.travelex.TboApi.CountryFromTbo
import com.travelex.User.PaymentInfo
import com.travelex.User.Role
import com.travelex.User.User
import com.travelex.User.UserRole
import com.travelex.admin.MarginType
import com.travelex.admin.SetMargin
import com.travelex.booking.Adult
import com.travelex.booking.Child
import com.travelex.booking.HotelForBooking
import com.travelex.booking.RoomBooked
import com.travelex.common.FacilityFromMGH
import com.travelex.feedBack.UserFeedBack
import com.travelex.hotelFunctionality.HotelBookingVO
import com.travelex.hotelFunctionality.HotelDetailVO
import com.travelex.hotelFunctionality.HotelSearchParamsVO
import com.travelex.hotelApiService.HotelApiService
import com.travelex.hotelFunctionality.TboHotelBookingVO
import com.travelex.order.OrderDetails
import com.travelex.socials.hotel.HotelFacilityByIdVO
import com.travelex.socials.hotel.HotelFacilityVO
import com.travelex.socials.hotel.HotelVO
import com.travelex.socials.hotel.LocalityByIdVO
import com.travelex.socials.searchHotel.SearchHotelVO
import com.travelex.socials.searchHotel.TboSearchHotelRoomDetailVO
import com.travelex.socials.tboSearchHotel.TboBookResponseVO
import com.travelex.socials.tboSearchHotel.TboHotelDetailVO
import grails.converters.JSON
import grails.plugin.mail.MailService
import org.apache.commons.lang3.RandomStringUtils
import org.codehaus.groovy.grails.plugins.web.taglib.CountryTagLib
import org.codehaus.groovy.grails.web.context.ServletContextHolder
import util.AppUtil
import wslite.soap.SOAPClient
import wslite.soap.SOAPResponse

import java.math.RoundingMode
import java.text.SimpleDateFormat

class HotelServiceController {
    public static final String TOKEN = "00c3d12bf007d78c9edb2897b8c909cb"
    public static final String MGHWORLD_BASE_URL = "http://www.demo.mghworld.net/API/v1/xml"


    def pdfRenderingService
    def hashCalculationService
    def grailsApplication
    MailService mailService


    def index() {}


    def bookHotel = {
        Integer minPrice = 200
        Integer maxPrice = 500000
        print('-----credentails are-----' + grailsApplication.config.tbo.TRAVELX_UNAME)
        print('-----credentails are-----' + grailsApplication.config.tbo.TRAVELX_PASS_HOTEL)

        render(view: "/searchHotel/newThemed/hotelSearch", model: [minPrice: minPrice, maxPrice: maxPrice])
    }

    def findCityForHotel = {
        String cityType = params.term
        List<CityFromTBO> citiesList = []
        List<String> filterName = params.list('city[]') as List<String>
        CountryFromTbo domesticCountry = CountryFromTbo.findByCountryName("India")
        filterName.each {
            if (it.equals('domestic')) {
                citiesList = CityFromTBO.createCriteria().list {
                    eq('countryFromTbo', domesticCountry)
                    ilike('name', "%" + cityType + "%")
                }
            } else {
                citiesList = CityFromTBO.createCriteria().list {
                    ne('countryFromTbo', domesticCountry)
                    ilike('name', "%" + cityType + "%")
                }
            }
            render citiesList?.name as JSON
        }
    }

    def searchData = { HotelSearchParamsVO hotelSearchParamsVO ->
        print('--------params are-----------' + params)


        Integer max = params.max ? params.int('max') : 10
        Integer offset = params.offset ? params.int('offset') : 0

        String mnPrice = params.priceRangeMin
        String mxPrice = params.priceRangeMax
        Integer minPrice = mnPrice ? mnPrice?.tokenize("INR")?.first() as Integer : 200
        Integer maxPrice = mxPrice ? mxPrice?.tokenize("INR")?.first() as Integer : 500000

        if (hotelSearchParamsVO.validate()) {
            Date checkInDate = AppUtil.convertDateFromStringToDate(params.checkIn)
            Date checkOutDate = AppUtil.convertDateFromStringToDate(params.checkOut)
            String noOfNights = checkOutDate - checkInDate
            hotelSearchParamsVO.passParamsInMethod(params)
            CityFromTBO cityFromTBO
            String placeDom = params.place
            cityFromTBO = CityFromTBO.findByName(params.place)
            hotelSearchParamsVO.place = cityFromTBO.name
            if (params.location == "international") {
                placeDom = params.place
                cityFromTBO = CityFromTBO.findByName(params.place)
                hotelSearchParamsVO.place = cityFromTBO.name
            }
            session["hotelSearchParamVO"] = hotelSearchParamsVO
            HotelApiService hotelApiService = new HotelApiService()
            List<SearchHotelVO> priceRangeHotelList = []
            List<SearchHotelVO> hotelListPaginate = []
            def searchResult = hotelApiService.search(hotelSearchParamsVO)
            def hotelList = session["hotelList"] as List
            print('------------ room 1111111111111111111111111111' + hotelList)
            String hPrice
            searchResult.each {
                print('----------rate before margin-----------' + it?.hotelRoomDetailVOArrayList?.hotelRateVO?.totalRate)
                print('---------travelex 360 margin ------------------' + it?.hotelRoomDetailVOArrayList?.hotelRateVO?.agentMarkUp?.first())
                Float agencyMarkUp = it?.hotelRoomDetailVOArrayList?.hotelRateVO?.agentMarkUp?.first()?.toFloat()
                hPrice = it.hotelRoomDetailVOArrayList?.hotelRateVO?.totalRate?.min()
                Float hotelPrice = hPrice.toFloat() + agencyMarkUp
                print('----------rate after margin-----------' + hotelPrice)

                if (hotelPrice >= minPrice && hotelPrice <= maxPrice) {
                    priceRangeHotelList.add(it)
                }
            }
            hotelListPaginate = priceRangeHotelList.subList(offset, max > priceRangeHotelList.size() ? priceRangeHotelList.size() : max)
            CountryFromTbo countryFromTbo = CountryFromTbo.findByCountryName("India")
            print('-------- sze of hotel list is=========================' + hotelList.size())

            render(view: "/searchHotel/newThemed/hotelSearchResultList", model: [
                    hotelList         : hotelListPaginate, minPrice: minPrice,
                    total             : hotelList.size(), cityName: cityFromTBO?.name,
                    searchParametersCO: hotelSearchParamsVO, maxPrice: maxPrice, noOfNights: noOfNights, searchFor: params.location,])
        } else {
            print('33333333333333333333333333333333333333333' + params)
            String modifySearch = params.modifySearch
            String searchFor = params.location
            List<CityFromTBO> citiesList = []
            CityFromTBO selectedCity
            if (modifySearch) {
                hotelSearchParamsVO.passParamsInMethod(params)
                CityFromTBO cityFromTBO
                String placeDom = params.place
                cityFromTBO = CityFromTBO.findByName(params.place)
                hotelSearchParamsVO.place = cityFromTBO.name
                if (params.location == "international") {
                    placeDom = params.place
                    cityFromTBO = CityFromTBO.findByName(params.place)
                    hotelSearchParamsVO.place = cityFromTBO.name
                }
                session["hotelSearchParamVO"] = hotelSearchParamsVO
                HotelApiService hotelApiService = new HotelApiService()
                List<SearchHotelVO> priceRangeHotelList = []
                List<SearchHotelVO> hotelListPaginate = []
                def searchResult = hotelApiService.search(hotelSearchParamsVO)
                def hotelList = session["hotelList"] as List
                String hPrice
                searchResult.each {
                    print('----------22222222222222222222222222' + it?.hotelRoomDetailVOArrayList?.hotelRateVO?.totalRate)
                    hPrice = it.hotelRoomDetailVOArrayList?.hotelRateVO?.totalRate?.min()
                    Float agencyMarkUp = it?.hotelRoomDetailVOArrayList?.hotelRateVO?.agentMarkUp?.first()?.toFloat()
                    Float hotelPrice = hPrice.toFloat() + agencyMarkUp
                    print('======hotel price=====================' + hotelPrice)
                    if (hotelPrice >= minPrice && hotelPrice <= maxPrice) {
                        priceRangeHotelList.add(it)
                    }
                }
                hotelListPaginate = priceRangeHotelList.subList(offset, max > priceRangeHotelList.size() ? priceRangeHotelList.size() : max)
                CountryFromTbo countryFromTbo = CountryFromTbo.findByCountryName("India")
                citiesList = CityFromTBO.findAllByCountryFromTbo(countryFromTbo)
                print('-------- sze of hotel list is=========================' + hotelList.size())

                render(view: "/searchHotel/newThemed/hotelSearchResultList", model: [modifySearch      : modifySearch,
                                                                                     hotelList         : hotelListPaginate, citiesList: citiesList, minPrice: 200,

                                                                                     total             : hotelList.size(), cityId: cityFromTBO?.id,
                                                                                     searchParametersCO: hotelSearchParamsVO, maxPrice: (hotelSearchParamsVO?.range as Integer)])
            } else {
                if (searchFor.equals('international')) {
                    CountryFromTbo interNational = CountryFromTbo.findByCountryNameNotEqual("India")
                    selectedCity = CityFromTBO.get(params.long('place'))
                    citiesList = CityFromTBO.findAllByCountryFromTbo(interNational)
                } else {
                    CountryFromTbo domesticCountry = CountryFromTbo.findByCountryName("India")
                    selectedCity = CityFromTBO.get(params.long('place'))
                    citiesList = CityFromTBO.findAllByCountryFromTbo(domesticCountry)
                }
                render(view: "/searchHotel/newThemed/hotelSearch", model: [citiesList: citiesList.unique(), hotelSearchParamsVO:
                        hotelSearchParamsVO, searchFor                               : searchFor, cityId: selectedCity?.id,
                                                                           minPrice  : minPrice, maxPrice: maxPrice])
            }
        }
    }

    def remotePaginateHotel = {
        print('-------------------------- i m in ajax scroll pagination -----------------' + params)
        List<SearchHotelVO> priceRangeHotelList = []
        List<SearchHotelVO> newList = []

        Integer max = params.max as Integer ? params.int('max') : 10
        Integer offset = params.offset as Integer ? params.int('offset') : 0
        HotelSearchParamsVO hotelSearchParamsVO = session["hotelSearchParamVO"] as HotelSearchParamsVO

        Integer minPrice = params.minRangePrice as Integer
        Integer maxPrice = params.priceRangeMax as Integer

        List<SearchHotelVO> totalCountOfHotels = []

        totalCountOfHotels = session["hotelList"] as List
        print('----------- total count of hotel is ------------' + totalCountOfHotels)
        totalCountOfHotels.each {
            String hPrice = it.hotelRoomDetailVOArrayList?.hotelRateVO?.totalRate?.min()
            Float agencyMarkUp = it?.hotelRoomDetailVOArrayList?.hotelRateVO?.agentMarkUp?.first()?.toFloat()
            Float hotelPrice = hPrice.toFloat() + agencyMarkUp
            if (hotelPrice >= (minPrice) && hotelPrice <= (maxPrice)) {
                priceRangeHotelList.add(it)
            }
        }
        Integer sizeOfHotelList = totalCountOfHotels.size()
        Integer tempMax = sizeOfHotelList - offset
        if (tempMax < 10) {
            tempMax = offset + tempMax
        } else {
            tempMax = offset + max
        }
        newList = priceRangeHotelList.subList(offset, tempMax)
        print('----------- list of res hotels are------------------' + newList)
        render(template: "/hotelTemplates/hotelSearchResult", model: [hotelList: newList, minPrice: minPrice, maxPrice:
                maxPrice, total                                                : priceRangeHotelList.size(), searchParametersCO: hotelSearchParamsVO])

    }


    def hotelListBasedOnRatingSubmit = {
//        SearchParametersCO searchParametersCO ->
        log.info('------------- params are in hotel list based on rating submit---------' + params)
        HotelSearchParamsVO hotelSearchParamsVO = session["hotelSearchParamVO"] as HotelSearchParamsVO
        String mnPrice = params.priceRangeMin
        String mxPrice = params.priceRangeMax
        Integer max = params.max ? params.int('max') : 10
        Integer offset = params.offset ? params.int('offset') : 0
        Integer minPrice = mnPrice ? mnPrice?.tokenize("INR")?.first() as Integer : 200
        Integer maxPrice = mxPrice ? mxPrice?.tokenize("INR")?.first() as Integer : 500000

        List<SearchHotelVO> priceRangeHotelList = []
        List<SearchHotelVO> finalHotelList = []
        List<SearchHotelVO> hotelListPaginate = []
        List<SearchHotelVO> totalCountOfHotels = session["hotelList"] as List
        List<String> filterName = params.list('selectedCheckBox[]') as List<String>
        List<String> selectedCheckBoxHotelStar = params.list('selectedCheckBoxHotelStar[]') as List<String>
        totalCountOfHotels.each {
            String hPrice = it?.hotelRoomDetailVOArrayList?.hotelRateVO?.totalRate?.min()
            Float agencyMarkUp = it?.hotelRoomDetailVOArrayList?.hotelRateVO?.agentMarkUp?.first()?.toFloat()
            Float hotelPrice = hPrice.toFloat() + agencyMarkUp
            if (selectedCheckBoxHotelStar) {
                print('-----aaaaaaaa------ star type -------aaaaaaaaaa---------' + it)
                def hotel = it
                selectedCheckBoxHotelStar.each {
                    print('----------- star type ----------------' + it)
                    if (it.equals('oneStar')) {
                        if ((hotel.hotelRating == "1" || hotel.hotelRating == "OneStar") && (hotelPrice >= minPrice && hotelPrice <= maxPrice)) {
                            filterName.each {
                                String filter = it
                                String hType = hotel.hotelName
                                print('--------------- htype is -----------------' + hType)
                                if (filter.equals('allHotelType')) {
                                    priceRangeHotelList.add(hotel)
                                } else {
                                    if (hType.contains(filter))
                                        priceRangeHotelList.add(hotel)
                                }
                            }
                        }
                    } else if (it.equals('twoStar')) {
                        print('----------- in 2 -------------------')
                        if ((hotel.hotelRating == "2" || hotel.hotelRating == "TwoStar") && (hotelPrice >= minPrice && hotelPrice <= maxPrice)) {
                            filterName.each {
                                String filter = it
                                String hType = hotel.hotelName
                                print('--------------- htype is -----------------' + hType)
                                if (filter.equals('allHotelType')) {
                                    priceRangeHotelList.add(hotel)
                                } else {
                                    if (hType.contains(filter))
                                        priceRangeHotelList.add(hotel)
                                }
                            }
                        }
                    } else if (it.equals('threeStar')) {
                        print('----------- in 3 -------------------')
                        if ((hotel.hotelRating == "3" || hotel.hotelRating == "ThreeStar") && (hotelPrice >= minPrice && hotelPrice <= maxPrice)) {
                            filterName.each {
                                String filter = it
                                String hType = hotel.hotelName
                                print('--------------- htype is -----------------' + hType)
                                if (filter.equals('allHotelType')) {
                                    priceRangeHotelList.add(hotel)
                                } else {
                                    if (hType.contains(filter))
                                        priceRangeHotelList.add(hotel)
                                }
                            }
                        }
                    } else if (it.equals('fourStar')) {
                        print('----------- in 4 -------------------')
                        if ((hotel.hotelRating == "4" || hotel.hotelRating == "FourStar") && (hotelPrice >= minPrice && hotelPrice <= maxPrice)) {
                            filterName.each {
                                String filter = it
                                String hType = hotel.hotelName
                                print('--------------- htype is -----------------' + hType)
                                if (filter.equals('allHotelType')) {
                                    priceRangeHotelList.add(hotel)
                                } else {
                                    if (hType.contains(filter))
                                        priceRangeHotelList.add(hotel)
                                }
                            }
                        }
                    } else if (it.equals('fiveStar')) {
                        print('----------- in 5 -------------------')
                        if ((hotel.hotelRating == "5" || hotel.hotelRating == "FiveStar") && (hotelPrice >= minPrice && hotelPrice <= maxPrice)) {
                            filterName.each {
                                String filter = it
                                String hType = hotel.hotelName
                                print('--------------- htype is -----------------' + hType)
                                if (filter.equals('allHotelType')) {
                                    priceRangeHotelList.add(hotel)
                                } else {
                                    if (hType.contains(filter))
                                        priceRangeHotelList.add(hotel)
                                }
                            }
                        }
                    } else if (it.equals('all') && (hotelPrice >= minPrice && hotelPrice <= maxPrice)) {
                        filterName.each {
                            String filter = it
                            String hType = hotel.hotelName
                            print('--------------- htype is -----------------' + hType)
                            if (filter.equals('allHotelType')) {
                                priceRangeHotelList.add(hotel)
                            } else {
                                if (hType.contains(filter))
                                    priceRangeHotelList.add(hotel)
                            }
                        }
                    }
                }
            }
        }
        finalHotelList = priceRangeHotelList
        hotelListPaginate = finalHotelList ? finalHotelList.subList(offset, (offset + max) > priceRangeHotelList.size() ? priceRangeHotelList.size() : offset + max) : []
        print('---------- hotel list for all -------------' + hotelListPaginate)
        render(template: "/hotelTemplates/hotelSearchResult", model: [hotelList: hotelListPaginate, minPrice: minPrice, maxPrice:
                maxPrice, total                                                : priceRangeHotelList.size(), searchParametersCO: hotelSearchParamsVO])

    }


    def searchHotelType = {
        print('----------- here in params search hotel type are -----------------' + params)


        Integer max = params.max ? params.int('max') : 10
        Integer offset = params.offset ? params.int('offset') : 0

        HotelSearchParamsVO hotelSearchParamsVO = session["hotelSearchParamVO"] as HotelSearchParamsVO

        List<SearchHotelVO> totalCountOfHotels = []
        List<SearchHotelVO> priceRangeHotelList = []
        List<SearchHotelVO> finalHotelList = []
        List<SearchHotelVO> hotelListPaginate = []
        totalCountOfHotels = session["hotelList"] as List

        String mnPrice = params.priceRangeMin
        String mxPrice = params.priceRangeMax
        Integer minPrice = mnPrice.tokenize("INR").first() as Integer
        Integer maxPrice = mxPrice.tokenize("INR").first() as Integer

        List<String> filterName = params.list('selectedCheckBox[]') as List<String>
        List<String> selectedCheckBoxHotelStar = params.list('selectedCheckBoxHotelStar[]') as List<String>

        totalCountOfHotels.each {
            String hPrice = it?.hotelRoomDetailVOArrayList?.hotelRateVO?.totalRate?.min()
            Float agencyMarkUp = it?.hotelRoomDetailVOArrayList?.hotelRateVO?.agentMarkUp?.first()?.toFloat()

            Float hotelPrice = hPrice.toFloat() + agencyMarkUp
            if (selectedCheckBoxHotelStar) {
                print('-----aaaaaaaa------ star type -------aaaaaaaaaa---------' + it)
                def hotel = it
                selectedCheckBoxHotelStar.each {
                    print('----------- star type ----------------' + it)
                    if (it.equals('oneStar')) {
                        print('----------- in 1 -------------------')
                        if ((hotel.hotelRating == "1" || hotel.hotelRating == "OneStar") && (hotelPrice >= minPrice && hotelPrice <= maxPrice)) {
                            filterName.each {
                                String filter = it
                                String hType = hotel.hotelName
                                print('--------------- htype is -----------------' + hType)
                                if (filter.equals('allHotelType')) {
                                    priceRangeHotelList.add(hotel)
                                } else {
                                    if (hType.contains(filter))
                                        priceRangeHotelList.add(hotel)
                                }
                            }
                        }
                    } else if (it.equals('twoStar')) {
                        print('----------- in 2 -------------------')
                        if ((hotel.hotelRating == "2" || hotel.hotelRating == "TwoStar") && (hotelPrice >= minPrice && hotelPrice <= maxPrice)) {
                            filterName.each {
                                String filter = it
                                String hType = hotel.hotelName
                                print('--------------- htype is -----------------' + hType)
                                if (filter.equals('allHotelType')) {
                                    priceRangeHotelList.add(hotel)
                                } else {
                                    if (hType.contains(filter))
                                        priceRangeHotelList.add(hotel)
                                }
                            }
                        }
                    } else if (it.equals('threeStar')) {
                        print('----------- in 3 -------------------')
                        if ((hotel.hotelRating == "3" || hotel.hotelRating == "ThreeStar") && (hotelPrice >= minPrice && hotelPrice <= maxPrice)) {
                            filterName.each {
                                String filter = it
                                String hType = hotel.hotelName
                                print('--------------- htype is -----------------' + hType)
                                if (filter.equals('allHotelType')) {
                                    priceRangeHotelList.add(hotel)
                                } else {
                                    if (hType.contains(filter))
                                        priceRangeHotelList.add(hotel)
                                }
                            }
                        }
                    } else if (it.equals('fourStar')) {
                        print('----------- in 4 -------------------')
                        if ((hotel.hotelRating == "4" || hotel.hotelRating == "FourStar") && (hotelPrice >= minPrice && hotelPrice <= maxPrice)) {
                            filterName.each {
                                String filter = it
                                String hType = hotel.hotelName
                                print('--------------- htype is -----------------' + hType)
                                if (filter.equals('allHotelType')) {
                                    priceRangeHotelList.add(hotel)
                                } else {
                                    if (hType.contains(filter))
                                        priceRangeHotelList.add(hotel)
                                }
                            }
                        }
                    } else if (it.equals('fiveStar')) {
                        print('----------- in 5 -------------------')
                        if ((hotel.hotelRating == "5" || hotel.hotelRating == "FiveStar") && (hotelPrice >= minPrice && hotelPrice <= maxPrice)) {
                            filterName.each {
                                String filter = it
                                String hType = hotel.hotelName
                                print('--------------- htype is -----------------' + hType)
                                if (filter.equals('allHotelType')) {
                                    priceRangeHotelList.add(hotel)
                                } else {
                                    if (hType.contains(filter))
                                        priceRangeHotelList.add(hotel)
                                }
                            }
                        }
                    } else if (it.equals('all') && (hotelPrice >= minPrice && hotelPrice <= maxPrice)) {
                        filterName.each {
                            String filter = it
                            String hType = hotel.hotelName
                            print('--------------- htype is -----------------' + hType)
                            if (filter.equals('allHotelType')) {
                                priceRangeHotelList.add(hotel)
                            } else {
                                if (hType.contains(filter))
                                    priceRangeHotelList.add(hotel)
                            }
                        }
                    }
                }
            }
        }
        finalHotelList = priceRangeHotelList
        hotelListPaginate = finalHotelList ? finalHotelList.subList(offset, (offset + max) > priceRangeHotelList.size() ? priceRangeHotelList.size() : offset + max) : []
        print('----------- final hotel list paginate-----------' + hotelListPaginate)
        render(template: "/hotelTemplates/hotelSearchResult", model: [hotelList: hotelListPaginate, minPrice: minPrice, maxPrice:
                maxPrice, total                                                : priceRangeHotelList.size(), searchParametersCO: hotelSearchParamsVO])


    }


    def searchFacilityFilter = {
        print('----------------- params are-------------' + params)
        HotelSearchParamsVO hotelSearchParamsVO = session["hotelSearchParamVO"] as HotelSearchParamsVO
        String mnPrice = params.priceRangeMin
        String mxPrice = params.priceRangeMax
        Integer minPrice = mnPrice ? mnPrice?.tokenize("INR")?.first() as Integer : 200
        Integer maxPrice = mxPrice ? mxPrice?.tokenize("INR")?.first() as Integer : 500000
        Integer total = 0
        String searchFilter = params.searchFilter
        String facilityFilter = params.facilityFilter

        Integer max = params.max ? params.int('max') : 10
        Integer offset = params.offset ? params.int('offset') : 0
        List<SearchHotelVO> finalHotelList = []
        List<SearchHotelVO> facilityFilterSearchList = []

        def hotelList = session["hotelList"] as List

        if (searchFilter.equals("facilities")) {
            String facility = it
            println '==========HotelFacilities==================' + it
            if (facilityFilter.equalsIgnoreCase("ac")) {
                if (facility.equalsIgnoreCase("Air conditioning") || facility.equalsIgnoreCase("Air conditioner")) {
                    print 'iiiiiiiiiii inside ac return true-----------------'
                    hotelList.each {
                        facilityFilterSearchList.add(it)
                    }
                }
            } else if (facilityFilter.equalsIgnoreCase("Parking")) {
                if (facility.equalsIgnoreCase("parking")) {
                    print 'iiiiiiiiiii inside parking return true-----------------'
                    hotelList.each {
                        facilityFilterSearchList.add(it)
                    }
                }
            } else if (facilityFilter.equalsIgnoreCase("Spa")) {
                if (facility.equalsIgnoreCase("spa") || facility.equalsIgnoreCase("massage")) {
                    print 'iiiiiiiiiii inside spa return true-----------------'
                    hotelList.each {
                        facilityFilterSearchList.add(it)
                    }
                }
            } else if (facilityFilter.equalsIgnoreCase("Bar")) {
                if (facility.equalsIgnoreCase("bar")) {
                    print 'iiiiiiiiiii inside bar return true-----------------'
                    hotelList.each {
                        facilityFilterSearchList.add(it)
                    }
                }
            } else if (facilityFilter.equalsIgnoreCase("Internet")) {
                if (facility.equalsIgnoreCase("internet") || facility.equalsIgnoreCase("WiFI")) {
                    print 'iiiiiiiiiii inside internet return true-----------------'
                    hotelList.each {
                        facilityFilterSearchList.add(it)
                    }
                }
            } else if (facilityFilter.equalsIgnoreCase("Restaurant")) {
                if (facility.equalsIgnoreCase("restaurant")) {
                    print 'iiiiiiiiiii inside restaurant return true-----------------'
                    hotelList.each {
                        facilityFilterSearchList.add(it)
                    }
                }
            } else if (facilityFilter.equalsIgnoreCase("Pool")) {
                if (facility.equalsIgnoreCase("pool")) {
                    print 'iiiiiiiiiii inside pool return true-----------------'
                    hotelList.each {
                        facilityFilterSearchList.add(it)
                    }
                }
            } else if (facilityFilter.equalsIgnoreCase("Gym")) {
                if (facility.equalsIgnoreCase("gym")) {
                    print 'iiiiiiiiiii inside gym return true-----------------'
                    hotelList.each {
                        facilityFilterSearchList.add(it)
                    }
                }
            }
        }
        hotelList = facilityFilterSearchList
        print('-------- here complete map is----------------' + facilityFilterSearchList)
        print('-------- here complete map is----------------' + hotelList.size())
        render(view: "/hotelTemplates/_hotelSearchResult", model: [maxPrice : maxPrice, minPrice: minPrice,
                                                                   hotelList: facilityFilterSearchList, total: hotelList.size(), hotelStarAll: params.hotelStarAll])
    }


    def hotelListBasedOnPriceRange = {
        print('----------------- params are---------' + params)
        List<String> filterName = params.list('selectedCheckBox[]') as List<String>
        print('========== hotel type are ============' + filterName)
        List<String> selectedCheckBoxHotelStar = params.list('selectedCheckBoxHotelStar[]') as List<String>
        print('================selected star----------' + selectedCheckBoxHotelStar)
        String mnPrice = params.priceRangeMin
        String mxPrice = params.priceRangeMax
        Integer minPrice = mnPrice.tokenize("INR").first() as Integer
        Integer maxPrice = mxPrice.tokenize("INR").first() as Integer

        Integer max = params.max ? params.int('max') : 10
        Integer offset = params.offset ? params.int('offset') : 0

        HotelSearchParamsVO hotelSearchParamsVO = session["hotelSearchParamVO"] as HotelSearchParamsVO
        List<SearchHotelVO> totalCountOfHotels = []
        List<SearchHotelVO> priceRangeHotelList = []
        List<SearchHotelVO> finalHotelList = []
        List<SearchHotelVO> hotelListPaginate = []

        totalCountOfHotels = session["hotelList"] as List

        totalCountOfHotels.each {
            String hPrice = it?.hotelRoomDetailVOArrayList?.hotelRateVO?.totalRate?.min()
            Float agencyMarkUp = it?.hotelRoomDetailVOArrayList?.hotelRateVO?.agentMarkUp?.first()?.toFloat()

            Float hotelPrice = hPrice.toFloat() + agencyMarkUp
            if (selectedCheckBoxHotelStar) {
                print('-----aaaaaaaa------ star type -------aaaaaaaaaa---------' + it)
                def hotel = it
                selectedCheckBoxHotelStar.each {
                    print('----------- star type ----------------' + it)
                    if (it.equals('oneStar')) {
                        print('----------- in 1 -------------------')
                        if ((hotel.hotelRating == "1" || hotel.hotelRating == "OneStar") && (hotelPrice >= minPrice && hotelPrice <= maxPrice)) {
                            filterName.each {
                                String filter = it
                                String hType = hotel.hotelName
                                print('--------------- htype is -----------------' + hType)
                                if (filter.equals('allHotelType')) {
                                    priceRangeHotelList.add(hotel)
                                } else {
                                    if (hType.contains(filter))
                                        priceRangeHotelList.add(hotel)
                                }
                            }
                        }
                    } else if (it.equals('twoStar')) {
                        print('----------- in 2 -------------------')
                        if ((hotel.hotelRating == "2" || hotel.hotelRating == "TwoStar") && (hotelPrice >= minPrice && hotelPrice <= maxPrice)) {
                            filterName.each {
                                String filter = it
                                String hType = hotel.hotelName
                                print('--------------- htype is -----------------' + hType)
                                if (filter.equals('allHotelType')) {
                                    priceRangeHotelList.add(hotel)
                                } else {
                                    if (hType.contains(filter))
                                        priceRangeHotelList.add(hotel)
                                }
                            }
                        }
                    } else if (it.equals('threeStar')) {
                        print('----------- in 3 -------------------')
                        if ((hotel.hotelRating == "3" || hotel.hotelRating == "ThreeStar") && (hotelPrice >= minPrice && hotelPrice <= maxPrice)) {
                            filterName.each {
                                String filter = it
                                String hType = hotel.hotelName
                                print('--------------- htype is -----------------' + hType)
                                if (filter.equals('allHotelType')) {
                                    priceRangeHotelList.add(hotel)
                                } else {
                                    if (hType.contains(filter))
                                        priceRangeHotelList.add(hotel)
                                }
                            }
                        }
                    } else if (it.equals('fourStar')) {
                        print('----------- in 4 -------------------')
                        if ((hotel.hotelRating == "4" || hotel.hotelRating == "FourStar") && (hotelPrice >= minPrice && hotelPrice <= maxPrice)) {
                            filterName.each {
                                String filter = it
                                String hType = hotel.hotelName
                                print('--------------- htype is -----------------' + hType)
                                if (filter.equals('allHotelType')) {
                                    priceRangeHotelList.add(hotel)
                                } else {
                                    if (hType.contains(filter))
                                        priceRangeHotelList.add(hotel)
                                }
                            }
                        }
                    } else if (it.equals('fiveStar')) {
                        print('----------- in 5 -------------------')
                        if ((hotel.hotelRating == "5" || hotel.hotelRating == "FiveStar") && (hotelPrice >= minPrice && hotelPrice <= maxPrice)) {
                            filterName.each {
                                String filter = it
                                String hType = hotel.hotelName
                                print('--------------- htype is -----------------' + hType)
                                if (filter.equals('allHotelType')) {
                                    priceRangeHotelList.add(hotel)
                                } else {
                                    if (hType.contains(filter))
                                        priceRangeHotelList.add(hotel)
                                }
                            }
                        }
                    } else if (it.equals('all') && (hotelPrice >= minPrice && hotelPrice <= maxPrice)) {
                        filterName.each {
                            String filter = it
                            String hType = hotel.hotelName
                            print('--------------- htype is -----------------' + hType)
                            if (filter.equals('allHotelType')) {
                                priceRangeHotelList.add(hotel)
                            } else {
                                if (hType.contains(filter))
                                    priceRangeHotelList.add(hotel)
                            }
                        }
                    }
                }
            }
        }
        finalHotelList = priceRangeHotelList
        hotelListPaginate = finalHotelList ? finalHotelList.subList(offset, (offset + max) > priceRangeHotelList.size() ? priceRangeHotelList.size() : offset + max) : []
        print('--------- size is-----------' + priceRangeHotelList.size())
        render(template: "/hotelTemplates/hotelSearchResult", model: [hotelList : hotelListPaginate, minPrice: minPrice, maxPrice:
                maxPrice, total                                                 : priceRangeHotelList.size(), searchParametersCO: hotelSearchParamsVO,
                                                                      noOfHotels: priceRangeHotelList.size()])
    }


    def hotelDetail = {

        log.info '===========Params=======' + params

        HotelSearchParamsVO hotelSearchParamsVO = session["hotelSearchParamVO"] as HotelSearchParamsVO
        String place = hotelSearchParamsVO?.place

        HotelDetailVO hotelDetailVO = new HotelDetailVO()

        hotelDetailVO.hotelId = params.id
        hotelDetailVO.hotelInstance = params.hotelInstance
        hotelDetailVO.sessionId = params.sessionId
        hotelDetailVO.hotelIndex = params.hotelIndex
        hotelDetailVO.supplierId = params.supplierId
        hotelDetailVO.price = params.price
        hotelDetailVO.noOfRooms = params.noOfRoom
        hotelDetailVO.adult = params.adult
        hotelDetailVO.child = params.child
        hotelDetailVO.checkIn = params.checkIn
        hotelDetailVO.checkOut = params.checkOut
        hotelDetailVO.peopleInRoomTwo = params.peopleInRoomTwo
        hotelDetailVO.peopleInRoomThree = params.peopleInRoomThree
        hotelDetailVO.peopleInRoomFour = params.peopleInRoomFour
        hotelDetailVO.childAgeRoomOne = params.childAgeRoomOne
        hotelDetailVO.childAgeRoomTwo
        hotelDetailVO.childAgeRoomThree = params.childAgeRoomThree
        hotelDetailVO.childAgeRoomFour = params.childAgeRoomFour

        hotelDetailVO.childInRoomTwo = params.childInRoomTwo
        hotelDetailVO.childInRoomThree = params.childInRoomThree
        hotelDetailVO.childInRoomFour = params.childInRoomFour

        List<SearchHotelVO> hotelList = session["hotelList"] as List
        String hotelIndex = params.hotelIndex

        SearchHotelVO hotelInstance1 = null
        hotelList.each {
            if (it.hotelIndex == hotelIndex) {
                hotelInstance1 = it
            } else {

            }
        }
//        if (params.sessionId) {
        HotelApiService hotelApiService = new HotelApiService()

        HotelVO hotelResult = hotelApiService.details(hotelDetailVO)

        log.info '========hotelDetails===111111=======' + hotelResult

        println '==========latitude=========' + hotelResult.geoData.latitude
        println '==========longitude=========' + hotelResult.geoData.longitude

//        }
        if (params.hotelIndex) {
            render(view: "/searchHotel/newThemed/forTBO/TboHotelSearchDetails", model: [hotelInstance1 : hotelInstance1, hotelIndex: params.hotelIndex, searchUrl: session["searchUrl"], price: hotelDetailVO.price, childAgeRoomFour: hotelDetailVO.childAgeRoomFour, childAgeRoomThree: hotelDetailVO.childAgeRoomThree,
                                                                                        childAgeRoomTwo: hotelDetailVO.childAgeRoomTwo, childAgeRoomOne: hotelDetailVO.childAgeRoomOne, childInRoomTwo: hotelDetailVO.childInRoomTwo, childInRoomThree: hotelDetailVO.childInRoomThree, childInRoomFour: hotelDetailVO.childInRoomFour, peopleInRoomTwo: hotelDetailVO.peopleInRoomTwo, peopleInRoomThree: hotelDetailVO.peopleInRoomThree, peopleInRoomFour: hotelDetailVO.peopleInRoomFour,
                                                                                        checkOut       : hotelDetailVO.checkOut, checkIn: hotelDetailVO.checkIn, child: hotelDetailVO.child, adult: hotelDetailVO.adult, noOfRoom: hotelDetailVO.noOfRooms, hotelVOInstance: hotelResult, place: place])
        } else {

            SearchHotelVO hotelInstance2 = null
            HotelVO hotelResult2 = hotelApiService.details(hotelDetailVO)

            hotelList.each {
                if (it.hotelId == hotelResult2.hotelId) {
                    hotelInstance2 = it
                }
            }
            def localityFetcher = hotelResult2.locality
            LocalityByIdVO locality1 = getLocalityById(localityFetcher)
            def locality = locality1.name


            List<HotelFacilityByIdVO> roomFacilityIdVOList = []
            hotelResult2.roomCategory.first().roomFacilities.each() { roomCat ->
                def roomId = roomCat.roomFacilityId
                roomFacilityIdVOList.add(roomId)
            }

            List<FacilityFromMGH> roomFacilityList = []
            roomFacilityIdVOList.each() { facId ->
                def facility = getFacilityById(facId)
                roomFacilityList.add(facility)
            }


            def loc = hotelResult2.location
            City city1 = City.findByLocationIdIlike(loc)
            def location = city1.name
            render(view: "/searchHotel/newThemed/hotelSearchDetails", model: [searchUrl      : session["searchUrl"], hotelInstance1: hotelInstance2, hotelList: hotelList, price: hotelDetailVO.price, supplierId: hotelDetailVO.supplierId, childAgeRoomFour: hotelDetailVO.childAgeRoomFour, childAgeRoomThree: hotelDetailVO.childAgeRoomThree,
                                                                              childAgeRoomTwo: hotelDetailVO.childAgeRoomTwo, childAgeRoomOne: hotelDetailVO.childAgeRoomOne, childInRoomTwo: hotelDetailVO.childInRoomTwo, childInRoomThree: hotelDetailVO.childInRoomThree, childInRoomFour: hotelDetailVO.childInRoomFour, peopleInRoomTwo: hotelDetailVO.peopleInRoomTwo, peopleInRoomThree: hotelDetailVO.peopleInRoomThree, peopleInRoomFour: hotelDetailVO.peopleInRoomFour,
                                                                              checkOut       : hotelDetailVO.checkOut, checkIn: hotelDetailVO.checkIn, child: hotelDetailVO.child, adult: hotelDetailVO.adult, noOfRoom: hotelDetailVO.noOfRooms, hotelVOInstance: hotelResult2, location: location, locality: locality, roomFacilityList: roomFacilityList])
        }
    }

    def hotelBooking = {

        HotelBookingVO hotelBookingVO = new HotelBookingVO()

        hotelBookingVO.childAgeRoomFour = params.childAgeRoomFour
        hotelBookingVO.childAgeRoomThree = params.childAgeRoomThree
        hotelBookingVO.childAgeRoomTwo = params.childAgeRoomTwo
        hotelBookingVO.childAgeRoomOne = params.childAgeRoomOne
        hotelBookingVO.hotelName = params.hotelName
        hotelBookingVO.hotelId = params.hotelId
        hotelBookingVO.childInRoomTwo = params.childInRoomTwo
        hotelBookingVO.childInRoomThree = params.childInRoomThree
        hotelBookingVO.childInRoomFour = params.childInRoomFour
        hotelBookingVO.peopleInRoomTwo = params.peopleInRoomTwo
        hotelBookingVO.peopleInRoomThree = params.peopleInRoomThree
        hotelBookingVO.peopleInRoomFour = params.peopleInRoomFour
        hotelBookingVO.noOfRoom = params.noOfRoom
        hotelBookingVO.hotelRoomCatId = params.hotelRoomCatId
        hotelBookingVO.checkOut = params.checkOut
        hotelBookingVO.checkIn = params.checkIn
        hotelBookingVO.child = params.child
        hotelBookingVO.adult = params.adult
        hotelBookingVO.supplierId = params.supplierId
        hotelBookingVO.roomType = params.roomType


        HotelApiService hotelApiService = new HotelApiService()

        def hotelResult = hotelApiService.book(hotelBookingVO)

        render("SUCCESS")

    }

    def takeOccupantInfo = {
        log.info '==============PARAMS===' + params
        println('11111111111111111111111111111111111111111111' + params)
        TboSearchHotelRoomDetailVO roomDetailVO = new TboSearchHotelRoomDetailVO()
        print('---------------- room details vo in take occupant info-----------' + roomDetailVO)
        String sessionId = null
        List<SearchHotelVO> hotelList = session["hotelList"] as List
        print('----------- hotel list in take occupant info==============' + hotelList)
        hotelList.each { hotel ->
            if (hotel.hotelIndex == params.hotelIndex) {
                sessionId = hotel.sessionId
                hotel.hotelRoomDetailVOArrayList.each { hotelRoom ->
                    if (hotelRoom.roomIndex == params.roomId) {
                        roomDetailVO.roomIndex = hotelRoom.roomIndex
                        roomDetailVO.amenities = hotelRoom.amenities
                        roomDetailVO.hotelRateVO = hotelRoom.hotelRateVO
                        print('-----aaa---------------- hotel rate vo---------' + hotelRoom.hotelRateVO)
                        print('-----bbb---------------- hotel rate vo---------' + roomDetailVO.hotelRateVO)

                        roomDetailVO.hotelRoomRateVO = hotelRoom.hotelRoomRateVO
                        print('-----ccc---------------- hotel rate vo---------' + hotelRoom.hotelRoomRateVO)
                        print('-----ddd---------------- hotel rate vo---------' + roomDetailVO.hotelRoomRateVO)
                        roomDetailVO.hotelWsOccupancyVO = hotelRoom.hotelWsOccupancyVO
                        roomDetailVO.ratePlanCOde = hotelRoom.ratePlanCOde
                        roomDetailVO.roomTypeName = hotelRoom.roomTypeName
                        roomDetailVO.roomTypeCode = hotelRoom.roomTypeCode
                    }
                }
            }
        }
        String noOfRoom = params.noOfRoom
        String child = params.child
        String childInRoomTwo = params.childInRoomTwo
        String childInRoomThree = params.childInRoomThree
        String childInRoomFour = params.childInRoomFour
        String adult = params.adult
        String peopleInRoomTwo = params.peopleInRoomTwo
        String peopleInRoomThree = params.peopleInRoomThree
        String peopleInRoomFour = params.peopleInRoomFour
        String checkOut = params.checkOut
        String checkIn = params.checkIn
        String trvlx360Tax = (roomDetailVO.hotelRateVO.totalTax).toBigDecimal().setScale(2, RoundingMode.HALF_UP) * 12.36 / 100
        print('==========ttxTax is============ ' + trvlx360Tax)
        log.info '=======Adult======' + adult
        log.info '-----------------' + noOfRoom
        log.info '-----------------' + roomDetailVO.hotelRateVO.totalRate
        def total = ((roomDetailVO.hotelRateVO.totalRate).toBigDecimal()?.setScale(2, RoundingMode.HALF_UP) + trvlx360Tax.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP) + (roomDetailVO.hotelRateVO.totalTax).toBigDecimal()?.setScale(2, RoundingMode.HALF_UP) + (roomDetailVO.hotelRateVO.agentMarkUp).toBigDecimal()?.setScale(2, RoundingMode.HALF_UP)
        ) * noOfRoom.toBigDecimal()
        log.info '-----------------' + total


        render(view: "/tboApi/addOccupantDetails", model: [peopleInRoomThree: peopleInRoomThree, peopleInRoomFour: peopleInRoomFour, sessionId: sessionId,
                                                           roomId           : params.roomId, hotelIndex: params.hotelIndex, roomDetailVO: roomDetailVO,
                                                           noOfRoom         : noOfRoom, child: child, childInRoomTwo: childInRoomTwo, childInRoomThree:
                childInRoomThree, childInRoomFour                           : childInRoomFour, adult: adult, peopleInRoomTwo: peopleInRoomTwo, checkOut: checkOut,
                                                           checkIn          : checkIn, total: total, trvlx360Tax: trvlx360Tax])
    }


    def bookingHotel = {
        println '=============Params=======' + params
        TboHotelBookingVO tboHotelBookingVO = new TboHotelBookingVO()
        tboHotelBookingVO.sessionId = params.sessionId
        tboHotelBookingVO.hotelIndex = params.hotelIndex
        tboHotelBookingVO.roomId = params.roomId

        tboHotelBookingVO.noOfRoom1 = params.noOfRoom
        tboHotelBookingVO.child = params.child
        tboHotelBookingVO.childInRoomTwo = params.childInRoomTwo
        tboHotelBookingVO.childInRoomThree = params.childInRoomThree
        tboHotelBookingVO.childInRoomFour = params.childInRoomFour
        tboHotelBookingVO.adult = params.adult
        tboHotelBookingVO.peopleInRoomTwo = params.peopleInRoomTwo
        tboHotelBookingVO.peopleInRoomThree = params.peopleInRoomThree
        tboHotelBookingVO.peopleInRoomFour = params.peopleInRoomFour
        tboHotelBookingVO.checkOut = params.checkOut
        tboHotelBookingVO.checkIn = params.checkIn

        tboHotelBookingVO.adult1FNameRoom1 = params.adult1FNameRoom1
        tboHotelBookingVO.adult1MNameRoom1 = params.adult1MNameRoom1
        tboHotelBookingVO.adult1LNameRoom1 = params.adult1LNameRoom1
        tboHotelBookingVO.adult1EmailRoom1 = params.adult1EmailRoom1
        tboHotelBookingVO.adult1CityRoom1 = params.adult1CityRoom1
        tboHotelBookingVO.adult1CityCode1 = params.adult1CityCode1
        tboHotelBookingVO.adult1PhoneRoom1 = params.adult1PhoneRoom1
        tboHotelBookingVO.adult1CountryRoom1 = params.adult1CountryRoom1
        tboHotelBookingVO.adult1CountryCode1 = params.adult1CountryCode1
        tboHotelBookingVO.adult1StateRoom1 = params.adult1StateRoom1
        tboHotelBookingVO.adult1AgeRoom1 = params.adult1AgeRoom1
        tboHotelBookingVO.adult1AddressOneRoom1 = params.adult1AddressOneRoom1
        tboHotelBookingVO.adult1AddressTwoRoom1 = params.adult1AddressTwoRoom1
        tboHotelBookingVO.adult1ZipRoom1 = params.adult1ZipRoom1

        tboHotelBookingVO.adult2FNameRoom1 = params.adult2FNameRoom1
        tboHotelBookingVO.adult2MNameRoom1 = params.adult2MNameRoom1
        tboHotelBookingVO.adult2LNameRoom1 = params.adult2LNameRoom1

        tboHotelBookingVO.adult3FNameRoom1 = params.adult3FNameRoom1
        tboHotelBookingVO.adult3MNameRoom1 = params.adult3MNameRoom1
        tboHotelBookingVO.adult3LNameRoom1 = params.adult3LNameRoom1

        tboHotelBookingVO.adult4FNameRoom1 = params.adult4FNameRoom1
        tboHotelBookingVO.adult4MNameRoom1 = params.adult4MNameRoom1
        tboHotelBookingVO.adult4LNameRoom1 = params.adult4LNameRoom1

        tboHotelBookingVO.adult5FNameRoom1 = params.adult5FNameRoom1
        tboHotelBookingVO.adult5MNameRoom1 = params.adult5MNameRoom1
        tboHotelBookingVO.adult5LNameRoom1 = params.adult5LNameRoom1

        tboHotelBookingVO.child1Room1FName = params.child1Room1FName
        tboHotelBookingVO.child1Room1MName = params.child1Room1MName
        tboHotelBookingVO.child1Room1LName = params.child1Room1LName

        tboHotelBookingVO.child2Room1FName = params.child2Room1FName
        tboHotelBookingVO.child2Room1MName = params.child2Room1MName
        tboHotelBookingVO.child2Room1LName = params.child2Room1LName

        tboHotelBookingVO.child3Room1FName = params.child3Room1FName
        tboHotelBookingVO.child3Room1MName = params.child3Room1MName
        tboHotelBookingVO.child3Room1LName = params.child3Room1LName

        tboHotelBookingVO.child4Room1FName = params.child4Room1FName
        tboHotelBookingVO.child4Room1MName = params.child4Room1MName
        tboHotelBookingVO.child4Room1LName = params.child4Room1LName

        tboHotelBookingVO.adult1FNameRoom2 = params.adult1FNameRoom2
        tboHotelBookingVO.adult1MNameRoom2 = params.adult1MNameRoom2
        tboHotelBookingVO.adult1LNameRoom2 = params.adult1LNameRoom2

        tboHotelBookingVO.adult2FNameRoom2 = params.adult2FNameRoom2
        tboHotelBookingVO.adult2MNameRoom2 = params.adult2MNameRoom2
        tboHotelBookingVO.adult2LNameRoom2 = params.adult2LNameRoom2

        tboHotelBookingVO.adult3FNameRoom2 = params.adult3FNameRoom2
        tboHotelBookingVO.adult3MNameRoom2 = params.adult3MNameRoom2
        tboHotelBookingVO.adult3LNameRoom2 = params.adult3LNameRoom2

        tboHotelBookingVO.adult4FNameRoom2 = params.adult4FNameRoom2
        tboHotelBookingVO.adult4MNameRoom2 = params.adult4MNameRoom2
        tboHotelBookingVO.adult4LNameRoom2 = params.adult4LNameRoom2

        tboHotelBookingVO.adult5FNameRoom2 = params.adult5FNameRoom2
        tboHotelBookingVO.adult5MNameRoom2 = params.adult5MNameRoom2
        tboHotelBookingVO.adult5LNameRoom2 = params.adult5LNameRoom2

        tboHotelBookingVO.child1Room2FName = params.child1Room2FName
        tboHotelBookingVO.child1Room2MName = params.child1Room2MName
        tboHotelBookingVO.child1Room2LName = params.child1Room2LName

        tboHotelBookingVO.child2Room2FName = params.child2Room2FName
        tboHotelBookingVO.child2Room2MName = params.child2Room2MName
        tboHotelBookingVO.child2Room2LName = params.child2Room2LName

        tboHotelBookingVO.child3Room2FName = params.child3Room2FName
        tboHotelBookingVO.child3Room2MName = params.child3Room2MName
        tboHotelBookingVO.child3Room2LName = params.child3Room2LName

        tboHotelBookingVO.child4Room2FName = params.child4Room2FName
        tboHotelBookingVO.child4Room2MName = params.child4Room2MName
        tboHotelBookingVO.child4Room2LName = params.child4Room2LName


        tboHotelBookingVO.adult1FNameRoom3 = params.adult1FNameRoom3
        tboHotelBookingVO.adult1MNameRoom3 = params.adult1MNameRoom3
        tboHotelBookingVO.adult1LNameRoom3 = params.adult1LNameRoom3

        tboHotelBookingVO.adult2FNameRoom3 = params.adult2FNameRoom3
        tboHotelBookingVO.adult2MNameRoom3 = params.adult2MNameRoom3
        tboHotelBookingVO.adult2LNameRoom3 = params.adult2LNameRoom3

        tboHotelBookingVO.adult3FNameRoom3 = params.adult3FNameRoom3
        tboHotelBookingVO.adult3MNameRoom3 = params.adult3MNameRoom3
        tboHotelBookingVO.adult3LNameRoom3 = params.adult3LNameRoom3

        tboHotelBookingVO.adult4FNameRoom3 = params.adult4FNameRoom3
        tboHotelBookingVO.adult4MNameRoom3 = params.adult4MNameRoom3
        tboHotelBookingVO.adult4LNameRoom3 = params.adult4LNameRoom3

        tboHotelBookingVO.adult5FNameRoom3 = params.adult5FNameRoom3
        tboHotelBookingVO.adult5MNameRoom3 = params.adult5MNameRoom3
        tboHotelBookingVO.adult5LNameRoom3 = params.adult5LNameRoom3

        tboHotelBookingVO.child1Room3FName = params.child1Room3FName
        tboHotelBookingVO.child1Room3MName = params.child1Room3MName
        tboHotelBookingVO.child1Room3LName = params.child1Room3LName

        tboHotelBookingVO.child2Room3FName = params.child2Room3FName
        tboHotelBookingVO.child2Room3MName = params.child2Room3MName
        tboHotelBookingVO.child2Room3LName = params.child2Room3LName

        tboHotelBookingVO.child3Room3FName = params.child3Room3FName
        tboHotelBookingVO.child3Room3MName = params.child3Room3MName
        tboHotelBookingVO.child3Room3LName = params.child3Room3LName

        tboHotelBookingVO.child4Room3FName = params.child4Room3FName
        tboHotelBookingVO.child4Room3MName = params.child4Room3MName
        tboHotelBookingVO.child4Room3LName = params.child4Room3LName


        tboHotelBookingVO.adult1FNameRoom4 = params.adult1FNameRoom4
        tboHotelBookingVO.adult1MNameRoom4 = params.adult1MNameRoom4
        tboHotelBookingVO.adult1LNameRoom4 = params.adult1LNameRoom4

        tboHotelBookingVO.adult2FNameRoom4 = params.adult2FNameRoom4
        tboHotelBookingVO.adult2MNameRoom4 = params.adult2MNameRoom4
        tboHotelBookingVO.adult2LNameRoom4 = params.adult2LNameRoom4

        tboHotelBookingVO.adult3FNameRoom4 = params.adult3FNameRoom4
        tboHotelBookingVO.adult3MNameRoom4 = params.adult3MNameRoom4
        tboHotelBookingVO.adult3LNameRoom4 = params.adult3LNameRoom4

        tboHotelBookingVO.adult4FNameRoom4 = params.adult4FNameRoom4
        tboHotelBookingVO.adult4MNameRoom4 = params.adult4MNameRoom4
        tboHotelBookingVO.adult4LNameRoom4 = params.adult4LNameRoom4

        tboHotelBookingVO.adult5FNameRoom4 = params.adult5FNameRoom4
        tboHotelBookingVO.adult5MNameRoom4 = params.adult5MNameRoom4
        tboHotelBookingVO.adult5LNameRoom4 = params.adult5LNameRoom4

        tboHotelBookingVO.child1Room4FName = params.child1Room4FName
        tboHotelBookingVO.child1Room4MName = params.child1Room4MName
        tboHotelBookingVO.child1Room4LName = params.child1Room4LName

        tboHotelBookingVO.child2Room4FName = params.child2Room4FName
        tboHotelBookingVO.child2Room4MName = params.child2Room4MName
        tboHotelBookingVO.child2Room4LName = params.child2Room4LName

        tboHotelBookingVO.child3Room4FName = params.child3Room4FName
        tboHotelBookingVO.child3Room4MName = params.child3Room4MName
        tboHotelBookingVO.child3Room4LName = params.child3Room4LName

        tboHotelBookingVO.child4Room4FName = params.child4Room4FName
        tboHotelBookingVO.child4Room4MName = params.child4Room4MName
        tboHotelBookingVO.child4Room4LName = params.child4Room4LName
        tboHotelBookingVO.totalAmmount = params.totalAmmount
        tboHotelBookingVO.countryName = params.country
        SetMargin setMargin = SetMargin.first()
        BigDecimal payUMoneyStake = ((params.totalAmmount as BigDecimal) * (setMargin.payUMoneyStake as BigDecimal)) / 100
        print('-----------111111111111111-------------' + payUMoneyStake)
        String totalAmount = params.totalAmmount
        print('-----------total ammount is-------------' + totalAmount)

        String total = (payUMoneyStake?.setScale(2, RoundingMode.HALF_UP) + totalAmount.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP)).toString()
        print('-----------total is-------------' + total)

        HotelApiService hotelApiService = new HotelApiService()
        session['tboHotelBookingVO'] = null


        session['tboHotelBookingVO'] = tboHotelBookingVO as TboHotelBookingVO

        String randomAlphanumeric = org.apache.commons.lang.RandomStringUtils.randomAlphanumeric(8)
        Date today = new Date()
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat('dd/MM/yyyy')
        String[] date = simpleDateFormat.format(today).split('/')
        def tranId = "${date[0]}" + "${date[1]}" + "${date[2]}" + randomAlphanumeric.toUpperCase() + "TR360"
//        String message = "JBZaLc|${tranId}|${total}|${params.productName}|${params.adult1MNameRoom1}|${params.adult1EmailRoom1}|||||||||||GQs7yium"
        String message = "${grailsApplication.config.payUMoney.Key}|${tranId}|${total}|${params.productName}|${params.adult1MNameRoom1}|${params.adult1EmailRoom1}|||||||||||${grailsApplication.config.payUMoney.Salt}"

        println '=========message====' + message
        String hash = hashCalculationService.calculateHashCode(message)
        println '=========hash====' + hash
        String serverUrl = grailsApplication.config.grails.serverURL
        log.info "-------- No of rooms-----------------------" + tboHotelBookingVO.noOfRoom1
        render(view: "/payuform", model: [hash                                  : hash, tranId: tranId, serverUrl: serverUrl, productInfo: params.productName, total:
                total, fName                                                    : params.adult1MNameRoom1, email: params.adult1EmailRoom1, phone:
                                                  params.adult1PhoneRoom1, lName: tboHotelBookingVO.adult1LNameRoom1, noOfRoom: tboHotelBookingVO?.noOfRoom1,
                                          payUMoneyStake                        : payUMoneyStake?.setScale(2, RoundingMode.HALF_UP), totalAmount: totalAmount])
    }

    def processBooking() {
        log.info '==========PARAMS====processBooking==1111=='
        log.info '==========PARAMS====processBooking===3333=' + params

        TboHotelBookingVO tboHotelBookingVO = session['tboHotelBookingVO'] as TboHotelBookingVO

        log.info '==========PARAMS====processBooking=22222==='
        log.info '==========PARAMS====processBooking=22222===' + tboHotelBookingVO
        log.info '==========PARAMS====processBooking==4444==' + tboHotelBookingVO?.adult1EmailRoom1

        HotelApiService hotelApiService = new HotelApiService()
        def bookRefId = hotelApiService.book(tboHotelBookingVO)
        log.info '==========PARAMS====book ref id====' + bookRefId

        if (bookRefId) {


            Role role = Role.findByAuthority('ROLE_USER')
            String charset = (('A'..'Z') + ('0'..'9')).join()
            Integer length = 9
            String bookingDescription = session["bookingDescription"]
            User user = User.findByUsername(tboHotelBookingVO.adult1EmailRoom1)
            String pass
            if (user) {
                print('---------- user alreday existin db')
            } else {
                user = new User()
                user.createOrCheckUserWhileBookHotel(tboHotelBookingVO)
                pass = org.apache.commons.lang.RandomStringUtils.randomAlphanumeric(8)
                print('-------------- user pass is--------------------' + pass)
                user.password = pass
                user.mobNo = params.phone
                user.save(flush: true)
                UserRole.create(user, role, true)
            }
//            def client2 = new SOAPClient("http://api.tektravels.com/tbohotelapi_v6/hotelservice.asmx")
            def client2 = new SOAPClient("http://airapi.travelboutiqueonline.com/tbohotelapi_v6/hotelservice.asmx")

            SOAPResponse response2 = client2.send(
                    """<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <AuthenticationData xmlns="http://TekTravel/HotelBookingApi">
      <SiteName></SiteName>
      <AccountCode></AccountCode>
    <UserName>${grailsApplication.config.tbo.TRAVELX_UNAME}</UserName>
<Password>${grailsApplication.config.tbo.TRAVELX_PASS_HOTEL}</Password>
    </AuthenticationData>
  </soap:Header>
  <soap:Body>
    <GetHotelBooking xmlns="http://TekTravel/HotelBookingApi">
      <request>
        <BookingId>${bookRefId}</BookingId>
      </request>
    </GetHotelBooking>
  </soap:Body>
</soap:Envelope>"""
            )
            log.info '---==response==3333==' + response2.text
            def ref = new XmlSlurper().parseText(response2.text)
            def bookingResult = ref.children().GetHotelBookingResponse.GetHotelBookingResult.BookingDetail
            BookingHistory bookingHistory = new BookingHistory()
            bookingHistory.saveBookingHistory(bookingResult, tboHotelBookingVO)
            bookingHistory.user = user
            user.addToBookingHistory(bookingHistory)
            bookingHistory.save(flush: true)
            print('----- booking history is --------------' + bookingHistory)

            HotelForBooking hotelForBooking = new HotelForBooking()
            def hotelBooked = hotelForBooking.saveHotelDetails(hotelForBooking, bookingResult)
            hotelBooked.bookingHistory = bookingHistory
            hotelBooked.save(flush: true)



            bookingResult.Roomtype.WSHotelRoom.each { hotelRoom ->
                print('----------bookingResult.Roomtype.WSHotelRoom--------' + hotelRoom.AdultCount)
                print('----------bookingResult.Roomtype.WSHotelRoom--------' + hotelRoom.ChildCount)

                RoomBooked roomBooked = new RoomBooked()
                roomBooked.roomCategory = hotelRoom.RoomName
                roomBooked.cancelPolicy = bookingResult.HotelCancelPolicy
                String cancellationDate = bookingResult.LastCancellationDate as String
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat('yyyy-MM-dd')
                roomBooked.lastCancellationDate = simpleDateFormat.parse(cancellationDate.substring(0, 10))
                roomBooked.hotelForBooking = hotelForBooking
                hotelForBooking.addToRoomBooked(roomBooked)
                roomBooked.save(flush: true)
                print('---------hotelRoom.GuestInfo.WSGuest--------------' + hotelRoom.GuestInfo.WSGuest.Title)
                print('---------hotelRoom.GuestInfo.WSGuest--------------' + hotelRoom.GuestInfo.WSGuest.FirstName)
                print('---------hotelRoom.GuestInfo.WSGuest--------------' + hotelRoom.GuestInfo.WSGuest.LastName)
                hotelRoom.GuestInfo.WSGuest.each { guest ->
                    print('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')

                    String guestType = guest.GuestType
                    print('=========guest type ===================' + guestType)
                    print('=========guest type ===================' + guestType.equals("Adult"))
                    if (guestType.equals("Adult")) {
                        Adult adult = new Adult()
                        adult.adultTitle = guest.Title
                        print('---------------title--in adult---------------------' + guest.Title)
                        adult.adultFirstName = guest.FirstName
                        print('---------------fname------adult-----------------' + guest.FirstName)
                        adult.adultLastName = guest.LastName
                        print('---------------lname-----adult------------------' + guest.LastName)
                        adult.laedPassanger = guest.LeadGuest
                        adult.roomBooked = roomBooked
                        roomBooked.addToAdults(adult)
                        adult.save(flush: true)
                    } else {
                        Child child = new Child()
                        child.roomBooked = roomBooked
                        child.childTitle = guest.Title
                        print('---------------title--in child---------------------' + guest.Title)
                        child.childFirstName = guest.FirstName
                        print('---------------title--in child---------------------' + guest.Title)
                        child.childLastName = guest.LastName
                        print('---------------title--in child---------------------' + guest.Title)
                        roomBooked.addToChild(child)
                        child.save(flush: true)
                    }
                }
            }
            print('----- booking history is --------------' + bookingHistory)
            SetMargin setMargin = SetMargin.findByMarginType(MarginType.DOMHOTEL)
            PaymentInfo paymentInfo = new PaymentInfo()
            paymentInfo.savePaymentInfoFromPayUMoney(params)
            bookingResult.Roomtype.WSHotelRoom.RoomRate.each {
                String tboRoomRate = it.TotalRoomPrice
                print('-------tbo room rate is---------' + it.TotalRoomPrice)
                paymentInfo.amount = (tboRoomRate.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP) + setMargin.marginMoney.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP)).toString()
                String tboTax = it.TotalRoomTax
                print('-------tbo room tax is---------' + it.TotalRoomTax)
                paymentInfo.totalTax = (tboTax.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP) + (tboTax.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP) * 12.36 / 100)).toString()

            }
            paymentInfo.bookingHistory = bookingHistory
            SetMargin setMargin1 = SetMargin.first()
            setMargin1.payUMoneyStake

            paymentInfo.payUMoneyStake = ((paymentInfo.amount.toBigDecimal().setScale(2, RoundingMode.HALF_UP) + paymentInfo.totalTax.toBigDecimal().setScale(2, RoundingMode.HALF_UP)) * (setMargin1.payUMoneyStake as BigDecimal) / 100).toString()

            paymentInfo.save(flush: true)
            String totalAmount = (paymentInfo.amount.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP) + paymentInfo.totalTax.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP)).toString()
            try {
                mailService.sendMail {
                    to user?.username
                    subject "Booking details"
                    html g.render(template: '/template/bookingConfirmationDetail', model: [bookingId: bookingHistory?.bookingIdTrvlx, userName:
                            user?.username, pass                                                    : pass, name: user?.firstName], plugin: "email-confirmation")
                }
            }
            catch (Exception exc) {
                log.info('---------- exception occured while sending mail-------------------' + exc.printStackTrace())
            }
            try {
                mailService.sendMail {
                    to user?.username
                    subject "Booking Invoice"
                    html g.render(template: "/hotelTemplates/invoiceTemplate", model: [paymentInfo: paymentInfo, bookingHistory: bookingHistory, roomBookedList: roomBookedList, hotelForBooking: hotelForBooking], plugin: "email-confirmation")
                }
            }
            catch (Exception exc) {
                log.info('---------- exception occured while sending mail-------------------' + exc.printStackTrace())
            }
            List<RoomBooked> roomBookedList = RoomBooked.findAllByHotelForBooking(hotelForBooking)
            render(view: "/searchHotel/newThemed/forTBO/tboBookingStatus", model: [bookingHistory: bookingHistory, amount:
                    totalAmount, hotelForBooking                                                 : hotelForBooking, roomBookedList:
                                                                                           roomBookedList])
        } else {
            render(view: '/hotelTemplates/technicalProblem')
        }
    }

    def getCancelPolicy = {
        log.info '============' + params
        Integer indexNo = params.roomId as Integer
//        def client = new SOAPClient("http://api.tektravels.com/tbohotelapi_v6/hotelservice.asmx")
        def client = new SOAPClient("http://airapi.travelboutiqueonline.com/tbohotelapi_v6/hotelservice.asmx")

        def response = client.send("""<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
<soap:Header>
<AuthenticationData xmlns="http://TekTravel/HotelBookingApi">
<SiteName></SiteName>
<AccountCode></AccountCode>
<UserName>${grailsApplication.config.tbo.TRAVELX_UNAME}</UserName>
<Password>${grailsApplication.config.tbo.TRAVELX_PASS_HOTEL}</Password>
</AuthenticationData>
</soap:Header>
<soap:Body>
<GetCancellationPolicy xmlns="http://TekTravel/HotelBookingApi">
      <request>
        <Index>${indexNo}</Index>
        <RatePlanCode>${params.rateCode}</RatePlanCode>
        <RoomTypeCode>${params.roomCode}</RoomTypeCode>
        <SessionId>${params.sessionId}</SessionId>
      </request>
    </GetCancellationPolicy>
</soap:Body>
</soap:Envelope>"""

        )
        log.info '==============cancellation response=====' + response.text
        def ref = new XmlSlurper().parseText(response.text)
        List<String> cancelPolicy = []
        ref.children().GetCancellationPolicyResponse.GetCancellationPolicyResult.CancellationPolicy.string.each {
            cancelPolicy.add(it)
        }
        println('------- cancel policy are ----------------' + cancelPolicy)
        ref.children().GetCancellationPolicyResponse.GetCancellationPolicyResult.HotelNorms.string.each {
            cancelPolicy.add(it)
        }
        render(template: "/searchHotel/newThemed/forTBO/cancelPolicy", model: [cancelPolicy: cancelPolicy, indexNo: indexNo])

    }


    def saveAllCities = {
        List<CountryFromTbo> countryFromTbo = CountryFromTbo.getAll()
        countryFromTbo.each { country ->
            if (!country.countryName.contains("&")) {

//                def client2 = new SOAPClient("http://api.tektravels.com/tbohotelapi_v6/hotelservice.asmx")
                def client2 = new SOAPClient("http://airapi.travelboutiqueonline.com/tbohotelapi_v6/hotelservice.asmx")

                SOAPResponse response2 = client2.send(
                        """<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
<soap:Header>
<AuthenticationData xmlns="http://TekTravel/HotelBookingApi">
<SiteName></SiteName>
<AccountCode></AccountCode>
<UserName>${grailsApplication.config.tbo.TRAVELX_UNAME}</UserName>
<Password>${grailsApplication.config.tbo.TRAVELX_PASS_HOTEL}</Password>
</AuthenticationData>
</soap:Header>
<soap:Body>
 <GetDestinationCityList xmlns="http://TekTravel/HotelBookingApi">
      <request>
        <CountryName>${country.countryName}</CountryName>
      </request>
    </GetDestinationCityList>
</soap:Body>
</soap:Envelope>"""
                )
//            println '---==response==3333==' + response2
                def ref = new XmlSlurper().parseText(response2.text)
                ref.children().GetDestinationCityListResponse.GetDestinationCityListResult.CityList.WSCity.eachWithIndex { city, itr ->
                    println '======== CityCode====' + city.CityCode
                    println '======== CityName====' + city.CityName
                    println '======== SPACE===='

                    println '======== FOR COUNTRY====' + country

//            CountryFromTbo countryFromTbo1 = CountryFromTbo.findByCountryName("India")

                    CityFromTBO cityFromTBO = new CityFromTBO(city, country)
//            CityFromTBO cityFromTBO = new CityFromTBO(city, countryFromTbo1)
                    cityFromTBO.save(flush: true)
                }

                println '========string Country====' + country.countryName
            }
        }
        render("SUCCESS")
    }


    def saveMghCity = {
        def a = ServletContextHolder.getServletContext().getRealPath("/")
        log.info '======AfterGetting path========'
        def file = new File(a, "/WEB-INF/xmlFiles/getAllLocations.xml")
        println '=====File=====' + file
        def locations = new XmlParser().parse(file)
        locations.children().location.eachWithIndex { locationData, itr ->
            if (itr == City.count()) {
                City city = new City(locationData)
                println '=======City========' + locationData.attribute("countryCode")
                city.save(flush: true)
                println '=======City= saved===at position====' + itr
            }
        }
    }

    def feedBack = {
        log.info '=======Params====' + params
        log.info '=======Params====' + params.message
        log.info '=======Params====' + params.email
        log.info '=======Params====' + params.subject
        log.info '=======Params====' + params.name

        UserFeedBack userFeedBack = new UserFeedBack()
        userFeedBack.name = params.name
        userFeedBack.email = params.email
        userFeedBack.subject = params.subject
        userFeedBack.message = params.message
        userFeedBack.save(flush: true)
    }

    def confirmBooking = {

        print('------  in confirm booking --------------' + params)
        String totalRate = params.totalRate
        String bookingId = params.bookingId
//        def client2 = new SOAPClient("http://api.tektravels.com/tbohotelapi_v6/hotelservice.asmx")
        def client2 = new SOAPClient("http://airapi.travelboutiqueonline.com/tbohotelapi_v6/hotelservice.asmx")

        SOAPResponse response2 = client2.send(
                """<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <AuthenticationData xmlns="http://TekTravel/HotelBookingApi">
      <SiteName></SiteName>
      <AccountCode></AccountCode>
    <UserName>${grailsApplication.config.tbo.TRAVELX_UNAME}</UserName>
<Password>${grailsApplication.config.tbo.TRAVELX_PASS_HOTEL}</Password>
    </AuthenticationData>
  </soap:Header>
  <soap:Body>
    <GetHotelBooking xmlns="http://TekTravel/HotelBookingApi">
      <request>
        <BookingId>${bookingId}</BookingId>
      </request>
    </GetHotelBooking>
  </soap:Body>
</soap:Envelope>"""
        )
        log.info '---==response==3333==' + response2.text
        def ref = new XmlSlurper().parseText(response2.text)

        def bookingResult = ref.children().GetHotelBookingResponse.GetHotelBookingResult

        def bookingDetail = bookingResult.BookingDetail


        TboHotelDetailVO hotelDetailVO = new TboHotelDetailVO()

        hotelDetailVO.description = bookingResult.Status.Description
        hotelDetailVO.category = bookingResult.Status.Category
        hotelDetailVO.bookingId = bookingDetail.BookingId
        hotelDetailVO.bookingRefNo = bookingDetail.BookingRefNo
        hotelDetailVO.confirmationNo = bookingDetail.ConfirmationNo
        hotelDetailVO.hotelName = bookingDetail.HotelName
        hotelDetailVO.rating = bookingDetail.Rating
        hotelDetailVO.checkInDate = bookingDetail.CheckInDate
        hotelDetailVO.checkOutDate = bookingDetail.CheckOutDate
        hotelDetailVO.hotelAddress1 = bookingDetail.HotelAddress1
        hotelDetailVO.hotelAddress2 = bookingDetail.HotelAddress2
        hotelDetailVO.cityId = bookingDetail.CityId
        hotelDetailVO.cityRef = bookingDetail.CityRef
        hotelDetailVO.flightInfo = bookingDetail.FlightInfo
        hotelDetailVO.specialRequest = bookingDetail.SpecialRequest
        hotelDetailVO.numberOfRooms = bookingDetail.NoOfRooms
        hotelDetailVO.hotelCancelPolicy = bookingDetail.HotelCancelPolicy
        hotelDetailVO.hotelPolicyDetails = bookingDetail.HotelPolicyDetails
        hotelDetailVO.guestTitle = bookingDetail.Guest.Title
        hotelDetailVO.guestFirstName = bookingDetail.Guest.FirstName
        hotelDetailVO.guestMiddleName = bookingDetail.Guest.MiddleName
        hotelDetailVO.guestLastName = bookingDetail.Guest.LastName
        hotelDetailVO.guestIsLead = bookingDetail.Guest.LeadGuest
        hotelDetailVO.guestAge = bookingDetail.Guest.Age
        hotelDetailVO.guestAddress1 = bookingDetail.Guest.Addressline1
        hotelDetailVO.guestAddress2 = bookingDetail.Guest.Addressline1
        hotelDetailVO.guestCountryCode = bookingDetail.Guest.Countrycode
        hotelDetailVO.guestAreaCode = bookingDetail.Guest.Areacode
        hotelDetailVO.guestPhoneNo = bookingDetail.Guest.Areacode
        hotelDetailVO.guestEmail = bookingDetail.Guest.Email
        hotelDetailVO.guestCity = bookingDetail.Guest.City
        hotelDetailVO.guestState = bookingDetail.Guest.State
        hotelDetailVO.guestCountry = bookingDetail.Guest.Country
        hotelDetailVO.guestZipCode = bookingDetail.Guest.Zipcode
        hotelDetailVO.roomIndex = bookingDetail.Guest.RoomIndex
        hotelDetailVO.isDomestic = bookingDetail.IsDomestic
        hotelDetailVO.bookingDate = bookingDetail.BookingDate
        hotelDetailVO.lastCancellationDate = bookingDetail.LastCancellationDate
        hotelDetailVO.map = bookingDetail.Map

        hotelDetailVO.roomType = bookingDetail.Roomtype.WSHotelRoom.RoomName

        hotelDetailVO.bookingStatus = bookingDetail.BookingStatus
        hotelDetailVO.agencyRef = bookingDetail.AgencyReference
        hotelDetailVO.bookingRefType = bookingDetail.BookingReferenceType
        hotelDetailVO.showAgencyRef = bookingDetail.ShowAgencyReference
        hotelDetailVO.agencyName = bookingDetail.Agency.Name
        hotelDetailVO.agencyAddress1 = bookingDetail.Agency.AddressLine1
        hotelDetailVO.agencyAddress2 = bookingDetail.Agency.AddressLine2
        hotelDetailVO.agencyEmail = bookingDetail.Agency.Email
        hotelDetailVO.agencyPhone = bookingDetail.Agency.Phone
        hotelDetailVO.agencyFax = bookingDetail.Agency.Fax
        hotelDetailVO.agencyCity = bookingDetail.Agency.City
        hotelDetailVO.agencyPin = bookingDetail.Agency.PIN


        OrderDetails orderDetails = new OrderDetails()
        orderDetails.guestName = bookingDetail.Guest.FirstName
        orderDetails.guestPhoneNo = bookingDetail.Guest.Areacode
        orderDetails.guestEmail = bookingDetail.Guest.Email
        orderDetails.checkInDate = bookingDetail.CheckInDate
        orderDetails.checkOutDate = bookingDetail.CheckOutDate
        orderDetails.hotelName = bookingDetail.HotelName
        orderDetails.numberOfRoomsBooked = bookingDetail.NoOfRooms
        orderDetails.bookingId = bookingDetail.BookingId
        orderDetails.bookingRefNo = bookingDetail.BookingRefNo
        orderDetails.confirmationNo = bookingDetail.ConfirmationNo
        orderDetails.bookingDate = bookingDetail.BookingDate
        orderDetails.hotelCancelPolicy = bookingDetail.HotelCancelPolicy

        orderDetails.orderNumber = UUID.randomUUID().toString()

        orderDetails.save(flush: true)

        String userEmail = bookingDetail.Guest.Email
        println '==========USER======' + userEmail

        User user = User.findByUsername(userEmail)
        println '==========USER======' + user.username
        if (user) {
            BookingHistory bookingHistory = new BookingHistory()
            bookingHistory.hotelName = bookingDetail.HotelName
            bookingHistory.hotelId = bookingDetail.HotelName
            bookingHistory.bookRefNo = bookingDetail.BookingId
            bookingHistory.checkIn = bookingDetail.CheckInDate
            bookingHistory.checkOut = bookingDetail.CheckOutDate
            bookingHistory.noOfRooms = bookingDetail.NoOfRooms
            bookingHistory.roomCategory = bookingDetail.Roomtype.WSHotelRoom.RoomName
            bookingHistory.bookingAmmount = totalRate
            bookingHistory.user = user

            println '==========Booking History======'
            print('4444444444444444444444444444444444444444444444444' + user)


            if (!bookingHistory.validate()) {
                bookingHistory.errors.allErrors.each {
                    println '----------------' + it
                }
            }
            bookingHistory.save(flush: true)
            println '========AFTER==Booking History======'
            print('5555555555555555555555555555555555555555555555555555555555' + hotelDetailVO)
            print('6666666666666666666666666666666666666666666666666666666666' + orderDetails)

        }
        render(view: "/searchHotel/newThemed/forTBO/tboBookingStatus", model: [hotelDetailVO: hotelDetailVO, orderDetails:
                orderDetails, user                                                          : user, bookingId: bookingId])


    }


    def getDestinationCityList = {
//        String country = params.countryName
        String country = "India"

//        def client2 = new SOAPClient("http://api.tektravels.com/tbohotelapi_v6/hotelservice.asmx")
        def client2 = new SOAPClient("http://airapi.travelboutiqueonline.com/tbohotelapi_v6/hotelservice.asmx")

        SOAPResponse response = client2.send(
                """<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <AuthenticationData xmlns="http://TekTravel/HotelBookingApi">
      <SiteName></SiteName>
      <AccountCode></AccountCode>
    <UserName>${grailsApplication.config.tbo.TRAVELX_UNAME}</UserName>
<Password>${grailsApplication.config.tbo.TRAVELX_PASS_HOTEL}</Password>
    </AuthenticationData>
  </soap:Header>
  <soap:Body>
    <GetDestinationCityList xmlns="http://TekTravel/HotelBookingApi">
      <request>
        <CountryName>${country}</CountryName>
      </request>
    </GetDestinationCityList>
  </soap:Body>
</soap:Envelope>"""
        )
        log.info '---==country==' + response.text
        def ref = new XmlSlurper().parseText(response.text)



        ref.children().CityList.WSCity.each { city ->
            log.info '=====City Code====' + city.CityCode
            log.info '=====CityName====' + city.CityName
            log.info '=====CountryCode====' + city.CountryCode
            log.info '=====CountryName====' + city.CountryName
            log.info '=====CurrencyCode====' + city.CurrencyCode

        }
        render("SUCCESS")
    }


    def invoiceDetails = {
        render(view: "/testView/invoiceDetails")
    }

    def deleteUser = {
        User user = User.findByUsername("kr.saurabh2@gmail.com")

//        UserRole userRole = UserRole.findByUser(user)
//        if (userRole) {
//            userRole.delete()
//        }
        BookingHistory bookingHistory = BookingHistory.findByUser(user)

        if (bookingHistory) {
            bookingHistory.delete()
        }

        user.delete()

        render("SUCCESS")

    }


    def changeUserPass = {
        println "==========Inside Change Pass============"
        Role role = Role.findByAuthority('ROLE_USER')

        User user = User.findByUsername("kr.saurabh2@gmail.com")

//        user.password = "1234"
//        user.pictureName = "picNotAvailable"
//        user.pictureType = "jpeg"
//        println'-------Password--------' + user.password
//        if (!user.validate()) {
//            user.errors.allErrors.each {
//                log.info '--------error is--------' + it
//            }
//        }
//        user.save(flush: true)

        UserRole.create(user, role, true)

        render("SUCCESS")
    }

    def exportPdfForHotelDetail = {
        String bookingId = params.bookingId
        print('-----------------------------------' + bookingId)
        BookingHistory bookingHistory = BookingHistory.findByBookId(bookingId)
        PaymentInfo paymentInfo = PaymentInfo.findByBookingHistory(bookingHistory)
        println('1111111111111111111111111111111111111111111' + bookingHistory)
        File tempReportFile = File.createTempFile('report', 'pdf')
        pdfRenderingService.render([template: '/searchHotel/newThemed/forTBO/pdfDownLoadForHotelBookingSummary', model:
                [bookingHistory: bookingHistory, amount: paymentInfo?.amount]], tempReportFile.newOutputStream())
        def mimeType = 'application/pdf'
        response.contentType = mimeType + '; name=' + tempReportFile.name
        response.setHeader("Content-disposition", "attachment; filename=$tempReportFile.name")
        response.outputStream << tempReportFile.bytes
        tempReportFile.delete()
        response.outputStream.flush()
    }


}
