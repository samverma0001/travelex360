package serviceProvider

import com.cloudinary.Cloudinary
import com.travelex.Hotel.*
import com.travelex.User.User
import com.travelex.common.Address
import com.travelex.common.Facility
import com.travelex.resizeService.ResizeImageService
import com.travelex.serviceProvider.AddHotelCO
import com.travelex.socials.Enums.FacilityFor
import com.travelex.socials.Enums.HotelType
import org.apache.commons.io.FileUtils
import org.springframework.web.multipart.commons.CommonsMultipartFile

//import resizeImage.ResizeImageService
import util.AppUtil

import java.text.DateFormat
import java.text.SimpleDateFormat

class ServiceProviderController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def scaffold = true
    def springSecurityService


    def addHotel = {
        def serviceProviderId = params.serviceProviderId
        println '=========ServiceProviderService provider-----======' + params.serviceProviderId

        render(view: "/serviceProviderViews/addHotelForServiceProvider", model: [serviceProviderId: serviceProviderId])
    }

    def addNewHotel = { AddHotelCO addHotelCO ->
        def webRootDir = AppUtil.staticResourcesDirPath

        Map config = new HashMap();
        config.put("cloud_name", "dv83vfnvq");
        config.put("api_key", "478778589577545");
        config.put("api_secret", "rDCoYk_XT_wrgc6VABFoWCfRDEM");
        Cloudinary cloudinary = new Cloudinary(config);

        CommonsMultipartFile uploadedFile1 = params.roomPicture1

//        def uploadResult= cloudinary.uploader().upload(uploadedFile1, Cloudinary.asMap("public_id", "sample_hotelRoom_pic2"))
//        println'=====MapRooooommmmm===='+ uploadResult

        List<CommonsMultipartFile> commonsMultipartFiles = params.hotelImages as List<CommonsMultipartFile>
        commonsMultipartFiles.each {
            println '==========11111-----======'
        }

        println '==========Params-----======' + params
        println '========= params.room2Price-----======' + commonsMultipartFiles.size()

        println '=========Srvice provider-----======' + params.serviceProviderId
        if (addHotelCO.validate()) {
            def serviceProviderId = params.serviceProviderId

            String addressOne = params.addressOne
            String addressTwo = params.addressTwo
            String addressCity = params.addressCity
            String addressState = params.addressState
            String addressZip = params.addressZip
            String addressCountry = params.addressCountry

            String hotelName = params.hotelName
            String hotelSummary = params.hotelSummary
            String hotelType = params.hotelType
            String hotelFacilities = params.hotelFacilities

            String bookingStartRoomOne = params.bookingStartRoomOne
            String roomOneName = params.roomOneName
            String roomOneDescription = params.roomOneDescription
            String roomOnePrice = params.roomOnePrice
            String roomOneFacilities = params.roomOneFacilities

            String bookingStartRoom2 = params.bookingStartRoom2
            String room2Name = params.room2Name
            String room2Description = params.room2Description
            String room2Price = params.room2Price
            String room2Facilities = params.room2Facilities

            String bookingStartRoom3 = params.bookingStartRoom3
            String room3Name = params.room3Name
            String room3Description = params.room3Description
            String room3Price = params.room3Price
            String room3Facilities = params.room3Facilities

            String bookingStartRoom4 = params.bookingStartRoom4
            String room4Name = params.room4Name
            String room4Description = params.room4Description
            String room4Price = params.room4Price
            String room4Facilities = params.room4Facilities

            String bookingStartRoom5 = params.bookingStartRoom5
            String room5Name = params.room5Name
            String room5Description = params.room5Description
            String room5Price = params.room5Price
            String room5Facilities = params.room5Facilities


            Hotel hotel1 = new Hotel()

            Address address1 = new Address()
            address1.address1 = addressOne
            address1.address2 = addressTwo
            address1.city = addressCity
            address1.state = addressState
            address1.zip = addressZip
            address1.country = addressCountry
            address1.save(flush: true)


            hotel1.name = hotelName
            hotel1.summary = hotelSummary
            hotel1.address = address1
            hotel1.type = HotelType.valueOf(hotelType)
            hotel1.save(flush: true)

            HotelRoom hotelRoom1 = new HotelRoom()
            hotelRoom1.name = roomOneName
            hotelRoom1.description = roomOneDescription
            hotelRoom1.price = roomOnePrice.toLong()
            hotelRoom1.hotel = hotel1
            hotel1.addToRoomTypes(hotelRoom1)
            hotel1.save(flush: true)
            hotelRoom1.save(flush: true)

            if (room2Name) {
                HotelRoom hotelRoom2 = new HotelRoom()
                hotelRoom2.name = room2Name
                hotelRoom2.description = room2Description
                hotelRoom2.price = room2Price.toLong()
                hotelRoom2.hotel = hotel1
                hotel1.addToRoomTypes(hotelRoom2)
                hotel1.save(flush: true)
                hotelRoom2.save(flush: true)

                Facility facility3 = new Facility()
                facility3.name = room2Facilities
                facility3.facilityFor = FacilityFor.HOTEL_ROOM
                facility3.hotelRoomId = hotelRoom2.id
                hotelRoom2.save(flush: true)
                facility3.save(flush: true)
            }
            if (room3Name) {
                HotelRoom hotelRoom3 = new HotelRoom()
                hotelRoom3.name = room3Name
                hotelRoom3.description = room3Description
                hotelRoom3.price = room3Price.toLong()
                hotelRoom3.hotel = hotel1
                hotel1.addToRoomTypes(hotelRoom3)
                hotel1.save(flush: true)
                hotelRoom3.save(flush: true)
                Facility facility4 = new Facility()
                facility4.name = room3Facilities
                facility4.facilityFor = FacilityFor.HOTEL_ROOM
                facility4.hotelRoomId = hotelRoom3.id
                hotelRoom3.save(flush: true)
                facility4.save(flush: true)
            }
            if (room4Name) {
                HotelRoom hotelRoom4 = new HotelRoom()
                hotelRoom4.name = room4Name
                hotelRoom4.description = room4Description
                hotelRoom4.price = room4Price.toLong()
                hotelRoom4.hotel = hotel1
                hotel1.addToRoomTypes(hotelRoom4)
                hotel1.save(flush: true)
                hotelRoom4.save(flush: true)
                Facility facility5 = new Facility()
                facility5.name = room4Facilities
                facility5.facilityFor = FacilityFor.HOTEL_ROOM
                facility5.hotelRoomId = hotelRoom4.id
                hotelRoom4.save(flush: true)
                facility5.save(flush: true)
            }
            if (room5Name) {
                HotelRoom hotelRoom5 = new HotelRoom()
                hotelRoom5.name = room5Name
                hotelRoom5.description = room5Description
                hotelRoom5.price = room5Price.toLong()
                hotelRoom5.hotel = hotel1
                hotel1.addToRoomTypes(hotelRoom5)
                hotel1.save(flush: true)
                hotelRoom5.save(flush: true)
                Facility facility6 = new Facility()
                facility6.name = room5Facilities
                facility6.facilityFor = FacilityFor.HOTEL_ROOM
                facility6.hotelRoomId = hotelRoom5.id
                hotelRoom5.save(flush: true)
                facility6.save(flush: true)
            }

            SimpleDateFormat fromUser = new SimpleDateFormat("MM/dd/yyyy");
            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-mm-dd")
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            List<String> dateBooking = []
            dateBooking = bookingStartRoomOne.tokenize(",")
            dateBooking.each {
                println '=====After tokenize===' + it
                Booking booking = new Booking()
                booking.bookingDate = fromUser.parse(it)
                booking.hotelRoom = hotelRoom1
                hotelRoom1.addToBooking(booking)
                hotelRoom1.save()
                booking.save()

                String reportDate = df.format(booking.bookingDate);
//               String checkInDate = myFormat.format(fromUser.parse(booking.bookingDate))
                println '=====After saving===' + reportDate
                println '=====After saving===' + reportDate

            }

//            Picture pictureRoom1 = new Picture()
//            pictureRoom1.filePath = roomOnePictureUrl
//            pictureRoom1.contentType = roomOnePictureContentType
//            hotelRoom1.addToPictures(pictureRoom1)
//            hotelRoom1.save(flush: true)
//            pictureRoom1.save(flush: true)

            if (params.roomPicture1) {
                try {


                    println '=========Inside Upload Picture Room =======' + params.roomPicture1
                    CommonsMultipartFile uploadedFile = params.roomPicture1
//                    def uploadedFile = params.hotelPicture
                    if (uploadedFile?.bytes) {
                        println '=========Inside Upload Picture Room IF=======' + params.roomPicture1

                        def userDir = new File(webRootDir, "/uploadedRoomPic/${hotelRoom1.id}/")
                        userDir.mkdirs()
                        String fileName = uploadedFile.originalFilename.trim()
                        uploadedFile.transferTo(new File(userDir, fileName))
                        RoomPicture roomPicture = new RoomPicture()
                        println '========fileNamefileName======' + fileName

                        roomPicture.roomPictureName = fileName
                        roomPicture.roomPictureType = uploadedFile.contentType
                        hotelRoom1.addToRoomPicture(roomPicture)
                        roomPicture.save()

                        roomPicture.roomPictureUrl = "https://res.cloudinary.com/dv83vfnvq/image/upload/roompic${roomPicture.id}.${fileName.tokenize(".").last()}"
                        println '=========Inside Upload Picture Room After IF=====roomPictureUrl==' + roomPicture.roomPictureUrl
                        roomPicture.save()
                        println '=========Inside Upload Picture Room After IF=====roomPictureUrl==' + roomPicture.roomPictureUrl

                        File fileToUpload = new File(webRootDir, "/uploadedRoomPic/${hotelRoom1.id}/" + fileName)
//                        println '=========Inside Upload Picture Room After Iload Picture Room After IF=======' + params.roomPicture1
                        def roomId = roomPicture.id
                        println '=======roomId====' + roomId
                        roomPicture.roomPictureUrl = "https://res.cloudinary.com/dv83vfnvq/image/upload/roompic${roomId}.${fileName.tokenize(".").last()}"
                        println '=========Inside Upload Picture Room After IF=====roomPictureUrl==' + roomPicture.roomPictureUrl
                        roomPicture.save()

                        println '=========Inside Upload Picture Room After IF=====roomPictureUrl==' + roomPicture.roomPictureUrl

                        def uploadResult = cloudinary.uploader().upload(fileToUpload, Cloudinary.asMap("public_id", "roompic" + roomId))
                        println '=====MapRooooommmmm====' + uploadResult

                    }
                    println '=====deleteing======deleteing========='
                    File fileToDelete = new File(webRootDir, "/uploadedRoomPic/${hotelRoom1.id}/" + uploadedFile.originalFilename.trim())
                    fileToDelete.delete()
//
                }
                catch (Exception e) {
                    e.printStackTrace()
                }
            }

            if (params.roomPicture2) {
                try {
                    println '=========Inside Upload Picture Room=======' + params.roomPicture2
                    CommonsMultipartFile uploadedFile = params.roomPicture2
//                    def uploadedFile = params.hotelPicture
                    if (uploadedFile?.bytes) {
//                        def webRootDir = AppUtil.staticResourcesDirPath
                        def userDir = new File(webRootDir, "/uploadedRoomPic/${hotelRoom1.id}/")
                        userDir.mkdirs()
                        String fileName = uploadedFile.originalFilename.trim()
                        uploadedFile.transferTo(new File(userDir, fileName))

                        RoomPicture roomPicture = new RoomPicture()
                        roomPicture.roomPictureName = fileName
                        roomPicture.roomPictureType = uploadedFile.contentType
                        hotelRoom1.addToRoomPicture(roomPicture)
                        roomPicture.save()

                        roomPicture.roomPictureUrl = "https://res.cloudinary.com/dv83vfnvq/image/upload/roompic${roomPicture.id}.${fileName.tokenize(".").last()}"
                        println '=========Inside Upload Picture Room After IF=====roomPictureUrl==' + roomPicture.roomPictureUrl
                        roomPicture.save()
                        println '=========Inside Upload Picture Room After IF=====roomPictureUrl==' + roomPicture.roomPictureUrl

                        File fileToUpload = new File(webRootDir, "/uploadedRoomPic/${hotelRoom1.id}/" + fileName)
//                        println '=========Inside Upload Picture Room After Iload Picture Room After IF=======' + params.roomPicture1
                        def roomId = roomPicture.id
                        println '=======roomId====' + roomId
                        roomPicture.roomPictureUrl = "https://res.cloudinary.com/dv83vfnvq/image/upload/roompic${roomId}.${fileName.tokenize(".").last()}"
                        println '=========Inside Upload Picture Room After IF=====roomPictureUrl==' + roomPicture.roomPictureUrl
                        roomPicture.save()

                        println '=========Inside Upload Picture Room After IF=====roomPictureUrl==' + roomPicture.roomPictureUrl

                        def uploadResult = cloudinary.uploader().upload(fileToUpload, Cloudinary.asMap("public_id", "roompic" + roomId))
                        println '=====MapRooooommmmm====' + uploadResult

                    }
                    println '=====deleteing======deleteing========='
                    File fileToDelete = new File(webRootDir, "/uploadedRoomPic/${hotelRoom1.id}/" + uploadedFile.originalFilename.trim())
                    fileToDelete.delete()
//                    ResizeImageService.scaleRoomImage(hotelRoom1)
                }
                catch (Exception e) {
                    e.printStackTrace()
                }
            }

            if (params.roomPicture3) {
                try {
                    println '=========Inside Upload Picture Room=======' + params.roomPicture3
                    CommonsMultipartFile uploadedFile = params.roomPicture3
//                    def uploadedFile = params.hotelPicture
                    if (uploadedFile?.bytes) {
                        def userDir = new File(webRootDir, "/uploadedRoomPic/${hotelRoom1.id}/")
                        userDir.mkdirs()
                        String fileName = uploadedFile.originalFilename.trim()
                        uploadedFile.transferTo(new File(userDir, fileName))

                        RoomPicture roomPicture = new RoomPicture()
                        roomPicture.roomPictureName = fileName
                        roomPicture.roomPictureType = uploadedFile.contentType
                        hotelRoom1.addToRoomPicture(roomPicture)
                        roomPicture.save()

                        roomPicture.roomPictureUrl = "https://res.cloudinary.com/dv83vfnvq/image/upload/roompic${roomPicture.id}.${fileName.tokenize(".").last()}"
                        println '=========Inside Upload Picture Room After IF=====roomPictureUrl==' + roomPicture.roomPictureUrl
                        roomPicture.save()
                        println '=========Inside Upload Picture Room After IF=====roomPictureUrl==' + roomPicture.roomPictureUrl

                        File fileToUpload = new File(webRootDir, "/uploadedRoomPic/${hotelRoom1.id}/" + fileName)
//                        println '=========Inside Upload Picture Room After Iload Picture Room After IF=======' + params.roomPicture1
                        def roomId = roomPicture.id
                        println '=======roomId====' + roomId
                        roomPicture.roomPictureUrl = "https://res.cloudinary.com/dv83vfnvq/image/upload/roompic${roomId}.${fileName.tokenize(".").last()}"
                        println '=========Inside Upload Picture Room After IF=====roomPictureUrl==' + roomPicture.roomPictureUrl
                        roomPicture.save()

                        println '=========Inside Upload Picture Room After IF=====roomPictureUrl==' + roomPicture.roomPictureUrl

                        def uploadResult = cloudinary.uploader().upload(fileToUpload, Cloudinary.asMap("public_id", "roompic" + roomId))
                        println '=====MapRooooommmmm====' + uploadResult

                    }
                    println '=====deleteing======deleteing========='
                    File fileToDelete = new File(webRootDir, "/uploadedRoomPic/${hotelRoom1.id}/" + uploadedFile.originalFilename.trim())
                    fileToDelete.delete()
//                    ResizeImageService.scaleRoomImage(hotelRoom1)
                }
                catch (Exception e) {
                    e.printStackTrace()
                }
            }

            if (params.hotelPicture) {
                try {
                    println '=========Inside Upload Picture =======' + params.hotelPicture
                    CommonsMultipartFile uploadedFile = params.hotelPicture
//                    def uploadedFile = params.hotelPicture
                    if (uploadedFile?.bytes) {
                        def userDir = new File(webRootDir, "/uploadedHotelPic/${hotel1.id}/")
                        userDir.mkdirs()
                        String fileName = uploadedFile.originalFilename.trim()
                        uploadedFile.transferTo(new File(userDir, fileName))

                        HotelPicture hotelPicture = new HotelPicture()
                        hotelPicture.hotelPictureName = fileName
                        hotelPicture.hotelPictureType = uploadedFile.contentType
                        hotel1.addToHotelPictures(hotelPicture)
                        hotelPicture.save()

                        hotelPicture.hotelPictureUrl = "https://res.cloudinary.com/dv83vfnvq/image/upload/hotelpic${hotelPicture.id}.${fileName.tokenize(".").last()}"
                        println '=========Inside Upload Picture Room After IF=====hotelpictureUrl==' + hotelPicture.hotelPictureUrl
                        hotelPicture.save()
                        println '=========Inside Upload Picture Room After IF=====hotelpictureUrl==' + hotelPicture.hotelPictureUrl

                        File fileToUpload = new File(webRootDir, "/uploadedHotelPic/${hotel1.id}/" + fileName)
//                        println '=========Inside Upload Picture Room After Iload Picture Room After IF=======' + params.roomPicture1
                        def hotelId = hotelPicture.id
                        println '=======roomId====' + hotelId
                        hotelPicture.hotelPictureUrl = "https://res.cloudinary.com/dv83vfnvq/image/upload/hotelpic${hotelId}.${fileName.tokenize(".").last()}"
                        println '=========Inside Upload Picture Room After IF====hotelpictureUrl==' + hotelPicture.hotelPictureUrl
                        hotelPicture.save()

                        println '=========Inside Upload Picture Room After IF=====hotelpictureUrl==' + hotelPicture.hotelPictureUrl

                        def uploadResult = cloudinary.uploader().upload(fileToUpload, Cloudinary.asMap("public_id", "hotelpic" + hotelId))
                        println '=====MapRooooommmmm====' + uploadResult
//                        hotel1.hotelPictureName = fileName
//                        hotel1.hotelPictureType = uploadedFile.contentType
                    }
                    println '=====deleteing======deleteing========='
                    File fileToDelete = new File(webRootDir, "/uploadedHotelPic/${hotel1.id}/" + uploadedFile.originalFilename.trim())
                    fileToDelete.delete()
//                    ResizeImageService.scaleImage(hotel1)
                }
                catch (Exception e) {
                    e.printStackTrace()
                }
            }
            if (params.hotelPicture2) {
                try {
                    println '=========Inside Upload Picture =======' + params.hotelPicture2
                    CommonsMultipartFile uploadedFile = params.hotelPicture2
//                    def uploadedFile = params.hotelPicture
                    if (uploadedFile?.bytes) {
                        def userDir = new File(webRootDir, "/uploadedHotelPic/${hotel1.id}/")
                        userDir.mkdirs()
                        String fileName = uploadedFile.originalFilename.trim()
                        uploadedFile.transferTo(new File(userDir, fileName))

                        HotelPicture hotelPicture = new HotelPicture()
                        hotelPicture.hotelPictureName = fileName
                        hotelPicture.hotelPictureType = uploadedFile.contentType
                        hotel1.addToHotelPictures(hotelPicture)
                        hotelPicture.save()

                        hotelPicture.hotelPictureUrl = "https://res.cloudinary.com/dv83vfnvq/image/upload/hotelpic${hotelPicture.id}.${fileName.tokenize(".").last()}"
                        println '=========Inside Upload Picture Room After IF=====hotelpictureUrl==' + hotelPicture.hotelPictureUrl
                        hotelPicture.save()
                        println '=========Inside Upload Picture Room After IF=====hotelpictureUrl==' + hotelPicture.hotelPictureUrl

                        File fileToUpload = new File(webRootDir, "/uploadedHotelPic/${hotel1.id}/" + fileName)
//                        println '=========Inside Upload Picture Room After Iload Picture Room After IF=======' + params.roomPicture1
                        def hotelId = hotelPicture.id
                        println '=======hotelId====' + hotelId
                        hotelPicture.hotelPictureUrl = "https://res.cloudinary.com/dv83vfnvq/image/upload/hotelpic${hotelId}.${fileName.tokenize(".").last()}"
                        println '=========Inside Upload Picture Room After IF====hotelpictureUrl==' + hotelPicture.hotelPictureUrl
                        hotelPicture.save()

                        println '=========Inside Upload Picture Room After IF=====hotelpictureUrl==' + hotelPicture.hotelPictureUrl

                        def uploadResult = cloudinary.uploader().upload(fileToUpload, Cloudinary.asMap("public_id", "hotelpic" + hotelId))
                        println '=====MapRooooommmmm====' + uploadResult
//                    ResizeImageService.scaleImage(hotel1)
                    }
                    println '=====deleteing======deleteing========='
                    File fileToDelete = new File(webRootDir, "/uploadedHotelPic/${hotel1.id}/" + uploadedFile.originalFilename.trim())
                    fileToDelete.delete()
                }
                catch (Exception e) {
                    e.printStackTrace()
                }
            }
            if (params.hotelPicture3) {
                try {
                    println '=========Inside Upload Picture =======' + params.hotelPicture3
                    CommonsMultipartFile uploadedFile = params.hotelPicture3
//                    def uploadedFile = params.hotelPicture
                    if (uploadedFile?.bytes) {
                        def userDir = new File(webRootDir, "/uploadedHotelPic/${hotel1.id}/")
                        userDir.mkdirs()
                        String fileName = uploadedFile.originalFilename.trim()
                        uploadedFile.transferTo(new File(userDir, fileName))

                        HotelPicture hotelPicture = new HotelPicture()
                        hotelPicture.hotelPictureName = fileName
                        hotelPicture.hotelPictureType = uploadedFile.contentType
                        hotel1.addToHotelPictures(hotelPicture)
                        hotelPicture.save()

                        hotelPicture.hotelPictureUrl = "https://res.cloudinary.com/dv83vfnvq/image/upload/hotelpic${hotelPicture.id}.${fileName.tokenize(".").last()}"
                        println '=========Inside Upload Picture Hotel After IF=====roomPictureUrl==' + hotelPicture.hotelPictureUrl
                        hotelPicture.save()
                        println '=========Inside Upload Picture Hotel After IF=====roomPictureUrl==' + hotelPicture.hotelPictureUrl

                        File fileToUpload = new File(webRootDir, "/uploadedHotelPic/${hotel1.id}/" + fileName)
//                        println '=========Inside Upload Picture Room After Iload Picture Room After IF=======' + params.roomPicture1
                        def hotelId = hotelPicture.id
                        println '=======hotelId====' + hotelId
                        hotelPicture.hotelPictureUrl = "https://res.cloudinary.com/dv83vfnvq/image/upload/hotelpic${hotelId}.${fileName.tokenize(".").last()}"
                        println '=========Inside Upload Picture Hotel After IF=====roomPictureUrl==' + hotelPicture.hotelPictureUrl
                        hotelPicture.save()

                        println '=========Inside Upload Picture Hotel After IF=====roomPictureUrl==' + hotelPicture.hotelPictureUrl

                        def uploadResult = cloudinary.uploader().upload(fileToUpload, Cloudinary.asMap("public_id", "hotelpic" + hotelId))
                        println '=====MapRooooommmmm====' + uploadResult
                    }
                    println '=====deleteing======deleteing========='
                    File fileToDelete = new File(webRootDir, "/uploadedHotelPic/${hotel1.id}/" + uploadedFile.originalFilename.trim())
                    fileToDelete.delete()
//                    ResizeImageService.scaleImage(hotel1)
                }
                catch (Exception e) {
                    e.printStackTrace()
                }
            }
            Facility facility1 = new Facility()
            facility1.name = hotelFacilities
            facility1.facilityFor = FacilityFor.HOTEL
            facility1.hotelId = hotel1.id
            hotel1.save(flush: true)
            facility1.save(flush: true)

//            Facility facility2 = new Facility()
//            facility2.name = roomOneFacilities
//            facility2.facilityFor = FacilityFor.HOTEL_ROOM
//            facility2.hotelRoomId = hotelRoom1.id
//            hotelRoom1.save(flush: true)
//            facility2.save(flush: true)
            println '===============ServiceProviderService Provider id ========' + serviceProviderId as Long
            println '===============ServiceProviderService Provider id ========' + serviceProviderId

            ServiceProvider serviceProvider1 = ServiceProvider.findById(serviceProviderId as Long)
            println '===============ServiceProviderService Provider ========' + serviceProvider1?.nameOfRepresentee
            serviceProvider1.addToHotel(hotel1)
            serviceProvider1.save(flush: true)
            hotel1.save(flush: true)

            redirect(controller: "home", action: "index")
        } else {
            render(view: "/serviceProviderViews/addHotelForServiceProvider", model: [addHotelCO: addHotelCO])
        }
    }

    def editHotel = {
        def hotelId = params.hotelId
        def serviceProviderId = params.serviceProviderId
        Hotel hotel = Hotel.findById(hotelId as Long)
        println '==========Hotel Id=======' + hotel.name
        hotel.roomTypes.each { println '==========Hotel Id=======' + it.name }
        List<Facility> facilityList = Facility.findAllByHotelIdAndFacilityFor(hotel.id, FacilityFor.HOTEL)
        List<Facility> facilityListRoom = Facility.findAllByHotelRoomIdAndFacilityFor(hotel.roomTypes[0].id, FacilityFor.HOTEL_ROOM)
        render(view: "/serviceProviderViews/editHotelForServiceProvider", model: [facilityListRoom: facilityListRoom, facilityList: facilityList, hotel: hotel, serviceProviderId: serviceProviderId])
    }

    def updateHotel = {

        println '==========Params-----======' + params
        println '==============params.addressId==================' + params.addressId
        println '==============params.hotelRoomId==================' + params.hotelRoomId

        def serviceProviderId = params.serviceProviderId

        String hotelId = params.hotelId
        String addressId = params.addressId
        String hotelPicId = params.hotelPicId
        String roomPicId = params.roomPicId
        String hotelFacilityId = params.hotelFacilityId
        String hotelRoomId = params.hotelRoomId

//        if (updateHotelCO.validate()) {

        String addressOne = params.addressOne
        String addressTwo = params.addressTwo
        String addressCity = params.addressCity
        String addressState = params.addressState
        String addressZip = params.addressZip
        String addressCountry = params.addressCountry

        String hotelName = params.hotelName
        String hotelSummary = params.hotelSummary
        String hotelType = params.hotelType
        String hotelFacilities = params.hotelFacilities

        String roomOneName = params.roomOneName
        String roomOneDescription = params.roomOneDescription
        String roomOnePrice = params.roomOnePrice
//        String roomOneMaximumPrice = params.roomOneMaximumPrice

        String roomOneFacilities = params.roomOneFacilities

        println '==============Before Address================='


        Hotel hotel1 = Hotel.get(hotelId as Long)

        Address address1 = Address.findByHotel(hotel1)
        println '==============Between Address================='

        address1.address1 = addressOne
        println '==============Between Address2================='

        address1.address2 = addressTwo
        address1.city = addressCity
        address1.state = addressState
        address1.zip = addressZip
        address1.country = addressCountry
        address1.save(flush: true)
        println '==============After Address================='


        hotel1.name = hotelName
        hotel1.summary = hotelSummary
        hotel1.address = address1
        hotel1.type = HotelType.valueOf(hotelType)
        hotel1.save(flush: true)

        println '==========Hotel Name=====' + hotel1.name

        HotelRoom hotelRoom1 = null
        if (hotelRoomId) {
            println '=====Existing Room====='
            hotelRoom1 = HotelRoom.findById(hotelRoomId as Long)
        } else {
            println '=====New Room====='
            hotelRoom1 = new HotelRoom()
        }
        hotelRoom1.name = roomOneName
        hotelRoom1.description = roomOneDescription ? roomOneDescription : null
        hotelRoom1.price = roomOnePrice.toLong()
//        hotelRoom1.maxPrice = roomOneMaximumPrice.toLong()
        hotelRoom1.hotel = hotel1
        hotel1.addToRoomTypes(hotelRoom1)
        hotel1.save(flush: true)
        hotelRoom1.save(flush: true)

//        Picture pictureRoom1
//
//        if (roomPicId) {
//            pictureRoom1 = Picture.get(roomPicId as Long)
//        } else {
//            pictureRoom1 = new Picture()
//        }
//        pictureRoom1.filePath = roomOnePictureUrl
//        pictureRoom1.contentType = roomOnePictureContentType
//        hotelRoom1.addToPictures(pictureRoom1)
//        hotelRoom1.save(flush: true)
//        pictureRoom1.save(flush: true)

        if (params.hotelPicture0) {
            try {
                println '=========Inside Upload Picture =======' + params.hotelPicture0
                println '=========Inside Upload pictureId2 =======' + params.pictureId0

                CommonsMultipartFile uploadedFile = params.hotelPicture0
                if (uploadedFile?.bytes) {
                    def webRootDir = AppUtil.staticResourcesDirPath
                    def userDir = new File(webRootDir, "/uploadedHotelPic/${hotel1.id}/")
                    userDir.mkdirs()
                    String fileName = uploadedFile.originalFilename.trim()
                    uploadedFile.transferTo(new File(userDir, fileName))

                    HotelPicture hotelPicture = HotelPicture.findById(params.pictureId0 as Long)
                    println '=========hotelPictureName =======' + hotelPicture.hotelPictureName

                    hotelPicture.hotelPictureName = fileName
                    hotelPicture.hotelPictureType = uploadedFile.contentType
                    hotel1.addToHotelPictures(hotelPicture)
                    hotelPicture.save()
                }
                ResizeImageService.scaleImage(hotel1)
            }
            catch (Exception e) {
                e.printStackTrace()
            }
        }
        if (params.hotelPicture1) {
            try {
                println '=========Inside Upload Picture =======' + params.hotelPicture1
                println '=========Inside Upload pictureId2 =======' + params.pictureId1

                CommonsMultipartFile uploadedFile = params.hotelPicture1
                if (uploadedFile?.bytes) {
                    def webRootDir = AppUtil.staticResourcesDirPath
                    def userDir = new File(webRootDir, "/uploadedHotelPic/${hotel1.id}/")
                    userDir.mkdirs()
                    String fileName = uploadedFile.originalFilename.trim()
                    uploadedFile.transferTo(new File(userDir, fileName))

                    HotelPicture hotelPicture = HotelPicture.findById(params.pictureId1 as Long)
                    println '=========hotelPictureName =======' + hotelPicture.hotelPictureName

                    hotelPicture.hotelPictureName = fileName
                    hotelPicture.hotelPictureType = uploadedFile.contentType
                    hotel1.addToHotelPictures(hotelPicture)
                    hotelPicture.save()
                }
                ResizeImageService.scaleImage(hotel1)
            }
            catch (Exception e) {
                e.printStackTrace()
            }
        }
        if (params.hotelPicture2) {
            try {
                println '=========Inside Upload Picture =======' + params.hotelPicture2
                println '=========Inside Upload pictureId2 =======' + params.pictureId2
                CommonsMultipartFile uploadedFile = params.hotelPicture2
                if (uploadedFile?.bytes) {
                    def webRootDir = AppUtil.staticResourcesDirPath
                    def userDir = new File(webRootDir, "/uploadedHotelPic/${hotel1.id}/")
                    userDir.mkdirs()
                    String fileName = uploadedFile.originalFilename.trim()
                    uploadedFile.transferTo(new File(userDir, fileName))

                    HotelPicture hotelPicture = HotelPicture.findById(params.pictureId2 as Long)
                    println '=========hotelPictureName =======' + hotelPicture.hotelPictureName

                    hotelPicture.hotelPictureName = fileName
                    hotelPicture.hotelPictureType = uploadedFile.contentType
                    hotel1.addToHotelPictures(hotelPicture)
                    hotelPicture.save()
                }
                ResizeImageService.scaleImage(hotel1)
            }
            catch (Exception e) {
                e.printStackTrace()
            }
        }


        Facility facility1 = Facility.findByHotelId(hotel1.id)
//        facility1 = new Facility()
        facility1.name = hotelFacilities
        facility1.facilityFor = FacilityFor.HOTEL
        facility1.hotelId = hotel1.id
        hotel1.save(flush: true)
        facility1.save(flush: true)


        Facility facility2 = Facility.findByHotelRoomId(hotelRoom1.id)
//        facility1 = new Facility()
        facility2.name = hotelFacilities
        facility2.facilityFor = FacilityFor.HOTEL_ROOM
        facility2.hotelRoomId = hotelRoom1.id
        hotelRoom1.save(flush: true)
        facility2.save(flush: true)


        ServiceProvider serviceProvider1 = ServiceProvider.findById(serviceProviderId as Long)
        println '===============ServiceProviderService Provider ========' + serviceProvider1.nameOfRepresentee
        serviceProvider1.addToHotel(hotel1)
        serviceProvider1.save(flush: true)
        hotel1.save(flush: true)

        redirect(controller: "home", action: "index")
    }

    def saveServiceProviderImage = {
        print('===========controller----ServiceProviderService provider and action-----------saveServiceProviderImage============' + params)
        User user = springSecurityService.currentUser as User
        def webRootDir = AppUtil.staticResourcesDirPath
        print('===========controller----ServiceProviderService provider and action-----------saveServiceProviderImage============' + webRootDir)
        CommonsMultipartFile uploadFile = params.serviceProviderProfileImage
        print('size is' + uploadFile.getBytes()?.size())
        if (uploadFile?.bytes) {
            print('222222222222222222222222222222')
            if (uploadFile?.getBytes()?.size() < 1000000) {
                def userDir = new File(webRootDir, "/serviceProviderImages/${user?.id}")
                print('hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh')
                if (user.pictureName) {
                    print('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
                    userDir.delete()
                    userDir.mkdirs()
                    String fileName = uploadFile?.originalFilename?.trim()
                    uploadFile.transferTo((new File(userDir, fileName)))
                    user?.pictureName = fileName
                    user?.pictureType = uploadFile?.getContentType()
                    user.save(flush: true)
                } else {
                    print('bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb')
                    userDir.mkdir()
                    String fileName = uploadFile?.originalFilename?.trim()
                    uploadFile.transferTo(new File(userDir, fileName))
                    user?.pictureName = fileName
                    user?.pictureType = uploadFile?.getContentType()
                    user.save(flush: true)
                }
                String serviceProviderImage = 'serviceProviderImage'
                print('gggggggggggggggggggggggggggggggggggg')
                ResizeImageService.scaleImage(user?.id, serviceProviderImage)
                print('mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm')
                flash.success = g.message(code: 'image.save.default.message')
                print('nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn')
                redirect(controller: 'home', action: 'index')

            } else {
                flash.warning = g.message(code: 'image.size.more.error')
                redirect(controller: 'home', action: 'index')
            }
        } else {
            flash.success = g.message(code: 'image.not.found.error')
            redirect(controller: 'home', action: 'index')
        }
    }

    def showServiceProviderImage = {
        print('111111111111111111111111111' + params)
        User user = User.get(params.long('userId'))
        def webRootDir = AppUtil.staticResourcesDirPath
        print('22222222222222222222222222222' + webRootDir)
        String imageName = user?.pictureName
        print('22222222222222222222222222222' + imageName)
        def avatarFilePath = new File(webRootDir, "/serviceProviderImages/${user?.id}/" + imageName)
        try {
            if (avatarFilePath) {
                response.setContentType(user.pictureType)
                response.setContentLength(avatarFilePath?.size()?.toInteger())
                OutputStream out = response.getOutputStream();
                out.write(avatarFilePath.bytes);
                out.close();
            }
        }
        catch (Exception e) {
            print('exception in show service provider image' + imageName)
        }
    }

    def showProfileImage = {
        def id = params.userId
        println '+++++++++++' + id
        Hotel hotel = Hotel.findById(id as Long)
        def webRootDir = AppUtil.staticResourcesDirPath
        String imageName = params.hotelPictureName ? params.hotelPictureName : hotel?.hotelPictures?.first()?.hotelPictureName
        println '---------imageName-----------' + imageName
        String fileNameToken = imageName?.tokenize('.')?.first()
        String fileExtension = imageName?.tokenize('.')?.last()
        def avatarFilePath
        try {

            if (params.picSize == "first") {
                avatarFilePath = new File(webRootDir, "/uploadedHotelPic/${hotel.id}/resize/" + fileNameToken + "${AppUtil.FILE_50}.${fileExtension}");
            } else {
                avatarFilePath = new File(webRootDir, "/uploadedHotelPic/${hotel.id}/resize/" + fileNameToken + "${AppUtil.FILE_200}.${fileExtension}");
            }

            println '---------avatarFilePath---------' + avatarFilePath.getAbsolutePath()


            if (avatarFilePath) {
                response.setContentType(hotel.hotelPictures?.first()?.hotelPictureType)
                response.setContentLength(avatarFilePath?.size()?.toInteger())
                OutputStream out = response.getOutputStream();
                out.write(avatarFilePath.bytes);
                out.close();
            }

        } catch (Exception e) {
            println '---------Excepton----------------'
            e.printStackTrace()
        }
    }

    def showRoomImage = {
        def id = params.roomId
        println '++++++Room+++++' + id
        HotelRoom hotelRoom = HotelRoom.findById(id as Long)
        def webRootDir = AppUtil.staticResourcesDirPath
        String imageName = params.roomPictureName ? params.roomPictureName : hotelRoom?.roomPicture?.first()?.roomPictureName
        println '---------imageName--Room---------' + imageName
        String fileNameToken = imageName?.tokenize('.')?.first()
        String fileExtension = imageName?.tokenize('.')?.last()
        def avatarFilePath
        try {

            if (params.picSize == "first") {
                avatarFilePath = new File(webRootDir, "/uploadedRoomPic/${hotelRoom.id}/resize/" + fileNameToken + "${AppUtil.FILE_50}.${fileExtension}");
            } else {
                avatarFilePath = new File(webRootDir, "/uploadedRoomPic/${hotelRoom.id}/resize/" + fileNameToken + "${AppUtil.FILE_200}.${fileExtension}");
            }

            println '---------avatarFilePath---------' + avatarFilePath.getAbsolutePath()


            if (avatarFilePath) {
                response.setContentType(hotelRoom.roomPicture[0].roomPictureType)
                response.setContentLength(avatarFilePath?.size()?.toInteger())
                OutputStream out = response.getOutputStream();
                out.write(avatarFilePath.bytes);
                out.close();
            }

        } catch (Exception e) {
            println '---------Excepton----------------'
            e.printStackTrace()
        }
    }

    def chooseService = {
        render(view: '/serviceProviderViews/chooseService')
    }

    def addRoom = {
        String roomNumber = params.roomNumber
        println '=========Inside Add Room=====' + roomNumber
        render(template: "/serviceProviderViews/addRoom", model: [roomNumber: roomNumber])
    }
}
