package activity

import grails.converters.JSON
import grails.plugins.springsecurity.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ActivityCategoryController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(['ROLE_ADMIN'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond ActivityCategory.list(params), model: [activityCategoryInstanceCount: ActivityCategory.count()]
    }

    @Secured(['ROLE_ADMIN'])
    def show(ActivityCategory activityCategoryInstance) {
        respond activityCategoryInstance
    }

    @Secured(['ROLE_ADMIN'])
    def create() {
        List<ActivityCategory> activityCategoryList = ActivityCategory.createCriteria().list {
            isNull('parentCategory')
        }
        respond new ActivityCategory(params), model: [activityCategoryList: activityCategoryList]
    }

    @Transactional
    @Secured(['ROLE_ADMIN'])
    def save(ActivityCategory activityCategoryInstance) {
        if (activityCategoryInstance == null) {
            notFound()
            return
        }

        if (activityCategoryInstance.hasErrors()) {
            respond activityCategoryInstance.errors, view: 'create'
            return
        }

        activityCategoryInstance.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'activityCategoryInstance.label', default: 'ActivityCategory'), activityCategoryInstance.id])
                redirect activityCategoryInstance
            }
            '*' { respond activityCategoryInstance, [status: CREATED] }
        }
    }

    @Secured(['ROLE_ADMIN'])
    def edit(ActivityCategory activityCategoryInstance) {
        List<ActivityCategory> activityCategoryList = ActivityCategory.createCriteria().list {
            isNull('parentCategory')
        }
        respond activityCategoryInstance, model: [activityCategoryList: activityCategoryList]
    }

    @Transactional
    @Secured(['ROLE_ADMIN'])
    def update(ActivityCategory activityCategoryInstance) {
        if (activityCategoryInstance == null) {
            notFound()
            return
        }

        if (activityCategoryInstance.hasErrors()) {
            respond activityCategoryInstance.errors, view: 'edit'
            return
        }

        activityCategoryInstance.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'ActivityCategory.label', default: 'ActivityCategory'), activityCategoryInstance.id])
                redirect activityCategoryInstance
            }
            '*' { respond activityCategoryInstance, [status: OK] }
        }
    }

    @Transactional
    @Secured(['ROLE_ADMIN'])
    def delete(ActivityCategory activityCategoryInstance) {

        if (activityCategoryInstance == null) {
            notFound()
            return
        }

        activityCategoryInstance.delete flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'ActivityCategory.label', default: 'ActivityCategory'), activityCategoryInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'activityCategoryInstance.label', default: 'ActivityCategory'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

    @Secured(['ROLE_ADMIN'])
    def findParentCategory() {
        String parentCategory = params.parentCategoryId
        ActivityCategory activityCategory = ActivityCategory.findById(parentCategory.toLong())
        List<ActivityCategory> activityCategoryList = ActivityCategory.createCriteria().list {
            eq('parentCategory', activityCategory)
        }
        render(template: '/activityCategory/activityCategoryTemplate', model: [activityCategoryList: activityCategoryList])

    }

}
