import com.travelex.TboApi.CityFromTBO
import com.travelex.TboApi.CountryFromTbo
import com.travelex.User.User
import com.travelex.flightFunctionality.FlightSearchResultVO
import grails.converters.deep.JSON
import wslite.soap.SOAPClient
import wslite.soap.SOAPResponse

import javax.xml.XMLConstants
import javax.xml.namespace.QName
import javax.xml.parsers.SAXParserFactory
import javax.xml.soap.MessageFactory
import javax.xml.soap.SOAPBody
import javax.xml.soap.SOAPBodyElement
import javax.xml.soap.SOAPEnvelope
import javax.xml.soap.SOAPHeader
import javax.xml.soap.SOAPHeaderElement
import javax.xml.soap.SOAPMessage
import javax.xml.soap.SOAPPart
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class TestController {
    def hashCalculationService

    def grailsApplication

    def changeUserPass = {
        User user = User.findByUsername("kr.saurabh@nexthoughts.com")

        user.password = "1234"

        user.save(flush: true)

        render("SUCCESS")
    }


    def testView = {
        render (view: '/searchHotel/newThemed/hotelSearchListTepm')
    }


    def saveAllCities = {
        List<CountryFromTbo> countryFromTbo = CountryFromTbo.getAll()
        CountryFromTbo countryFromTbo1 = CountryFromTbo.findByCountryName("India")
//        countryFromTbo.each { country ->
//            if (!country.countryName.contains("&")) {

        def client2 = new SOAPClient("http://api.tektravels.com/tbohotelapi_v6/hotelservice.asmx")
//        def client2 = new SOAPClient("http://airapi.travelboutiqueonline.com/tbohotelapi_v6/hotelservice.asmx")

        SOAPResponse response2 = client2.send(
                """<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
<soap:Header>
<AuthenticationData xmlns="http://TekTravel/HotelBookingApi">
<SiteName></SiteName>
<AccountCode></AccountCode>
<UserName>${grailsApplication.config.tbo.TRAVELX_UNAME}</UserName>
<Password>${grailsApplication.config.tbo.TRAVELX_PASS_HOTEL}</Password>
</AuthenticationData>
</soap:Header>
<soap:Body>
 <GetDestinationCityList xmlns="http://TekTravel/HotelBookingApi">
      <request>
        <CountryName>${countryFromTbo1.countryName}</CountryName>
      </request>
    </GetDestinationCityList>
</soap:Body>
</soap:Envelope>"""
        )
        println '---==response==3333==' + response2
        def ref = new XmlSlurper().parseText(response2.text)
        ref.children().GetDestinationCityListResponse.GetDestinationCityListResult.CityList.WSCity.eachWithIndex { city, itr ->
            println '======== CityCode====' + city.CityCode
            println '======== CityName====' + city.CityName
            println '======== SPACE===='

            println '======== FOR COUNTRY====' + countryFromTbo1

//            CountryFromTbo countryFromTbo1 = CountryFromTbo.findByCountryName("India")

            CityFromTBO cityFromTBO = new CityFromTBO(city, countryFromTbo1)
//            CityFromTBO cityFromTBO = new CityFromTBO(city, countryFromTbo1)
            cityFromTBO.save(flush: true)
        }

        println '========string Country====' + countryFromTbo1.countryName
//            }
//        }
        render("SUCCESS")
    }
    def bookFlight = {
        println "==========PARAMS==InBookFlight========" + params
        def flightSessionId = params.flightSessionId
        def flightCode = params.flightCode


        String adult1TName = params.adult1TName
        String adult1Age = params.adult1Age
        String adult1Email = params.adult1Email
        String adult1AddressOne = params.adult1AddressOne
        String adult1AddressTwo = params.adult1AddressTwo
        String adult1City = params.adult1City
        String adult1FName = params.adult1FName
        String adult1LName = params.adult1LName
        String adult1Phone = params.adult1Phone
        String adult1State = params.adult1State
        String adult1Zip = params.adult1Zip
        String adult1Country = params.adult1Country

        String adult2TName = params.adult2TName
        String adult2FName = params.adult2FName
        String adult2LName = params.adult2LName

        String adult3TName = params.adult3TName
        String adult3FName = params.adult3FName
        String adult3LName = params.adult3LName

        String adult4TName = params.adult4TName
        String adult4FName = params.adult4FName
        String adult4LName = params.adult4LName

        String adult5TName = params.adult5TName
        String adult5FName = params.adult5FName
        String adult5LName = params.adult5LName

        String adult6TName = params.adult6TName
        String adult6FName = params.adult6FName
        String adult6LName = params.adult6LName


        String child1TName = params.child1TName
        String child1FName = params.child1FName
        String child1LName = params.child1LName

        String child2TName = params.child2TName
        String child2FName = params.child2FName
        String child2LName = params.child2LName

        String child3TName = params.child3TName
        String child3FName = params.child3FName
        String child3LName = params.child3LName

        String child4TName = params.child4TName
        String child4FName = params.child4FName
        String child4LName = params.child4LName

        println "==========flightSessionId==========" + params.flightSessionId
        println "==========PARAMS==========" + params.flightCode

        List<FlightSearchResultVO> flightSearchVOList = session["flightSearchList"] as List
        flightSearchVOList.each {
            println "==========PARAMS==from session========" + it.flightSegmentVO.first().airLineName
            println "==========PARAMS==from airLineCode========" + it.flightSegmentVO.first().airLineCode
            println "==========PARAMS==from airLineCode========" + it.flightSegmentVO.first().flightNumber

        }
        FlightSearchResultVO flightSearchVO = null
        flightSearchVOList.each {
            it.flightSegmentVO.each { segment ->
                if (segment.flightNumber == flightCode) {
                    flightSearchVO = it
                    println '=======Inside flight Segment======flightSearchVO.flightSegmentVO.first().airLineName=====' + flightSearchVO.flightSegmentVO.first().airLineName
                }

            }
        }
        println '================destination====' + flightSearchVO.destination
        println '================origin====' + flightSearchVO.origin
        println '================flightSearchVO.flightFare====' + flightSearchVO.flightFare
        println '================airLineCode====' + flightSearchVO.flightSegmentVO.first().airLineCode
        println '================departureTime====' + flightSearchVO.flightSegmentVO.first().departureTime
        println '================arrivalTime====' + flightSearchVO.flightSegmentVO.first().arrivalTime
        println '================flightFareRule.departureDate====' + flightSearchVO.flightFareRule.first().departureDate
        println '================flightSearchVO.flightFareRule.returnDate====' + flightSearchVO.flightFareRule.first().returnDate


        MessageFactory factory = MessageFactory.newInstance();
        SOAPMessage soapMsg = factory.createMessage();
        SOAPPart part = soapMsg.getSOAPPart();
        SOAPEnvelope envelope = part.getEnvelope();
        SOAPHeader header = envelope.getHeader();
        SOAPBody body = envelope.getBody();
        QName qName = new QName("http://192.168.0.170/TT/BookingAPI", "AuthenticationData");
        SOAPHeaderElement headerElement = header.addHeaderElement(qName);
//
        headerElement.addChildElement("UserName").addTextNode("${grailsApplication.config.tbo.TRAVELX_UNAME}")
        headerElement.addChildElement("Password").addTextNode("${grailsApplication.config.tbo.TRAVELX_PASS_HOTEL}")

        QName qName1 = new QName("http://192.168.0.170/TT/BookingAPI", "Book");

        SOAPBodyElement element = body.addBodyElement(qName1);

        def reqElementTag = element.addChildElement("bookRequest")
        reqElementTag.addChildElement("Remarks").addTextNode("FlightBook");
        reqElementTag.addChildElement("InstantTicket").addTextNode("true");
        def fare = reqElementTag.addChildElement("Fare");
        fare.addChildElement("BaseFare").addTextNode(flightSearchVO?.flightFare?.baseFare)
        fare.addChildElement("Tax").addTextNode(flightSearchVO?.flightFare?.tax)
        fare.addChildElement("ServiceTax").addTextNode(flightSearchVO?.flightFare?.serviceTax)
        fare.addChildElement("AdditionalTxnFee").addTextNode(flightSearchVO?.flightFare?.additionalTxnFee)
        fare.addChildElement("AgentCommission").addTextNode(flightSearchVO?.flightFare?.agentCommission)
        fare.addChildElement("TdsOnCommission").addTextNode(flightSearchVO?.flightFare?.tdsOnCommission)
        fare.addChildElement("IncentiveEarned").addTextNode(flightSearchVO?.flightFare?.incentiveEarned)
        fare.addChildElement("TdsOnIncentive").addTextNode(flightSearchVO?.flightFare?.tdsOnIncentive)
        fare.addChildElement("PLBEarned").addTextNode(flightSearchVO?.flightFare?.pLBEarned)
        fare.addChildElement("TdsOnPLB").addTextNode(flightSearchVO?.flightFare?.tdsOnPLB)
        fare.addChildElement("PublishedPrice").addTextNode(flightSearchVO?.flightFare?.publishedPrice)
        fare.addChildElement("AirTransFee").addTextNode(flightSearchVO?.flightFare?.airTransFee)
        fare.addChildElement("Currency").addTextNode("INR")
        fare.addChildElement("Discount").addTextNode(flightSearchVO?.flightFare?.discount)
        fare.addChildElement("OtherCharges").addTextNode(flightSearchVO?.flightFare?.otherCharges)
        fare.addChildElement("FuelSurcharge").addTextNode(flightSearchVO?.flightFare?.fuelSurcharge)
        fare.addChildElement("TransactionFee").addTextNode(flightSearchVO?.flightFare?.transactionFee)
        fare.addChildElement("ReverseHandlingCharge").addTextNode(flightSearchVO?.flightFare?.reverseHandlingCharge)
        fare.addChildElement("OfferedFare").addTextNode(flightSearchVO?.flightFare?.offeredFare)
        fare.addChildElement("AgentServiceCharge").addTextNode(flightSearchVO?.flightFare?.agentServiceCharge)

        def passenger = reqElementTag.addChildElement("Passenger");

        if (adult1FName) {
            def wSPassenger = passenger.addChildElement("WSPassenger");
            wSPassenger.addChildElement("Title").addTextNode(adult1TName)
            wSPassenger.addChildElement("FirstName").addTextNode(adult1FName)
            wSPassenger.addChildElement("LastName").addTextNode(adult1LName)
            wSPassenger.addChildElement("Type").addTextNode("Adult")
            wSPassenger.addChildElement("DateOfBirth").addTextNode("1988-02-12T00:00:00")
            def passFare = wSPassenger.addChildElement("Fare");

            passFare.addChildElement("BaseFare").addTextNode(flightSearchVO?.fareBreakDown[0]?.baseFare)
            passFare.addChildElement("Tax").addTextNode(flightSearchVO?.fareBreakDown[0]?.tax)
            passFare.addChildElement("ServiceTax").addTextNode("0")
            passFare.addChildElement("AdditionalTxnFee").addTextNode(flightSearchVO?.fareBreakDown[0]?.additionalTxnFee)
            passFare.addChildElement("AgentCommission").addTextNode("0")
            passFare.addChildElement("TdsOnCommission").addTextNode("0")
            passFare.addChildElement("IncentiveEarned").addTextNode("0")
            passFare.addChildElement("TdsOnIncentive").addTextNode("0")
            passFare.addChildElement("PLBEarned").addTextNode("0")
            passFare.addChildElement("TdsOnPLB").addTextNode("0")
            passFare.addChildElement("PublishedPrice").addTextNode("0")
            passFare.addChildElement("AirTransFee").addTextNode("0")
            passFare.addChildElement("Currency").addTextNode("INR") //Make sure the currency
            passFare.addChildElement("Discount").addTextNode("0")
            passFare.addChildElement("OtherCharges").addTextNode("0")
            passFare.addChildElement("FuelSurcharge").addTextNode(flightSearchVO?.fareBreakDown[0]?.fuelSurcharge)
            passFare.addChildElement("TransactionFee").addTextNode("0")
            passFare.addChildElement("ReverseHandlingCharge").addTextNode("0")
            passFare.addChildElement("OfferedFare").addTextNode("0")
            passFare.addChildElement("AgentServiceCharge").addTextNode(flightSearchVO?.fareBreakDown[0]?.agentServiceCharge)

            wSPassenger.addChildElement("Gender").addTextNode("Male")
            wSPassenger.addChildElement("PassportNumber").addTextNode("")
            wSPassenger.addChildElement("PassportExpiry").addTextNode("0001-01-01T00:00:00")
            wSPassenger.addChildElement("PinCode").addTextNode("")
            wSPassenger.addChildElement("Country").addTextNode("IN")//Country Code
            wSPassenger.addChildElement("Phone").addTextNode("9717544305")
            wSPassenger.addChildElement("AddressLine1").addTextNode("Cannout Place")
            wSPassenger.addChildElement("AddressLine2").addTextNode("")
            wSPassenger.addChildElement("Email").addTextNode(adult1Email)
            def meal = wSPassenger.addChildElement("Meal");
            meal.addChildElement("Code").addTextNode("")
            meal.addChildElement("Description").addTextNode("")
            def seat = wSPassenger.addChildElement("Seat");
            seat.addChildElement("Code").addTextNode("")
            seat.addChildElement("Description").addTextNode("")
            wSPassenger.addChildElement("FFAirline").addTextNode("")
            wSPassenger.addChildElement("FFNumber").addTextNode("")
        }

        if (adult2FName) {
            def wSPassenger = passenger.addChildElement("WSPassenger");
            wSPassenger.addChildElement("Title").addTextNode(adult2TName)
            wSPassenger.addChildElement("FirstName").addTextNode(adult2FName)
            wSPassenger.addChildElement("LastName").addTextNode(adult2LName)
            wSPassenger.addChildElement("Type").addTextNode("Adult")
            wSPassenger.addChildElement("DateOfBirth").addTextNode("1988-02-12T00:00:00")
            def passFare = wSPassenger.addChildElement("Fare");

            passFare.addChildElement("BaseFare").addTextNode(flightSearchVO?.fareBreakDown[0]?.baseFare)
            passFare.addChildElement("Tax").addTextNode(flightSearchVO?.fareBreakDown[0]?.tax)
            passFare.addChildElement("ServiceTax").addTextNode("0")
            passFare.addChildElement("AdditionalTxnFee").addTextNode(flightSearchVO?.fareBreakDown[0]?.additionalTxnFee)
            passFare.addChildElement("AgentCommission").addTextNode("0")
            passFare.addChildElement("TdsOnCommission").addTextNode("0")
            passFare.addChildElement("IncentiveEarned").addTextNode("0")
            passFare.addChildElement("TdsOnIncentive").addTextNode("0")
            passFare.addChildElement("PLBEarned").addTextNode("0")
            passFare.addChildElement("TdsOnPLB").addTextNode("0")
            passFare.addChildElement("PublishedPrice").addTextNode("0")
            passFare.addChildElement("AirTransFee").addTextNode("0")
            passFare.addChildElement("Currency").addTextNode("INR") //Make sure the currency
            passFare.addChildElement("Discount").addTextNode("0")
            passFare.addChildElement("OtherCharges").addTextNode("0")
            passFare.addChildElement("FuelSurcharge").addTextNode(flightSearchVO?.fareBreakDown[0]?.fuelSurcharge)
            passFare.addChildElement("TransactionFee").addTextNode("0")
            passFare.addChildElement("ReverseHandlingCharge").addTextNode("0")
            passFare.addChildElement("OfferedFare").addTextNode("0")
            passFare.addChildElement("AgentServiceCharge").addTextNode(flightSearchVO?.fareBreakDown[0]?.agentServiceCharge)

            wSPassenger.addChildElement("Gender").addTextNode("Male")
            wSPassenger.addChildElement("PassportNumber").addTextNode("")
            wSPassenger.addChildElement("PassportExpiry").addTextNode("0001-01-01T00:00:00")
            wSPassenger.addChildElement("PinCode").addTextNode("")
            wSPassenger.addChildElement("Country").addTextNode("IN")//Country Code
            wSPassenger.addChildElement("Phone").addTextNode("9717544305")
            wSPassenger.addChildElement("AddressLine1").addTextNode("Cannout Place")
            wSPassenger.addChildElement("AddressLine2").addTextNode("")
            wSPassenger.addChildElement("Email").addTextNode(adult1Email)
            def meal = wSPassenger.addChildElement("Meal");
            meal.addChildElement("Code").addTextNode("")
            meal.addChildElement("Description").addTextNode("")
            def seat = wSPassenger.addChildElement("Seat");
            seat.addChildElement("Code").addTextNode("")
            seat.addChildElement("Description").addTextNode("")
            wSPassenger.addChildElement("FFAirline").addTextNode("")
            wSPassenger.addChildElement("FFNumber").addTextNode("")
        }

        if (adult3FName) {
            def wSPassenger = passenger.addChildElement("WSPassenger");
            wSPassenger.addChildElement("Title").addTextNode(adult3TName)
            wSPassenger.addChildElement("FirstName").addTextNode(adult3FName)
            wSPassenger.addChildElement("LastName").addTextNode(adult3LName)
            wSPassenger.addChildElement("Type").addTextNode("Adult")
            wSPassenger.addChildElement("DateOfBirth").addTextNode("1988-02-12T00:00:00")
            def passFare = wSPassenger.addChildElement("Fare");

            passFare.addChildElement("BaseFare").addTextNode(flightSearchVO?.fareBreakDown[0]?.baseFare)
            passFare.addChildElement("Tax").addTextNode(flightSearchVO?.fareBreakDown[0]?.tax)
            passFare.addChildElement("ServiceTax").addTextNode("0")
            passFare.addChildElement("AdditionalTxnFee").addTextNode(flightSearchVO?.fareBreakDown[0]?.additionalTxnFee)
            passFare.addChildElement("AgentCommission").addTextNode("0")
            passFare.addChildElement("TdsOnCommission").addTextNode("0")
            passFare.addChildElement("IncentiveEarned").addTextNode("0")
            passFare.addChildElement("TdsOnIncentive").addTextNode("0")
            passFare.addChildElement("PLBEarned").addTextNode("0")
            passFare.addChildElement("TdsOnPLB").addTextNode("0")
            passFare.addChildElement("PublishedPrice").addTextNode("0")
            passFare.addChildElement("AirTransFee").addTextNode("0")
            passFare.addChildElement("Currency").addTextNode("INR") //Make sure the currency
            passFare.addChildElement("Discount").addTextNode("0")
            passFare.addChildElement("OtherCharges").addTextNode("0")
            passFare.addChildElement("FuelSurcharge").addTextNode(flightSearchVO?.fareBreakDown[0]?.fuelSurcharge)
            passFare.addChildElement("TransactionFee").addTextNode("0")
            passFare.addChildElement("ReverseHandlingCharge").addTextNode("0")
            passFare.addChildElement("OfferedFare").addTextNode("0")
            passFare.addChildElement("AgentServiceCharge").addTextNode(flightSearchVO?.fareBreakDown[0]?.agentServiceCharge)

            wSPassenger.addChildElement("Gender").addTextNode("Male")
            wSPassenger.addChildElement("PassportNumber").addTextNode("")
            wSPassenger.addChildElement("PassportExpiry").addTextNode("0001-01-01T00:00:00")
            wSPassenger.addChildElement("PinCode").addTextNode("")
            wSPassenger.addChildElement("Country").addTextNode("IN")//Country Code
            wSPassenger.addChildElement("Phone").addTextNode("9717544305")
            wSPassenger.addChildElement("AddressLine1").addTextNode("Cannout Place")
            wSPassenger.addChildElement("AddressLine2").addTextNode("")
            wSPassenger.addChildElement("Email").addTextNode(adult1Email)
            def meal = wSPassenger.addChildElement("Meal");
            meal.addChildElement("Code").addTextNode("")
            meal.addChildElement("Description").addTextNode("")
            def seat = wSPassenger.addChildElement("Seat");
            seat.addChildElement("Code").addTextNode("")
            seat.addChildElement("Description").addTextNode("")
            wSPassenger.addChildElement("FFAirline").addTextNode("")
            wSPassenger.addChildElement("FFNumber").addTextNode("")
        }
        if (adult4FName) {
            def wSPassenger = passenger.addChildElement("WSPassenger");
            wSPassenger.addChildElement("Title").addTextNode(adult4TName)
            wSPassenger.addChildElement("FirstName").addTextNode(adult4FName)
            wSPassenger.addChildElement("LastName").addTextNode(adult4LName)
            wSPassenger.addChildElement("Type").addTextNode("Adult")
            wSPassenger.addChildElement("DateOfBirth").addTextNode("1988-02-12T00:00:00")
            def passFare = wSPassenger.addChildElement("Fare");

            passFare.addChildElement("BaseFare").addTextNode(flightSearchVO?.fareBreakDown[0]?.baseFare)
            passFare.addChildElement("Tax").addTextNode(flightSearchVO?.fareBreakDown[0]?.tax)
            passFare.addChildElement("ServiceTax").addTextNode("0")
            passFare.addChildElement("AdditionalTxnFee").addTextNode(flightSearchVO?.fareBreakDown[0]?.additionalTxnFee)
            passFare.addChildElement("AgentCommission").addTextNode("0")
            passFare.addChildElement("TdsOnCommission").addTextNode("0")
            passFare.addChildElement("IncentiveEarned").addTextNode("0")
            passFare.addChildElement("TdsOnIncentive").addTextNode("0")
            passFare.addChildElement("PLBEarned").addTextNode("0")
            passFare.addChildElement("TdsOnPLB").addTextNode("0")
            passFare.addChildElement("PublishedPrice").addTextNode("0")
            passFare.addChildElement("AirTransFee").addTextNode("0")
            passFare.addChildElement("Currency").addTextNode("INR") //Make sure the currency
            passFare.addChildElement("Discount").addTextNode("0")
            passFare.addChildElement("OtherCharges").addTextNode("0")
            passFare.addChildElement("FuelSurcharge").addTextNode(flightSearchVO?.fareBreakDown[0]?.fuelSurcharge)
            passFare.addChildElement("TransactionFee").addTextNode("0")
            passFare.addChildElement("ReverseHandlingCharge").addTextNode("0")
            passFare.addChildElement("OfferedFare").addTextNode("0")
            passFare.addChildElement("AgentServiceCharge").addTextNode(flightSearchVO?.fareBreakDown[0]?.agentServiceCharge)

            wSPassenger.addChildElement("Gender").addTextNode("Male")
            wSPassenger.addChildElement("PassportNumber").addTextNode("")
            wSPassenger.addChildElement("PassportExpiry").addTextNode("0001-01-01T00:00:00")
            wSPassenger.addChildElement("PinCode").addTextNode("")
            wSPassenger.addChildElement("Country").addTextNode("IN")//Country Code
            wSPassenger.addChildElement("Phone").addTextNode("9717544305")
            wSPassenger.addChildElement("AddressLine1").addTextNode("Cannout Place")
            wSPassenger.addChildElement("AddressLine2").addTextNode("")
            wSPassenger.addChildElement("Email").addTextNode(adult1Email)
            def meal = wSPassenger.addChildElement("Meal");
            meal.addChildElement("Code").addTextNode("")
            meal.addChildElement("Description").addTextNode("")
            def seat = wSPassenger.addChildElement("Seat");
            seat.addChildElement("Code").addTextNode("")
            seat.addChildElement("Description").addTextNode("")
            wSPassenger.addChildElement("FFAirline").addTextNode("")
            wSPassenger.addChildElement("FFNumber").addTextNode("")
        }
        if (adult5FName) {
            def wSPassenger = passenger.addChildElement("WSPassenger");
            wSPassenger.addChildElement("Title").addTextNode(adult5TName)
            wSPassenger.addChildElement("FirstName").addTextNode(adult5FName)
            wSPassenger.addChildElement("LastName").addTextNode(adult5LName)
            wSPassenger.addChildElement("Type").addTextNode("Adult")
            wSPassenger.addChildElement("DateOfBirth").addTextNode("1988-02-12T00:00:00")
            def passFare = wSPassenger.addChildElement("Fare");

            passFare.addChildElement("BaseFare").addTextNode(flightSearchVO?.fareBreakDown[0]?.baseFare)
            passFare.addChildElement("Tax").addTextNode(flightSearchVO?.fareBreakDown[0]?.tax)
            passFare.addChildElement("ServiceTax").addTextNode("0")
            passFare.addChildElement("AdditionalTxnFee").addTextNode(flightSearchVO?.fareBreakDown[0]?.additionalTxnFee)
            passFare.addChildElement("AgentCommission").addTextNode("0")
            passFare.addChildElement("TdsOnCommission").addTextNode("0")
            passFare.addChildElement("IncentiveEarned").addTextNode("0")
            passFare.addChildElement("TdsOnIncentive").addTextNode("0")
            passFare.addChildElement("PLBEarned").addTextNode("0")
            passFare.addChildElement("TdsOnPLB").addTextNode("0")
            passFare.addChildElement("PublishedPrice").addTextNode("0")
            passFare.addChildElement("AirTransFee").addTextNode("0")
            passFare.addChildElement("Currency").addTextNode("INR") //Make sure the currency
            passFare.addChildElement("Discount").addTextNode("0")
            passFare.addChildElement("OtherCharges").addTextNode("0")
            passFare.addChildElement("FuelSurcharge").addTextNode(flightSearchVO?.fareBreakDown[0]?.fuelSurcharge)
            passFare.addChildElement("TransactionFee").addTextNode("0")
            passFare.addChildElement("ReverseHandlingCharge").addTextNode("0")
            passFare.addChildElement("OfferedFare").addTextNode("0")
            passFare.addChildElement("AgentServiceCharge").addTextNode(flightSearchVO?.fareBreakDown[0]?.agentServiceCharge)

            wSPassenger.addChildElement("Gender").addTextNode("Male")
            wSPassenger.addChildElement("PassportNumber").addTextNode("")
            wSPassenger.addChildElement("PassportExpiry").addTextNode("0001-01-01T00:00:00")
            wSPassenger.addChildElement("PinCode").addTextNode("")
            wSPassenger.addChildElement("Country").addTextNode("IN")//Country Code
            wSPassenger.addChildElement("Phone").addTextNode("9717544305")
            wSPassenger.addChildElement("AddressLine1").addTextNode("Cannout Place")
            wSPassenger.addChildElement("AddressLine2").addTextNode("")
            wSPassenger.addChildElement("Email").addTextNode(adult1Email)
            def meal = wSPassenger.addChildElement("Meal");
            meal.addChildElement("Code").addTextNode("")
            meal.addChildElement("Description").addTextNode("")
            def seat = wSPassenger.addChildElement("Seat");
            seat.addChildElement("Code").addTextNode("")
            seat.addChildElement("Description").addTextNode("")
            wSPassenger.addChildElement("FFAirline").addTextNode("")
            wSPassenger.addChildElement("FFNumber").addTextNode("")
        }
        if (child1FName) {
            def wSPassenger = passenger.addChildElement("WSPassenger");
            wSPassenger.addChildElement("Title").addTextNode(child1TName)
            wSPassenger.addChildElement("FirstName").addTextNode(child1FName)
            wSPassenger.addChildElement("LastName").addTextNode(child1LName)
            wSPassenger.addChildElement("Type").addTextNode("Adult")
            wSPassenger.addChildElement("DateOfBirth").addTextNode("1988-02-12T00:00:00")
            def passFare = wSPassenger.addChildElement("Fare");

            passFare.addChildElement("BaseFare").addTextNode(flightSearchVO?.fareBreakDown[1]?.baseFare)
            passFare.addChildElement("Tax").addTextNode(flightSearchVO?.fareBreakDown[1]?.tax)
            passFare.addChildElement("ServiceTax").addTextNode("0")
            passFare.addChildElement("AdditionalTxnFee").addTextNode(flightSearchVO?.fareBreakDown[1]?.additionalTxnFee)
            passFare.addChildElement("AgentCommission").addTextNode("0")
            passFare.addChildElement("TdsOnCommission").addTextNode("0")
            passFare.addChildElement("IncentiveEarned").addTextNode("0")
            passFare.addChildElement("TdsOnIncentive").addTextNode("0")
            passFare.addChildElement("PLBEarned").addTextNode("0")
            passFare.addChildElement("TdsOnPLB").addTextNode("0")
            passFare.addChildElement("PublishedPrice").addTextNode("0")
            passFare.addChildElement("AirTransFee").addTextNode("0")
            passFare.addChildElement("Currency").addTextNode("INR") //Make sure the currency
            passFare.addChildElement("Discount").addTextNode("0")
            passFare.addChildElement("OtherCharges").addTextNode("0")
            passFare.addChildElement("FuelSurcharge").addTextNode(flightSearchVO?.fareBreakDown[1]?.fuelSurcharge)
            passFare.addChildElement("TransactionFee").addTextNode("0")
            passFare.addChildElement("ReverseHandlingCharge").addTextNode("0")
            passFare.addChildElement("OfferedFare").addTextNode("0")
            passFare.addChildElement("AgentServiceCharge").addTextNode(flightSearchVO?.fareBreakDown[1]?.agentServiceCharge)

            wSPassenger.addChildElement("Gender").addTextNode("Male")
            wSPassenger.addChildElement("PassportNumber").addTextNode("")
            wSPassenger.addChildElement("PassportExpiry").addTextNode("0001-01-01T00:00:00")
            wSPassenger.addChildElement("PinCode").addTextNode("")
            wSPassenger.addChildElement("Country").addTextNode("IN")//Country Code
            wSPassenger.addChildElement("Phone").addTextNode("9717544305")
            wSPassenger.addChildElement("AddressLine1").addTextNode("Cannout Place")
            wSPassenger.addChildElement("AddressLine2").addTextNode("")
            wSPassenger.addChildElement("Email").addTextNode(adult1Email)
            def meal = wSPassenger.addChildElement("Meal");
            meal.addChildElement("Code").addTextNode("")
            meal.addChildElement("Description").addTextNode("")
            def seat = wSPassenger.addChildElement("Seat");
            seat.addChildElement("Code").addTextNode("")
            seat.addChildElement("Description").addTextNode("")
            wSPassenger.addChildElement("FFAirline").addTextNode("")
            wSPassenger.addChildElement("FFNumber").addTextNode("")
        }
        if (child2FName) {
            def wSPassenger = passenger.addChildElement("WSPassenger");
            wSPassenger.addChildElement("Title").addTextNode(child2TName)
            wSPassenger.addChildElement("FirstName").addTextNode(child2FName)
            wSPassenger.addChildElement("LastName").addTextNode(child2LName)
            wSPassenger.addChildElement("Type").addTextNode("Adult")
            wSPassenger.addChildElement("DateOfBirth").addTextNode("1988-02-12T00:00:00")
            def passFare = wSPassenger.addChildElement("Fare");

            passFare.addChildElement("BaseFare").addTextNode(flightSearchVO?.fareBreakDown[1]?.baseFare)
            passFare.addChildElement("Tax").addTextNode(flightSearchVO?.fareBreakDown[1]?.tax)
            passFare.addChildElement("ServiceTax").addTextNode("0")
            passFare.addChildElement("AdditionalTxnFee").addTextNode(flightSearchVO?.fareBreakDown[1]?.additionalTxnFee)
            passFare.addChildElement("AgentCommission").addTextNode("0")
            passFare.addChildElement("TdsOnCommission").addTextNode("0")
            passFare.addChildElement("IncentiveEarned").addTextNode("0")
            passFare.addChildElement("TdsOnIncentive").addTextNode("0")
            passFare.addChildElement("PLBEarned").addTextNode("0")
            passFare.addChildElement("TdsOnPLB").addTextNode("0")
            passFare.addChildElement("PublishedPrice").addTextNode("0")
            passFare.addChildElement("AirTransFee").addTextNode("0")
            passFare.addChildElement("Currency").addTextNode("INR") //Make sure the currency
            passFare.addChildElement("Discount").addTextNode("0")
            passFare.addChildElement("OtherCharges").addTextNode("0")
            passFare.addChildElement("FuelSurcharge").addTextNode(flightSearchVO?.fareBreakDown[1]?.fuelSurcharge)
            passFare.addChildElement("TransactionFee").addTextNode("0")
            passFare.addChildElement("ReverseHandlingCharge").addTextNode("0")
            passFare.addChildElement("OfferedFare").addTextNode("0")
            passFare.addChildElement("AgentServiceCharge").addTextNode(flightSearchVO?.fareBreakDown[1]?.agentServiceCharge)

            wSPassenger.addChildElement("Gender").addTextNode("Male")
            wSPassenger.addChildElement("PassportNumber").addTextNode("")
            wSPassenger.addChildElement("PassportExpiry").addTextNode("0001-01-01T00:00:00")
            wSPassenger.addChildElement("PinCode").addTextNode("")
            wSPassenger.addChildElement("Country").addTextNode("IN")//Country Code
            wSPassenger.addChildElement("Phone").addTextNode("9717544305")
            wSPassenger.addChildElement("AddressLine1").addTextNode("Cannout Place")
            wSPassenger.addChildElement("AddressLine2").addTextNode("")
            wSPassenger.addChildElement("Email").addTextNode(adult1Email)
            def meal = wSPassenger.addChildElement("Meal");
            meal.addChildElement("Code").addTextNode("")
            meal.addChildElement("Description").addTextNode("")
            def seat = wSPassenger.addChildElement("Seat");
            seat.addChildElement("Code").addTextNode("")
            seat.addChildElement("Description").addTextNode("")
            wSPassenger.addChildElement("FFAirline").addTextNode("")
            wSPassenger.addChildElement("FFNumber").addTextNode("")
        }
        if (child3FName) {
            def wSPassenger = passenger.addChildElement("WSPassenger");
            wSPassenger.addChildElement("Title").addTextNode(child3TName)
            wSPassenger.addChildElement("FirstName").addTextNode(child3FName)
            wSPassenger.addChildElement("LastName").addTextNode(child3LName)
            wSPassenger.addChildElement("Type").addTextNode("Adult")
            wSPassenger.addChildElement("DateOfBirth").addTextNode("1988-02-12T00:00:00")
            def passFare = wSPassenger.addChildElement("Fare");

            passFare.addChildElement("BaseFare").addTextNode(flightSearchVO?.fareBreakDown[1]?.baseFare)
            passFare.addChildElement("Tax").addTextNode(flightSearchVO?.fareBreakDown[1]?.tax)
            passFare.addChildElement("ServiceTax").addTextNode("0")
            passFare.addChildElement("AdditionalTxnFee").addTextNode(flightSearchVO?.fareBreakDown[1]?.additionalTxnFee)
            passFare.addChildElement("AgentCommission").addTextNode("0")
            passFare.addChildElement("TdsOnCommission").addTextNode("0")
            passFare.addChildElement("IncentiveEarned").addTextNode("0")
            passFare.addChildElement("TdsOnIncentive").addTextNode("0")
            passFare.addChildElement("PLBEarned").addTextNode("0")
            passFare.addChildElement("TdsOnPLB").addTextNode("0")
            passFare.addChildElement("PublishedPrice").addTextNode("0")
            passFare.addChildElement("AirTransFee").addTextNode("0")
            passFare.addChildElement("Currency").addTextNode("INR") //Make sure the currency
            passFare.addChildElement("Discount").addTextNode("0")
            passFare.addChildElement("OtherCharges").addTextNode("0")
            passFare.addChildElement("FuelSurcharge").addTextNode(flightSearchVO?.fareBreakDown[1]?.fuelSurcharge)
            passFare.addChildElement("TransactionFee").addTextNode("0")
            passFare.addChildElement("ReverseHandlingCharge").addTextNode("0")
            passFare.addChildElement("OfferedFare").addTextNode("0")
            passFare.addChildElement("AgentServiceCharge").addTextNode(flightSearchVO?.fareBreakDown[1]?.agentServiceCharge)

            wSPassenger.addChildElement("Gender").addTextNode("Male")
            wSPassenger.addChildElement("PassportNumber").addTextNode("")
            wSPassenger.addChildElement("PassportExpiry").addTextNode("0001-01-01T00:00:00")
            wSPassenger.addChildElement("PinCode").addTextNode("")
            wSPassenger.addChildElement("Country").addTextNode("IN")//Country Code
            wSPassenger.addChildElement("Phone").addTextNode("9717544305")
            wSPassenger.addChildElement("AddressLine1").addTextNode("Cannout Place")
            wSPassenger.addChildElement("AddressLine2").addTextNode("")
            wSPassenger.addChildElement("Email").addTextNode(adult1Email)
            def meal = wSPassenger.addChildElement("Meal");
            meal.addChildElement("Code").addTextNode("")
            meal.addChildElement("Description").addTextNode("")
            def seat = wSPassenger.addChildElement("Seat");
            seat.addChildElement("Code").addTextNode("")
            seat.addChildElement("Description").addTextNode("")
            wSPassenger.addChildElement("FFAirline").addTextNode("")
            wSPassenger.addChildElement("FFNumber").addTextNode("")
        }


        reqElementTag.addChildElement("Origin").addTextNode(flightSearchVO?.origin);
        reqElementTag.addChildElement("Destination").addTextNode(flightSearchVO?.destination);
        def segment = reqElementTag.addChildElement("Segment");
        def wsSegment = segment.addChildElement("WSSegment");
        wsSegment.addChildElement("SegmentIndicator").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.segmentIndicator);
        def airLine = wsSegment.addChildElement("Airline");
        airLine.addChildElement("AirlineCode").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.airLineCode);
        airLine.addChildElement("AirlineName").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.airLineName);
        airLine.addChildElement("AirLineRemarks").addTextNode("");

        wsSegment.addChildElement("FlightNumber").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.flightNumber);
        wsSegment.addChildElement("FareClass").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.fareClass);
        def origin = wsSegment.addChildElement("Origin");
        origin.addChildElement("AirportCode").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.originAirPortCode);
        origin.addChildElement("AirportName").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.originCountryName);
        origin.addChildElement("Terminal").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.originTerminal);
        origin.addChildElement("CityCode").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.originCityCode);
        origin.addChildElement("CityName").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.originCityName);
        origin.addChildElement("CountryCode").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.originCountryCode);
        origin.addChildElement("CountryName").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.originCountryName);

        def destination = wsSegment.addChildElement("Destination");
        destination.addChildElement("AirportCode").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.destinationAirPortCode);
        destination.addChildElement("AirportName").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.destinationAirPortName);
        destination.addChildElement("Terminal").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.destinationTerminal);
        destination.addChildElement("CityCode").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.destinationCityCode);
        destination.addChildElement("CityName").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.destinationCityName);
        destination.addChildElement("CountryCode").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.destinationCountryCode);
        destination.addChildElement("CountryName").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.destinationCountryName);

        wsSegment.addChildElement("DepTIme").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.departureTime);
        wsSegment.addChildElement("ArrTime").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.arrivalTime);
        wsSegment.addChildElement("ETicketEligible").addTextNode("true");
        wsSegment.addChildElement("Duration").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.duration);
        wsSegment.addChildElement("Stop").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.stops);
        wsSegment.addChildElement("Craft").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.craftType);
        wsSegment.addChildElement("Status").addTextNode("Confirmed");
        wsSegment.addChildElement("OperatingCarrier").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.operatingCarrier);

        reqElementTag.addChildElement("FareType").addTextNode("PUB");
        def fareRule = reqElementTag.addChildElement("FareRule");
        def wsFareRule = fareRule.addChildElement("WSFareRule");

        wsFareRule.addChildElement("Origin").addTextNode(flightSearchVO?.flightFareRule?.first()?.origin);
        wsFareRule.addChildElement("Destination").addTextNode(flightSearchVO?.flightFareRule?.first()?.destination);
        wsFareRule.addChildElement("Airline").addTextNode(flightSearchVO?.flightFareRule?.first()?.airline);
        wsFareRule.addChildElement("FareRestriction").addTextNode(flightSearchVO?.flightFareRule?.first()?.fareRestriction);
        wsFareRule.addChildElement("FareBasisCode").addTextNode(flightSearchVO?.flightFareRule?.first()?.fareBasisCode);
        wsFareRule.addChildElement("DepartureDate").addTextNode(flightSearchVO?.flightFareRule?.first()?.departureDate);
        wsFareRule.addChildElement("ReturnDate").addTextNode(flightSearchVO?.flightFareRule?.first()?.returnDate);
        wsFareRule.addChildElement("Source").addTextNode("Amadeus");

        reqElementTag.addChildElement("Source").addTextNode("Amadeus");
        def paymentInfo = reqElementTag.addChildElement("PaymentInformation");
        paymentInfo.addChildElement("PaymentInformationId").addTextNode("0");
        paymentInfo.addChildElement("InvoiceNumber").addTextNode("0");
        paymentInfo.addChildElement("PaymentId").addTextNode("0");
        paymentInfo.addChildElement("Amount").addTextNode(flightSearchVO?.flightFare?.offeredFare);
        paymentInfo.addChildElement("IPAddress").addTextNode("127.0.0.1");
        paymentInfo.addChildElement("TrackId").addTextNode("0");
        paymentInfo.addChildElement("PaymentGateway").addTextNode("APICustomer");
        paymentInfo.addChildElement("PaymentModeType").addTextNode("Deposited");

        reqElementTag.addChildElement("SessionId").addTextNode(flightSearchVO?.sessionId);
        reqElementTag.addChildElement("PromotionalPlanType").addTextNode("Normal");



        ByteArrayOutputStream outputStream = new ByteArrayOutputStream()
        soapMsg.writeTo(outputStream)

        String requestNew = "<?xml version='1.0' encoding='utf-16'?>" + outputStream.toString()

        println '---=requestNew ==========' + requestNew
        def client = new SOAPClient("http://api.tektravels.com/tboapi_v7/service.asmx")
        SOAPResponse response = client.send(requestNew)


        println '---==BOOK Response===' + response.text
        def bookresponse = new XmlSlurper().parseText(response.text)
        println '---==bookresponse==' + bookresponse

        println '=================PNR=============' + bookresponse.children().BookResponse.BookResult.PNR
        println '=================BookingId=============' + bookresponse.children().BookResponse.BookResult.BookingId
        println '=================StatusCode=============' + bookresponse.children().BookResponse.BookResult.Status.StatusCode
        println '=================Description=============' + bookresponse.children().BookResponse.BookResult.Status.Description
        println '=================Category=============' + bookresponse.children().BookResponse.BookResult.Status.Category
        println '=================SSRDenied=============' + bookresponse.children().BookResult.SSRDenied
        println '=================ProdType=============' + bookresponse.children().BookResponse.BookResult.ProdType

        render("SUCCESS")
    }

    def makePostCall() {
        String st = UUID.randomUUID().toString()
        println '====ST' + st
        String keyPayU = "JBZaLc"
        String amount = "100"
//        String txnid = "AAENVM76869HFHJGJ"
        String txnid = "53BC054A909DC9D3B165C80CCCA7F1707410B79AA7D9FFD909B68CE78E97DD48E00BF37E4A9A9D1B2D210849615703F77000EFC84EFC304EBFD97C77911B6E8A"
        String productinfo = "New Sandy Hotel"
        String firstname = "Kumar"
        String email = "kr.saurabh2@gmail.com"
        String phone = "9717544305"
        String surl = "http://127.0.0.1:8090/travelx"

        MessageDigest md = MessageDigest.getInstance("SHA-512");

        FileInputStream fis = new
                FileInputStream("/home/saurabh/loging.log");
        byte[] dataBytes = new byte[1024];
        int nread = 0;
        while ((nread = fis.read(dataBytes)) != -1) {
            md.update(dataBytes, 0, nread);
        };
        byte[] mdbytes = md.digest();
//convert the byte to hex format method
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < mdbytes.length; i++) {
            sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        System.out.println("Hex format : " + sb.toString());
//convert the byte to hex format method 2
        StringBuffer hexString = new StringBuffer(); for (int i = 0; i < mdbytes.length; i++) {
            hexString.append(Integer.toHexString(0xFF & mdbytes[i]));
        }
        System.out.println("Hex format : " + hexString.toString());



        String hash = hexString.toString()
//        String hash = "sha512 (JBZaLc|26443022|3000|New Sandy Hotel|Kumar|kr.saurabh2@gmail.com|new product1|new product2|new product3|new product4|new product5||||||GQs7yium)"
        String service_provider = "Travelex360"
        String outputData = null
        HttpURLConnection con = null
        try {
            String data = "key=" + URLEncoder.encode("${keyPayU}", "UTF-8")
            data += "&txnid=" + URLEncoder.encode("${txnid}", "UTF-8")
            data += "&amount=" + URLEncoder.encode("${amount}", "UTF-8")

            data += "&firstname=" + URLEncoder.encode("${firstname}", "UTF-8")
            data += "&email=" + URLEncoder.encode("${email}", "UTF-8")
            data += "&phone=" + URLEncoder.encode("${phone}", "UTF-8")
            data += "&productinfo=" + URLEncoder.encode("${productinfo}", "UTF-8")

            data += "&surl=" + URLEncoder.encode("${surl}", "UTF-8")
            data += "&furl=" + URLEncoder.encode("${surl}", "UTF-8")
            data += "&service_provider=" + URLEncoder.encode("${service_provider}", "UTF-8")

            data += "&hash=" + URLEncoder.encode("${hash}", "UTF-8")


            println '**********Parameters************' + data

//
            URL url = new URL(" https://test.payu.in/_payment")

//            println '^^^^^^^^^^URL^^^^^^' + url
            con = (HttpURLConnection) url.openConnection();
//            println '%%%%%%%%%%%^^^^^^^^^^^'
//            SAXParserFactory spf = SAXParserFactory.newInstance();
//            spf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);

            con.setRequestMethod("POST");

//            URL obj = new URL(url);
            con.setRequestProperty("Content-length", String.valueOf(data.length()));
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0;Windows98;DigExt)");
            con.setDoOutput(true);
            con.setDoInput(true);
//println'========con.getOutputStream()===='+con.getInputStream()
//            DataOutputStream wr = new DataOutputStream(con.getInputStream())
            DataOutputStream wr = new DataOutputStream(con.getOutputStream())


            wr.writeBytes(data);

//            wr.flush();
            wr.close();
//println'========con.getIntputStream()===='+con.getInputStream().text

            InputStream is = con.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));

            String line = rd.text;

            println '888888888rd887' + line

//            def response =  StringBuilder.newInstance()
//            println '9999999999999999999999999999997'
//
//            while (line=rd.readLine() != null) {
//                println '8xxxxxxxxxxxxxxxxxxxxxx'
//
//                response.append(line);
//                response.append('\r');
//            }
//
//            println '00000000000response.toString()0000000' + response.toString()

            render(contentType: "text/html", text: line);
//            return line;

//            rd.close();
//
//            println '408080870879807907979'
//            println '4============Stream==========' + con.getInputStream()
//
//            outputData = new XmlSlurper().parse(con.getInputStream())
//            println '-----------CancelBooking XML output-----------' + outputData
        }
        catch (
                Exception e
                ) {
            println '==========Eception======' + e
        }
        finally {

            if (con != null) {
                con.disconnect();
            }
        }
    }

    def makePostCall2() {
        String keyPayU = "JBZaLc"
        String amount = "100"
//        String txnid = "AAENVM76869HFHJGJ"
        String txnid = "53BC054A909DC9D3B165C80CCCA7F1707410B79AA7D9FFD909B68CE78E97DD48E00BF37E4A9A9D1B2D210849615703F77000EFC84EFC304EBFD97C77911B6E8A"
        String productinfo = "New Sandy Hotel"
        String firstname = "Kumar"
        String email = "kr.saurabh2@gmail.com"
        String phone = "9717544305"
        String surl = "http://127.0.0.1:8090/travelx"

        MessageDigest md = MessageDigest.getInstance("SHA-512");

        FileInputStream fis = new
                FileInputStream("/home/saurabh/loging.log");
        byte[] dataBytes = new byte[1024];
        int nread = 0;
        while ((nread = fis.read(dataBytes)) != -1) {
            md.update(dataBytes, 0, nread);
        };
        byte[] mdbytes = md.digest();
//convert the byte to hex format method
        StringBuffer sb1 = new StringBuffer();
        for (int i = 0; i < mdbytes.length; i++) {
            sb1.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        System.out.println("Hex format : " + sb1.toString());
//convert the byte to hex format method 2
        StringBuffer hexString = new StringBuffer(); for (int i = 0; i < mdbytes.length; i++) {
            hexString.append(Integer.toHexString(0xFF & mdbytes[i]));
        }
        System.out.println("Hex format : " + hexString.toString());



        String hash = hexString.toString()


        String serverUrl = grailsApplication.config.grails.serverURL
//        String callbackurl = "${serverUrl}/google/googleCallback"
        StringBuilder sb = new StringBuilder("key=");

        sb.append(URLEncoder.encode(keyPayU, "UTF-8"));
        sb.append("&txnid=");
        sb.append(URLEncoder.encode("${txnid}", "UTF-8"));
        sb.append("&amount=");
        sb.append(URLEncoder.encode("${amount}", "UTF-8"));
        sb.append("&firstname=");
        sb.append(URLEncoder.encode("${firstname}", "UTF-8"));
        sb.append("&email=");
        sb.append(URLEncoder.encode("${email}", "UTF-8"));
        sb.append("&phone=");
        sb.append(URLEncoder.encode("${phone}", "UTF-8"));
        sb.append("&surl=");
        sb.append(URLEncoder.encode("${surl}", "UTF-8"));
        sb.append("&furl=");
        sb.append(URLEncoder.encode("${surl}", "UTF-8"));
        sb.append("&service_provider=");
        sb.append(URLEncoder.encode("travelex360.com", "UTF-8"));
        sb.append("&hash=");
        sb.append(URLEncoder.encode("${hash}", "UTF-8"));
        URL url = new URL('https://test.payu.in/_payment');
        HttpURLConnection connection = (HttpURLConnection) url.openConnection()
        String accessToken = ''
        try {
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//            connection.setRequestProperty("Content-Length", "" + sb.toString().length());
//            connection.setRequestProperty("Host", "accounts.google.com");
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream());
            outputStreamWriter.write(sb.toString());
            outputStreamWriter.flush();
            String resultData = connection.content.text
            println '=======resultData===' + resultData
            render(contentType: "text/html", text: resultData);
//            def responseJson = JSON.parse(resultData)
//            accessToken = responseJson?.access_token
        }
        catch (Exception e) {
            println '=======Exception=====' + e
            println("Failed to get access token from google " + e)
        }
        return accessToken

    }

    def testApp() {

        String st = UUID.randomUUID().toString()
        println '====ST' + st
        String keyPayU = "JBZaLc"
        String amount = "100"
//        String txnid = "AAENVM76869HFHJGJ"
        String txnid = "53BC054A909DC9D3B165C80CCCA7F1707410B79AA7D9FFD909B68CE78E97DD48E00BF37E4A9A9D1B2D210849615703F77000EFC84EFC304EBFD97C77911B6E8A"
        String productinfo = "New Sandy Hotel"
        String firstname = "Kumar"
        String email = "kr.saurabh2@gmail.com"
        String phone = "9717544305"
        String surl = "http://127.0.0.1:8090/travelx"

        MessageDigest md = MessageDigest.getInstance("SHA-512");

        FileInputStream fis = new
                FileInputStream("/home/saurabh/loging.log");
        byte[] dataBytes = new byte[1024];
        int nread = 0;
        while ((nread = fis.read(dataBytes)) != -1) {
            md.update(dataBytes, 0, nread);
        };
        byte[] mdbytes = md.digest();
//convert the byte to hex format method
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < mdbytes.length; i++) {
            sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        System.out.println("Hex format : " + sb.toString());
//convert the byte to hex format method 2
        StringBuffer hexString = new StringBuffer(); for (int i = 0; i < mdbytes.length; i++) {
            hexString.append(Integer.toHexString(0xFF & mdbytes[i]));
        }
        System.out.println("Hex format : " + hexString.toString());



        String hash = hexString.toString()
//        String hash = "sha512 (JBZaLc|26443022|3000|New Sandy Hotel|Kumar|kr.saurabh2@gmail.com|new product1|new product2|new product3|new product4|new product5||||||GQs7yium)"
        String service_provider = "Travelex360"
        String outputData = null
        HttpURLConnection con = null

        String data = "key=" + URLEncoder.encode("${keyPayU}", "UTF-8")
        data += "&txnid=" + URLEncoder.encode("${txnid}", "UTF-8")
        data += "&amount=" + URLEncoder.encode("${amount}", "UTF-8")

        data += "&firstname=" + URLEncoder.encode("${firstname}", "UTF-8")
        data += "&email=" + URLEncoder.encode("${email}", "UTF-8")
        data += "&phone=" + URLEncoder.encode("${phone}", "UTF-8")
        data += "&productinfo=" + URLEncoder.encode("${productinfo}", "UTF-8")

        data += "&surl=" + URLEncoder.encode("${surl}", "UTF-8")
        data += "&furl=" + URLEncoder.encode("${surl}", "UTF-8")
        data += "&service_provider=" + URLEncoder.encode("${service_provider}", "UTF-8")

        data += "&hash=" + URLEncoder.encode("${hash}", "UTF-8")


        URL url;
        HttpURLConnection connection = null;
        String targetURL = "https://test.payu.in/_payment"
        try {
            //Create connection
            url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length", "" +
                    Integer.toString(data.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.writeBytes(data);
            wr.flush();
            wr.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            println '========RESPONSE+======' + response.toString();
            render("SUCCESS")
//            return response.toString();

        } catch (Exception e) {

            e.printStackTrace();
            return null;

        } finally {

            if (connection != null) {
                connection.disconnect();
            }
        }
//        render(view: "/payuform")
    }

    def renderTest() {


        String message = "JBZaLc|ABCD123|3000|New Hotel|Kumar|kr.saurabh2@gmail.com|||||||||||GQs7yium";
        String hash = hashCalculationService.calculateHashCode(message)
//        String hash = "cdc4b2273b28ba85881f527af6d27bd95c7259740eb0446e8131bc81db7c962873fde78e30361226ac3ea0297df7755224e3e7f80bacc1b4fc971154cb1a05bf"
        render(view: "/payuform", model: [hash: hash])
//        render("SUCCESS")
    }

    def returnOnLocal() {
        println '============PARAMS===='
        println '============PARAMS====' + params
    }

    def receiveDataPayumoney() {

        println '==============Inside Payumoney receive==========='
        println '==============Inside Payumoney receive==========='
        println '==============PARAMS========' + params
        println '==============PARAMS========' + params.tboHotelBookingVO
    }
}
