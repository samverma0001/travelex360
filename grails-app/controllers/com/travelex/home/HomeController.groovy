package com.travelex.home

import activity.ActivityBookingHistory
import com.travelex.Hotel.AddToActivitySummary
import com.travelex.Hotel.AddToTravelSummary
import com.travelex.Hotel.BookingHistory
import com.travelex.User.PaymentInfo
import com.travelex.User.Role
import com.travelex.User.User
import com.travelex.admin.BookingStatusType
import com.travelex.booking.HotelForBooking
import com.travelex.booking.RoomBooked
import com.travelex.bootstrap.UtilService
import com.travelex.flight.BookedFlight
import com.travelex.flight.BookedFlightFare
import com.travelex.flight.BookedFlightPassenger
import com.travelex.flight.BookedFlightTicket
import com.travelex.socials.login.ResetPasswordCo
import com.travelex.socials.login.UpdateUserCO
import grails.plugin.mail.MailService
import grails.plugins.springsecurity.Secured
import serviceProvider.ServiceProvider
import wslite.soap.SOAPClient
import wslite.soap.SOAPResponse

class HomeController {

    def springSecurityService
    MailService mailService
    def grailsApplication

    @Secured(["ROLE_USER", "ROLE_ADMIN", "ROLE_PROVIDER"])
    def index = {
        print('-----------------------------' + springSecurityService.isLoggedIn())
        User user = springSecurityService.currentUser as User
        Role admin = Role.findByAuthority("ROLE_ADMIN")
        Role serviceProviderRole = Role.findByAuthority("ROLE_PROVIDER")
        Role userRole = Role.findByAuthority("ROLE_USER")
        if (user?.authorities?.contains(admin)) {
            print('=======current user is======ADMIN==')
            redirect(controller: "admin", action: "index")
        } else if (user?.authorities?.contains(serviceProviderRole)) {
            print('=======current user is======SERVICE PROVIDER==')
            ServiceProvider serviceProvider = ServiceProvider.findByUser(user)
            print('-------------serviceProvider----------' + serviceProvider)
            render(view: '/serviceProviderViews/profilePageForServiceProvider', model: [serviceProvider: serviceProvider,
                                                                                        user           : user])
        } else if (user?.authorities?.contains(userRole)) {
            print('=======current user is======NORMAL USER==')
            render(view: '/home/userLanding')
        } else {
            redirect(controller: "login", action: "index")
        }
    }

    def landingPage = {
        render(view: '/index')
    }


    def userLanding = {
        User user = springSecurityService.currentUser as User
        List<AddToActivitySummary> addToActivitySummaryList = AddToActivitySummary.findAllByUser(user)
        List<AddToTravelSummary> addToTravelSummaryList = AddToTravelSummary.findAllByUser(user)
        List<BookingHistory> bookingHistoryList = BookingHistory.findAllByUser(user)
        println '========Booking history====' + bookingHistoryList.size()
        List<ActivityBookingHistory> activityBookingHistoryList = ActivityBookingHistory.findAllByUser(user)
        render(view: '/home/profilePage', model: [user: user])

    }

    def cancelRoomBooking = {
        print('---------- params are here for cancel room booking -----------------' + params)
        String bookingRefId = params.bookRefNo
        String bookingHistoryId = params.bookingHistoryId
        String hotelCancel = "1"

//        def client = new SOAPClient("http://api.tektravels.com/tbohotelapi_v6/hotelservice.asmx")
        def client = new SOAPClient("http://airapi.travelboutiqueonline.com/tbohotelapi_v6/hotelservice.asmx")

        def response = client.send("""<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <AuthenticationData xmlns="http://TekTravel/HotelBookingApi">
      <SiteName></SiteName>
      <AccountCode></AccountCode>
      <UserName>${grailsApplication.config.tbo.TRAVELX_UNAME}</UserName>
      <Password>${grailsApplication.config.tbo.TRAVELX_PASS_HOTEL}</Password>
    </AuthenticationData>
  </soap:Header>
  <soap:Body>
    <SendHotelChangeRequest xmlns="http://TekTravel/HotelBookingApi">
      <request>
        <BookingId>${bookingRefId}</BookingId>
        <RequestType>HotelCancel</RequestType>
        <Remarks>"abcd"</Remarks>
      </request>
    </SendHotelChangeRequest>
  </soap:Body>
</soap:Envelope>"""
        )
        print('---------- here response of request for cancel -------------------' + response.text)
        def responseText = new XmlSlurper().parseText(response.text)
        String cancelId = responseText.children().SendHotelChangeRequestResponse.SendHotelChangeRequestResult.ChangeRequestId
        String responseStatus = responseText.children().SendHotelChangeRequestResponse.SendHotelChangeRequestResult.Status.StatusCode
        print('------------------ response status code is -------------------------' + responseStatus)
        if (responseStatus.equals('01')) {
            BookingHistory bookingHistory = BookingHistory.findById(bookingHistoryId.toLong())
            bookingHistory.bookingStatus = BookingStatusType.Cancelled
            bookingHistory.bookingCancelId = cancelId.toString()
            flash.success = g.message(code: 'booking.cancel.successfully')
            redirect(controller: 'home', action: 'userHotelBookingSummary')
        } else if (responseStatus.equals('02')) {
            flash.error = g.message(code: 'booking.cancel.merchant.authentication.fail')
            redirect(controller: 'home', action: 'userHotelBookingSummary')
        } else if (responseStatus.equals('06') || responseStatus.equals('08') || responseStatus.equals('09') || responseStatus.equals('10')) {
            flash.error = g.message(code: 'booking.cancel.merchant.tbo.fail')
            redirect(controller: 'home', action: 'userHotelBookingSummary')
        }

    }

    def showVoucher = {
        print('============= params in show booking id ======================' + params)
        User user = springSecurityService.currentUser as User
        BookingHistory bookingHistory = BookingHistory.get(params.long('bookingHistoryId'))
        HotelForBooking hotelForBooking = HotelForBooking.findByBookingHistory(bookingHistory)
        List<RoomBooked> roomBookedList = RoomBooked.findAllByHotelForBooking(hotelForBooking)
        render(view: '/home/showVoucher', model: [bookingHistory: bookingHistory, roomBookedList: roomBookedList, hotelForBooking:
                hotelForBooking])
    }

    def showInvoice = {
        BookingHistory bookingHistory = BookingHistory.get(params.long('bookingHistoryId'))
        HotelForBooking hotelForBooking = HotelForBooking.findByBookingHistory(bookingHistory)
        List<RoomBooked> roomBookedList = RoomBooked.findAllByHotelForBooking(hotelForBooking)
        PaymentInfo paymentInfo = PaymentInfo.findByBookingHistory(bookingHistory)
        render(view: '/home/showInvoice', model: [paymentInfo: paymentInfo, bookingHistory: bookingHistory, hotelForBooking: hotelForBooking, roomBookedList: roomBookedList])

    }


    def sendVoucherAsEmail = {
        BookingHistory bookingHistory = BookingHistory.get(params.long('bookingHistoryId'))
        HotelForBooking hotelForBooking = HotelForBooking.findByBookingHistory(bookingHistory)
        List<RoomBooked> roomBookedList = RoomBooked.findAllByHotelForBooking(hotelForBooking)
        String body = g.render(template: '/hotelTemplates/mailHotelVoucherTemplate', model: [bookingHistory: bookingHistory, roomBookedList: roomBookedList, hotelForBooking:
                hotelForBooking])
        String receiver = params.emailTO
        String about = 'Voucher details'
//        UtilService.sendMailMethod(to, body, subject)
        try {
            mailService.sendMail {
                to receiver
                subject about
                html body
            }
        }
        catch (Exception e) {
            print('------- in send mail method------- exception occur---' + e.printStackTrace())
        }
        flash.success = g.message(code: 'default.success.emailSend.message')
        redirect(controller: 'home', action: 'showVoucher', params: [bookingHistoryId: bookingHistory?.id])
    }


    def aboutToActivateAccount = {
        log.info "---------- params are -----------" + params
        String userId = params.user
        User user = User.findById(userId.toLong())
        springSecurityService?.reauthenticate(user?.username)
        render(view: 'activateAccount', model: [user: user])
    }


    def infoServiceProvider = {
        render(view: 'registrationInformationForServiceProvider')
    }


    def profile = {
        render(view: '/home/profilePage')
    }

    def aboutUs = {
        render(view: '/footers/aboutUs')
    }

    def howItWork = {
        render(view: '/footers/howItWork')
    }

    def contactUs = {
        render(view: '/footers/contactUs')
    }

    def termsOfUse = {
        render(view: '/footers/termsOfUse')
    }

    def privacyPolicy = {
        render(view: '/footers/privacyPolicy')
    }

    def requestToChangePassword = {
        render(view: '/home/requestToChangePassword')
    }

    def updateCurrentPassword = { ResetPasswordCo resetPasswordCo ->
        User user = springSecurityService.currentUser as User
        if (resetPasswordCo.validate()) {
            if (user.password.equals(springSecurityService.encodePassword(params.currentPassword))) {
                user.password = params.newPassword
                user.save(flush: true)
                redirect(controller: 'home', action: 'userLanding')
            }
        } else {
            render(view: '/home/requestToChangePassword', model: [resetPasswordCo: resetPasswordCo])
        }

    }

    def userHotelBookingSummary = {
        User user = springSecurityService.currentUser as User
        List<BookingHistory> bookingHistoryList = BookingHistory.findAllByUser(user)
        render(view: '/home/userHotelBookingSummary', model: [bookingHistoryList: bookingHistoryList])
    }

    def addProfile = { UpdateUserCO updateUserCO ->
        print('------------ params ------------' + params)
        User user = springSecurityService.currentUser as User
        String notValid = params.notValid
        if (updateUserCO.validate()) {
            user.firstName = updateUserCO.firstName
            user.lastName = updateUserCO.lastName
            user.mobNo = updateUserCO.mobNo
            user.save(flush: true)
            flash.success = g.message(code: 'user.update.success.alert')
            redirect(controller: 'home', action: 'userLanding')
        } else {
            print('11111111111111111111111111')
            render(view: '/home/profilePage', model: [user: user, notValid: notValid, updateUserCO: updateUserCO])
        }

    }

    def userFlightBookingSummary = {
        User user = springSecurityService.currentUser as User
        List<BookedFlight> bookedFlightList = BookedFlight.findAllByUser(user)
        render(view: '/home/userFlightBookingSummary', model: [bookedFlightList: bookedFlightList])
    }

    def flightDetails = {
        print('===========params====================' + params)
        BookedFlight bookedFlight = BookedFlight.get(params.long('flightBookId'))
        BookedFlightFare bookedFlightFare = bookedFlight.bookedFlightFare
        List<BookedFlightPassenger> bookedFlightPassengerList = BookedFlightPassenger.findAllByBookedFlight(bookedFlight)
        List<BookedFlightTicket> bookedFlightTicketList = BookedFlightTicket.findAllByBookedFlight(bookedFlight)
        log.info '------------------bookedFlightPassengerList----------------' + bookedFlightPassengerList
        log.info '------------------bookedFlightTicketList----------------' + bookedFlightTicketList

        render(template: '/flightTemplate/flightInvoiceTemplate', model: [bookedFlightFare         : bookedFlightFare,
                                                                          bookedFlightPassengerList: bookedFlightPassengerList,
                                                                          bookedFlightTicketList   : bookedFlightTicketList,
                                                                          bookedFlight             : bookedFlight])


    }

    def cancelFlightBooking = {

        BookedFlight bookedFlight = BookedFlight.get(params.long('flightBookId'))
        List<BookedFlightTicket> bookedFlightTicketList = BookedFlightTicket.createCriteria().list {
            eq('bookedFlight', bookedFlight)
        }

        def client = new SOAPClient("http://api.tektravels.com/tboapi_v7/service.asmx")
        SOAPResponse getCancelResponse = client.send("""<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <AuthenticationData xmlns="http://192.168.0.170/TT/BookingAPI">
      <SiteName>string</SiteName>
      <AccountCode>string</AccountCode>
      <UserName>${grailsApplication.config.tbo.TRAVELX_UNAME}</UserName>
      <Password>${grailsApplication.config.tbo.TRAVELX_PASS_HOTEL}</Password>
    </AuthenticationData>
  </soap:Header>
  <soap:Body>
    <SendChangeRequest xmlns="http://192.168.0.170/TT/BookingAPI">
      <request>
        <BookingId>${bookedFlight?.bookingId}</BookingId>
        <RequestType>Cancellation</RequestType>
        <TicketId>
          <int>${bookedFlightTicketList?.first()?.ticketId}</int>
        </TicketId>
        <Remarks>cancel flight</Remarks>
        <IsFullBookingCancel>true</IsFullBookingCancel>
      </request>
    </SendChangeRequest>
  </soap:Body>
</soap:Envelope>"""
        )

        print '--------------------getCancelResponse---------------------------' + getCancelResponse.text





    }


}
