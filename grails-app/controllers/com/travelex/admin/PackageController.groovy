package com.travelex.admin

import com.travelex.Hotel.Hotel
import com.travelex.TboApi.CityFromTBO
import com.travelex.TboApi.CountryFromTbo
import com.travelex.admin.holiday.HolidayHotelRoomBedType
import com.travelex.admin.holiday.HolidayHotelRoomVO
import com.travelex.admin.holiday.StayDetailCO
import com.travelex.admin.holiday.ItineraryCO
import com.travelex.admin.holiday.PackageDetailCO
import com.travelex.admin.holiday.PackageHotelCO
import grails.converters.JSON
import org.springframework.web.multipart.commons.CommonsMultipartFile
import util.AppUtil
import grails.transaction.Transactional

@Transactional(readOnly = true)
class PackageController {

    def springSecurityService
    def imageService
    static allowedMethods = [save: "POST"]

    def index(Integer max) {
    }

    def show = {
    }

    def create() {
    }

    def save = {
    }

    def edit = {
    }

    def update = {
    }

    @Transactional
    def delete() {
    }

    def countryForPackage = {
        print '-------------params in cityForPackage------------------' + params
        List<String> filterName = params.list('city[]') as List<String>
        CountryFromTbo domesticCountry = CountryFromTbo.findByCountryName("India")
        String name = params.term
        List<CountryFromTbo> countryFromTbos = []
        filterName.each {
            if (it.equals('domestic')) {
                countryFromTbos = CountryFromTbo.createCriteria().list {
                    idEq(domesticCountry?.id)
                    ilike('countryName', "%" + name + "%")
                }
            } else {
                countryFromTbos = CountryFromTbo.createCriteria().list {
                    ilike('countryName', "%" + name + "%")
                    ne('id', domesticCountry?.id)
                }
            }
            render countryFromTbos?.countryName as JSON
        }
    }

    def cityForPackage = {
        print '-------------params in countryForPackage------------------' + params
        List<String> filterName = params.list('city[]') as List<String>
        CountryFromTbo country = CountryFromTbo.findByCountryName(params.countryName)
        print('-------country-------------' + country.countryName)
        String name = params.term
        List<CityFromTBO> cityFromTBOs = []
        filterName.each {
            if (it.equals('domestic')) {
                print('11111111111111111')
                cityFromTBOs = CityFromTBO.createCriteria().list {
                    eq('countryFromTbo', country)
                    ilike('name', "%" + name + "%")
                }
            } else {
                print('22222222222222')
                cityFromTBOs = CityFromTBO.createCriteria().list {
                    eq('countryFromTbo', country)
                    ilike('name', "%" + name + "%")
                }
            }
            render cityFromTBOs?.name as JSON
        }
    }


    def showPackages = {
        HolidayCategory holidayCategory = HolidayCategory.get(params.long('categoryId'))
        List<HolidayPackage> holidayPackageList = HolidayPackage.createCriteria().list {
            eq('holidayCategory', holidayCategory)
        }
        render(view: '/package/showPackages', model: [holidayPackageList: holidayPackageList])
    }

    def moreDetails = {
        print "---------params-------------------" + params
        HolidayPackage holidayPackage = HolidayPackage.get(params.long('holidayPackageId'))
        print "---------holidayPackage-------------------" + holidayPackage
        List<StayDetail> stayDetailList = StayDetail.findAllByHolidayPackage(holidayPackage)
        List<HolidayPackageFlight> packageFlightList = HolidayPackageFlight.createCriteria().list {
            eq('holidayPackage', holidayPackage)
        }
        PackageDetail packageDetail = PackageDetail.findByHolidayPackage(holidayPackage)
        List<Itinerary> itineraryList = Itinerary.createCriteria().list {
            eq('holidayPackage', holidayPackage)
        }

        List<HolidayImage> holidayImageList = HolidayImage.findAllByHolidayPackage(holidayPackage)

        print "---------packageHotelList-------------------"
        render(view: '/package/moreDetails', model: [holidayPackage: holidayPackage, stayDetailList: stayDetailList, packageFlightList:
                packageFlightList, packageDetail                   : packageDetail, itineraryList: itineraryList, holidayImageList: holidayImageList])
    }

    def summaryForBooking = {
        print '----------- package params are ----------' + params
        HolidayPackage aPackage = HolidayPackage.get(params.long('id'))
        render(view: "/package/summaryForBooking", model: [aPackage: aPackage])
    }

    def listHotel = {
        print "=============params========================" + params
        Long stayId = params.long('stayId')
        List<HolidayHotel> holidayHotelList = HolidayHotel.list()
        render(view: "/package/listHotel", model: [holidayHotelList: holidayHotelList, stayId: stayId])
    }

    def createHotel = {
        render(view: '/package/createHotel')
    }

    def saveHotelForPackage = { PackageHotelCO packageHotelCO ->
        print('------------- params --------------------' + params)
        if (packageHotelCO.validate()) {
            HolidayHotel holidayHotel = new HolidayHotel()
            PackageHotelCO.saveHotelForPackage(packageHotelCO, holidayHotel)
            holidayHotel.save(flush: true, failOnError: true)
            def webRootDir = AppUtil.staticResourcesDirPath
            CommonsMultipartFile uploadedFile = packageHotelCO.mainImageName
            if (uploadedFile?.bytes) {
                def packageImagesDir = new File(webRootDir, "/packageHotelImages/${holidayHotel.id}")
                if (packageImagesDir.exists()) {
                    packageImagesDir.deleteDir()
                    packageImagesDir.mkdirs()
                } else {
                    packageImagesDir.mkdirs()
                }
                holidayHotel.imageName = uploadedFile.getOriginalFilename().trim()
                holidayHotel.imageType = uploadedFile.contentType
                holidayHotel.save(flush: true, failOnError: true)
                File file = new File("${webRootDir}/packageHotelImages/${holidayHotel.id}/${holidayHotel.imageName}")
                imageService.storeImageInFileSystem(file, uploadedFile.bytes)
            }
            redirect(controller: 'package', action: 'showHotel', params: [holidayHotelId: holidayHotel?.id])
        } else {
            render(view: '/package/createHotel', model: [packageHotelCO: packageHotelCO])
        }
    }

    def showHotel = {
        HolidayHotel holidayHotel = HolidayHotel.get(params.long('holidayHotelId'))
        List<HolidayHotelRoom> holidayHotelRoomList = HolidayHotelRoom.createCriteria().list {
            eq('holidayHotel', holidayHotel)
        }
        render(view: '/package/showHotel', model: [holidayHotel: holidayHotel, holidayHotelRoomList: holidayHotelRoomList])

    }

    def showHolidayHotelImage = {
        HolidayHotel holidayHotel = HolidayHotel.get(params.long('holidayHotelId'))
        print('-----------------------' + holidayHotel)
        def webRootDir = AppUtil.staticResourcesDirPath
        def avatarFilePath
        try {
            avatarFilePath = new File(webRootDir, "/packageHotelImages/${holidayHotel?.id}/${holidayHotel.imageName}");
            if (avatarFilePath) {
                response.setContentType(holidayHotel.imageName)
                response.setContentLength(avatarFilePath?.size()?.toInteger())
                OutputStream out = response.getOutputStream();
                out.write(avatarFilePath.bytes);
                out.close();
            }
        } catch (Exception e) {
            e.printStackTrace()
        }
    }

    def editHotel = {
        print('-----------params--------------------' + params)
        HolidayHotel holidayHotel = HolidayHotel.get(params.long('holidayHotelId'))
        render(view: '/package/editHotel', model: [holidayHotel: holidayHotel])
    }

    def updateHotelForPackage = { PackageHotelCO packageHotelCO ->
        print('-----------params--------------------' + params)
        HolidayHotel holidayHotel = HolidayHotel.get(params.long('holidayHotelId'))
        if (packageHotelCO.validate()) {
            PackageHotelCO.updateHotelForPackage(packageHotelCO, holidayHotel)
            holidayHotel.save(flush: true, failOnError: true)
            def webRootDir = AppUtil.staticResourcesDirPath
            CommonsMultipartFile uploadedFile = packageHotelCO.mainImageName
            if (uploadedFile?.bytes) {
                def packageImagesDir = new File(webRootDir, "/packageHotelImages/${holidayHotel.id}")
                if (packageImagesDir.exists()) {
                    packageImagesDir.deleteDir()
                    packageImagesDir.mkdirs()
                } else {
                    packageImagesDir.mkdirs()
                }
                holidayHotel.imageName = uploadedFile.originalFilename.trim()
                holidayHotel.imageType = uploadedFile.contentType
                holidayHotel.save(flush: true, failOnError: true)
                File file = new File("${webRootDir}/packageHotelImages/${holidayHotel.id}/${holidayHotel.imageName}")
                imageService.storeImageInFileSystem(file, uploadedFile.bytes)
            }
            redirect(controller: 'package', action: 'showHotel', params: [holidayHotelId: holidayHotel?.id])
        } else {
            render(view: '/package/editHotel', model: [holidayHotel: holidayHotel, packageHotelCO: packageHotelCO])
        }
    }

    def deleteHotel = {
        print '------------------params in delete hotel---------------' + params
        HolidayHotel holidayHotel = HolidayHotel.get(params.long('holidayHotelId'))
        print '------------------hotel in delete hotel---------------' + holidayHotel

        List<HolidayHotelStayDetail> holidayHotelStayDetailList = HolidayHotelStayDetail.createCriteria().list {
            eq('holidayHotel', holidayHotel)
        }
        print '------------------holidayHotelStayDetailList---------------' + holidayHotelStayDetailList

        holidayHotelStayDetailList.each {
            it.stayDetail = null
            it.holidayHotel = null
            it.delete(flush: true)
        }
        holidayHotel.delete(flush: true)
        flash.success = g.message(code: "hotel.delete.success.message")
        redirect(controller: 'package', action: 'listHotel')
    }

    def listFlight = {
        Long holidayPackageId = params.long('holidayPackageId')
        List<Flight> flightList = Flight.list()
        render(view: '/package/listFlight', model: [flightList: flightList, holidayPackageId: holidayPackageId])
    }


    def createFlight = {
        print('----------- add flight -----------------------' + params)
        render(view: '/package/createFlight')
    }

    def saveFlightForPackage = { FlightCO flightCO ->
        print('----------- params params -----------------------' + params)
        if (flightCO.validate()) {
            Flight flight = new Flight()
            FlightCO.saveFlightForPackage(flightCO, flight)
            redirect(controller: 'package', action: 'showFlight', params: [flightId: flight?.id])
        } else {
            render(view: '/package/createFlight', model: [flightCO: flightCO])
        }
    }

    def showFlight = {
        Flight flight = Flight.get(params.long('flightId'))
        render(view: '/package/showFlight', model: [flight: flight])

    }


    def editFlight = {
        print('-----------params--------------------' + params)
        Flight flight = Flight.get(params.long('flightId'))
        render(view: '/package/editFlight', model: [flight: flight])
    }

    def updateFlightForPackage = { FlightCO flightCO ->
        Flight flight = Flight.get(params.long('flightId'))
        if (flightCO.validate()) {
            FlightCO.updateFlightForPackage(flightCO, flight)
            flight.save(flush: true, failOnError: true)
            redirect(controller: 'package', action: 'showFlight', params: [flightId: flight?.id])
        } else {
            render(view: '/package/editFlight', model: [flight: flight, flightCO: flightCO])
        }
    }

    def deleteFlight = {
        Flight flight = Flight.get(params.long('flightId'))
//        PackageFlight packageFlight = PackageFlight.findByFlight(flight)
//        packageFlight.flight = null
        flight.delete(flush: true)
        flash.success = g.message(code: "flight.delete.success.message")
        redirect(controller: "package", action: 'listFlight')
    }


    def deleteFlightForPackage = {
        HolidayPackage holidayPackage = HolidayPackage.get(params.long('packageId'))
        Flight flight = Flight.get(params.long('flightId'))
        HolidayPackageFlight packageFlight = HolidayPackageFlight.findByHolidayPackageAndFlight(holidayPackage, flight)
        packageFlight.holidayPackage = null
        packageFlight.flight = null
        packageFlight.delete(flush: true)
        flight.delete(flush: true)
        redirect(controller: 'package', action: 'show', params: [id: holidayPackage?.id])
    }

    def addPackageDetail = {
        HolidayPackage holidayPackage = HolidayPackage.get(params.long('holidayPackageId'))
        render(view: '/package/addDetail', model: [holidayPackage: holidayPackage])
    }


    def saveDetailForPackage = { PackageDetailCO packageDetailCO ->
        print('----------- params params -----------------------' + params)
        List<CommonsMultipartFile> commonsMultipartFiles = request.getFiles('holidayImages')
        print('-----------------commonsMultipartFiles-------------------' + commonsMultipartFiles.size())
        HolidayPackage holidayPackage = HolidayPackage.get(params.long('holidayPackageId'))
        if (packageDetailCO.validate()) {
            PackageDetail packageDetail = new PackageDetail()
            PackageDetailCO.savePackageDetail(packageDetailCO, packageDetail)
            if (commonsMultipartFiles) {
                def webRootDir = AppUtil.staticResourcesDirPath
                print('------------webRootDir------------------------' + webRootDir)
                CommonsMultipartFile commonsMultipartFile
                commonsMultipartFiles.each {
                    commonsMultipartFile = it
                    if (commonsMultipartFile.bytes) {
                        HolidayImage holidayImage = new HolidayImage()
                        print('------------it.originalFile Name------------------------' + it)
                        holidayImage.imageName = commonsMultipartFile.originalFilename.trim()
                        holidayImage.imageType = commonsMultipartFile.contentType
                        holidayImage.holidayPackage = holidayPackage
                        holidayImage.save(flush: true)
                        holidayPackage?.addToHolidayImages(holidayImage)
                        def packageImagesDir = new File(webRootDir, "holiday/holidayPackageDetail/${holidayImage.id}")
                        if (packageImagesDir.exists()) {
                            packageImagesDir.deleteDir()
                            packageImagesDir.mkdirs()
                        } else {
                            packageImagesDir.mkdirs()
                        }
                        File file = new File("${webRootDir}/holiday/holidayPackageDetail/${holidayImage.id}/${holidayImage.imageName}")
                        imageService.storeImageInFileSystem(file, commonsMultipartFile.bytes)
                    }
                }
            }
            packageDetail.holidayPackage = holidayPackage
            packageDetail.save(flush: true, failOnError: true)
            redirect(controller: 'holiday', action: 'show', params: [holidayPackageId: holidayPackage?.id])
        } else {
            render(view: '/package/addDetail', model: [packageDetailCO: packageDetailCO, holidayPackage: holidayPackage])
        }
    }


    def showHolidayImage = {
        HolidayImage holidayImage = HolidayImage.get(params.long('holidayImageId'))
        print('------------in show holidayImagfe-----------' + holidayImage)
        def webRootDir = AppUtil.staticResourcesDirPath
        def avatarFilePath
        try {
            avatarFilePath = new File(webRootDir, "/holiday/holidayPackageDetail/${holidayImage?.id}/${holidayImage.imageName}");
            if (avatarFilePath) {
                response.setContentType(holidayImage.imageName)
                response.setContentLength(avatarFilePath?.size()?.toInteger())
                OutputStream out = response.getOutputStream();
                out.write(avatarFilePath.bytes);
                out.close();
            }
        } catch (Exception e) {
            e.printStackTrace()
        }
    }


    def deleteHolidayImage = {
        print('-----------------deleteHolidayImage------------------' + params)
        HolidayPackage holidayPackage = HolidayPackage.get(params.long('holidayPackageId'))
        HolidayImage holidayImage = HolidayImage.get(params.long('holidayImageId'))
        HolidayImage holidayImageObj = HolidayImage.createCriteria().get {
            eq('holidayPackage', holidayPackage)
            idEq(holidayImage?.id)
        }
        holidayPackage.removeFromHolidayImages(holidayImageObj)
        def webAppDir = AppUtil.staticResourcesDirPath
        print "-----------webAppDir---------------------" + webAppDir
        File imageDir = new File("${webAppDir}/holiday/holidayPackageDetail/${holidayImageObj?.id}/${holidayImageObj.imageName}");
        print "-----------imageDir---------------------" + imageDir
        if (imageDir.exists()) {
            print("------------------imageDir.exist()------------------------")
            imageDir.deleteDir()
        }
        holidayImage.delete(flush: true)
        redirect(controller: 'holiday', action: 'show', params: [holidayPackageId: holidayPackage?.id])
    }


    def editDetail = {
        print('----------------params-------------------' + params)
        HolidayPackage holidayPackage = HolidayPackage.get(params.long('holidayPackageId'))
        PackageDetail packageDetail = PackageDetail.get(params.long('detailId'))
        HolidayImage holidayImage = HolidayImage.get(params.long('holidayImageId'))

        render(view: '/package/editDetail', model: [holidayPackage: holidayPackage, packageDetail: packageDetail, holidayImageId: holidayImage?.id])
    }

    def updateDetailForPackage = { PackageDetailCO packageDetailCO ->
        print('----------params-------------------' + params)
        HolidayPackage holidayPackage = HolidayPackage.get(params.long('holidayPackageId'))
        PackageDetail packageDetail = PackageDetail.get(params.long('detailId'))
        HolidayImage holidayImage = HolidayImage.get(params.long('holidayImageId'))
        print('-----------------holidayImage--------------------' + holidayImage)
        CommonsMultipartFile commonsMultipartFile = params.holidayImages
        if (packageDetailCO.validate()) {
            PackageDetailCO.updatePackageDetail(packageDetailCO, packageDetail)
            packageDetail.save(flush: true, failOnError: true)
            def webRootDir = AppUtil.staticResourcesDirPath
            if (commonsMultipartFile.bytes) {
                print('11111111111111111111111111')
                if (!holidayImage) {
                    holidayImage = new HolidayImage()
                    print('------------holiday Image does not exist-new created------------------' + holidayImage)
                }
                holidayImage.imageName = commonsMultipartFile.originalFilename.trim()
                holidayImage.imageType = commonsMultipartFile.contentType
                holidayImage.holidayPackage = holidayPackage
                holidayImage.save(flush: true, failOnError: true)
                holidayPackage.addToHolidayImages(holidayImage)
                def packageImagesDir = new File(webRootDir, "holiday/holidayPackageDetail/${holidayImage.id}")
                if (packageImagesDir.exists()) {
                    packageImagesDir.deleteDir()
                    packageImagesDir.mkdirs()
                } else {
                    packageImagesDir.mkdirs()
                }
                File file = new File("${webRootDir}/holiday/holidayPackageDetail/${holidayImage.id}/${holidayImage.imageName}")
                imageService.storeImageInFileSystem(file, commonsMultipartFile.bytes)


            }
            redirect(controller: 'holiday', action: 'show', params: [holidayPackageId: holidayPackage?.id])
        } else {
            render(view: '/package/editDetail', model: [holidayPackage: holidayPackage, packageDetail: packageDetail, packageDetailCO: packageDetailCO])
        }
    }

    def deleteDetail = {
        HolidayPackage holidayPackage = HolidayPackage.get(params.long('holidayPackageId'))
        PackageDetail packageDetail = PackageDetail.findByIdAndHolidayPackage(params.long('detailId'), holidayPackage)
        packageDetail.holidayPackage = null
        holidayPackage.packageDetail = null
        packageDetail.delete(flush: true)
        redirect(controller: 'holiday', action: 'show', params: [holidayPackageId: holidayPackage?.id])
    }


    def addPackageItinerary = {
        HolidayPackage holidayPackage = HolidayPackage.get(params.long('holidayPackageId'))
        def tempNight = StayDetail.createCriteria().get {
            projections {
                sum('noOfNight')
            }
            eq('holidayPackage', holidayPackage)
        }

        print '=================no of night===================' + tempNight

        render(view: '/package/addItinerary', model: [holidayPackageId: holidayPackage?.id, tempNight: tempNight])
    }

    def saveItineraryForPackage = { ItineraryCO itineraryCO ->
        HolidayPackage holidayPackage = HolidayPackage.get(params.long('holidayPackageId'))
        def tempNight = StayDetail.createCriteria().get {
            projections {
                sum('noOfNight')
            }
            eq('holidayPackage', holidayPackage)
        }
        if (itineraryCO.validate()) {
            String dayNum = itineraryCO.dayNumber
            Itinerary itineraryFound = Itinerary.findByDayNumberAndHolidayPackage(dayNum, holidayPackage)
            if (!itineraryFound) {
                Itinerary itinerary = new Itinerary()
                ItineraryCO.saveItineraryForPackage(itineraryCO, itinerary)
                itinerary.holidayPackage = holidayPackage
                itinerary.save(flush: true, failOnError: true)
                holidayPackage.addToItinerary(itinerary)
                redirect(controller: 'holiday', action: 'show', params: [holidayPackageId: holidayPackage?.id])
            } else {
                flash.error = g.message(code: 'itinerary.available.alert')
                render(view: '/package/addItinerary', model: [holidayPackageId: holidayPackage?.id, itineraryCO: itineraryCO, tempNight: tempNight])
            }
        } else {
            render(view: '/package/addItinerary', model: [holidayPackageId: holidayPackage?.id, itineraryCO: itineraryCO, tempNight: tempNight])
        }
    }

    def editItinerary = {
        HolidayPackage holidayPackage = HolidayPackage.get(params.long('holidayPackageId'))
        Itinerary itinerary = Itinerary.get(params.long('itineraryId'))
        def tempNight = StayDetail.createCriteria().get {
            projections {
                sum('noOfNight')
            }
            eq('holidayPackage', holidayPackage)
        }
        render(view: '/package/editItinerary', model: [holidayPackage: holidayPackage, itinerary: itinerary, tempNight: tempNight])
    }

    def updateItineraryForPackage = { ItineraryCO itineraryCO ->
        HolidayPackage holidayPackage = HolidayPackage.get(params.long('holidayPackageId'))
        def tempNight = StayDetail.createCriteria().get {
            projections {
                sum('noOfNight')
            }
            eq('holidayPackage', holidayPackage)
        }
        Itinerary itinerary = Itinerary.get(params.long('itineraryId'))
        if (itineraryCO.validate()) {
            ItineraryCO.updateItineraryForPackage(itineraryCO, itinerary)
            itinerary.save(flush: true, failOnError: true)
            redirect(controller: 'holiday', action: 'show', params: [holidayPackageId: holidayPackage?.id])
        } else {
            render(view: '/package/editItinerary', model: [holidayPackage: holidayPackage, itinerary: itinerary, itineraryCO: itineraryCO, tempNight: tempNight])
        }
    }

    def deleteItinerary = {
        HolidayPackage holidayPackage = HolidayPackage.get(params.long('holidayPackageId'))
        Itinerary itinerary = Itinerary.findByIdAndHolidayPackage(params.long('itineraryId'), holidayPackage)
        itinerary.holidayPackage = null
        holidayPackage.removeFromItinerary(itinerary)
        itinerary.delete(flush: true)
        redirect(controller: 'holiday', action: 'show', params: [holidayPackageId: holidayPackage?.id])
    }


    def createStay = {
        Long holidayPackageId = params.long('holidayPackageId')
        render(view: '/package/createStay', model: [holidayPackageId: holidayPackageId])
    }

    def saveStayForPackage = { StayDetailCO stayDetailCO ->
        print('-----------params ----------------' + params)
        HolidayPackage holidayPackage = HolidayPackage.get(params.long('holidayPackageId'))
        if (stayDetailCO.validate()) {
            StayDetail stayDetail = new StayDetail()
            StayDetailCO.saveStayForPackage(stayDetailCO, stayDetail)
            stayDetail.holidayPackage = holidayPackage
            stayDetail.save(flush: true, failOnError: true)
            holidayPackage.addToStayDetails(stayDetail)
            flash.success = g.message(code: 'stayDetail.successAdded.alert')
            redirect(controller: 'holiday', action: 'show', params: [holidayPackageId: holidayPackage?.id])
        } else {
            render(view: '/package/createStay', model: [stayDetailCO: stayDetailCO, holidayPackageId: holidayPackage?.id])
        }
    }

    def editStayForPackage = {
        StayDetail stayDetail = StayDetail.get(params.long('stayId'))
        Long holidayPackageId = params.long('holidayPackageId')
        render(view: '/package/editStayForPackage', model: [stayDetail: stayDetail, holidayPackageId: holidayPackageId])
    }

    def updateStayForPackage = { StayDetailCO stayDetailCO ->
        StayDetail stayDetail = StayDetail.get(params.long('stayId'))
        HolidayPackage holidayPackage = HolidayPackage.get(params.long('holidayPackageId'))
        if (stayDetailCO.validate()) {
            StayDetailCO.updateStayForPackage(stayDetailCO, stayDetail)
            stayDetail.save(flush: true, failOnError: true)
            stayDetail.holidayPackage = holidayPackage
            redirect(controller: 'holiday', action: 'show', params: [holidayPackageId: holidayPackage?.id])
        } else {
            render(view: '/package/editStayForPackage', model: [stayDetailCO: stayDetailCO, stayDetail: stayDetail, holidayPackageId: holidayPackage?.id])
        }
    }

    def deleteStayForPackage = {
        StayDetail stayDetail = StayDetail.get(params.long("stayId"))
        HolidayPackage holidayPackage = HolidayPackage.get(params.long('holidayPackageId'))
        stayDetail.holidayPackage = null
        holidayPackage.removeFromStayDetails(stayDetail)
        stayDetail.delete(flush: true)
        redirect(controller: 'holiday', action: 'show', params: [holidayPackageId: holidayPackage?.id])
    }

    def addHotelToPackage = {
        print("------------------params------------------------" + params)
        HolidayHotelStayDetail holidayHotelStayDetail = new HolidayHotelStayDetail()
        StayDetail stayDetail = StayDetail.get(params.long('stayId'))
        HolidayHotel holidayHotel = HolidayHotel.get(params.long('holidayHotelId'))
        holidayHotelStayDetail.stayDetail = stayDetail
        holidayHotelStayDetail.holidayHotel = holidayHotel
        holidayHotelStayDetail.save(flush: true, failOnError: true)
        flash.success = g.message(code: "packageHotel.created.success")
        redirect(controller: 'holiday', action: 'index')
    }

    def addFlightToPackage = {
        Flight flight = Flight.get(params.long('flightId'))
        HolidayPackage holidayPackage = HolidayPackage.get(params.long('holidayPackageId'))
        HolidayPackageFlight packageFlight = new HolidayPackageFlight()
        packageFlight.flight = flight
        packageFlight.holidayPackage = holidayPackage
        packageFlight.save(flush: true, failOnError: true)
        flash.success = g.message(code: "packageFlight.created.success")
        redirect(controller: 'holiday', action: 'index')
    }

    def removeFlightFromPackage = {
        HolidayPackageFlight holidayPackageFlight = HolidayPackageFlight.get(params.long('holidayPackageFlightId'))
        holidayPackageFlight.delete(flush: true)
        flash.success = g.message(code: 'holidayPackageFlight.flight.remove.success')
        redirect(controller: 'holiday', action: 'show', params: [holidayPackageId: params.holidayPackageId])
    }


    def createRoom = {
        print("---------------params are---------------------------" + params)
        render(view: '/package/createRoom', model: [holidayHotelId: params.holidayHotelId])
    }

    def saveRoom = { HolidayHotelRoomVO holidayHotelRoomVO ->
        print('--------------------params------------------------' + params)
        HolidayHotel holidayHotel = HolidayHotel.get(params.long('holidayHotelId'))
        if (holidayHotelRoomVO.validate()) {
            HolidayHotelRoom holidayHotelRoom
            HolidayHotelRoomBedType bedType = holidayHotelRoomVO.bedType as HolidayHotelRoomBedType
            print('-------------bedTYpe=================================' + bedType)
            holidayHotelRoom = HolidayHotelRoom.createCriteria().get {
                eq('holidayHotel', holidayHotel)
                eq('roomCategory', holidayHotelRoomVO?.roomCategory)
                eq('holidayHotelRoomBedType', bedType)
            }
            if (!holidayHotelRoom) {
                holidayHotelRoom = new HolidayHotelRoom()
                HolidayHotelRoomVO.saveRoomForHolidayHotel(holidayHotelRoomVO, holidayHotelRoom)
                holidayHotelRoom.holidayHotel = holidayHotel
                holidayHotelRoom.save(flush: true, failOnError: true)
                holidayHotel.addToHolidayHotelRoom(holidayHotelRoom)
            } else {
                flash.error = g.message(code: 'hotelRoom.already.exist.error')
                render(view: '/package/createRoom', model: [holidayHotelId: holidayHotel?.id])
            }
            redirect(controller: 'package', action: 'showHotel', params: [holidayHotelId: holidayHotel?.id])
        } else {
            render(view: '/package/createRoom', model: [holidayHotelRoomVO: holidayHotelRoomVO, holidayHotelId: holidayHotel?.id])
        }

    }

    def editRoom = {
        HolidayHotelRoom holidayHotelRoom = HolidayHotelRoom.get(params.long('roomId'))
        render(view: '/package/editRoom', model: [holidayHotelId: holidayHotelRoom?.holidayHotel?.id, holidayHotelRoom: holidayHotelRoom])

    }

    def updateRoom = { HolidayHotelRoomVO holidayHotelRoomVO ->
        HolidayHotelRoom holidayHotelRoom = HolidayHotelRoom.get(params.long('roomId'))
        if (holidayHotelRoomVO.validate()) {
            HolidayHotelRoomBedType bedType = holidayHotelRoomVO.bedType as HolidayHotelRoomBedType
            print('-------------bedTYpe=================================' + bedType)
            HolidayHotelRoom holidayHotelRoomFound = HolidayHotelRoom.createCriteria().get {
                ne('id', holidayHotelRoom?.id)
                eq('roomCategory', holidayHotelRoomVO?.roomCategory)
                eq('holidayHotelRoomBedType', bedType)
            }
            print('-------------------holidayHotelRoomFound------------------------' + holidayHotelRoomFound)
            if (!holidayHotelRoomFound) {
                HolidayHotelRoomVO.updateRoomForHolidayHotel(holidayHotelRoomVO, holidayHotelRoom)
                holidayHotelRoom.save(flush: true, failOnError: true)
            } else {
                flash.error = g.message(code: 'hotelRoom.already.exist.error')
                render(view: '/package/editRoom', model: [holidayHotelId: holidayHotelRoom?.holidayHotel?.id])
            }
            redirect(controller: 'package', action: 'showHotel', params: [holidayHotelId: holidayHotelRoom?.holidayHotel?.id])
        } else {
            render(view: '/package/editRoom', model: [holidayHotelRoomVO: holidayHotelRoomVO, holidayHotelId: holidayHotelRoom?.holidayHotel?.id, holidayHotelRoom: holidayHotelRoom])
        }
    }

    def deleteRoom = {
        print("------------------------params are----------------------------" + params)
        HolidayHotelRoom holidayHotelRoom = HolidayHotelRoom.get(params.long('roomId'))
        HolidayHotel holidayHotel = holidayHotelRoom.holidayHotel
        print("------------------------holidayHotelRoom are----------------------------" + holidayHotelRoom)
        holidayHotelRoom.delete(flush: true)
        holidayHotelRoom?.holidayHotel?.removeFromHolidayHotelRoom(holidayHotelRoom)
        redirect(controller: 'package', action: 'showHotel', params: [holidayHotelId: holidayHotel?.id])

    }


}
