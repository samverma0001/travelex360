package com.travelex.admin

import com.travelex.admin.margin.DomHotelMarginVO

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

//@Transactional(readOnly = true)
class SetMarginController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        List<SetMargin> setMarginList = SetMargin.list()
        render(view: '/setMargin/index', model: [setMarginList: setMarginList])
    }

    def show(SetMargin setMarginInstance) {
        print('============================' + params)
        SetMargin setMargin = SetMargin.get(params.long('setMarginInstance'))
        print('======setMagin======================' + setMargin)

        render(view: '/setMargin/show', model: [setMargin: setMargin])
    }

    def create() {
        respond new SetMargin(params)
    }

    @Transactional
    def save(SetMargin setMarginInstance) {
        print('----------------------------' + params)
        DomHotelMarginVO domHotelMarginVO = new DomHotelMarginVO()
        bindData(domHotelMarginVO, params)
        if (domHotelMarginVO.validate()) {
            SetMargin setMargin = new SetMargin()
            DomHotelMarginVO.saveDomHotelMargin(setMargin, domHotelMarginVO)
            if (setMargin.save(flush: true)) {
                redirect(action: 'show', params: [setMarginInstance: setMargin?.id])
            } else {
                render(view: '/setMargin/create', model: [domHotelMarginVO: domHotelMarginVO])
            }
        } else {
            render(view: '/setMargin/create', model: [domHotelMarginVO: domHotelMarginVO])
        }


    }

    def edit(SetMargin setMarginInstance) {
        respond setMarginInstance
    }

    def update = { DomHotelMarginVO domHotelMarginVO ->
        print('-------------------------' + params)
        SetMargin setMargin = SetMargin.get(params.long('setMarginId'))
        if (domHotelMarginVO.validate()) {
            print('111111111111111111111111111111' + params)
            DomHotelMarginVO.updateDomHotelMargin(setMargin, domHotelMarginVO)
            if (setMargin.save(flush: true)) {
                print('22222222222222222222222' + params)
                redirect(action: 'show', params: [setMarginInstance: setMargin?.id])
            } else {
                print('3333333333333333333333333333' + params)
                render(view: '/setMargin/edit', model: [setMarginInstance: setMargin])
            }
        } else {
            print('444444444444444444444444444444444' + params)
            render(view: '/setMargin/edit', model: [domHotelMarginVO: domHotelMarginVO, setMarginInstance: setMargin])
        }
    }

    @Transactional
    def delete(SetMargin setMarginInstance) {

        if (setMarginInstance == null) {
            notFound()
            return
        }

        setMarginInstance.delete flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'SetMargin.label', default: 'SetMargin'), setMarginInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'setMarginInstance.label', default: 'SetMargin'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
