package com.travelex.admin

import com.travelex.admin.holiday.HolidayCategoryCO
import com.travelex.admin.holiday.HolidayPackageCO
import com.travelex.admin.holiday.HolidayStatus
import grails.plugin.mail.MailService
import org.springframework.web.multipart.commons.CommonsMultipartFile
import util.AppUtil

class HolidayController {

    def imageService
    MailService mailService

    def index() {
        List<HolidayPackage> holidayPackageList = HolidayPackage.findAllByHolidayStatus(HolidayStatus.ACTIVE)
        print('-----------holidayList--------------' + holidayPackageList)
        render(view: '/holiday/list', model: [holidayPackageList: holidayPackageList])
    }

    def create() {
        List<HolidayCategory> holidayCategoryList = HolidayCategory.list()
        render(view: '/holiday/create', model: [holidayCategoryList: holidayCategoryList])
    }

    def save = { HolidayPackageCO holidayPackageCO ->
        print('--------------params--------------------' + params)
        HolidayCategory holidayCategory = HolidayCategory.get(params.long('categoryId'))
        List<HolidayCategory> holidayCategoryList = HolidayCategory.list()
        if (holidayPackageCO.validate()) {
            String holidayTempName = params.holidayName
            HolidayPackage holidayFound = HolidayPackage.findByHolidayCategoryAndHolidayName(holidayCategory, holidayTempName)
            if (!holidayFound) {
                HolidayPackage holidayPackage = new HolidayPackage()
                HolidayPackageCO.saveHolidayPackage(holidayPackageCO, holidayPackage)
                holidayPackage.holidayCategory = holidayCategory
                holidayPackage.save(flush: true, failOnError: true)
                String webRootDir = AppUtil.staticResourcesDirPath
                CommonsMultipartFile commonsMultipartFile = params.holidayImageName
                if (commonsMultipartFile.bytes) {
                    String fileName = commonsMultipartFile?.originalFilename?.trim()
                    File f = new File("${webRootDir}/holidayImages/${holidayPackage?.id}")
                    if (f.exists()) {
                        f.deleteDir()
                        f.mkdirs()
                        print "----------------directory can write -----------------------------" + f.canWrite()
                    } else {
                        f.mkdirs()
                        print "----------------directory can write -----------------------------" + f.canWrite()
                    }
                    File file = new File("${webRootDir}/holidayImages/${holidayPackage?.id}/${fileName}")
                    file.createNewFile()
                    holidayPackage.holidayImageName = fileName
                    holidayPackage.holidayImageType = commonsMultipartFile.contentType
                    imageService.storeImageInFileSystem(file, commonsMultipartFile.bytes)
                    redirect(controller: 'holiday', action: 'show', params: [holidayPackageId: holidayPackage?.id])
                } else {
                    redirect(controller: 'holiday', action: 'show', params: [holidayPackageId: holidayPackage?.id])
                }
            } else {
                flash.error = g.message(code: 'holiday.exist.alert')
                render(view: '/holiday/create', model: [holidayPackageCO: holidayPackageCO, holidayCategoryList:
                        holidayCategoryList])
            }

        } else {
            render(view: '/holiday/create', model: [holidayPackageCO: holidayPackageCO, holidayCategoryList:
                    holidayCategoryList])
        }
    }

    def showHolidayPackageImage = {
        HolidayPackage holidayPackage = HolidayPackage.get(params.long('holidayPackageId'))
        print('-----------------------' + holidayPackage)
        def webRootDir = AppUtil.staticResourcesDirPath
        def avatarFilePath
        try {
            avatarFilePath = new File(webRootDir, "/holidayImages/${holidayPackage?.id}/${holidayPackage.holidayImageName}");
            if (avatarFilePath) {
                response.setContentType(holidayPackage.holidayImageName)
                response.setContentLength(avatarFilePath?.size()?.toInteger())
                OutputStream out = response.getOutputStream();
                out.write(avatarFilePath.bytes);
                out.close();
            }
        } catch (Exception e) {
            e.printStackTrace()
        }
    }


    def show = {
        HolidayPackage holidayPackage = HolidayPackage.get(params.long('holidayPackageId'))
        PackageDetail packageDetail = PackageDetail.findByHolidayPackage(holidayPackage)
        List<StayDetail> stayDetails = StayDetail.findAllByHolidayPackage(holidayPackage)
        List<Itinerary> itineraryList = Itinerary.createCriteria().list {
            eq('holidayPackage', holidayPackage)
        }
        List<HolidayImage> holidayImageList = HolidayImage.createCriteria().list {
            eq('holidayPackage', holidayPackage)
        }
        List<HolidayPackageFlight> holidayPackageFlightList = HolidayPackageFlight.createCriteria().list {
            eq('holidayPackage', holidayPackage)
        }
        render(view: '/holiday/show', model: [holidayPackage          : holidayPackage, packageDetail: packageDetail, stayDetails:
                stayDetails, itineraryList                            : itineraryList, holidayImageList: holidayImageList,
                                              holidayPackageFlightList: holidayPackageFlightList])
    }

    def edit = {
        HolidayPackage holidayPackage = HolidayPackage.get(params.long('holidayPackageId'))
        List<HolidayCategory> holidayCategoryList = HolidayCategory.list()
        render(view: '/holiday/edit', model: [holidayPackage: holidayPackage, holidayCategoryList: holidayCategoryList])
    }

    def update = { HolidayPackageCO holidayPackageCO ->
        print "----------------------update params--------" + params
        HolidayPackage holidayPackage = HolidayPackage.get(params.long('holidayPackageId'))
        HolidayCategory holidayCategory = HolidayCategory.get(params.long('categoryId'))
        List<HolidayCategory> holidayCategoryList = HolidayCategory.list()
        if (holidayPackageCO.validate()) {
            HolidayPackageCO.updateHolidayPackage(holidayPackageCO, holidayPackage)
            holidayPackage.holidayCategory = holidayCategory
            String webRootDir = AppUtil.staticResourcesDirPath
            CommonsMultipartFile commonsMultipartFile = params.holidayImageName
            if (commonsMultipartFile.bytes) {
                String fileName = commonsMultipartFile?.originalFilename?.trim()
                File f = new File("${webRootDir}/holidayImages/${holidayPackage?.id}")
                if (f.exists()) {
                    f.deleteDir()
                    f.mkdirs()
                    print "----------------directory can write -----------------------------" + f.canWrite()
                } else {
                    f.mkdirs()
                    print "----------------directory can write ----in else-------------------------" + f.canWrite()
                }
                File file = new File("${webRootDir}/holidayImages/${holidayPackage?.id}/${fileName}")
                holidayPackage.holidayImageName = fileName
                holidayPackage.holidayImageType = commonsMultipartFile.contentType
                file.createNewFile()
                imageService.storeImageInFileSystem(file, commonsMultipartFile.bytes)
                redirect(controller: 'holiday', action: 'index', params: [categoryId: holidayPackage?.holidayCategory?.id])
            } else {
                redirect(controller: 'holiday', action: 'index', params: [categoryId: holidayPackage?.holidayCategory?.id])
            }
        } else {
            render(view: '/holiday/edit', model: [holidayPackage: holidayPackage, holidayPackageCO: holidayPackageCO, holidayCategoryList: holidayCategoryList])
        }
    }

    def delete = {
        HolidayPackage holidayPackage = HolidayPackage.get(params.long('holidayPackageId'))
        List<HolidayPackageFlight> holidayPackageFlightList = HolidayPackageFlight.createCriteria().list {
            eq('holidayPackage', holidayPackage)
        }
        print('-------------holidayPackageFlightList--------------------' + holidayPackageFlightList)
        holidayPackageFlightList.each {
            print('-------------it--------------------' + it)
            it.holidayPackage = null
            it.flight = null
            it.delete(flush: true)
            print('-------------it--------------------' + it)
        }
        List<StayDetail> stayDetailList = StayDetail.createCriteria().list {
            eq('holidayPackage', holidayPackage)
        }
        print('-------------stayDetailList--------------------' + stayDetailList)
        stayDetailList.each {
            StayDetail stayDetail = it
            List<HolidayHotelStayDetail> holidayHotelStayDetailList = HolidayHotelStayDetail.createCriteria().list {
                eq('stayDetail', stayDetail)
            }
            holidayHotelStayDetailList.each {
                it.stayDetail = null
                it.holidayHotel = null
                it.delete(flush: true)
            }
        }
        holidayPackage.delete(flush: true)
        flash.success = g.message(code: 'holidayPackage.delete.success')
        redirect(controller: 'holiday', action: 'index')
    }


    def listCategory = {
        List<HolidayCategory> holidayCategories = HolidayCategory.list()
        render(view: '/holiday/listCategory', model: [holidayCategories: holidayCategories])
    }

    def createCategory = {
        render(view: '/holiday/createCategory')
    }

    def saveCategory = { HolidayCategoryCO holidayCategoryCO ->
        print('-----------params------------------------' + params)
        String categoryName = params.categoryName
        HolidayCategory holidayCategory
        if (holidayCategoryCO.validate()) {
            holidayCategory = HolidayCategory.findByCategoryName(categoryName)
            if (!holidayCategory) {
                holidayCategory = new HolidayCategory()
                print '-----------------holidayCategory-------------' + holidayCategory
                HolidayCategoryCO.saveHolidayCategory(holidayCategoryCO, holidayCategory)
                holidayCategory.save(flush: true, failOnError: true)
                String webRootDir = AppUtil.staticResourcesDirPath
                print('-------------webRootDir---------------' + webRootDir)
                CommonsMultipartFile commonsMultipartFile = params.categoryImage
                if (commonsMultipartFile.bytes) {
                    String fileName = commonsMultipartFile?.originalFilename?.trim()
                    File f = new File("${webRootDir}/categoryImages/${holidayCategory?.id}")
                    if (f.exists()) {
                        f.deleteDir()
                        f.mkdirs()
                        print "----------------directory can write -----------------------------" + f.canWrite()
                    } else {
                        f.mkdirs()
                        print "----------------directory can write -----------------------------" + f.canWrite()
                    }
                    File file = new File("${webRootDir}/categoryImages/${holidayCategory?.id}/${fileName}")
                    file.createNewFile()
                    holidayCategory.imageName = fileName
                    holidayCategory.imageType = commonsMultipartFile.contentType
                    imageService.storeImageInFileSystem(file, commonsMultipartFile.bytes)
                }
                redirect(controller: 'holiday', action: 'listCategory')

            } else {
                flash.error = g.message(code: 'categoryName.same.message')
                render(view: '/holiday/createCategory', model: [holidayCategoryCO: holidayCategoryCO])
            }
        }
        render(view: '/holiday/createCategory', model: [holidayCategoryCO: holidayCategoryCO])
    }
    def editCategory = {
        HolidayCategory holidayCategory = HolidayCategory.get(params.long('categoryId'))
        render(view: '/holiday/editCategory', model: [holidayCategory: holidayCategory])
    }

    def updateCategory = { HolidayCategoryCO holidayCategoryCO ->
        String categoryName = params.categoryName
        HolidayCategory holidayCategory = HolidayCategory.get(params.long('categoryId'))
        if (holidayCategoryCO.validate()) {
            HolidayCategory holidayCategoryFound = HolidayCategory.findByCategoryNameAndIdNotEqual(categoryName, holidayCategory?.id)
            if (!holidayCategoryFound) {
                holidayCategory.categoryName = categoryName
                HolidayCategoryCO.updateHolidayCategory(holidayCategoryCO, holidayCategory)
                redirect(controller: 'holiday', action: 'listCategory')
                holidayCategory.save(flush: true, failOnError: true)
                CommonsMultipartFile commonsMultipartFile = params.categoryImage
                String webRootDir = AppUtil.staticResourcesDirPath
                if (commonsMultipartFile.bytes) {
                    String fileName = commonsMultipartFile?.originalFilename?.trim()
                    File f = new File("${webRootDir}/categoryImages/${holidayCategory?.id}")
                    if (f.exists()) {
                        f.deleteDir()
                        f.mkdirs()
                        print "----------------directory can write -----------------------------" + f.canWrite()
                    } else {
                        f.mkdirs()
                        print "----------------directory can write ----in else-------------------------" + f.canWrite()
                    }
                    File file = new File("${webRootDir}/categoryImages/${holidayCategory?.id}/${fileName}")
                    holidayCategory.imageName = fileName
                    holidayCategory.imageType = commonsMultipartFile.contentType
                    file.createNewFile()
                    imageService.storeImageInFileSystem(file, commonsMultipartFile.bytes)
                }
            } else {
                flash.error = g.message(code: 'categoryName.same.message')
                render(view: '/holiday/editCategory', model: [holidayCategory: holidayCategory])
            }
        } else {
            render(view: '/holiday/editCategory', model: [holidayCategory: holidayCategory, holidayCategoryCO: holidayCategoryCO])
        }
    }

    def deleteCategory = {
        HolidayCategory holidayCategory = HolidayCategory.get(params.long('categoryId'))
        List<HolidayPackage> holidayList = HolidayPackage.findAllByHolidayCategory(holidayCategory)
        holidayList.each {
            it.holidayCategory = null
        }
        holidayCategory.delete(flush: true)
        flash.success = g.message(code: 'categoryName.delete.message')
        redirect(controller: 'holiday', action: 'listCategory')
    }

    def showHolidayCategory = {
        List<HolidayCategory> holidayCategoryList = HolidayCategory.list()

        List<BigDecimal> rangeList = []
        holidayCategoryList.each {
            HolidayCategory holidayCategory = it
            def c = HolidayPackage.createCriteria()
            def val = c.get {
                projections {
                    min('priceRange')
                }
                eq('holidayCategory', holidayCategory)
            }
            rangeList << val
        }
        print('---------------------value----------------' + rangeList)

        render(view: '/holiday/showHolidayCategory', model: [holidayCategoryList: holidayCategoryList, rangeList: rangeList])
    }

    def showHolidayCategoryImage = {
        HolidayCategory holidayCategory = HolidayCategory.get(params.long('categoryId'))
        print('-----------------------' + holidayCategory)
        def webRootDir = AppUtil.staticResourcesDirPath
        print('-------webRootDir----------------' + webRootDir)
        def avatarFilePath
        try {
            avatarFilePath = new File(webRootDir, "/categoryImages/${holidayCategory?.id}/${holidayCategory.imageName}");
            if (avatarFilePath) {
                response.setContentType(holidayCategory.imageName)
                response.setContentLength(avatarFilePath?.size()?.toInteger())
                OutputStream out = response.getOutputStream();
                out.write(avatarFilePath.bytes);
                out.close();
            }
        } catch (Exception e) {
            e.printStackTrace()
        }
    }

    def submitQuery = {
        print "------------------submit query for packages- params----------------" + params


        HolidayQuery holidayQuery = new HolidayQuery()
        holidayQuery.name = params.name
        holidayQuery.email = params.email
        holidayQuery.contactNo = params.contactNo
        holidayQuery.message = params.message
        holidayQuery.save(flush: true)
        try {
            mailService.sendMail {
                to "info@travelx360.com"
                subject "Holiday package query"
                html g.render(template: '/holiday/holidayQueryTemplate', model: [holidayQuery: holidayQuery])
            }
        }
        catch (Exception exc) {
            print '------exception occue while sending mail to travelex team for package query----------' + exc.printStackTrace()
        }
        flash.success = g.message(code: 'holidayQuery.submit.success.message')
        redirect(controller: 'holiday', action: 'showHolidayCategory')


    }


}
