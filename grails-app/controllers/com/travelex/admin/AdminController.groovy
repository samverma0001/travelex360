package com.travelex.admin

import com.travelex.Hotel.BookingHistory
import com.travelex.User.PaymentInfo
import com.travelex.User.Role
import com.travelex.User.User
import com.travelex.User.UserRole
import com.travelex.admin.margin.BookingType
import grails.plugin.mail.MailService
import serviceProvider.ServiceProvider

class AdminController {
    MailService mailService
    def springSecurityService

    def index() {
        println '========Inside Admin========'
        render(view: "/admin/adminIndex")

    }

    def approveServiceProvider = {

        String pass = "LoccoM"
        ServiceProvider serviceProvider = ServiceProvider.findById(params.serviceProviderId as Long)
        println '====serviceProvider=====' + serviceProvider.nameOfRepresentee
        serviceProvider.approved = true
        serviceProvider.save(flush: true)

        User newServiceProvider = new User()
        newServiceProvider.firstName = serviceProvider.nameOfRepresentee
        newServiceProvider.lastName = serviceProvider.nameOfRepresentee
        newServiceProvider.username = serviceProvider.officialEmail
        newServiceProvider.password = pass

        newServiceProvider.token = UUID.randomUUID().toString()
        newServiceProvider.save(flush: true)
        def email = serviceProvider.officialEmail
        User userInstance = User.findByUsername(serviceProvider.officialEmail)
        def token = userInstance.token
        mailService.sendMail {
            println '========Inside send mail======'
            to email
            subject "Registration request sent"
            html g.render(template: '/chooseProfileLogin/signUpMailTemplateForServiceProvider', model: [userToken: token, pass: pass], plugin: "email-confirmation")
            println '========Processed send mail======'
        }
        redirect(action: 'aboutToActivateAccountServiceProvider')

//        redirect(action: "activateServiceProviderAccount")
//        render("success")
    }

    def checkExistingEmail() {
        if (ServiceProvider.countByOfficialEmail(params.emailCheck)) {
            render("true")
        } else {
            render("false")
        }
    }
    def aboutToActivateAccountServiceProvider = {
        def user = params.currentUser
        render(view: '/home/activateAccountServiceProvider')
    }

    def activateServiceProviderAccount = {
        Role role1 = Role.findByAuthority('ROLE_PROVIDER')

        def token = params.userToken
        User user = User.findByToken(token)

//        User user = User.findByUsername(params.email)
        try {
            user.enabled = true
            user.hasVerifiedEmail = true
            user.token = UUID.randomUUID().toString()

            if (!user.save(flush: true)) {
                redirect(action: 'auth', controller: 'login', params: [messageString: 'Unable to Activate user.'])
            }

            UserRole.create(user, role1, true)

            UserRole userRole1 = UserRole.findByUserAndRole(user, role1)
            def userRole = userRole1.role.authority
            springSecurityService?.reauthenticate(user?.username)
            redirect(action: 'index', controller: 'home', params: [userRole: userRole])
        }
        catch (Exception e) {
            redirect(action: 'auth', controller: 'login', params: [messageString: 'Unable to Activate user.'])

        }
    }
}
