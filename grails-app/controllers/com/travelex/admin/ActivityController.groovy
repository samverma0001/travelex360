package com.travelex.admin

import activity.ActivitiesServiceProviders
import activity.Activity
import com.travelex.User.Role
import com.travelex.User.User
import com.travelex.User.UserRole
import com.travelex.socials.Enums.ActivityType
import serviceProvider.ServiceProvider

import java.sql.Timestamp

class ActivityController {

    def springSecurityService
    def mailService

    def index() {}

    def createActivityTemp = {
        render(view: "/adminactivity/createActivityTemp")
    }

    def createActivity = {
        print '-----------------------------'
        def activityTypeList = ActivityType.list()
        render(view: "/adminactivity/createActivity", model: [activityTypeList: activityTypeList])
    }

    def editActivity = {
        def activitiesList = Activity.list()
        render(view: "/adminactivity/editActivity", model: [activitiesList: activitiesList])
    }

    def editActivityModal = {
        println '==============in controller edit activity modal-------------'+params.activityId
        def activityInstance = Activity.get(params.activityId)
        println '---------------------------------'+activityInstance.serviceProviderId
        def serviceProviderInstance = ActivitiesServiceProviders.get(activityInstance.serviceProviderId)
//        return [activityInstance: activityInstance, serviceProviderInstance: serviceProviderInstance]
        println '-----------------'+activityInstance.type
        render (template: "activityEditModal", model: [activityInstance: activityInstance, serviceProviderInstance: serviceProviderInstance])
    }

    def saveActivity = {
        def profileImage1 = request.getFile('profileImage')
        def profileImage
        if(profileImage1.getFileItem().fileName.isEmpty()){
            String pathToImage1 = servletContext.getRealPath("/") + '/images/avatar.png';
            File file = new File(pathToImage1);
            profileImage = file.getBytes();
        }else{
            profileImage = profileImage1.bytes
        }
        try {
            def activitiesInstance = new Activity(name:params.name,location: params.location,companyAuthorisedPersonalName: params.companyAuthorisedPersonalName, activityCompanyName: params.activityCompanyName,primaryInterest: params.primaryInterest, secondaryInterest: params.secondaryInterest, phoneNumber: params.phoneNumber, email: params.email, createdDate: new Timestamp(new Date().time),editedDate: new Timestamp(new Date().time),activityImage: profileImage,duration: params.duration,type: params.activityType)
            if(params.serviceProviderName != null) {
                def activityServiceProvInst = ActivitiesServiceProviders.findByName(params.serviceProviderName)
                if(activityServiceProvInst) {
                    activitiesInstance.serviceProvider = activityServiceProvInst
                    activitiesInstance.save(flush:true, failOnError: true)
                } else {
                    activitiesInstance.serviceProvider = ActivitiesServiceProviders.findByName("Travelex")
                    activitiesInstance.save(flush:true, failOnError: true)
                }
            } else {
                activitiesInstance.serviceProvider = ActivitiesServiceProviders.findByName("Travelex")
                activitiesInstance.save(flush:true, failOnError: true)
            }
        }catch (e){
            e.printStackTrace()
        }
        def activitiesList = Activity.list()
        render(view: "/adminactivity/createActivity", model: [activitiesList: activitiesList])
    }

    def createServiceProvider = {
        render(view: "/adminactivity/createServiceProvider")
    }

    def updateActivityModal ={
        def activityInstance = Activity.get(Integer.parseInt(params.activityId))
        activityInstance.name = params.activityName
        activityInstance.type = params.type
        activityInstance.location = params.location
        activityInstance.duration = params.duration
        activityInstance.primaryInterest = params.primaryInterest
        activityInstance.secondaryInterest = params.secondaryInterest
        activityInstance.activityCompanyName = params.activityCompanyName
        activityInstance.companyAuthorisedPersonalName = params.companyAuthorisedPersonalName
        activityInstance.phoneNumber = params.phoneNumber
        activityInstance.email = params.email
        render "success"
    }

    def fetchServiceProvider = {
        println '---------------'+params
    }

    def saveServiceProvider = {
        def profileImage1 = request.getFile('profileImage')
        def profileImage
        if(profileImage1.getFileItem().fileName.isEmpty()){
            String pathToImage1 = servletContext.getRealPath("/") + '/images/avatar.png';
            File file = new File(pathToImage1);
            profileImage = file.getBytes();
        }else{
            profileImage = profileImage1.bytes
        }
        try {
            def serviceProviderInstance = new ActivitiesServiceProviders(name:params.name,location:params.location,email:params.email,phoneNumber: params.phoneNumber,authorisedPersonalName: params.authorisedPersonalName, createdDate: new Timestamp(new Date().time),editedDate: new Timestamp(new Date().time),serviceProviderImage: profileImage)
            serviceProviderInstance.save(flush: true,failOnError: true)
            render(view: "/adminactivity/createServiceProvider")
        } catch (e) {
            e.printStackTrace()
            render(view: "/adminactivity/createServiceProvider")
        }
    }

    def renderProfileImage(){
        User user = session.user
        byte[] profileImage;
        if(user == null){
            String pathToImage1 =  servletContext.getRealPath("/") + '/images/avatar.png';
            println pathToImage1
            File file = new File(pathToImage1);
            profileImage = file.getBytes();
        }else{
            profileImage = user.profileImage;
        }
        response.outputStream << profileImage
    }

    def approveServiceProvider = {

        String pass = "LoccoM"
        ServiceProvider serviceProvider = ServiceProvider.findById(params.serviceProviderId as Long)
        println '====serviceProvider=====' + serviceProvider.nameOfRepresentee
        serviceProvider.approved = true
        serviceProvider.save(flush: true)

        User newServiceProvider = new User()
        newServiceProvider.firstName = serviceProvider.nameOfRepresentee
        newServiceProvider.lastName = serviceProvider.nameOfRepresentee
        newServiceProvider.username = serviceProvider.officialEmail
        newServiceProvider.password = pass

        newServiceProvider.token = UUID.randomUUID().toString()
        newServiceProvider.save(flush: true)
        def email = serviceProvider.officialEmail
        User userInstance = User.findByUsername(serviceProvider.officialEmail)
        def token = userInstance.token
        mailService.sendMail {
            println '========Inside send mail======'
            to email
            subject "Registration request sent"
            html g.render(template: '/chooseProfileLogin/signUpMailTemplateForServiceProvider', model: [userToken: token, pass: pass], plugin: "email-confirmation")
            println '========Processed send mail======'
        }
        redirect(action: 'aboutToActivateAccountServiceProvider')

//        redirect(action: "activateServiceProviderAccount")
//        render("success")
    }

    def checkExistingEmail() {
        if (ServiceProvider.countByOfficialEmail(params.emailCheck)) {
            render("true")
        } else {
            render("false")
        }
    }
    def aboutToActivateAccountServiceProvider = {
        def user = params.currentUser
        render(view: '/home/activateAccountServiceProvider')
    }

    def activateServiceProviderAccount = {
        Role role1 = Role.findByAuthority('ROLE_PROVIDER')

        def token = params.userToken
        User user = User.findByToken(token)

//        User user = User.findByUsername(params.email)
        try {
            user.enabled = true
            user.hasVerifiedEmail = true
            user.token = UUID.randomUUID().toString()

            if (!user.save(flush: true)) {
                redirect(action: 'auth', controller: 'login', params: [messageString: 'Unable to Activate user.'])
            }

            UserRole.create(user, role1, true)

            UserRole userRole1 = UserRole.findByUserAndRole(user, role1)
            def userRole = userRole1.role.authority
            springSecurityService?.reauthenticate(user?.username)
            redirect(action: 'index', controller: 'home', params: [userRole: userRole])
        }
        catch (Exception e) {
            redirect(action: 'auth', controller: 'login', params: [messageString: 'Unable to Activate user.'])

        }
    }



}
