package com.travelex.admin

import com.travelex.admin.coupon.CouponVO
import grails.plugins.springsecurity.Secured
import util.AppUtil

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

//@Transactional(readOnly = true)
class CouponController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Coupon.list(params), model: [couponInstanceCount: Coupon.count()]
    }

    def show(Coupon couponInstance) {
        respond couponInstance
    }

    def create() {
        respond new Coupon(params)
    }

    @Transactional
    def save(Coupon couponInstance) {
        CouponVO couponVO = new CouponVO()
        bindData(couponVO, params)
        if (couponVO.validate()) {
            if (couponInstance == null) {
                print('1111111111111111111')
                notFound()
                return
            }

            if (couponInstance.hasErrors()) {
                print('222222222222222222222')
                respond couponInstance.errors, view: 'create'
                return
            }

            couponInstance.save flush: true

            request.withFormat {
                print('3333333333333333333333')

                form {
                    flash.message = message(code: 'default.created.message', args: [message(code: 'couponInstance.label', default: 'Coupon'), couponInstance.id])
                    redirect couponInstance
                }
                '*' { respond couponInstance, [status: CREATED] }
            }
        } else {
            print('in else -------------------')
            render(view: '/coupon/create', model: [couponVO: couponVO])
        }

    }

    def edit(Coupon couponInstance) {
        respond couponInstance
    }

    @Transactional
    def update(Coupon couponInstance) {
        CouponVO couponVO = new CouponVO()
        bindData(couponVO, params)
        if (couponVO.validate()) {
            if (couponInstance == null) {
                notFound()
                return
            }

            if (couponInstance.hasErrors()) {
                respond couponInstance.errors, view: 'edit'
                return
            }

            couponInstance.save flush: true

            request.withFormat {
                form {
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'Coupon.label', default: 'Coupon'), couponInstance.id])
                    redirect couponInstance
                }
                '*' { respond couponInstance, [status: OK] }
            }
        } else {
            print('in else -------------------')
            render(view: '/coupon/edit', model: [couponVO: couponVO,couponInstance:couponInstance ])
        }
    }

    @Transactional
    def delete(Coupon couponInstance) {

        if (couponInstance == null) {
            notFound()
            return
        }

        couponInstance.delete flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Coupon.label', default: 'Coupon'), couponInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'couponInstance.label', default: 'Coupon'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }


    def changeStatusDeactivate() {
        println '=====PARAMS=====' + params
        String couponId = params.couponId
        Coupon coupon = Coupon.findById(couponId as Long)

        coupon.status = Status.EXPIRED

        coupon.save(flush: true)

        redirect(action: "index")

    }

    def changeStatusActivate() {

        println '=====PARAMS=====' + params

        String couponId = params.couponId
        Coupon coupon = Coupon.findById(couponId as Long)

        coupon.status = Status.ACTIVE

        coupon.save(flush: true)

        redirect(action: "index")

    }
}
