package com.travelex.Hotel

import com.travelex.TboApi.CountryFromTbo
import com.travelex.User.User
import grails.plugins.springsecurity.Secured

class HotelController {
    def springSecurityService

    def scaffold = true

    def bookHotel = {
        def year = new Date().format("yy")
        def month = new Date().format("MMM")
        def monthInNumber = new Date().format("MM")
        def day = new Date().format("dd")
        def day1 = new Date().next()
        def checkOutDay = day1.format("dd")

        List<Date> dateList = []

        for (int i = 0; i <= 12; i++) {
            def current = new Date()
//            println 'currentDate' + current
            Calendar cal = Calendar.getInstance()
            cal.setTime(current)
            cal.set(Calendar.MONTH, (cal.get(Calendar.MONTH) + i))
            current = cal.getTime()
            dateList.add(current)
        }

        dateList.each() { date ->

        }
        List<String> citiesList = []
        def cityList = City.getAll()
        cityList.each { cityFromMGH ->
            citiesList.add(cityFromMGH.name)
        }
        List<CountryFromTbo> countryFromTbo = CountryFromTbo.getAll()
        countryFromTbo.each { cityFromTBO ->
            citiesList.add(cityFromTBO.countryName)
        }
        render(view: "/searchHotel/newThemed/hotelSearch", model: [dateList: dateList, citiesList: citiesList.unique(), cityList: cityList, year: year, month: month, day: day, checkOutDay: checkOutDay, monthInNumber: monthInNumber])
//        render(view: "/searchHotel/hotelBooking", model: [dateList: dateList, cityList: cityList, year: year, month: month, day: day, checkOutDay: checkOutDay, monthInNumber: monthInNumber])

    }
    def bookHotelPrev = {
        def cityList = City.getAll()
        render(view: "/hotel/bookHotel", model: [cityList: cityList])

    }

    def hotelList = {
        Long rangeMin = 0
        Long rangeMax = 0
        String budget = params.budget
        def place = params.place
        def place1 = "%" + place + "%"
        println '=============pLace' + place1
        println '===========' + budget
        def val = budget.value
        println '===========' + val

        if (budget == "1") {
            rangeMin = 1000
            rangeMax = 2000
        } else if (budget == "2") {
            rangeMin = 2000
            rangeMax = 4000
        } else if (budget == "3") {
            rangeMin = 4000
            rangeMax = 6000
        } else if (budget == "4") {
            rangeMin = 6000
            rangeMax = 8000
        } else if (budget == "5") {
            rangeMin = 8000
            rangeMax = 10000
        } else if (budget == "6") {
            rangeMin = 10000
            rangeMax = 12000
        } else if (budget == "7") {
            rangeMin = 12000
            rangeMax = 30000
        } else if (budget == "8") {
            rangeMin = 30000
            rangeMax = 70000
        } else if (budget == "9") {
            rangeMin = 70000
            rangeMax = 120000
        } else {
            render ' Return to home'
        }
        println '===============' + rangeMax
        println '===============' + rangeMin
//            Long aLong=5000
//            Long bLong=120000
        def range = HotelRoom.createCriteria()
        def result = range.list { between("maxPrice", rangeMin, rangeMax) }
//            List<HotelRoom> hotelRoomList=HotelRoom.findAllByMaxPriceBetween(rangeMin,rangeMax)

        println '============Range======' + result

        List<Hotel> hotelList = result*.hotel


        render(view: "/hotel/hotelList", model: [hotelList: hotelList])

    }


    def hotelDetails = {
        def hotelInstance = params.id
        def hotelInstance1 = Hotel.get(hotelInstance)
        List<HotelRoom> hotelRoom = HotelRoom.findAllByHotel(hotelInstance1)
        render(view: "/hotel/hotelDetails", model: [hotelInstance: hotelInstance1, hotelRoom: hotelRoom])
    }
    def searchHotel = {
        def year = new Date().format("yy")
        def month = new Date().format("MMM")
        def monthInNumber = new Date().format("MM")
        def day = new Date().format("dd")
        def day1 = new Date().next()
        def checkOutDay = day1.format("dd")

        List<Date> dateList = []

        for (int i = 0; i <= 12; i++) {
            def current = new Date()
            Calendar cal = Calendar.getInstance()
            cal.setTime(current)
            cal.set(Calendar.MONTH, (cal.get(Calendar.MONTH) + i))
            current = cal.getTime()
            dateList.add(current)
        }

        dateList.each() { date ->

        }


        def cityList = City.getAll()
        render(view: "/searchHotel/newThemed/hotelSearch", model: [dateList: dateList, cityList: cityList, year: year, month: month, day: day, checkOutDay: checkOutDay, monthInNumber: monthInNumber])

    }
    def resultList = {
        render(view: "/searchHotel/newThemed/hotelSearchResultList")

    }


    @Secured(['ROLE_USER'])
    def addToSummary = {
        User user = springSecurityService.currentUser as User
        AddToTravelSummary addToTravelSummary = new AddToTravelSummary()
        addToTravelSummary.hotelName = params.hotelName
        addToTravelSummary.hotelId = params.hotelId
        addToTravelSummary.roomType = params.roomType
        addToTravelSummary.addedOnDate = new Date().format("MM/dd/yyyy")
        addToTravelSummary.travelStartDate = params.checkIn
        addToTravelSummary.travelEndDate = params.checkOut
        addToTravelSummary.address = params.hotelPlace
        addToTravelSummary.noOfRooms = params.noOfRoom
        addToTravelSummary.roomCatId = params.hotelRoomCategoryId
        addToTravelSummary.occupantAdult = params.adult
        addToTravelSummary.occupantChild = params.child
        addToTravelSummary.price = params.price
        addToTravelSummary.addedByUser = user.username
        addToTravelSummary.user = user

        if (!addToTravelSummary.validate()) {
            addToTravelSummary.errors.allErrors.each {
                println '----------------' + it
            }
        }
        addToTravelSummary.save(flush: true)
        println '==============' + addToTravelSummary.user?.firstName
        List<AddToActivitySummary> addToActivitySummaryList = AddToActivitySummary.findAllByUser(user)
        List<AddToTravelSummary> addToTravelSummaryList = AddToTravelSummary.findAllByUser(user)

        List<BookingHistory> bookingHistoryList = BookingHistory.findAllByUser(user)

        redirect(controller: "home")

    }


    def delSummary = {
        Long id = params.addSummary as Long
        String from = params.from
        AddToTravelSummary addToTravelSummary = AddToTravelSummary.get(id)
        addToTravelSummary.user = null //to detach any relation between two domains
        addToTravelSummary.delete(flush: true)
        if (from == "travelSummary") {
            redirect(controller: "AddToTravelSummary", action: "index")
        } else {
            redirect(controller: "home", action: "index")
        }

    }

    def savePreviousUrl = {
        String referer = request.getHeader("referer")
        println '=======Referal======' + referer
        URL url = new URL(referer)
        session["prevUrl"] = url
        println '=====URL==In SavePrevUrl=====' + session["prevUrl"]
        render(session["prevUrl"])
    }

}
