package com.travelex.Hotel

import com.travelex.User.User
import com.travelex.admin.BookingStatusType
import com.travelex.admin.margin.BookingType
import com.travelex.booking.Adult
import com.travelex.booking.Child
import com.travelex.booking.HotelForBooking
import com.travelex.booking.RoomBooked
import wslite.soap.SOAPClient

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class BookingHistoryController {

    def grailsApplication

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        List<BookingHistory> bookingHistoryList = BookingHistory.findAllByBookingType(BookingType?.HOTEL)
        render(view: '/bookingHistory/index', model: [bookingHistoryList: bookingHistoryList])
    }

    def show(BookingHistory bookingHistoryInstance) {
        HotelForBooking hotelForBooking = HotelForBooking.findByBookingHistory(bookingHistoryInstance)
        render(view: '/bookingHistory/show', model: [bookingHistoryInstance: bookingHistoryInstance, hotelForBooking: hotelForBooking])
    }

    @Transactional
    def delete(BookingHistory bookingHistoryInstance) {

        if (bookingHistoryInstance == null) {
            notFound()
            return
        }

        HotelForBooking hotelForBooking = HotelForBooking.findByBookingHistory(bookingHistoryInstance)
        List<RoomBooked> roomBookedList = RoomBooked.findAllByHotelForBooking(hotelForBooking)
        roomBookedList.each {
            List<Adult> adultList = Adult.findAllByRoomBooked(it)
            adultList.each {
                it.roomBooked = null
            }
            List<Child> childList = Child.findAllByRoomBooked(it)
            childList.each {
                it.roomBooked = null
            }
            it.hotelForBooking = null
        }
        hotelForBooking.delete(flush: true)
        bookingHistoryInstance.delete flush: true


        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'BookingHistory.label', default: 'BookingHistory'), bookingHistoryInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'bookingHistoryInstance.label', default: 'BookingHistory'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

    def searchBookingType = {
        print('111111111111111111111111111' + params)
        List<String> filterNameList = params.list('selectedCheckBox[]') as List<String>
        print('111111111111111111111111111' + filterNameList)
        List<BookingHistory> bookingHistoryList = []

        filterNameList.each {
            if (it.equals('booked')) {
                bookingHistoryList = BookingHistory.findAllByBookingStatus(BookingStatusType.Booked)
            } else if (it.equals('cancelled')) {
                bookingHistoryList = bookingHistoryList + BookingHistory.findAllByBookingStatus(BookingStatusType.Cancelled)

            } else if (it.equals('vouchered')) {
                bookingHistoryList = bookingHistoryList + BookingHistory.findAllByBookingStatus(BookingStatusType.Vouchered)
            } else {
                bookingHistoryList = BookingHistory.findAll()
            }
        }
        print('------------------------------------------ ' + bookingHistoryList)
        render(template: '/bookingHistory/list', model: [bookingHistoryList: bookingHistoryList])
    }

    def showVoucher = {
        BookingHistory bookingHistory = BookingHistory.get(params.long('bookingHistoryId'))
        HotelForBooking hotelForBooking = HotelForBooking.findByBookingHistory(bookingHistory)
        List<RoomBooked> roomBookedList = RoomBooked.findAllByHotelForBooking(hotelForBooking)
        render(view: '/bookingHistory/showVoucher', model: [bookingHistory: bookingHistory, roomBookedList: roomBookedList, hotelForBooking:
                hotelForBooking])
    }


    def cancelBookingByAdmin = {
        print('---------- params are here for cancel room booking -----------------' + params)
        String bookingRefId = params.bookRefNo
        String bookingHistoryId = params.bookingHistoryId
        String hotelCancel = "1"

//        def client = new SOAPClient("http://api.tektravels.com/tbohotelapi_v6/hotelservice.asmx")
        def client = new SOAPClient("http://airapi.travelboutiqueonline.com/tbohotelapi_v6/hotelservice.asmx")

        def response = client.send("""<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <AuthenticationData xmlns="http://TekTravel/HotelBookingApi">
      <SiteName></SiteName>
      <AccountCode></AccountCode>
      <UserName>${grailsApplication.config.tbo.TRAVELX_UNAME}</UserName>
      <Password>${grailsApplication.config.tbo.TRAVELX_PASS_HOTEL}</Password>
    </AuthenticationData>
  </soap:Header>
  <soap:Body>
    <SendHotelChangeRequest xmlns="http://TekTravel/HotelBookingApi">
      <request>
        <BookingId>${bookingRefId}</BookingId>
        <RequestType>HotelCancel</RequestType>
        <Remarks>"abcd"</Remarks>
      </request>
    </SendHotelChangeRequest>
  </soap:Body>
</soap:Envelope>"""
        )
        print('---------- here response of request for cancel -------------------' + response.text)
        def responseText = new XmlSlurper().parseText(response.text)
        String cancelId = responseText.children().SendHotelChangeRequestResponse.SendHotelChangeRequestResult.ChangeRequestId
        String responseStatus = responseText.children().SendHotelChangeRequestResponse.SendHotelChangeRequestResult.Status.StatusCode
        print('------------------ response status code is -------------------------' + responseStatus)
        if (responseStatus.equals('01')) {
            BookingHistory bookingHistory = BookingHistory.findById(bookingHistoryId.toLong())
            bookingHistory.bookingStatus = BookingStatusType.Cancelled
            bookingHistory.bookingCancelId = cancelId.toString()
            flash.success = g.message(code: 'booking.cancel.successfully')
            redirect(controller: 'bookingHistory', action: 'index')
        } else if (responseStatus.equals('02')) {
            flash.error = g.message(code: 'booking.cancel.merchant.authentication.fail')
            redirect(controller: 'bookingHistory', action: 'index')
        } else if (responseStatus.equals('06') || responseStatus.equals('08') || responseStatus.equals('09') || responseStatus.equals('10')) {
            flash.error = g.message(code: 'booking.cancel.merchant.tbo.fail')
            redirect(controller: 'bookingHistory', action: 'index')
        }

    }


}
