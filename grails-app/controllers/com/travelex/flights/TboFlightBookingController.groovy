package com.travelex.flights

import com.travelex.User.Role
import com.travelex.User.User
import com.travelex.User.UserRole
import com.travelex.admin.Flight
import com.travelex.bootstrap.UtilService
import com.travelex.flight.BookedFlight
import com.travelex.flight.BookedFlightFare
import com.travelex.flight.BookedFlightPassenger
import com.travelex.flight.BookedFlightTicket
import com.travelex.flight.FlightCity
import com.travelex.flightFunctionality.FareBreakDown
import com.travelex.flightFunctionality.FlightFare
import com.travelex.flightFunctionality.FlightFareRule
import com.travelex.flightFunctionality.FlightSearchResultVO
import com.travelex.flightFunctionality.FlightSearchVO
import com.travelex.flightFunctionality.FlightSegmentVO
import grails.converters.JSON
import org.apache.commons.lang.RandomStringUtils
import org.codehaus.groovy.grails.web.context.ServletContextHolder
import wslite.soap.SOAPClient
import wslite.soap.SOAPResponse

import javax.xml.namespace.QName
import javax.xml.soap.MessageFactory
import javax.xml.soap.SOAPBody
import javax.xml.soap.SOAPBodyElement
import javax.xml.soap.SOAPEnvelope
import javax.xml.soap.SOAPHeader
import javax.xml.soap.SOAPHeaderElement
import javax.xml.soap.SOAPMessage
import javax.xml.soap.SOAPPart
import java.text.SimpleDateFormat

class TboFlightBookingController {

    def grailsApplication
    UtilService utilService

    def index() {}


    def searchFlights = {
        render(view: "/flights/flightSearch")
    }


    def findCityForFlight = {
        String cityName = params.term
        List<FlightCity> citiesList = []
        citiesList = FlightCity.createCriteria().list {
            ilike('cityName', cityName + "%")
        }
        List<String> finalCityList = []
        String cityNameCode
        citiesList.each {
            cityNameCode = it.cityName + "(" + it.cityCode + ")"
            finalCityList << cityNameCode
        }
        render finalCityList as JSON
    }

    def flightCityList = {
        println '======INSIDE save City========'
        def a = ServletContextHolder.getServletContext().getRealPath("/")
        println '======AfterGetting path========' + a
        def file = new File(a, "/WEB-INF/xmlFiles/CityList.xml")
        println '=====File=====' + file
        def locations = new XmlSlurper().parseText(file.text)
//        def locations = new XmlSlurper().parse(file.text)
        println '===========After parsing==========' + locations
        println '===========After parsing==========' + locations.children().root

        locations.city.each { city ->
            FlightCity flightCity = new FlightCity()
            flightCity.cityCode = city.CityCode
            flightCity.cityName = city.CityName
            flightCity.countryCode = city.CountryCode
            flightCity.save(flush: true)
        }
        render("SUCCESS")
    }


    def searchFlightsForCity2 = {
        print('------------params are---------------------' + params)
        Integer max = params.max ? params.int('max') : 10
        print('------------max---------------------' + max)
        Integer offset = params.offset ? params.int('offset') : 0
        print('-----------offset---------------------' + offset)
        String flightFromDom = params.flightFromDom
        String flightToDom = params.flightToDom
//        String flightFromInter = params.flightFromInter
//        String flightToInter = params.flightToInter

        String location = params.location
        String flightFrom = null
        String flightTo = null

        if (location == "Domestic") {
            flightFrom = flightFromDom?.substring(flightFromDom?.indexOf("(")?.next(), flightFromDom?.indexOf(")"))
            flightTo = flightToDom?.substring(flightToDom?.indexOf("(")?.next(), flightToDom?.indexOf(")"))
        } else {
            flightFrom = flightFromDom?.substring(flightFromDom?.indexOf("(")?.next(), flightFromDom?.indexOf(")"))
            flightTo = flightToDom?.substring(flightToDom?.indexOf("(")?.next(), flightToDom?.indexOf(")"))
        }

        String departDate = params.departDate
        String timeTable = params.timeTable
        String returnDate = params.returnDate
        String flightClass = params.flightClass ?: 'Economy'
        String adult = params.adult
        String children = params.children
        String infant = params.infant
//        String carrier = params.carrier
//        println '=========carrier ========' + carrier

        FlightSearchVO flightSearchVo = new FlightSearchVO()

        String checkIn = params.departDate
        SimpleDateFormat fromUser = new SimpleDateFormat("mm/dd/yyyy");
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-mm-dd")
        SimpleDateFormat fromUser2 = new SimpleDateFormat("mm/dd/yyyy");
        SimpleDateFormat myFormat2 = new SimpleDateFormat("yyyy-mm-dd")
//        try {
        String flightDate = myFormat.format(fromUser.parse(checkIn))
        String flightReturnDate = null
        if (returnDate) {
            flightReturnDate = myFormat.format(fromUser.parse(returnDate))
        }

        flightSearchVo.journeyType = timeTable
        flightSearchVo.journeyDate = flightDate
        flightSearchVo.journeyReturnDate = flightReturnDate
        flightSearchVo.flightFromDom = flightFromDom
        flightSearchVo.flightToDom = flightToDom
//        flightSearchVo.flightFromInter = flightFromInter
//        flightSearchVo.flightToInter = flightToInter
        flightSearchVo.cabinClass = flightClass
        flightSearchVo.location = location
        flightSearchVo.adult = adult
        flightSearchVo.children = children
        flightSearchVo.infant = infant
//        flightSearchVo.preferredCarrier = carrier

        session["flightSearchVO"] = flightSearchVo

//            String checkOutDate = myFormat2.format(fromUser2.parse(checkOut))

//        def flightDate1 = new Date()

        def client = new SOAPClient("http://api.tektravels.com/tboapi_v7/service.asmx")
        SOAPResponse response
        if (flightReturnDate) {
            print("------------inside if -----------------")
            response = client.send("""<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"
               xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Header>
        <AuthenticationData xmlns="http://192.168.0.170/TT/BookingAPI">
            <SiteName/>
            <AccountCode/>
             <UserName>${grailsApplication.config.tbo.FLIGHT_USERNAME}</UserName>
             <Password>${grailsApplication.config.tbo.FLIGHT_PASS}</Password>
        </AuthenticationData>
    </soap:Header>
    <soap:Body>
        <Search xmlns="http://192.168.0.170/TT/BookingAPI">
            <request>
                <Origin>${flightFrom}</Origin>
                <Destination>${flightTo}</Destination>
                <DepartureDate>${flightDate}</DepartureDate>
                <Type>${timeTable}</Type>
                <ReturnDate>${flightReturnDate}</ReturnDate>
                <CabinClass>${flightClass}</CabinClass>
                <PreferredCarrier></PreferredCarrier>
                <AdultCount>${adult.toInteger()}</AdultCount>
                <ChildCount>${children.toInteger()}</ChildCount>
                <InfantCount>${infant.toInteger()}</InfantCount>
                <SeniorCount>0</SeniorCount>
                <PromotionalPlanType>Normal</PromotionalPlanType>
            </request>
        </Search>
    </soap:Body>
</soap:Envelope>"""
            )
        } else {
            print("------------inside else -----------------")
            response = client.send("""<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"
               xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Header>
        <AuthenticationData xmlns="http://192.168.0.170/TT/BookingAPI">
            <SiteName/>
            <AccountCode/>
             <UserName>${grailsApplication.config.tbo.FLIGHT_USERNAME}</UserName>
             <Password>${grailsApplication.config.tbo.FLIGHT_PASS}</Password>
        </AuthenticationData>
    </soap:Header>
    <soap:Body>
        <Search xmlns="http://192.168.0.170/TT/BookingAPI">
            <request>
                <Origin>${flightFrom}</Origin>
                <Destination>${flightTo}</Destination>
                <DepartureDate>${flightDate}</DepartureDate>
                <Type>${timeTable}</Type>
                <CabinClass>${flightClass}</CabinClass>
                <PreferredCarrier></PreferredCarrier>
                <AdultCount>${adult.toInteger()}</AdultCount>
                <ChildCount>${children.toInteger()}</ChildCount>
                <InfantCount>${infant.toInteger()}</InfantCount>
                <SeniorCount>0</SeniorCount>
                <PromotionalPlanType>Normal</PromotionalPlanType>
            </request>
        </Search>
    </soap:Body>
</soap:Envelope>"""
            )
        }
        println '---==response==3333==' + response.text

        def countryList = new XmlSlurper().parseText(response.text)

        List<FlightSearchResultVO> flightSearchVOList = []
        List<FlightSearchResultVO> flightSearchVOListInbound = []
        List<FlightSearchResultVO> flightSearchVOListPaginated = []
        List<FlightSearchResultVO> flightSearchVOListPaginatedInbound = []
        countryList.children().SearchResponse.SearchResult.Result.WSResult.each {
            String indicator = it.TripIndicator
            print '----------indicator------------' + indicator
            if (indicator.equals('1')) {
                FlightSearchResultVO flightSearchResultVO1 = new FlightSearchResultVO()
                flightSearchResultVO1.isDomestic = countryList.children().SearchResponse.SearchResult.IsDomestic
                String lccFlag = it.IsLcc
                if (lccFlag.equalsIgnoreCase('false')) {
                    flightSearchResultVO1.isLcc = false
                } else {
                    flightSearchResultVO1.isLcc = true
                }
                flightSearchResultVO1.isRoundTrip = countryList.children().SearchResponse.SearchResult.RoundTrip
                flightSearchResultVO1.isRefundable = it.NonRefundable
                flightSearchResultVO1.tripIndicator = it.TripIndicator
                FlightFare flightFare1 = new FlightFare()
                flightFare1.baseFare = it.Fare.BaseFare
                flightFare1.tax = it.Fare.Tax
                flightFare1.serviceTax = it.Fare.ServiceTax
                flightFare1.additionalTxnFee = it.Fare.AdditionalTxnFee
                flightFare1.agentCommission = it.Fare.AgentCommission
                flightFare1.tdsOnCommission = it.Fare.TdsOnCommission
                flightFare1.incentiveEarned = it.Fare.IncentiveEarned
                flightFare1.tdsOnIncentive = it.Fare.TdsOnIncentive
                flightFare1.pLBEarned = it.Fare.PLBEarned
                flightFare1.tdsOnPLB = it.Fare.TdsOnPLB
                flightFare1.publishedPrice = it.Fare.PublishedPrice
                flightFare1.airTransFee = it.Fare.AirTransFee
                flightFare1.currency = it.Fare.Currency
                flightFare1.discount = it.Fare.Discount
                flightFare1.otherCharges = it.Fare.OtherCharges
                flightFare1.fuelSurcharge = it.Fare.FuelSurcharge
                flightFare1.transactionFee = it.Fare.TransactionFee
                flightFare1.reverseHandlingCharge = it.Fare.ReverseHandlingCharge
                flightFare1.offeredFare = it.Fare.OfferedFare
                flightFare1.agentServiceCharge = it.Fare.AgentServiceCharge
                flightFare1.agentConvienceCharges = it.Fare.AgentConvienceCharges
                flightSearchResultVO1.flightFare = flightFare1

                FareBreakDown fareBreakDown1 = new FareBreakDown()

                it.FareBreakdown.WSPTCFare.each { fareBrkDown ->
                    fareBreakDown1.passengerType = fareBrkDown.PassengerType
                    fareBreakDown1.passengerCount = fareBrkDown.PassengerCount
                    fareBreakDown1.baseFare = fareBrkDown.BaseFare
                    fareBreakDown1.tax = fareBrkDown.Tax
                    fareBreakDown1.airlineTransFee = fareBrkDown.AirlineTransFee
                    fareBreakDown1.additionalTxnFee = fareBrkDown.AdditionalTxnFee
                    fareBreakDown1.fuelSurcharge = fareBrkDown.FuelSurcharge
                    fareBreakDown1.agentServiceCharge = fareBrkDown.AgentServiceCharge
                    fareBreakDown1.agentConvienceCharges = fareBrkDown.AgentConvienceCharges
                    flightSearchResultVO1.fareBreakDown.add(fareBreakDown1)
                }
                it.Segment.WSSegment.each { wsegment ->
                    FlightSegmentVO flightSegmentVO1 = new FlightSegmentVO()
                    flightSegmentVO1.segmentIndicator = wsegment.SegmentIndicator
                    flightSegmentVO1.airLineCode = wsegment.Airline.AirlineCode
                    flightSegmentVO1.airLineName = wsegment.Airline.AirlineName
                    flightSegmentVO1.airLineRemark = wsegment.Airline.AirLineRemarks
                    flightSegmentVO1.flightNumber = wsegment.FlightNumber
                    flightSegmentVO1.fareClass = wsegment.FareClass
                    flightSegmentVO1.originAirPortCode = wsegment.Origin.AirportCode
                    flightSegmentVO1.originAirPortName = wsegment.Origin.AirportName
                    flightSegmentVO1.originTerminal = wsegment.Origin.Terminal
                    flightSegmentVO1.originCityCode = wsegment.Origin.CityCode
                    flightSegmentVO1.originCityName = wsegment.Origin.CityName
                    flightSegmentVO1.originCountryCode = wsegment.Origin.CountryCode
                    flightSegmentVO1.originCountryName = wsegment.Origin.CountryName
                    flightSegmentVO1.destinationAirPortCode = wsegment.Destination.AirportCode
                    flightSegmentVO1.destinationAirPortName = wsegment.Destination.AirportName
                    flightSegmentVO1.destinationTerminal = wsegment.Destination.Terminal
                    flightSegmentVO1.destinationCityCode = wsegment.Destination.CityCode
                    flightSegmentVO1.destinationCityName = wsegment.Destination.CityName
                    flightSegmentVO1.destinationCountryCode = wsegment.Destination.CountryCode
                    flightSegmentVO1.destinationCountryName = wsegment.Destination.CountryName
                    flightSegmentVO1.departureTime = wsegment.DepTIme
                    flightSegmentVO1.arrivalTime = wsegment.ArrTime
                    flightSegmentVO1.stops = wsegment.Stop
                    print '---------flightSegmentVO1.stops---------' + flightSegmentVO1.stops
                    flightSegmentVO1.duration = wsegment.Duration
                    flightSegmentVO1.status = wsegment.Status
                    flightSegmentVO1.craftType = wsegment.Craft
                    flightSegmentVO1.operatingCarrier = wsegment.OperatingCarrier
                    flightSegmentVO1.eTicketEligible = wsegment.ETicketEligible
                    flightSearchResultVO1.flightSegmentVO.add(flightSegmentVO1)
                }
                FlightFareRule flightFareRule1 = new FlightFareRule()

                it.FareRule.WSFareRule.each { fareRule ->
                    flightFareRule1.origin = fareRule.Origin
                    flightFareRule1.destination = fareRule.Destination
                    flightFareRule1.airline = fareRule.Airline
                    flightFareRule1.fareRestriction = fareRule.FareRestriction
                    flightFareRule1.fareBasisCode = fareRule.FareBasisCode
                    flightFareRule1.departureDate = fareRule.DepartureDate
                    flightFareRule1.returnDate = fareRule.ReturnDate
                    flightFareRule1.source = fareRule.Source
                    flightSearchResultVO1.flightFareRule.add(flightFareRule1)

                }
                flightSearchResultVO1.origin = it.Origin
                flightSearchResultVO1.destination = it.Destination
                flightSearchResultVO1.ibDuration = it.IbDuration
                print '--------------flightSearchResultVO1.ibDuration----------------' + flightSearchResultVO1.ibDuration
                flightSearchResultVO1.obDuration = it.ObDuration
                print '--------------flightSearchResultVO1.obDuration----------------' + flightSearchResultVO1.obDuration
                flightSearchResultVO1.promotionalPlan = it.PromotionalPlanType
                flightSearchResultVO1.segmentKey = it.SegmentKey
                flightSearchResultVO1.sessionId = countryList.children().SearchResponse.SearchResult.SessionId
                flightSearchVOList << flightSearchResultVO1
            } else {
                FlightSearchResultVO flightSearchResultVO = new FlightSearchResultVO()
                flightSearchResultVO.isDomestic = countryList.children().SearchResponse.SearchResult.IsDomestic
                String lccFlag = it.IsLcc
                if (lccFlag.equalsIgnoreCase('false')) {
                    flightSearchResultVO.isLcc = false
                } else {
                    flightSearchResultVO.isLcc = true
                }
                flightSearchResultVO.isRoundTrip = countryList.children().SearchResponse.SearchResult.RoundTrip
                flightSearchResultVO.isRefundable = it.NonRefundable
                flightSearchResultVO.tripIndicator = it.TripIndicator
                FlightFare flightFare1 = new FlightFare()
                flightFare1.baseFare = it.Fare.BaseFare
                print '----------flightFare1.baseFare----------------------' + flightFare1.offeredFare
                flightFare1.tax = it.Fare.Tax
                flightFare1.serviceTax = it.Fare.ServiceTax
                flightFare1.additionalTxnFee = it.Fare.AdditionalTxnFee
                flightFare1.agentCommission = it.Fare.AgentCommission
                flightFare1.tdsOnCommission = it.Fare.TdsOnCommission
                flightFare1.incentiveEarned = it.Fare.IncentiveEarned
                flightFare1.tdsOnIncentive = it.Fare.TdsOnIncentive
                flightFare1.pLBEarned = it.Fare.PLBEarned
                flightFare1.tdsOnPLB = it.Fare.TdsOnPLB
                flightFare1.publishedPrice = it.Fare.PublishedPrice
                flightFare1.airTransFee = it.Fare.AirTransFee
                flightFare1.currency = it.Fare.Currency
                flightFare1.discount = it.Fare.Discount
                flightFare1.otherCharges = it.Fare.OtherCharges
                flightFare1.fuelSurcharge = it.Fare.FuelSurcharge
                flightFare1.transactionFee = it.Fare.TransactionFee
                flightFare1.reverseHandlingCharge = it.Fare.ReverseHandlingCharge
                flightFare1.offeredFare = it.Fare.OfferedFare
                flightFare1.agentServiceCharge = it.Fare.AgentServiceCharge
                flightFare1.agentConvienceCharges = it.Fare.AgentConvienceCharges
                flightSearchResultVO.flightFare = flightFare1
                FareBreakDown fareBreakDown1 = new FareBreakDown()
                it.FareBreakdown.WSPTCFare.each { fareBrkDown ->
                    fareBreakDown1.passengerType = fareBrkDown.PassengerType
                    fareBreakDown1.passengerCount = fareBrkDown.PassengerCount
                    fareBreakDown1.baseFare = fareBrkDown.BaseFare
                    fareBreakDown1.tax = fareBrkDown.Tax
                    fareBreakDown1.airlineTransFee = fareBrkDown.AirlineTransFee
                    fareBreakDown1.additionalTxnFee = fareBrkDown.AdditionalTxnFee
                    fareBreakDown1.fuelSurcharge = fareBrkDown.FuelSurcharge
                    fareBreakDown1.agentServiceCharge = fareBrkDown.AgentServiceCharge
                    fareBreakDown1.agentConvienceCharges = fareBrkDown.AgentConvienceCharges
                    flightSearchResultVO.fareBreakDown.add(fareBreakDown1)
                }
                it.Segment.WSSegment.each { wsegment ->
                    FlightSegmentVO flightSegmentVO1 = new FlightSegmentVO()
                    flightSegmentVO1.segmentIndicator = wsegment.SegmentIndicator
                    flightSegmentVO1.airLineCode = wsegment.Airline.AirlineCode
                    flightSegmentVO1.airLineName = wsegment.Airline.AirlineName
                    flightSegmentVO1.airLineRemark = wsegment.Airline.AirLineRemarks
                    flightSegmentVO1.flightNumber = wsegment.FlightNumber
                    flightSegmentVO1.fareClass = wsegment.FareClass
                    flightSegmentVO1.originAirPortCode = wsegment.Origin.AirportCode
                    flightSegmentVO1.originAirPortName = wsegment.Origin.AirportName
                    flightSegmentVO1.originTerminal = wsegment.Origin.Terminal
                    flightSegmentVO1.originCityCode = wsegment.Origin.CityCode
                    flightSegmentVO1.originCityName = wsegment.Origin.CityName
                    flightSegmentVO1.originCountryCode = wsegment.Origin.CountryCode
                    flightSegmentVO1.originCountryName = wsegment.Origin.CountryName
                    flightSegmentVO1.destinationAirPortCode = wsegment.Destination.AirportCode
                    flightSegmentVO1.destinationAirPortName = wsegment.Destination.AirportName
                    flightSegmentVO1.destinationTerminal = wsegment.Destination.Terminal
                    flightSegmentVO1.destinationCityCode = wsegment.Destination.CityCode
                    flightSegmentVO1.destinationCityName = wsegment.Destination.CityName
                    flightSegmentVO1.destinationCountryCode = wsegment.Destination.CountryCode
                    flightSegmentVO1.destinationCountryName = wsegment.Destination.CountryName
                    flightSegmentVO1.departureTime = wsegment.DepTIme
                    flightSegmentVO1.arrivalTime = wsegment.ArrTime
                    flightSegmentVO1.stops = wsegment.Stop
                    flightSegmentVO1.duration = wsegment.Duration
                    flightSegmentVO1.status = wsegment.Status
                    flightSegmentVO1.craftType = wsegment.Craft
                    flightSegmentVO1.operatingCarrier = wsegment.OperatingCarrier
                    flightSegmentVO1.eTicketEligible = wsegment.ETicketEligible
                    flightSearchResultVO.flightSegmentVO.add(flightSegmentVO1)
                }

                FlightFareRule flightFareRule1 = new FlightFareRule()

                it.FareRule.WSFareRule.each { fareRule ->
                    flightFareRule1.origin = fareRule.Origin
                    flightFareRule1.destination = fareRule.Destination
                    flightFareRule1.airline = fareRule.Airline
                    flightFareRule1.fareRestriction = fareRule.FareRestriction
                    flightFareRule1.fareBasisCode = fareRule.FareBasisCode
                    flightFareRule1.departureDate = fareRule.DepartureDate
                    flightFareRule1.returnDate = fareRule.ReturnDate
                    flightFareRule1.source = fareRule.Source
                    flightSearchResultVO.flightFareRule.add(flightFareRule1)

                }

                flightSearchResultVO.origin = it.Origin

                flightSearchResultVO.destination = it.Destination
                flightSearchResultVO.ibDuration = it.IbDuration
                flightSearchResultVO.obDuration = it.ObDuration
                flightSearchResultVO.promotionalPlan = it.PromotionalPlanType
                flightSearchResultVO.segmentKey = it.SegmentKey
                flightSearchResultVO.sessionId = countryList.children().SearchResponse.SearchResult.SessionId
                print '------------sessionId----------------' + flightSearchResultVO.sessionId
                flightSearchVOListInbound.add(flightSearchResultVO)
            }
        }
        String minPriceRange = flightSearchVOList?.flightFare?.offeredFare?.min()
        print '-----------minPriceRange-----------' + minPriceRange
        String maxPriceRange = flightSearchVOList?.flightFare?.offeredFare?.max()
        print '-----------maxPriceRange-----------' + maxPriceRange
        flightSearchVOListPaginated = flightSearchVOList ? flightSearchVOList.subList(offset, (offset + max) > flightSearchVOList.size() ? flightSearchVOList.size() : offset + max) : []
        flightSearchVOListPaginatedInbound = flightSearchVOListInbound ? flightSearchVOListInbound.subList(offset, (offset + max) > flightSearchVOListInbound.size() ? flightSearchVOListInbound.size() : offset + max) : []
        session["flightSearchList"] = flightSearchVOList
        session["flightSearchVOListInbound"] = flightSearchVOListInbound
        int nonStopCount = 0
        int redEyesCount = 0
        int budgetFlightCount = 0
        flightSearchVOList.each {
            FlightSearchResultVO flightSearchResultVO = it
            if (flightSearchResultVO.flightSegmentVO.size() - 1 == 0) {
                print '------------nonStopCount-------' + nonStopCount
                nonStopCount++
            }
            BigDecimal avgPrice = (minPriceRange.toBigDecimal() + maxPriceRange.toBigDecimal()) / 2
            print '----------avgPrice---------' + avgPrice
            BigDecimal price = flightSearchResultVO.flightFare.offeredFare.toBigDecimal()
            print '--------offered--price---------' + price
            if (avgPrice <= price) {
                print '-----redEyesCount--------' + redEyesCount
                redEyesCount++
            } else {
                if (avgPrice >= price) {
                    print '-----budgetFlightCount--------' + budgetFlightCount
                    budgetFlightCount++
                }
            }
        }
        if (!flightSearchVo?.journeyReturnDate) {
            render(view: "/flights/flightSearchResultList", model: [flightSearchVOList                : flightSearchVOListPaginated,
                                                                    flightSearchVo                    : flightSearchVo, total: flightSearchVOList.size(),
                                                                    flightSearchVOListPaginatedInbound: flightSearchVOListPaginatedInbound, totalInbound: flightSearchVOListInbound.size(), minPriceRange: minPriceRange, maxPriceRange: maxPriceRange, nonStopCount: nonStopCount, redEyesCount: redEyesCount, budgetFlightCount: budgetFlightCount])
        } else {
            render(view: "/flights/flightSearchResultList", model: [flightSearchVOList                : flightSearchVOListPaginated,
                                                                    flightSearchVo                    : flightSearchVo, total: flightSearchVOListInbound.size(),
                                                                    flightSearchVOListPaginatedInbound: flightSearchVOListPaginatedInbound, totalInbound: flightSearchVOListPaginatedInbound.size(), minPriceRange: minPriceRange, maxPriceRange: maxPriceRange])
        }

    }


    def flightSearchResultPaginated = {
        print '--------flightSearchResultPaginated----------' + params
        Integer max = params.max ? params.int('max') : 10
        print '--------max----------' + max
        Integer offset = params.offset ? params.int('offset') : 0
        print '--------offset----------' + offset
        FlightSearchVO flightSearchVO = session["flightSearchVO"] as FlightSearchVO
        List<FlightSearchResultVO> flightSearchVOList = []
        List<FlightSearchResultVO> flightSearchVOListPaginated = []
        flightSearchVOList = session["flightSearchList"] as List
        flightSearchVOListPaginated = flightSearchVOList ? flightSearchVOList.subList(offset, (offset + max) > flightSearchVOList.size() ? flightSearchVOList.size() : offset + max) : []
        print '--------flightSearchVOList---------' + flightSearchVOList
        print '--------flightSearchVOListPaginated---------' + flightSearchVOListPaginated
        List<FlightSearchResultVO> flightSearchVOListInbound = []
        List<FlightSearchResultVO> flightSearchVOListPaginatedInbound = []

        if (!flightSearchVO?.journeyReturnDate) {
            String renderOneWay = render(template: "/flightTemplate/flightOneWayTemplate", model: [flightSearchVOList: flightSearchVOListPaginated, flightSearchVo:
                    flightSearchVO, total                                                                            : flightSearchVOList.size()
            ])
            render renderOneWay
        } else {
            print 'else'
            flightSearchVOListInbound = session["flightSearchVOListInbound"] as List
            print "flightSearchVOListInbound" + flightSearchVOListInbound
            flightSearchVOListPaginatedInbound = flightSearchVOListInbound ? flightSearchVOListInbound.subList(offset, (offset + max) > flightSearchVOListInbound.size() ? flightSearchVOListInbound.size() : offset + max) : []
            print "flightSearchVOListPaginatedInbound" + flightSearchVOListPaginatedInbound
            def renderOutBound = g.render(template: "/flightTemplate/flightReturnOutboundTemplate", model: [flightSearchVOList: flightSearchVOListPaginated, flightSearchVo:
                    flightSearchVO, total                                                                                     : flightSearchVOList.size()
            ])
            def renderInBound = g.render(template: "/flightTemplate/flightReturnInboundTemplate", model: [flightSearchVOListPaginatedInbound: flightSearchVOListPaginatedInbound, flightSearchVo:
                    flightSearchVO, total                                                                                                   : flightSearchVOList.size()
            ])
            def data = [renderOutBound: renderOutBound, renderInBound: renderInBound]
            render data as JSON
        }

    }


    def searchFlightByCategory = {
        print '------------' + params
        List<FlightSearchResultVO> flightSearchVOList = []
        List<FlightSearchResultVO> flightSearchVOFilterList = []
        flightSearchVOList = session["flightSearchList"] as List
        String flightCategory = params.flightCategory

        String minPriceRange = flightSearchVOList?.flightFare?.offeredFare?.min()
        String maxPriceRange = flightSearchVOList?.flightFare?.offeredFare?.max()

        BigDecimal avgPrice = (minPriceRange.toBigDecimal() + maxPriceRange.toBigDecimal()) / 2
        print '----------avgPrice---------' + avgPrice


        if (flightCategory.equals('nonStopFlightCategory')) {
            flightSearchVOList.each {
                FlightSearchResultVO flightSearchResultVO = it
                if (flightSearchResultVO?.flightSegmentVO?.size() - 1 == 0) {
                    flightSearchVOFilterList << flightSearchResultVO
                }
            }
        } else if (flightCategory.equals('redEyesFlightCategory')) {
            flightSearchVOList.each {
                FlightSearchResultVO flightSearchResultVO = it
                BigDecimal price = flightSearchResultVO.flightFare.offeredFare.toBigDecimal()
                if (avgPrice <= price) {
                    flightSearchVOFilterList << flightSearchResultVO
                }
            }
        } else if (flightCategory.equals('budgetFlightCategory')) {
            flightSearchVOList.each {
                FlightSearchResultVO flightSearchResultVO = it
                BigDecimal price = flightSearchResultVO.flightFare.offeredFare.toBigDecimal()
                if (avgPrice >= price) {
                    flightSearchVOFilterList << flightSearchResultVO
                }
            }
        } else if (flightCategory.equals('allFlightCategory')) {
            flightSearchVOFilterList = flightSearchVOList

        }
        render(template: "/flightTemplate/flightOneWayTemplate", model: [flightSearchVOList: flightSearchVOFilterList, total: flightSearchVOFilterList.size()])
    }


    def findFlightItinerary = {
        print('---------params---------------' + params)
        String flightSearchResultVO = params.flightSearchResultVO
        Integer modalIndex = params.modalIndex as Integer
        List<FlightSearchResultVO> flightSearchResultVOs = session["flightSearchList"] as List<FlightSearchResultVO>
        List<FlightSearchResultVO> flightSearchResultVOInbound = session["flightSearchVOListInbound"] as List<FlightSearchResultVO>
        print('---------flightSearchResultVO---------------' + flightSearchResultVO)
        FlightSearchResultVO flightSearchResultVOFound
        if (params.tripIndicator.equals("1")) {
            flightSearchResultVOs.each {
                print('1111111111111111111111111111')
                if (it.toString() == flightSearchResultVO) {
                    print('2222222222222222222222222222')
                    flightSearchResultVOFound = it
                }
            }
        } else if (params.tripIndicator.equals("2")) {
            flightSearchResultVOInbound.each {
                print('1111111111111111111111111111')
                if (it.toString() == flightSearchResultVO) {
                    print('2222222222222222222222222222')
                    flightSearchResultVOFound = it
                }
            }
        }
        print('---------flightSearchResultVOFound---------------' + flightSearchResultVOFound)
        render(view: '/flights/_flightItineraryTemplate', model: [flightSearchResultVOFound: flightSearchResultVOFound,
                                                                  indexNo                  : modalIndex, searchFor: "Itenerary"])
    }

    def findFlightSSR = {
        print('---------params------flightFareDetail---------' + params)
        String flightSearchResultVO = params.flightSearchResultVO
        Integer modalIndex = params.modalIndex as Integer
        FlightSearchVO flightSearchVO = session["flightSearchVO"] as FlightSearchVO
        List<FlightSearchResultVO> flightSearchResultVOs = session["flightSearchList"] as List<FlightSearchResultVO>
        List<FlightSearchResultVO> flightSearchResultVOInbound = session["flightSearchVOListInbound"] as List<FlightSearchResultVO>
        print('---------flightSearchResultVO---------------' + flightSearchResultVO)
        FlightSearchResultVO flightSearchResultVOFound
        if (params.tripIndicator.equals("1")) {
            print '-----------params.tripIndicator.equals("1")--------'
            flightSearchResultVOs.each {
                if (it.toString() == flightSearchResultVO) {
                    flightSearchResultVOFound = it
                }
            }
        } else if (params.tripIndicator.equals("2")) {
            print '-----------params.tripIndicator.equals("2")--------'
            flightSearchResultVOInbound.each {
                if (it.toString() == flightSearchResultVO) {
                    flightSearchResultVOFound = it
                }
            }
        }
        String sessionId = flightSearchResultVOFound?.sessionId
        if (!flightSearchVO?.journeyReturnDate) {
            print '-----------journey date not coming'
            sessionId = sessionId
        } else {
            if (params.tripIndicator.equals("2")) {
                print '-----------journey date not coming------2-'
                List<String> sessionIdList = Arrays.asList(sessionId.split(","));
                sessionId = sessionIdList.last()
            } else if (params.tripIndicator.equals("1")) {
                print '-----------journey date not coming------1-'
                List<String> sessionIdList = Arrays.asList(sessionId.split(","));
                sessionId = sessionIdList.first()
            }
        }
        print('---------flightSearchResultVOFound---------------' + flightSearchResultVOFound)
        print('------grailsApplication.config.tbo.FLIGHT_USERNAME--' + grailsApplication.config.tbo.FLIGHT_USERNAME)
        print('------grailsApplication.config.tbo.FLIGHT_PASSWORD--' + grailsApplication.config.tbo.FLIGHT_PASS)
        String airLineName = flightSearchResultVOFound?.flightSegmentVO?.airLineName?.first()
        def client = new SOAPClient("http://api.tektravels.com/tboapi_v7/service.asmx")
        SOAPResponse response = client.send("""<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <AuthenticationData xmlns="http://192.168.0.170/TT/BookingAPI">
      <SiteName></SiteName>
      <AccountCode></AccountCode>
      <UserName>${grailsApplication.config.tbo.FLIGHT_USERNAME}</UserName>
      <Password>${grailsApplication.config.tbo.FLIGHT_PASS}</Password>
    </AuthenticationData>
  </soap:Header>
  <soap:Body>
     <GetFareRule xmlns="http://192.168.0.170/TT/BookingAPI">
      <fareRuleRequest>
        <Result>
         <TripIndicator>${flightSearchResultVOFound?.tripIndicator?.toInteger()}</TripIndicator>
      <Fare>
        <BaseFare>${flightSearchResultVOFound?.flightFare?.baseFare}</BaseFare>
        <Tax>${flightSearchResultVOFound?.flightFare?.tax}</Tax>
        <ServiceTax>${flightSearchResultVOFound?.flightFare?.serviceTax}</ServiceTax>
        <AdditionalTxnFee>${flightSearchResultVOFound?.flightFare?.additionalTxnFee}</AdditionalTxnFee>
        <AgentCommission>${flightSearchResultVOFound?.flightFare?.agentCommission}</AgentCommission>
        <TdsOnCommission>${flightSearchResultVOFound?.flightFare?.tdsOnCommission}</TdsOnCommission>
        <IncentiveEarned>${flightSearchResultVOFound?.flightFare?.incentiveEarned}</IncentiveEarned>
        <TdsOnIncentive>${flightSearchResultVOFound?.flightFare?.tdsOnIncentive}</TdsOnIncentive>
        <PLBEarned>${flightSearchResultVOFound?.flightFare?.pLBEarned}</PLBEarned>
        <TdsOnPLB>${flightSearchResultVOFound?.flightFare?.tdsOnPLB}</TdsOnPLB>
        <PublishedPrice>${flightSearchResultVOFound?.flightFare?.publishedPrice}</PublishedPrice>
        <AirTransFee>${flightSearchResultVOFound?.flightFare?.airTransFee}</AirTransFee>
        <Currency>${flightSearchResultVOFound?.flightFare?.currency}</Currency>
        <Discount>${flightSearchResultVOFound?.flightFare?.discount}</Discount>
        <ChargeBU>
        </ChargeBU>
        <OtherCharges>${flightSearchResultVOFound?.flightFare?.otherCharges}</OtherCharges>
        <FuelSurcharge>${flightSearchResultVOFound?.flightFare?.fuelSurcharge}</FuelSurcharge>
        <TransactionFee>${flightSearchResultVOFound?.flightFare?.transactionFee}</TransactionFee>
        <ReverseHandlingCharge>${flightSearchResultVOFound?.flightFare?.reverseHandlingCharge}</ReverseHandlingCharge>
        <OfferedFare>${flightSearchResultVOFound?.flightFare?.offeredFare}</OfferedFare>
        <AgentServiceCharge>${flightSearchResultVOFound?.flightFare?.agentServiceCharge}</AgentServiceCharge>
      </Fare>
      <FareBreakdown>
        <WSPTCFare>
          <PassengerType>${flightSearchResultVOFound?.fareBreakDown?.passengerType?.first()}</PassengerType>
          <PassengerCount>${flightSearchResultVOFound?.fareBreakDown?.passengerCount?.first()}</PassengerCount>
          <BaseFare>${flightSearchResultVOFound?.fareBreakDown?.baseFare?.first()}</BaseFare>
          <Tax>${flightSearchResultVOFound?.fareBreakDown?.tax?.first()}</Tax>
          <AirlineTransFee>${flightSearchResultVOFound?.fareBreakDown?.airlineTransFee?.first()}</AirlineTransFee>
          <AdditionalTxnFee>${flightSearchResultVOFound?.fareBreakDown?.additionalTxnFee?.first()}</AdditionalTxnFee>
          <FuelSurcharge>${flightSearchResultVOFound?.fareBreakDown?.fuelSurcharge?.first()}</FuelSurcharge>
        </WSPTCFare>
      </FareBreakdown>
      <Origin>${flightSearchResultVOFound?.origin}</Origin>
      <Destination>${flightSearchResultVOFound?.destination}</Destination>
      <Segment>
        <WSSegment>
          <SegmentIndicator>${flightSearchResultVOFound?.flightSegmentVO?.segmentIndicator?.first()?.toInteger()}</SegmentIndicator>
          <Airline>
            <AirlineCode>${flightSearchResultVOFound?.flightSegmentVO?.airLineCode?.first()}</AirlineCode>
            <AirlineName>${flightSearchResultVOFound?.flightSegmentVO?.airLineName?.first()}</AirlineName>
          </Airline>
          <FlightNumber>${flightSearchResultVOFound?.flightSegmentVO?.flightNumber?.first()}</FlightNumber>
          <FareClass>${flightSearchResultVOFound?.flightSegmentVO?.fareClass?.first()}</FareClass>
          <Origin>
            <AirportCode>${flightSearchResultVOFound?.flightSegmentVO?.originAirPortCode?.first()}</AirportCode>
            <AirportName>${flightSearchResultVOFound?.flightSegmentVO?.originAirPortName?.first()}</AirportName>
            <Terminal />
            <CityCode>${flightSearchResultVOFound?.flightSegmentVO?.originCityCode?.first()}</CityCode>
            <CityName>${flightSearchResultVOFound?.flightSegmentVO?.originCityName?.first()}</CityName>
            <CountryCode>${flightSearchResultVOFound?.flightSegmentVO?.originCountryCode?.first()}</CountryCode>
            <CountryName>${flightSearchResultVOFound?.flightSegmentVO?.originCountryName?.first()}</CountryName>
          </Origin>
          <Destination>
            <AirportCode>${flightSearchResultVOFound?.flightSegmentVO?.destinationAirPortCode?.first()}</AirportCode>
            <AirportName>${flightSearchResultVOFound?.flightSegmentVO?.destinationAirPortName?.first()}</AirportName>
            <Terminal />
            <CityCode>${flightSearchResultVOFound?.flightSegmentVO?.destinationCityCode?.first()}</CityCode>
            <CityName>${flightSearchResultVOFound?.flightSegmentVO?.destinationCityName?.first()}</CityName>
            <CountryCode>${flightSearchResultVOFound?.flightSegmentVO?.destinationCountryCode?.first()}</CountryCode>
            <CountryName>${flightSearchResultVOFound?.flightSegmentVO?.destinationCountryName?.first()}</CountryName>
          </Destination>
          <DepTIme>${flightSearchResultVOFound?.flightSegmentVO?.departureTime?.first()}</DepTIme>
          <ArrTime>${flightSearchResultVOFound?.flightSegmentVO?.arrivalTime?.first()}</ArrTime>
          <ETicketEligible>${flightSearchResultVOFound?.flightSegmentVO?.eTicketEligible?.first()}</ETicketEligible>
          <Duration>${flightSearchResultVOFound?.flightSegmentVO?.duration?.first()}</Duration>
          <Stop>${flightSearchResultVOFound?.flightSegmentVO?.stops?.first()?.toInteger()}</Stop>
          <Craft>${flightSearchResultVOFound?.flightSegmentVO?.craftType?.first()}</Craft>
          <Status>${flightSearchResultVOFound?.flightSegmentVO?.status?.first()}</Status>
        </WSSegment>
      </Segment>
      <IbDuration />
      <ObDuration>${flightSearchResultVOFound?.obDuration}</ObDuration>
      <Source>${flightSearchResultVOFound?.flightFareRule?.source?.first()}</Source>
      <FareRule>
        <WSFareRule>
          <Origin>${flightSearchResultVOFound?.flightFareRule?.origin?.first()}</Origin>
          <Destination>${flightSearchResultVOFound?.flightFareRule?.destination?.first()}</Destination>
          <Airline>${flightSearchResultVOFound?.flightFareRule?.airline?.first()}</Airline>
          <FareRestriction />
          <FareBasisCode>${flightSearchResultVOFound?.flightFareRule?.fareBasisCode?.first()}</FareBasisCode>
          <DepartureDate>${flightSearchResultVOFound?.flightFareRule?.departureDate?.first()}</DepartureDate>
          <ReturnDate>${flightSearchResultVOFound?.flightFareRule?.returnDate?.first()}</ReturnDate>
          <Source>${flightSearchResultVOFound?.flightFareRule?.source?.first()}</Source>
        </WSFareRule>
      </FareRule>
      <IsLcc>${flightSearchResultVOFound?.isLcc}</IsLcc>
      <ObSegCount>${flightSearchResultVOFound?.flightSegmentVO?.size()?.toInteger()}</ObSegCount>
      <PromotionalPlanType>Normal</PromotionalPlanType>
      <NonRefundable>${flightSearchResultVOFound?.isRefundable}</NonRefundable>
      <SegmentKey>${flightSearchResultVOFound?.segmentKey}</SegmentKey>
        </Result>
        <SessionId>${sessionId}</SessionId>
     </fareRuleRequest>
    </GetFareRule>
  </soap:Body>
</soap:Envelope>"""
        )
        print('----------------responseresponse--------------' + response.text)
        String description = response.GetFareRuleResponse.GetFareRuleResult.FareRules.WSFareRule.FareRuleDetail
        if (description) {
        } else {
            description = 'Baggage info depends upon airlines'
        }
        print('----------------description--------------' + description)

        render(template: "/flights/flightItineraryTemplate", model: [description: description,
                                                                     indexNo    : modalIndex, searchFor: "Baggage"])
    }


    def flightFairDetail = {
        print('---------params------flightFareDetail---------' + params)
        String flightSearchResultVO = params.flightSearchResultVO
        Integer modalIndex = params.modalIndex as Integer
        FlightSearchVO flightSearchVO = session["flightSearchVO"] as FlightSearchVO
        List<FlightSearchResultVO> flightSearchResultVOs = session["flightSearchList"] as List<FlightSearchResultVO>
        List<FlightSearchResultVO> flightSearchResultVOInbound = session["flightSearchVOListInbound"] as List<FlightSearchResultVO>
        FlightSearchResultVO flightSearchResultVOFound
        if (params.tripIndicator.equals("1")) {
            flightSearchResultVOs.each {
                if (it.toString() == flightSearchResultVO) {
                    flightSearchResultVOFound = it
                }
            }
        } else if (params.tripIndicator.equals("2")) {
            flightSearchResultVOInbound.each {
                if (it.toString() == flightSearchResultVO) {
                    flightSearchResultVOFound = it
                }
            }
        }
        String sessionId = flightSearchResultVOFound?.sessionId
        if (!flightSearchVO?.journeyReturnDate) {
            sessionId = sessionId
        } else {
            if (params.tripIndicator.equals("2")) {
                List<String> sessionIdList = Arrays.asList(sessionId.split(","));
                sessionId = sessionIdList.last()
            } else if (params.tripIndicator.equals("1")) {
                List<String> sessionIdList = Arrays.asList(sessionId.split(","));
                sessionId = sessionIdList.first()
            }
        }
        String airLineName = flightSearchResultVOFound?.flightSegmentVO?.airLineName?.first()
        if (flightSearchResultVOFound?.isLcc) {
            def client = new SOAPClient("http://api.tektravels.com/tboapi_v7/service.asmx")
            SOAPResponse response = client.send("""<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <AuthenticationData xmlns="http://192.168.0.170/TT/BookingAPI">
      <SiteName></SiteName>
      <AccountCode></AccountCode>
      <UserName>${grailsApplication.config.tbo.FLIGHT_USERNAME}</UserName>
      <Password>${grailsApplication.config.tbo.FLIGHT_PASS}</Password>
    </AuthenticationData>
  </soap:Header>
  <soap:Body>
    <GetFareQuote xmlns="http://192.168.0.170/TT/BookingAPI">
      <fareQuoteRequest>
        <Result>
          <WSResult>
         <TripIndicator>${flightSearchResultVOFound?.tripIndicator?.toInteger()}</TripIndicator>
      <Fare>
        <BaseFare>${flightSearchResultVOFound?.flightFare?.baseFare}</BaseFare>
        <Tax>${flightSearchResultVOFound?.flightFare?.tax}</Tax>
        <ServiceTax>${flightSearchResultVOFound?.flightFare?.serviceTax}</ServiceTax>
        <AdditionalTxnFee>${flightSearchResultVOFound?.flightFare?.additionalTxnFee}</AdditionalTxnFee>
        <AgentCommission>${flightSearchResultVOFound?.flightFare?.agentCommission}</AgentCommission>
        <TdsOnCommission>${flightSearchResultVOFound?.flightFare?.tdsOnCommission}</TdsOnCommission>
        <IncentiveEarned>${flightSearchResultVOFound?.flightFare?.incentiveEarned}</IncentiveEarned>
        <TdsOnIncentive>${flightSearchResultVOFound?.flightFare?.tdsOnIncentive}</TdsOnIncentive>
        <PLBEarned>${flightSearchResultVOFound?.flightFare?.pLBEarned}</PLBEarned>
        <TdsOnPLB>${flightSearchResultVOFound?.flightFare?.tdsOnPLB}</TdsOnPLB>
        <PublishedPrice>${flightSearchResultVOFound?.flightFare?.publishedPrice}</PublishedPrice>
        <AirTransFee>${flightSearchResultVOFound?.flightFare?.airTransFee}</AirTransFee>
        <Currency>${flightSearchResultVOFound?.flightFare?.currency}</Currency>
        <Discount>${flightSearchResultVOFound?.flightFare?.discount}</Discount>
        <ChargeBU>
        </ChargeBU>
        <OtherCharges>${flightSearchResultVOFound?.flightFare?.otherCharges}</OtherCharges>
        <FuelSurcharge>${flightSearchResultVOFound?.flightFare?.fuelSurcharge}</FuelSurcharge>
        <TransactionFee>${flightSearchResultVOFound?.flightFare?.transactionFee}</TransactionFee>
        <ReverseHandlingCharge>${flightSearchResultVOFound?.flightFare?.reverseHandlingCharge}</ReverseHandlingCharge>
        <OfferedFare>${flightSearchResultVOFound?.flightFare?.offeredFare}</OfferedFare>
        <AgentServiceCharge>${flightSearchResultVOFound?.flightFare?.agentServiceCharge}</AgentServiceCharge>
      </Fare>
      <FareBreakdown>
        <WSPTCFare>
          <PassengerType>${flightSearchResultVOFound?.fareBreakDown?.passengerType?.first()}</PassengerType>
          <PassengerCount>${flightSearchResultVOFound?.fareBreakDown?.passengerCount?.first()}</PassengerCount>
          <BaseFare>${flightSearchResultVOFound?.fareBreakDown?.baseFare?.first()}</BaseFare>
          <Tax>${flightSearchResultVOFound?.fareBreakDown?.tax?.first()}</Tax>
          <AirlineTransFee>${flightSearchResultVOFound?.fareBreakDown?.airlineTransFee?.first()}</AirlineTransFee>
          <AdditionalTxnFee>${flightSearchResultVOFound?.fareBreakDown?.additionalTxnFee?.first()}</AdditionalTxnFee>
          <FuelSurcharge>${flightSearchResultVOFound?.fareBreakDown?.fuelSurcharge?.first()}</FuelSurcharge>
        </WSPTCFare>
      </FareBreakdown>
      <Origin>${flightSearchResultVOFound?.origin}</Origin>
      <Destination>${flightSearchResultVOFound?.destination}</Destination>
      <Segment>
        <WSSegment>
          <SegmentIndicator>${flightSearchResultVOFound?.flightSegmentVO?.segmentIndicator?.first()?.toInteger()}</SegmentIndicator>
          <Airline>
            <AirlineCode>${flightSearchResultVOFound?.flightSegmentVO?.airLineCode?.first()}</AirlineCode>
            <AirlineName>${flightSearchResultVOFound?.flightSegmentVO?.airLineName?.first()}</AirlineName>
          </Airline>
          <FlightNumber>${flightSearchResultVOFound?.flightSegmentVO?.flightNumber?.first()}</FlightNumber>
          <FareClass>${flightSearchResultVOFound?.flightSegmentVO?.fareClass?.first()}</FareClass>
          <Origin>
            <AirportCode>${flightSearchResultVOFound?.flightSegmentVO?.originAirPortCode?.first()}</AirportCode>
            <AirportName>${flightSearchResultVOFound?.flightSegmentVO?.originAirPortName?.first()}</AirportName>
            <Terminal />
            <CityCode>${flightSearchResultVOFound?.flightSegmentVO?.originCityCode?.first()}</CityCode>
            <CityName>${flightSearchResultVOFound?.flightSegmentVO?.originCityName?.first()}</CityName>
            <CountryCode>${flightSearchResultVOFound?.flightSegmentVO?.originCountryCode?.first()}</CountryCode>
            <CountryName>${flightSearchResultVOFound?.flightSegmentVO?.originCountryName?.first()}</CountryName>
          </Origin>
          <Destination>
            <AirportCode>${flightSearchResultVOFound?.flightSegmentVO?.destinationAirPortCode?.first()}</AirportCode>
            <AirportName>${flightSearchResultVOFound?.flightSegmentVO?.destinationAirPortName?.first()}</AirportName>
            <Terminal />
            <CityCode>${flightSearchResultVOFound?.flightSegmentVO?.destinationCityCode?.first()}</CityCode>
            <CityName>${flightSearchResultVOFound?.flightSegmentVO?.destinationCityName?.first()}</CityName>
            <CountryCode>${flightSearchResultVOFound?.flightSegmentVO?.destinationCountryCode?.first()}</CountryCode>
            <CountryName>${flightSearchResultVOFound?.flightSegmentVO?.destinationCountryName?.first()}</CountryName>
          </Destination>
          <DepTIme>${flightSearchResultVOFound?.flightSegmentVO?.departureTime?.first()}</DepTIme>
          <ArrTime>${flightSearchResultVOFound?.flightSegmentVO?.arrivalTime?.first()}</ArrTime>
          <ETicketEligible>${flightSearchResultVOFound?.flightSegmentVO?.eTicketEligible?.first()}</ETicketEligible>
          <Duration>${flightSearchResultVOFound?.flightSegmentVO?.duration?.first()}</Duration>
          <Stop>${flightSearchResultVOFound?.flightSegmentVO?.stops?.first()?.toInteger()}</Stop>
          <Craft>${flightSearchResultVOFound?.flightSegmentVO?.craftType?.first()}</Craft>
          <Status>${flightSearchResultVOFound?.flightSegmentVO?.status?.first()}</Status>
        </WSSegment>
      </Segment>
      <IbDuration />
      <ObDuration>${flightSearchResultVOFound?.obDuration}</ObDuration>
      <Source>${flightSearchResultVOFound?.flightFareRule?.source?.first()}</Source>
      <FareRule>
        <WSFareRule>
          <Origin>${flightSearchResultVOFound?.flightFareRule?.origin?.first()}</Origin>
          <Destination>${flightSearchResultVOFound?.flightFareRule?.destination?.first()}</Destination>
          <Airline>${flightSearchResultVOFound?.flightFareRule?.airline?.first()}</Airline>
          <FareRestriction />
          <FareBasisCode>${flightSearchResultVOFound?.flightFareRule?.fareBasisCode?.first()}</FareBasisCode>
          <DepartureDate>${flightSearchResultVOFound?.flightFareRule?.departureDate?.first()}</DepartureDate>
          <ReturnDate>${flightSearchResultVOFound?.flightFareRule?.returnDate?.first()}</ReturnDate>
          <Source>${flightSearchResultVOFound?.flightFareRule?.source?.first()}</Source>
        </WSFareRule>
      </FareRule>
      <IsLcc>${flightSearchResultVOFound?.isLcc}</IsLcc>
      <ObSegCount>${flightSearchResultVOFound?.flightSegmentVO?.size()?.toInteger()}</ObSegCount>
      <PromotionalPlanType>Normal</PromotionalPlanType>
      <NonRefundable>${flightSearchResultVOFound?.isRefundable}</NonRefundable>
      <SegmentKey>${flightSearchResultVOFound?.segmentKey}</SegmentKey>
         </WSResult>
        </Result>
        <SessionId>${sessionId}</SessionId>
      </fareQuoteRequest>
    </GetFareQuote>
  </soap:Body>
</soap:Envelope>"""
            )
            print('-----------------soapResponse-----11111 22222   ----------' + response.text)
            def flightFareResult = new XmlSlurper().parseText(response.text)

            flightSearchResultVOFound.flightFare.baseFare = flightFareResult.children().GetFareQuoteResponse.GetFareQuoteResult.Result.Fare.BaseFare
            flightSearchResultVOFound.flightFare.tax = flightFareResult.children().GetFareQuoteResponse.GetFareQuoteResult.Result.Fare.Tax
            flightSearchResultVOFound.flightFare.serviceTax = flightFareResult.children().GetFareQuoteResponse.GetFareQuoteResult.Result.Fare.ServiceTax
            flightSearchResultVOFound.flightFare.additionalTxnFee = flightFareResult.children().GetFareQuoteResponse.GetFareQuoteResult.Result.Fare.AdditionalTxnFee
            flightSearchResultVOFound.flightFare.agentCommission = flightFareResult.children().GetFareQuoteResponse.GetFareQuoteResult.Result.Fare.AgentCommission
            flightSearchResultVOFound.flightFare.tdsOnCommission = flightFareResult.children().GetFareQuoteResponse.GetFareQuoteResult.Result.Fare.TdsOnCommission
            flightSearchResultVOFound.flightFare.incentiveEarned = flightFareResult.children().GetFareQuoteResponse.GetFareQuoteResult.Result.Fare.IncentiveEarned
            flightSearchResultVOFound.flightFare.tdsOnIncentive = flightFareResult.children().GetFareQuoteResponse.GetFareQuoteResult.Result.Fare.TdsOnIncentive
            flightSearchResultVOFound.flightFare.pLBEarned = flightFareResult.children().GetFareQuoteResponse.GetFareQuoteResult.Result.Fare.PLBEarned
            flightSearchResultVOFound.flightFare.tdsOnPLB = flightFareResult.children().GetFareQuoteResponse.GetFareQuoteResult.Result.Fare.TdsOnPLB
            flightSearchResultVOFound.flightFare.publishedPrice = flightFareResult.children().GetFareQuoteResponse.GetFareQuoteResult.Result.Fare.PublishedPrice
            flightSearchResultVOFound.flightFare.airTransFee = flightFareResult.children().GetFareQuoteResponse.GetFareQuoteResult.Result.Fare.AirTransFee
            flightSearchResultVOFound.flightFare.currency = flightFareResult.children().GetFareQuoteResponse.GetFareQuoteResult.Result.Fare.Currency
            flightSearchResultVOFound.flightFare.discount = flightFareResult.children().GetFareQuoteResponse.GetFareQuoteResult.Result.Fare.Discount
            flightSearchResultVOFound.flightFare.otherCharges = flightFareResult.children().GetFareQuoteResponse.GetFareQuoteResult.Result.Fare.OtherCharges
            flightSearchResultVOFound.flightFare.fuelSurcharge = flightFareResult.children().GetFareQuoteResponse.GetFareQuoteResult.Result.Fare.FuelSurcharge
            flightSearchResultVOFound.flightFare.transactionFee = flightFareResult.children().GetFareQuoteResponse.GetFareQuoteResult.Result.Fare.TransactionFee
            flightSearchResultVOFound.flightFare.reverseHandlingCharge = flightFareResult.children().GetFareQuoteResponse.GetFareQuoteResult.Result.Fare.ReverseHandlingCharge
            flightSearchResultVOFound.flightFare.offeredFare = flightFareResult.children().GetFareQuoteResponse.GetFareQuoteResult.Result.Fare.OfferedFare
            flightSearchResultVOFound.flightFare.agentServiceCharge = flightFareResult.children().GetFareQuoteResponse.GetFareQuoteResult.Result.Fare.AgentServiceCharge
            flightSearchResultVOFound.flightFare.agentConvienceCharges = flightFareResult.children().GetFareQuoteResponse.GetFareQuoteResult.Result.Fare.AgentConvienceCharges
        }
        render(template: "/flights/flightItineraryTemplate", model: [flightSearchResultVO: flightSearchResultVOFound,
                                                                     indexNo             : modalIndex, searchFor: "FareRule"])
    }

    def searchFlightForFilter = {
        print '------------' + params
        Integer max = params.max ? params.int('max') : 10
        Integer offset = params.offset ? params.int('offset') : 0
        List<String> checkedNoOfStop = params.list('checkedNoOfStop[]') as List<String>
        List<String> checkedAirlineNames = params.list('checkedAirlineNames[]') as List<String>
        List<String> checkedBudgetRangeFilter = params.list('checkedBudgetRangeFilter[]') as List<String>

        List<FlightSearchResultVO> flightSearchVOList = []
        List<FlightSearchResultVO> flightSearchVOListPaginated = []
        List<FlightSearchResultVO> flightSearchVOListInbound = []
        List<FlightSearchResultVO> flightSearchVOListPaginatedInbound = []
        List<FlightSearchResultVO> flightSearchVOFilterList = []

        flightSearchVOList = session["flightSearchList"] as List
        FlightSearchVO flightSearchVO = session["flightSearchVO"] as FlightSearchVO

//        flightSearchVOListInbound = session["flightSearchVOListInbound"] as List
        List<String> sliderPriceRange = params.list('sliderPriceRange[]')
        String minPrice, maxPrice;
        minPrice = sliderPriceRange.first()
        print 'min' + minPrice
        maxPrice = sliderPriceRange.last()
        print 'max' + maxPrice
        if (flightSearchVO.journeyReturnDate) {
        } else {
            if (checkedNoOfStop || checkedAirlineNames || checkedBudgetRangeFilter) {
                if (checkedNoOfStop) {
                    print '----no of stop------'
                    checkedNoOfStop.each { filter ->
                        if (filter.equals('1')) {
                            flightSearchVOList.each { flight ->
                                print '-------flight.flightSegmentVO.stop-------' + flight.flightSegmentVO.size()
                                if (flight.flightSegmentVO.size() - 1 == 1) {
                                    flightSearchVOFilterList << flight
                                }
                            }
                        } else if (filter.equals('2')) {
                            flightSearchVOList.each { flight ->
                                print '-------flight.flightSegmentVO.stop-------' + flight.flightSegmentVO.size()
                                if (flight.flightSegmentVO.size() - 1 == 2) {
                                    flightSearchVOFilterList << flight
                                }
                            }
                        } else if (filter.equals('3')) {
                            flightSearchVOList.each { flight ->
                                print '-------flight.flightSegmentVO.stop-------' + flight.flightSegmentVO.size()
                                if (flight.flightSegmentVO.size() - 1 == 3) {
                                    flightSearchVOFilterList << flight
                                }
                            }
                        } else if (filter.equals('4')) {
                            flightSearchVOList.each { flight ->
                                print '-------flight.flightSegmentVO.stop-------' + flight.flightSegmentVO.size()
                                if (flight.flightSegmentVO.size() - 1 == 4) {
                                    flightSearchVOFilterList << flight
                                }
                            }
                        } else if (filter.equals('5')) {
                            flightSearchVOList.each { flight ->
                                print '-------flight.flightSegmentVO.stop-------' + flight.flightSegmentVO.size()
                                if (flight.flightSegmentVO.size() - 1 == 5) {
                                    flightSearchVOFilterList << flight
                                }
                            }
                        }
                    }
                }
                if (checkedAirlineNames) {
                    print '----airline name------'
                    checkedAirlineNames.each {
                        String airlineName = it
                        print '----airline name------' + airlineName
                        flightSearchVOList.each {
                            FlightSearchResultVO flightSearchResultVO = it
                            List<String> nameList = flightSearchResultVO?.flightSegmentVO?.airLineName
                            String offeredFare = flightSearchResultVO.flightFare.offeredFare
                            if (airlineName in nameList && ((minPrice.toBigDecimal() <= offeredFare.toBigDecimal()) && (maxPrice.toBigDecimal() >= offeredFare.toBigDecimal()))) {
                                print '-----added-----'
                                flightSearchVOFilterList << flightSearchResultVO
                            }
                        }
                    }
                }
                if (checkedBudgetRangeFilter) {
                    print '----checkedBudgetRangeFilter------'
                    checkedBudgetRangeFilter.each {
                        String budgetRange = it
                        flightSearchVOList.each {
                            FlightSearchResultVO flightSearchResultVO = it
                            String offerFare = flightSearchResultVO.flightFare?.offeredFare
                            print '-------offerFare---------' + offerFare
                            if (offerFare.toBigDecimal() <= budgetRange.toBigDecimal()) {
                                flightSearchVOFilterList << flightSearchResultVO
                            }
                        }
                    }
                }
            } else (params.'sliderPriceRange[]') {
                print '----sliderPriceRange------'
                flightSearchVOList.each {
                    FlightSearchResultVO flightSearchResultVO = it
                    String offerFare = flightSearchResultVO.flightFare?.offeredFare
                    if (offerFare.toBigDecimal() >= minPrice.toBigDecimal() && offerFare.toBigDecimal() <= maxPrice.toBigDecimal()) {
                        flightSearchVOFilterList << flightSearchResultVO
                    }
                }
            }
        }

//        flightSearchVOListPaginated = flightSearchVOFilterList ? flightSearchVOFilterList.subList(offset, (offset + max) > flightSearchVOFilterList.size() ? flightSearchVOFilterList.size() : offset + max) : []
        print '-----flightSearchVOListPaginated-------' + flightSearchVOFilterList
        render(template: "/flightTemplate/flightOneWayTemplate", model: [flightSearchVOList: flightSearchVOFilterList, total: flightSearchVOFilterList.size()])
    }


    def addOccupantForFlight = {
        print('------------params-------------------' + params)
        FlightSearchVO flightSearchVO = session["flightSearchVO"] as FlightSearchVO
        print('============flightSearchVO===========' + flightSearchVO)
        List<FlightSearchResultVO> flightSearchVOList = session["flightSearchList"] as List<FlightSearchResultVO>
        List<FlightSearchResultVO> flightSearchVOListInBound = session["flightSearchVOListInbound"] as List<FlightSearchResultVO>
        String flightSearchResult = params.flightInstanceId
        print('============flightSearchResult===========' + flightSearchResult)
        List<FlightSearchResultVO> flightForBooking = []
        String outBoundFlight = params.outBoundFlight
        print '------outBoundFlight------' + outBoundFlight
        String inBoundFlight = params.inBoundFlight
        print '------inBoundFlight------' + inBoundFlight
        if (flightSearchVO.journeyReturnDate) {
            flightSearchVOList.each { FlightSearchResultVO flightSearchResultVO1 ->
                if (flightSearchResultVO1.toString().equals(outBoundFlight)) {
                    print('outbound')
                    flightForBooking << flightSearchResultVO1
                }
            }
            flightSearchVOListInBound.each { FlightSearchResultVO flightSearchResultVO1 ->
                if (flightSearchResultVO1.toString().equals(inBoundFlight)) {
                    print('inbound')
                    flightForBooking << flightSearchResultVO1
                }
            }
        } else {
            flightSearchVOList.each { FlightSearchResultVO flightSearchResultVO1 ->
                if (flightSearchResultVO1.toString().equals(flightSearchResult)) {
                    flightForBooking << flightSearchResultVO1
                }
            }
        }
        print('----------------flightForBooking----------------' + flightForBooking)
        render(view: "/tboApi/flightAddOccupantDetail", model: [flightForBooking: flightForBooking, flightSearchVo: flightSearchVO])

    }

    def bookTicket = {
        print('---------------params----------------------' + params)
        List<FlightSearchResultVO> flightSearchVOList = session["flightSearchList"] as List<FlightSearchResultVO>
        List<FlightSearchResultVO> flightSearchVOListInBound = session["flightSearchVOListInbound"] as List<FlightSearchResultVO>
        List flightForBookingList = params.list('flightSearchResult')

        List flightSearchResultVOList = []
        FlightSearchVO flightSearchVO = session["flightSearchVO"] as FlightSearchVO

        String userName = params.adult1Email
        User newUser = User.findByUsername(userName)
        Role role = Role.findByAuthority('ROLE_USER')
        String pass
        if (newUser) {
        } else {
            newUser = new User()
            newUser.firstName = params.adult1FName
            newUser.lastName = params.adult1LName
            newUser.username = params.adult1Email
            newUser.mobNo = params.adult1Phone
            String randomAlphanumeric = RandomStringUtils.randomAlphanumeric(8)
            Date today = new Date()
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat('dd/MM/yyyy')
            String[] date = simpleDateFormat.format(today).split('/')
            newUser.coustId = "TR" + "CS" + randomAlphanumeric.toUpperCase() + "${date[0]}" + "${date[1]}" + "${date[2]}"
            newUser.accountExpired = false
            newUser.accountLocked = false
            newUser.hasVerifiedEmail = true
            newUser.passwordExpired = false
            newUser.enabled = true
            newUser.pictureName = "picNotAvailable"
            newUser.pictureType = "jpeg"
            pass = RandomStringUtils.randomAlphanumeric(8)
            print('-------------- user pass is--------------------' + pass)
            newUser.password = pass
            newUser.save(flush: true)
            UserRole.create(newUser, role, true)
        }
        BookedFlight bookedFlight
        String description
        def client = new SOAPClient("http://api.tektravels.com/tboapi_v7/service.asmx")
        if (!flightSearchVO?.journeyReturnDate) {
            FlightSearchResultVO flightSearchResultVO
            String flightVar = flightForBookingList.get(0).replace("[", '').replace("]", '')
            print 'flightSearchResultVOList' + flightForBookingList.get(0).replace("[", '').replace("]", '')
            flightSearchVOList.each { flight ->
                print '--------one way lcc-----------' + flight
                if (flight.toString() == flightVar) {
                    flightSearchResultVO = flight
                    print('-----------------flightSearchResultVO-----------' + flightSearchResultVO)
                }
            }
            def fareQuotedResult = client.send("""<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <AuthenticationData xmlns="http://192.168.0.170/TT/BookingAPI">
      <SiteName></SiteName>
      <AccountCode></AccountCode>
      <UserName>${grailsApplication.config.tbo.FLIGHT_USERNAME}</UserName>
      <Password>${grailsApplication.config.tbo.FLIGHT_PASS}</Password>
    </AuthenticationData>
  </soap:Header>
  <soap:Body>
    <GetFareQuote xmlns="http://192.168.0.170/TT/BookingAPI">
      <fareQuoteRequest>
        <Result>
          <WSResult>
         <TripIndicator>${flightSearchResultVO?.tripIndicator?.toInteger()}</TripIndicator>
      <Fare>
        <BaseFare>${flightSearchResultVO?.flightFare?.baseFare}</BaseFare>
        <Tax>${flightSearchResultVO?.flightFare?.tax}</Tax>
        <ServiceTax>${flightSearchResultVO?.flightFare?.serviceTax}</ServiceTax>
        <AdditionalTxnFee>${flightSearchResultVO?.flightFare?.additionalTxnFee}</AdditionalTxnFee>
        <AgentCommission>${flightSearchResultVO?.flightFare?.agentCommission}</AgentCommission>
        <TdsOnCommission>${flightSearchResultVO?.flightFare?.tdsOnCommission}</TdsOnCommission>
        <IncentiveEarned>${flightSearchResultVO?.flightFare?.incentiveEarned}</IncentiveEarned>
        <TdsOnIncentive>${flightSearchResultVO?.flightFare?.tdsOnIncentive}</TdsOnIncentive>
        <PLBEarned>${flightSearchResultVO?.flightFare?.pLBEarned}</PLBEarned>
        <TdsOnPLB>${flightSearchResultVO?.flightFare?.tdsOnPLB}</TdsOnPLB>
        <PublishedPrice>${flightSearchResultVO?.flightFare?.publishedPrice}</PublishedPrice>
        <AirTransFee>${flightSearchResultVO?.flightFare?.airTransFee}</AirTransFee>
        <Currency>${flightSearchResultVO?.flightFare?.currency}</Currency>
        <Discount>${flightSearchResultVO?.flightFare?.discount}</Discount>
        <ChargeBU>
        </ChargeBU>
        <OtherCharges>${flightSearchResultVO?.flightFare?.otherCharges}</OtherCharges>
        <FuelSurcharge>${flightSearchResultVO?.flightFare?.fuelSurcharge}</FuelSurcharge>
        <TransactionFee>${flightSearchResultVO?.flightFare?.transactionFee}</TransactionFee>
        <ReverseHandlingCharge>${flightSearchResultVO?.flightFare?.reverseHandlingCharge}</ReverseHandlingCharge>
        <OfferedFare>${flightSearchResultVO?.flightFare?.offeredFare}</OfferedFare>
        <AgentServiceCharge>${flightSearchResultVO?.flightFare?.agentServiceCharge}</AgentServiceCharge>
      </Fare>
      <FareBreakdown>
        <WSPTCFare>
          <PassengerType>${flightSearchResultVO?.fareBreakDown?.passengerType?.first()}</PassengerType>
          <PassengerCount>${flightSearchResultVO?.fareBreakDown?.passengerCount?.first()}</PassengerCount>
          <BaseFare>${flightSearchResultVO?.fareBreakDown?.baseFare?.first()}</BaseFare>
          <Tax>${flightSearchResultVO?.fareBreakDown?.tax?.first()}</Tax>
          <AirlineTransFee>${flightSearchResultVO?.fareBreakDown?.airlineTransFee?.first()}</AirlineTransFee>
          <AdditionalTxnFee>${flightSearchResultVO?.fareBreakDown?.additionalTxnFee?.first()}</AdditionalTxnFee>
          <FuelSurcharge>${flightSearchResultVO?.fareBreakDown?.fuelSurcharge?.first()}</FuelSurcharge>
        </WSPTCFare>
      </FareBreakdown>
      <Origin>${flightSearchResultVO?.origin}</Origin>
      <Destination>${flightSearchResultVO?.destination}</Destination>
      <Segment>
        <WSSegment>
          <SegmentIndicator>${flightSearchResultVO?.flightSegmentVO?.segmentIndicator?.first()?.toInteger()}</SegmentIndicator>
          <Airline>
            <AirlineCode>${flightSearchResultVO?.flightSegmentVO?.airLineCode?.first()}</AirlineCode>
            <AirlineName>${flightSearchResultVO?.flightSegmentVO?.airLineName?.first()}</AirlineName>
          </Airline>
          <FlightNumber>${flightSearchResultVO?.flightSegmentVO?.flightNumber?.first()}</FlightNumber>
          <FareClass>${flightSearchResultVO?.flightSegmentVO?.fareClass?.first()}</FareClass>
          <Origin>
            <AirportCode>${flightSearchResultVO?.flightSegmentVO?.originAirPortCode?.first()}</AirportCode>
            <AirportName>${flightSearchResultVO?.flightSegmentVO?.originAirPortName?.first()}</AirportName>
            <Terminal />
            <CityCode>${flightSearchResultVO?.flightSegmentVO?.originCityCode?.first()}</CityCode>
            <CityName>${flightSearchResultVO?.flightSegmentVO?.originCityName?.first()}</CityName>
            <CountryCode>${flightSearchResultVO?.flightSegmentVO?.originCountryCode?.first()}</CountryCode>
            <CountryName>${flightSearchResultVO?.flightSegmentVO?.originCountryName?.first()}</CountryName>
          </Origin>
          <Destination>
            <AirportCode>${flightSearchResultVO?.flightSegmentVO?.destinationAirPortCode?.first()}</AirportCode>
            <AirportName>${flightSearchResultVO?.flightSegmentVO?.destinationAirPortName?.first()}</AirportName>
            <Terminal />
            <CityCode>${flightSearchResultVO?.flightSegmentVO?.destinationCityCode?.first()}</CityCode>
            <CityName>${flightSearchResultVO?.flightSegmentVO?.destinationCityName?.first()}</CityName>
            <CountryCode>${flightSearchResultVO?.flightSegmentVO?.destinationCountryCode?.first()}</CountryCode>
            <CountryName>${flightSearchResultVO?.flightSegmentVO?.destinationCountryName?.first()}</CountryName>
          </Destination>
          <DepTIme>${flightSearchResultVO?.flightSegmentVO?.departureTime?.first()}</DepTIme>
          <ArrTime>${flightSearchResultVO?.flightSegmentVO?.arrivalTime?.first()}</ArrTime>
          <ETicketEligible>${flightSearchResultVO?.flightSegmentVO?.eTicketEligible?.first()}</ETicketEligible>
          <Duration>${flightSearchResultVO?.flightSegmentVO?.duration?.first()}</Duration>
          <Stop>${flightSearchResultVO?.flightSegmentVO?.stops?.first()?.toInteger()}</Stop>
          <Craft>${flightSearchResultVO?.flightSegmentVO?.craftType?.first()}</Craft>
          <Status>${flightSearchResultVO?.flightSegmentVO?.status?.first()}</Status>
        </WSSegment>
      </Segment>
      <IbDuration />
      <ObDuration>${flightSearchResultVO?.obDuration}</ObDuration>
      <Source>${flightSearchResultVO?.flightFareRule?.source?.first()}</Source>
      <FareRule>
        <WSFareRule>
          <Origin>${flightSearchResultVO?.flightFareRule?.origin?.first()}</Origin>
          <Destination>${flightSearchResultVO?.flightFareRule?.destination?.first()}</Destination>
          <Airline>${flightSearchResultVO?.flightFareRule?.airline?.first()}</Airline>
          <FareRestriction />
          <FareBasisCode>${flightSearchResultVO?.flightFareRule?.fareBasisCode?.first()}</FareBasisCode>
          <DepartureDate>${flightSearchResultVO?.flightFareRule?.departureDate?.first()}</DepartureDate>
          <ReturnDate>${flightSearchResultVO?.flightFareRule?.returnDate?.first()}</ReturnDate>
          <Source>${flightSearchResultVO?.flightFareRule?.source?.first()}</Source>
        </WSFareRule>
      </FareRule>
      <IsLcc>${flightSearchResultVO?.isLcc}</IsLcc>
      <ObSegCount>${flightSearchResultVO?.flightSegmentVO?.size()?.toInteger()}</ObSegCount>
      <PromotionalPlanType>Normal</PromotionalPlanType>
      <NonRefundable>${flightSearchResultVO?.isRefundable}</NonRefundable>
      <SegmentKey>${flightSearchResultVO?.segmentKey}</SegmentKey>
         </WSResult>
        </Result>
        <SessionId>${flightSearchResultVO?.sessionId}</SessionId>
      </fareQuoteRequest>
    </GetFareQuote>
  </soap:Body>
</soap:Envelope>""")
            print '----------------fareQuotedResult------------------' + fareQuotedResult.text

            SOAPResponse response = client.send("""<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <AuthenticationData xmlns="http://192.168.0.170/TT/BookingAPI">
      <SiteName></SiteName>
      <AccountCode></AccountCode>
      <UserName>${grailsApplication.config.tbo.FLIGHT_USERNAME}</UserName>
      <Password>${grailsApplication.config.tbo.FLIGHT_PASS}</Password>
    </AuthenticationData>
  </soap:Header>
  <soap:Body>
    <Ticket xmlns="http://192.168.0.170/TT/BookingAPI">
      <wsTicketRequest>
        <BookingID></BookingID>
        <Origin>${flightSearchResultVO?.flightFareRule?.origin?.first()}</Origin>
        <Destination>${flightSearchResultVO?.flightFareRule?.destination?.first()}</Destination>
        <Segment>
          <WSSegment>
            <SegmentIndicator>${flightSearchResultVO?.flightSegmentVO?.segmentIndicator?.first()?.toInteger()}</SegmentIndicator>
            <Airline>
            <AirlineCode>${flightSearchResultVO?.flightSegmentVO?.airLineCode?.first()}</AirlineCode>
            <AirlineName>${flightSearchResultVO?.flightSegmentVO?.airLineName?.first()}</AirlineName>
            </Airline>
            <FlightNumber>${flightSearchResultVO?.flightSegmentVO?.flightNumber?.first()}</FlightNumber>
            <FareClass>${flightSearchResultVO?.flightSegmentVO?.fareClass?.first()}</FareClass>
            <AirlinePNR>string</AirlinePNR>
            <Origin>
                <AirportCode>${flightSearchResultVO?.flightSegmentVO?.originAirPortCode?.first()}</AirportCode>
                <AirportName>${flightSearchResultVO?.flightSegmentVO?.originAirPortName?.first()}</AirportName>
                <Terminal />
                <CityCode>${flightSearchResultVO?.flightSegmentVO?.originCityCode?.first()}</CityCode>
                <CityName>${flightSearchResultVO?.flightSegmentVO?.originCityName?.first()}</CityName>
                <CountryCode>${flightSearchResultVO?.flightSegmentVO?.originCountryCode?.first()}</CountryCode>
                <CountryName>${flightSearchResultVO?.flightSegmentVO?.originCountryName?.first()}</CountryName>
            </Origin>
            <Destination>
                <AirportCode>${flightSearchResultVO?.flightSegmentVO?.destinationAirPortCode?.first()}</AirportCode>
                <AirportName>${flightSearchResultVO?.flightSegmentVO?.destinationAirPortName?.first()}</AirportName>
                <Terminal />
                <CityCode>${flightSearchResultVO?.flightSegmentVO?.destinationCityCode?.first()}</CityCode>
                <CityName>${flightSearchResultVO?.flightSegmentVO?.destinationCityName?.first()}</CityName>
                <CountryCode>${flightSearchResultVO?.flightSegmentVO?.destinationCountryCode?.first()}</CountryCode>
                <CountryName>${flightSearchResultVO?.flightSegmentVO?.destinationCountryName?.first()}</CountryName>
            </Destination>
            <DepTIme>${flightSearchResultVO?.flightSegmentVO?.departureTime?.first()}</DepTIme>
            <ArrTime>${flightSearchResultVO?.flightSegmentVO?.arrivalTime?.first()}</ArrTime>
            <ETicketEligible>${flightSearchResultVO?.flightSegmentVO?.eTicketEligible?.first()}</ETicketEligible>
            <Duration>${flightSearchResultVO?.flightSegmentVO?.duration?.first()}</Duration>
            <Stop>${flightSearchResultVO?.flightSegmentVO?.stops?.first()?.toInteger()}</Stop>
            <Craft>${flightSearchResultVO?.flightSegmentVO?.craftType?.first()}</Craft>
            <Status>${flightSearchResultVO?.flightSegmentVO?.status?.first()}</Status>
            <OperatingCarrier>string</OperatingCarrier>
          </WSSegment>
        </Segment>
        <FareType>string</FareType>
        <FareRule>
          <WSFareRule>
            <Origin>${flightSearchResultVO?.flightFareRule?.origin?.first()}</Origin>
            <Destination>${flightSearchResultVO?.flightFareRule?.destination?.first()}</Destination>
            <Airline>${flightSearchResultVO?.flightFareRule?.airline?.first()}</Airline>
            <FareRestriction>${flightSearchResultVO?.flightFareRule?.fareRestriction?.first()}</FareRestriction>
            <FareBasisCode>${flightSearchResultVO?.flightFareRule?.fareBasisCode?.first()}</FareBasisCode>
            <FareRuleDetail></FareRuleDetail>
            <DepartureDate>${flightSearchResultVO?.flightFareRule?.departureDate?.first()}</DepartureDate>
            <ReturnDate>${flightSearchResultVO?.flightFareRule?.returnDate?.first()}</ReturnDate>
            <Source>${flightSearchResultVO?.flightFareRule?.source?.first()}</Source>
          </WSFareRule>
        </FareRule>
        <Fare>
          <BaseFare>${flightSearchResultVO?.flightFare?.baseFare}</BaseFare>
          <Tax>${flightSearchResultVO?.flightFare?.tax}</Tax>
          <ServiceTax>${flightSearchResultVO?.flightFare?.serviceTax}</ServiceTax>
          <AdditionalTxnFee>${flightSearchResultVO?.flightFare?.additionalTxnFee}</AdditionalTxnFee>
          <AgentCommission>${flightSearchResultVO?.flightFare?.agentCommission}</AgentCommission>
          <TdsOnCommission>${flightSearchResultVO?.flightFare?.tdsOnCommission}</TdsOnCommission>
          <IncentiveEarned>${flightSearchResultVO?.flightFare?.incentiveEarned}</IncentiveEarned>
          <TdsOnIncentive>${flightSearchResultVO?.flightFare?.tdsOnIncentive}</TdsOnIncentive>
          <PLBEarned>${flightSearchResultVO?.flightFare?.pLBEarned}</PLBEarned>
          <TdsOnPLB>${flightSearchResultVO?.flightFare?.tdsOnPLB}</TdsOnPLB>
          <PublishedPrice>${flightSearchResultVO?.flightFare?.publishedPrice}</PublishedPrice>
          <AirTransFee>${flightSearchResultVO?.flightFare?.airTransFee}</AirTransFee>
          <Currency>${flightSearchResultVO?.flightFare?.currency}</Currency>
          <Discount>${flightSearchResultVO?.flightFare?.discount}</Discount>
          <ChargeBU>
            <ChargeBreakUp xsi:nil="true" />
            <ChargeBreakUp xsi:nil="true" />
          </ChargeBU>
          <OtherCharges>${flightSearchResultVO?.flightFare?.otherCharges}</OtherCharges>
          <FuelSurcharge>${flightSearchResultVO?.flightFare?.fuelSurcharge}</FuelSurcharge>
          <TransactionFee>${flightSearchResultVO?.flightFare?.transactionFee}</TransactionFee>
          <ReverseHandlingCharge>${flightSearchResultVO?.flightFare?.reverseHandlingCharge}</ReverseHandlingCharge>
          <OfferedFare>${flightSearchResultVO?.flightFare?.offeredFare}</OfferedFare>
          <AgentServiceCharge>${flightSearchResultVO?.flightFare?.agentServiceCharge}</AgentServiceCharge>
          <AgentConvienceCharges>${flightSearchResultVO?.flightFare?.agentConvienceCharges}</AgentConvienceCharges>
        </Fare>
        <Passenger>
          <WSPassenger>
            <Title>Mr</Title>
            <FirstName>Anil</FirstName>
            <LastName>Kumar</LastName>
            <Type>Adult</Type>
            <DateOfBirth>1990-08-16</DateOfBirth>
            <Fare>
                <BaseFare>${flightSearchResultVO?.flightFare?.baseFare}</BaseFare>
                <Tax>${flightSearchResultVO?.flightFare?.tax}</Tax>
                <ServiceTax>${flightSearchResultVO?.flightFare?.serviceTax}</ServiceTax>
                <AdditionalTxnFee>${flightSearchResultVO?.flightFare?.additionalTxnFee}</AdditionalTxnFee>
                <AgentCommission>${flightSearchResultVO?.flightFare?.agentCommission}</AgentCommission>
                <TdsOnCommission>${flightSearchResultVO?.flightFare?.tdsOnCommission}</TdsOnCommission>
                <IncentiveEarned>${flightSearchResultVO?.flightFare?.incentiveEarned}</IncentiveEarned>
                <TdsOnIncentive>${flightSearchResultVO?.flightFare?.tdsOnIncentive}</TdsOnIncentive>
                <PLBEarned>${flightSearchResultVO?.flightFare?.pLBEarned}</PLBEarned>
                <TdsOnPLB>${flightSearchResultVO?.flightFare?.tdsOnPLB}</TdsOnPLB>
                <PublishedPrice>${flightSearchResultVO?.flightFare?.publishedPrice}</PublishedPrice>
                <AirTransFee>${flightSearchResultVO?.flightFare?.airTransFee}</AirTransFee>
                <Discount>${flightSearchResultVO?.flightFare?.discount}</Discount>
                <OtherCharges>${flightSearchResultVO?.flightFare?.otherCharges}</OtherCharges>
                <FuelSurcharge>${flightSearchResultVO?.flightFare?.fuelSurcharge}</FuelSurcharge>
                <TransactionFee>${flightSearchResultVO?.flightFare?.transactionFee}</TransactionFee>
                <ReverseHandlingCharge>${flightSearchResultVO?.flightFare?.reverseHandlingCharge}</ReverseHandlingCharge>
                <OfferedFare>${flightSearchResultVO?.flightFare?.offeredFare}</OfferedFare>
                <AgentServiceCharge>${flightSearchResultVO?.flightFare?.agentServiceCharge}</AgentServiceCharge>
            </Fare>
            <Ssr xsi:nil="true" />
            <Gender>Male</Gender>
            <PassportNumber>string</PassportNumber>
            <PassportExpiry>2020-01-01</PassportExpiry>
            <PinCode>string</PinCode>
            <Country>IN</Country>
            <Phone>8377001072</Phone>
            <AddressLine1>abc,karnal</AddressLine1>
            <Email>samverma0001@gmail.com</Email>
          </WSPassenger>
        </Passenger>
        <Remarks>Flight Ticket</Remarks>
        <InstantTicket>true</InstantTicket>
        <PaymentInformation>
          <PaymentInformationId>0</PaymentInformationId>
          <InvoiceNumber>0</InvoiceNumber>
          <PaymentId/>
          <Amount>${flightSearchResultVO?.flightFare?.publishedPrice}</Amount>
          <IPAddress/>
          <TrackId>0</TrackId>
          <PaymentGateway>APICustomer</PaymentGateway>
          <PaymentModeType>Deposited</PaymentModeType>
        </PaymentInformation>
        <Source>${flightSearchResultVO?.flightFareRule?.source?.first()}</Source>
        <SessionId>${flightSearchResultVO?.sessionId}</SessionId>
        <IsOneWayBooking>true</IsOneWayBooking>
        <PromotionalPlanType>Normal</PromotionalPlanType>
      </wsTicketRequest>
    </Ticket>
  </soap:Body>
</soap:Envelope>"""
            )
            print('--------before return------------response----------------' + response.text)
            description = response.TicketResponse.TicketResult.Status.Description
            String statusCode = response.TicketResponse.TicketResult.Status.StatusCode

            if (statusCode.equals('14')) {
                bookedFlight = new BookedFlight()
                bookedFlight.pnr = response.TicketResponse.TicketResult.PNR
                bookedFlight.refId = response.TicketResponse.TicketResult.RefId
                bookedFlight.bookingId = response.TicketResponse.TicketResult.BookingId
                bookedFlight.user = newUser
                bookedFlight.save(flush: true, failOnError: true)
                newUser.addToBookedFlights(bookedFlight)

                SOAPResponse addBookingDetailResponse = client.send("""<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
            <soap:Header>
                <AuthenticationData xmlns="http://192.168.0.170/TT/BookingAPI">
                <SiteName></SiteName>
                <AccountCode></AccountCode>
                <UserName>${grailsApplication.config.tbo.FLIGHT_USERNAME}</UserName>
                <Password>${grailsApplication.config.tbo.FLIGHT_PASS}</Password>
            </AuthenticationData>
            </soap:Header>
                <soap:Body>
                <AddBookingDetail xmlns="http://192.168.0.170/TT/BookingAPI">
                <saveRequest>
                    <RefId>${bookedFlight?.refId}</RefId>
                    <BookingStatus>Ticketed</BookingStatus>
                </saveRequest>
                </AddBookingDetail>
            </soap:Body>
            </soap:Envelope>"""
                )
                print('--------------addBookingDetailResponse--------------------' + addBookingDetailResponse.text)
                String source = fareQuotedResult.GetFareQuoteResponse.GetFareQuoteResult.Result.Source
                print '------------------source-----------------' + source
                SOAPResponse getBooking = client.send("""<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <AuthenticationData xmlns="http://192.168.0.170/TT/BookingAPI">
      <SiteName></SiteName>
      <AccountCode></AccountCode>
      <UserName>${grailsApplication.config.tbo.FLIGHT_USERNAME}</UserName>
      <Password>${grailsApplication.config.tbo.FLIGHT_PASS}</Password>
    </AuthenticationData>
  </soap:Header>
  <soap:Body>
    <GetBooking xmlns="http://192.168.0.170/TT/BookingAPI">
      <bookingRequest>
        <BookingId>${bookedFlight?.bookingId}</BookingId>
        <Pnr>${bookedFlight?.pnr}</Pnr>
        <Source>${source}</Source>
      </bookingRequest>
    </GetBooking>
  </soap:Body>
</soap:Envelope>"""
                )

                print('--------------getDetailResponse--------------------' + getBooking.text)

                def bookingResult = getBooking.GetBookingResponse.GetBookingResult
                bookedFlight.origin = bookingResult.Origin
                bookedFlight.destination = bookingResult.Destination
                bookedFlight.fareType = bookingResult.FareType
                bookedFlight.source = bookingResult.Source
                bookedFlight.status = bookingResult.Description
                bookedFlight.save(flush: true, failOnError: true)

                BookedFlightFare bookedFlightFare = new BookedFlightFare()
                bookedFlightFare.additionalTxnFee = utilService.convertStringToBigdecimal(bookingResult.Fare.AdditionalTxnFee.text())
                bookedFlightFare.agentCommission = utilService.convertStringToBigdecimal(bookingResult.Fare.AgentCommission.text())
                bookedFlightFare.tdsOnCommission = utilService.convertStringToBigdecimal(bookingResult.Fare.TdsOnCommission.text())
                bookedFlightFare.airTransFee = utilService.convertStringToBigdecimal(bookingResult.Fare.AirTransFee.text())
                bookedFlightFare.baseFare = utilService.convertStringToBigdecimal(bookingResult.Fare.BaseFare.text())
                bookedFlightFare.tax = utilService.convertStringToBigdecimal(bookingResult.Fare.Tax.text())
                bookedFlightFare.otherCharges = utilService.convertStringToBigdecimal(bookingResult.Fare.OtherCharges.text())
                bookedFlightFare.fuelSurcharge = utilService.convertStringToBigdecimal(bookingResult.Fare.FuelSurcharge.text())
                bookedFlightFare.discount = utilService.convertStringToBigdecimal(bookingResult.Fare.Discount.text())
                bookedFlightFare.serviceTax = utilService.convertStringToBigdecimal(bookingResult.Fare.ServiceTax.text())
                bookedFlightFare.currency = bookingResult.Fare.Currency
                bookedFlightFare.plb = utilService.convertStringToBigdecimal(bookingResult.Fare.PLBEarned.text())
                bookedFlightFare.tdsOnPLB = utilService.convertStringToBigdecimal(bookingResult.Fare.TdsOnPLB.text())
                bookedFlightFare.incentiveEarned = utilService.convertStringToBigdecimal(bookingResult.Fare.IncentiveEarned.text())
                bookedFlightFare.tdsOnIncentive = utilService.convertStringToBigdecimal(bookingResult.Fare.TdsOnIncentive.text())
                bookedFlightFare.offeredFare = utilService.convertStringToBigdecimal(bookingResult.Fare.OfferedFare.text())
                bookedFlightFare.agentServiceCharge = utilService.convertStringToBigdecimal(bookingResult.Fare.AgentServiceCharge.text())
                bookedFlightFare.publishedPrice = utilService.convertStringToBigdecimal(bookingResult.Fare.PublishedPrice.text())

                bookedFlightFare.bookedFlight = bookedFlight
                bookedFlightFare.save(flush: true, failOnError: true)

                def passengerDetail = bookingResult.Passenger.WSPassenger
                bookingResult.Passenger.WSPassenger.each {
                    BookedFlightPassenger bookedFlightPassenger = new BookedFlightPassenger()
                    bookedFlightPassenger.title = passengerDetail.Title
                    bookedFlightPassenger.firstName = passengerDetail.FirstName
                    bookedFlightPassenger.lastName = passengerDetail.LastName
                    bookedFlightPassenger.type = passengerDetail.Type
                    bookedFlightPassenger.gender = passengerDetail.Gender
//            bookedFlightPassenger.dateOfBirth = passengerDetail.DateOfBirth
                    bookedFlightPassenger.dateOfBirth = new Date()
                    bookedFlightPassenger.passportNumber = passengerDetail.PassportNumber
                    bookedFlightPassenger.passportExpiry = passengerDetail.PassportExpiry
                    bookedFlightPassenger.country = passengerDetail.Country
                    bookedFlightPassenger.phone = passengerDetail.Phone
                    bookedFlightPassenger.pincode = passengerDetail.PinCode
                    bookedFlightPassenger.address1 = passengerDetail.AddressLine1
                    bookedFlightPassenger.address2 = passengerDetail.AddressLine2
                    bookedFlightPassenger.email = passengerDetail.Email
                    bookedFlightPassenger.seat = passengerDetail.Seat
                    bookedFlightPassenger.meal = passengerDetail.Meal
                    bookedFlightPassenger.bookedFlight = bookedFlight
                    bookedFlightPassenger.save(flush: true, failOnError: true)
                    bookedFlight.addToBookedFlightPassanger(bookedFlightPassenger)
                }
                def ticketDetail = bookingResult.Ticket.WSTicket
                bookingResult.Ticket.WSTicket.each {
                    BookedFlightTicket bookedFlightTicket = new BookedFlightTicket()
                    bookedFlightTicket.ticketId = utilService.convertStringToInteger(ticketDetail.TicketId.text())
                    bookedFlightTicket.ticketNumber = ticketDetail.TicketNumber
                    bookedFlightTicket.title = ticketDetail.Title
                    bookedFlightTicket.firstName = ticketDetail.FirstName
                    bookedFlightTicket.lastName = ticketDetail.LastName
                    bookedFlightTicket.passengerType = ticketDetail.PaxType
                    bookedFlightTicket.issueDate = new Date()
                    bookedFlightTicket.tourCode = ticketDetail.TourCode
                    bookedFlightTicket.bookedFlight = bookedFlight
                    bookedFlightTicket.save(flush: true, failOnError: true)
                    bookedFlight.addToBookedFlightTicket(bookedFlightTicket)
                }

            }

        }
        if (flightSearchVO?.journeyReturnDate) {
            String varFlight = flightForBookingList.get(0)
            String[] array = varFlight.split(',')
            String ouBound = array[0]
            String iBound = array[1]
            String outBound = ouBound.substring(1)
            String inBound = iBound.substring(1, iBound.length() - 1)
            print 'outBound' + outBound
            print 'inBound' + inBound
            flightSearchVOListInBound.each { flight ->
                print '---------inBound-------' + flight
                if (inBound.toString() == flight.toString()) {
                    flightSearchResultVOList << flight
                    print('-----------------flightSearchResultVOList-----------' + flightSearchResultVOList)
                }
            }

            flightSearchVOList.each { flight ->
                print '---------outBound--------' + flight
                if (outBound.toString() == flight.toString()) {
                    flightSearchResultVOList << flight
                    print('-----------------flightSearchResultVOList-----------' + flightSearchResultVOList)
                }
            }

            flightSearchResultVOList.each { FlightSearchResultVO flightSearchResultVO ->
                print '-------flightSearchResultVO.sessionId-------' + flightSearchResultVO.sessionId
                String[] sessionIdList = flightSearchResultVO.sessionId.split(',')

                if (flightSearchResultVO.tripIndicator.equals("1")) {
                    print 'going to book outbound'
                    String sessionId = sessionIdList.first()
                    print 'going to book outbound sessionId' + sessionId

                    if (flightSearchResultVO.isLcc) {
                        def fareQuotedResult = client.send("""<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <AuthenticationData xmlns="http://192.168.0.170/TT/BookingAPI">
      <SiteName></SiteName>
      <AccountCode></AccountCode>
      <UserName>${grailsApplication.config.tbo.FLIGHT_USERNAME}</UserName>
      <Password>${grailsApplication.config.tbo.FLIGHT_PASS}</Password>
    </AuthenticationData>
  </soap:Header>
  <soap:Body>
    <GetFareQuote xmlns="http://192.168.0.170/TT/BookingAPI">
      <fareQuoteRequest>
        <Result>
          <WSResult>
         <TripIndicator>${flightSearchResultVO?.tripIndicator?.toInteger()}</TripIndicator>
      <Fare>
        <BaseFare>${flightSearchResultVO?.flightFare?.baseFare}</BaseFare>
        <Tax>${flightSearchResultVO?.flightFare?.tax}</Tax>
        <ServiceTax>${flightSearchResultVO?.flightFare?.serviceTax}</ServiceTax>
        <AdditionalTxnFee>${flightSearchResultVO?.flightFare?.additionalTxnFee}</AdditionalTxnFee>
        <AgentCommission>${flightSearchResultVO?.flightFare?.agentCommission}</AgentCommission>
        <TdsOnCommission>${flightSearchResultVO?.flightFare?.tdsOnCommission}</TdsOnCommission>
        <IncentiveEarned>${flightSearchResultVO?.flightFare?.incentiveEarned}</IncentiveEarned>
        <TdsOnIncentive>${flightSearchResultVO?.flightFare?.tdsOnIncentive}</TdsOnIncentive>
        <PLBEarned>${flightSearchResultVO?.flightFare?.pLBEarned}</PLBEarned>
        <TdsOnPLB>${flightSearchResultVO?.flightFare?.tdsOnPLB}</TdsOnPLB>
        <PublishedPrice>${flightSearchResultVO?.flightFare?.publishedPrice}</PublishedPrice>
        <AirTransFee>${flightSearchResultVO?.flightFare?.airTransFee}</AirTransFee>
        <Currency>${flightSearchResultVO?.flightFare?.currency}</Currency>
        <Discount>${flightSearchResultVO?.flightFare?.discount}</Discount>
        <ChargeBU>
        </ChargeBU>
        <OtherCharges>${flightSearchResultVO?.flightFare?.otherCharges}</OtherCharges>
        <FuelSurcharge>${flightSearchResultVO?.flightFare?.fuelSurcharge}</FuelSurcharge>
        <TransactionFee>${flightSearchResultVO?.flightFare?.transactionFee}</TransactionFee>
        <ReverseHandlingCharge>${flightSearchResultVO?.flightFare?.reverseHandlingCharge}</ReverseHandlingCharge>
        <OfferedFare>${flightSearchResultVO?.flightFare?.offeredFare}</OfferedFare>
        <AgentServiceCharge>${flightSearchResultVO?.flightFare?.agentServiceCharge}</AgentServiceCharge>
      </Fare>
      <FareBreakdown>
        <WSPTCFare>
          <PassengerType>${flightSearchResultVO?.fareBreakDown?.passengerType?.first()}</PassengerType>
          <PassengerCount>${flightSearchResultVO?.fareBreakDown?.passengerCount?.first()}</PassengerCount>
          <BaseFare>${flightSearchResultVO?.fareBreakDown?.baseFare?.first()}</BaseFare>
          <Tax>${flightSearchResultVO?.fareBreakDown?.tax?.first()}</Tax>
          <AirlineTransFee>${flightSearchResultVO?.fareBreakDown?.airlineTransFee?.first()}</AirlineTransFee>
          <AdditionalTxnFee>${flightSearchResultVO?.fareBreakDown?.additionalTxnFee?.first()}</AdditionalTxnFee>
          <FuelSurcharge>${flightSearchResultVO?.fareBreakDown?.fuelSurcharge?.first()}</FuelSurcharge>
        </WSPTCFare>
      </FareBreakdown>
      <Origin>${flightSearchResultVO?.origin}</Origin>
      <Destination>${flightSearchResultVO?.destination}</Destination>
      <Segment>
        <WSSegment>
          <SegmentIndicator>${flightSearchResultVO?.flightSegmentVO?.segmentIndicator?.first()?.toInteger()}</SegmentIndicator>
          <Airline>
            <AirlineCode>${flightSearchResultVO?.flightSegmentVO?.airLineCode?.first()}</AirlineCode>
            <AirlineName>${flightSearchResultVO?.flightSegmentVO?.airLineName?.first()}</AirlineName>
          </Airline>
          <FlightNumber>${flightSearchResultVO?.flightSegmentVO?.flightNumber?.first()}</FlightNumber>
          <FareClass>${flightSearchResultVO?.flightSegmentVO?.fareClass?.first()}</FareClass>
          <Origin>
            <AirportCode>${flightSearchResultVO?.flightSegmentVO?.originAirPortCode?.first()}</AirportCode>
            <AirportName>${flightSearchResultVO?.flightSegmentVO?.originAirPortName?.first()}</AirportName>
            <Terminal />
            <CityCode>${flightSearchResultVO?.flightSegmentVO?.originCityCode?.first()}</CityCode>
            <CityName>${flightSearchResultVO?.flightSegmentVO?.originCityName?.first()}</CityName>
            <CountryCode>${flightSearchResultVO?.flightSegmentVO?.originCountryCode?.first()}</CountryCode>
            <CountryName>${flightSearchResultVO?.flightSegmentVO?.originCountryName?.first()}</CountryName>
          </Origin>
          <Destination>
            <AirportCode>${flightSearchResultVO?.flightSegmentVO?.destinationAirPortCode?.first()}</AirportCode>
            <AirportName>${flightSearchResultVO?.flightSegmentVO?.destinationAirPortName?.first()}</AirportName>
            <Terminal />
            <CityCode>${flightSearchResultVO?.flightSegmentVO?.destinationCityCode?.first()}</CityCode>
            <CityName>${flightSearchResultVO?.flightSegmentVO?.destinationCityName?.first()}</CityName>
            <CountryCode>${flightSearchResultVO?.flightSegmentVO?.destinationCountryCode?.first()}</CountryCode>
            <CountryName>${flightSearchResultVO?.flightSegmentVO?.destinationCountryName?.first()}</CountryName>
          </Destination>
          <DepTIme>${flightSearchResultVO?.flightSegmentVO?.departureTime?.first()}</DepTIme>
          <ArrTime>${flightSearchResultVO?.flightSegmentVO?.arrivalTime?.first()}</ArrTime>
          <ETicketEligible>${flightSearchResultVO?.flightSegmentVO?.eTicketEligible?.first()}</ETicketEligible>
          <Duration>${flightSearchResultVO?.flightSegmentVO?.duration?.first()}</Duration>
          <Stop>${flightSearchResultVO?.flightSegmentVO?.stops?.first()?.toInteger()}</Stop>
          <Craft>${flightSearchResultVO?.flightSegmentVO?.craftType?.first()}</Craft>
          <Status>${flightSearchResultVO?.flightSegmentVO?.status?.first()}</Status>
        </WSSegment>
      </Segment>
      <IbDuration />
      <ObDuration>${flightSearchResultVO?.obDuration}</ObDuration>
      <Source>${flightSearchResultVO?.flightFareRule?.source?.first()}</Source>
      <FareRule>
        <WSFareRule>
          <Origin>${flightSearchResultVO?.flightFareRule?.origin?.first()}</Origin>
          <Destination>${flightSearchResultVO?.flightFareRule?.destination?.first()}</Destination>
          <Airline>${flightSearchResultVO?.flightFareRule?.airline?.first()}</Airline>
          <FareRestriction />
          <FareBasisCode>${flightSearchResultVO?.flightFareRule?.fareBasisCode?.first()}</FareBasisCode>
          <DepartureDate>${flightSearchResultVO?.flightFareRule?.departureDate?.first()}</DepartureDate>
          <ReturnDate>${flightSearchResultVO?.flightFareRule?.returnDate?.first()}</ReturnDate>
          <Source>${flightSearchResultVO?.flightFareRule?.source?.first()}</Source>
        </WSFareRule>
      </FareRule>
      <IsLcc>${flightSearchResultVO?.isLcc}</IsLcc>
      <ObSegCount>${flightSearchResultVO?.flightSegmentVO?.size()?.toInteger()}</ObSegCount>
      <PromotionalPlanType>Normal</PromotionalPlanType>
      <NonRefundable>${flightSearchResultVO?.isRefundable}</NonRefundable>
      <SegmentKey>${flightSearchResultVO?.segmentKey}</SegmentKey>
         </WSResult>
        </Result>
        <SessionId>${flightSearchResultVO?.sessionId?.split(',')?.first()}</SessionId>
      </fareQuoteRequest>
    </GetFareQuote>
  </soap:Body>
</soap:Envelope>""")
                        print '------------outbound----fareQuotedResult------------------' + fareQuotedResult.text
                    } else {
                        def bookResponse = client.send("""<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <AuthenticationData xmlns="http://192.168.0.170/TT/BookingAPI">
      <SiteName>string</SiteName>
      <AccountCode>string</AccountCode>
      <UserName>${grailsApplication.config.tbo.FLIGHT_USERNAME}</UserName>
      <Password>${grailsApplication.config.tbo.FLIGHT_PASS}</Password>
    </AuthenticationData>
  </soap:Header>
  <soap:Body>
    <Book xmlns="http://192.168.0.170/TT/BookingAPI">
      <bookRequest>
        <Remarks>Flight Book</Remarks>
        <InstantTicket>true</InstantTicket>
        <Fare>
          <BaseFare>${flightSearchResultVO?.flightFare?.baseFare}</BaseFare>
          <Tax>${flightSearchResultVO?.flightFare?.tax}</Tax>
          <ServiceTax>${flightSearchResultVO?.flightFare?.serviceTax}</ServiceTax>
          <AdditionalTxnFee>${flightSearchResultVO?.flightFare?.additionalTxnFee}</AdditionalTxnFee>
          <AgentCommission>${flightSearchResultVO?.flightFare?.agentCommission}</AgentCommission>
          <TdsOnCommission>${flightSearchResultVO?.flightFare?.tdsOnCommission}</TdsOnCommission>
          <IncentiveEarned>${flightSearchResultVO?.flightFare?.incentiveEarned}</IncentiveEarned>
          <TdsOnIncentive>${flightSearchResultVO?.flightFare?.tdsOnIncentive}</TdsOnIncentive>
          <PLBEarned>${flightSearchResultVO?.flightFare?.pLBEarned}</PLBEarned>
          <TdsOnPLB>${flightSearchResultVO?.flightFare?.tdsOnPLB}</TdsOnPLB>
          <PublishedPrice>${flightSearchResultVO?.flightFare?.publishedPrice}</PublishedPrice>
          <AirTransFee>${flightSearchResultVO?.flightFare?.airTransFee}</AirTransFee>
          <Currency>${flightSearchResultVO?.flightFare?.currency}</Currency>
          <Discount>${flightSearchResultVO?.flightFare?.discount}</Discount>
          <ChargeBU>
          </ChargeBU>
          <OtherCharges>${flightSearchResultVO?.flightFare?.otherCharges}</OtherCharges>
          <FuelSurcharge>${flightSearchResultVO?.flightFare?.fuelSurcharge}</FuelSurcharge>
          <TransactionFee>${flightSearchResultVO?.flightFare?.transactionFee}</TransactionFee>
          <ReverseHandlingCharge>${flightSearchResultVO?.flightFare?.reverseHandlingCharge}</ReverseHandlingCharge>
          <OfferedFare>${flightSearchResultVO?.flightFare?.offeredFare}</OfferedFare>
          <AgentServiceCharge>${flightSearchResultVO?.flightFare?.agentServiceCharge}</AgentServiceCharge>
          <AgentConvienceCharges>${flightSearchResultVO?.flightFare?.agentConvienceCharges}</AgentConvienceCharges>
        </Fare>
        <Passenger>
          <WSPassenger>
            <Title>${params.adult1TName}</Title>
            <FirstName>${params.adult1FName}</FirstName>
            <LastName>${params.adult1LName}</LastName>
            <Type>Adult</Type>
            <DateOfBirth>1988-02-12T00:00:00</DateOfBirth>
            <Fare>
                <BaseFare>${flightSearchResultVO?.flightFare?.baseFare}</BaseFare>
                <Tax>${flightSearchResultVO?.flightFare?.tax}</Tax>
                <ServiceTax>${flightSearchResultVO?.flightFare?.serviceTax}</ServiceTax>
                <AdditionalTxnFee>${flightSearchResultVO?.flightFare?.additionalTxnFee}</AdditionalTxnFee>
                <AgentCommission>${flightSearchResultVO?.flightFare?.agentCommission}</AgentCommission>
                <TdsOnCommission>${flightSearchResultVO?.flightFare?.tdsOnCommission}</TdsOnCommission>
                <IncentiveEarned>${flightSearchResultVO?.flightFare?.incentiveEarned}</IncentiveEarned>
                <TdsOnIncentive>${flightSearchResultVO?.flightFare?.tdsOnIncentive}</TdsOnIncentive>
                <PLBEarned>${flightSearchResultVO?.flightFare?.pLBEarned}</PLBEarned>
                <TdsOnPLB>${flightSearchResultVO?.flightFare?.tdsOnPLB}</TdsOnPLB>
                <PublishedPrice>${flightSearchResultVO?.flightFare?.publishedPrice}</PublishedPrice>
                <AirTransFee>${flightSearchResultVO?.flightFare?.airTransFee}</AirTransFee>
                <Discount>${flightSearchResultVO?.flightFare?.discount}</Discount>
                <OtherCharges>${flightSearchResultVO?.flightFare?.otherCharges}</OtherCharges>
                <FuelSurcharge>${flightSearchResultVO?.flightFare?.fuelSurcharge}</FuelSurcharge>
                <TransactionFee>${flightSearchResultVO?.flightFare?.transactionFee}</TransactionFee>
                <ReverseHandlingCharge>${flightSearchResultVO?.flightFare?.reverseHandlingCharge}</ReverseHandlingCharge>
                <OfferedFare>${flightSearchResultVO?.flightFare?.offeredFare}</OfferedFare>
                <AgentServiceCharge>${flightSearchResultVO?.flightFare?.agentServiceCharge}</AgentServiceCharge>
            </Fare>
            <Ssr xsi:nil="true" />
            <Gender>Male</Gender>
            <PassportNumber>string</PassportNumber>
            <PassportExpiry>0001-01-01T00:00:00</PassportExpiry>
            <PinCode>110096</PinCode>
            <Country>IN</Country>
            <Phone>8377001072</Phone>
            <AddressLine1>c-52, Cannought Place</AddressLine1>
            <AddressLine2>string</AddressLine2>
            <Email>${params.adult1Email}</Email>
            <Meal xsi:nil="true" />
            <Seat xsi:nil="true" />
            <FFAirline>string</FFAirline>
            <FFNumber>string</FFNumber>
          </WSPassenger>
        </Passenger>
        <Origin>${flightSearchResultVO?.origin}</Origin>
        <Destination>${flightSearchResultVO?.destination}</Destination>
        <Segment>
          <WSSegment>
            <SegmentIndicator>${flightSearchResultVO?.flightSegmentVO?.first()?.segmentIndicator}</SegmentIndicator>
            <Airline>
                <AirlineCode>${flightSearchResultVO?.flightSegmentVO?.first()?.airLineCode}</AirlineCode>
                <AirlineName>${flightSearchResultVO?.flightSegmentVO?.first()?.airLineName}</AirlineName>
                <AirlineRemarks></AirlineRemarks>
            </Airline>
            <FlightNumber>${flightSearchResultVO?.flightSegmentVO?.first()?.flightNumber}</FlightNumber>
            <FareClass>${flightSearchResultVO?.flightSegmentVO?.first()?.fareClass}</FareClass>
            <Origin>
                <AirportCode>${flightSearchResultVO?.flightSegmentVO?.first()?.originAirPortCode}</AirportCode>
                <AirportName>${flightSearchResultVO?.flightSegmentVO?.first()?.originCountryName}</AirportName>
                <Terminal>${flightSearchResultVO?.flightSegmentVO?.first()?.originTerminal}</Terminal>
                <CityCode>${flightSearchResultVO?.flightSegmentVO?.first()?.originCityCode}</CityCode>
                <CityName>${flightSearchResultVO?.flightSegmentVO?.first()?.originCityName}</CityName>
                <CountryCode>${flightSearchResultVO?.flightSegmentVO?.first()?.originCountryCode}</CountryCode>
                <CountryName>${flightSearchResultVO?.flightSegmentVO?.first()?.originCountryName}</CountryName>
            </Origin>
            <Destination>
                <AirportCode>${flightSearchResultVO?.flightSegmentVO?.first()?.destinationAirPortCode}</AirportCode>
                <AirportName>${flightSearchResultVO?.flightSegmentVO?.first()?.destinationAirPortName}</AirportName>
                <Terminal>${flightSearchResultVO?.flightSegmentVO?.first()?.destinationTerminal}</Terminal>
                <CityCode>${flightSearchResultVO?.flightSegmentVO?.first()?.destinationCityCode}</CityCode>
                <CityName>${flightSearchResultVO?.flightSegmentVO?.first()?.destinationCityName}</CityName>
                <CountryCode>${flightSearchResultVO?.flightSegmentVO?.first()?.destinationCountryCode}</CountryCode>
                <CountryName>${flightSearchResultVO?.flightSegmentVO?.first()?.destinationCountryName}</CountryName>
            </Destination>
            <DepTIme>${flightSearchResultVO?.flightSegmentVO?.first()?.departureTime}</DepTIme>
            <ArrTime>${flightSearchResultVO?.flightSegmentVO?.first()?.arrivalTime}</ArrTime>
            <ETicketEligible>${flightSearchResultVO?.flightSegmentVO?.first()?.eTicketEligible}</ETicketEligible>
            <Duration>${flightSearchResultVO?.flightSegmentVO?.first()?.duration}</Duration>
            <Stop>${flightSearchResultVO?.flightSegmentVO?.first()?.stops}</Stop>
            <Craft>${flightSearchResultVO?.flightSegmentVO?.first()?.craftType}</Craft>
            <Status>Confirmed</Status>
            <OperatingCarrier>${flightSearchResultVO?.flightSegmentVO?.first()?.operatingCarrier}</OperatingCarrier>
          </WSSegment>
          </Segment>
        <FareType>PUB</FareType>
        <FareRule>
          <WSFareRule>
            <Origin>${flightSearchResultVO?.flightFareRule?.first()?.origin}</Origin>
            <Destination>${flightSearchResultVO?.flightFareRule?.first()?.destination}</Destination>
            <Airline>${flightSearchResultVO?.flightFareRule?.first()?.airline}</Airline>
            <FareRestriction>${flightSearchResultVO?.flightFareRule?.first()?.fareRestriction}</FareRestriction>
            <FareBasisCode>${flightSearchResultVO?.flightFareRule?.first()?.fareBasisCode}</FareBasisCode>
            <FareRuleDetail>string</FareRuleDetail>
            <DepartureDate>${flightSearchResultVO?.flightFareRule?.first()?.departureDate}</DepartureDate>
            <ReturnDate>${flightSearchResultVO?.flightFareRule?.first()?.returnDate}</ReturnDate>
            <Source>${flightSearchResultVO?.flightFareRule?.first()?.source}</Source>
          </WSFareRule>
        </FareRule>
        <Source>Amadeus</Source>
        <PaymentInformation>
          <PaymentInformationId>0</PaymentInformationId>
          <InvoiceNumber>0</InvoiceNumber>
          <PaymentId>0</PaymentId>
          <Amount>${flightSearchResultVO?.flightFare?.offeredFare}</Amount>
          <IPAddress>127.0.0.1</IPAddress>
          <TrackId>0</TrackId>
          <PaymentGateway>APICustomer</PaymentGateway>
          <PaymentModeType>Deposited</PaymentModeType>
        </PaymentInformation>
        <SessionId>${flightSearchResultVO?.sessionId?.split(',')?.last()}</SessionId>
        <PromotionalPlanType>Normal</PromotionalPlanType>
        <SegmentKey>${flightSearchResultVO?.segmentKey}</SegmentKey>
      </bookRequest>
    </Book>
  </soap:Body>
</soap:Envelope>""")

                    }

                    def ticketResponse = client.send("""<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <AuthenticationData xmlns="http://192.168.0.170/TT/BookingAPI">
      <SiteName></SiteName>
      <AccountCode></AccountCode>
      <UserName>${grailsApplication.config.tbo.FLIGHT_USERNAME}</UserName>
      <Password>${grailsApplication.config.tbo.FLIGHT_PASS}</Password>
    </AuthenticationData>
  </soap:Header>
  <soap:Body>
    <Ticket xmlns="http://192.168.0.170/TT/BookingAPI">
      <wsTicketRequest>
        <BookingID></BookingID>
        <Origin>${flightSearchResultVO?.flightFareRule?.origin?.first()}</Origin>
        <Destination>${flightSearchResultVO?.flightFareRule?.destination?.first()}</Destination>
        <Segment>
          <WSSegment>
            <SegmentIndicator>${flightSearchResultVO?.flightSegmentVO?.segmentIndicator?.first()?.toInteger()}</SegmentIndicator>
            <Airline>
            <AirlineCode>${flightSearchResultVO?.flightSegmentVO?.airLineCode?.first()}</AirlineCode>
            <AirlineName>${flightSearchResultVO?.flightSegmentVO?.airLineName?.first()}</AirlineName>
            </Airline>
            <FlightNumber>${flightSearchResultVO?.flightSegmentVO?.flightNumber?.first()}</FlightNumber>
            <FareClass>${flightSearchResultVO?.flightSegmentVO?.fareClass?.first()}</FareClass>
            <AirlinePNR>string</AirlinePNR>
            <Origin>
                <AirportCode>${flightSearchResultVO?.flightSegmentVO?.originAirPortCode?.first()}</AirportCode>
                <AirportName>${flightSearchResultVO?.flightSegmentVO?.originAirPortName?.first()}</AirportName>
                <Terminal />
                <CityCode>${flightSearchResultVO?.flightSegmentVO?.originCityCode?.first()}</CityCode>
                <CityName>${flightSearchResultVO?.flightSegmentVO?.originCityName?.first()}</CityName>
                <CountryCode>${flightSearchResultVO?.flightSegmentVO?.originCountryCode?.first()}</CountryCode>
                <CountryName>${flightSearchResultVO?.flightSegmentVO?.originCountryName?.first()}</CountryName>
            </Origin>
            <Destination>
                <AirportCode>${flightSearchResultVO?.flightSegmentVO?.destinationAirPortCode?.first()}</AirportCode>
                <AirportName>${flightSearchResultVO?.flightSegmentVO?.destinationAirPortName?.first()}</AirportName>
                <Terminal />
                <CityCode>${flightSearchResultVO?.flightSegmentVO?.destinationCityCode?.first()}</CityCode>
                <CityName>${flightSearchResultVO?.flightSegmentVO?.destinationCityName?.first()}</CityName>
                <CountryCode>${flightSearchResultVO?.flightSegmentVO?.destinationCountryCode?.first()}</CountryCode>
                <CountryName>${flightSearchResultVO?.flightSegmentVO?.destinationCountryName?.first()}</CountryName>
            </Destination>
            <DepTIme>${flightSearchResultVO?.flightSegmentVO?.departureTime?.first()}</DepTIme>
            <ArrTime>${flightSearchResultVO?.flightSegmentVO?.arrivalTime?.first()}</ArrTime>
            <ETicketEligible>${flightSearchResultVO?.flightSegmentVO?.eTicketEligible?.first()}</ETicketEligible>
            <Duration>${flightSearchResultVO?.flightSegmentVO?.duration?.first()}</Duration>
            <Stop>${flightSearchResultVO?.flightSegmentVO?.stops?.first()?.toInteger()}</Stop>
            <Craft>${flightSearchResultVO?.flightSegmentVO?.craftType?.first()}</Craft>
            <Status>${flightSearchResultVO?.flightSegmentVO?.status?.first()}</Status>
            <OperatingCarrier>string</OperatingCarrier>
          </WSSegment>
        </Segment>
        <FareType>string</FareType>
        <FareRule>
          <WSFareRule>
            <Origin>${flightSearchResultVO?.flightFareRule?.origin?.first()}</Origin>
            <Destination>${flightSearchResultVO?.flightFareRule?.destination?.first()}</Destination>
            <Airline>${flightSearchResultVO?.flightFareRule?.airline?.first()}</Airline>
            <FareRestriction>${flightSearchResultVO?.flightFareRule?.fareRestriction?.first()}</FareRestriction>
            <FareBasisCode>${flightSearchResultVO?.flightFareRule?.fareBasisCode?.first()}</FareBasisCode>
            <FareRuleDetail></FareRuleDetail>
            <DepartureDate>${flightSearchResultVO?.flightFareRule?.departureDate?.first()}</DepartureDate>
            <ReturnDate>${flightSearchResultVO?.flightFareRule?.returnDate?.first()}</ReturnDate>
            <Source>${flightSearchResultVO?.flightFareRule?.source?.first()}</Source>
          </WSFareRule>
        </FareRule>
        <Fare>
          <BaseFare>${flightSearchResultVO?.flightFare?.baseFare}</BaseFare>
          <Tax>${flightSearchResultVO?.flightFare?.tax}</Tax>
          <ServiceTax>${flightSearchResultVO?.flightFare?.serviceTax}</ServiceTax>
          <AdditionalTxnFee>${flightSearchResultVO?.flightFare?.additionalTxnFee}</AdditionalTxnFee>
          <AgentCommission>${flightSearchResultVO?.flightFare?.agentCommission}</AgentCommission>
          <TdsOnCommission>${flightSearchResultVO?.flightFare?.tdsOnCommission}</TdsOnCommission>
          <IncentiveEarned>${flightSearchResultVO?.flightFare?.incentiveEarned}</IncentiveEarned>
          <TdsOnIncentive>${flightSearchResultVO?.flightFare?.tdsOnIncentive}</TdsOnIncentive>
          <PLBEarned>${flightSearchResultVO?.flightFare?.pLBEarned}</PLBEarned>
          <TdsOnPLB>${flightSearchResultVO?.flightFare?.tdsOnPLB}</TdsOnPLB>
          <PublishedPrice>${flightSearchResultVO?.flightFare?.publishedPrice}</PublishedPrice>
          <AirTransFee>${flightSearchResultVO?.flightFare?.airTransFee}</AirTransFee>
          <Currency>${flightSearchResultVO?.flightFare?.currency}</Currency>
          <Discount>${flightSearchResultVO?.flightFare?.discount}</Discount>
          <ChargeBU>
            <ChargeBreakUp xsi:nil="true" />
            <ChargeBreakUp xsi:nil="true" />
          </ChargeBU>
          <OtherCharges>${flightSearchResultVO?.flightFare?.otherCharges}</OtherCharges>
          <FuelSurcharge>${flightSearchResultVO?.flightFare?.fuelSurcharge}</FuelSurcharge>
          <TransactionFee>${flightSearchResultVO?.flightFare?.transactionFee}</TransactionFee>
          <ReverseHandlingCharge>${flightSearchResultVO?.flightFare?.reverseHandlingCharge}</ReverseHandlingCharge>
          <OfferedFare>${flightSearchResultVO?.flightFare?.offeredFare}</OfferedFare>
          <AgentServiceCharge>${flightSearchResultVO?.flightFare?.agentServiceCharge}</AgentServiceCharge>
          <AgentConvienceCharges>${flightSearchResultVO?.flightFare?.agentConvienceCharges}</AgentConvienceCharges>
        </Fare>
        <Passenger>
          <WSPassenger>
            <Title>Mr</Title>
            <FirstName>Anil</FirstName>
            <LastName>Kumar</LastName>
            <Type>Adult</Type>
            <DateOfBirth>1990-08-16</DateOfBirth>
            <Fare>
                <BaseFare>${flightSearchResultVO?.flightFare?.baseFare}</BaseFare>
                <Tax>${flightSearchResultVO?.flightFare?.tax}</Tax>
                <ServiceTax>${flightSearchResultVO?.flightFare?.serviceTax}</ServiceTax>
                <AdditionalTxnFee>${flightSearchResultVO?.flightFare?.additionalTxnFee}</AdditionalTxnFee>
                <AgentCommission>${flightSearchResultVO?.flightFare?.agentCommission}</AgentCommission>
                <TdsOnCommission>${flightSearchResultVO?.flightFare?.tdsOnCommission}</TdsOnCommission>
                <IncentiveEarned>${flightSearchResultVO?.flightFare?.incentiveEarned}</IncentiveEarned>
                <TdsOnIncentive>${flightSearchResultVO?.flightFare?.tdsOnIncentive}</TdsOnIncentive>
                <PLBEarned>${flightSearchResultVO?.flightFare?.pLBEarned}</PLBEarned>
                <TdsOnPLB>${flightSearchResultVO?.flightFare?.tdsOnPLB}</TdsOnPLB>
                <PublishedPrice>${flightSearchResultVO?.flightFare?.publishedPrice}</PublishedPrice>
                <AirTransFee>${flightSearchResultVO?.flightFare?.airTransFee}</AirTransFee>
                <Discount>${flightSearchResultVO?.flightFare?.discount}</Discount>
                <OtherCharges>${flightSearchResultVO?.flightFare?.otherCharges}</OtherCharges>
                <FuelSurcharge>${flightSearchResultVO?.flightFare?.fuelSurcharge}</FuelSurcharge>
                <TransactionFee>${flightSearchResultVO?.flightFare?.transactionFee}</TransactionFee>
                <ReverseHandlingCharge>${flightSearchResultVO?.flightFare?.reverseHandlingCharge}</ReverseHandlingCharge>
                <OfferedFare>${flightSearchResultVO?.flightFare?.offeredFare}</OfferedFare>
                <AgentServiceCharge>${flightSearchResultVO?.flightFare?.agentServiceCharge}</AgentServiceCharge>
            </Fare>
            <Ssr xsi:nil="true" />
            <Gender>Male</Gender>
            <PassportNumber>string</PassportNumber>
            <PassportExpiry>2020-01-01</PassportExpiry>
            <PinCode>string</PinCode>
            <Country>IN</Country>
            <Phone>8377001072</Phone>
            <AddressLine1>abc,karnal</AddressLine1>
            <Email>samverma0001@gmail.com</Email>
          </WSPassenger>
        </Passenger>
        <Remarks>Flight Ticket</Remarks>
        <InstantTicket>true</InstantTicket>
        <PaymentInformation>
          <PaymentInformationId>0</PaymentInformationId>
          <InvoiceNumber>0</InvoiceNumber>
          <PaymentId/>
          <Amount>${flightSearchResultVO?.flightFare?.publishedPrice}</Amount>
          <IPAddress/>
          <TrackId>0</TrackId>
          <PaymentGateway>APICustomer</PaymentGateway>
          <PaymentModeType>Deposited</PaymentModeType>
        </PaymentInformation>
        <Source>${flightSearchResultVO?.flightFareRule?.source?.first()}</Source>
        <SessionId>${flightSearchResultVO?.sessionId?.split(',')?.first()}</SessionId>
        <IsOneWayBooking>true</IsOneWayBooking>
        <PromotionalPlanType>Normal</PromotionalPlanType>
      </wsTicketRequest>
    </Ticket>
  </soap:Body>
</soap:Envelope>"""
                    )
                    print('--------outbound------------response----------------' + response.text)
                    description = ticketResponse.TicketResponse.TicketResult.Status.Description
                    String statusCode = ticketResponse.TicketResponse.TicketResult.Status.StatusCode

                    if (statusCode.equals('14')) {
                        bookedFlight = new BookedFlight()
                        bookedFlight.pnr = ticketResponse.TicketResponse.TicketResult.PNR
                        bookedFlight.refId = ticketResponse.TicketResponse.TicketResult.RefId
                        bookedFlight.bookingId = ticketResponse.TicketResponse.TicketResult.BookingId
                        bookedFlight.user = newUser
                        bookedFlight.save(flush: true, failOnError: true)
                        newUser.addToBookedFlights(bookedFlight)

                        SOAPResponse addBookingDetailResponse = client.send("""<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
            <soap:Header>
                <AuthenticationData xmlns="http://192.168.0.170/TT/BookingAPI">
                <SiteName></SiteName>
                <AccountCode></AccountCode>
                <UserName>${grailsApplication.config.tbo.FLIGHT_USERNAME}</UserName>
                <Password>${grailsApplication.config.tbo.FLIGHT_PASS}</Password>
            </AuthenticationData>
            </soap:Header>
                <soap:Body>
                <AddBookingDetail xmlns="http://192.168.0.170/TT/BookingAPI">
                <saveRequest>
                    <RefId>${bookedFlight?.refId}</RefId>
                    <BookingStatus>Ticketed</BookingStatus>
                </saveRequest>
                </AddBookingDetail>
            </soap:Body>
            </soap:Envelope>"""
                        )
                        print('--------------addBookingDetailResponse--------------------' + addBookingDetailResponse.text)
//                        String source = fareQuotedResult.GetFareQuoteResponse.GetFareQuoteResult.Result.Source
                        String source = flightSearchResultVO?.flightFareRule?.first()?.source
                        print '------------------source-----------------' + source
                        SOAPResponse getBooking = client.send("""<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <AuthenticationData xmlns="http://192.168.0.170/TT/BookingAPI">
      <SiteName></SiteName>
      <AccountCode></AccountCode>
      <UserName>${grailsApplication.config.tbo.FLIGHT_USERNAME}</UserName>
      <Password>${grailsApplication.config.tbo.FLIGHT_PASS}</Password>
    </AuthenticationData>
  </soap:Header>
  <soap:Body>
    <GetBooking xmlns="http://192.168.0.170/TT/BookingAPI">
      <bookingRequest>
        <BookingId>${bookedFlight?.bookingId}</BookingId>
        <Pnr>${bookedFlight?.pnr}</Pnr>
        <Source>${source}</Source>
      </bookingRequest>
    </GetBooking>
  </soap:Body>
</soap:Envelope>"""
                        )

                        print('--------------getDetailResponse--------------------' + getBooking.text)

                        def bookingResult = getBooking.GetBookingResponse.GetBookingResult
                        bookedFlight.origin = bookingResult.Origin
                        bookedFlight.destination = bookingResult.Destination
                        bookedFlight.fareType = bookingResult.FareType
                        bookedFlight.source = bookingResult.Source
                        bookedFlight.status = bookingResult.Description
                        bookedFlight.save(flush: true, failOnError: true)

                        BookedFlightFare bookedFlightFare = new BookedFlightFare()
                        bookedFlightFare.additionalTxnFee = utilService.convertStringToBigdecimal(bookingResult.Fare.AdditionalTxnFee.text())
                        bookedFlightFare.agentCommission = utilService.convertStringToBigdecimal(bookingResult.Fare.AgentCommission.text())
                        bookedFlightFare.tdsOnCommission = utilService.convertStringToBigdecimal(bookingResult.Fare.TdsOnCommission.text())
                        bookedFlightFare.airTransFee = utilService.convertStringToBigdecimal(bookingResult.Fare.AirTransFee.text())
                        bookedFlightFare.baseFare = utilService.convertStringToBigdecimal(bookingResult.Fare.BaseFare.text())
                        bookedFlightFare.tax = utilService.convertStringToBigdecimal(bookingResult.Fare.Tax.text())
                        bookedFlightFare.otherCharges = utilService.convertStringToBigdecimal(bookingResult.Fare.OtherCharges.text())
                        bookedFlightFare.fuelSurcharge = utilService.convertStringToBigdecimal(bookingResult.Fare.FuelSurcharge.text())
                        bookedFlightFare.discount = utilService.convertStringToBigdecimal(bookingResult.Fare.Discount.text())
                        bookedFlightFare.serviceTax = utilService.convertStringToBigdecimal(bookingResult.Fare.ServiceTax.text())
                        bookedFlightFare.currency = bookingResult.Fare.Currency
                        bookedFlightFare.plb = utilService.convertStringToBigdecimal(bookingResult.Fare.PLBEarned.text())
                        bookedFlightFare.tdsOnPLB = utilService.convertStringToBigdecimal(bookingResult.Fare.TdsOnPLB.text())
                        bookedFlightFare.incentiveEarned = utilService.convertStringToBigdecimal(bookingResult.Fare.IncentiveEarned.text())
                        bookedFlightFare.tdsOnIncentive = utilService.convertStringToBigdecimal(bookingResult.Fare.TdsOnIncentive.text())
                        bookedFlightFare.offeredFare = utilService.convertStringToBigdecimal(bookingResult.Fare.OfferedFare.text())
                        bookedFlightFare.agentServiceCharge = utilService.convertStringToBigdecimal(bookingResult.Fare.AgentServiceCharge.text())
                        bookedFlightFare.publishedPrice = utilService.convertStringToBigdecimal(bookingResult.Fare.PublishedPrice.text())

                        bookedFlightFare.bookedFlight = bookedFlight
                        bookedFlightFare.save(flush: true, failOnError: true)

                        def passengerDetail = bookingResult.Passenger.WSPassenger
                        bookingResult.Passenger.WSPassenger.each {
                            BookedFlightPassenger bookedFlightPassenger = new BookedFlightPassenger()
                            bookedFlightPassenger.title = passengerDetail.Title
                            bookedFlightPassenger.firstName = passengerDetail.FirstName
                            bookedFlightPassenger.lastName = passengerDetail.LastName
                            bookedFlightPassenger.type = passengerDetail.Type
                            bookedFlightPassenger.gender = passengerDetail.Gender
//            bookedFlightPassenger.dateOfBirth = passengerDetail.DateOfBirth
                            bookedFlightPassenger.dateOfBirth = new Date()
                            bookedFlightPassenger.passportNumber = passengerDetail.PassportNumber
                            bookedFlightPassenger.passportExpiry = passengerDetail.PassportExpiry
                            bookedFlightPassenger.country = passengerDetail.Country
                            bookedFlightPassenger.phone = passengerDetail.Phone
                            bookedFlightPassenger.pincode = passengerDetail.PinCode
                            bookedFlightPassenger.address1 = passengerDetail.AddressLine1
                            bookedFlightPassenger.address2 = passengerDetail.AddressLine2
                            bookedFlightPassenger.email = passengerDetail.Email
                            bookedFlightPassenger.seat = passengerDetail.Seat
                            bookedFlightPassenger.meal = passengerDetail.Meal
                            bookedFlightPassenger.bookedFlight = bookedFlight
                            bookedFlightPassenger.save(flush: true, failOnError: true)
                            bookedFlight.addToBookedFlightPassanger(bookedFlightPassenger)
                        }
                        def ticketDetail = bookingResult.Ticket.WSTicket
                        bookingResult.Ticket.WSTicket.each {
                            BookedFlightTicket bookedFlightTicket = new BookedFlightTicket()
                            bookedFlightTicket.ticketId = utilService.convertStringToInteger(ticketDetail.TicketId.text())
                            bookedFlightTicket.ticketNumber = ticketDetail.TicketNumber
                            bookedFlightTicket.title = ticketDetail.Title
                            bookedFlightTicket.firstName = ticketDetail.FirstName
                            bookedFlightTicket.lastName = ticketDetail.LastName
                            bookedFlightTicket.passengerType = ticketDetail.PaxType
                            bookedFlightTicket.issueDate = new Date()
                            bookedFlightTicket.tourCode = ticketDetail.TourCode
                            bookedFlightTicket.bookedFlight = bookedFlight
                            bookedFlightTicket.save(flush: true, failOnError: true)
                            bookedFlight.addToBookedFlightTicket(bookedFlightTicket)
                        }

                    }

                } else {
                    print('---------going to book inbound---------------------')
                    if (flightSearchResultVO?.isLcc) {
                        def fareQuotedResult = client.send("""<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <AuthenticationData xmlns="http://192.168.0.170/TT/BookingAPI">
      <SiteName></SiteName>
      <AccountCode></AccountCode>
      <UserName>${grailsApplication.config.tbo.FLIGHT_USERNAME}</UserName>
      <Password>${grailsApplication.config.tbo.FLIGHT_PASS}</Password>
    </AuthenticationData>
  </soap:Header>
  <soap:Body>
    <GetFareQuote xmlns="http://192.168.0.170/TT/BookingAPI">
      <fareQuoteRequest>
        <Result>
          <WSResult>
         <TripIndicator>${flightSearchResultVO?.tripIndicator?.toInteger()}</TripIndicator>
      <Fare>
        <BaseFare>${flightSearchResultVO?.flightFare?.baseFare}</BaseFare>
        <Tax>${flightSearchResultVO?.flightFare?.tax}</Tax>
        <ServiceTax>${flightSearchResultVO?.flightFare?.serviceTax}</ServiceTax>
        <AdditionalTxnFee>${flightSearchResultVO?.flightFare?.additionalTxnFee}</AdditionalTxnFee>
        <AgentCommission>${flightSearchResultVO?.flightFare?.agentCommission}</AgentCommission>
        <TdsOnCommission>${flightSearchResultVO?.flightFare?.tdsOnCommission}</TdsOnCommission>
        <IncentiveEarned>${flightSearchResultVO?.flightFare?.incentiveEarned}</IncentiveEarned>
        <TdsOnIncentive>${flightSearchResultVO?.flightFare?.tdsOnIncentive}</TdsOnIncentive>
        <PLBEarned>${flightSearchResultVO?.flightFare?.pLBEarned}</PLBEarned>
        <TdsOnPLB>${flightSearchResultVO?.flightFare?.tdsOnPLB}</TdsOnPLB>
        <PublishedPrice>${flightSearchResultVO?.flightFare?.publishedPrice}</PublishedPrice>
        <AirTransFee>${flightSearchResultVO?.flightFare?.airTransFee}</AirTransFee>
        <Currency>${flightSearchResultVO?.flightFare?.currency}</Currency>
        <Discount>${flightSearchResultVO?.flightFare?.discount}</Discount>
        <ChargeBU>
        </ChargeBU>
        <OtherCharges>${flightSearchResultVO?.flightFare?.otherCharges}</OtherCharges>
        <FuelSurcharge>${flightSearchResultVO?.flightFare?.fuelSurcharge}</FuelSurcharge>
        <TransactionFee>${flightSearchResultVO?.flightFare?.transactionFee}</TransactionFee>
        <ReverseHandlingCharge>${flightSearchResultVO?.flightFare?.reverseHandlingCharge}</ReverseHandlingCharge>
        <OfferedFare>${flightSearchResultVO?.flightFare?.offeredFare}</OfferedFare>
        <AgentServiceCharge>${flightSearchResultVO?.flightFare?.agentServiceCharge}</AgentServiceCharge>
      </Fare>
      <FareBreakdown>
        <WSPTCFare>
          <PassengerType>${flightSearchResultVO?.fareBreakDown?.passengerType?.first()}</PassengerType>
          <PassengerCount>${flightSearchResultVO?.fareBreakDown?.passengerCount?.first()}</PassengerCount>
          <BaseFare>${flightSearchResultVO?.fareBreakDown?.baseFare?.first()}</BaseFare>
          <Tax>${flightSearchResultVO?.fareBreakDown?.tax?.first()}</Tax>
          <AirlineTransFee>${flightSearchResultVO?.fareBreakDown?.airlineTransFee?.first()}</AirlineTransFee>
          <AdditionalTxnFee>${flightSearchResultVO?.fareBreakDown?.additionalTxnFee?.first()}</AdditionalTxnFee>
          <FuelSurcharge>${flightSearchResultVO?.fareBreakDown?.fuelSurcharge?.first()}</FuelSurcharge>
        </WSPTCFare>
      </FareBreakdown>
      <Origin>${flightSearchResultVO?.origin}</Origin>
      <Destination>${flightSearchResultVO?.destination}</Destination>
      <Segment>
        <WSSegment>
          <SegmentIndicator>${flightSearchResultVO?.flightSegmentVO?.segmentIndicator?.first()?.toInteger()}</SegmentIndicator>
          <Airline>
            <AirlineCode>${flightSearchResultVO?.flightSegmentVO?.airLineCode?.first()}</AirlineCode>
            <AirlineName>${flightSearchResultVO?.flightSegmentVO?.airLineName?.first()}</AirlineName>
          </Airline>
          <FlightNumber>${flightSearchResultVO?.flightSegmentVO?.flightNumber?.first()}</FlightNumber>
          <FareClass>${flightSearchResultVO?.flightSegmentVO?.fareClass?.first()}</FareClass>
          <Origin>
            <AirportCode>${flightSearchResultVO?.flightSegmentVO?.originAirPortCode?.first()}</AirportCode>
            <AirportName>${flightSearchResultVO?.flightSegmentVO?.originAirPortName?.first()}</AirportName>
            <Terminal />
            <CityCode>${flightSearchResultVO?.flightSegmentVO?.originCityCode?.first()}</CityCode>
            <CityName>${flightSearchResultVO?.flightSegmentVO?.originCityName?.first()}</CityName>
            <CountryCode>${flightSearchResultVO?.flightSegmentVO?.originCountryCode?.first()}</CountryCode>
            <CountryName>${flightSearchResultVO?.flightSegmentVO?.originCountryName?.first()}</CountryName>
          </Origin>
          <Destination>
            <AirportCode>${flightSearchResultVO?.flightSegmentVO?.destinationAirPortCode?.first()}</AirportCode>
            <AirportName>${flightSearchResultVO?.flightSegmentVO?.destinationAirPortName?.first()}</AirportName>
            <Terminal />
            <CityCode>${flightSearchResultVO?.flightSegmentVO?.destinationCityCode?.first()}</CityCode>
            <CityName>${flightSearchResultVO?.flightSegmentVO?.destinationCityName?.first()}</CityName>
            <CountryCode>${flightSearchResultVO?.flightSegmentVO?.destinationCountryCode?.first()}</CountryCode>
            <CountryName>${flightSearchResultVO?.flightSegmentVO?.destinationCountryName?.first()}</CountryName>
          </Destination>
          <DepTIme>${flightSearchResultVO?.flightSegmentVO?.departureTime?.first()}</DepTIme>
          <ArrTime>${flightSearchResultVO?.flightSegmentVO?.arrivalTime?.first()}</ArrTime>
          <ETicketEligible>${flightSearchResultVO?.flightSegmentVO?.eTicketEligible?.first()}</ETicketEligible>
          <Duration>${flightSearchResultVO?.flightSegmentVO?.duration?.first()}</Duration>
          <Stop>${flightSearchResultVO?.flightSegmentVO?.stops?.first()?.toInteger()}</Stop>
          <Craft>${flightSearchResultVO?.flightSegmentVO?.craftType?.first()}</Craft>
          <Status>${flightSearchResultVO?.flightSegmentVO?.status?.first()}</Status>
        </WSSegment>
      </Segment>
      <IbDuration />
      <ObDuration>${flightSearchResultVO?.obDuration}</ObDuration>
      <Source>${flightSearchResultVO?.flightFareRule?.source?.first()}</Source>
      <FareRule>
        <WSFareRule>
          <Origin>${flightSearchResultVO?.flightFareRule?.origin?.first()}</Origin>
          <Destination>${flightSearchResultVO?.flightFareRule?.destination?.first()}</Destination>
          <Airline>${flightSearchResultVO?.flightFareRule?.airline?.first()}</Airline>
          <FareRestriction />
          <FareBasisCode>${flightSearchResultVO?.flightFareRule?.fareBasisCode?.first()}</FareBasisCode>
          <DepartureDate>${flightSearchResultVO?.flightFareRule?.departureDate?.first()}</DepartureDate>
          <ReturnDate>${flightSearchResultVO?.flightFareRule?.returnDate?.first()}</ReturnDate>
          <Source>${flightSearchResultVO?.flightFareRule?.source?.first()}</Source>
        </WSFareRule>
      </FareRule>
      <IsLcc>${flightSearchResultVO?.isLcc}</IsLcc>
      <ObSegCount>${flightSearchResultVO?.flightSegmentVO?.size()?.toInteger()}</ObSegCount>
      <PromotionalPlanType>Normal</PromotionalPlanType>
      <NonRefundable>${flightSearchResultVO?.isRefundable}</NonRefundable>
      <SegmentKey>${flightSearchResultVO?.segmentKey}</SegmentKey>
         </WSResult>
        </Result>
        <SessionId>${flightSearchResultVO?.sessionId?.split(',')?.last()}</SessionId>
      </fareQuoteRequest>
    </GetFareQuote>
  </soap:Body>
</soap:Envelope>""")
                        print '--------inbound--------fareQuotedResult------------------' + fareQuotedResult.text
                    } else {
                        print('---------non lcc---------------------')
                        def bookResponse = client.send("""<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <AuthenticationData xmlns="http://192.168.0.170/TT/BookingAPI">
      <SiteName>string</SiteName>
      <AccountCode>string</AccountCode>
      <UserName>${grailsApplication.config.tbo.FLIGHT_USERNAME}</UserName>
      <Password>${grailsApplication.config.tbo.FLIGHT_PASS}</Password>
    </AuthenticationData>
  </soap:Header>
  <soap:Body>
    <Book xmlns="http://192.168.0.170/TT/BookingAPI">
      <bookRequest>
        <Remarks>Flight Book</Remarks>
        <InstantTicket>true</InstantTicket>
        <Fare>
          <BaseFare>${flightSearchResultVO?.flightFare?.baseFare}</BaseFare>
          <Tax>${flightSearchResultVO?.flightFare?.tax}</Tax>
          <ServiceTax>${flightSearchResultVO?.flightFare?.serviceTax}</ServiceTax>
          <AdditionalTxnFee>${flightSearchResultVO?.flightFare?.additionalTxnFee}</AdditionalTxnFee>
          <AgentCommission>${flightSearchResultVO?.flightFare?.agentCommission}</AgentCommission>
          <TdsOnCommission>${flightSearchResultVO?.flightFare?.tdsOnCommission}</TdsOnCommission>
          <IncentiveEarned>${flightSearchResultVO?.flightFare?.incentiveEarned}</IncentiveEarned>
          <TdsOnIncentive>${flightSearchResultVO?.flightFare?.tdsOnIncentive}</TdsOnIncentive>
          <PLBEarned>${flightSearchResultVO?.flightFare?.pLBEarned}</PLBEarned>
          <TdsOnPLB>${flightSearchResultVO?.flightFare?.tdsOnPLB}</TdsOnPLB>
          <PublishedPrice>${flightSearchResultVO?.flightFare?.publishedPrice}</PublishedPrice>
          <AirTransFee>${flightSearchResultVO?.flightFare?.airTransFee}</AirTransFee>
          <Currency>${flightSearchResultVO?.flightFare?.currency}</Currency>
          <Discount>${flightSearchResultVO?.flightFare?.discount}</Discount>
          <ChargeBU>
          </ChargeBU>
          <OtherCharges>${flightSearchResultVO?.flightFare?.otherCharges}</OtherCharges>
          <FuelSurcharge>${flightSearchResultVO?.flightFare?.fuelSurcharge}</FuelSurcharge>
          <TransactionFee>${flightSearchResultVO?.flightFare?.transactionFee}</TransactionFee>
          <ReverseHandlingCharge>${flightSearchResultVO?.flightFare?.reverseHandlingCharge}</ReverseHandlingCharge>
          <OfferedFare>${flightSearchResultVO?.flightFare?.offeredFare}</OfferedFare>
          <AgentServiceCharge>${flightSearchResultVO?.flightFare?.agentServiceCharge}</AgentServiceCharge>
          <AgentConvienceCharges>${flightSearchResultVO?.flightFare?.agentConvienceCharges}</AgentConvienceCharges>
        </Fare>
        <Passenger>
          <WSPassenger>
            <Title>${params.adult1TName}</Title>
            <FirstName>${params.adult1FName}</FirstName>
            <LastName>${params.adult1LName}</LastName>
            <Type>Adult</Type>
            <DateOfBirth>1988-02-12T00:00:00</DateOfBirth>
            <Fare>
                <BaseFare>${flightSearchResultVO?.flightFare?.baseFare}</BaseFare>
                <Tax>${flightSearchResultVO?.flightFare?.tax}</Tax>
                <ServiceTax>${flightSearchResultVO?.flightFare?.serviceTax}</ServiceTax>
                <AdditionalTxnFee>${flightSearchResultVO?.flightFare?.additionalTxnFee}</AdditionalTxnFee>
                <AgentCommission>${flightSearchResultVO?.flightFare?.agentCommission}</AgentCommission>
                <TdsOnCommission>${flightSearchResultVO?.flightFare?.tdsOnCommission}</TdsOnCommission>
                <IncentiveEarned>${flightSearchResultVO?.flightFare?.incentiveEarned}</IncentiveEarned>
                <TdsOnIncentive>${flightSearchResultVO?.flightFare?.tdsOnIncentive}</TdsOnIncentive>
                <PLBEarned>${flightSearchResultVO?.flightFare?.pLBEarned}</PLBEarned>
                <TdsOnPLB>${flightSearchResultVO?.flightFare?.tdsOnPLB}</TdsOnPLB>
                <PublishedPrice>${flightSearchResultVO?.flightFare?.publishedPrice}</PublishedPrice>
                <AirTransFee>${flightSearchResultVO?.flightFare?.airTransFee}</AirTransFee>
                <Discount>${flightSearchResultVO?.flightFare?.discount}</Discount>
                <OtherCharges>${flightSearchResultVO?.flightFare?.otherCharges}</OtherCharges>
                <FuelSurcharge>${flightSearchResultVO?.flightFare?.fuelSurcharge}</FuelSurcharge>
                <TransactionFee>${flightSearchResultVO?.flightFare?.transactionFee}</TransactionFee>
                <ReverseHandlingCharge>${flightSearchResultVO?.flightFare?.reverseHandlingCharge}</ReverseHandlingCharge>
                <OfferedFare>${flightSearchResultVO?.flightFare?.offeredFare}</OfferedFare>
                <AgentServiceCharge>${flightSearchResultVO?.flightFare?.agentServiceCharge}</AgentServiceCharge>
            </Fare>
            <Ssr xsi:nil="true" />
            <Gender>Male</Gender>
            <PassportNumber>string</PassportNumber>
            <PassportExpiry>0001-01-01T00:00:00</PassportExpiry>
            <PinCode>110096</PinCode>
            <Country>IN</Country>
            <Phone>8377001072</Phone>
            <AddressLine1>c-52, Cannought Place</AddressLine1>
            <AddressLine2>string</AddressLine2>
            <Email>${params.adult1Email}</Email>
            <Meal xsi:nil="true" />
            <Seat xsi:nil="true" />
            <FFAirline>string</FFAirline>
            <FFNumber>string</FFNumber>
          </WSPassenger>
        </Passenger>
        <Origin>${flightSearchResultVO?.origin}</Origin>
        <Destination>${flightSearchResultVO?.destination}</Destination>
        <Segment>
          <WSSegment>
            <SegmentIndicator>${flightSearchResultVO?.flightSegmentVO?.first()?.segmentIndicator}</SegmentIndicator>
            <Airline>
                <AirlineCode>${flightSearchResultVO?.flightSegmentVO?.first()?.airLineCode}</AirlineCode>
                <AirlineName>${flightSearchResultVO?.flightSegmentVO?.first()?.airLineName}</AirlineName>
                <AirlineRemarks>${flightSearchResultVO?.flightSegmentVO?.first()?.airLineRemark}</AirlineRemarks>
            </Airline>
            <FlightNumber>${flightSearchResultVO?.flightSegmentVO?.first()?.flightNumber}</FlightNumber>
            <FareClass>${flightSearchResultVO?.flightSegmentVO?.first()?.fareClass}</FareClass>
            <Origin>
                <AirportCode>${flightSearchResultVO?.flightSegmentVO?.first()?.originAirPortCode}</AirportCode>
                <AirportName>${flightSearchResultVO?.flightSegmentVO?.first()?.originCountryName}</AirportName>
                <Terminal>${flightSearchResultVO?.flightSegmentVO?.first()?.originTerminal}</Terminal>
                <CityCode>${flightSearchResultVO?.flightSegmentVO?.first()?.originCityCode}</CityCode>
                <CityName>${flightSearchResultVO?.flightSegmentVO?.first()?.originCityName}</CityName>
                <CountryCode>${flightSearchResultVO?.flightSegmentVO?.first()?.originCountryCode}</CountryCode>
                <CountryName>${flightSearchResultVO?.flightSegmentVO?.first()?.originCountryName}</CountryName>
            </Origin>
            <Destination>
                <AirportCode>${flightSearchResultVO?.flightSegmentVO?.first()?.destinationAirPortCode}</AirportCode>
                <AirportName>${flightSearchResultVO?.flightSegmentVO?.first()?.destinationAirPortName}</AirportName>
                <Terminal>${flightSearchResultVO?.flightSegmentVO?.first()?.destinationTerminal}</Terminal>
                <CityCode>${flightSearchResultVO?.flightSegmentVO?.first()?.destinationCityCode}</CityCode>
                <CityName>${flightSearchResultVO?.flightSegmentVO?.first()?.destinationCityName}</CityName>
                <CountryCode>${flightSearchResultVO?.flightSegmentVO?.first()?.destinationCountryCode}</CountryCode>
                <CountryName>${flightSearchResultVO?.flightSegmentVO?.first()?.destinationCountryName}</CountryName>
            </Destination>
            <DepTIme>${flightSearchResultVO?.flightSegmentVO?.first()?.departureTime}</DepTIme>
            <ArrTime>${flightSearchResultVO?.flightSegmentVO?.first()?.arrivalTime}</ArrTime>
            <ETicketEligible>${flightSearchResultVO?.flightSegmentVO?.first()?.eTicketEligible}</ETicketEligible>
            <Duration>${flightSearchResultVO?.flightSegmentVO?.first()?.duration}</Duration>
            <Stop>${flightSearchResultVO?.flightSegmentVO?.first()?.stops}</Stop>
            <Craft>${flightSearchResultVO?.flightSegmentVO?.first()?.craftType}</Craft>
            <Status>Confirmed</Status>
            <OperatingCarrier>${flightSearchResultVO?.flightSegmentVO?.first()?.operatingCarrier}</OperatingCarrier>
          </WSSegment>
        </Segment>
        <FareType>PUB</FareType>
        <FareRule>
          <WSFareRule>
            <Origin>${flightSearchResultVO?.flightFareRule?.first()?.origin}</Origin>
            <Destination>${flightSearchResultVO?.flightFareRule?.first()?.destination}</Destination>
            <Airline>${flightSearchResultVO?.flightFareRule?.first()?.airline}</Airline>
            <FareRestriction>${flightSearchResultVO?.flightFareRule?.first()?.fareRestriction}</FareRestriction>
            <FareBasisCode>${flightSearchResultVO?.flightFareRule?.first()?.fareBasisCode}</FareBasisCode>
            <FareRuleDetail>string</FareRuleDetail>
            <DepartureDate>${flightSearchResultVO?.flightFareRule?.first()?.departureDate}</DepartureDate>
            <ReturnDate>${flightSearchResultVO?.flightFareRule?.first()?.returnDate}</ReturnDate>
            <Source>${flightSearchResultVO?.flightFareRule?.first()?.source}</Source>
          </WSFareRule>
        </FareRule>
        <Source>Amadeus</Source>
        <PaymentInformation>
          <PaymentInformationId>0</PaymentInformationId>
          <InvoiceNumber>0</InvoiceNumber>
          <PaymentId>0</PaymentId>
          <Amount>${flightSearchResultVO?.flightFare?.offeredFare}</Amount>
          <IPAddress>127.0.0.1</IPAddress>
          <TrackId>0</TrackId>
          <PaymentGateway>APICustomer</PaymentGateway>
          <PaymentModeType>Deposited</PaymentModeType>
        </PaymentInformation>
        <SessionId>${flightSearchResultVO?.sessionId?.split(',')?.last()}</SessionId>
        <PromotionalPlanType>Normal</PromotionalPlanType>
        <SegmentKey>${flightSearchResultVO?.segmentKey}</SegmentKey>
      </bookRequest>
    </Book>
  </soap:Body>
</soap:Envelope>""")
                    }

                    def ticketResponse = client.send("""<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <AuthenticationData xmlns="http://192.168.0.170/TT/BookingAPI">
      <SiteName></SiteName>
      <AccountCode></AccountCode>
      <UserName>${grailsApplication.config.tbo.FLIGHT_USERNAME}</UserName>
      <Password>${grailsApplication.config.tbo.FLIGHT_PASS}</Password>
    </AuthenticationData>
  </soap:Header>
  <soap:Body>
    <Ticket xmlns="http://192.168.0.170/TT/BookingAPI">
      <wsTicketRequest>
        <BookingID></BookingID>
        <Origin>${flightSearchResultVO?.flightFareRule?.origin?.first()}</Origin>
        <Destination>${flightSearchResultVO?.flightFareRule?.destination?.first()}</Destination>
        <Segment>
          <WSSegment>
            <SegmentIndicator>${flightSearchResultVO?.flightSegmentVO?.segmentIndicator?.first()?.toInteger()}</SegmentIndicator>
            <Airline>
            <AirlineCode>${flightSearchResultVO?.flightSegmentVO?.airLineCode?.first()}</AirlineCode>
            <AirlineName>${flightSearchResultVO?.flightSegmentVO?.airLineName?.first()}</AirlineName>
            </Airline>
            <FlightNumber>${flightSearchResultVO?.flightSegmentVO?.flightNumber?.first()}</FlightNumber>
            <FareClass>${flightSearchResultVO?.flightSegmentVO?.fareClass?.first()}</FareClass>
            <AirlinePNR>string</AirlinePNR>
            <Origin>
                <AirportCode>${flightSearchResultVO?.flightSegmentVO?.originAirPortCode?.first()}</AirportCode>
                <AirportName>${flightSearchResultVO?.flightSegmentVO?.originAirPortName?.first()}</AirportName>
                <Terminal />
                <CityCode>${flightSearchResultVO?.flightSegmentVO?.originCityCode?.first()}</CityCode>
                <CityName>${flightSearchResultVO?.flightSegmentVO?.originCityName?.first()}</CityName>
                <CountryCode>${flightSearchResultVO?.flightSegmentVO?.originCountryCode?.first()}</CountryCode>
                <CountryName>${flightSearchResultVO?.flightSegmentVO?.originCountryName?.first()}</CountryName>
            </Origin>
            <Destination>
                <AirportCode>${flightSearchResultVO?.flightSegmentVO?.destinationAirPortCode?.first()}</AirportCode>
                <AirportName>${flightSearchResultVO?.flightSegmentVO?.destinationAirPortName?.first()}</AirportName>
                <Terminal />
                <CityCode>${flightSearchResultVO?.flightSegmentVO?.destinationCityCode?.first()}</CityCode>
                <CityName>${flightSearchResultVO?.flightSegmentVO?.destinationCityName?.first()}</CityName>
                <CountryCode>${flightSearchResultVO?.flightSegmentVO?.destinationCountryCode?.first()}</CountryCode>
                <CountryName>${flightSearchResultVO?.flightSegmentVO?.destinationCountryName?.first()}</CountryName>
            </Destination>
            <DepTIme>${flightSearchResultVO?.flightSegmentVO?.departureTime?.first()}</DepTIme>
            <ArrTime>${flightSearchResultVO?.flightSegmentVO?.arrivalTime?.first()}</ArrTime>
            <ETicketEligible>${flightSearchResultVO?.flightSegmentVO?.eTicketEligible?.first()}</ETicketEligible>
            <Duration>${flightSearchResultVO?.flightSegmentVO?.duration?.first()}</Duration>
            <Stop>${flightSearchResultVO?.flightSegmentVO?.stops?.first()?.toInteger()}</Stop>
            <Craft>${flightSearchResultVO?.flightSegmentVO?.craftType?.first()}</Craft>
            <Status>${flightSearchResultVO?.flightSegmentVO?.status?.first()}</Status>
            <OperatingCarrier>string</OperatingCarrier>
          </WSSegment>
        </Segment>
        <FareType>string</FareType>
        <FareRule>
          <WSFareRule>
            <Origin>${flightSearchResultVO?.flightFareRule?.origin?.first()}</Origin>
            <Destination>${flightSearchResultVO?.flightFareRule?.destination?.first()}</Destination>
            <Airline>${flightSearchResultVO?.flightFareRule?.airline?.first()}</Airline>
            <FareRestriction>${flightSearchResultVO?.flightFareRule?.fareRestriction?.first()}</FareRestriction>
            <FareBasisCode>${flightSearchResultVO?.flightFareRule?.fareBasisCode?.first()}</FareBasisCode>
            <FareRuleDetail></FareRuleDetail>
            <DepartureDate>${flightSearchResultVO?.flightFareRule?.departureDate?.first()}</DepartureDate>
            <ReturnDate>${flightSearchResultVO?.flightFareRule?.returnDate?.first()}</ReturnDate>
            <Source>${flightSearchResultVO?.flightFareRule?.source?.first()}</Source>
          </WSFareRule>
        </FareRule>
        <Fare>
          <BaseFare>${flightSearchResultVO?.flightFare?.baseFare}</BaseFare>
          <Tax>${flightSearchResultVO?.flightFare?.tax}</Tax>
          <ServiceTax>${flightSearchResultVO?.flightFare?.serviceTax}</ServiceTax>
          <AdditionalTxnFee>${flightSearchResultVO?.flightFare?.additionalTxnFee}</AdditionalTxnFee>
          <AgentCommission>${flightSearchResultVO?.flightFare?.agentCommission}</AgentCommission>
          <TdsOnCommission>${flightSearchResultVO?.flightFare?.tdsOnCommission}</TdsOnCommission>
          <IncentiveEarned>${flightSearchResultVO?.flightFare?.incentiveEarned}</IncentiveEarned>
          <TdsOnIncentive>${flightSearchResultVO?.flightFare?.tdsOnIncentive}</TdsOnIncentive>
          <PLBEarned>${flightSearchResultVO?.flightFare?.pLBEarned}</PLBEarned>
          <TdsOnPLB>${flightSearchResultVO?.flightFare?.tdsOnPLB}</TdsOnPLB>
          <PublishedPrice>${flightSearchResultVO?.flightFare?.publishedPrice}</PublishedPrice>
          <AirTransFee>${flightSearchResultVO?.flightFare?.airTransFee}</AirTransFee>
          <Currency>${flightSearchResultVO?.flightFare?.currency}</Currency>
          <Discount>${flightSearchResultVO?.flightFare?.discount}</Discount>
          <ChargeBU>
            <ChargeBreakUp xsi:nil="true" />
            <ChargeBreakUp xsi:nil="true" />
          </ChargeBU>
          <OtherCharges>${flightSearchResultVO?.flightFare?.otherCharges}</OtherCharges>
          <FuelSurcharge>${flightSearchResultVO?.flightFare?.fuelSurcharge}</FuelSurcharge>
          <TransactionFee>${flightSearchResultVO?.flightFare?.transactionFee}</TransactionFee>
          <ReverseHandlingCharge>${flightSearchResultVO?.flightFare?.reverseHandlingCharge}</ReverseHandlingCharge>
          <OfferedFare>${flightSearchResultVO?.flightFare?.offeredFare}</OfferedFare>
          <AgentServiceCharge>${flightSearchResultVO?.flightFare?.agentServiceCharge}</AgentServiceCharge>
          <AgentConvienceCharges>${flightSearchResultVO?.flightFare?.agentConvienceCharges}</AgentConvienceCharges>
        </Fare>
        <Passenger>
          <WSPassenger>
            <Title>Mr</Title>
            <FirstName>Anil</FirstName>
            <LastName>Kumar</LastName>
            <Type>Adult</Type>
            <DateOfBirth>1990-08-16</DateOfBirth>
            <Fare>
                <BaseFare>${flightSearchResultVO?.flightFare?.baseFare}</BaseFare>
                <Tax>${flightSearchResultVO?.flightFare?.tax}</Tax>
                <ServiceTax>${flightSearchResultVO?.flightFare?.serviceTax}</ServiceTax>
                <AdditionalTxnFee>${flightSearchResultVO?.flightFare?.additionalTxnFee}</AdditionalTxnFee>
                <AgentCommission>${flightSearchResultVO?.flightFare?.agentCommission}</AgentCommission>
                <TdsOnCommission>${flightSearchResultVO?.flightFare?.tdsOnCommission}</TdsOnCommission>
                <IncentiveEarned>${flightSearchResultVO?.flightFare?.incentiveEarned}</IncentiveEarned>
                <TdsOnIncentive>${flightSearchResultVO?.flightFare?.tdsOnIncentive}</TdsOnIncentive>
                <PLBEarned>${flightSearchResultVO?.flightFare?.pLBEarned}</PLBEarned>
                <TdsOnPLB>${flightSearchResultVO?.flightFare?.tdsOnPLB}</TdsOnPLB>
                <PublishedPrice>${flightSearchResultVO?.flightFare?.publishedPrice}</PublishedPrice>
                <AirTransFee>${flightSearchResultVO?.flightFare?.airTransFee}</AirTransFee>
                <Discount>${flightSearchResultVO?.flightFare?.discount}</Discount>
                <OtherCharges>${flightSearchResultVO?.flightFare?.otherCharges}</OtherCharges>
                <FuelSurcharge>${flightSearchResultVO?.flightFare?.fuelSurcharge}</FuelSurcharge>
                <TransactionFee>${flightSearchResultVO?.flightFare?.transactionFee}</TransactionFee>
                <ReverseHandlingCharge>${flightSearchResultVO?.flightFare?.reverseHandlingCharge}</ReverseHandlingCharge>
                <OfferedFare>${flightSearchResultVO?.flightFare?.offeredFare}</OfferedFare>
                <AgentServiceCharge>${flightSearchResultVO?.flightFare?.agentServiceCharge}</AgentServiceCharge>
            </Fare>
            <Ssr xsi:nil="true" />
            <Gender>Male</Gender>
            <PassportNumber>string</PassportNumber>
            <PassportExpiry>2020-01-01</PassportExpiry>
            <PinCode>string</PinCode>
            <Country>IN</Country>
            <Phone>8377001072</Phone>
            <AddressLine1>abc,karnal</AddressLine1>
            <Email>samverma0001@gmail.com</Email>
          </WSPassenger>
        </Passenger>
        <Remarks>Flight Ticket</Remarks>
        <InstantTicket>true</InstantTicket>
        <PaymentInformation>
          <PaymentInformationId>0</PaymentInformationId>
          <InvoiceNumber>0</InvoiceNumber>
          <PaymentId/>
          <Amount>${flightSearchResultVO?.flightFare?.publishedPrice}</Amount>
          <IPAddress/>
          <TrackId>0</TrackId>
          <PaymentGateway>APICustomer</PaymentGateway>
          <PaymentModeType>Deposited</PaymentModeType>
        </PaymentInformation>
        <Source>${flightSearchResultVO?.flightFareRule?.source?.first()}</Source>
        <SessionId>${flightSearchResultVO?.sessionId?.split(',')?.last()}</SessionId>
        <IsOneWayBooking>true</IsOneWayBooking>
        <PromotionalPlanType>Normal</PromotionalPlanType>
      </wsTicketRequest>
    </Ticket>
  </soap:Body>
</soap:Envelope>"""
                    )
                    print('----------inbound----------response----------------' + ticketResponse.text)

                    description = ticketResponse.TicketResponse.TicketResult.Status.Description
                    String statusCode = ticketResponse.TicketResponse.TicketResult.Status.StatusCode

                    if (statusCode.equals('14')) {
                        bookedFlight = new BookedFlight()
                        bookedFlight.pnr = ticketResponse.TicketResponse.TicketResult.PNR
                        bookedFlight.refId = ticketResponse.TicketResponse.TicketResult.RefId
                        bookedFlight.bookingId = ticketResponse.TicketResponse.TicketResult.BookingId
                        bookedFlight.user = newUser
                        bookedFlight.save(flush: true, failOnError: true)
                        newUser.addToBookedFlights(bookedFlight)

                        SOAPResponse addBookingDetailResponse = client.send("""<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
            <soap:Header>
                <AuthenticationData xmlns="http://192.168.0.170/TT/BookingAPI">
                <SiteName></SiteName>
                <AccountCode></AccountCode>
                <UserName>${grailsApplication.config.tbo.FLIGHT_USERNAME}</UserName>
                <Password>${grailsApplication.config.tbo.FLIGHT_PASS}</Password>
            </AuthenticationData>
            </soap:Header>
                <soap:Body>
                <AddBookingDetail xmlns="http://192.168.0.170/TT/BookingAPI">
                <saveRequest>
                    <RefId>${bookedFlight?.refId}</RefId>
                    <BookingStatus>Ticketed</BookingStatus>
                </saveRequest>
                </AddBookingDetail>
            </soap:Body>
            </soap:Envelope>"""
                        )
                        print('--------------addBookingDetailResponse--------------------' + addBookingDetailResponse.text)
//                        String source = fareQuotedResult.GetFareQuoteResponse.GetFareQuoteResult.Result.Source
                        String source = flightSearchResultVO?.flightFareRule?.first()?.source
                        print '------------------source-----------------' + source
                        SOAPResponse getBooking = client.send("""<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <AuthenticationData xmlns="http://192.168.0.170/TT/BookingAPI">
      <SiteName></SiteName>
      <AccountCode></AccountCode>
      <UserName>${grailsApplication.config.tbo.FLIGHT_USERNAME}</UserName>
      <Password>${grailsApplication.config.tbo.FLIGHT_PASS}</Password>
    </AuthenticationData>
  </soap:Header>
  <soap:Body>
    <GetBooking xmlns="http://192.168.0.170/TT/BookingAPI">
      <bookingRequest>
        <BookingId>${bookedFlight?.bookingId}</BookingId>
        <Pnr>${bookedFlight?.pnr}</Pnr>
        <Source>${source}</Source>
      </bookingRequest>
    </GetBooking>
  </soap:Body>
</soap:Envelope>"""
                        )

                        print('--------------getDetailResponse--------------------' + getBooking.text)

                        def bookingResult = getBooking.GetBookingResponse.GetBookingResult
                        bookedFlight.origin = bookingResult.Origin
                        bookedFlight.destination = bookingResult.Destination
                        bookedFlight.fareType = bookingResult.FareType
                        bookedFlight.source = bookingResult.Source
                        bookedFlight.status = bookingResult.Description
                        bookedFlight.save(flush: true, failOnError: true)

                        BookedFlightFare bookedFlightFare = new BookedFlightFare()
                        bookedFlightFare.additionalTxnFee = utilService.convertStringToBigdecimal(bookingResult.Fare.AdditionalTxnFee.text())
                        bookedFlightFare.agentCommission = utilService.convertStringToBigdecimal(bookingResult.Fare.AgentCommission.text())
                        bookedFlightFare.tdsOnCommission = utilService.convertStringToBigdecimal(bookingResult.Fare.TdsOnCommission.text())
                        bookedFlightFare.airTransFee = utilService.convertStringToBigdecimal(bookingResult.Fare.AirTransFee.text())
                        bookedFlightFare.baseFare = utilService.convertStringToBigdecimal(bookingResult.Fare.BaseFare.text())
                        bookedFlightFare.tax = utilService.convertStringToBigdecimal(bookingResult.Fare.Tax.text())
                        bookedFlightFare.otherCharges = utilService.convertStringToBigdecimal(bookingResult.Fare.OtherCharges.text())
                        bookedFlightFare.fuelSurcharge = utilService.convertStringToBigdecimal(bookingResult.Fare.FuelSurcharge.text())
                        bookedFlightFare.discount = utilService.convertStringToBigdecimal(bookingResult.Fare.Discount.text())
                        bookedFlightFare.serviceTax = utilService.convertStringToBigdecimal(bookingResult.Fare.ServiceTax.text())
                        bookedFlightFare.currency = bookingResult.Fare.Currency
                        bookedFlightFare.plb = utilService.convertStringToBigdecimal(bookingResult.Fare.PLBEarned.text())
                        bookedFlightFare.tdsOnPLB = utilService.convertStringToBigdecimal(bookingResult.Fare.TdsOnPLB.text())
                        bookedFlightFare.incentiveEarned = utilService.convertStringToBigdecimal(bookingResult.Fare.IncentiveEarned.text())
                        bookedFlightFare.tdsOnIncentive = utilService.convertStringToBigdecimal(bookingResult.Fare.TdsOnIncentive.text())
                        bookedFlightFare.offeredFare = utilService.convertStringToBigdecimal(bookingResult.Fare.OfferedFare.text())
                        bookedFlightFare.agentServiceCharge = utilService.convertStringToBigdecimal(bookingResult.Fare.AgentServiceCharge.text())
                        bookedFlightFare.publishedPrice = utilService.convertStringToBigdecimal(bookingResult.Fare.PublishedPrice.text())

                        bookedFlightFare.bookedFlight = bookedFlight
                        bookedFlightFare.save(flush: true, failOnError: true)

                        def passengerDetail = bookingResult.Passenger.WSPassenger
                        bookingResult.Passenger.WSPassenger.each {
                            BookedFlightPassenger bookedFlightPassenger = new BookedFlightPassenger()
                            bookedFlightPassenger.title = passengerDetail.Title
                            bookedFlightPassenger.firstName = passengerDetail.FirstName
                            bookedFlightPassenger.lastName = passengerDetail.LastName
                            bookedFlightPassenger.type = passengerDetail.Type
                            bookedFlightPassenger.gender = passengerDetail.Gender
//            bookedFlightPassenger.dateOfBirth = passengerDetail.DateOfBirth
                            bookedFlightPassenger.dateOfBirth = new Date()
                            bookedFlightPassenger.passportNumber = passengerDetail.PassportNumber
                            bookedFlightPassenger.passportExpiry = passengerDetail.PassportExpiry
                            bookedFlightPassenger.country = passengerDetail.Country
                            bookedFlightPassenger.phone = passengerDetail.Phone
                            bookedFlightPassenger.pincode = passengerDetail.PinCode
                            bookedFlightPassenger.address1 = passengerDetail.AddressLine1
                            bookedFlightPassenger.address2 = passengerDetail.AddressLine2
                            bookedFlightPassenger.email = passengerDetail.Email
                            bookedFlightPassenger.seat = passengerDetail.Seat
                            bookedFlightPassenger.meal = passengerDetail.Meal
                            bookedFlightPassenger.bookedFlight = bookedFlight
                            bookedFlightPassenger.save(flush: true, failOnError: true)
                            bookedFlight.addToBookedFlightPassanger(bookedFlightPassenger)
                        }
                        def ticketDetail = bookingResult.Ticket.WSTicket
                        bookingResult.Ticket.WSTicket.each {
                            BookedFlightTicket bookedFlightTicket = new BookedFlightTicket()
                            bookedFlightTicket.ticketId = utilService.convertStringToInteger(ticketDetail.TicketId.text())
                            bookedFlightTicket.ticketNumber = ticketDetail.TicketNumber
                            bookedFlightTicket.title = ticketDetail.Title
                            bookedFlightTicket.firstName = ticketDetail.FirstName
                            bookedFlightTicket.lastName = ticketDetail.LastName
                            bookedFlightTicket.passengerType = ticketDetail.PaxType
                            bookedFlightTicket.issueDate = new Date()
                            bookedFlightTicket.tourCode = ticketDetail.TourCode
                            bookedFlightTicket.bookedFlight = bookedFlight
                            bookedFlightTicket.save(flush: true, failOnError: true)
                            bookedFlight.addToBookedFlightTicket(bookedFlightTicket)
                        }

                    }
                }

            }


        }

        render(view: "/flights/tboFlightBookingIdDisplay", model:
                [bookedFlight: bookedFlight, description: description])
    }


    def flightBooking = {
        println "==========PARAMS==InBookFlight========" + params
        def flightSessionId = params.flightSessionId
        def flightCode = params.flightCode


        String adult1TName = params.adult1TName
        String adult1Age = params.adult1Age
        String adult1Email = params.adult1Email
        String adult1AddressOne = params.adult1AddressOne
        String adult1AddressTwo = params.adult1AddressTwo
        String adult1City = params.adult1City
        String adult1FName = params.adult1FName
        String adult1LName = params.adult1LName
        String adult1Phone = params.adult1Phone
        String adult1State = params.adult1State
        String adult1Zip = params.adult1Zip
        String adult1Country = params.adult1Country

        String adult2TName = params.adult2TName
        String adult2FName = params.adult2FName
        String adult2LName = params.adult2LName

        String adult3TName = params.adult3TName
        String adult3FName = params.adult3FName
        String adult3LName = params.adult3LName

        String adult4TName = params.adult4TName
        String adult4FName = params.adult4FName
        String adult4LName = params.adult4LName

        String adult5TName = params.adult5TName
        String adult5FName = params.adult5FName
        String adult5LName = params.adult5LName

        String adult6TName = params.adult6TName
        String adult6FName = params.adult6FName
        String adult6LName = params.adult6LName


        String child1TName = params.child1TName
        String child1FName = params.child1FName
        String child1LName = params.child1LName

        String child2TName = params.child2TName
        String child2FName = params.child2FName
        String child2LName = params.child2LName

        String child3TName = params.child3TName
        String child3FName = params.child3FName
        String child3LName = params.child3LName

        String child4TName = params.child4TName
        String child4FName = params.child4FName
        String child4LName = params.child4LName

        println "==========flightSessionId==========" + params.flightSessionId
        println "==========PARAMS==========" + params.flightCode

        List<FlightSearchResultVO> flightSearchVOList = session["flightSearchList"] as List
        flightSearchVOList.each {
            println "==========PARAMS==from airLineName========" + it.flightSegmentVO.first().airLineName
            println "==========PARAMS==from airLineCode========" + it.flightSegmentVO.first().airLineCode
            println "==========PARAMS==from flightNumber========" + it.flightSegmentVO.first().flightNumber

        }
        FlightSearchResultVO flightSearchVO = null
        flightSearchVOList.each {
            it.flightSegmentVO.each { segment ->
                if (segment.flightNumber == flightCode) {
                    flightSearchVO = it
                    println '=======Inside flight Segment======flightSearchVO.flightSegmentVO.first().airLineName=====' + flightSearchVO.flightSegmentVO.first().airLineName
                }

            }
        }
        println '================destination====' + flightSearchVO.destination
        println '================origin====' + flightSearchVO.origin
        println '================flightSearchVO.flightFare====' + flightSearchVO.flightFare.baseFare
        println '===flightSearchVO.flightFareRule?.first().source=============' + flightSearchVO.flightFareRule?.first().source
        println '===flightSearchVO.flightFareRule?.source=============' + flightSearchVO.flightFareRule?.source
        println '===flightSearchVO.flightFareRule?.first().origin=============' + flightSearchVO.flightFareRule?.first().origin
        println '===flightSearchVO.flightFareRule?.first().destination=============' + flightSearchVO.flightFareRule?.first().destination
        println '===flightSearchVO.flightFareRule?.first().airline=============' + flightSearchVO.flightFareRule?.first().airline
        println '===flightSearchVO.flightFareRule?.first().fareRestriction=============' + flightSearchVO.flightFareRule?.first().fareRestriction
        println '===flightSearchVO.flightFareRule?.first().fareBasisCode=============' + flightSearchVO.flightFareRule?.first().fareBasisCode
        println '===flightSearchVO.flightFareRule?.first().departureDate=============' + flightSearchVO.flightFareRule?.first().departureDate
        println '===flightSearchVO.flightFareRule?.first().returnDate=============' + flightSearchVO.flightFareRule?.first().returnDate
        println '================airLineCode====' + flightSearchVO.flightSegmentVO.first().airLineCode
        println '================departureTime====' + flightSearchVO.flightSegmentVO.first().departureTime
        println '================arrivalTime====' + flightSearchVO.flightSegmentVO.first().arrivalTime
        println '================flightFareRule.departureDate====' + flightSearchVO.flightFareRule.first().departureDate
        println '================flightSearchVO.flightFareRule.returnDate====' + flightSearchVO.flightFareRule.first().returnDate


        MessageFactory factory = MessageFactory.newInstance();
        SOAPMessage soapMsg = factory.createMessage();
        SOAPPart part = soapMsg.getSOAPPart();
        SOAPEnvelope envelope = part.getEnvelope();
        SOAPHeader header = envelope.getHeader();
        SOAPBody body = envelope.getBody();
        QName qName = new QName("http://192.168.0.170/TT/BookingAPI", "AuthenticationData");
        SOAPHeaderElement headerElement = header.addHeaderElement(qName);
//
        headerElement.addChildElement("UserName").addTextNode("${grailsApplication.config.tbo.TRAVELX_UNAME}")
        headerElement.addChildElement("Password").addTextNode("${grailsApplication.config.tbo.TRAVELX_PASS_HOTEL}")

        QName qName1 = new QName("http://192.168.0.170/TT/BookingAPI", "Book");

        SOAPBodyElement element = body.addBodyElement(qName1);

        def reqElementTag = element.addChildElement("bookRequest")
        reqElementTag.addChildElement("Remarks").addTextNode("FlightBook");
        reqElementTag.addChildElement("InstantTicket").addTextNode("true");
        def fare = reqElementTag.addChildElement("Fare");
        fare.addChildElement("BaseFare").addTextNode(flightSearchVO?.flightFare?.baseFare)
        fare.addChildElement("Tax").addTextNode(flightSearchVO?.flightFare?.tax)
        fare.addChildElement("ServiceTax").addTextNode(flightSearchVO?.flightFare?.serviceTax)
        fare.addChildElement("AdditionalTxnFee").addTextNode(flightSearchVO?.flightFare?.additionalTxnFee)
        fare.addChildElement("AgentCommission").addTextNode(flightSearchVO?.flightFare?.agentCommission)
        fare.addChildElement("TdsOnCommission").addTextNode(flightSearchVO?.flightFare?.tdsOnCommission)
        fare.addChildElement("IncentiveEarned").addTextNode(flightSearchVO?.flightFare?.incentiveEarned)
        fare.addChildElement("TdsOnIncentive").addTextNode(flightSearchVO?.flightFare?.tdsOnIncentive)
        fare.addChildElement("PLBEarned").addTextNode(flightSearchVO?.flightFare?.pLBEarned)
        fare.addChildElement("TdsOnPLB").addTextNode(flightSearchVO?.flightFare?.tdsOnPLB)
        fare.addChildElement("PublishedPrice").addTextNode(flightSearchVO?.flightFare?.publishedPrice)
        fare.addChildElement("AirTransFee").addTextNode(flightSearchVO?.flightFare?.airTransFee)
        fare.addChildElement("Currency").addTextNode(flightSearchVO?.flightFare?.currency)
        fare.addChildElement("Discount").addTextNode(flightSearchVO?.flightFare?.discount)
        fare.addChildElement("OtherCharges").addTextNode(flightSearchVO?.flightFare?.otherCharges)
        fare.addChildElement("FuelSurcharge").addTextNode(flightSearchVO?.flightFare?.fuelSurcharge)
        fare.addChildElement("TransactionFee").addTextNode(flightSearchVO?.flightFare?.transactionFee)
        fare.addChildElement("ReverseHandlingCharge").addTextNode(flightSearchVO?.flightFare?.reverseHandlingCharge)
        fare.addChildElement("OfferedFare").addTextNode(flightSearchVO?.flightFare?.offeredFare)
        fare.addChildElement("AgentServiceCharge").addTextNode(flightSearchVO?.flightFare?.agentServiceCharge)
        fare.addChildElement("AgentConvienceCharges").addTextNode(flightSearchVO?.flightFare?.agentConvienceCharges)

        def passenger = reqElementTag.addChildElement("Passenger");

        if (adult1FName) {
            print '-----adult1FName------------------' + adult1FName
            def wSPassenger = passenger.addChildElement("WSPassenger");
            wSPassenger.addChildElement("Title").addTextNode(adult1TName)
            wSPassenger.addChildElement("FirstName").addTextNode(adult1FName)
            wSPassenger.addChildElement("LastName").addTextNode(adult1LName)
            wSPassenger.addChildElement("Type").addTextNode("Adult")
            wSPassenger.addChildElement("DateOfBirth").addTextNode("1988-02-12T00:00:00")
            def passFare = wSPassenger.addChildElement("Fare");

            passFare.addChildElement("BaseFare").addTextNode(flightSearchVO?.flightFare?.baseFare)
            passFare.addChildElement("Tax").addTextNode(flightSearchVO?.flightFare?.tax)
            passFare.addChildElement("ServiceTax").addTextNode(flightSearchVO?.flightFare?.serviceTax)
            passFare.addChildElement("AdditionalTxnFee").addTextNode(flightSearchVO?.flightFare?.additionalTxnFee)
            passFare.addChildElement("AgentCommission").addTextNode(flightSearchVO?.flightFare?.agentCommission)
            passFare.addChildElement("TdsOnCommission").addTextNode(flightSearchVO?.flightFare?.tdsOnCommission)
            passFare.addChildElement("IncentiveEarned").addTextNode(flightSearchVO?.flightFare?.incentiveEarned)
            passFare.addChildElement("TdsOnIncentive").addTextNode(flightSearchVO?.flightFare?.tdsOnIncentive)
            passFare.addChildElement("PLBEarned").addTextNode(flightSearchVO?.flightFare?.pLBEarned)
            passFare.addChildElement("TdsOnPLB").addTextNode(flightSearchVO?.flightFare?.tdsOnPLB)
            passFare.addChildElement("PublishedPrice").addTextNode(flightSearchVO?.flightFare?.publishedPrice)
            passFare.addChildElement("AirTransFee").addTextNode(flightSearchVO?.flightFare?.airTransFee)
            passFare.addChildElement("Currency").addTextNode(flightSearchVO?.flightFare?.currency)
            //Make sure the currency
            passFare.addChildElement("Discount").addTextNode(flightSearchVO?.flightFare?.discount)
            passFare.addChildElement("OtherCharges").addTextNode(flightSearchVO?.flightFare?.otherCharges)
            passFare.addChildElement("FuelSurcharge").addTextNode(flightSearchVO?.flightFare?.fuelSurcharge)
            passFare.addChildElement("TransactionFee").addTextNode(flightSearchVO?.flightFare?.transactionFee)
            passFare.addChildElement("ReverseHandlingCharge").addTextNode(flightSearchVO?.flightFare?.reverseHandlingCharge)
            passFare.addChildElement("OfferedFare").addTextNode(flightSearchVO?.flightFare?.offeredFare)
            passFare.addChildElement("AgentServiceCharge").addTextNode(flightSearchVO?.flightFare?.agentServiceCharge)
            passFare.addChildElement("AgentConvienceCharges").addTextNode(flightSearchVO?.flightFare?.agentConvienceCharges)

            wSPassenger.addChildElement("Gender").addTextNode("Male")
            wSPassenger.addChildElement("PassportNumber").addTextNode("")
            wSPassenger.addChildElement("PassportExpiry").addTextNode("0001-01-01T00:00:00")
            wSPassenger.addChildElement("PinCode").addTextNode("")
            wSPassenger.addChildElement("Country").addTextNode("IN")//Country Code
            wSPassenger.addChildElement("Phone").addTextNode("9717544305")
            wSPassenger.addChildElement("AddressLine1").addTextNode("Cannought Place")
            wSPassenger.addChildElement("AddressLine2").addTextNode("")
            wSPassenger.addChildElement("Email").addTextNode(adult1Email)
            def meal = wSPassenger.addChildElement("Meal");
            meal.addChildElement("Code").addTextNode("")
            meal.addChildElement("Description").addTextNode("")
            def seat = wSPassenger.addChildElement("Seat");
            seat.addChildElement("Code").addTextNode("")
            seat.addChildElement("Description").addTextNode("")
            wSPassenger.addChildElement("FFAirline").addTextNode("")
            wSPassenger.addChildElement("FFNumber").addTextNode("")

        }

        if (adult2FName) {
            def wSPassenger = passenger.addChildElement("WSPassenger");
            wSPassenger.addChildElement("Title").addTextNode(adult2TName)
            wSPassenger.addChildElement("FirstName").addTextNode(adult2FName)
            wSPassenger.addChildElement("LastName").addTextNode(adult2LName)
            wSPassenger.addChildElement("Type").addTextNode("Adult")
            wSPassenger.addChildElement("DateOfBirth").addTextNode("1988-02-12T00:00:00")
            def passFare = wSPassenger.addChildElement("Fare");

            passFare.addChildElement("BaseFare").addTextNode(flightSearchVO?.flightFare?.baseFare)
            passFare.addChildElement("Tax").addTextNode(flightSearchVO?.flightFare?.tax)
            passFare.addChildElement("ServiceTax").addTextNode(flightSearchVO?.flightFare?.serviceTax)
            passFare.addChildElement("AdditionalTxnFee").addTextNode(flightSearchVO?.flightFare?.additionalTxnFee)
            passFare.addChildElement("AgentCommission").addTextNode(flightSearchVO?.flightFare?.agentCommission)
            passFare.addChildElement("TdsOnCommission").addTextNode(flightSearchVO?.flightFare?.tdsOnCommission)
            passFare.addChildElement("IncentiveEarned").addTextNode(flightSearchVO?.flightFare?.incentiveEarned)
            passFare.addChildElement("TdsOnIncentive").addTextNode(flightSearchVO?.flightFare?.tdsOnIncentive)
            passFare.addChildElement("PLBEarned").addTextNode(flightSearchVO?.flightFare?.pLBEarned)
            passFare.addChildElement("TdsOnPLB").addTextNode(flightSearchVO?.flightFare?.tdsOnPLB)
            passFare.addChildElement("PublishedPrice").addTextNode(flightSearchVO?.flightFare?.publishedPrice)
            passFare.addChildElement("AirTransFee").addTextNode(flightSearchVO?.flightFare?.airTransFee)
            passFare.addChildElement("Currency").addTextNode(flightSearchVO?.flightFare?.currency)
            //Make sure the currency
            passFare.addChildElement("Discount").addTextNode(flightSearchVO?.flightFare?.discount)
            passFare.addChildElement("OtherCharges").addTextNode(flightSearchVO?.flightFare?.otherCharges)
            passFare.addChildElement("FuelSurcharge").addTextNode(flightSearchVO?.flightFare?.fuelSurcharge)
            passFare.addChildElement("TransactionFee").addTextNode(flightSearchVO?.flightFare?.transactionFee)
            passFare.addChildElement("ReverseHandlingCharge").addTextNode(flightSearchVO?.flightFare?.reverseHandlingCharge)
            passFare.addChildElement("OfferedFare").addTextNode(flightSearchVO?.flightFare?.offeredFare)
            passFare.addChildElement("AgentServiceCharge").addTextNode(flightSearchVO?.flightFare?.agentServiceCharge)

            wSPassenger.addChildElement("Gender").addTextNode("Male")
            wSPassenger.addChildElement("PassportNumber").addTextNode("")
            wSPassenger.addChildElement("PassportExpiry").addTextNode("0001-01-01T00:00:00")
            wSPassenger.addChildElement("PinCode").addTextNode("")
            wSPassenger.addChildElement("Country").addTextNode("IN")//Country Code
            wSPassenger.addChildElement("Phone").addTextNode("9717544305")
            wSPassenger.addChildElement("AddressLine1").addTextNode("Cannout Place")
            wSPassenger.addChildElement("AddressLine2").addTextNode("")
            wSPassenger.addChildElement("Email").addTextNode(adult1Email)
            def meal = wSPassenger.addChildElement("Meal");
            meal.addChildElement("Code").addTextNode("")
            meal.addChildElement("Description").addTextNode("")
            def seat = wSPassenger.addChildElement("Seat");
            seat.addChildElement("Code").addTextNode("")
            seat.addChildElement("Description").addTextNode("")
            wSPassenger.addChildElement("FFAirline").addTextNode("")
            wSPassenger.addChildElement("FFNumber").addTextNode("")
        }

        if (adult3FName) {
            def wSPassenger = passenger.addChildElement("WSPassenger");
            wSPassenger.addChildElement("Title").addTextNode(adult3TName)
            wSPassenger.addChildElement("FirstName").addTextNode(adult3FName)
            wSPassenger.addChildElement("LastName").addTextNode(adult3LName)
            wSPassenger.addChildElement("Type").addTextNode("Adult")
            wSPassenger.addChildElement("DateOfBirth").addTextNode("1988-02-12T00:00:00")
            def passFare = wSPassenger.addChildElement("Fare");

            passFare.addChildElement("BaseFare").addTextNode(flightSearchVO?.fareBreakDown[0]?.baseFare)
            passFare.addChildElement("Tax").addTextNode(flightSearchVO?.fareBreakDown[0]?.tax)
            passFare.addChildElement("ServiceTax").addTextNode("0")
            passFare.addChildElement("AdditionalTxnFee").addTextNode(flightSearchVO?.fareBreakDown[0]?.additionalTxnFee)
            passFare.addChildElement("AgentCommission").addTextNode("0")
            passFare.addChildElement("TdsOnCommission").addTextNode("0")
            passFare.addChildElement("IncentiveEarned").addTextNode("0")
            passFare.addChildElement("TdsOnIncentive").addTextNode("0")
            passFare.addChildElement("PLBEarned").addTextNode("0")
            passFare.addChildElement("TdsOnPLB").addTextNode("0")
            passFare.addChildElement("PublishedPrice").addTextNode("0")
            passFare.addChildElement("AirTransFee").addTextNode("0")
            passFare.addChildElement("Currency").addTextNode("INR") //Make sure the currency
            passFare.addChildElement("Discount").addTextNode("0")
            passFare.addChildElement("OtherCharges").addTextNode("0")
            passFare.addChildElement("FuelSurcharge").addTextNode(flightSearchVO?.fareBreakDown[0]?.fuelSurcharge)
            passFare.addChildElement("TransactionFee").addTextNode("0")
            passFare.addChildElement("ReverseHandlingCharge").addTextNode("0")
            passFare.addChildElement("OfferedFare").addTextNode("0")
            passFare.addChildElement("AgentServiceCharge").addTextNode(flightSearchVO?.fareBreakDown[0]?.agentServiceCharge)

            wSPassenger.addChildElement("Gender").addTextNode("Male")
            wSPassenger.addChildElement("PassportNumber").addTextNode("")
            wSPassenger.addChildElement("PassportExpiry").addTextNode("0001-01-01T00:00:00")
            wSPassenger.addChildElement("PinCode").addTextNode("")
            wSPassenger.addChildElement("Country").addTextNode("IN")//Country Code
            wSPassenger.addChildElement("Phone").addTextNode("9717544305")
            wSPassenger.addChildElement("AddressLine1").addTextNode("Cannout Place")
            wSPassenger.addChildElement("AddressLine2").addTextNode("")
            wSPassenger.addChildElement("Email").addTextNode(adult1Email)
            def meal = wSPassenger.addChildElement("Meal");
            meal.addChildElement("Code").addTextNode("")
            meal.addChildElement("Description").addTextNode("")
            def seat = wSPassenger.addChildElement("Seat");
            seat.addChildElement("Code").addTextNode("")
            seat.addChildElement("Description").addTextNode("")
            wSPassenger.addChildElement("FFAirline").addTextNode("")
            wSPassenger.addChildElement("FFNumber").addTextNode("")
        }
        if (adult4FName) {
            def wSPassenger = passenger.addChildElement("WSPassenger");
            wSPassenger.addChildElement("Title").addTextNode(adult4TName)
            wSPassenger.addChildElement("FirstName").addTextNode(adult4FName)
            wSPassenger.addChildElement("LastName").addTextNode(adult4LName)
            wSPassenger.addChildElement("Type").addTextNode("Adult")
            wSPassenger.addChildElement("DateOfBirth").addTextNode("1988-02-12T00:00:00")
            def passFare = wSPassenger.addChildElement("Fare");

            passFare.addChildElement("BaseFare").addTextNode(flightSearchVO?.fareBreakDown[0]?.baseFare)
            passFare.addChildElement("Tax").addTextNode(flightSearchVO?.fareBreakDown[0]?.tax)
            passFare.addChildElement("ServiceTax").addTextNode("0")
            passFare.addChildElement("AdditionalTxnFee").addTextNode(flightSearchVO?.fareBreakDown[0]?.additionalTxnFee)
            passFare.addChildElement("AgentCommission").addTextNode("0")
            passFare.addChildElement("TdsOnCommission").addTextNode("0")
            passFare.addChildElement("IncentiveEarned").addTextNode("0")
            passFare.addChildElement("TdsOnIncentive").addTextNode("0")
            passFare.addChildElement("PLBEarned").addTextNode("0")
            passFare.addChildElement("TdsOnPLB").addTextNode("0")
            passFare.addChildElement("PublishedPrice").addTextNode("0")
            passFare.addChildElement("AirTransFee").addTextNode("0")
            passFare.addChildElement("Currency").addTextNode("INR") //Make sure the currency
            passFare.addChildElement("Discount").addTextNode("0")
            passFare.addChildElement("OtherCharges").addTextNode("0")
            passFare.addChildElement("FuelSurcharge").addTextNode(flightSearchVO?.fareBreakDown[0]?.fuelSurcharge)
            passFare.addChildElement("TransactionFee").addTextNode("0")
            passFare.addChildElement("ReverseHandlingCharge").addTextNode("0")
            passFare.addChildElement("OfferedFare").addTextNode("0")
            passFare.addChildElement("AgentServiceCharge").addTextNode(flightSearchVO?.fareBreakDown[0]?.agentServiceCharge)

            wSPassenger.addChildElement("Gender").addTextNode("Male")
            wSPassenger.addChildElement("PassportNumber").addTextNode("")
            wSPassenger.addChildElement("PassportExpiry").addTextNode("0001-01-01T00:00:00")
            wSPassenger.addChildElement("PinCode").addTextNode("")
            wSPassenger.addChildElement("Country").addTextNode("IN")//Country Code
            wSPassenger.addChildElement("Phone").addTextNode("9717544305")
            wSPassenger.addChildElement("AddressLine1").addTextNode("Cannout Place")
            wSPassenger.addChildElement("AddressLine2").addTextNode("")
            wSPassenger.addChildElement("Email").addTextNode(adult1Email)
            def meal = wSPassenger.addChildElement("Meal");
            meal.addChildElement("Code").addTextNode("")
            meal.addChildElement("Description").addTextNode("")
            def seat = wSPassenger.addChildElement("Seat");
            seat.addChildElement("Code").addTextNode("")
            seat.addChildElement("Description").addTextNode("")
            wSPassenger.addChildElement("FFAirline").addTextNode("")
            wSPassenger.addChildElement("FFNumber").addTextNode("")
        }
        if (adult5FName) {
            def wSPassenger = passenger.addChildElement("WSPassenger");
            wSPassenger.addChildElement("Title").addTextNode(adult5TName)
            wSPassenger.addChildElement("FirstName").addTextNode(adult5FName)
            wSPassenger.addChildElement("LastName").addTextNode(adult5LName)
            wSPassenger.addChildElement("Type").addTextNode("Adult")
            wSPassenger.addChildElement("DateOfBirth").addTextNode("1988-02-12T00:00:00")
            def passFare = wSPassenger.addChildElement("Fare");

            passFare.addChildElement("BaseFare").addTextNode(flightSearchVO?.fareBreakDown[0]?.baseFare)
            passFare.addChildElement("Tax").addTextNode(flightSearchVO?.fareBreakDown[0]?.tax)
            passFare.addChildElement("ServiceTax").addTextNode("0")
            passFare.addChildElement("AdditionalTxnFee").addTextNode(flightSearchVO?.fareBreakDown[0]?.additionalTxnFee)
            passFare.addChildElement("AgentCommission").addTextNode("0")
            passFare.addChildElement("TdsOnCommission").addTextNode("0")
            passFare.addChildElement("IncentiveEarned").addTextNode("0")
            passFare.addChildElement("TdsOnIncentive").addTextNode("0")
            passFare.addChildElement("PLBEarned").addTextNode("0")
            passFare.addChildElement("TdsOnPLB").addTextNode("0")
            passFare.addChildElement("PublishedPrice").addTextNode("0")
            passFare.addChildElement("AirTransFee").addTextNode("0")
            passFare.addChildElement("Currency").addTextNode("INR") //Make sure the currency
            passFare.addChildElement("Discount").addTextNode("0")
            passFare.addChildElement("OtherCharges").addTextNode("0")
            passFare.addChildElement("FuelSurcharge").addTextNode(flightSearchVO?.fareBreakDown[0]?.fuelSurcharge)
            passFare.addChildElement("TransactionFee").addTextNode("0")
            passFare.addChildElement("ReverseHandlingCharge").addTextNode("0")
            passFare.addChildElement("OfferedFare").addTextNode("0")
            passFare.addChildElement("AgentServiceCharge").addTextNode(flightSearchVO?.fareBreakDown[0]?.agentServiceCharge)

            wSPassenger.addChildElement("Gender").addTextNode("Male")
            wSPassenger.addChildElement("PassportNumber").addTextNode("")
            wSPassenger.addChildElement("PassportExpiry").addTextNode("0001-01-01T00:00:00")
            wSPassenger.addChildElement("PinCode").addTextNode("")
            wSPassenger.addChildElement("Country").addTextNode("IN")//Country Code
            wSPassenger.addChildElement("Phone").addTextNode("9717544305")
            wSPassenger.addChildElement("AddressLine1").addTextNode("Cannout Place")
            wSPassenger.addChildElement("AddressLine2").addTextNode("")
            wSPassenger.addChildElement("Email").addTextNode(adult1Email)
            def meal = wSPassenger.addChildElement("Meal");
            meal.addChildElement("Code").addTextNode("")
            meal.addChildElement("Description").addTextNode("")
            def seat = wSPassenger.addChildElement("Seat");
            seat.addChildElement("Code").addTextNode("")
            seat.addChildElement("Description").addTextNode("")
            wSPassenger.addChildElement("FFAirline").addTextNode("")
            wSPassenger.addChildElement("FFNumber").addTextNode("")
        }
        if (child1FName) {
            def wSPassenger = passenger.addChildElement("WSPassenger");
            wSPassenger.addChildElement("Title").addTextNode(child1TName)
            wSPassenger.addChildElement("FirstName").addTextNode(child1FName)
            wSPassenger.addChildElement("LastName").addTextNode(child1LName)
            wSPassenger.addChildElement("Type").addTextNode("Adult")
            wSPassenger.addChildElement("DateOfBirth").addTextNode("1988-02-12T00:00:00")
            def passFare = wSPassenger.addChildElement("Fare");

            passFare.addChildElement("BaseFare").addTextNode(flightSearchVO?.fareBreakDown[1]?.baseFare)
            passFare.addChildElement("Tax").addTextNode(flightSearchVO?.fareBreakDown[1]?.tax)
            passFare.addChildElement("ServiceTax").addTextNode("0")
            passFare.addChildElement("AdditionalTxnFee").addTextNode(flightSearchVO?.fareBreakDown[1]?.additionalTxnFee)
            passFare.addChildElement("AgentCommission").addTextNode("0")
            passFare.addChildElement("TdsOnCommission").addTextNode("0")
            passFare.addChildElement("IncentiveEarned").addTextNode("0")
            passFare.addChildElement("TdsOnIncentive").addTextNode("0")
            passFare.addChildElement("PLBEarned").addTextNode("0")
            passFare.addChildElement("TdsOnPLB").addTextNode("0")
            passFare.addChildElement("PublishedPrice").addTextNode("0")
            passFare.addChildElement("AirTransFee").addTextNode("0")
            passFare.addChildElement("Currency").addTextNode("INR") //Make sure the currency
            passFare.addChildElement("Discount").addTextNode("0")
            passFare.addChildElement("OtherCharges").addTextNode("0")
            passFare.addChildElement("FuelSurcharge").addTextNode(flightSearchVO?.fareBreakDown[1]?.fuelSurcharge)
            passFare.addChildElement("TransactionFee").addTextNode("0")
            passFare.addChildElement("ReverseHandlingCharge").addTextNode("0")
            passFare.addChildElement("OfferedFare").addTextNode("0")
            passFare.addChildElement("AgentServiceCharge").addTextNode(flightSearchVO?.fareBreakDown[1]?.agentServiceCharge)

            wSPassenger.addChildElement("Gender").addTextNode("Male")
            wSPassenger.addChildElement("PassportNumber").addTextNode("")
            wSPassenger.addChildElement("PassportExpiry").addTextNode("0001-01-01T00:00:00")
            wSPassenger.addChildElement("PinCode").addTextNode("")
            wSPassenger.addChildElement("Country").addTextNode("IN")//Country Code
            wSPassenger.addChildElement("Phone").addTextNode("9717544305")
            wSPassenger.addChildElement("AddressLine1").addTextNode("Cannout Place")
            wSPassenger.addChildElement("AddressLine2").addTextNode("")
            wSPassenger.addChildElement("Email").addTextNode(adult1Email)
            def meal = wSPassenger.addChildElement("Meal");
            meal.addChildElement("Code").addTextNode("")
            meal.addChildElement("Description").addTextNode("")
            def seat = wSPassenger.addChildElement("Seat");
            seat.addChildElement("Code").addTextNode("")
            seat.addChildElement("Description").addTextNode("")
            wSPassenger.addChildElement("FFAirline").addTextNode("")
            wSPassenger.addChildElement("FFNumber").addTextNode("")
        }
        if (child2FName) {
            def wSPassenger = passenger.addChildElement("WSPassenger");
            wSPassenger.addChildElement("Title").addTextNode(child2TName)
            wSPassenger.addChildElement("FirstName").addTextNode(child2FName)
            wSPassenger.addChildElement("LastName").addTextNode(child2LName)
            wSPassenger.addChildElement("Type").addTextNode("Adult")
            wSPassenger.addChildElement("DateOfBirth").addTextNode("1988-02-12T00:00:00")
            def passFare = wSPassenger.addChildElement("Fare");

            passFare.addChildElement("BaseFare").addTextNode(flightSearchVO?.fareBreakDown[1]?.baseFare)
            passFare.addChildElement("Tax").addTextNode(flightSearchVO?.fareBreakDown[1]?.tax)
            passFare.addChildElement("ServiceTax").addTextNode("0")
            passFare.addChildElement("AdditionalTxnFee").addTextNode(flightSearchVO?.fareBreakDown[1]?.additionalTxnFee)
            passFare.addChildElement("AgentCommission").addTextNode("0")
            passFare.addChildElement("TdsOnCommission").addTextNode("0")
            passFare.addChildElement("IncentiveEarned").addTextNode("0")
            passFare.addChildElement("TdsOnIncentive").addTextNode("0")
            passFare.addChildElement("PLBEarned").addTextNode("0")
            passFare.addChildElement("TdsOnPLB").addTextNode("0")
            passFare.addChildElement("PublishedPrice").addTextNode("0")
            passFare.addChildElement("AirTransFee").addTextNode("0")
            passFare.addChildElement("Currency").addTextNode("INR") //Make sure the currency
            passFare.addChildElement("Discount").addTextNode("0")
            passFare.addChildElement("OtherCharges").addTextNode("0")
            passFare.addChildElement("FuelSurcharge").addTextNode(flightSearchVO?.fareBreakDown[1]?.fuelSurcharge)
            passFare.addChildElement("TransactionFee").addTextNode("0")
            passFare.addChildElement("ReverseHandlingCharge").addTextNode("0")
            passFare.addChildElement("OfferedFare").addTextNode("0")
            passFare.addChildElement("AgentServiceCharge").addTextNode(flightSearchVO?.fareBreakDown[1]?.agentServiceCharge)

            wSPassenger.addChildElement("Gender").addTextNode("Male")
            wSPassenger.addChildElement("PassportNumber").addTextNode("")
            wSPassenger.addChildElement("PassportExpiry").addTextNode("0001-01-01T00:00:00")
            wSPassenger.addChildElement("PinCode").addTextNode("")
            wSPassenger.addChildElement("Country").addTextNode("IN")//Country Code
            wSPassenger.addChildElement("Phone").addTextNode("9717544305")
            wSPassenger.addChildElement("AddressLine1").addTextNode("Cannout Place")
            wSPassenger.addChildElement("AddressLine2").addTextNode("")
            wSPassenger.addChildElement("Email").addTextNode(adult1Email)
            def meal = wSPassenger.addChildElement("Meal");
            meal.addChildElement("Code").addTextNode("")
            meal.addChildElement("Description").addTextNode("")
            def seat = wSPassenger.addChildElement("Seat");
            seat.addChildElement("Code").addTextNode("")
            seat.addChildElement("Description").addTextNode("")
            wSPassenger.addChildElement("FFAirline").addTextNode("")
            wSPassenger.addChildElement("FFNumber").addTextNode("")
        }
        if (child3FName) {
            def wSPassenger = passenger.addChildElement("WSPassenger");
            wSPassenger.addChildElement("Title").addTextNode(child3TName)
            wSPassenger.addChildElement("FirstName").addTextNode(child3FName)
            wSPassenger.addChildElement("LastName").addTextNode(child3LName)
            wSPassenger.addChildElement("Type").addTextNode("Adult")
            wSPassenger.addChildElement("DateOfBirth").addTextNode("1988-02-12T00:00:00")
            def passFare = wSPassenger.addChildElement("Fare");

            passFare.addChildElement("BaseFare").addTextNode(flightSearchVO?.fareBreakDown[1]?.baseFare)
            passFare.addChildElement("Tax").addTextNode(flightSearchVO?.fareBreakDown[1]?.tax)
            passFare.addChildElement("ServiceTax").addTextNode("0")
            passFare.addChildElement("AdditionalTxnFee").addTextNode(flightSearchVO?.fareBreakDown[1]?.additionalTxnFee)
            passFare.addChildElement("AgentCommission").addTextNode("0")
            passFare.addChildElement("TdsOnCommission").addTextNode("0")
            passFare.addChildElement("IncentiveEarned").addTextNode("0")
            passFare.addChildElement("TdsOnIncentive").addTextNode("0")
            passFare.addChildElement("PLBEarned").addTextNode("0")
            passFare.addChildElement("TdsOnPLB").addTextNode("0")
            passFare.addChildElement("PublishedPrice").addTextNode("0")
            passFare.addChildElement("AirTransFee").addTextNode("0")
            passFare.addChildElement("Currency").addTextNode("INR") //Make sure the currency
            passFare.addChildElement("Discount").addTextNode("0")
            passFare.addChildElement("OtherCharges").addTextNode("0")
            passFare.addChildElement("FuelSurcharge").addTextNode(flightSearchVO?.fareBreakDown[1]?.fuelSurcharge)
            passFare.addChildElement("TransactionFee").addTextNode("0")
            passFare.addChildElement("ReverseHandlingCharge").addTextNode("0")
            passFare.addChildElement("OfferedFare").addTextNode("0")
            passFare.addChildElement("AgentServiceCharge").addTextNode(flightSearchVO?.fareBreakDown[1]?.agentServiceCharge)

            wSPassenger.addChildElement("Gender").addTextNode("Male")
            wSPassenger.addChildElement("PassportNumber").addTextNode("")
            wSPassenger.addChildElement("PassportExpiry").addTextNode("0001-01-01T00:00:00")
            wSPassenger.addChildElement("PinCode").addTextNode("")
            wSPassenger.addChildElement("Country").addTextNode("IN")//Country Code
            wSPassenger.addChildElement("Phone").addTextNode("9717544305")
            wSPassenger.addChildElement("AddressLine1").addTextNode("Cannout Place")
            wSPassenger.addChildElement("AddressLine2").addTextNode("")
            wSPassenger.addChildElement("Email").addTextNode(adult1Email)
            def meal = wSPassenger.addChildElement("Meal");
            meal.addChildElement("Code").addTextNode("")
            meal.addChildElement("Description").addTextNode("")
            def seat = wSPassenger.addChildElement("Seat");
            seat.addChildElement("Code").addTextNode("")
            seat.addChildElement("Description").addTextNode("")
            wSPassenger.addChildElement("FFAirline").addTextNode("")
            wSPassenger.addChildElement("FFNumber").addTextNode("")
        }


        reqElementTag.addChildElement("Origin").addTextNode(flightSearchVO?.origin);
        reqElementTag.addChildElement("Destination").addTextNode(flightSearchVO?.destination);
        def segment = reqElementTag.addChildElement("Segment");
        def wsSegment = segment.addChildElement("WSSegment");
        wsSegment.addChildElement("SegmentIndicator").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.segmentIndicator);
        def airLine = wsSegment.addChildElement("Airline");
        airLine.addChildElement("AirlineCode").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.airLineCode);
        airLine.addChildElement("AirlineName").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.airLineName);
        airLine.addChildElement("AirLineRemarks").addTextNode("");

        wsSegment.addChildElement("FlightNumber").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.flightNumber);
        wsSegment.addChildElement("FareClass").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.fareClass);
        def origin = wsSegment.addChildElement("Origin");
        origin.addChildElement("AirportCode").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.originAirPortCode);
        origin.addChildElement("AirportName").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.originCountryName);
        origin.addChildElement("Terminal").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.originTerminal);
        origin.addChildElement("CityCode").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.originCityCode);
        origin.addChildElement("CityName").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.originCityName);
        origin.addChildElement("CountryCode").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.originCountryCode);
        origin.addChildElement("CountryName").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.originCountryName);

        def destination = wsSegment.addChildElement("Destination");
        destination.addChildElement("AirportCode").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.destinationAirPortCode);
        destination.addChildElement("AirportName").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.destinationAirPortName);
        destination.addChildElement("Terminal").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.destinationTerminal);
        destination.addChildElement("CityCode").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.destinationCityCode);
        destination.addChildElement("CityName").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.destinationCityName);
        destination.addChildElement("CountryCode").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.destinationCountryCode);
        destination.addChildElement("CountryName").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.destinationCountryName);

        wsSegment.addChildElement("DepTIme").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.departureTime);
        wsSegment.addChildElement("ArrTime").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.arrivalTime);
        wsSegment.addChildElement("ETicketEligible").addTextNode("true");
        wsSegment.addChildElement("Duration").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.duration);
        wsSegment.addChildElement("Stop").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.stops);
        wsSegment.addChildElement("Craft").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.craftType);
        wsSegment.addChildElement("Status").addTextNode("Confirmed");
        wsSegment.addChildElement("OperatingCarrier").addTextNode(flightSearchVO?.flightSegmentVO?.first()?.operatingCarrier);

        reqElementTag.addChildElement("FareType").addTextNode("PUB");
        def fareRule = reqElementTag.addChildElement("FareRule");
        def wsFareRule = fareRule.addChildElement("WSFareRule");

        wsFareRule.addChildElement("Origin").addTextNode(flightSearchVO?.flightFareRule?.first()?.origin);
        wsFareRule.addChildElement("Destination").addTextNode(flightSearchVO?.flightFareRule?.first()?.destination);
        wsFareRule.addChildElement("Airline").addTextNode(flightSearchVO?.flightFareRule?.first()?.airline);
        wsFareRule.addChildElement("FareRestriction").addTextNode(flightSearchVO?.flightFareRule?.first()?.fareRestriction);
        wsFareRule.addChildElement("FareBasisCode").addTextNode(flightSearchVO?.flightFareRule?.first()?.fareBasisCode);
        wsFareRule.addChildElement("DepartureDate").addTextNode(flightSearchVO?.flightFareRule?.first()?.departureDate);
        wsFareRule.addChildElement("ReturnDate").addTextNode(flightSearchVO?.flightFareRule?.first()?.returnDate);
        wsFareRule.addChildElement("Source").addTextNode(flightSearchVO?.flightFareRule?.first()?.source);

        reqElementTag.addChildElement("Source").addTextNode("Amadeus");
        def paymentInfo = reqElementTag.addChildElement("PaymentInformation");
        paymentInfo.addChildElement("PaymentInformationId").addTextNode("0");
        paymentInfo.addChildElement("InvoiceNumber").addTextNode("0");
        paymentInfo.addChildElement("PaymentId").addTextNode("0");
        paymentInfo.addChildElement("Amount").addTextNode(flightSearchVO?.flightFare?.offeredFare);
        paymentInfo.addChildElement("IPAddress").addTextNode("127.0.0.1");
        paymentInfo.addChildElement("TrackId").addTextNode("0");
        paymentInfo.addChildElement("PaymentGateway").addTextNode("APICustomer");
        paymentInfo.addChildElement("PaymentModeType").addTextNode("Deposited");

        reqElementTag.addChildElement("SessionId").addTextNode(flightSearchVO?.sessionId);
        reqElementTag.addChildElement("PromotionalPlanType").addTextNode("Normal");

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream()
        soapMsg.writeTo(outputStream)

        String requestNew = "<?xml version='1.0' encoding='utf-16'?>" + outputStream.toString()

        println '---=requestNew ==========' + requestNew
        def client = new SOAPClient("http://api.tektravels.com/tboapi_v7/service.asmx")
        SOAPResponse response = client.send(requestNew)
        println '-------------==BOOK Response===-----------' + response.text
        def bookresponse = new XmlSlurper().parseText(response.text)
        println '-------------==bookresponse==--------' + bookresponse
        String pnr = bookresponse.children().BookResponse.BookResult.PNR
        print '--------pnr--------' + pnr
        String bookingId = bookresponse.children().BookResponse.BookResult.BookingId
        print '--------bookingId--------' + bookingId
        print '----${flightSearchVO?.flightFareRule?.source?.first()}----' + flightSearchVO?.flightFareRule?.source?.first()
        String statusCode = bookresponse.children().BookResponse.BookResult.Status.StatusCode

        String userName = adult1Email
        User newUser = User.findByUsername(userName)
        Role role = Role.findByAuthority('ROLE_USER')
        String pass
        if (newUser) {
        } else {
            newUser = new User()
            newUser.firstName = params.adult1FName
            newUser.lastName = params.adult1LName
            newUser.username = params.adult1Email
            newUser.mobNo = params.adult1Phone
            String randomAlphanumeric = RandomStringUtils.randomAlphanumeric(8)
            Date today = new Date()
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat('dd/MM/yyyy')
            String[] date = simpleDateFormat.format(today).split('/')
            newUser.coustId = "TR" + "CS" + randomAlphanumeric.toUpperCase() + "${date[0]}" + "${date[1]}" + "${date[2]}"
            newUser.accountExpired = false
            newUser.accountLocked = false
            newUser.hasVerifiedEmail = true
            newUser.passwordExpired = false
            newUser.enabled = true
            newUser.pictureName = "picNotAvailable"
            newUser.pictureType = "jpeg"
            pass = RandomStringUtils.randomAlphanumeric(8)
            print('-------------- user pass is--------------------' + pass)
            newUser.password = pass
            newUser.save(flush: true)
            UserRole.create(newUser, role, true)
        }
        print '---------flightSearchVO?.flightFareRule?.first()?.source----------' + flightSearchVO?.flightFareRule?.first()?.source
        BookedFlight bookedFlight
        if (statusCode.equals('06')) {
            bookedFlight = new BookedFlight()
            bookedFlight.pnr = pnr
            bookedFlight.refId = "NonLcc"
            bookedFlight.bookingId = bookingId
            bookedFlight.user = newUser
            bookedFlight.save(flush: true, failOnError: true)
            newUser.addToBookedFlights(bookedFlight)

            SOAPResponse bookedTicketDetail = client.send("""<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <AuthenticationData xmlns="http://192.168.0.170/TT/BookingAPI">
      <SiteName></SiteName>
      <AccountCode></AccountCode>
      <UserName>${grailsApplication.config.tbo.FLIGHT_USERNAME}</UserName>
      <Password>${grailsApplication.config.tbo.FLIGHT_PASS}</Password>
    </AuthenticationData>
  </soap:Header>
  <soap:Body>
    <Ticket xmlns="http://192.168.0.170/TT/BookingAPI">
      <wsTicketRequest>
        <BookingID>${bookedFlight?.bookingId}</BookingID>
        <Origin>${flightSearchVO?.flightFareRule?.origin?.first()}</Origin>
        <Destination>${flightSearchVO?.flightFareRule?.destination?.first()}</Destination>
        <Segment>
          <WSSegment>
            <SegmentIndicator>${flightSearchVO?.flightSegmentVO?.segmentIndicator?.first()?.toInteger()}</SegmentIndicator>
            <Airline>
            <AirlineCode>${flightSearchVO?.flightSegmentVO?.airLineCode?.first()}</AirlineCode>
            <AirlineName>${flightSearchVO?.flightSegmentVO?.airLineName?.first()}</AirlineName>
            </Airline>
            <FlightNumber>${flightSearchVO?.flightSegmentVO?.flightNumber?.first()}</FlightNumber>
            <FareClass>${flightSearchVO?.flightSegmentVO?.fareClass?.first()}</FareClass>
            <AirlinePNR>string</AirlinePNR>
            <Origin>
                <AirportCode>${flightSearchVO?.flightSegmentVO?.originAirPortCode?.first()}</AirportCode>
                <AirportName>${flightSearchVO?.flightSegmentVO?.originAirPortName?.first()}</AirportName>
                <Terminal />
                <CityCode>${flightSearchVO?.flightSegmentVO?.originCityCode?.first()}</CityCode>
                <CityName>${flightSearchVO?.flightSegmentVO?.originCityName?.first()}</CityName>
                <CountryCode>${flightSearchVO?.flightSegmentVO?.originCountryCode?.first()}</CountryCode>
                <CountryName>${flightSearchVO?.flightSegmentVO?.originCountryName?.first()}</CountryName>
            </Origin>
            <Destination>
                <AirportCode>${flightSearchVO?.flightSegmentVO?.destinationAirPortCode?.first()}</AirportCode>
                <AirportName>${flightSearchVO?.flightSegmentVO?.destinationAirPortName?.first()}</AirportName>
                <Terminal />
                <CityCode>${flightSearchVO?.flightSegmentVO?.destinationCityCode?.first()}</CityCode>
                <CityName>${flightSearchVO?.flightSegmentVO?.destinationCityName?.first()}</CityName>
                <CountryCode>${flightSearchVO?.flightSegmentVO?.destinationCountryCode?.first()}</CountryCode>
                <CountryName>${flightSearchVO?.flightSegmentVO?.destinationCountryName?.first()}</CountryName>
            </Destination>
            <DepTIme>${flightSearchVO?.flightSegmentVO?.departureTime?.first()}</DepTIme>
            <ArrTime>${flightSearchVO?.flightSegmentVO?.arrivalTime?.first()}</ArrTime>
            <ETicketEligible>${flightSearchVO?.flightSegmentVO?.eTicketEligible?.first()}</ETicketEligible>
            <Duration>${flightSearchVO?.flightSegmentVO?.duration?.first()}</Duration>
            <Stop>${flightSearchVO?.flightSegmentVO?.stops?.first()?.toInteger()}</Stop>
            <Craft>${flightSearchVO?.flightSegmentVO?.craftType?.first()}</Craft>
            <Status>${flightSearchVO?.flightSegmentVO?.status?.first()}</Status>
            <OperatingCarrier>string</OperatingCarrier>
          </WSSegment>
        </Segment>
        <FareType>string</FareType>
        <FareRule>
          <WSFareRule>
            <Origin>${flightSearchVO?.flightFareRule?.origin?.first()}</Origin>
            <Destination>${flightSearchVO?.flightFareRule?.destination?.first()}</Destination>
            <Airline>${flightSearchVO?.flightFareRule?.airline?.first()}</Airline>
            <FareRestriction>${flightSearchVO?.flightFareRule?.fareRestriction?.first()}</FareRestriction>
            <FareBasisCode>${flightSearchVO?.flightFareRule?.fareBasisCode?.first()}</FareBasisCode>
            <FareRuleDetail></FareRuleDetail>
            <DepartureDate>${flightSearchVO?.flightFareRule?.departureDate?.first()}</DepartureDate>
            <ReturnDate>${flightSearchVO?.flightFareRule?.returnDate?.first()}</ReturnDate>
            <Source>${flightSearchVO?.flightFareRule?.source?.first()}</Source>
          </WSFareRule>
        </FareRule>
        <Fare>
          <BaseFare>${flightSearchVO?.flightFare?.baseFare}</BaseFare>
          <Tax>${flightSearchVO?.flightFare?.tax}</Tax>
          <ServiceTax>${flightSearchVO?.flightFare?.serviceTax}</ServiceTax>
          <AdditionalTxnFee>${flightSearchVO?.flightFare?.additionalTxnFee}</AdditionalTxnFee>
          <AgentCommission>${flightSearchVO?.flightFare?.agentCommission}</AgentCommission>
          <TdsOnCommission>${flightSearchVO?.flightFare?.tdsOnCommission}</TdsOnCommission>
          <IncentiveEarned>${flightSearchVO?.flightFare?.incentiveEarned}</IncentiveEarned>
          <TdsOnIncentive>${flightSearchVO?.flightFare?.tdsOnIncentive}</TdsOnIncentive>
          <PLBEarned>${flightSearchVO?.flightFare?.pLBEarned}</PLBEarned>
          <TdsOnPLB>${flightSearchVO?.flightFare?.tdsOnPLB}</TdsOnPLB>
          <PublishedPrice>${flightSearchVO?.flightFare?.publishedPrice}</PublishedPrice>
          <AirTransFee>${flightSearchVO?.flightFare?.airTransFee}</AirTransFee>
          <Currency>${flightSearchVO?.flightFare?.currency}</Currency>
          <Discount>${flightSearchVO?.flightFare?.discount}</Discount>
          <ChargeBU>
            <ChargeBreakUp xsi:nil="true" />
            <ChargeBreakUp xsi:nil="true" />
          </ChargeBU>
          <OtherCharges>${flightSearchVO?.flightFare?.otherCharges}</OtherCharges>
          <FuelSurcharge>${flightSearchVO?.flightFare?.fuelSurcharge}</FuelSurcharge>
          <TransactionFee>${flightSearchVO?.flightFare?.transactionFee}</TransactionFee>
          <ReverseHandlingCharge>${flightSearchVO?.flightFare?.reverseHandlingCharge}</ReverseHandlingCharge>
          <OfferedFare>${flightSearchVO?.flightFare?.offeredFare}</OfferedFare>
          <AgentServiceCharge>${flightSearchVO?.flightFare?.agentServiceCharge}</AgentServiceCharge>
          <AgentConvienceCharges>${flightSearchVO?.flightFare?.agentConvienceCharges}</AgentConvienceCharges>
        </Fare>
        <Passenger>
          <WSPassenger>
            <Title>${adult1TName}</Title>
            <FirstName>${adult1FName}</FirstName>
            <LastName>${adult1LName}</LastName>
            <Type>Adult</Type>
            <DateOfBirth>1990-08-16</DateOfBirth>
            <Fare>
                <BaseFare>${flightSearchVO?.flightFare?.baseFare}</BaseFare>
                <Tax>${flightSearchVO?.flightFare?.tax}</Tax>
                <ServiceTax>${flightSearchVO?.flightFare?.serviceTax}</ServiceTax>
                <AdditionalTxnFee>${flightSearchVO?.flightFare?.additionalTxnFee}</AdditionalTxnFee>
                <AgentCommission>${flightSearchVO?.flightFare?.agentCommission}</AgentCommission>
                <TdsOnCommission>${flightSearchVO?.flightFare?.tdsOnCommission}</TdsOnCommission>
                <IncentiveEarned>${flightSearchVO?.flightFare?.incentiveEarned}</IncentiveEarned>
                <TdsOnIncentive>${flightSearchVO?.flightFare?.tdsOnIncentive}</TdsOnIncentive>
                <PLBEarned>${flightSearchVO?.flightFare?.pLBEarned}</PLBEarned>
                <TdsOnPLB>${flightSearchVO?.flightFare?.tdsOnPLB}</TdsOnPLB>
                <PublishedPrice>${flightSearchVO?.flightFare?.publishedPrice}</PublishedPrice>
                <AirTransFee>${flightSearchVO?.flightFare?.airTransFee}</AirTransFee>
                <Discount>${flightSearchVO?.flightFare?.discount}</Discount>
                <OtherCharges>${flightSearchVO?.flightFare?.otherCharges}</OtherCharges>
                <FuelSurcharge>${flightSearchVO?.flightFare?.fuelSurcharge}</FuelSurcharge>
                <TransactionFee>${flightSearchVO?.flightFare?.transactionFee}</TransactionFee>
                <ReverseHandlingCharge>${flightSearchVO?.flightFare?.reverseHandlingCharge}</ReverseHandlingCharge>
                <OfferedFare>${flightSearchVO?.flightFare?.offeredFare}</OfferedFare>
                <AgentServiceCharge>${flightSearchVO?.flightFare?.agentServiceCharge}</AgentServiceCharge>
            </Fare>
            <Ssr xsi:nil="true" />
            <Gender>Male</Gender>
            <PassportNumber>string</PassportNumber>
            <PassportExpiry>2020-01-01</PassportExpiry>
            <PinCode>string</PinCode>
            <Country>IN</Country>
            <Phone>8377001072</Phone>
            <AddressLine1>abc,karnal</AddressLine1>
            <Email>samverma0001@gmail.com</Email>
          </WSPassenger>
        </Passenger>
        <Remarks>Flight Ticket</Remarks>
        <InstantTicket>true</InstantTicket>
        <PaymentInformation>
          <PaymentInformationId>0</PaymentInformationId>
          <InvoiceNumber>0</InvoiceNumber>
          <PaymentId/>
          <Amount>${flightSearchVO?.flightFare?.publishedPrice}</Amount>
          <IPAddress/>
          <TrackId>0</TrackId>
          <PaymentGateway>APICustomer</PaymentGateway>
          <PaymentModeType>Deposited</PaymentModeType>
        </PaymentInformation>
        <Source>${flightSearchVO?.flightFareRule?.source?.first()}</Source>
        <SessionId>${flightSearchVO?.sessionId}</SessionId>
        <IsOneWayBooking>true</IsOneWayBooking>
        <PromotionalPlanType>Normal</PromotionalPlanType>
      </wsTicketRequest>
    </Ticket>
  </soap:Body>
</soap:Envelope>"""
            )
            print('--------------bookedTicketDetail--------------------' + bookedTicketDetail.text)
            bookedFlight.refId = bookedTicketDetail.TicketResponse.TicketResult.RefId
            bookedFlight.save(flush: true, failOnError: true)


            SOAPResponse addBookingDetailResponse = client.send("""<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
            <soap:Header>
                <AuthenticationData xmlns="http://192.168.0.170/TT/BookingAPI">
                <SiteName></SiteName>
                <AccountCode></AccountCode>
                <UserName>${grailsApplication.config.tbo.FLIGHT_USERNAME}</UserName>
                <Password>${grailsApplication.config.tbo.FLIGHT_PASS}</Password>
            </AuthenticationData>
            </soap:Header>
                <soap:Body>
                <AddBookingDetail xmlns="http://192.168.0.170/TT/BookingAPI">
                <saveRequest>
                    <RefId>${bookedFlight?.refId}</RefId>
                    <BookingStatus>Ticketed</BookingStatus>
                </saveRequest>
                </AddBookingDetail>
            </soap:Body>
            </soap:Envelope>"""
            )

            print "------------addBookingDetailResponse----------------------" + addBookingDetailResponse.text

            SOAPResponse getBooking = client.send("""<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <AuthenticationData xmlns="http://192.168.0.170/TT/BookingAPI">
      <SiteName></SiteName>
      <AccountCode></AccountCode>
      <UserName>${grailsApplication.config.tbo.FLIGHT_USERNAME}</UserName>
      <Password>${grailsApplication.config.tbo.FLIGHT_PASS}</Password>
    </AuthenticationData>
  </soap:Header>
  <soap:Body>
    <GetBooking xmlns="http://192.168.0.170/TT/BookingAPI">
      <bookingRequest>
        <BookingId>${bookedFlight?.bookingId}</BookingId>
        <Pnr>${bookedFlight?.pnr}</Pnr>
        <Source>${flightSearchVO?.flightFareRule?.source?.first()}</Source>
      </bookingRequest>
    </GetBooking>
  </soap:Body>
</soap:Envelope>"""
            )

            print '------getBooking-------------------' + getBooking.text




            def bookingResult = getBooking.GetBookingResponse.GetBookingResult
            bookedFlight.origin = bookingResult.Origin
            bookedFlight.destination = bookingResult.Destination
            bookedFlight.fareType = bookingResult.FareType
            bookedFlight.source = bookingResult.Source
            bookedFlight.status = bookingResult.Description
            bookedFlight.save(flush: true, failOnError: true)

            BookedFlightFare bookedFlightFare = new BookedFlightFare()
            bookedFlightFare.additionalTxnFee = utilService.convertStringToBigdecimal(bookingResult.Fare.AdditionalTxnFee.text())
            bookedFlightFare.agentCommission = utilService.convertStringToBigdecimal(bookingResult.Fare.AgentCommission.text())
            bookedFlightFare.tdsOnCommission = utilService.convertStringToBigdecimal(bookingResult.Fare.TdsOnCommission.text())
            bookedFlightFare.airTransFee = utilService.convertStringToBigdecimal(bookingResult.Fare.AirTransFee.text())
            bookedFlightFare.baseFare = utilService.convertStringToBigdecimal(bookingResult.Fare.BaseFare.text())
            bookedFlightFare.tax = utilService.convertStringToBigdecimal(bookingResult.Fare.Tax.text())
            bookedFlightFare.otherCharges = utilService.convertStringToBigdecimal(bookingResult.Fare.OtherCharges.text())
            bookedFlightFare.fuelSurcharge = utilService.convertStringToBigdecimal(bookingResult.Fare.FuelSurcharge.text())
            bookedFlightFare.discount = utilService.convertStringToBigdecimal(bookingResult.Fare.Discount.text())
            bookedFlightFare.serviceTax = utilService.convertStringToBigdecimal(bookingResult.Fare.ServiceTax.text())
            bookedFlightFare.currency = bookingResult.Fare.Currency
            bookedFlightFare.plb = utilService.convertStringToBigdecimal(bookingResult.Fare.PLBEarned.text())
            bookedFlightFare.tdsOnPLB = utilService.convertStringToBigdecimal(bookingResult.Fare.TdsOnPLB.text())
            bookedFlightFare.incentiveEarned = utilService.convertStringToBigdecimal(bookingResult.Fare.IncentiveEarned.text())
            bookedFlightFare.tdsOnIncentive = utilService.convertStringToBigdecimal(bookingResult.Fare.TdsOnIncentive.text())
            bookedFlightFare.offeredFare = utilService.convertStringToBigdecimal(bookingResult.Fare.OfferedFare.text())
            bookedFlightFare.agentServiceCharge = utilService.convertStringToBigdecimal(bookingResult.Fare.AgentServiceCharge.text())
            bookedFlightFare.publishedPrice = utilService.convertStringToBigdecimal(bookingResult.Fare.PublishedPrice.text())

            bookedFlightFare.bookedFlight = bookedFlight
            bookedFlightFare.save(flush: true, failOnError: true)

            def passengerDetail = bookingResult.Passenger.WSPassenger
            bookingResult.Passenger.WSPassenger.each {
                BookedFlightPassenger bookedFlightPassenger = new BookedFlightPassenger()
                bookedFlightPassenger.title = passengerDetail.Title
                bookedFlightPassenger.firstName = passengerDetail.FirstName
                bookedFlightPassenger.lastName = passengerDetail.LastName
                bookedFlightPassenger.type = passengerDetail.Type
                bookedFlightPassenger.gender = passengerDetail.Gender
//            bookedFlightPassenger.dateOfBirth = passengerDetail.DateOfBirth
                bookedFlightPassenger.dateOfBirth = new Date()
                bookedFlightPassenger.passportNumber = passengerDetail.PassportNumber
                bookedFlightPassenger.passportExpiry = passengerDetail.PassportExpiry
                bookedFlightPassenger.country = passengerDetail.Country
                bookedFlightPassenger.phone = passengerDetail.Phone
                bookedFlightPassenger.pincode = passengerDetail.PinCode
                bookedFlightPassenger.address1 = passengerDetail.AddressLine1
                bookedFlightPassenger.address2 = passengerDetail.AddressLine2
                bookedFlightPassenger.email = passengerDetail.Email
                bookedFlightPassenger.seat = passengerDetail.Seat
                bookedFlightPassenger.meal = passengerDetail.Meal
                bookedFlightPassenger.bookedFlight = bookedFlight
                bookedFlightPassenger.save(flush: true, failOnError: true)
                bookedFlight.addToBookedFlightPassanger(bookedFlightPassenger)
            }
            def ticketDetail = bookingResult.Ticket.WSTicket
            bookingResult.Ticket.WSTicket.each {
                BookedFlightTicket bookedFlightTicket = new BookedFlightTicket()
                bookedFlightTicket.ticketId = utilService.convertStringToInteger(ticketDetail.TicketId.text())
                bookedFlightTicket.ticketNumber = ticketDetail.TicketNumber
                bookedFlightTicket.title = ticketDetail.Title
                bookedFlightTicket.firstName = ticketDetail.FirstName
                bookedFlightTicket.lastName = ticketDetail.LastName
                bookedFlightTicket.passengerType = ticketDetail.PaxType
                bookedFlightTicket.issueDate = new Date()
                bookedFlightTicket.tourCode = ticketDetail.TourCode
                bookedFlightTicket.bookedFlight = bookedFlight
                bookedFlightTicket.save(flush: true, failOnError: true)
                bookedFlight.addToBookedFlightTicket(bookedFlightTicket)
            }


        }

        render(view: "/flights/tboFlightBookingIdDisplay", model: [bookedFlight: bookedFlight, description: response.TicketResponse.TicketResult.Status.Description])
    }


    def makeTicketing = {

    }

//    def getBooking = {
//        print '----------------------params in get booking-----------------' + params
//        String bookingId = params.bookingId
//        String pnr = params.pnr
//        def client = new SOAPClient("http://api.tektravels.com/tboapi_v7/service.asmx")
//        SOAPResponse response = client.send("""<?xml version="1.0" encoding="utf-8"?>
//<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"
//               xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
//    <soap:Header>
//        <AuthenticationData xmlns="http://192.168.0.170/TT/BookingAPI">
//            <SiteName/>
//            <AccountCode/>
//             <UserName>${grailsApplication.config.tbo.TRAVELX_UNAME}</UserName>
//             <Password>${grailsApplication.config.tbo.TRAVELX_PASS_HOTEL}</Password>
//        </AuthenticationData>
//    </soap:Header>
//    <soap:Body>
//        <GetBooking xmlns="http://192.168.0.170/TT/BookingAPI">
//      <bookingRequest>
//        <BookingId>${bookingId}</BookingId>
//        <Pnr>${pnr}</Pnr>
//        <Source>Abacus</Source>
//      </bookingRequest>
//    </GetBooking>
//    </soap:Body>
//</soap:Envelope>"""
//        )
//        println '---==GET BOOKING====' + response.text
//
//        render("SUCCESS")
//    }
}
