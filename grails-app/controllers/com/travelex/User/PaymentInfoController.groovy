package com.travelex.User


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class PaymentInfoController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond PaymentInfo.list(params), model: [paymentInfoInstanceCount: PaymentInfo.count()]
    }

    def show(PaymentInfo paymentInfoInstance) {
        respond paymentInfoInstance
    }

    @Transactional
    def update(PaymentInfo paymentInfoInstance) {
        if (paymentInfoInstance == null) {
            notFound()
            return
        }

        if (paymentInfoInstance.hasErrors()) {
            respond paymentInfoInstance.errors, view: 'edit'
            return
        }

        paymentInfoInstance.save flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'PaymentInfo.label', default: 'PaymentInfo'), paymentInfoInstance.id])
                redirect paymentInfoInstance
            }
            '*' { respond paymentInfoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(PaymentInfo paymentInfoInstance) {

        if (paymentInfoInstance == null) {
            notFound()
            return
        }

        paymentInfoInstance.delete flush: true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'PaymentInfo.label', default: 'PaymentInfo'), paymentInfoInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'paymentInfoInstance.label', default: 'PaymentInfo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
