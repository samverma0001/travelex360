package com.travelex.User

import com.travelex.socials.login.SignUpCO
import grails.plugin.mail.MailService
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.web.multipart.commons.CommonsMultipartFile
import util.AppUtil

class UserController {
    def springSecurityService
    MailService mailService
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [userList: User.list(params), userInstanceTotal: User.count()]
    }

    def create() {
//        [userInstance: new User(params)]
        render(view: '/user/create')
    }

    def save() {
        print('----------------------' + params)
        SignUpCO signUpCO = new SignUpCO()
        bindData(signUpCO, params)
        String tempRole = signUpCO.selectRole
        if (signUpCO.validate()) {
            print('11111111111111111111111')
            User userInstance = new User()
            userInstance = SignUpCO.createUserByAdmin(userInstance, signUpCO)
            if (userInstance.save(flush: true)) {
                String decideRole = signUpCO.selectRole
                Role role
                if (decideRole.equals("ROLE_ADMIN")) {
                    role = Role.findByAuthority("ROLE_ADMIN")
                    UserRole.create(userInstance, role, true)
                } else if (decideRole.equals("ROLE_PROVIDER")) {
                    role = Role.findByAuthority("ROLE_PROVIDER")
                    UserRole.create(userInstance, role, true)
                } else if (decideRole.equals("ROLE_USER")) {
                    role = Role.findByAuthority("ROLE_USER")
                    UserRole.create(userInstance, role, true)
                }
                flash.success = g.message(code: 'default.user.save.success.message')
                redirect(action: "show", id: userInstance?.id)
            } else {
                render(view: "create", model: [signUpCO: signUpCO, role: tempRole])
            }
        } else {
            render(view: "create", model: [signUpCO: signUpCO, role: tempRole])
        }
    }

    def show(Long id) {
        def userInstance = User.get(id)
        String role = UserRole.findByUser(userInstance).role.authority
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "list")
            return
        }
//        [userInstance: userInstance]
        render(view: '/user/show', model: [role: role, userInstance: userInstance])
    }

    def edit(Long id) {
        def userInstance = User.get(id)
        UserRole userRole = UserRole.findByUser(userInstance)
        String role = userRole.role.authority
        print('-------------------------' + role)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "list")
            return
        }
//        [userInstance: userInstance]
        render(view: '/user/edit', model: [role: role, userInstance: userInstance])
    }

    def update(Long id, Long version) {
        print('1111111111111111111111111' + params)
        SignUpCO signUpCO = new SignUpCO()
        bindData(signUpCO, params)
        User userInstance = User.get(id)
        print('222222222222222222222222222222' + userInstance)
        if (signUpCO.validate()) {
            print('333333333333333333333333333333')
            userInstance = SignUpCO.updateUserByAdmin(userInstance, signUpCO)
            if (!userInstance.save(flush: true)) {
                print('44444444444444444444444444444444444444')
                render(view: "edit", model: [userInstance: userInstance, signUpCO: signUpCO])
            } else {
                print('5555555555555555555555555555555')
                flash.message = message(code: 'default.updated.message', args: [message(code: 'user.label', default: 'User'), userInstance.id])
                redirect(action: "show", id: userInstance.id)
            }
        } else {
            print('666666666666666666666666666666666666')
            render(view: "edit", model: [userInstance: userInstance, signUpCO: signUpCO])
        }
    }

    def delete(Long id) {
        def userInstance = User.get(id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "list")
            return
        }

        UserRole userRole = UserRole.findByUser(userInstance)
        print('--------------- user role is ------------------' + userRole)
        print('--------------- user role is ------------------' + userRole.user)
        print('--------------- user role is ------------------' + userRole.role)

        userRole.user = null;
        userRole.role = null;
//        userRole.delete(flush: true)
        print('--------------- user role is ------------------' + userRole.user)
        print('--------------- user role is ------------------' + userRole.role)
        try {
            userInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "show", id: id)
        }
    }

    def saveProfilePicture = {
        def webRootDir = AppUtil.staticResourcesDirPath
        print '---------------webRootDir-----------------------' + webRootDir
        CommonsMultipartFile uploadedFile = params.profilePicture
        print '---------------uploadedFile-----------------------' + uploadedFile
        if (uploadedFile?.bytes) {
            def user = springSecurityService.currentUser as User
            print '---------------11111111-----------------------'
            User userInstance = User.findById(user.id as Long)
            def userDir = new File(webRootDir, "/uploadedProfilePic/${userInstance.id}/")
            userDir.mkdirs()
            print '---------------222222222-----------------------'
            String fileName = uploadedFile.originalFilename.trim()
            uploadedFile.transferTo(new File(userDir, fileName))
            userInstance.pictureName = fileName
            userInstance.pictureType = uploadedFile.contentType
            userInstance.save(flush: true)
            redirect(controller: "home", action: "index")
        }
    }

    def showProfileImage = {
        def id = params.userId
        User buyerUser = User.findById(id as Long)
        def webRootDir = AppUtil.staticResourcesDirPath
        String imageName = buyerUser?.pictureName
        String fileNameToken = imageName?.tokenize('.')?.first()
        String fileExtension = imageName?.tokenize('.')?.last()
        def avatarFilePath
        try {
            avatarFilePath = new File(webRootDir, "/uploadedProfilePic/${buyerUser.id}/" + fileNameToken + ".${fileExtension}");
            log.info '---------avatarFilePath---------' + avatarFilePath.getAbsolutePath()
            if (avatarFilePath) {
                response.setContentType(buyerUser.pictureType)
                response.setContentLength(avatarFilePath?.size()?.toInteger())
                OutputStream out = response.getOutputStream();
                out.write(avatarFilePath.bytes);
                out.close();
            }

        } catch (Exception e) {
            log.info '---------Excepton----------------' + e.stackTrace
            e.printStackTrace()
        }
    }

    def sendMailToUser = {
        print('------------ params are ------------------' + params)
        try {
            print('1111111111111111111')
            mailService.sendMail {
                to params.receiver
                subject params.subject
                html params.body
            }
        }
        catch (Exception exc) {
            log.info('---------- exception occured while sending mail-------------------' + exc.printStackTrace())

        }
        flash.success = g.message(code: 'default.success.emailSend.message')
        redirect(controller: 'user', action: 'index')
    }


}
