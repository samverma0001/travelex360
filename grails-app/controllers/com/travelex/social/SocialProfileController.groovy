package com.travelex.social

import org.springframework.dao.DataIntegrityViolationException

class SocialProfileController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [socialProfileInstanceList: SocialProfile.list(params), socialProfileInstanceTotal: SocialProfile.count()]
    }

    def create() {
        [socialProfileInstance: new SocialProfile(params)]
    }

    def save() {
        def socialProfileInstance = new SocialProfile(params)
        if (!socialProfileInstance.save(flush: true)) {
            render(view: "create", model: [socialProfileInstance: socialProfileInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'socialProfile.label', default: 'SocialProfile'), socialProfileInstance.id])
        redirect(action: "show", id: socialProfileInstance.id)
    }

    def show(Long id) {
        def socialProfileInstance = SocialProfile.get(id)
        if (!socialProfileInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'socialProfile.label', default: 'SocialProfile'), id])
            redirect(action: "list")
            return
        }

        [socialProfileInstance: socialProfileInstance]
    }

    def edit(Long id) {
        def socialProfileInstance = SocialProfile.get(id)
        if (!socialProfileInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'socialProfile.label', default: 'SocialProfile'), id])
            redirect(action: "list")
            return
        }

        [socialProfileInstance: socialProfileInstance]
    }

    def update(Long id, Long version) {
        def socialProfileInstance = SocialProfile.get(id)
        if (!socialProfileInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'socialProfile.label', default: 'SocialProfile'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (socialProfileInstance.version > version) {
                socialProfileInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'socialProfile.label', default: 'SocialProfile')] as Object[],
                        "Another user has updated this SocialProfile while you were editing")
                render(view: "edit", model: [socialProfileInstance: socialProfileInstance])
                return
            }
        }

        socialProfileInstance.properties = params

        if (!socialProfileInstance.save(flush: true)) {
            render(view: "edit", model: [socialProfileInstance: socialProfileInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'socialProfile.label', default: 'SocialProfile'), socialProfileInstance.id])
        redirect(action: "show", id: socialProfileInstance.id)
    }

    def delete(Long id) {
        def socialProfileInstance = SocialProfile.get(id)
        if (!socialProfileInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'socialProfile.label', default: 'SocialProfile'), id])
            redirect(action: "list")
            return
        }

        try {
            socialProfileInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'socialProfile.label', default: 'SocialProfile'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'socialProfile.label', default: 'SocialProfile'), id])
            redirect(action: "show", id: id)
        }
    }
}
