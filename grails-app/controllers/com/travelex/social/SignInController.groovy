package com.travelex.social

import com.travelex.User.Role
import com.travelex.User.User
import com.travelex.User.UserRole
import com.travelex.socials.FacebookSignupVO
import com.travelex.socials.GoogleSignupVO
import com.travelex.socials.LinkedInSignupVO
import com.travelex.socials.SocialProfileType
import com.travelex.socials.login.RegisterCO
import com.travelex.socials.login.ResetPasswordCo
import com.travelex.socials.login.SignUpCO
import com.travelex.socials.login.SignInCo
import com.travelex.socials.login.SignUpForServiceProviderCO
import grails.converters.JSON
import grails.plugin.mail.MailService
import org.apache.commons.lang.RandomStringUtils
import serviceProvider.ServiceProvider

import java.text.SimpleDateFormat

class SignInController {
    MailService mailService
    def grailsApplication
    def springSecurityService
    def signupService
    def simpleCaptchaService
//    AsynchronousMailService asyncMailService

    public static
    final String FACEBOOK_PERMISSIONS = "read_stream,publish_stream,user_about_me,user_status,user_birthday,user_hometown,user_religion_politics,user_location,user_work_history,email,user_website,user_likes,user_religion_politics,user_friends"
    private static final String GOOGLE_PROFILE_SCOPE = "https://www.googleapis.com/auth/userinfo.profile";
    private static final String GOOGLE_EMAIL_SCOPE = "https://www.googleapis.com/auth/userinfo.email";


    def index() {
        render(view: '/chooseProfileLogin/signInUsingSocialMedia')
    }

    def serviceProviderSignUp = {
//        render(view: '/chooseProfileLogin/serviceProviderSignInUsingSocialMedia')
        render(view: '/chooseProfileLogin/serviceProviderRegister')

    }
    def resetPassword = {
        render(view: '/chooseProfileLogin/emailForPasswordProtect')

    }
    def activateAccount = {
        Role role1 = Role.findByAuthority('ROLE_USER')

        def token = params.userToken
        User user = User.findByToken(token)
        try {
            user.enabled = true
            user.hasVerifiedEmail = true
            user.token = UUID.randomUUID().toString()

            if (!user.save(flush: true)) {
                redirect(action: 'auth', controller: 'login', params: [messageString: 'Unable to Activate user.'])
            }

            UserRole.create(user, role1, true)

            UserRole userRole1 = UserRole.findByUserAndRole(user, role1)
            def userRole = userRole1.role.authority
            springSecurityService?.reauthenticate(user?.username)
            redirect(action: 'index', controller: 'home', params: [userRole: userRole])
        }
        catch (Exception e) {
            redirect(action: 'auth', controller: 'login', params: [messageString: 'Unable to Activate user.'])

        }
//        user.token=UUID.randomUUID().toString()
//        if (!user.save(flush: true)) {
//            redirect(action: 'auth', controller: 'login', params: [messageString: 'Unable to Activate user.'])
//        }

//        springSecurityService?.reauthenticate(user?.username)
//        redirect(action: 'index', controller: 'home')
    }

    def forgetPassword = {
        def userToken = params.userToken
        User user = User.findByToken(userToken)
        if (user) {
            render(view: '/chooseProfileLogin/forgetPassword', model: [userToken: userToken, userInstance: user])
        } else {
            render(view: '/errorPages/errorPage')
        }

    }

    def changePassword = {
        String newPassword = params.newPassword
        print('--------new password------------' + newPassword)
        String confirmPassword = params.confirmPassword
        print('--------confirmPassword------------' + confirmPassword)
        String userToken = params.userToken
        print('--------userToken------------' + userToken)
        if (newPassword && confirmPassword) {
            if (newPassword.equals(confirmPassword)) {
                User user = User.findByToken(userToken)
                user.password = newPassword
                user.save()
                if (!user.save(flush: true)) {
                    redirect(action: 'auth', controller: 'login', params: [messageString: 'Unable to Create user.'])
                }
                flash.success = g.message(code: 'password.update.success.alert')
                springSecurityService.reauthenticate(user?.username)
                redirect(controller: 'home', action: 'index')
            } else {
                flash.error = g.message(code: 'newandconfirm.password.notsame.error')
                render(view: '/chooseProfileLogin/forgetPassword', model: [userToken: userToken])
            }
        } else {
            flash.error = g.message(code: 'newandconfirm.password.blank.error')
            render(view: '/chooseProfileLogin/forgetPassword', model: [userToken: userToken])
        }
    }

    def sendPasswordReset = {
        def email = params.emailForReset
        if (email) {
            User user = User.findByUsername(email)
            if (user) {
                def token = user.token
                mailService.sendMail {
                    to user.username
                    subject "Password Reset"
                    html g.render(template: '/chooseProfileLogin/mailTemplate', model: [userToken: token], plugin: "email-confirmation")
                }
                redirect(action: 'index')
            } else {
                flash.error = g.message(code: 'user.notregister.error')
                render(view: '/chooseProfileLogin/emailForPasswordProtect')
            }
        } else {
            flash.error = g.message(code: 'field.cannot.blank')
            render(view: '/chooseProfileLogin/emailForPasswordProtect')
        }
    }


    def signUp = { RegisterCO registerCO ->
        println '==========' + params
        if (registerCO.validate()) {
            if (simpleCaptchaService.validateCaptcha(params.captcha)) {
                User user = new User()
                RegisterCO.createUser(user, registerCO)
                user.enabled = true
                if (user.save(flush: true, failOnError: true)) {
                    Role roleUser = Role.findByAuthority("ROLE_USER")
                    UserRole.create(user, roleUser, true)
                    mailService.sendMail {
                        async true
                        to user.username
                        subject "Activate Your Account"
                        html g.render(template: '/chooseProfileLogin/signUpMailTemplate', model: [userToken: user.token, name: user.firstName])
                    }
                    redirect(action: 'aboutToActivateAccount', controller: 'home', params: [user: user?.id])
                } else {
                    redirect(action: 'index', controller: 'signIn', params: [messageString: 'Unable to Create user.'])
                }
            } else {
                flash.error = g.message(code: 'default.error.captcha.notsame')
                render(view: '/chooseProfileLogin/signInUsingSocialMedia', model: [registerCO: registerCO])
            }
        } else {
            render(view: '/chooseProfileLogin/signInUsingSocialMedia', model: [registerCO: registerCO])
        }
    }

    def signUpForServiceProvider = { SignUpForServiceProviderCO signUpForServiceProviderCO ->

        if (signUpForServiceProviderCO.validate()) {
            if (!ServiceProvider.countByOfficialEmail(signUpForServiceProviderCO.officialEmail)) {
                ServiceProvider serviceProvider = new ServiceProvider()
                serviceProvider.nameOfHotel = signUpForServiceProviderCO.nameOfHotel
                serviceProvider.city = signUpForServiceProviderCO.city
                serviceProvider.state = signUpForServiceProviderCO.state
                serviceProvider.address = signUpForServiceProviderCO.address
                serviceProvider.propertyType = signUpForServiceProviderCO.propertyType
                serviceProvider.nameOfRepresentee = signUpForServiceProviderCO.nameOfRepresentee
                serviceProvider.designationOfRepresentee = signUpForServiceProviderCO.designationOfRepresentee
                serviceProvider.officialEmail = signUpForServiceProviderCO.officialEmail
                serviceProvider.website = signUpForServiceProviderCO.website
                serviceProvider.mobNo = signUpForServiceProviderCO.mobNo

                if (!serviceProvider.save(flush: true)) {
                    redirect(action: 'serviceProviderLogin', controller: 'login', params: [messageString: 'Unable to Register.'])
                }
                def email = signUpForServiceProviderCO.officialEmail
                try {
                    mailService.sendMail {
                        println '========Inside send mail======'
                        to email
                        subject "Registration request sent"
                        body "You will shortly receive your login credentials after Admin approval "
                        println '========Processed send mail======'
                    }
                }
                catch (Exception exc) {
                    print('1111111111111111111111111111')
                }
                redirect(action: 'infoServiceProvider', controller: 'home')
            } else {
                render(view: '/chooseProfileLogin/serviceProviderRegister', model: [signUpCO: signUpForServiceProviderCO])

            }
        } else {
            render(view: '/chooseProfileLogin/serviceProviderRegister', model: [signUpCO: signUpForServiceProviderCO])
        }
    }

//    def signIn = { SignInCo signInCo ->
//
//        if (signInCo.validate()) {
//            springSecurityService?.reauthenticate(signInCo?.username)
//            redirect(controller: 'home', action: 'index')
//        } else {
//            signInCo.errors.allErrors.each {
//            }
//            render(view: '/chooseProfileLogin/signInUsingSocialMedia', model: [signInCo: signInCo])
//        }
//
//
//    }

    def connectFacebook = {
//        String serverUrl = grailsApplication.config.grails.serverURL
//        print('-----------server url------------------' + serverUrl)
        String callbackurl = "http://localhost:8090/travelx/signIn/facebookCallback"
        print('-----------callback url------------------' + callbackurl)
        String facebookApiKey = grailsApplication.config.facebook.apiKey
        print('-----------facebookApiKey ------------------' + facebookApiKey)
        String facebookUrl = "https://graph.facebook.com/oauth/authorize?client_id=${facebookApiKey}&redirect_uri=${callbackurl}?cmd=add&scope=${FACEBOOK_PERMISSIONS}"
        print('----------- facebookUrl------------------' + facebookUrl)
        redirect(url: facebookUrl)
    }

    def facebookCallBack = {
        println '=================Inside fecebook CallBack-==========' + params
        String authCode = params.code
        User user = springSecurityService.currentUser as User
        if (authCode) {
            String serverUrl = grailsApplication.config.grails.serverURL
            print '------------1111111111111-------' + serverUrl
            String callbackurl = "${serverUrl}/signIn/facebookCallBack"
            print '------------22222222222222222222222222-------' + callbackurl
            String facebookTokenUrl = "https://graph.facebook.com/oauth/access_token?client_id=${grailsApplication.config.facebook.apiKey}&client_secret=${grailsApplication.config.facebook.apiSecret}&code=${authCode}&redirect_uri=${callbackurl}?cmd=add&scope=${FACEBOOK_PERMISSIONS}"
            print '------------3333333333333333333333-------' + facebookTokenUrl

            URL url = new URL(facebookTokenUrl)
            print '------------44444444444-------' + url

            String response = url.text
            print '------------5555555555-------' + response

            String accessToken
            if (response.contains('access_token=')) {
                String[] resp = response.split('access_token=')
                accessToken = resp[1]
                if (accessToken.contains('&')) {
                    accessToken = accessToken.substring(0, accessToken.indexOf('&'))
                }
                String facebookUrl = "https://graph.facebook.com/me?fields=name,first_name,last_name,link,username,address,gender,email,religion,birthday,bio,location,education,friends,age_range,about,hometown&access_token=${accessToken}"

                print '------------66666666666-------' + facebookUrl
                URL profileDataUrl = new URL(facebookUrl)
                print '------------777777777777-------' + profileDataUrl

                def jsonString = JSON.parse(profileDataUrl.text)
                print '------------88888888888888888-------' + jsonString

                FacebookSignupVO facebookSignupVO = new FacebookSignupVO(jsonString)
                facebookSignupVO.accessToken = accessToken
                print('------------99-------------------' + accessToken)
                if (user) {
                    if (!SocialProfile.countByUserAndType(user, SocialProfileType.FACEBOOK)) {
                        signupService.createFacebookSocialProfile(user, facebookSignupVO)
                    } else {
                        SocialProfile socialProfile = SocialProfile.findByUserAndType(user, SocialProfileType.FACEBOOK)
                        signupService.updateFacebookSocialProfile(socialProfile, facebookSignupVO)
                    }
                } else {
                    user = User.findByUsername(facebookSignupVO.email)
                    if (user) {
                        if (!SocialProfile.countByUserAndType(user, SocialProfileType.FACEBOOK)) {
                            signupService.createFacebookSocialProfile(user, facebookSignupVO)
                        }
                    } else {
                        user = new User()
                        user.firstName = facebookSignupVO.name
                        user.username = facebookSignupVO.email
                        user.accountLocked = false
                        user.accountExpired = false
                        user.enabled = true
                        user.hasVerifiedEmail = true
                        user.password = facebookSignupVO.defaultPassword
//                        UserRole.create(user,role1,true)

                        if (!user.save(flush: true)) {
                            redirect(action: 'index', controller: 'home', params: [messageString: 'Unable to Create user.'])
                        } else {
                            UserRole.create(user, Role.findByAuthority('ROLE_USER'), true)
//                            FundRaiser fundRaiser = new FundRaiser()
//                            fundRaiser.user = user
//                            user.fundRaiser = fundRaiser
                            if (facebookSignupVO.gender.equalsIgnoreCase('female')) {
//                                fundRaiser.gender=Gender.FEMALE
                            }
//                            AppUtil.save(fundRaiser)
                            UserRole.create(user, Role.findByAuthority("FUNDRAISER_ROLE"), true)
                            signupService.createFacebookSocialProfile(user, facebookSignupVO)
                        }
                    }
                }
                if (springSecurityService.currentUser) {
                    flash.clear()
                    flash.message = "Your Facebook Profile updated successfully..."
                    redirect(action: 'index', controller: 'home')
                } else {
                    SocialProfile facebookProfile = SocialProfile.findByUserAndType(user, SocialProfileType.FACEBOOK)
                    facebookProfile.accessToken = facebookSignupVO.accessToken
                    facebookProfile.save(flush: true)
                    springSecurityService?.reauthenticate(facebookSignupVO?.email)
                    redirect(action: 'index', controller: 'home')
                }
            }
        } else {
            flash.message = params.error_message
            redirect(action: 'auth', controller: 'login')
        }
    }

    def connectLinkedIn = {
        String serverUrl = grailsApplication.config.grails.serverURL
        print('---------server url--------------' + serverUrl)
        String callbackurl = "${serverUrl}/signIn/linkedInCallback"
        print('---------callbackurl --------------' + callbackurl)
        String randomId = UUID.randomUUID().toString()
        String linkedApiKey = grailsApplication.config.linkedIn.apiKey
        print('---------linkedApiKey --------------' + linkedApiKey)
        String url = "https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=${linkedApiKey}&scope=r_fullprofile%20r_emailaddress&state=${randomId}&redirect_uri=${callbackurl}"
        redirect(url: url)
    }

    def linkedInCallback = {
        print '------------------paramas in linked in call back--------------------' + params
        String authCode = params.code
        User user = springSecurityService.currentUser as User
        if (authCode) {
            String accessToken = signupService.postCallForLinkedInAccessToken(authCode)
            if (accessToken) {
                LinkedInSignupVO linkedInSignupVO = signupService.getLinkedInUserProfile(accessToken)
                if (user) {
                    if (!SocialProfile.countByUserAndType(user, SocialProfileType.LINKED_IN)) {
                        signupService.createLinkedInSocialProfile(user, linkedInSignupVO)
                    } else {
                        SocialProfile socialProfile = SocialProfile.findByUserAndType(user, SocialProfileType.LINKED_IN)
                        signupService.updateLinkedInSocialProfile(socialProfile, linkedInSignupVO)
                    }
                } else {
                    user = User.findByUsername(linkedInSignupVO.email)
                    if (user) {
                        if (!SocialProfile.countByUserAndType(user, SocialProfileType.LINKED_IN)) {
                            signupService.createLinkedInSocialProfile(user, linkedInSignupVO)
                        } else {
                            SocialProfile socialProfile = SocialProfile.findByUserAndType(user, SocialProfileType.LINKED_IN)
                            signupService.updateLinkedInSocialProfile(socialProfile, linkedInSignupVO)

                        }
                    } else {
                        user = new User()
//                        user.firstName = linkedInSignupVO.name
                        user.username = linkedInSignupVO.email
                        String userIpAddress = request.getRemoteAddr()
                        user.accountLocked = false
                        user.accountExpired = false
                        user.enabled = true
//                        user.hasVerifiedEmail = true
                        user.password = linkedInSignupVO.defaultPassword
//                        UserRole.create(user,role1,true)
                        String randomAlphanumeric = RandomStringUtils.randomAlphanumeric(8)
                        Date today = new Date()
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat('dd/MM/yyyy')
                        String[] date = simpleDateFormat.format(today).split('/')
                        user.coustId = "TR" + "CS" + randomAlphanumeric.toUpperCase() + "${date[0]}" + "${date[1]}" + "${date[2]}"
                        if (!user.save(flush: true)) {
                            redirect(action: 'auth', controller: 'login', params: [messageString: 'Unable to Create user.'])
                        } else {
                            UserRole.create(user, Role.findByAuthority('ROLE_USER'), true)
                            if (linkedInSignupVO?.gender?.equalsIgnoreCase('female')) {
//                                fundRaiser.gender=Gender.FEMALE
                            }
//                            AppUtil.save(fundRaiser)
                            UserRole.create(user, Role.findByAuthority("FUNDRAISER_ROLE"), true)
                            signupService.createLinkedInSocialProfile(user, linkedInSignupVO)

                        }
                    }
                }
                if (springSecurityService.currentUser) {
                    flash.clear()
                    flash.message = "Your LINKED_IN Profile updated successfully..."
                    redirect(action: 'index', controller: 'home')
                } else {
                    SocialProfile linkedInProfile = SocialProfile.findByUserAndType(user, SocialProfileType.LINKED_IN)
                    linkedInProfile.accessToken = accessToken
                    linkedInProfile.save(flush: true)
                    springSecurityService?.reauthenticate(linkedInSignupVO?.email)
                    redirect(action: 'index', controller: 'home')
                }
            }
        } else {
            redirect(action: 'auth', controller: 'login')
        }
    }

    def connectGoogle = {
        String serverUrl = grailsApplication.config.grails.serverURL
        String callbackurl = "${serverUrl}/signIn/googleCallback"
        println("****************** Google Callback URL ***************** " + callbackurl)
        String googleApiKey = grailsApplication.config.google.apiKey
        String authorizeUrl = "https://accounts.google.com/o/oauth2/auth?scope=${GOOGLE_EMAIL_SCOPE}+${GOOGLE_PROFILE_SCOPE}&state=xyz&redirect_uri=${callbackurl}&response_type=code&client_id=${googleApiKey}&access_type=offline&approval_prompt=force"
        println '******************* Google Redirect URL **************** ' + authorizeUrl
        URL urlForGooglePlus = new URL(authorizeUrl)
        redirect(url: urlForGooglePlus)
    }

    def googleCallback = {
        print '------google call back-----------' + params

        String code = params?.code
        print '------ ------ code-----------' + code

        User user = springSecurityService.currentUser as User
        print '------ ------ user-----------' + user

        if (code) {
            print '------ ------ inif-----------'

            String accessToken = signupService.postCallForGoogleAccessToken(code)

            print '------ ------ accessToken-----------' + accessToken
            if (accessToken) {
                GoogleSignupVO googleSignupVO = signupService.getGoogleUserProfile(accessToken)
                if (user) {
                    print '------------ in if user is------------' + user
                    if (!SocialProfile.countByUserAndType(user, SocialProfileType.GOOGLE)) {
                        signupService.createGoogleSocialProfile(user, googleSignupVO)
                    }
                } else {
                    user = User.findByUsername(googleSignupVO.email)
                    print '------------ in else user found or not------------' + user
                    if (user) {
                        print '------------ in if 11 user is------------' + user

                        if (!SocialProfile.countByUserAndType(user, SocialProfileType.GOOGLE)) {
                            signupService.createGoogleSocialProfile(user, googleSignupVO)
                        }
                    } else {
                        user = new User()
                        print '------------ in else user is------------' + user
                        String userIpAddress = request.getRemoteAddr()
                        user.firstName = googleSignupVO.firstName
                        user.lastName = googleSignupVO.lastName
                        user.username = googleSignupVO.email
                        user.ipAddress = userIpAddress
                        user.accountLocked = false
                        user.accountExpired = false
                        user.enabled = true
                        user.hasVerifiedEmail = true
                        user.password = googleSignupVO.defaultPassword
                        String randomAlphanumeric = RandomStringUtils.randomAlphanumeric(8)
                        Date today = new Date()
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat('dd/MM/yyyy')
                        String[] date = simpleDateFormat.format(today).split('/')
                        user.coustId = "TR" + "CS" + randomAlphanumeric.toUpperCase() + "${date[0]}" + "${date[1]}" + "${date[2]}"
                        user.save(flush: true)
                        print '------------   user savd of not ------------' + user
                        if (user.save()) {
                            print '------------ in if user role is------------' + user
                            UserRole.create(user, Role.findByAuthority('ROLE_USER'), true)
                            signupService.createGoogleSocialProfile(user, googleSignupVO)
                        } else {
                            print '------------ in else 22 user is------------' + user
                            redirect(action: 'auth', controller: 'login', params: [messageString: 'Unable to Create user.'])
                        }
                    }
                }
                if (springSecurityService.currentUser) {
                    print '------------ in   springSecurityService.currentUser------------' + springSecurityService.currentUser
                    flash.clear()
                    flash.message = "Your Google Profile updated successfully..."
                    redirect(action: 'index', controller: 'home')
                } else {
                    print '------------ in  else 333333  is------------' + user
                    SocialProfile googleSocialProfile = SocialProfile.findByUserAndType(user, SocialProfileType.GOOGLE)
                    print '------------ in else social profilfe  is------------' + googleSocialProfile
                    googleSocialProfile.accessToken = accessToken
                    googleSocialProfile.save(flush: true)
                    springSecurityService?.reauthenticate(googleSignupVO?.email)
                    redirect(action: 'index', controller: 'home')
                }
            }
        } else {
            redirect(action: 'auth', controller: 'login')
        }
    }
}
