package com.travelex.plan

import activity.Activity
import activity.ActivityBookingHistory
import activity.ActivityPicture
import com.travelex.Hotel.AddToActivitySummary
import com.travelex.Hotel.AddToTravelSummary
import com.travelex.Hotel.BookingHistory
import com.travelex.User.User

class PlanController {
    def springSecurityService
    def mghApiLayerService
    def plannerService

    def index() {
        Date startDate2
        List<Date> endDateList = []
        List<Date> startDateList = []

        def json = "[{},"
        User user = springSecurityService.currentUser as User

        List<AddToTravelSummary> addToTravelSummaryList = []

        List<AddToActivitySummary> addToActivitySummaryList = []
        List<BookingHistory> bookingHistoryList = BookingHistory.findAllByUser(user)
        List<ActivityBookingHistory> activityBookingHistoryList = ActivityBookingHistory.findAllByUser(user)
//        if (activityBookingHistoryList) {
//            json += plannerService.activityBooking(activityBookingHistoryList)
//            println '__________activityHistory_________' + json
//        }
//        if (addToActivitySummaryList) {
//            json += plannerService.activitySummary(addToActivitySummaryList)
//        }
        if (bookingHistoryList) {
            json += plannerService.bookingHistory(bookingHistoryList)
        }
//        if (addToTravelSummaryList) {
//            json += plannerService.addToTravelSummary(addToTravelSummaryList)
//        }
        json += "{}]"

        println '$$$$$$$$$$$%' + json

        render(view: "/calender/planer", model: [bookingHistoryList: bookingHistoryList, addToTravelSummaryList: addToTravelSummaryList, json: json, startDate2: startDate2, endDateList: endDateList, startDateList: startDateList, user: user])
    }


    def planTravelSummaryDetails = {
        def travelSummary = params.hotelId
        def roomType = params.roomType

        println '---------RoomType----------' + roomType
        def hotelVO = mghApiLayerService.getHotelDetails(travelSummary)

        hotelVO?.roomCategory.each {
            println '----------roomCategory---------' + it?.name
        }
        render(view: "/plan/detailedHistory", model: [travelSummary: travelSummary, hotelVOInstance: hotelVO, roomType: roomType])
    }


    def planActivityDetails = {
        Long activity = params.activity as Long
        Activity activity2 = Activity.findById(activity)
        println 'Activity____________' + activity2?.name
        ActivityPicture activityPicture2 = ActivityPicture.findByActivity(activity2)
        List<Activity> activityList1 = Activity.getAll()
        List<Activity> activityList3 = Activity.findAllByType(activity2?.type)
        activityList3.remove(activity2)
        activityList1.removeAll(activityList3)
        Collections.shuffle(activityList1)
        render(view: "/plan/detailedActivityHistory", model: [activityList1: activityList1, activityPicture: activityPicture2, activity2: activity2, activityList: activityList3])

    }


    def planBookingDetails = {
        def bookingHistory = params.hotelId
        def hotelVO = mghApiLayerService.getHotelDetails(bookingHistory)
        render(view: "/plan/detailedHistory", model: [bookingHistory: bookingHistory, hotelVOInstance: hotelVO])
    }


}
