package travelx

import java.math.RoundingMode

class TravelxTagLib {

    static namespace = "travelx"
    def createLink = { attrs ->
        String controller = attrs.controller
        String action = attrs.action
        Map params = attrs.params
//        Map params = attrs.params
        String link = createLink(controller: controller, action: action)
//        String link = createLink(controller: controller, action: action, params: params)
        println("************" + link)
        out << link
    }

    def renderAlert = {
        if (flash.success) {
            out << '<div class="alert alert-success alert-dismissable">\n' +
                    '  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n' +
                    '  <strong>Congrats!</strong> ' + flash.success + '\n' +
                    '</div>'
        } else if (flash.warning) {
            out << '<div class="alert alert-warning alert-dismissable">\n' +
                    '  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n' +
                    '  <strong>Error!</strong> ' + flash.warning + '\n' +
                    '</div>'
        } else if (flash.error) {
            out << '<div class="alert alert-danger alert-dismissable">\n' +
                    '  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n' +
                    '  <strong>Error!</strong> ' + flash.error + '\n' +
                    '</div>'
        }


    }

    def star = {
        out << "<b style='color:red;font-size:20px'>&nbsp;*</b>"
    }

    def adminUserTab = {
        out << g.render(template: "/admin/adminTabTemplate")
    }

    def calculatePerNightRoomCost={attrs ->
        String total=attrs.totalAmount
        String nights=attrs.noOfNights

        if(total!=""&&total!=" "&&nights!=""&&nights!=" "){
         double amount= Double.parseDouble(total)
         int night=Integer.parseInt(nights)
         double oneRoom=amount/night;
         out<< oneRoom?.toBigDecimal()?.setScale(2, RoundingMode.HALF_UP)
        }

    }

}
