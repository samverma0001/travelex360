package travelex

import com.travelex.Hotel.City
import org.codehaus.groovy.grails.web.context.ServletContextHolder



class SaveMGHCitiesJob {
    static triggers = {
//        simple repeatInterval: 5000l // execute job once in 5 seconds
    }

    def execute() {
        def a = ServletContextHolder.getServletContext().getRealPath("/")
        println '======AfterGetting path========'
        def file = new File(a, "/WEB-INF/xmlFiles/getAllLocations.xml")
        println '=====File=====' + file
        def locations = new XmlParser().parse(file)
        locations.children().location.eachWithIndex { locationData, itr ->
            if (itr == City.count()) {
                City city = new City(locationData)
                println '=======City========' + locationData.attribute("countryCode")
                city.save(flush: true)
                println '=======City= saved===at position====' + itr
            }
        }
    }
}
