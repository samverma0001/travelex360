package travelex

import com.travelex.TboApi.CityFromTBO
import com.travelex.TboApi.CountryFromTbo
import wslite.soap.SOAPClient
import wslite.soap.SOAPResponse

class SaveTboCitiesJob {
    def grailsApplication

//    public static TRAVELX_UNAME = 'travelx360'
//    public static TRAVELX_PASS_HOTEL = 'travel@1234'

    public static TRAVELX_UNAME = 'DELT261'
    public static TRAVELX_PASS_HOTEL = '360@travel'

    static triggers = {
//        simple repeatInterval: 500000l // execute job once in 5 seconds

//        simple repeatCount: 1
    }

    def execute() {
        List<CountryFromTbo> countryFromTbo = CountryFromTbo.getAll()
        countryFromTbo.each { country ->
            println '========string CITY====' + country.countryName

//            def client2 = new SOAPClient("http://api.tektravels.com/tbohotelapi_v6/hotelservice.asmx")
            def client2 = new SOAPClient("http://airapi.travelboutiqueonline.com/tbohotelapi_v6/hotelservice.asmx")

            SOAPResponse response2 = client2.send(
                    """<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
<soap:Header>
<AuthenticationData xmlns="http://TekTravel/HotelBookingApi">
<SiteName></SiteName>
<AccountCode></AccountCode>
<UserName>${TRAVELX_UNAME}</UserName>
<Password>${TRAVELX_PASS_HOTEL}</Password>
</AuthenticationData>
</soap:Header>
<soap:Body>
 <GetDestinationCityList xmlns="http://TekTravel/HotelBookingApi">
      <request>
        <CountryName>${country.countryName}</CountryName>
      </request>
    </GetDestinationCityList>
</soap:Body>
</soap:Envelope>"""
            )
            println '---==response==3333==' + response2
            def ref = new XmlSlurper().parseText(response2.text)
            ref.children().GetDestinationCityListResponse.GetDestinationCityListResult.CityList.WSCity.eachWithIndex { city, itr ->
                println '======== CityCode====' + city.CityCode
                println '======== CityName====' + city.CityName
                println '======== SPACE===='

                println '======== FOR COUNTRY====' + country.countryName

                CityFromTBO cityFromTBO = new CityFromTBO(city, country)
                cityFromTBO.save(flush: true)
            }
            println '========string Country====' + country.countryName
        }
    }
}