import activity.ActivitiesServiceProviders
import activity.Activity

import java.sql.Timestamp

class BootStrap {
    def bootStrapService

    def init = { servletContext ->
        bootStrapService.createBootStrapData()


        def activitiesServiceProvidersInst = ActivitiesServiceProviders.findByName("Travelx")
        def activitiesInst = Activity.findByName("Travelx Activity")
        def activityServiceProviderInst
        if(activitiesServiceProvidersInst == null) {
            activityServiceProviderInst = new ActivitiesServiceProviders(name: "Travelx",location: "Bangalore",authorisedPersonalName: "Vipul", phoneNumber: "9686968870",email: "chandu.javadevelpr@gmail.com",rating: 5,createdDate: new Timestamp(new Date().time),editedDate: new Timestamp(new Date().time))
            activityServiceProviderInst.save(flush: true,failOnError: true)
        }
        if(activitiesInst == null) {
            def activityInst = new Activity(name:"Travelx Activity",companyAuthorisedPersonalName: "Vipul",activityCompanyName: "Travelx",location: "Delhi",primaryInterest: "Swimming", secondaryInterest: "Eating", phoneNumber: "9686968870", email: "chandu.ys@yahoo.co.in", createdDate: new Timestamp(new Date().time),editedDate: new Timestamp(new Date().time),duration: "1Day",type: 'ADVENTURE_SPORTS',serviceProvider: activityServiceProviderInst)
            activityInst.save(flush: true,failOnError: true)
        }




    }
    def destroy = {
    }
}
