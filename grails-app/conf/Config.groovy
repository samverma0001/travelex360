// locations to search for config files that get merged into the main config;
// config files can be ConfigSlurper scripts, Java properties files, or classes
// in the classpath in ConfigSlurper format

grails.config.locations = ["classpath:${appName}-config.properties",
                           "classpath:${appName}-config.groovy",
                           "file:${userHome}/.grails/${appName}-config.properties",
                           "file:${userHome}/.grails/${appName}-config.groovy",
                           "classpath:local-config.groovy",
                           "file:tx-local-config.groovy",
                           //TODO: SpringSecurityConfig
]

// if (System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }

grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [
        all          : '*/*',
        atom         : 'application/atom+xml',
        css          : 'text/css',
        csv          : 'text/csv',
        form         : 'application/x-www-form-urlencoded',
        html         : ['text/html', 'application/xhtml+xml'],
        js           : 'text/javascript',
        json         : ['application/json', 'text/json'],
        multipartForm: 'multipart/form-data',
        rss          : 'application/rss+xml',
        text         : 'text/plain',
        xml          : ['text/xml', 'application/xml']
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// What URL patterns should be processed by the resources plugin
grails.resources.adhoc.patterns = ['/images/*', '/css/*', '/js/*', '/plugins/*']

// The default codec used to encode data with ${}
grails.views.default.codec = "none" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart = false
// Added by the JQuery Validation UI plugin:
grails.views.gsp.sitemesh.preprocess = false

jqueryValidationUi {
    errorClass = 'error'
    validClass = 'valid'
    onsubmit = true
    renderErrorsOnTop = false
    qTip {
        packed = true
        classes = 'ui-tooltip-red ui-tooltip-shadow ui-tooltip-rounded'
    }

    /*
      Grails constraints to JQuery Validation rules mapping for client side validation.
      Constraint not found in the ConstraintsMap will trigger remote AJAX validation.
    */
    StringConstraintsMap = [
            blank     : 'required', // inverse: blank=false, required=true
            creditCard: 'creditcard',
            email     : 'email',
            inList    : 'inList',
            minSize   : 'minlength',
            maxSize   : 'maxlength',
            size      : 'rangelength',
            matches   : 'matches',
            notEqual  : 'notEqual',
            url       : 'url',
            nullable  : 'required',
            unique    : 'unique',
            validator : 'validator'
    ]

    // Long, Integer, Short, Float, Double, BigInteger, BigDecimal
    NumberConstraintsMap = [
            min      : 'min',
            max      : 'max',
            range    : 'range',
            notEqual : 'notEqual',
            nullable : 'required',
            inList   : 'inList',
            unique   : 'unique',
            validator: 'validator'
    ]

    CollectionConstraintsMap = [
            minSize  : 'minlength',
            maxSize  : 'maxlength',
            size     : 'rangelength',
            nullable : 'required',
            validator: 'validator'
    ]

    DateConstraintsMap = [
            min      : 'minDate',
            max      : 'maxDate',
            range    : 'rangeDate',
            notEqual : 'notEqual',
            nullable : 'required',
            inList   : 'inList',
            unique   : 'unique',
            validator: 'validator'
    ]

    ObjectConstraintsMap = [
            nullable : 'required',
            validator: 'validator'
    ]

    CustomConstraintsMap = [
            phone  : 'true',
            phoneUS: 'true'
    ]
}
// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// configure auto-caching of queries by default (if false you can cache individual queries with 'cache: true')
grails.hibernate.cache.queries = false

grails.plugins.springsecurity.successHandler.defaultTargetUrl = '/home/index'

grails.gsp.tldScanPattern = 'classpath*:/META-INF/*.tld,/WEB-INF/tld/*.tld'
environments {
    development {
        grails.serverURL = 'http://localhost:8090/travelx'
        grails.logging.jul.usebridge = true
        google.apiKey = '560136143289-ve8q4s5imas7echi0jgqfmp97bnjhrp5.apps.googleusercontent.com'
        google.apiSecret = '8HAt4AV99FDb4ChL66hpYMlO'
        facebook.apiKey = '803730453025975'
        facebook.apiSecret = '79a714675f295e387e1e043c83aa6253'
        linkedIn.apiKey = '75lnbgwktw8zsb'
        linkedin.secret = 'X8CGuJRn2KK795Qb'
        tbo.TRAVELX_UNAME = 'travelx360'
        tbo.TRAVELX_PASS_HOTEL = 'travel@1234'
        tbo.FLIGHT_USERNAME = 'travelx360'
        tbo.FLIGHT_PASS = 'travel@1234'
        payUMoney.Key = 'JBZaLc'
        payUMoney.Salt = 'GQs7yium'

    }
    production {
        grails.logging.jul.usebridge = false
        grails.serverURL = "http://travelex360.com"
        // TODO: grails.serverURL = "http://www.changeme.com"
        google.apiKey = '145025867705-tvnvki1rtom07gf07e4434oshv2o4hk5.apps.googleusercontent.com'
        google.apiSecret = 'R7p_UxOdgHoxYm20v8MT18Jw'
        facebook.apiKey = '803054073093613'
        facebook.apiSecret = 'bc3a347c6b2aa3329db65dcdd1571986'
        linkedIn.apiKey = '75spm6qy30n7dv'
        linkedin.secret = 'tqX9spQH31XSDdTL'
        tbo.TRAVELX_UNAME = 'DELT261'
        tbo.TRAVELX_PASS_HOTEL = '360@travel'
        tbo.FLIGHT_USERNAME = 'travelx360'
        tbo.FLIGHT_PASS = 'travel@1234'
        payUMoney.Key = 'wO0qly'
        payUMoney.Salt = '4dqdJRa9'
    }
}

grails {
    mail {
        host = "smtp.gmail.com"
        port = 465
        username = "harish.kumar@recruitexindia.com"
        password = "harish@000"
        props = ["mail.smtp.auth"                  : "true",
                 "mail.smtp.socketFactory.port"    : "465",
                 "mail.smtp.socketFactory.class"   : "javax.net.ssl.SSLSocketFactory",
                 "mail.smtp.socketFactory.fallback": "false"]
    }
}
grails.mail.default.from = "harish.kumar@recruitexindia.com"
// log4j configuration
log4j = {
//    // Enable the Asynchronous Mail plugin logging
//    trace 'grails.app.jobs.grails.plugin.asyncmail', 'grails.app.services.grails.plugin.asyncmail'
//    // Enable the Quartz plugin logging
//    debug 'grails.plugins.quartz'
    // Example of changing the log pattern for the default console appender:
    //
    //appenders {
    //    console name:'stdout', layout:pattern(conversionPattern: '%c{2} %m%n')
    //}

    error 'org.codehaus.groovy.grails.web.servlet',        // controllers
            'org.codehaus.groovy.grails.web.pages',          // GSP
            'org.codehaus.groovy.grails.web.sitemesh',       // layouts
            'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
            'org.codehaus.groovy.grails.web.mapping',        // URL mapping
            'org.codehaus.groovy.grails.commons',            // core / classloading
            'org.codehaus.groovy.grails.plugins',            // plugins
            'org.codehaus.groovy.grails.orm.hibernate',      // hibernate integration
            'org.springframework',
            'org.hibernate',
            'net.sf.ehcache.hibernate'

    warn 'org.example.domain'
    info "grails.app"
    info "grails.app.taglib"
}

asynchronous.mail.override = true

// Added by the Spring Security Core plugin:
grails.plugins.springsecurity.userLookup.userDomainClassName = 'com.travelex.User.User'
grails.plugins.springsecurity.userLookup.authorityJoinClassName = 'com.travelex.User.UserRole'
grails.plugins.springsecurity.authority.className = 'com.travelex.User.Role'

//In Config.groovy
grails.plugins.remotepagination.max = 10
//EnableBootstrap here when using twitter bootstrap, default is set to false.
grails.plugins.remotepagination.enableBootstrap = true

grails.plugin.databasemigration.changelogLocation = "grails-app/migrations"
grails.plugin.databasemigration.changelogFileName = 'changelog.xml'
grails.plugin.databasemigration.updateOnStart = false
grails.plugin.databasemigration.updateOnStartFileNames = ['changelog.xml']