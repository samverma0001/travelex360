grails.servlet.version = "2.5" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.target.level = 1.6
grails.project.source.level = 1.6
grails.server.port.http = 8090
//grails.project.war.file = "target/${appName}-${appVersion}.war"

// uncomment (and adjust settings) to fork the JVM to isolate classpaths
//grails.project.fork = [
//   run: [maxMemory:1024, minMemory:64, debug:false, maxPerm:256]
//]
grails.project.fork = [
        // configure settings for compilation JVM, note that if you alter the Groovy version forked compilation is required
        //  compile: [maxMemory: 256, minMemory: 64, debug: false, maxPerm: 256, daemon:true],

        test   : [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, daemon: true],
        // configure settings for the run-app JVM
        run    : [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, forkReserve: false],
        // configure settings for the run-war JVM
        war    : [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, forkReserve: false],
        // configure settings for the Console UI JVM
        console: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256]
]
grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // specify dependency exclusions here; for example, uncomment this to disable ehcache:
        // excludes 'ehcache'
    }
    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    checksums true // Whether to verify checksums on resolve
    legacyResolve false
    // whether to do a secondary resolve on plugin installation, not advised and here for backwards compatibility

    repositories {
        inherits true // Whether to inherit repository definitions from plugins

        grailsPlugins()
        grailsHome()
        grailsCentral()

        mavenLocal()
        mavenCentral()

        // uncomment these (or add new ones) to enable remote dependency resolution from public Maven repositories
        //mavenRepo "http://snapshots.repository.codehaus.org"
        //mavenRepo "http://repository.codehaus.org"
        //mavenRepo "http://download.java.net/maven/2/"
        //mavenRepo "http://repository.jboss.com/maven2/"
    }

    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes e.g.
        compile 'com.github.groovy-wslite:groovy-wslite:0.7.2'
        runtime 'com.github.groovy-wslite:groovy-wslite:0.7.2'

        runtime 'javax.servlet:jstl:1.1.2'
        runtime 'taglibs:standard:1.1.2'
        runtime 'com.cloudinary:cloudinary-taglib:1.0.8'
        runtime 'com.cloudinary:cloudinary:1.0.8'
        runtime 'mysql:mysql-connector-java:5.1.22'

    }

    plugins {
        build ":tomcat:7.0.50"
//
//        runtime ":database-migration:1.3.8"
        runtime ":hibernate:3.6.10.7"
//        compile ":google-analytics:2.3.3"
//        runtime ":hibernate:${grailsVersion}" // or ":hibernate4:4.1.11.6"
        runtime ":jquery:1.8.3"
        runtime ":resources:1.2"
        compile ":mail:1.0.6"
        compile ":quartz:1.0.2"
        compile ":jquery-validation-ui:1.4.4"
        compile ":scaffolding:2.0.0"
//        compile ":amazon-s3:0.8.2"
//        compile ":aws-sdk:1.8.4"
        // Uncomment these (or add new ones) to enable additional resources capabilities
        //runtime ":zipped-resources:1.0"
        //runtime ":cached-resources:1.0"
        //runtime ":yui-minify-resources:0.1.5"
        compile ":remote-pagination:0.4.8"
        compile ":twitter-bootstrap:3.0.0"
        compile ":pretty-time:2.1.3.Final-1.0.1"
//        build ":tomcat:${grailsVersion}"
        compile ":spring-security-core:1.2.7.3"
        runtime ':database-migration:1.3.6'
        compile ':cache:1.0.1'
        compile ":richui:0.8"
        compile ":pdf:0.6"
        compile ":rendering:0.4.4"
        compile ":simple-captcha:1.0.0"
    }
}
