package com.travelex

//import org.bson.types.ObjectId

class WebUtils {

    static Random random = new Random();

//    public static WantDTO convertWant(UnsureWanted uw) {
//        if (uw == null) {
//            return null
//        }
//
//        return new WantDTO(description: uw.specsNote,
//                id: uw.id,
////                budget: uw.wishWellProd?.priceRange?.toString(),
//                recommendationCount: SavvyRecomm.countByWantedIdAndDeleted(uw.id, false),
////               commentCount: Comment.countByUnsureWanted(uw),
//                date: uw.dateCreated,
////                timeLeft: daysBetween(uw.timeframe.beginDate, uw.timeframe.endDate),
////                categoryMinPrice: uw.wishWellProd.prodCategory.minPrice,
////                categoryMaxPrice: uw.wishWellProd.prodCategory.maxPrice,
////                priceRange: uw.wishWellProd.priceRange,
//                prodCategory: uw.wishWellProd.prodCategory,
////                prefType: uw.wishWellProd.prefType.id,
////                tecValues: uw.wishWellProd.prodTecSpecValues.collect {it.id},
//                newInd: uw.newInd,
//                refurbished: uw.refurbished,
//                displayName: uw.urlDisplay(),
//                title: uw.title)
//    }


    public static int daysBetween(Date firstDate, Date secondDate) {

        Calendar startDate = firstDate.toCalendar()
        Calendar endDate = secondDate.toCalendar()

        Calendar date = (Calendar) startDate.clone();
        int daysBetween = 0;
        while (date.before(endDate)) {
            date.add(Calendar.DAY_OF_MONTH, 1);
            daysBetween++;
        }
        return daysBetween;
    }

    public static String randomizeKeyForImage(String key) {

        String s1 = key.substring(0, (key.length() / 2).intValue())
        String s2 = key.substring((key.length() / 2).intValue())

        key = s2 + s1

        return key.reverse().toString()


    }

//    public static Object getUserId(ObjectId id) {
//
//        return BuyerUser.get(id) != null ? BuyerUser.get(id) : null
//    }
//
//    public static String generateBuyItLink(RecommLink recommLink,
//                                           BuyerUser buyerUser = null,
//                                           String ipAddress = null) {
//
//        //Return the one that's not used if we deal with a logged in buyer user
//        if (buyerUser != null) {
//
//            def criteria = Transaction.createCriteria()
//
//            Transaction trx = criteria.get {
//                eq("buyerUser", buyerUser)
//                eq("pending", true)
//                eq("purchased", false)
//                eq("recommLink", recommLink)
//            }
//
//            if (trx != null) {
//                if (recommLink.link.indexOf("?") > -1) {
//                    return recommLink.link + "&trxId=" + trx.id
//                }
//                return recommLink.link + "?trxId=" + trx.id
//            }
//        }
//
//        //otherwise create one
//        Transaction trx = null
//        Transaction.withTransaction {
//            trx = new Transaction(pending: true, purchased: false,
//                    ipAddress: ipAddress,
//                    recommLink: recommLink)
//            if (buyerUser != null) {
//                trx.buyerUser = buyerUser
//            }
//
//            trx.save(flush: true)
//        }
//
//        if (recommLink.link.indexOf("?") > -1) {
//            return recommLink.link + "&trxId=" + trx.id
//        }
//
//        return recommLink.link + "?trxId=" + trx.id
//    }

    public static String convertToURLFriendly(String val) {

        val = val.trim()

        if (!val.contains("-")) {
            val = val.replaceAll(" ", "-")
        } else {
            val = val.replaceAll(" ", "_")
        }

        val = val.toLowerCase().encodeAsURL()

        return val
    }

    public static String convertSEOFriendlyParam(String val) {

        val = val.decodeURL()

        if (val.contains("_")) {
            val = val.replaceAll("_", " ")
        } else {
            val = val.replaceAll("-", " ")
        }

        return val
    }

    /**
     * @param floor
     * @param max
     */
    public static int generateRandomInRange(int floor, int max) {
        int result = random.nextInt(max)
        if (result < floor) {
            return result + floor
        }
        return result;
    }
}

