package com.travelex.resizeService

import grails.transaction.Transactional

import javax.imageio.ImageIO
import java.awt.Graphics2D
import java.awt.Image
import java.awt.geom.AffineTransform
import java.awt.image.BufferedImage

@Transactional
class ImageService {

    def serviceMethod() {

    }

    byte[] scale(byte[] srcFile, int destWidth, int destHeight) throws IOException {
        BufferedImage src = ImageIO.read(new ByteArrayInputStream(srcFile));
        BufferedImage dest = new BufferedImage(destWidth, destHeight,
                BufferedImage.TYPE_INT_RGB);
        Graphics2D g = dest.createGraphics();
        AffineTransform at = AffineTransform.getScaleInstance(
                (double) destWidth / src.getWidth(),
                (double) destHeight / src.getHeight());
        g.drawRenderedImage(src, at);
        ByteArrayOutputStream baos = new ByteArrayOutputStream()
        ImageIO.write(dest, "JPG", baos);
        return baos.toByteArray()
    }


    int getNewWidht(int realHeight, Image img) {
        double width = img.getWidth() * realHeight / img.getHeight()
        return (int) Math.floor(width)
    }

    int getNewHeight(int realWidth, Image img) {
        double height = img.getHeight() * realWidth / img.getWidth()
        return (int) Math.floor(height)
    }

    void storeImageInFileSystem(File file, byte[] avatar) {
        if (avatar) {
            FileOutputStream fos = new FileOutputStream(file);
            BufferedImage src = ImageIO.read(new ByteArrayInputStream(avatar))
            if (src.getHeight() > src.getWidth()) {
                fos.write(scale(avatar, getNewWidht(400, src), 400))
            } else {
                fos.write(scale(avatar, 400, getNewHeight(400, src)))
            }
            fos.close()
        }
    }


}
