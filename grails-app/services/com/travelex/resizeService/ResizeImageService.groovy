package com.travelex.resizeService

import util.AppUtil

import javax.imageio.ImageIO
import java.awt.*
import java.awt.image.BufferedImage

class ResizeImageService {


    public static scaleImage(Long id, String imageType) {
        String image = imageType
        def webRootDir = AppUtil.staticResourcesDirPath
        String path
        File folder
        def userDir
        if (image.equals('packageImage')) {
            print '---------------inside packageImage-----------'
            userDir = new File(webRootDir, "/uploadedPackageImages/${id}/resize")
            userDir.mkdirs()
            folder = new File(webRootDir, "/uploadedPackageImages/${id}");
        } else if (image.equals('packageHotelImage')) {
            print '---------------inside packageHotelImage-----------'
            userDir = new File(webRootDir, "/packageHotelImages/${id}/resize")
            userDir.mkdirs()
            folder = new File(webRootDir, "/packageHotelImages/${id}");
        } else if (image.equals('holidayImage')) {
            print '---------------inside holidayImage-----------'
            print '---------------webRootDir webRootDir-----------' + webRootDir
            userDir = new File(webRootDir, "/holidayImages/${id}/resize")
            userDir.mkdirs()
            folder = new File(webRootDir, "/holidayImages/${id}");
        }
        print('before list of files')
        File[] listOfFiles = folder.listFiles();
        System.out.println("Total No of Files:" + listOfFiles.length);

//        File newFileJPG3 = null;
        for (int i = 0; i < listOfFiles.length; i++) {
            print('---------------1111-------------')
            if (listOfFiles[i].isFile()) {
                print('--------------2222-2-------------')
                String fileName = listOfFiles[i].getName()
                println("File " + listOfFiles[i].getName());
                attachDocumentAndResize(id, fileName, webRootDir, image)
            }
        }
        System.out.println("DONE");
    }


    public static void attachDocumentAndResize(Long id, String fileName, String webRootDir, String imgType) {
        String fileNameToken = fileName.tokenize('.').first()
        String fileExtension = fileName.tokenize('.').last()
        Image img = null;
        BufferedImage tempJPG = null;
        BufferedImage tempJPG1 = null;
        BufferedImage tempJPG2 = null;
        BufferedImage tempJPG3 = null;
        File newFileJPG = null;
        File newFileJPG1 = null;
        File newFileJPG2 = null;

        if (imgType.equals('packageImage')) {
            img = ImageIO.read(new File(webRootDir, "/uploadedPackageImages/${id}/" + fileName));
            print("--------------img-----11--------------------" + img)
            tempJPG = resizeImage(img, 50, getNewHeight(50, img));
            newFileJPG = new File(webRootDir, "/uploadedPackageImages/${id}/resize/" + fileNameToken + "${AppUtil.FILE_50}.${fileExtension}");
            ImageIO.write(tempJPG, "jpg", newFileJPG);
            tempJPG1 = resizeImage(img, 200, getNewHeight(200, img));
            newFileJPG1 = new File(webRootDir, "/uploadedPackageImages/${id}/resize/" + fileNameToken + "${AppUtil.FILE_200}.${fileExtension}");
            ImageIO.write(tempJPG1, "jpg", newFileJPG1);
            tempJPG2 = resizeImage(img, 500, getNewHeight(500, img));
            newFileJPG2 = new File(webRootDir, "/uploadedPackageImages/${id}/resize/" + fileNameToken + "${AppUtil.FILE_500}.${fileExtension}");
            ImageIO.write(tempJPG2, "jpg", newFileJPG2);
        } else if (imgType.equals('packageHotelImage')) {
            img = ImageIO.read(new File(webRootDir, "/packageHotelImages/${id}/" + fileName));
            print("--------------img-----22--------------------" + img)
            tempJPG = resizeImage(img, 50, getNewHeight(50, img));
            newFileJPG = new File(webRootDir, "/packageHotelImages/${id}/resize/" + fileNameToken + "${AppUtil.FILE_50}.${fileExtension}");
            ImageIO.write(tempJPG, "jpg", newFileJPG);
            tempJPG1 = resizeImage(img, 200, getNewHeight(200, img));
            newFileJPG1 = new File(webRootDir, "/packageHotelImages/${id}/resize/" + fileNameToken + "${AppUtil.FILE_200}.${fileExtension}");
            ImageIO.write(tempJPG1, "jpg", newFileJPG1);
            tempJPG2 = resizeImage(img, 500, getNewHeight(500, img));
            newFileJPG2 = new File(webRootDir, "/packageHotelImages/${id}/resize/" + fileNameToken + "${AppUtil.FILE_500}.${fileExtension}");
            ImageIO.write(tempJPG2, "jpg", newFileJPG2);
        } else if (imgType.equals('holidayImage')) {
            img = ImageIO.read(new File(webRootDir, "/holidayImages/${id}/" + fileName));
            print("--------------img-----22--------------------" + img)
            tempJPG = resizeImage(img, 50, getNewHeight(50, img));
            newFileJPG = new File(webRootDir, "/holidayImages/${id}/resize/" + fileNameToken + "${AppUtil.FILE_50}.${fileExtension}");
            ImageIO.write(tempJPG, "jpg", newFileJPG);
            tempJPG1 = resizeImage(img, 200, getNewHeight(200, img));
            newFileJPG1 = new File(webRootDir, "/holidayImages/${id}/resize/" + fileNameToken + "${AppUtil.FILE_200}.${fileExtension}");
            ImageIO.write(tempJPG1, "jpg", newFileJPG1);
            tempJPG2 = resizeImage(img, 500, getNewHeight(500, img));
            newFileJPG2 = new File(webRootDir, "/holidayImages/${id}/resize/" + fileNameToken + "${AppUtil.FILE_500}.${fileExtension}");
            ImageIO.write(tempJPG2, "jpg", newFileJPG2);
        }

    }


    public static BufferedImage resizeImage(final Image image, int width, int height) {
        final BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        final Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.setComposite(AlphaComposite.Src);
        //below three lines are for RenderingHints for better image quality at cost of higher processing time
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.drawImage(image, 0, 0, width, height, null);
        graphics2D.dispose();
        return bufferedImage;
    }

    private static int getNewHeight(int realWidth, Image img) {
        double height = img.getHeight() * realWidth / img.getWidth()
        return (int) Math.floor(height)
    }


}
