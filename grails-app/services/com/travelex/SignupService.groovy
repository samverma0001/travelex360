package com.travelex

import com.travelex.User.User
import com.travelex.social.SocialProfile
import com.travelex.socials.FacebookSignupVO
import com.travelex.socials.GoogleSignupVO
import com.travelex.socials.LinkedInSignupVO
import grails.converters.JSON

class SignupService {

    def grailsApplication

    public SocialProfile createFacebookSocialProfile(User user, FacebookSignupVO facebookSignupVO) {
        SocialProfile socialProfile = new SocialProfile(user, facebookSignupVO)
        socialProfile.alternateEmail = facebookSignupVO?.email
        socialProfile.user = user
        user.addToSocialProfileList(socialProfile)
        if (!socialProfile.save(flush: true)) {
            socialProfile.errors.allErrors.each {
                println('--------------SocialProfile Facebook------------------------' + it)
            }
        }
        return socialProfile
    }

    public updateFacebookSocialProfile(SocialProfile socialProfile, FacebookSignupVO facebookSignupVO) {
        print("------------------------------------------")
        print(facebookSignupVO)
        socialProfile.alternateEmail = facebookSignupVO?.email
        socialProfile.profileId = facebookSignupVO.profileId
        socialProfile.accessToken = facebookSignupVO.accessToken
        socialProfile.name = facebookSignupVO.name
        socialProfile.userName = facebookSignupVO.facebookUserName
        socialProfile.hometown = facebookSignupVO.hometown
        socialProfile.gender = facebookSignupVO.gender
        socialProfile.profileUrl = facebookSignupVO.link
        socialProfile.birthday = facebookSignupVO.birthday
        socialProfile.location = facebookSignupVO.currentLocation
    }


    public postCallForLinkedInAccessToken(String authCode) {
        String serverUrl = grailsApplication.config.grails.serverURL
        print '---------serverurl--------------------' + serverUrl
        String callbackurl = "${serverUrl}/signIn/linkedInCallback"
        print '---------callbackurl--------------------' + callbackurl
        String linkedInTokenUrl = "https://www.linkedin.com/uas/oauth2/accessToken?"
        print '---------linkedInTokenUrl--------------------' + linkedInTokenUrl
        StringBuilder sb = new StringBuilder("grant_type=authorization_code&code=");
        print '---------sb--------------------' + sb
        sb.append(URLEncoder.encode(authCode, "UTF-8"));
        sb.append("&redirect_uri=");
        sb.append(URLEncoder.encode(callbackurl, "UTF-8"));
        sb.append("&client_id=");
        sb.append(URLEncoder.encode("${grailsApplication.config.linkedIn.apiKey}", "UTF-8"));
        sb.append("&client_secret=");
        sb.append(URLEncoder.encode("${grailsApplication.config.linkedin.secret}", "UTF-8"));
        print '---------sb-22222-------------------' + sb
        URL url = new URL(linkedInTokenUrl)
        print '---------url--------------------' + url
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        print '---------connection--------------------' + connection
        String accessToken = ''
        try {
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Length", "" + sb.toString().length());
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream());
            outputStreamWriter.write(sb.toString());
            outputStreamWriter.flush();
            String resultData = connection?.content?.text
            def responseJson = JSON.parse(resultData)
            accessToken = responseJson.access_token
            println("Response code ${connection.responseCode} , Message : ${connection.responseMessage}")
            if (connection.responseCode != 200) {
                println("Unable to Make Post Call")
            }
        } finally {
            connection?.disconnect()
        }
        println("------------------------access token---------------" + accessToken)
        return accessToken
    }

    public createLinkedInSocialProfile(User user, LinkedInSignupVO linkedInSignupVO) {
        SocialProfile socialProfile = new SocialProfile(user, linkedInSignupVO)
        socialProfile.alternateEmail = linkedInSignupVO.email
        socialProfile.user = user
        user.addToSocialProfileList(socialProfile)
        if (!socialProfile.save(flush: true)) {
            socialProfile.errors.allErrors.each {
                println('--------------SocialProfile LinkedIn------------------------' + it)
            }
        }
    }

    public updateLinkedInSocialProfile(SocialProfile socialProfile, LinkedInSignupVO linkedInSignupVO) {
        socialProfile.profileId = linkedInSignupVO.profileId
        socialProfile.alternateEmail = linkedInSignupVO.email
        socialProfile.name = linkedInSignupVO.name
        socialProfile.location = linkedInSignupVO.location
        socialProfile.summary = linkedInSignupVO.summary
        socialProfile.gender = linkedInSignupVO.gender
        socialProfile.birthday = linkedInSignupVO.dateOfBirth
        socialProfile.profileUrl = linkedInSignupVO?.publicProfileUrl
        socialProfile.save(flush: true)
    }

    public LinkedInSignupVO getLinkedInUserProfile(String accessToken) {
        String urlString = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,picture-url,public-profile-url,educations,positions,specialties,headline,summary,industry,location:(name),date-of-birth,languages,connections)?oauth2_access_token=${accessToken}&format=json";
        URL url = new URL(urlString)
        String jsonResponse = url.text
        print '-------jsonResponse-------------------' + jsonResponse
        def responseJson = JSON.parse(jsonResponse)
        print '-------responseJson-------------------' + responseJson
        LinkedInSignupVO linkedInSignupVO = new LinkedInSignupVO(responseJson)
        linkedInSignupVO.accessToken = accessToken
        return linkedInSignupVO
    }


    public String postCallForGoogleAccessToken(String code) {
        String serverUrl = grailsApplication.config.grails.serverURL
        String callbackurl = "${serverUrl}/signIn/googleCallback"
        StringBuilder sb = new StringBuilder("code=");
        sb.append(URLEncoder.encode(code, "UTF-8"));
        sb.append("&client_id=");
        sb.append(URLEncoder.encode("${grailsApplication.config.google.apiKey}", "UTF-8"));
        sb.append("&client_secret=");
        sb.append(URLEncoder.encode("${grailsApplication.config.google.apiSecret}", "UTF-8"));
        sb.append("&redirect_uri=");
        sb.append(URLEncoder.encode(callbackurl, "UTF-8"));
        sb.append("&grant_type=");
        sb.append(URLEncoder.encode('authorization_code', "UTF-8"));
        URL url = new URL('https://accounts.google.com/o/oauth2/token');
        HttpURLConnection connection = (HttpURLConnection) url.openConnection()
        String accessToken = ''
        try {
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Length", "" + sb.toString().length());
            connection.setRequestProperty("Host", "accounts.google.com");
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream());
            outputStreamWriter.write(sb.toString());
            outputStreamWriter.flush();
            println("Response code ${connection.responseCode} , Message : ${connection.responseMessage}")
            String resultData = connection.content.text
            def responseJson = JSON.parse(resultData)
            accessToken = responseJson?.access_token
        }
        catch (Exception e) {
            println("Failed to get access token from google " + e)
        }
        return accessToken
    }


    public createGoogleSocialProfile(User user, GoogleSignupVO googleSignupVO) {
        SocialProfile socialProfile = new SocialProfile(user, googleSignupVO)
        user.addToSocialProfileList(socialProfile)
        if (!socialProfile.save(flush: true)) {
            socialProfile.errors.allErrors.each {
                println('--------------SocialProfile Google------------------------' + it)
            }
        }
    }

    public GoogleSignupVO getGoogleUserProfile(String accessToken) {
        String urlString = "https://www.googleapis.com/oauth2/v1/userinfo??output=json&access_token=${accessToken}"
        URL url = new URL(urlString)
        println("----------------------------------response text------------------" + url)
        String responseText = url.text
        println("----------------------------------response text------------------" + responseText)
        def jsonData = JSON.parse(responseText)
        println("----------------------------------jsonData------------------" + jsonData)
        GoogleSignupVO googleSignupVO = new GoogleSignupVO(jsonData)
        googleSignupVO.accessToken = accessToken
        return googleSignupVO
    }


}
