package com.travelex.planner

import activity.ActivityBookingHistory
import com.travelex.Hotel.AddToActivitySummary
import com.travelex.Hotel.AddToTravelSummary
import com.travelex.Hotel.BookingHistory
import travelx.TravelxTagLib

import org.codehaus.groovy.grails.plugins.web.taglib.ApplicationTagLib

import java.text.ParseException
import java.text.SimpleDateFormat

class PlannerService {

    ApplicationTagLib abc = new ApplicationTagLib()

//    def json  ="{},"
    def activityBooking(List<ActivityBookingHistory> activityBookingHistoryList) {
//        def json
        def json = "{},"
        activityBookingHistoryList.each {

            String startActivity = it?.actStart
            String endActivity = it?.actEnd
            String identity = it?.activityId
            println '_________+++++++++_______' + startActivity
            println '_________+++++++++_______' + endActivity
            println '_________JSON_______' + json
            println '_________ID_______' + identity

            Date startDate
            Date endDate
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("MM/dd/yyyy");
            try {

                startDate = simpleDateFormat.parse(startActivity);
                endDate = simpleDateFormat2.parse(endActivity);
                println '!!!!!!!!@@@@@@@@@ Addsummary Start Date' + startDate
                System.out.println("date : " + simpleDateFormat.format(startDate));

            }
            catch (ParseException ex) {
                System.out.println("Exception " + ex);
            }

            Date d = startDate
            def m = d.format("MM")
            def y = d.format("yyyy")
            def day = d.format("dd")

            Date d2 = endDate
            def m2 = d2.format("MM")
            def y2 = d2.format("yyyy")
            def day2 = d2.format("dd")


            json += "{"
            json += "title:'Activity Booking:-${it?.activityName}',"
            json += "start:new Date(${y},${m}-1,${day}),"
            json += "end:new Date(${y2},${m2}-1,${day2}),"
            json += "allDay: true,"
            json += "url: '${abc.createLink(controller: "plan", action: "planActivityDetails", params: [activity: it?.activityId])}',"
            json += "className:'fc-charcoal'"
            json += " } ,"

        }
        return (json)
    }

    def activitySummary(List<AddToActivitySummary> addToActivitySummaryList) {
//        def json
        def json = "{},"

        addToActivitySummaryList.each {
            String startActivity = it?.addedOn
            println '_________+++++++++_______' + startActivity
            Date startDate
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
            try {

                startDate = simpleDateFormat.parse(startActivity);
                println '!!!!!!!!@@@@@@@@@ Addsummary Start Date' + startDate
                System.out.println("date : " + simpleDateFormat.format(startDate));

            }
            catch (ParseException ex) {
                System.out.println("Exception " + ex);
            }
            Date d = startDate
            def start = d.format("yyyy-mm-dd")
            def m = d.format("MM")
            def y = d.format("yyyy")
            def day = d.format("dd")
            json += "{"
            json += "title:'Activity Summary:-${it?.name}',"
            json += "start:new Date(${y},${m}-1,${day}),"
            json += "allDay: false,"
            json += "url: '${abc.createLink(controller: "plan", action: "planActivityDetails", params: [activity: it?.actId])}',"
            json += "className:'fc-grey'"
            json += " } ,"

        }
        return (json)

    }

    def bookingHistory(List<BookingHistory> bookingHistoryList) {
//        def json
        def json = "{},"

        bookingHistoryList.each {
            String endEvent = it.checkOut
            String event = it?.checkIn
            String roomType = it?.roomCategory
            Date startDate
            Date endDate
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
            try {

                startDate = simpleDateFormat.parse(event);
                endDate = simpleDateFormat2.parse(endEvent);
                println '!!!!!!!!@@@@@@@@@' + startDate
                System.out.println("date : " + simpleDateFormat.format(startDate));
                System.out.println("date : " + simpleDateFormat.format(endDate));

            }
            catch (ParseException ex) {
                System.out.println("Exception " + ex);
            }


            Date d = startDate
            def m = d.format("MM")
            println '!!!!!!!!!!!!!!month' + m
            def y = d.format("yyyy")
            def day = d.format("dd")

            println '____---------++++++++________' + day
            Date d2 = endDate
            def m2 = d2.format("MM")
            println '!!!!!!!!!!!!!!month' + m2
            def y2 = d2.format("yyyy")
            def day2 = d2.format("dd")

            println '____---------++++++++________' + day2

//            TravelxTagLib tagLib=new TravelxTagLib()
//            tagLib.createLink(controller: 'plan',action: 'planActivityDetails')


            json += "{"
            json += "title:'Booking for:-${it?.hotelName}',"
            json += "start:new Date(${y},${m}-1,${day}),"
            json += "end:new Date(${y2},${m2}-1,${day2}),"
            json += "allDay: true,"
            json += "url: '${abc.createLink(controller: "plan", action: "planTravelSummaryDetails", params: [hotelId: it?.hotelId, roomType: roomType])}',"
            json += "className:'fc-red'"
            json += " } ,"

//        }
        }
        return (json)

    }

    def addToTravelSummary(List<AddToTravelSummary> addToTravelSummaryList) {
//        def json
        def json = "{},"

        List<Date> addToTravelSummaryStart = []
        List<Date> addToTravelSummaryEnd = []
        addToTravelSummaryList.each {

            String endActivity = it?.travelEndDate
            String startActivity = it?.travelStartDate
            println '_________+++++++++_______' + startActivity
            String roomType = it?.roomType
            Date startDate
            Date endDate
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("MM/dd/yyyy");
            try {

                startDate = simpleDateFormat.parse(startActivity);
                endDate = simpleDateFormat2.parse(endActivity);
                println '!!!!!!!!@@@@@@@@@ Addsummary Start Date' + startDate
                println '!!!!!!!!@@@@@@@@@ Addsummary Start Date' + endDate
                System.out.println("date : " + simpleDateFormat.format(startDate));

            }
            catch (ParseException ex) {
                System.out.println("Exception " + ex);
            }
            addToTravelSummaryStart.add(startDate)
            addToTravelSummaryEnd.add(endDate)
//
            Date d2 = endDate
            def m2 = endDate.format("MM")
            def y2 = d2.format("yyyy")
            def day2 = d2.format("dd")

            Date d = startDate
            def m = d.format("MM")
            def y = d.format("yyyy")
            def day = d.format("dd")

            json += "{"
            json += "title:'Travel Summary:-${it?.hotelName}',"
            json += "start:new Date(${y},${m}-1,${day}),"
            json += "end:new Date(${y2},${m2}-1,${day2}),"
            json += "allDay: true,"
//            json += "url: '${abc.createLink(controller: "plan" , action:  "planTravelSummaryDetails", params: [hotelId: it?.hotelId,roomType:roomType])}',"
            json += "url: '${abc.createLink(controller: "AddToTravelSummary", action: "index", params: [summaryCalId: it.id])}',"

            json += "className:'fc-yellow'"
            json += " } ,"

//        }
        }
        return (json)

    }


}
