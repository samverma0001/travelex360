package com.travelex.hash

import grails.transaction.Transactional

import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

@Transactional
class HashCalculationService {

    def calculateHashCode(String message) {
        MessageDigest md;
        String hash = "";
        md = MessageDigest.getInstance("SHA-512");

        md.update(message.getBytes());
        byte[] mb = md.digest();
        for (int i = 0; i < mb.length; i++) {
            byte temp = mb[i];
            String s = Integer.toHexString(new Byte(temp));
            while (s.length() < 2) {
                s = "0" + s;
            }
            s = s.substring(s.length() - 2);
            hash += s;
        }
        System.out.println(hash.length());
        System.out.println("CRYPTO: " + hash);

        return hash


    }
}
