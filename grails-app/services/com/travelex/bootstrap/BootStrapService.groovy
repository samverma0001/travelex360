package com.travelex.bootstrap

import com.travelex.Hotel.Hotel
import com.travelex.Hotel.HotelRoom
import com.travelex.TboApi.CountryFromTbo
import com.travelex.User.Role
import com.travelex.User.User
import com.travelex.User.UserRole
import com.travelex.admin.AmountType
import com.travelex.admin.MarginType
import com.travelex.admin.SetMargin
import com.travelex.admin.Status
import com.travelex.common.Address
import com.travelex.common.Facility
import com.travelex.flight.FlightCity
import com.travelex.socials.Enums.FacilityFor
import com.travelex.socials.Enums.HotelType
import org.apache.commons.lang.RandomStringUtils
import serviceProvider.ServiceProvider
import wslite.soap.SOAPClient
import wslite.soap.SOAPResponse

import java.text.SimpleDateFormat

class BootStrapService {

//    public static TRAVELX_UNAME = 'travelx360'
//    public static TRAVELX_PASS_HOTEL = 'travel@1234'

    public static TRAVELX_UNAME = 'DELT261'
    public static TRAVELX_PASS_HOTEL = '360@travel'


    def grailsApplication

    void createBootStrapData() {
        createRoles()
        createAdmin()
        createUser()
        createSupplier()
        tboSaveCountry()
        createServiceProvider()
//        flightCity()
        if (SetMargin.count) {
        } else {
            createMargin()
        }
    }


    void createMargin() {
        SetMargin setDomsticMargin = new SetMargin()
        setDomsticMargin.marginMoney = '200'
        setDomsticMargin.payUMoneyStake = '2.96'
        setDomsticMargin.marginMoney = '200'
        setDomsticMargin.validFrom = new Date().clearTime()
        setDomsticMargin.validTo = new Date().clearTime()
        setDomsticMargin.status = Status.ACTIVE
        setDomsticMargin.amountType = AmountType.AMOUNT
        setDomsticMargin.marginType = MarginType.DOMHOTEL
        setDomsticMargin.save(flush: true)

        SetMargin setInterMargin = new SetMargin()
        setInterMargin.marginMoney = '200'
        setInterMargin.payUMoneyStake = '2.96'
        setInterMargin.marginMoney = '200'
        setInterMargin.validFrom = new Date().clearTime()
        setInterMargin.validTo = new Date().clearTime()
        setInterMargin.status = Status.ACTIVE
        setInterMargin.amountType = AmountType.AMOUNT
        setInterMargin.marginType = MarginType.INTERHOTEL
        setInterMargin.save(flush: true)

    }

    void flightCity() {
        FlightCity flightCity = new FlightCity()
        flightCity.cityName = "Agra"
        flightCity.cityCode = "(AGR)"
        flightCity.countryCode = "IN"

        flightCity.save(flush: true)

        FlightCity flightCity1 = new FlightCity()
        flightCity1.cityName = "Mumbai"
        flightCity1.cityCode = "(BOM)"
        flightCity1.countryCode = "IN"

        flightCity1.save(flush: true)

    }

    void createRoles() {
        if (!Role.count()) {
            Role adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
            Role userRole = new Role(authority: 'ROLE_USER').save(flush: true)
            Role providerRole = new Role(authority: 'ROLE_PROVIDER').save(flush: true)
        }
    }

    void createAdmin() {
        String pass = RandomStringUtils.randomAlphanumeric(8)
        String randomAlphanumeric = RandomStringUtils.randomAlphanumeric(8)
        Date today = new Date()
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat('dd/MM/yyyy')
        String[] date = simpleDateFormat.format(today).split('/')
        if (!User.count()) {
            Role role = Role.findByAuthority('ROLE_ADMIN')
            User admin1 = new User(firstName: 'vipul', username: 'vipulgupta0001@email.com', coustId: "TR" + "CS" + randomAlphanumeric.toUpperCase() + "${date[0]}" + "${date[1]}" + "${date[2]}", enabled: true, password: 'vipulgupta', mobNo: '9988776655')
            if (admin1.validate() && !admin1.hasErrors()) {
                admin1.save(flush: true, failOnError: true)
                UserRole.create(admin1, role, true)
            }

            User admin2 = new User(username: 'sumitverma0001@email.com', firstName: 'sumit', enabled: true, password: 'sumitverma', mobNo: '9813046928')
            if (admin2.validate() && !admin2.hasErrors()) {
                admin2.save(flush: true, failOnError: true)
                UserRole.create(admin2, role, true)
            }
        }

    }

    void createUser() {
        String pass = RandomStringUtils.randomAlphanumeric(8)
        String randomAlphanumeric = RandomStringUtils.randomAlphanumeric(8)
        Date today = new Date()
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat('dd/MM/yyyy')
        String[] date = simpleDateFormat.format(today).split('/')
        Role role = Role.findByAuthority('ROLE_USER')
        if (!User.findByUsername('user1@email.com')) {
            User user1 = new User(username: 'user1@email.com', firstName: 'user1', coustId: "TR" + "CS" + randomAlphanumeric.toUpperCase() + "${date[0]}" + "${date[1]}" + "${date[2]}", enabled: true, password: '1234', mobNo: '9988776655')
            if (user1.validate() && !user1.hasErrors()) {
                user1.save(flush: true, failOnError: true)
                UserRole.create(user1, role, true)
            }
        }

        if (!User.findByUsername('user2@email.com')) {
            User user2 = new User(username: 'user2@email.com', firstName: 'user2', enabled: true, password: '1234', mobNo: '9988776655')
            if (user2.validate() && !user2.hasErrors()) {
                user2.save(flush: true, failOnError: true)
                UserRole.create(user2, role, true)
            }
        }
    }

    void createSupplier() {
        Role role = Role.findByAuthority('ROLE_PROVIDER')
        String randomAlphanumeric = RandomStringUtils.randomAlphanumeric(8)
        Date today = new Date()
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat('dd/MM/yyyy')
        String[] date = simpleDateFormat.format(today).split('/')
        if (!User.findByUsername('kmr.saurabh@travelx360.com')) {
            User user2 = new User(username: 'sumitverma@travelx360.com', firstName: 'sumit', coustId: "TR" + "CS" + randomAlphanumeric.toUpperCase() + "${date[0]}" + "${date[1]}" + "${date[2]}", enabled: true, password: '1234', mobNo: '98130469285')
            if (user2.validate() && !user2.hasErrors()) {
                user2.save(flush: true, failOnError: true)
                UserRole.create(user2, role, true)
            }
        }

        if (!User.findByUsername('kr.saurabh@travelx360.com')) {
            User user2 = new User(username: 'kr.saurabh@travelx360.com', firstName: 'Saurabh', enabled: true, password: '1234', mobNo: '9988776655')
            if (user2.validate() && !user2.hasErrors()) {
                user2.save(flush: true, failOnError: true)
                UserRole.create(user2, role, true)
            }
        }


    }

    def tboSaveCountry = {
        if (!CountryFromTbo.count()) {

//            def client = new SOAPClient("http://api.tektravels.com/tbohotelapi_v6/hotelservice.asmx")
            def client = new SOAPClient("http://airapi.travelboutiqueonline.com/tbohotelapi_v6/hotelservice.asmx")

            SOAPResponse response = client.send(
                    """<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
<soap:Header>
<AuthenticationData xmlns="http://TekTravel/HotelBookingApi">
<SiteName></SiteName>
<AccountCode></AccountCode>
<UserName>${TRAVELX_UNAME}</UserName>
<Password>${TRAVELX_PASS_HOTEL}</Password>
</AuthenticationData>
</soap:Header>
<soap:Body>
<GetCountryList xmlns="http://TekTravel/HotelBookingApi" />
</soap:Body>
</soap:Envelope>"""
            )
            println '---==response==3333==' + response
            def countryList = new XmlSlurper().parseText(response.text)
            println '---==countryList==' + countryList

            countryList.children().GetCountryListResponse.GetCountryListResult.CountryList.string.each { country ->
                CountryFromTbo countryFromTbo = new CountryFromTbo()
                countryFromTbo.countryName = country.text()
                countryFromTbo.save(flush: true)
            }
        }
    }


    void createServiceProvider() {
        if (!ServiceProvider.count()) {

//            Hotel hotel1 = new Hotel()

//            Address address1 = new Address()
//            address1.address1 = "Marine drive"
//            address1.address2 = "near marine drive"
//            address1.city = "Mumbai"
//            address1.zipCode = "022002"
//            address1.country = "India"
//            hotel1.name = "Taj Hotel Mumbai"
//            hotel1.summary = "Best Hotel In Town"
//            hotel1.type = HotelType.RESORTS_AND_PLACES
//            hotel1.save(flush: true)
//            address1.hotel = hotel1
//            address1.save(flush: true)
//
//
//            HotelRoom hotelRoom1 = new HotelRoom()
//            hotelRoom1.name = "Lounge"
//            hotelRoom1.description = "Best Hotel In Town"
//            hotelRoom1.price = 5000
//            hotelRoom1.hotel = hotel1
//            hotel1.addToHotelRooms(hotelRoom1)
//            hotel1.save(flush: true)
//            hotelRoom1.save(flush: true)

//            Picture pictureRoom1 = new Picture()
//            pictureRoom1.filePath = "http://0.tqn.com/d/goflorida/1/7/q/j/universal-hardrock-room.jpeg"
//            pictureRoom1.contentType = "jpeg"
//            hotelRoom1.addToPictures(pictureRoom1)
//            hotelRoom1.save(flush: true)
//            pictureRoom1.save(flush: true)
//
//            Picture picture1 = new Picture()
//            picture1.filePath = "http://www.durhamld.com/images/Taj_01.jpg"
//            picture1.contentType = "jpeg"
//            hotel1.addToPictures(picture1)
//            hotel1.save(flush: true)
//            picture1.save(flush: true)


//            Facility facility5 = new Facility()
//            facility5.name = "Parking"
//            facility5.facilityFor = FacilityFor.HOTEL
//            facility5.hotelId = hotel1.id
////        hotel1.addToHotelFacilities(facility5)
//            hotel1.save(flush: true)
//            facility5.save(flush: true)
//
//            User user = User.findByUsername("kmr.saurabh@travelx360.com")
//            ServiceProvider serviceProvider1 = new ServiceProvider()
//            serviceProvider1.nameOfHotel = 'travelx'
//            serviceProvider1.city = 'Noida'
//            serviceProvider1.state = 'UP'
//            serviceProvider1.address = 'Sector -62'
//            serviceProvider1.propertyType = 'hotel'
//            serviceProvider1.nameOfRepresentee = 'Travelex'
//            serviceProvider1.officialEmail = 'kmr.saurabh@travelx360.com'
//            serviceProvider1.user = user
//            serviceProvider1.website = 'travel360.com'
//            serviceProvider1.mobNo = '9717544305'
//            serviceProvider1.approved = 'true'
//            serviceProvider1.addToHotel(hotel1)
//            serviceProvider1.save(flush: true)
//            hotel1.save(flush: true)

            ////////////////////////////////////////////////////////////////////////////////////////////////////

//            Hotel hotel2 = new Hotel()
//
//            Address address2 = new Address()
//            address2.address1 = "Aashram Marg"
//            address2.address2 = "near jawahar circle"
//            address2.city = "Jaipur"
//            address2.zipCode = "022002"
//            address2.country = "India"
//            hotel2.name = "Marriott Hotel"
//            hotel2.summary = "Best Hotel experience"
//            hotel2.type = HotelType.BUSINESS_HOTELS
//            hotel2.save(flush: true)
//            address2.hotel = hotel2
//            address2.save(flush: true)
//
//            HotelRoom hotelRoom2 = new HotelRoom()
//            hotelRoom2.name = "Club-Room"
//            hotelRoom2.description = "Best Hotel room experience"
//            hotelRoom2.price = 10000
//            hotelRoom2.hotel = hotel2
//            hotel2.addToHotelRooms(hotelRoom2)
//            hotel2.save(flush: true)
//            hotelRoom2.save(flush: true)

//            Picture pictureRoom2 = new Picture()
//            pictureRoom2.filePath = "http://www.landmarklondon.co.uk/uploads/files/Deluxe-Room1.jpg"
//            pictureRoom2.contentType = "jpeg"
//            hotelRoom2.addToPictures(pictureRoom2)
//            hotelRoom2.save(flush: true)
//            pictureRoom2.save(flush: true)
//
//            Picture picture2 = new Picture()
//            picture2.filePath = "http://blessingsonthenet.com/img/uploads/hotels/aim_bn_1374048083.jpg"
//            picture2.contentType = "jpeg"
//            hotel2.addToPictures(picture2)
//            hotel2.save(flush: true)
//            picture2.save(flush: true)


//            Facility facility6 = new Facility()
//            facility6.name = "Parking"
//            facility6.facilityFor = FacilityFor.HOTEL
//            facility6.hotelId = hotel2.id
////        hotel1.addToHotelFacilities(facility6)
//            hotel1.save(flush: true)
//            facility6.save(flush: true)
//
//            serviceProvider1.addToHotel(hotel2)
//
//            serviceProvider1.save(flush: true)
//            hotel2.save(flush: true)
//        ServiceProvider serviceProvider1 = new ServiceProvider(nameOfHotel: 'newGenX', city: 'New Delhi', state: 'New Delhi', address: 'Paschim Vihar', propertyType: 'hotel', nameOfRepresentee: 'Travelex360', officialEmail: 'kumar.saurabh@travelx360.com', website: 'www.travelx360.com', mobNo: '9717544305', approved: 'true', hotel: hotel1)
//        serviceProvider1.save(flush: true, failOnError: true)

        }
    }

}
