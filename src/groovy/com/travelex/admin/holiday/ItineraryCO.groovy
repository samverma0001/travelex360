package com.travelex.admin.holiday

import com.travelex.admin.Itinerary
import grails.validation.Validateable


@Validateable
class ItineraryCO {

    String dayHeading
    String dayMeal
    String description
    String dayNumber


    static constraints = {

        dayHeading(nullable: false, blank: false)
        dayMeal(nullable: false, blank: false)
        description(nullable: false, blank: false)
        dayNumber(nullable: false, blank: false)

    }

    public static void saveItineraryForPackage(ItineraryCO itineraryCO, Itinerary itinerary) {

        itinerary.dayHeading = itineraryCO.dayHeading
        itinerary.dayMeal = itineraryCO.dayMeal
        itinerary.description = itineraryCO.description
        itinerary.dayNumber = itineraryCO.dayNumber
    }

    public static void updateItineraryForPackage(ItineraryCO itineraryCO, Itinerary itinerary) {

        itinerary.dayHeading = itineraryCO.dayHeading
        itinerary.dayMeal = itineraryCO.dayMeal
        itinerary.description = itineraryCO.description
        itinerary.dayNumber = itineraryCO.dayNumber
    }


}
