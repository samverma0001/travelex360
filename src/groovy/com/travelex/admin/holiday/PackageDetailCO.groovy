package com.travelex.admin.holiday

import com.travelex.admin.HolidayImage
import com.travelex.admin.HolidayPackage
import com.travelex.admin.PackageDetail
import grails.validation.Validateable
import org.springframework.web.multipart.commons.CommonsMultipartFile
import util.AppUtil

@Validateable
class PackageDetailCO {

    String overview
    String inclusion
    String exclusion
    String paymentPolicy
    String cancellationPolicy
    String termsCondition

    List<CommonsMultipartFile> holidayImages = []

    static constraints = {

        overview(nullable: false, blank: false)
        inclusion(nullable: false, blank: false)
        exclusion(nullable: false, blank: false)
        paymentPolicy(nullable: false, blank: false)
        cancellationPolicy(nullable: false, blank: false)
        termsCondition(nullable: false, blank: false)

    }

    public static void savePackageDetail(PackageDetailCO packageDetailCO, PackageDetail packageDetail) {
        packageDetail.overview = packageDetailCO.overview
        packageDetail.inclusion = packageDetailCO.inclusion
        packageDetail.exclusion = packageDetailCO.exclusion
        packageDetail.paymentPolicy = packageDetailCO.paymentPolicy
        packageDetail.cancellationPolicy = packageDetailCO.cancellationPolicy
        packageDetail.termsCondition = packageDetailCO.termsCondition
    }

    public static void updatePackageDetail(PackageDetailCO packageDetailCO, PackageDetail packageDetail) {
        packageDetail.overview = packageDetailCO.overview
        packageDetail.inclusion = packageDetailCO.inclusion
        packageDetail.exclusion = packageDetailCO.exclusion
        packageDetail.paymentPolicy = packageDetailCO.paymentPolicy
        packageDetail.cancellationPolicy = packageDetailCO.cancellationPolicy
        packageDetail.termsCondition = packageDetailCO.termsCondition
    }

}
