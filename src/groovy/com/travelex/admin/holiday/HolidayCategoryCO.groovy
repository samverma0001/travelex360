package com.travelex.admin.holiday

import com.travelex.admin.HolidayCategory
import com.travelex.resizeService.ImageService
import grails.validation.Validateable
import org.springframework.web.multipart.commons.CommonsMultipartFile
import util.AppUtil

@Validateable
class HolidayCategoryCO {

    ImageService imageService

    String categoryName
    CommonsMultipartFile categoryImage

    static constraints = {

        categoryName(nullable: false, blank: false)
        categoryImage(nullable: true, blank: true)

    }

    public static
    saveHolidayCategory(HolidayCategoryCO holidayCategoryCO, HolidayCategory holidayCategory) {
        holidayCategory.categoryName = holidayCategoryCO.categoryName

    }

    public static
    updateHolidayCategory(HolidayCategoryCO holidayCategoryCO, HolidayCategory holidayCategory) {
        holidayCategory.categoryName = holidayCategoryCO.categoryName

    }

}
