package com.travelex.admin.holiday

public enum HolidayStatus {

    ACTIVE("Active"),
    EXPIRED("Expired")


    final String value

    HolidayStatus(String value) {
        this.value = value
    }

    String toString() {
        return value
    }

    public String getKey() {
        return name()
    }

    public String getValue() {
        return value
    }

    public static List<HolidayStatus> list() {
        return [ACTIVE, EXPIRED]
    }
}