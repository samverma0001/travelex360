package com.travelex.admin.holiday

import com.travelex.admin.StayDetail
import com.travelex.common.Address
import grails.validation.Validateable

@Validateable
class StayDetailCO {

    String country
    String state
    String city
    String location
    String noOfNight

    static constraints = {
        country(nullable: false, blank: false)
        state(nullable: false, blank: false)
        city(nullable: false, blank: false)
        location(nullable: false, blank: false)
        noOfNight(nullable: false, blank: false)
    }

    public static void saveStayForPackage(StayDetailCO stayDetailCO, StayDetail stayDetail) {
        stayDetail.country = stayDetailCO.country
        stayDetail.state = stayDetailCO.state
        stayDetail.city = stayDetailCO.city
        stayDetail.location = stayDetailCO.location
        stayDetail.noOfNight = stayDetailCO.noOfNight
    }

    public static void updateStayForPackage(StayDetailCO stayDetailCO, StayDetail stayDetail) {
        stayDetail.country = stayDetailCO.country
        stayDetail.state = stayDetailCO.state
        stayDetail.city = stayDetailCO.city
        stayDetail.location = stayDetailCO.location
        stayDetail.noOfNight = stayDetailCO.noOfNight
    }


}
