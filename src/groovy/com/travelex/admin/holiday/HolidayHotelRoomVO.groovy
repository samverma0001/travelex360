package com.travelex.admin.holiday

import com.travelex.admin.HolidayHotel
import com.travelex.admin.HolidayHotelRoom
import grails.validation.Validateable

@Validateable
class HolidayHotelRoomVO {

    String roomCategory
    BigDecimal roomPrice
    String roomAmenities
    String bedType


    static constraints = {
        roomCategory(nullable: false, blank: false)
        roomPrice(nullable: false, blank: false)
        bedType(nullable: false)
        roomAmenities(nullable: true, blank: true)
    }

    public
    static void saveRoomForHolidayHotel(HolidayHotelRoomVO holidayHotelRoomVO, HolidayHotelRoom holidayHotelRoom) {
        holidayHotelRoom.roomPrice = holidayHotelRoomVO.roomPrice
        holidayHotelRoom.roomAmenities = holidayHotelRoomVO.roomAmenities
        holidayHotelRoom.roomCategory = holidayHotelRoomVO.roomCategory
        String roomBedType = holidayHotelRoomVO.bedType
        if (roomBedType.equalsIgnoreCase("SINGLESHARING")) {
            holidayHotelRoom.holidayHotelRoomBedType = HolidayHotelRoomBedType.SINGLESHARING
        } else if (roomBedType.equalsIgnoreCase("TWINSHARING")) {
            holidayHotelRoom.holidayHotelRoomBedType = HolidayHotelRoomBedType.TWINSHARING
        } else if (roomBedType.equalsIgnoreCase("TRIPLESHARING")) {
            holidayHotelRoom.holidayHotelRoomBedType = HolidayHotelRoomBedType.TRIPLESHARING
        } else if (roomBedType.equalsIgnoreCase("CHILDWITHBED")) {
            holidayHotelRoom.holidayHotelRoomBedType = HolidayHotelRoomBedType.CHILDWITHBED
        } else if (roomBedType.equalsIgnoreCase("CHILDWITHOUTBED")) {
            holidayHotelRoom.holidayHotelRoomBedType = HolidayHotelRoomBedType.CHILDWITHOUTBED
        }

    }

    public
    static void updateRoomForHolidayHotel(HolidayHotelRoomVO holidayHotelRoomVO, HolidayHotelRoom holidayHotelRoom) {
        holidayHotelRoom.roomPrice = holidayHotelRoomVO.roomPrice
        holidayHotelRoom.roomAmenities = holidayHotelRoomVO.roomAmenities
        holidayHotelRoom.roomCategory = holidayHotelRoomVO.roomCategory
        String roomBedType = holidayHotelRoomVO.bedType
        if (roomBedType.equalsIgnoreCase("SINGLESHARING")) {
            holidayHotelRoom.holidayHotelRoomBedType = HolidayHotelRoomBedType.SINGLESHARING
        } else if (roomBedType.equalsIgnoreCase("TWINSHARING")) {
            holidayHotelRoom.holidayHotelRoomBedType = HolidayHotelRoomBedType.TWINSHARING
        } else if (roomBedType.equalsIgnoreCase("TRIPLESHARING")) {
            holidayHotelRoom.holidayHotelRoomBedType = HolidayHotelRoomBedType.TRIPLESHARING
        } else if (roomBedType.equalsIgnoreCase("CHILDWITHBED")) {
            holidayHotelRoom.holidayHotelRoomBedType = HolidayHotelRoomBedType.CHILDWITHBED
        } else if (roomBedType.equalsIgnoreCase("CHILDWITHOUTBED")) {
            holidayHotelRoom.holidayHotelRoomBedType = HolidayHotelRoomBedType.CHILDWITHOUTBED
        }

    }


}
