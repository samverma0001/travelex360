package com.travelex.admin.holiday

import com.travelex.admin.HolidayCategory
import com.travelex.admin.HolidayPackage
import com.travelex.resizeService.ImageService
import grails.validation.Validateable
import org.springframework.web.multipart.commons.CommonsMultipartFile
import util.AppUtil

@Validateable
class HolidayPackageCO {

    String holidayName
    CommonsMultipartFile holidayImageName
    String priceRange
    String totalSeat
    Boolean visaIncluded
    Boolean visaOnArrival
    Boolean travelInsurance
    String startDate
    String endDate
    String status
    String type
    String categoryId

    static constraints = {

        holidayName(nullable: false, blank: false)
        holidayImageName(nullable: true, blank: true)
        priceRange(nullable: false, blank: false)
        totalSeat(nullable: false, blank: false)
        visaIncluded(nullable: true)
        visaOnArrival(nullable: true)
        travelInsurance(nullable: true)
        startDate(nullable: false, blank: false)
        endDate(nullable: false, blank: false)
        status(nullable: false, blank: false)
        type(nullable: false, blank: false)


    }

    public static void saveHolidayPackage(HolidayPackageCO holidayPackageCO, HolidayPackage holidayPackage) {
        holidayPackage.holidayName = holidayPackageCO.holidayName
        holidayPackage.startDate = AppUtil.convertDateFromStringToDate(holidayPackageCO.startDate)
        holidayPackage.endDate = AppUtil.convertDateFromStringToDate(holidayPackageCO.endDate)
        String status = holidayPackageCO.status
        if (status.equalsIgnoreCase('ACTIVE')) {
            holidayPackage.holidayStatus = HolidayStatus.ACTIVE
        } else {
            holidayPackage.holidayStatus = HolidayStatus.EXPIRED
        }
        String type = holidayPackageCO.type
        if (type.equalsIgnoreCase('DOMESTIC')) {
            holidayPackage.holidayType = HolidayType.DOMESTIC
        } else {
            holidayPackage.holidayType = HolidayType.INTERNATIONAL
        }
        holidayPackage.priceRange = holidayPackageCO.priceRange.toBigDecimal()
        holidayPackage.totalSeat = holidayPackageCO.totalSeat
        if (holidayPackageCO.travelInsurance) {
            holidayPackage.travelInsurance = true
        } else {
            holidayPackage.travelInsurance = false
        }
        if (holidayPackageCO.visaOnArrival) {
            holidayPackage.visaOnArrival = true
        } else {
            holidayPackage.visaOnArrival = false
        }
        if (holidayPackageCO.visaIncluded) {
            holidayPackage.visaIncluded = true
        } else {
            holidayPackage.visaIncluded = false
        }
    }

    public static void updateHolidayPackage(HolidayPackageCO holidayPackageCO, HolidayPackage holidayPackage) {
        holidayPackage.holidayName = holidayPackageCO.holidayName
        holidayPackage.startDate = AppUtil.convertDateFromStringToDate(holidayPackageCO.startDate)
        holidayPackage.endDate = AppUtil.convertDateFromStringToDate(holidayPackageCO.endDate)
        String status = holidayPackageCO.status
        if (status.equalsIgnoreCase('ACTIVE')) {
            holidayPackage.holidayStatus = HolidayStatus.ACTIVE
        } else {
            holidayPackage.holidayStatus = HolidayStatus.EXPIRED
        }
        String type = holidayPackageCO.type
        if (type.equalsIgnoreCase('DOMESTIC')) {
            holidayPackage.holidayType = HolidayType.DOMESTIC
        } else {
            holidayPackage.holidayType = HolidayType.INTERNATIONAL
        }
        holidayPackage.priceRange = holidayPackageCO.priceRange.toBigDecimal()
        holidayPackage.totalSeat = holidayPackageCO.totalSeat
        if (holidayPackageCO.travelInsurance) {
            holidayPackage.travelInsurance = true
        } else {
            holidayPackage.travelInsurance = false
        }
        if (holidayPackageCO.visaOnArrival) {
            holidayPackage.visaOnArrival = true
        } else {
            holidayPackage.visaOnArrival = false
        }
        if (holidayPackageCO.visaIncluded) {
            holidayPackage.visaIncluded = true
        } else {
            holidayPackage.visaIncluded = false
        }
    }

}
