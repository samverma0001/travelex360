package com.travelex.admin.holiday

public enum HolidayHotelRoomBedType {

    SINGLESHARING("Single sharing"),
    TWINSHARING("Twin sharing"),
    TRIPLESHARING("Triple sharing"),
    CHILDWITHBED("Child with bed"),
    CHILDWITHOUTBED("Child without bed")

    final String value

    HolidayHotelRoomBedType(String value) {
        this.value = value
    }

    String toString() {
        return value
    }

    public String getKey() {
        return name()
    }

    public String getValue() {
        return value
    }

    public static List<HolidayHotelRoomBedType> list() {
        return [SINGLESHARING, TWINSHARING, TRIPLESHARING, CHILDWITHBED, CHILDWITHOUTBED]
    }


}