package com.travelex.admin.holiday

public enum HolidayType {

    DOMESTIC("Domestic"),
    INTERNATIONAL("International")


    final String value

    HolidayType(
            String value
    ) {
        this.value = value
    }

    String toString() {
        return value
    }

    public String getKey() {
        return name()
    }

    public String getValue() {
        return value
    }

    public static List<HolidayType> list() {
        return [DOMESTIC, INTERNATIONAL]
    }


}