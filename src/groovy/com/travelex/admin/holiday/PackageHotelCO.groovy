package com.travelex.admin.holiday

import com.travelex.admin.HolidayHotel
import com.travelex.admin.PackageHotelType
import com.travelex.socials.Enums.HotelType
import grails.validation.Validateable
import org.springframework.web.multipart.commons.CommonsMultipartFile

@Validateable
class PackageHotelCO {

    String hotelName
    CommonsMultipartFile mainImageName
    String packageHotelType
    String type
    String overview
    String facilities
    static constraints = {

        hotelName(nullable: false, blank: false)
        mainImageName(nullable: true, blank: true)
        type(nullable: false)
        packageHotelType(nullable: false)
        overview(nullable: false, blank: false)
        facilities(nullable: false, blank: false)

    }

    public static saveHotelForPackage(PackageHotelCO packageHotelCO, HolidayHotel holidayHotel) {
        holidayHotel.hotelName = packageHotelCO.hotelName
        holidayHotel.overview = packageHotelCO.overview
        String packageHotelType = packageHotelCO.packageHotelType
        if (packageHotelType.equals('ONESTAR')) {
            holidayHotel.packageHotelType = PackageHotelType.ONESTAR
        } else if (packageHotelType.equals('TWOSTAR')) {
            holidayHotel.packageHotelType = PackageHotelType.TWOSTAR
        } else if (packageHotelType.equals('THREESTAR')) {
            holidayHotel.packageHotelType = PackageHotelType.THREESTAR
        } else if (packageHotelType.equals('FOURSTAR')) {
            holidayHotel.packageHotelType = PackageHotelType.FOURSTAR
        } else if (packageHotelType.equals('FIVESTAR')) {
            holidayHotel.packageHotelType = PackageHotelType.FIVESTAR
        }
        String type = packageHotelCO.type
        if (type.equals("RESORTS_AND_PLACES")) {
            holidayHotel.type = HotelType.RESORTS_AND_PLACES
        } else if (type.equals("HERITAGES_HOTELS")) {
            holidayHotel.type = HotelType.HERITAGES_HOTELS
        } else if (type.equals("HOLIDAY_COTTAGE")) {
            holidayHotel.type = HotelType.HOLIDAY_COTTAGE
        } else if (type.equals("BUSINESS_HOTELS")) {
            holidayHotel.type = HotelType.BUSINESS_HOTELS
        } else if (type.equals("BED_AND_BREAKFAST")) {
            holidayHotel.type = HotelType.BED_AND_BREAKFAST
        } else if (type.equals("MOTELS")) {
            holidayHotel.type = HotelType.MOTELS
        }
        holidayHotel.facilities = packageHotelCO.facilities
    }

    public static updateHotelForPackage(PackageHotelCO packageHotelCO, HolidayHotel holidayHotel) {
        holidayHotel.hotelName = packageHotelCO.hotelName
        holidayHotel.overview = packageHotelCO.overview
        String packageHotelType = packageHotelCO.packageHotelType
        if (packageHotelType.equals('ONESTAR')) {
            holidayHotel.packageHotelType = PackageHotelType.ONESTAR
        } else if (packageHotelType.equals('TWOSTAR')) {
            holidayHotel.packageHotelType = PackageHotelType.TWOSTAR
        } else if (packageHotelType.equals('THREESTAR')) {
            holidayHotel.packageHotelType = PackageHotelType.THREESTAR
        } else if (packageHotelType.equals('FOURSTAR')) {
            holidayHotel.packageHotelType = PackageHotelType.FOURSTAR
        } else if (packageHotelType.equals('FIVESTAR')) {
            holidayHotel.packageHotelType = PackageHotelType.FIVESTAR
        }
        String type = packageHotelCO.type
        if (type.equals("RESORTS_AND_PLACES")) {
            holidayHotel.type = HotelType.RESORTS_AND_PLACES
        } else if (type.equals("HERITAGES_HOTELS")) {
            holidayHotel.type = HotelType.HERITAGES_HOTELS
        } else if (type.equals("HOLIDAY_COTTAGE")) {
            holidayHotel.type = HotelType.HOLIDAY_COTTAGE
        } else if (type.equals("BUSINESS_HOTELS")) {
            holidayHotel.type = HotelType.BUSINESS_HOTELS
        } else if (type.equals("BED_AND_BREAKFAST")) {
            holidayHotel.type = HotelType.BED_AND_BREAKFAST
        } else if (type.equals("MOTELS")) {
            holidayHotel.type = HotelType.MOTELS
        }
        holidayHotel.facilities = packageHotelCO.facilities
    }


}
