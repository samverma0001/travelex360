package com.travelex.admin.coupon

import grails.validation.Validateable

@Validateable
class CouponVO {

    String amount
    String couponCode
    String validNoOfDays


    static constraints = {
        amount(nullable: false, blank: false)
        couponCode(nullable: false, blank: false)
        validNoOfDays(nullable: false, blank: false)

    }


}
