package com.travelex.admin

public enum Status {

    ACTIVE("Active"),
    EXPIRED("Expired")


    final String value

    Status(String value) {
        this.value = value
    }

    String toString() {
        return value
    }

    public String getKey() {
        return name()
    }

    public String getValue() {
        return value
    }

    public static List<Status> list() {

        return [ACTIVE, EXPIRED]
    }
}