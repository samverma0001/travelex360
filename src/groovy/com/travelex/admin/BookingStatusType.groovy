package com.travelex.admin

public enum BookingStatusType {

    Booked("Booked"),
    Cancelled("Cancelled"),
    Vouchered("Vouchered")


    final String value

    BookingStatusType(String value) {
        this.value = value
    }

    String toString() {
        return value
    }

    public String getKey() {
        return name()
    }

    public String getValue() {
        return value
    }

    public static List<BookingStatusType> list() {
        return [Booked, Cancelled, Vouchered]
    }


}