package com.travelex.admin

public enum MarginType {

    DOMHOTEL("DomesticHotel"),
    INTERHOTEL("InterNationalHotel")


    final String value

    MarginType(String value) {
        this.value = value
    }

    String toString() {
        return value
    }

    public String getKey() {
        return name()
    }

    public String getValue() {
        return value
    }

    public static List<MarginType> list() {

        return [DOMHOTEL, INTERHOTEL]
    }




}