package com.travelex.admin

public enum PackageHotelType {
    ONESTAR("One Star"),
    TWOSTAR("Two Star"),
    THREESTAR("Three Star"),
    FOURSTAR("Four Star"),
    FIVESTAR("Five Star")


    final String value

    PackageHotelType(
            String value
    ) {
        this.value = value
    }

    String toString() {
        return value
    }

    public String getKey() {
        return name()
    }

    public String getValue() {
        return value
    }

    public static List<PackageHotelType> list() {
        return [ONESTAR, TWOSTAR, THREESTAR, FOURSTAR, FIVESTAR]
    }
}