package com.travelex.admin.margin

import com.travelex.admin.AmountType
import com.travelex.admin.MarginType
import com.travelex.admin.SetMargin
import com.travelex.admin.Status
import grails.validation.Validateable

import java.text.SimpleDateFormat

@Validateable
class DomHotelMarginVO {

    String marginMoney
    String validFrom
    String validTo
    String amountType
    String status
    String marginType
    String payUMoneyStake


    static constraints = {

        marginMoney(nullable: false, blank: false)
        validFrom(nullable: false, blank: false)
        validTo(nullable: false, blank: false)
        payUMoneyStake(nullable: false, blank: false)

    }


    public static saveDomHotelMargin(SetMargin setMargin, DomHotelMarginVO domHotelMarginVO) {
        setMargin.marginMoney = domHotelMarginVO.marginMoney
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat('dd/MM/yyyy')
        setMargin.validFrom = simpleDateFormat.parse(domHotelMarginVO.validFrom)
        setMargin.validTo = simpleDateFormat.parse(domHotelMarginVO.validTo)
        String amountType = setMargin.amountType
        if (amountType.equals('')) {
            setMargin.amountType = AmountType.AMOUNT
        } else {
            setMargin.amountType = AmountType.PERCENT
        }
        String status = domHotelMarginVO.status
        if (status.equals("Active")) {
            domHotelMarginVO.status = Status.ACTIVE
        } else {
            domHotelMarginVO.status = Status.EXPIRED
        }
        String mgnType = domHotelMarginVO.marginType
        if (mgnType.equalsIgnoreCase("DOMHOTEL")) {
            setMargin.marginType = MarginType.DOMHOTEL
        } else if (mgnType.equalsIgnoreCase("INTERHOTEL")) {
            setMargin.marginType = MarginType.INTERHOTEL
        }
        setMargin.payUMoneyStake = domHotelMarginVO.payUMoneyStake
    }

    public static updateDomHotelMargin(SetMargin setMargin, DomHotelMarginVO domHotelMarginVO) {
        setMargin.marginMoney = domHotelMarginVO.marginMoney
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat('dd/MM/yyyy')
        setMargin.validFrom = simpleDateFormat.parse(domHotelMarginVO.validFrom)
        setMargin.validTo = simpleDateFormat.parse(domHotelMarginVO.validTo)
        String amountType = setMargin.amountType
        if (amountType.equals('PERCENT')) {
            setMargin.amountType = AmountType.AMOUNT
        } else {
            setMargin.amountType = AmountType.PERCENT
        }
        String status = domHotelMarginVO.status
        if (status.equals("ACTIVE")) {
            domHotelMarginVO.status = Status.ACTIVE
        } else {
            domHotelMarginVO.status = Status.EXPIRED
        }
        String mgnType = domHotelMarginVO.marginType
        if (mgnType.equalsIgnoreCase("DOMHOTEL")) {
            setMargin.marginType = MarginType.DOMHOTEL
        } else if (mgnType.equalsIgnoreCase("INTERHOTEL")) {
            setMargin.marginType = MarginType.INTERHOTEL
        }
        setMargin.payUMoneyStake = domHotelMarginVO.payUMoneyStake
    }


}
