package com.travelex.admin.margin

public enum BookingType {

    HOTEL("Hotel"),
    FLIGHT("Flight"),
    BUS("Bus")


    final String value

    BookingType(
            String value
    ) {
        this.value = value
    }

    String toString() {
        return value
    }

    public String getKey() {
        return name()
    }

    public String getValue() {
        return value
    }

    public static List<BookingType> list() {

        return [HOTEL, FLIGHT, BUS]
    }


}