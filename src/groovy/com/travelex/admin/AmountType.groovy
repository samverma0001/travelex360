package com.travelex.admin

public enum AmountType {

    PERCENT("Percent"),
    AMOUNT("Amount")


    final String value

    AmountType(String value) {
        this.value = value
    }

    String toString() {
        return value
    }

    public String getKey() {
        return name()
    }

    public String getValue() {
        return value
    }

    public static List<AmountType> list() {

        return [PERCENT, AMOUNT]
    }
}