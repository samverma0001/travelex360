package com.travelex.admin

import grails.validation.Validateable
import util.AppUtil

@Validateable
class FlightCO {

    String flightName
    String flightNumber
    String departureFrom
    String destinationTo
    String departureDate
    String destinationDate
    String departureTime
    String destinationTime

    static constraints = {
        flightName(nullable: false, blank: false)
        flightNumber(nullable: false, blank: false)
        departureFrom(nullable: false, blank: false)
        destinationTo(nullable: false, blank: false)
        departureDate(nullable: false, blank: false)
        destinationDate(nullable: false, blank: false)
        departureTime(nullable: false, blank: false)
        destinationTime(nullable: false, blank: false)
    }

    public static void saveFlightForPackage(FlightCO flightCO, Flight flight) {
        flight.flightName = flightCO.flightName
        flight.flightNumber = flightCO.flightNumber
        flight.departureFrom = flightCO.departureFrom
        flight.destinationTo = flightCO.destinationTo
        flight.departureTime = flightCO.departureTime
        flight.destinationTime = flightCO.destinationTime
        Date departure = AppUtil.convertDateFromStringToDate(flightCO.departureDate)
        flight.departureDate = departure
        Date destination = AppUtil.convertDateFromStringToDate(flightCO.departureDate)
        flight.destinationDate = destination
        flight.save(flush: true, failOnError: true)
    }

    public static void updateFlightForPackage(FlightCO flightCO, Flight flight) {
        flight.flightName = flightCO.flightName
        flight.flightNumber = flightCO.flightNumber
        flight.departureFrom = flightCO.departureFrom
        flight.destinationTo = flightCO.destinationTo
        flight.departureTime = flightCO.departureTime
        flight.destinationTime = flightCO.destinationTime
        Date departure = AppUtil.convertDateFromStringToDate(flightCO.departureDate)
        flight.departureDate = departure
        Date destination = AppUtil.convertDateFromStringToDate(flightCO.departureDate)
        flight.destinationDate = destination
        flight.save(flush: true, failOnError: true)
    }


}
