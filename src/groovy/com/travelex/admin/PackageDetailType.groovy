package com.travelex.admin

public enum PackageDetailType {

    public enum PackageHotelType {
        DETAILS("Package Details"),
        ABOUTPlACE("About the place"),
        FLIGHT("Flight"),
        ITINERARY("Itinerary"),
        HOTEL("Hotel"),


        final String value

        PackageHotelType(
                String value
        ) {
            this.value = value
        }

        String toString() {
            return value
        }

        public String getKey() {
            return name()
        }

        public String getValue() {
            return value
        }

        public static List<PackageHotelType> list() {
            return [DETAILS, ABOUTPlACE, FLIGHT, ITINERARY, HOTEL]
        }
    }
}