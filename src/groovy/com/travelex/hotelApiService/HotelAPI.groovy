package com.travelex.hotelApiService

import com.travelex.hotelFunctionality.HotelBookingVO
import com.travelex.hotelFunctionality.HotelDetailVO
import com.travelex.hotelFunctionality.HotelSearchParamsVO
import com.travelex.hotelFunctionality.TboHotelBookingVO
import com.travelex.socials.hotel.HotelVO
import com.travelex.socials.searchHotel.SearchHotelVO

public interface HotelAPI {

    List<SearchHotelVO> search(HotelSearchParamsVO hotelSearchParamsVO);

    HotelVO details(HotelDetailVO hotelDetailVO);

    def book(TboHotelBookingVO hotelBookingVO);

    Boolean isEnabled

    ServiceType serviceType = ServiceType.TRAVELEX


}