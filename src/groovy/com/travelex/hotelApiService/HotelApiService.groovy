package com.travelex.hotelApiService

import com.travelex.hotelFunctionality.HotelBookingVO
import com.travelex.hotelFunctionality.HotelDetailVO
import com.travelex.hotelFunctionality.HotelSearchParamsVO
import com.travelex.hotelFunctionality.TboHotelBookingVO
import com.travelex.socials.hotel.HotelVO
import com.travelex.socials.searchHotel.SearchHotelVO
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsHttpSession
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsWebRequest
import org.springframework.web.context.request.RequestContextHolder
import groovy.util.logging.*

@Log4j
class HotelApiService implements HotelAPI {


    List<HotelAPI> services = [
            new TboHotelAPI(),
//            new MghHotelAPI(),
//            new TravelxHotelApi()
    ]

    List<SearchHotelVO> search(HotelSearchParamsVO hotelSearchParamsVO) {
        def offset = 0
        def max = offset + 10
        List<SearchHotelVO> hotelSearchParamsVOList = []
        List<SearchHotelVO> hotelSearchParamsVOList1 = []
        List<SearchHotelVO> hotelSearchParamsVOList2 = []
        services.each { api ->
            if (api) {

                if (api.isEnabled) {
                    log.info '============Inside search service=========='
                    hotelSearchParamsVOList1 = api.search(hotelSearchParamsVO)
                }

                hotelSearchParamsVOList.addAll(hotelSearchParamsVOList1)
            }
        }
        GrailsWebRequest request = RequestContextHolder?.currentRequestAttributes()
        GrailsHttpSession session = request.session
        session["hotelList"] = hotelSearchParamsVOList
        log.info '============hotelSearchList==========' + hotelSearchParamsVOList
//        println '======hotelSearchList=====' + hotelSearchParamsVOList
        return hotelSearchParamsVOList
//        return hotelSearchParamsVOList.subList(offset, max > hotelSearchParamsVOList.size() ? hotelSearchParamsVOList.size() : max)
    }

    HotelVO details(HotelDetailVO hotelDetailVO) {
        HotelVO hotelVO = new HotelVO()

        services.each { api ->
            if (api) {

                if (api.isEnabled) {
                    if (hotelDetailVO.sessionId) {
                        if (api.serviceType == ServiceType.TBO) {
                            hotelVO = api.details(hotelDetailVO)
                        }
                    } else {
                        if (api.serviceType == ServiceType.MGH) {
                            hotelVO = api.details(hotelDetailVO)
                        }
                    }
                }
                log.info '============Inside details service=========='
            }
        }
        return hotelVO
    }


    def book(TboHotelBookingVO hotelBookingVO) {
        String bookingIdForService = null
        services.each { api ->
            if (api) {

                if (api.isEnabled) {
                    if (hotelBookingVO.sessionId) {
                        if (api.serviceType == ServiceType.TBO) {
                            bookingIdForService = api.book(hotelBookingVO)
                        }
                    } else if (api.serviceType == ServiceType.MGH) {

                        bookingIdForService = api.book(hotelBookingVO)
                    }
                }
            }
            log.info '============Inside details service=========='
        }
        return bookingIdForService
    }

}


