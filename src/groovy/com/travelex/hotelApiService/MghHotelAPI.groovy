package com.travelex.hotelApiService

import com.travelex.Hotel.City
import com.travelex.hotelFunctionality.HotelBookingVO
import com.travelex.hotelFunctionality.HotelDetailVO
import com.travelex.hotelFunctionality.HotelSearchParamsVO
import com.travelex.hotelFunctionality.TboHotelBookingVO
import com.travelex.socials.hotel.ContactVO
import com.travelex.socials.hotel.GeoDataVO
import com.travelex.socials.hotel.HotelFacilityVO
import com.travelex.socials.hotel.HotelMediaVO
import com.travelex.socials.hotel.HotelPromotionsVO
import com.travelex.socials.hotel.HotelRoomRatesVO
import com.travelex.socials.hotel.HotelVO
import com.travelex.socials.hotel.MasterInclusionsVO
import com.travelex.socials.hotel.RoomCategoriesNameVO
import com.travelex.socials.hotel.RoomFacilityVO
import com.travelex.socials.hotel.RoomMediaVO
import com.travelex.socials.hotel.RoomTypeVO
import com.travelex.socials.searchHotel.SearchHotelFacilitiesVO
import com.travelex.socials.searchHotel.SearchHotelMealPlanVO
import com.travelex.socials.searchHotel.SearchHotelMediaListVO
import com.travelex.socials.searchHotel.SearchHotelRefundPolicyVO
import com.travelex.socials.searchHotel.SearchHotelRoomCategoriesVO
import com.travelex.socials.searchHotel.SearchHotelRoomTypeVO
import com.travelex.socials.searchHotel.SearchHotelVO
import com.travelex.socials.searchHotel.SearchRoomRateTypeVO
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsHttpSession
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsWebRequest
import org.springframework.web.context.request.RequestContextHolder

import java.text.ParseException
import java.text.SimpleDateFormat

import groovy.util.logging.*

@Log4j
class MghHotelAPI implements HotelAPI {
    public static final String TOKEN = "00c3d12bf007d78c9edb2897b8c909cb"
    public static final String MGHWORLD_BASE_URL = "http://www.demo.mghworld.net/API/v1/xml"

    List<SearchHotelVO> search(HotelSearchParamsVO searchParams) {


        Long rangeMin = 0
        Long rangeMax = 0
//        String checkInDate=0
        String budget = searchParams.range

        if (budget == "1") {
            rangeMin = 0
            rangeMax = 0
        } else if (budget == "2") {
            rangeMin = 100
            rangeMax = 1000
        } else if (budget == "3") {
            rangeMin = 1000
            rangeMax = 4000
        } else if (budget == "4") {
            rangeMin = 4000
            rangeMax = 6000
        } else if (budget == "5") {
            rangeMin = 6000
            rangeMax = 8000
        } else if (budget == "6") {
            rangeMin = 8000
            rangeMax = 10000
        } else if (budget == "7") {
            rangeMin = 10000
            rangeMax = 12000
        } else if (budget == "8") {
            rangeMin = 12000
            rangeMax = 30000
        } else if (budget == "9") {
            rangeMin = 30000
            rangeMax = 70000
        } else if (budget == "10") {
            rangeMin = 70000
            rangeMax = 120000
        }

        City city1 = City.findByNameIlike(searchParams?.place)
        def locId = city1?.locationId
        println '================After Location Search======Location Id Is==============' + locId
//        String checkInDate=null
        String checkIn = searchParams.checkIn
        String checkOut = searchParams.checkOut

        SimpleDateFormat fromUser = new SimpleDateFormat("mm/dd/yyyy");
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-mm-dd")
        SimpleDateFormat fromUser2 = new SimpleDateFormat("mm/dd/yyyy");
        SimpleDateFormat myFormat2 = new SimpleDateFormat("yyyy-mm-dd")
        try {

//            String checkInDate = searchParams?.checkIn
            String checkInDate = myFormat.format(fromUser.parse(checkIn))
            String checkOutDate = myFormat2.format(fromUser2.parse(checkOut))
            def searchUrl = null
            if (searchParams?.noOfRooms == "2") {
                searchUrl = "${MGHWORLD_BASE_URL}/search&rooms=${searchParams?.noOfRooms}&token=${TOKEN}&location_id=${locId}&locality_id=&facility=1&checkin_date=${checkInDate}&checkout_date=${checkOutDate}&occupant_adult=${searchParams?.adult},${searchParams?.peopleInRoomTwo}&occupant_child=${searchParams?.children ? (searchParams?.children == "0" ? "0" : searchParams?.children + "-" + (searchParams.firstChildAgeRoomOne + (searchParams.secondChildAgeRoomOne ? ";" + searchParams.secondChildAgeRoomOne : "") + (searchParams.thirdChildAgeRoomOne ? ";" + searchParams.thirdChildAgeRoomOne : "") + (searchParams.fourthChildAgeRoomOne ? ";" + searchParams.fourthChildAgeRoomOne : ""))) : "0"},${searchParams?.childInRoomTwo ? (searchParams?.childInRoomTwo == "0" ? "0" : searchParams?.childInRoomTwo + "-" + (searchParams.firstChildAgeRoomTwo + (searchParams.secondChildAgeRoomTwo ? ";" + searchParams.secondChildAgeRoomTwo : "") + (searchParams.thirdChildAgeRoomTwo ? ";" + searchParams.thirdChildAgeRoomTwo : "") + (searchParams.fourthChildAgeRoomTwo ? ";" + searchParams.fourthChildAgeRoomTwo : ""))) : "0"}&sendMultiRate=1"
            } else if (searchParams?.noOfRooms == "3") {
                searchUrl = "${MGHWORLD_BASE_URL}/search&rooms=${searchParams?.noOfRooms}&token=${TOKEN}&location_id=${locId}&locality_id=&facility=1&checkin_date=${checkInDate}&checkout_date=${checkOutDate}&occupant_adult=${searchParams?.adult},${searchParams?.peopleInRoomTwo},${searchParams?.peopleInRoomThree}&occupant_child=${searchParams?.children ? (searchParams?.children == "0" ? "0" : searchParams?.children + "-" + (searchParams.firstChildAgeRoomOne + (searchParams.secondChildAgeRoomOne ? ";" + searchParams.secondChildAgeRoomOne : "") + (searchParams.thirdChildAgeRoomOne ? ";" + searchParams.thirdChildAgeRoomOne : "") + (searchParams.fourthChildAgeRoomOne ? ";" + searchParams.fourthChildAgeRoomOne : ""))) : "0"},${searchParams?.childInRoomTwo ? (searchParams?.childInRoomTwo == "0" ? "0" : searchParams?.childInRoomTwo + "-" + (searchParams.firstChildAgeRoomTwo + (searchParams.secondChildAgeRoomTwo ? ";" + searchParams.secondChildAgeRoomTwo : "") + (searchParams.thirdChildAgeRoomTwo ? ";" + searchParams.thirdChildAgeRoomTwo : "") + (searchParams.fourthChildAgeRoomTwo ? ";" + searchParams.fourthChildAgeRoomTwo : ""))) : "0"},${searchParams?.childInRoomThree ? (searchParams?.childInRoomThree == "0" ? "0" : searchParams?.childInRoomThree + "-" + (searchParams.firstChildAgeRoomTwo + (searchParams.secondChildAgeRoomThree ? ";" + searchParams.secondChildAgeRoomThree : "") + (searchParams.thirdChildAgeRoomThree ? ";" + searchParams.thirdChildAgeRoomThree : "") + (searchParams.fourthChildAgeRoomThree ? ";" + searchParams.fourthChildAgeRoomThree : ""))) : "0"}&sendMultiRate=1"
            } else if (searchParams?.noOfRooms == "4") {
                searchUrl = "${MGHWORLD_BASE_URL}/search&rooms=${searchParams?.noOfRooms}&token=${TOKEN}&location_id=${locId}&locality_id=&facility=1&checkin_date=${checkInDate}&checkout_date=${checkOutDate}&occupant_adult=${searchParams?.adult},${searchParams?.peopleInRoomTwo},${searchParams?.peopleInRoomThree},${searchParams?.peopleInRoomFour}&occupant_child=${searchParams?.children ? (searchParams?.children == "0" ? "0" : searchParams?.children + "-" + (searchParams.firstChildAgeRoomOne + (searchParams.secondChildAgeRoomOne ? ";" + searchParams.secondChildAgeRoomOne : "") + (searchParams.thirdChildAgeRoomOne ? ";" + searchParams.thirdChildAgeRoomOne : "") + (searchParams.fourthChildAgeRoomOne ? ";" + searchParams.fourthChildAgeRoomOne : ""))) : "0"},${searchParams?.childInRoomTwo ? (searchParams?.childInRoomTwo == "0" ? "0" : searchParams?.childInRoomTwo + "-" + (searchParams.firstChildAgeRoomTwo + (searchParams.secondChildAgeRoomTwo ? ";" + searchParams.secondChildAgeRoomTwo : "") + (searchParams.thirdChildAgeRoomTwo ? ";" + searchParams.thirdChildAgeRoomTwo : "") + (searchParams.fourthChildAgeRoomTwo ? ";" + searchParams.fourthChildAgeRoomTwo : "0"))) : "0"},${searchParams?.childInRoomThree ? (searchParams?.childInRoomThree == "0" ? "0" : searchParams?.childInRoomThree + "-" + (searchParams.firstChildAgeRoomThree + (searchParams.secondChildAgeRoomThree ? ";" + searchParams.secondChildAgeRoomThree : "") + (searchParams.thirdChildAgeRoomThree ? ";" + searchParams.thirdChildAgeRoomThree : "") + (searchParams.fourthChildAgeRoomThree ? ";" + searchParams.fourthChildAgeRoomThree : "0"))) : "0"},${searchParams?.childInRoomFour ? (searchParams?.childInRoomFour == "0" ? "0" : searchParams?.childInRoomFour + "-" + (searchParams.firstChildAgeRoomFour + (searchParams.secondChildAgeRoomFour ? ";" + searchParams.secondChildAgeRoomFour : "") + (searchParams.thirdChildAgeRoomFour ? ";" + searchParams.thirdChildAgeRoomFour : "") + (searchParams.fourthChildAgeRoomFour ? ";" + searchParams.fourthChildAgeRoomFour : ""))) : "0"}&sendMultiRate=1"
            } else {
//            def searchUrl = "${MGHWORLD_BASE_URL}/search&rooms=${searchParams?.noOfRooms}&token=${TOKEN}&location_id=${locId}&locality_id=&facility=1&checkin_date=${checkInDate}&checkout_date=${checkOutDate}&occupant_adult=${searchParams?.adult}&occupant_child=${searchParams?.children ? (searchParams?.children == "0" ? "" : searchParams?.children) : ""}&price_min=${rangeMin ? (rangeMin == "0" ? "" : rangeMin) : ""}&price_max=${rangeMax ? (rangeMax == "0" ? "" : rangeMax) : ""}"
                searchUrl = "${MGHWORLD_BASE_URL}/search&rooms=${searchParams?.noOfRooms}&token=${TOKEN}&location_id=${locId}&locality_id=&facility=1&checkin_date=${checkInDate}&checkout_date=${checkOutDate}&occupant_adult=${searchParams?.adult}&occupant_child=${searchParams?.children ? (searchParams?.children == "0" ? "0" : searchParams?.children + "-" + (searchParams.firstChildAgeRoomOne + (searchParams.secondChildAgeRoomOne ? ";" + searchParams.secondChildAgeRoomOne : "") + (searchParams.thirdChildAgeRoomOne ? ";" + searchParams.thirdChildAgeRoomOne : "") + (searchParams.fourthChildAgeRoomOne ? ";" + searchParams.fourthChildAgeRoomOne : ""))) : "0"}&sendMultiRate=1"
            }
            println '---------------------Search URL----------------' + searchUrl

            GrailsWebRequest request = RequestContextHolder?.currentRequestAttributes()
            GrailsHttpSession session = request.session
            session["searchUrl"] = searchUrl

            def searchData = new XmlParser().parse(searchUrl)

            println '===========searchData===========' + searchData
//
            List<SearchHotelVO> hotelList = []
            println '================In Search Hotel inner===================='
            searchData.children().hotel.each { hotel ->

                List<MasterInclusionsVO> hotelInclusionList = []
                List<HotelPromotionsVO> hotelPromotionList = []
                List<SearchHotelMediaListVO> hotelMediaList = []
                List<SearchHotelRoomCategoriesVO> hotelRoomCategoryList = []
                List<SearchHotelFacilitiesVO> hotelFacilityList = []
                List<SearchHotelRoomTypeVO> searchHotelRoomTypeList = []
                HotelRoomRatesVO hotelRoomRatesVOs
                List<HotelRoomRatesVO> hotelRoomRatesVOs2 = []

                println '================In Search HotelList inner===================='

                println '============' + hotel.attribute("name")
                SearchHotelVO searchHotelVO = new SearchHotelVO(hotel)

                hotel.masterInclusions.inclusion.each { inclusions ->
                    MasterInclusionsVO masterInclusionsVO = new MasterInclusionsVO(inclusions)
                    hotelInclusionList.add(masterInclusionsVO)
                }


                hotel.mediaList.media.each { media ->
                    println '========media========' + media.attribute("src")
                    SearchHotelMediaListVO searchHotelMediaListVO = new SearchHotelMediaListVO(media)
                    hotelMediaList.add(searchHotelMediaListVO)
                }

                hotel.roomCategories.roomCategory.each { category ->
                    SearchHotelRoomCategoriesVO searchHotelRoomCategoriesVO = new SearchHotelRoomCategoriesVO(category)
                    hotelRoomCategoryList.add(searchHotelRoomCategoriesVO)
                }

                hotel.roomTypes.roomType.each { roomType ->
                    SearchHotelRoomTypeVO searchHotelRoomTypeVO = new SearchHotelRoomTypeVO(roomType)
                    searchHotelRoomTypeList.add(searchHotelRoomTypeVO)
                }

                hotel.facilities.facility.each { facility ->
                    SearchHotelFacilitiesVO searchHotelFacilitiesVO = new SearchHotelFacilitiesVO(facility)
                    hotelFacilityList.add(searchHotelFacilitiesVO)
                }



                hotel.rates.rate.each { rates ->
                    List<SearchHotelMealPlanVO> mealPlanVOList = []
                    List<SearchHotelRefundPolicyVO> refundPolicyVOList = []

                    List<SearchRoomRateTypeVO> hotelRoomRateList = []

                    HotelRoomRatesVO hotelRoomRatesVO = new HotelRoomRatesVO(rates)


                    rates.roomInfo.room.roomrate.each { roomRate ->
                        println '========RoomRate Type55555555=====' + roomRate.attribute("type")
                        println '========RoomRate 99999=====' + roomRate.text()
                        SearchRoomRateTypeVO searchRoomRateTypeVO = new SearchRoomRateTypeVO(roomRate)
                        hotelRoomRateList.add(searchRoomRateTypeVO)
                    }
                    hotelRoomRateList.each {
                        println '=========roomRate=Type===' + it.roomRateType
                        println '=========roomRate====' + it.roomRate
                    }

                    rates.mealPlans.mealPlan.each { mealPlan ->
                        println '==========mealPlan========' + mealPlan.text()
                        SearchHotelMealPlanVO searchHotelMealPlanVO = new SearchHotelMealPlanVO(mealPlan)
                        mealPlanVOList.add(searchHotelMealPlanVO)
                    }

                    rates.refundPolicy.policy.each { refundPolicy ->
                        println '==========RefundPolicy========' + refundPolicy.attribute("deduction")
                        SearchHotelRefundPolicyVO searchHotelRefundPolicyVO = new SearchHotelRefundPolicyVO(refundPolicy)
                        refundPolicyVOList.add(searchHotelRefundPolicyVO)
                    }
                    mealPlanVOList.each { println '=========Meal Plan====' + it.mealPlan }

                    hotelRoomRatesVO.roomRateType = hotelRoomRateList
                    hotelRoomRatesVO.mealPlan = mealPlanVOList
                    hotelRoomRatesVO.refundPolicy = refundPolicyVOList
                    hotelRoomRatesVOs2.add(hotelRoomRatesVO)
                }

                hotelRoomRatesVOs2.eachWithIndex { roomRate, i ->
                    println '==========room rate in roomRate====' + roomRate.roomRateType[0].roomRate
                }
                searchHotelVO.inclusions = hotelInclusionList
                searchHotelVO.searchHotelMedias = hotelMediaList
                searchHotelVO.searchHotelRoomCategory = hotelRoomCategoryList
                searchHotelVO.searchHotelRoomType = searchHotelRoomTypeList
                searchHotelVO.hotelFacility = hotelFacilityList
                searchHotelVO.hotelRoomRatesVOs = hotelRoomRatesVOs2


                hotelList.add(searchHotelVO)
            }


            hotelList.each { hotel ->
                println '+++++++++++++Hotel name+++++' + hotel.hotelName
                println '+++++++++++++Hotel id+++++' + hotel.hotelId
                println '+++++++++++++Hotel description+++++' + hotel.shortDescription
                hotel.searchHotelMedias.each { src ->
                    println '==============mediaListDataFromSearchHotelMedia==========' + src.mediaSrc
                }
            }
            return hotelList
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

    }

    HotelVO details(HotelDetailVO hotelDetailVO) {

        println '==Inside HotelDetail Function========'
        def urlHotelDetails = "${MGHWORLD_BASE_URL}/getHotelDetail?token=${TOKEN}&hotelid=${hotelDetailVO.hotelId}"
        def hotelDetails = new XmlParser().parse(urlHotelDetails)

        println '==========hotel details PARSED DATA=================//////=========' + hotelDetails
        println '======hotelDetail========' + hotelDetails.children().name.text()
        HotelVO hotelVO = new HotelVO(hotelDetails)
        List<GeoDataVO> geoDataVOList = []
        List<HotelMediaVO> hotelMediaVOList = []
        List<HotelFacilityVO> hotelFacilityVOList = []


        hotelDetails.children().geoData.each { geodata ->
            GeoDataVO geoDataVO = new GeoDataVO(geodata)
            geoDataVOList.add(geoDataVO)
        }

        hotelDetails.children().contactDetail.each { contact ->
            List<ContactVO> contactVOList = []
            ContactVO contactVO = new ContactVO(contact)
            contactVOList.add(contactVO)
            hotelVO.contacts = contactVOList
        }

        hotelDetails.children().mediaList.media.each { media ->
            HotelMediaVO hotelMediaVO = new HotelMediaVO(media)
            hotelMediaVOList.add(hotelMediaVO)
        }

        hotelDetails.children().facilities.facility.each { facility ->
            HotelFacilityVO hotelFacilityVO = new HotelFacilityVO(facility)
            hotelFacilityVOList.add(hotelFacilityVO)
        }
        List<RoomTypeVO> roomTypeVOList = []
        hotelDetails.children().roomTypes.roomType.each { roomType ->
            println '====INSIDE=Hotel ROOM TYPE==============' + roomType.attribute("name")

            RoomTypeVO roomTypeVO = new RoomTypeVO(roomType)
            println '=ROOMTYPE NAME==' + roomTypeVO.roomTypeName
            roomTypeVOList.add(roomTypeVO)

            hotelVO.roomType = roomTypeVOList
            roomTypeVOList.each { println '====hotel VO roomType==' + it.roomTypeName }
        }
        List<RoomCategoriesNameVO> roomCategoriesNameVOList = []

        hotelDetails.children().roomCategories.roomCategory.each { roomCat ->

            println '+))))))))))((((((((((RoomCategory)))))))))))(((((((' + roomCat.attribute("name")
            RoomCategoriesNameVO roomCategoriesNameVO = new RoomCategoriesNameVO(roomCat)
            println '%%%%%%%%%%%%%%%roomCategoryVO%%%%%%%%' + roomCategoriesNameVO.name


            roomCat.mediaList.media.each { roomMedia ->
                println '+++++++++++++Media++++++' + roomMedia.attribute("src")
                List<RoomMediaVO> roomMediaVOList = []
                RoomMediaVO roomMediaVO = new RoomMediaVO(roomMedia)
                roomMediaVOList.add(roomMediaVO)
                roomCategoriesNameVO.roomPictures = roomMediaVOList
            }
            List<RoomFacilityVO> roomFacilityVOList = []

            roomCat.roomFacilities.facility.each { roomFacility ->
                RoomFacilityVO roomFacilityVO = new RoomFacilityVO(roomFacility)

                roomFacilityVOList.add(roomFacilityVO)
                roomCategoriesNameVO.roomFacilities = roomFacilityVOList
            }
            roomCategoriesNameVOList.add(roomCategoriesNameVO)
            hotelVO.roomCategory = roomCategoriesNameVOList
            hotelVO.hotelPictures = hotelMediaVOList
            hotelVO.facilities = hotelFacilityVOList
        }

        println '=======hotelVO======' + hotelVO.name
        GeoDataVO geoDataVO = new GeoDataVO(hotelDetails)
        hotelVO.geoData.add(geoDataVO)
        return hotelVO
    }

    def book(TboHotelBookingVO hotelBookingVO) {

    }

    Boolean isEnabled = true

    ServiceType serviceType = ServiceType.MGH


}
