package com.travelex.hotelApiService

import com.travelex.Hotel.BookingHistory
import com.travelex.TboApi.CityFromTBO
import com.travelex.User.User
import com.travelex.admin.MarginType
import com.travelex.admin.SetMargin
import com.travelex.hotelFunctionality.HotelBookingVO
import com.travelex.hotelFunctionality.HotelDetailVO
import com.travelex.hotelFunctionality.HotelSearchParamsVO
import com.travelex.hotelFunctionality.TboHotelBookingVO
import com.travelex.socials.hotel.ContactVO
import com.travelex.socials.hotel.GeoDataVO
import com.travelex.socials.hotel.HotelFacilityVO
import com.travelex.socials.hotel.HotelMediaVO
import com.travelex.socials.hotel.HotelVO
import com.travelex.socials.hotel.RoomCategoriesNameVO
import com.travelex.socials.hotel.RoomFacilityVO
import com.travelex.socials.hotel.RoomMediaVO
import com.travelex.socials.searchHotel.HotelRoomRatesVO
import com.travelex.socials.searchHotel.SearchHotelMediaListVO
import com.travelex.socials.searchHotel.SearchHotelRoomTypeVO
import com.travelex.socials.searchHotel.SearchHotelVO
import com.travelex.socials.searchHotel.SearchRoomRateTypeVO
import com.travelex.socials.searchHotel.TboRateVO
import com.travelex.socials.searchHotel.TboRoomAminities
import com.travelex.socials.searchHotel.TboSearchHotelRateVO
import com.travelex.socials.searchHotel.TboSearchHotelRoomDetailVO
import com.travelex.socials.searchHotel.TboSearchHotelRoomRateVO
import com.travelex.socials.searchHotel.TboSearchHotelWsOccupancyVO
import com.travelex.socials.tboSearchHotel.TboBookResponseVO
import com.travelex.socials.tboSearchHotel.TboSearchHotelVO
import org.apache.commons.lang.RandomStringUtils
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsHttpSession
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsWebRequest
import org.springframework.web.context.request.RequestContextHolder
import wslite.soap.SOAPClient
import wslite.soap.SOAPResponse

import javax.xml.namespace.QName
import javax.xml.soap.MessageFactory
import javax.xml.soap.SOAPBody
import javax.xml.soap.SOAPBodyElement
import javax.xml.soap.SOAPEnvelope
import javax.xml.soap.SOAPHeader
import javax.xml.soap.SOAPHeaderElement
import javax.xml.soap.SOAPMessage
import javax.xml.soap.SOAPPart
import java.text.SimpleDateFormat
import groovy.util.logging.*

@Log4j

class TboHotelAPI implements HotelAPI {

    def springSecurityService
    def grailsApplication

//    public static TRAVELX_UNAME = 'travelx360'
//    public static TRAVELX_PASS_HOTEL = 'travel@1234'

    public static TRAVELX_UNAME = 'DELT261'
    public static TRAVELX_PASS_HOTEL = '360@travel'

    List<SearchHotelVO> search(HotelSearchParamsVO searchParams) {
        println '=========Inside TBO ========' + searchParams?.place
        String checkIn = searchParams.checkIn
        String checkOut = searchParams.checkOut
        SimpleDateFormat fromUser = new SimpleDateFormat("mm/dd/yyyy");
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-mm-dd")
        SimpleDateFormat fromUser2 = new SimpleDateFormat("mm/dd/yyyy");
        SimpleDateFormat myFormat2 = new SimpleDateFormat("yyyy-mm-dd")
        try {
            println '=========Inside TBO2 ========' + fromUser.parse(checkIn)
            String checkInDate = myFormat.format(fromUser.parse(checkIn))
            String checkOutDate = myFormat2.format(fromUser2.parse(checkOut))

            ///////////////All Star rating hotel will display///////////////////////////
            String rating = "All"

            CityFromTBO cityFromTBO = CityFromTBO.findByNameIlike(searchParams?.place)
            println '=========Inside TBO2 ========'
            println '=========Inside TBO2 ====City Name===='
            println '=========Inside TBO2 ====City Name====' + cityFromTBO.name

            String locId = cityFromTBO.locationId
            String location = cityFromTBO.name

            Boolean dom = cityFromTBO?.countryFromTbo?.countryName == "India"
            println '=========Domestic or not======' + dom
            String coun = cityFromTBO?.countryFromTbo?.countryName
            print('-------country name---11----------' + coun)

//            def client = new SOAPClient("http://api.tektravels.com/tbohotelapi_v6/hotelservice.asmx")
            def client = new SOAPClient("http://airapi.travelboutiqueonline.com/tbohotelapi_v6/hotelservice.asmx")

            SOAPResponse response = null

            if (searchParams?.noOfRooms == "1") {
                response = client.send("""<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
<soap:Header>
<AuthenticationData xmlns="http://TekTravel/HotelBookingApi">
<SiteName></SiteName>
<AccountCode></AccountCode>
<UserName>${TRAVELX_UNAME}</UserName>
<Password>${TRAVELX_PASS_HOTEL}</Password>
</AuthenticationData>
</soap:Header>
<soap:Body>
<Search xmlns="http://TekTravel/HotelBookingApi">
<request>
<CheckInDate>${checkInDate}</CheckInDate>
<CheckOutDate>${checkOutDate}</CheckOutDate>
<CountryName>${coun}</CountryName>
<IsDomestic>${dom}</IsDomestic>
<CityReference>${location}</CityReference>
<CityId>${locId}</CityId>
<NoOfRooms>${searchParams?.noOfRooms}</NoOfRooms>
<RoomGuest>
<WSRoomGuestData>
<NoOfAdults>${searchParams?.adult as Integer}</NoOfAdults>
 <NoOfChild>${searchParams?.children ? searchParams?.children as Integer : 0}</NoOfChild>
<ChildAge>
${searchParams?.firstChildAgeRoomOne ? "<int>" + searchParams?.firstChildAgeRoomOne + "</int>" : ""}
${searchParams?.secondChildAgeRoomOne ? "<int>" + searchParams?.secondChildAgeRoomOne + "</int>" : ""}
</ChildAge>
</WSRoomGuestData>
</RoomGuest>
<HotelName></HotelName>
<Rating>${rating}</Rating>
</request>
</Search>
</soap:Body>
</soap:Envelope>"""
                )
            } else if (searchParams?.noOfRooms == "2") {
                response = client.send("""<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
<soap:Header>
<AuthenticationData xmlns="http://TekTravel/HotelBookingApi">
<SiteName></SiteName>
<AccountCode></AccountCode>
<UserName>${TRAVELX_UNAME}</UserName>
<Password>${TRAVELX_PASS_HOTEL}</Password>
</AuthenticationData>
</soap:Header>
<soap:Body>
<Search xmlns="http://TekTravel/HotelBookingApi">
<request>
<CheckInDate>${checkInDate}</CheckInDate>
<CheckOutDate>${checkOutDate}</CheckOutDate>
<CountryName>${coun}</CountryName>
<IsDomestic>${dom}</IsDomestic>
<CityReference>${location}</CityReference>
<CityId>${locId}</CityId>
<NoOfRooms>${searchParams?.noOfRooms}</NoOfRooms>
<RoomGuest>
<WSRoomGuestData>
<NoOfAdults>${searchParams?.adult as Integer}</NoOfAdults>
 <NoOfChild>${searchParams?.children ? searchParams?.children as Integer : 0}</NoOfChild>
<ChildAge>
${searchParams?.firstChildAgeRoomOne ? "<int>" + searchParams?.firstChildAgeRoomOne + "</int>" : ""}
${searchParams?.secondChildAgeRoomOne ? "<int>" + searchParams?.secondChildAgeRoomOne + "</int>" : ""}
</ChildAge>
</WSRoomGuestData>
<WSRoomGuestData>
<NoOfAdults>${searchParams?.peopleInRoomTwo as Integer}</NoOfAdults>
 <NoOfChild>${searchParams?.childInRoomTwo ? searchParams?.childInRoomTwo as Integer : 0}</NoOfChild>
<ChildAge>
${searchParams?.firstChildAgeRoomTwo ? "<int>" + searchParams?.firstChildAgeRoomTwo + "</int>" : ""}
${searchParams?.secondChildAgeRoomTwo ? "<int>" + searchParams?.secondChildAgeRoomTwo + "</int>" : ""}
</ChildAge>
</WSRoomGuestData>
</RoomGuest>
<HotelName></HotelName>
<Rating>${rating}</Rating>
</request>
</Search>
</soap:Body>
</soap:Envelope>"""
                )
            } else if (searchParams?.noOfRooms == "3") {
                response = client.send("""<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
<soap:Header>
<AuthenticationData xmlns="http://TekTravel/HotelBookingApi">
<SiteName></SiteName>
<AccountCode></AccountCode>
<UserName>${TRAVELX_UNAME}</UserName>
<Password>${TRAVELX_PASS_HOTEL}</Password>
</AuthenticationData>
</soap:Header>
<soap:Body>
<Search xmlns="http://TekTravel/HotelBookingApi">
<request>
<CheckInDate>${checkInDate}</CheckInDate>
<CheckOutDate>${checkOutDate}</CheckOutDate>
<CountryName>${coun}</CountryName>
<IsDomestic>${dom}</IsDomestic>
<CityReference>${location}</CityReference>
<CityId>${locId}</CityId>
<NoOfRooms>${searchParams?.noOfRooms}</NoOfRooms>
<RoomGuest>
<WSRoomGuestData>
<NoOfAdults>${searchParams?.adult as Integer}</NoOfAdults>
 <NoOfChild>${searchParams?.children ? searchParams?.children as Integer : 0}</NoOfChild>
<ChildAge>
${searchParams?.firstChildAgeRoomOne ? "<int>" + searchParams?.firstChildAgeRoomOne + "</int>" : ""}
${searchParams?.secondChildAgeRoomOne ? "<int>" + searchParams?.secondChildAgeRoomOne + "</int>" : ""}
</ChildAge>
</WSRoomGuestData>
<WSRoomGuestData>
<NoOfAdults>${searchParams?.peopleInRoomTwo as Integer}</NoOfAdults>
 <NoOfChild>${searchParams?.childInRoomTwo ? searchParams?.childInRoomTwo as Integer : 0}</NoOfChild>
<ChildAge>
${searchParams?.firstChildAgeRoomTwo ? "<int>" + searchParams?.firstChildAgeRoomTwo + "</int>" : ""}
${searchParams?.secondChildAgeRoomTwo ? "<int>" + searchParams?.secondChildAgeRoomTwo + "</int>" : ""}
</ChildAge>
</WSRoomGuestData>
<WSRoomGuestData>
<NoOfAdults>${searchParams?.peopleInRoomThree as Integer}</NoOfAdults>
 <NoOfChild>${searchParams?.childInRoomThree ? searchParams?.childInRoomThree as Integer : 0}</NoOfChild>
<ChildAge>
${searchParams?.firstChildAgeRoomThree ? "<int>" + searchParams?.firstChildAgeRoomThree + "</int>" : ""}
${searchParams?.secondChildAgeRoomThree ? "<int>" + searchParams?.secondChildAgeRoomThree + "</int>" : ""}
</ChildAge>
</WSRoomGuestData>
</RoomGuest>
<HotelName></HotelName>
<Rating>${rating}</Rating>
</request>
</Search>
</soap:Body>
</soap:Envelope>"""
                )
            } else if (searchParams?.noOfRooms == "4") {
                response = client.send("""<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
<soap:Header>
<AuthenticationData xmlns="http://TekTravel/HotelBookingApi">
<SiteName></SiteName>
<AccountCode></AccountCode>
<UserName>${TRAVELX_UNAME}</UserName>
<Password>${TRAVELX_PASS_HOTEL}</Password>
</AuthenticationData>
</soap:Header>
<soap:Body>
<Search xmlns="http://TekTravel/HotelBookingApi">
<request>
<CheckInDate>${checkInDate}</CheckInDate>
<CheckOutDate>${checkOutDate}</CheckOutDate>
<CountryName>${coun}</CountryName>
<IsDomestic>${dom}</IsDomestic>
<CityReference>${location}</CityReference>
<CityId>${locId}</CityId>
<NoOfRooms>${searchParams?.noOfRooms}</NoOfRooms>
<RoomGuest>
    <WSRoomGuestData>
        <NoOfAdults>${searchParams?.adult as Integer}</NoOfAdults>
        <NoOfChild>${searchParams?.children ? searchParams?.children as Integer : ""}</NoOfChild>
        <ChildAge>
            ${searchParams?.firstChildAgeRoomOne ? "<int>" + searchParams?.firstChildAgeRoomOne + "</int>" : ""}
            ${searchParams?.secondChildAgeRoomOne ? "<int>" + searchParams?.secondChildAgeRoomOne + "</int>" : ""}
        </ChildAge>
    </WSRoomGuestData>
    <WSRoomGuestData>
        <NoOfAdults>${searchParams?.peopleInRoomTwo as Integer}</NoOfAdults>
         <NoOfChild>${searchParams?.childInRoomTwo ? searchParams?.childInRoomTwo as Integer : ""}</NoOfChild>
        <ChildAge>
            ${searchParams?.firstChildAgeRoomTwo ? "<int>" + searchParams?.firstChildAgeRoomTwo + "</int>" : ""}
            ${searchParams?.secondChildAgeRoomTwo ? "<int>" + searchParams?.secondChildAgeRoomTwo + "</int>" : ""}
        </ChildAge>
    </WSRoomGuestData>
    <WSRoomGuestData>
        <NoOfAdults>${searchParams?.peopleInRoomThree as Integer}</NoOfAdults>
        <NoOfChild>${searchParams?.childInRoomThree ? searchParams?.childInRoomThree as Integer : ""}</NoOfChild>
        <ChildAge>
            ${searchParams?.firstChildAgeRoomThree ? "<int>" + searchParams?.firstChildAgeRoomThree + "</int>" : ""}
            ${searchParams?.secondChildAgeRoomThree ? "<int>" + searchParams?.secondChildAgeRoomThree + "</int>" : ""}
        </ChildAge>
    </WSRoomGuestData>
    <WSRoomGuestData>
        <NoOfAdults>${searchParams?.peopleInRoomFour as Integer}</NoOfAdults>
         <NoOfChild>${searchParams?.childInRoomFour ? searchParams?.childInRoomFour as Integer : ""}</NoOfChild>
        <ChildAge>
            ${searchParams?.firstChildAgeRoomFour ? "<int>" + searchParams?.firstChildAgeRoomFour + "</int>" : ""}
            ${searchParams?.secondChildAgeRoomFour ? "<int>" + searchParams?.secondChildAgeRoomFour + "</int>" : ""}
        </ChildAge>
    </WSRoomGuestData>
</RoomGuest>
<HotelName></HotelName>
<Rating>${rating}</Rating>
</request>
</Search>
</soap:Body>
</soap:Envelope>"""
                )
            }
            println '===response11====' + response.text
            def ref = new XmlSlurper().parseText(response.text)
            List<SearchHotelVO> searchHotelVoList = []
            log.info '----search status code-----------' + ref.children().SearchResponse.SearchResult.Status.StatusCode + '======Description========' + ref.children().SearchResponse.SearchResult.Status.Description
            String sessionId = ref.children().SearchResponse.SearchResult.SessionId.text()
            ref.children().SearchResponse.SearchResult.Result.WSHotelResult.each { hotel ->
                log.info '============HotelName=========' + hotel.HotelInfo.HotelName.text()
                SearchHotelVO searchHotelVO = new SearchHotelVO()
                searchHotelVO.hotelId = hotel.HotelInfo.HotelCode.text()
                searchHotelVO.sessionId = sessionId
                searchHotelVO.hotelName = hotel.HotelInfo.HotelName.text()
                searchHotelVO.shortDescription = hotel.HotelInfo.HotelDescription.text()
                searchHotelVO.location = hotel.HotelInfo.HotelLocation.text()
                searchHotelVO.hotelRating = hotel.HotelInfo.Rating.text()
                searchHotelVO.hotelIndex = hotel.Index.text()

                List<SearchHotelMediaListVO> searchHotelMedia = []
                SearchHotelMediaListVO hotelMediaListVO = new SearchHotelMediaListVO()
                hotelMediaListVO.mediaSrc = hotel.HotelInfo.HotelPicture.text()
                print('------------ media src is -----11---------------' + hotel.HotelInfo.HotelPicture.text())
                print('------------ media src is ----22----------------' + hotelMediaListVO.mediaSrc)
                searchHotelMedia.add(hotelMediaListVO)
                searchHotelVO.searchHotelMedias = searchHotelMedia

                ///////////////////////////////////////////////////////////////////////////////////////////////////
                List<TboSearchHotelRoomDetailVO> hotelRoomDetailVOArrayList1 = []

                List<HotelRoomRatesVO> hotelRoomRatesVO = []


                hotel.RoomDetails.WSHotelRoomsDetails.each { room ->

                    TboSearchHotelRoomDetailVO hotelRoomDetailVO1 = new TboSearchHotelRoomDetailVO()
                    TboSearchHotelRateVO tboSearchHotelRateVO = new TboSearchHotelRateVO()
                    TboSearchHotelRoomRateVO tboSearchHotelRoomRateVO = new TboSearchHotelRoomRateVO()

                    hotelRoomDetailVO1.ratePlanCOde = room.RatePlanCode
                    hotelRoomDetailVO1.roomIndex = room.Index
                    hotelRoomDetailVO1.roomTypeCode = room.RoomTypeCode
                    hotelRoomDetailVO1.roomTypeName = room.RoomTypeName
                    print('---------roomTypeName-----------' + hotelRoomDetailVO1.roomTypeName)
                    tboSearchHotelRateVO.totalRate = room.Rate.TotalRate
                    print('-------11111-------------------------' + tboSearchHotelRateVO.totalRate)
                    tboSearchHotelRateVO.totalTax = room.Rate.TotalTax
                    print('-------222222------total tax by tbo-------------------' + tboSearchHotelRateVO.totalTax)
                    if (dom) {
                        String setMargin = SetMargin.findByMarginType(MarginType.DOMHOTEL).marginMoney
                        print('---------travelex 360 margin - domestic-----------------' + setMargin)
                        room.Rate.AgentMarkUp = setMargin
                        print('---------travelex 360 margin -----room.Rate.AgentMarkUp-------------' + room.Rate.AgentMarkUp)
                        tboSearchHotelRateVO.agentMarkUp = room.Rate.AgentMarkUp
                        print('-------333333-------------------------' + tboSearchHotelRateVO.agentMarkUp)
                    } else {
                        String setMargin = SetMargin.findByMarginType(MarginType.INTERHOTEL).marginMoney
                        print('---------travelex 360 international margin ------------------' + setMargin)
                        room.Rate.AgentMarkUp = setMargin
                        print('---------travelex 360 margin -----room.Rate.AgentMarkUp-------------' + room.Rate.AgentMarkUp)
                        tboSearchHotelRateVO.agentMarkUp = room.Rate.AgentMarkUp
                        print('-------333333-------------------------' + tboSearchHotelRateVO.agentMarkUp)

                    }


                    tboSearchHotelRateVO.currency = room.Rate.Currency
                    print('-------4444444-------------------------' + tboSearchHotelRateVO.currency)

                    hotelRoomDetailVO1.hotelRateVO = tboSearchHotelRateVO

                    tboSearchHotelRoomRateVO.totalRoomRate = room.RoomRate.TotalRoomRate
                    print('-------55555-------------------------' + tboSearchHotelRoomRateVO.totalRoomRate)

                    tboSearchHotelRoomRateVO.extraGuestCharges = room.RoomRate.ExtraGuestCharges
                    print('-------666666-------------------------' + tboSearchHotelRoomRateVO.extraGuestCharges)
                    tboSearchHotelRoomRateVO.discount = room.RoomRate.DisCount
                    tboSearchHotelRoomRateVO.otherCharges = room.RoomRate.OtherCharges
                    tboSearchHotelRoomRateVO.serviceTax = room.RoomRate.ServiceTax
                    tboSearchHotelRoomRateVO.totalRoomTax = room.RoomRate.TotalRoomTax
                    hotelRoomDetailVOArrayList1.add(hotelRoomDetailVO1)

                    List<TboRoomAminities> amenities1 = []
                    room.Amenities.string.each {
                        TboRoomAminities tboRoomAminities = new TboRoomAminities()
                        tboRoomAminities.name = it
                        print('--------- room amenity are ------------' + it)
                        amenities1.add(tboRoomAminities)
                    }
                    hotelRoomDetailVO1.amenities = amenities1


                    TboSearchHotelRoomRateVO tboSearchHotelRoomRateVO1 = new TboSearchHotelRoomRateVO()
                    tboSearchHotelRoomRateVO1.totalRoomRate = room.RoomRate.TotalRoomRate
                    print('-------room.RoomRate.TotalRoomRate---------' + room.RoomRate.TotalRoomRate)
                    tboSearchHotelRoomRateVO1.extraGuestCharges = room.RoomRate.ExtraGuestCharges
                    print('-------room.RoomRate.ExtraGuestCharges---------' + room.RoomRate.ExtraGuestCharges)
                    tboSearchHotelRoomRateVO1.discount = room.RoomRate.DisCount
                    print('-------room.RoomRate.DisCount---------' + room.RoomRate.DisCount)
                    tboSearchHotelRoomRateVO1.otherCharges = room.RoomRate.OtherCharges
                    print('-------room.RoomRate.OtherCharges---------' + room.RoomRate.OtherCharges)
                    tboSearchHotelRoomRateVO1.serviceTax = room.RoomRate.ServiceTax
                    print('-------room.RoomRate.ServiceTax---------' + room.RoomRate.ServiceTax)
                    tboSearchHotelRoomRateVO1.totalRoomTax = room.RoomRate.TotalRoomTax
                    print('-------room.RoomRate.TotalRoomTax---------' + room.RoomRate.TotalRoomTax)
                    List<TboRateVO> tboRateVOList = []
                    room.RoomRate.DayRates.WSDayRates.each { dayRate ->
                        TboRateVO tboRateVO = new TboRateVO()
                        tboRateVO.baseFare = dayRate.BaseFare
                        print('-------dayRate.BaseFare---------' + dayRate.BaseFare)
                        tboRateVO.dayRate = dayRate.Days
                        print('-------ayRate.Days---------' + dayRate.Days)
                        tboRateVOList.add(tboRateVO)
                    }
                    tboSearchHotelRoomRateVO1.dayRates = tboRateVOList
                    hotelRoomDetailVO1.hotelRoomRateVO = tboSearchHotelRoomRateVO1

                    TboSearchHotelWsOccupancyVO tboSearchHotelWsOccupancyVO = new TboSearchHotelWsOccupancyVO()
                    tboSearchHotelWsOccupancyVO.days = room.RoomRate.DayRates.WSDayRates.Days
                    print('------------Days-----------------' + room.RoomRate.DayRates.WSDayRates.Days)
                    tboSearchHotelWsOccupancyVO.baseFare = room.RoomRate.DayRates.WSDayRates.BaseFare
                    print('------------BaseFare-----------------' + room.RoomRate.DayRates.WSDayRates.BaseFare)
                    tboSearchHotelWsOccupancyVO.maxAdult = room.Occupancy.MaxAdult
                    print('------------MaxAdult-----------------' + room.Occupancy.MaxAdult)
                    tboSearchHotelWsOccupancyVO.maxChild = room.Occupancy.MaxChild
                    print('------------MaxChild-----------------' + room.Occupancy.MaxChild)
                    tboSearchHotelWsOccupancyVO.maxInfant = room.Occupancy.MaxInfant
                    print('------------MaxInfant-----------------' + room.Occupancy.MaxInfant)
                    tboSearchHotelWsOccupancyVO.maxGuest = room.Occupancy.MaxGuest
                    print('------------MaxGuest-----------------' + room.Occupancy.MaxGuest)
                    tboSearchHotelWsOccupancyVO.baseAdult = room.Occupancy.BaseAdult
                    print('------------BaseAdult-----------------' + room.Occupancy.BaseAdult)
                    tboSearchHotelWsOccupancyVO.baseChild = room.Occupancy.BaseChild
                    print('------------BaseChild-----------------' + room.Occupancy.BaseChild)
                    hotelRoomDetailVO1.hotelWsOccupancyVO = tboSearchHotelWsOccupancyVO

                }
                searchHotelVO.hotelRoomDetailVOArrayList = hotelRoomDetailVOArrayList1
                print('==========aaaaaaa======================' + searchHotelVO.hotelRoomDetailVOArrayList.hotelRateVO)
                searchHotelVoList.add(searchHotelVO)
            }
            return searchHotelVoList

        }
        catch (Exception e) {
            List<SearchHotelVO> searchHotelVoList = []
            log.info '===Exception occurred==' + e
            return searchHotelVoList

        }


    }

    HotelVO details(HotelDetailVO hotelDetailVO) {

//        def client = new SOAPClient("http://api.tektravels.com/tbohotelapi_v6/hotelservice.asmx")
        def client = new SOAPClient("http://airapi.travelboutiqueonline.com/tbohotelapi_v6/hotelservice.asmx")

        SOAPResponse response2 = client.send(
                """<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
<soap:Header>
<AuthenticationData xmlns="http://TekTravel/HotelBookingApi">
<SiteName></SiteName>
<AccountCode></AccountCode>
<UserName>${TRAVELX_UNAME}</UserName>
<Password>${TRAVELX_PASS_HOTEL}</Password>
</AuthenticationData>
</soap:Header>
<soap:Body>
 <GetHotelDetails xmlns="http://TekTravel/HotelBookingApi">
      <request>
        <SessionId>${hotelDetailVO.sessionId}</SessionId>
        <Index>${hotelDetailVO.hotelIndex as Integer}</Index>
      </request>
    </GetHotelDetails>
</soap:Body>
</soap:Envelope>"""
        )
        println '---==response==Hotel Details==' + response2.text
        def ref = new XmlSlurper().parseText(response2.text)

//        println '========string Details====' + ref


        def hotelDetail = ref.children().GetHotelDetailsResponse.GetHotelDetailsResult.HotelDetail

        println '==========Address=========' + hotelDetail.Address
        println '==========Attractions=========' + hotelDetail.Attractions
        println '==========CountryName=========' + hotelDetail.CountryName
        println '==========Description=========' + hotelDetail.Description
        println '==========Email=========' + hotelDetail.Email
        println '==========FaxNumber=========' + hotelDetail.FaxNumber
        println '==========HotelCode=========' + hotelDetail.HotelCode
        println '==========HotelFacilities=========' + hotelDetail.HotelFacilities
        println '==========HotelFacilities1111111111111111=========' + hotelDetail.HotelFacilities

        hotelDetail.HotelFacilities.string.each {
            println '==========HotelFacilities222222222222222222=========' + it

        }
        println '==========HotelName=========' + hotelDetail.HotelName
        println '==========HotelPolicy=========' + hotelDetail.HotelPolicy
        println '==========HotelRating=========' + hotelDetail.HotelRating
        println '==========Image=========' + hotelDetail.Image
        println '==========ImageUrls=========' + hotelDetail.ImageUrls
        println '==========Map=========' + hotelDetail.Map
        println '==========PhoneNumber=========' + hotelDetail.PhoneNumber
        println '==========PinCode=========' + hotelDetail.PinCode
        println '==========RoomData====RoomData=====' + hotelDetail.RoomData
        println '==========RoomData====RoomData11111111=====' + hotelDetail.RoomData.WSRoom
        println '==========RoomData====RoomData22222222=====' + hotelDetail.RoomData


        hotelDetail.RoomData.WSRoom.each { roomDetails ->
            println '==========RoomData====Description=====' + roomDetails.Description
            println '==========RoomData====Description=====' + roomDetails.Images
            println '==========RoomData====Description=====' + roomDetails.RoomName
            println '==========RoomData====Description=====' + roomDetails.RoomTypeCode

        }

        def wsRooms = hotelDetail.RoomData.WSRoom
        print('----------- here ws room image details are ---------' + wsRooms.RoomName)
        print('----------- here ws room image details are ---------' + wsRooms.RoomTypeCode)
        print('----------- here ws room image details are ---------' + wsRooms.Description)
        print('----------- here ws room image details are ---------' + wsRooms.Images)




        println '==========RoomData====RoomFacilities====' + hotelDetail.RoomFacilities
        println '==========RoomData====Services====' + hotelDetail.Services
        println '==========HotelWebsiteUrl===' + hotelDetail.HotelWebsiteUrl





        HotelVO hotelVO = new HotelVO()
        hotelVO.hotelId = hotelDetail.HotelCode
        hotelVO.name = hotelDetail.HotelName
        hotelVO.category = hotelDetail.HotelRating
        hotelVO.category = hotelDetail.HotelRating
        hotelVO.description = hotelDetail.Description
        hotelVO.address = hotelDetail.Address

        List<HotelFacilityVO> facilities = []

        hotelDetail.HotelFacilities.string.each {
            println '==========HotelFacilities=========' + it
            HotelFacilityVO hotelFacilityVO = new HotelFacilityVO()
            hotelFacilityVO.facilityName = it
            facilities.add(hotelFacilityVO)
        }

        GeoDataVO geoDataVO = new GeoDataVO()

        String map = hotelDetail.Map
        if (map) {
            geoDataVO.latitude = map?.tokenize('|')?.first()
            println '==========latitude=========' + geoDataVO.latitude
            geoDataVO.longitude = map?.tokenize('|')?.last()
            println '==========longitude=========' + geoDataVO.longitude
        } else {
            println '===============lattitude and longitude================' + geoDataVO.longitude
        }
        hotelVO.geoData = geoDataVO as List<GeoDataVO>
        List<HotelMediaVO> hotelPictures = []
        hotelDetail.ImageUrls.string.each {
            HotelMediaVO hotelMediaVO = new HotelMediaVO()
            hotelMediaVO.src = it
            hotelPictures.add(hotelMediaVO)
        }

        hotelVO.facilities = facilities
        hotelVO.hotelPictures = hotelPictures

        List<ContactVO> contacts1 = []

        ContactVO contactVO = new ContactVO()
        contactVO.email = hotelDetail.Email
        contactVO.phone = hotelDetail.PhoneNumber
        if (contactVO.phone.contains('-Mobile') || contactVO.phone.contains('-Switch')) {
            contactVO.phone = contactVO.phone.substring(0, contactVO.phone.indexOf("-"))
        } else {
            contactVO.phone = contactVO.phone
        }
        log.info '============= now contact no is-------111111111------' + contactVO.phone
        contacts1.add(contactVO)

        hotelVO.contacts = contacts1
        List<RoomCategoriesNameVO> roomCategory1 = []
        List<RoomFacilityVO> roomFacilities1 = []
        hotelDetail.RoomFacilities.string.each {
            RoomFacilityVO facilityVO = new RoomFacilityVO()
            facilityVO.roomFacilityName = it
            roomFacilities1.add(facilityVO)
        }

        hotelDetail.RoomData.WSRoom.each { roomDetails ->
            List<RoomMediaVO> roomPictures1 = []

            RoomCategoriesNameVO categoriesNameVO = new RoomCategoriesNameVO()
            categoriesNameVO.roomCategoryId = roomDetails.RoomTypeCode
            categoriesNameVO.name = roomDetails.RoomName
            categoriesNameVO.roomFacilities = roomFacilities1
//            categoriesNameVO.roomCategoryId =roomDetails.RoomTypeCode

            roomDetails.Images.string.each {
                RoomMediaVO roomMediaVO = new RoomMediaVO()
                roomMediaVO.roomMediaSrc = it
                roomPictures1.add(roomMediaVO)
            }
            categoriesNameVO.roomPictures = roomPictures1
            categoriesNameVO.description = roomDetails.Description

            roomCategory1.add(categoriesNameVO)
        }

        hotelVO.roomCategory = roomCategory1

        return hotelVO
    }

    def book(TboHotelBookingVO hotelBookingVO) {

        User user = springSecurityService as User
        String totalOccupants = hotelBookingVO.adult?.toInteger() + (hotelBookingVO.peopleInRoomTwo ? hotelBookingVO.peopleInRoomTwo?.toInteger() : 0) + (hotelBookingVO.peopleInRoomThree ? hotelBookingVO.peopleInRoomThree?.toInteger() : 0) + (hotelBookingVO.peopleInRoomFour ? hotelBookingVO.peopleInRoomFour?.toInteger() : 0) +
                hotelBookingVO.child?.toInteger() + (hotelBookingVO.childInRoomTwo ? hotelBookingVO.childInRoomTwo?.toInteger() : 0) + (hotelBookingVO.childInRoomThree ? hotelBookingVO.childInRoomThree?.toInteger() : 0) + (hotelBookingVO.childInRoomFour ? hotelBookingVO.childInRoomFour?.toInteger() : 0)

        Boolean leadGuest = true

        TboSearchHotelRoomDetailVO roomDetailVO = new TboSearchHotelRoomDetailVO()
        SearchHotelVO hotelVO = new SearchHotelVO()

        GrailsWebRequest request = RequestContextHolder?.currentRequestAttributes()
        GrailsHttpSession session = request.session

        List<SearchHotelVO> hotelList = session["hotelList"] as List

        hotelList.each { hotel ->
            if (hotel.hotelIndex == hotelBookingVO.hotelIndex) {
                hotelVO.sessionId = hotel.sessionId
                hotelVO.category = hotel.category
                hotelVO.hotelName = hotel.hotelName
                hotelVO.hotelId = hotel.hotelId
                hotelVO.hotelIndex = hotel.hotelIndex
                hotel.hotelRoomDetailVOArrayList.each { hotelRoom ->
                    if (hotelRoom.roomIndex == hotelBookingVO.roomId) {
                        roomDetailVO.roomIndex = hotelRoom.roomIndex
                        roomDetailVO.amenities = hotelRoom.amenities
                        roomDetailVO.hotelRateVO = hotelRoom.hotelRateVO
                        roomDetailVO.hotelRoomRateVO = hotelRoom.hotelRoomRateVO
                        roomDetailVO.hotelWsOccupancyVO = hotelRoom.hotelWsOccupancyVO
                        roomDetailVO.ratePlanCOde = hotelRoom.ratePlanCOde
                        roomDetailVO.roomTypeName = hotelRoom.roomTypeName
                        roomDetailVO.roomTypeCode = hotelRoom.roomTypeCode
                    }
                }
            }
        }

        println '=============roomDetails===============' + roomDetailVO.hotelRateVO
        MessageFactory factory = MessageFactory.newInstance();
        SOAPMessage soapMsg = factory.createMessage();
        SOAPPart part = soapMsg.getSOAPPart();
        SOAPEnvelope envelope = part.getEnvelope();
        SOAPHeader header = envelope.getHeader();
        SOAPBody body = envelope.getBody();
        QName qName = new QName("http://TekTravel/HotelBookingApi", "AuthenticationData");
        SOAPHeaderElement headerElement = header.addHeaderElement(qName);
//
        headerElement.addChildElement("UserName").addTextNode("${TRAVELX_UNAME}")
        headerElement.addChildElement("Password").addTextNode("${TRAVELX_PASS_HOTEL}")

        QName qName1 = new QName("http://TekTravel/HotelBookingApi", "Book");

        SOAPBodyElement element = body.addBodyElement(qName1);

        def reqElementTag = element.addChildElement("request")
        def roomCode = reqElementTag.addChildElement("RoomCodes");

        (1..(hotelBookingVO.noOfRoom1 as Integer)).each {
            roomCode.addChildElement("string").addTextNode("${roomDetailVO.roomTypeCode + "###" + roomDetailVO.ratePlanCOde}")
        }

        hotelBookingVO.adult1AddressOneRoom1 = "travelex demo"
        hotelBookingVO.adult1CountryCode1 = "51"
        hotelBookingVO.adult1CityCode1 = "12"
        hotelBookingVO.adult1CityRoom1 = "a"
        hotelBookingVO.adult1StateRoom1 = "HR"
        hotelBookingVO.adult1CountryRoom1 = "1"

        def guest = reqElementTag.addChildElement("Guest")
//        (1..(totalOccupants as Integer)).each {
        if (hotelBookingVO.adult1FNameRoom1) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.adult1FNameRoom1)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.adult1MNameRoom1)
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.adult1LNameRoom1)
            wsGuest.addChildElement("LeadGuest").addTextNode("${leadGuest}")
            wsGuest.addChildElement("Addressline1").addTextNode(hotelBookingVO.adult1AddressOneRoom1)
            wsGuest.addChildElement("Countrycode").addTextNode(hotelBookingVO.adult1CountryCode1)
            wsGuest.addChildElement("Areacode").addTextNode(hotelBookingVO.adult1CityCode1)
            wsGuest.addChildElement("Phoneno").addTextNode(hotelBookingVO.adult1PhoneRoom1)
            wsGuest.addChildElement("Email").addTextNode(hotelBookingVO.adult1EmailRoom1)
            wsGuest.addChildElement("City").addTextNode(hotelBookingVO.adult1CityRoom1)
            wsGuest.addChildElement("State").addTextNode(hotelBookingVO.adult1StateRoom1)
            wsGuest.addChildElement("Country").addTextNode(hotelBookingVO.adult1CountryRoom1)
            wsGuest.addChildElement("GuestType").addTextNode("Adult")
            wsGuest.addChildElement("RoomIndex").addTextNode("${0}")
        }
        if (hotelBookingVO.adult2FNameRoom1) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.adult2FNameRoom1)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.adult2MNameRoom1)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.adult2LNameRoom1)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Adult")
            wsGuest.addChildElement("RoomIndex").addTextNode("${0}")
        }
        if (hotelBookingVO.adult3FNameRoom1) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.adult3FNameRoom1)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.adult3MNameRoom1)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.adult3LNameRoom1)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Adult")
            wsGuest.addChildElement("RoomIndex").addTextNode("${0}")
        }
        if (hotelBookingVO.adult4FNameRoom1) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.adult4FNameRoom1)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.adult4MNameRoom1)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.adult4LNameRoom1)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Adult")
            wsGuest.addChildElement("RoomIndex").addTextNode("${0}")
        }
        if (hotelBookingVO.adult5FNameRoom1) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.adult5FNameRoom1)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.adult5MNameRoom1)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.adult5LNameRoom1)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Adult")
            wsGuest.addChildElement("RoomIndex").addTextNode("${0}")
        }

        if (hotelBookingVO.child1Room1FName) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.child1Room1FName)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.child1Room1MName)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.child1Room1LName)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Child")
            wsGuest.addChildElement("RoomIndex").addTextNode("${0}")
        }
        if (hotelBookingVO.child2Room1FName) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.child2Room1FName)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.child2Room1MName)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.child2Room1LName)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Child")
            wsGuest.addChildElement("RoomIndex").addTextNode("${0}")
        }
        if (hotelBookingVO.child3Room1FName) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.child3Room1FName)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.child3Room1MName)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.child3Room1LName)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Child")
            wsGuest.addChildElement("RoomIndex").addTextNode("${0}")
        }
        if (hotelBookingVO.child4Room1FName) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.child4Room1FName)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.child4Room1MName)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.child4Room1LName)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Child")
            wsGuest.addChildElement("RoomIndex").addTextNode("${0}")
        }



        if (hotelBookingVO.adult1FNameRoom2) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.adult1FNameRoom2)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.adult1MNameRoom2)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.adult1LNameRoom2)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Adult")
            wsGuest.addChildElement("RoomIndex").addTextNode("${1}")
        }
        if (hotelBookingVO.adult2FNameRoom2) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.adult2FNameRoom2)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.adult2MNameRoom2)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.adult2LNameRoom2)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Adult")
            wsGuest.addChildElement("RoomIndex").addTextNode("${1}")

        }
        if (hotelBookingVO.adult3FNameRoom2) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.adult3FNameRoom2)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.adult3MNameRoom2)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.adult3LNameRoom2)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Adult")
            wsGuest.addChildElement("RoomIndex").addTextNode("${1}")

        }
        if (hotelBookingVO.adult4FNameRoom2) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.adult4FNameRoom2)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.adult4MNameRoom2)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.adult4LNameRoom2)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Adult")
            wsGuest.addChildElement("RoomIndex").addTextNode("${1}")

        }
        if (hotelBookingVO.adult5FNameRoom2) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.adult5FNameRoom2)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.adult5MNameRoom2)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.adult5LNameRoom2)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Adult")
            wsGuest.addChildElement("RoomIndex").addTextNode("${1}")

        }

        if (hotelBookingVO.child1Room2FName) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.child1Room2FName)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.child1Room2MName)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.child1Room2LName)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Child")
            wsGuest.addChildElement("RoomIndex").addTextNode("${1}")

        }
        if (hotelBookingVO.child2Room2FName) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.child2Room2FName)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.child2Room2MName)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.child2Room2LName)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Child")
            wsGuest.addChildElement("RoomIndex").addTextNode("${1}")

        }
        if (hotelBookingVO.child3Room2FName) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.child3Room2FName)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.child3Room2MName)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.child3Room2LName)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Child")
            wsGuest.addChildElement("RoomIndex").addTextNode("${1}")

        }
        if (hotelBookingVO.child4Room2FName) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.child4Room2FName)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.child4Room2MName)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.child4Room2LName)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Child")
            wsGuest.addChildElement("RoomIndex").addTextNode("${1}")

        }

        if (hotelBookingVO.adult1FNameRoom3) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.adult1FNameRoom3)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.adult1MNameRoom3)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.adult1LNameRoom3)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Adult")
            wsGuest.addChildElement("RoomIndex").addTextNode("${2}")
        }
        if (hotelBookingVO.adult2FNameRoom3) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.adult2FNameRoom3)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.adult2MNameRoom3)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.adult2LNameRoom3)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Adult")
            wsGuest.addChildElement("RoomIndex").addTextNode("${2}")

        }
        if (hotelBookingVO.adult3FNameRoom3) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.adult3FNameRoom3)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.adult3MNameRoom3)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.adult3LNameRoom3)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Adult")
            wsGuest.addChildElement("RoomIndex").addTextNode("${2}")

        }
        if (hotelBookingVO.adult4FNameRoom3) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.adult4FNameRoom3)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.adult4MNameRoom3)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.adult4LNameRoom3)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Adult")
            wsGuest.addChildElement("RoomIndex").addTextNode("${2}")

        }
        if (hotelBookingVO.adult5FNameRoom3) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.adult5FNameRoom3)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.adult5MNameRoom3)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.adult5LNameRoom3)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Adult")
            wsGuest.addChildElement("RoomIndex").addTextNode("${2}")

        }

        if (hotelBookingVO.child1Room3FName) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.child1Room3FName)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.child1Room3MName)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.child1Room3LName)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Child")
            wsGuest.addChildElement("RoomIndex").addTextNode("${2}")

        }
        if (hotelBookingVO.child2Room3FName) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.child2Room3FName)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.child2Room3MName)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.child2Room3LName)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Child")
            wsGuest.addChildElement("RoomIndex").addTextNode("${2}")

        }
        if (hotelBookingVO.child3Room3FName) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.child3Room3FName)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.child3Room3MName)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.child3Room3LName)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Child")
            wsGuest.addChildElement("RoomIndex").addTextNode("${2}")

        }
        if (hotelBookingVO.child4Room3FName) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.child4Room3FName)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.child4Room3MName)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.child4Room3LName)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Child")
            wsGuest.addChildElement("RoomIndex").addTextNode("${2}")
        }


        if (hotelBookingVO.adult1FNameRoom4) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.adult1FNameRoom4)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.adult1MNameRoom4)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.adult1LNameRoom4)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Adult")
            wsGuest.addChildElement("RoomIndex").addTextNode("${3}")
        }
        if (hotelBookingVO.adult2FNameRoom4) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.adult2FNameRoom4)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.adult2MNameRoom4)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.adult2LNameRoom4)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Adult")
            wsGuest.addChildElement("RoomIndex").addTextNode("${3}")

        }
        if (hotelBookingVO.adult3FNameRoom4) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.adult3FNameRoom4)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.adult3MNameRoom4)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.adult3LNameRoom4)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Adult")
            wsGuest.addChildElement("RoomIndex").addTextNode("${3}")

        }
        if (hotelBookingVO.adult4FNameRoom4) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.adult4FNameRoom4)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.adult4MNameRoom4)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.adult4LNameRoom4)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Adult")
            wsGuest.addChildElement("RoomIndex").addTextNode("${3}")

        }
        if (hotelBookingVO.adult5FNameRoom4) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.adult5FNameRoom4)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.adult5MNameRoom4)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.adult5LNameRoom4)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Adult")
            wsGuest.addChildElement("RoomIndex").addTextNode("${3}")

        }

        if (hotelBookingVO.child1Room4FName) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.child1Room4FName)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.child1Room4MName)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.child1Room4LName)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Child")
            wsGuest.addChildElement("RoomIndex").addTextNode("${3}")

        }
        if (hotelBookingVO.child2Room4FName) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.child2Room4FName)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.child2Room4MName)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.child2Room4LName)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Child")
            wsGuest.addChildElement("RoomIndex").addTextNode("${3}")

        }
        if (hotelBookingVO.child3Room4FName) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.child3Room4FName)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.child3Room4MName)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.child3Room4LName)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Child")
            wsGuest.addChildElement("RoomIndex").addTextNode("${3}")

        }
        if (hotelBookingVO.child4Room4FName) {
            def wsGuest = guest.addChildElement("WSGuest")
            wsGuest.addChildElement("Title").addTextNode(hotelBookingVO.child4Room4FName)
            wsGuest.addChildElement("FirstName").addTextNode(hotelBookingVO.child4Room4MName)
            wsGuest.addChildElement("MiddleName").addTextNode("")
            wsGuest.addChildElement("LastName").addTextNode(hotelBookingVO.child4Room4LName)
            wsGuest.addChildElement("LeadGuest").addTextNode("false")
            wsGuest.addChildElement("GuestType").addTextNode("Child")
            wsGuest.addChildElement("RoomIndex").addTextNode("${3}")

        }

        reqElementTag.addChildElement("SessionId").addTextNode(hotelBookingVO.sessionId)
        reqElementTag.addChildElement("FlightInfo").addTextNode("")
        reqElementTag.addChildElement("SpecialRequest").addTextNode("no")

        def paymentInfo = reqElementTag.addChildElement("PaymentInfo")
        paymentInfo.addChildElement("PaymentId").addTextNode("0")
        paymentInfo.addChildElement("Amount").addTextNode("${roomDetailVO.hotelRateVO.totalRate}")
        paymentInfo.addChildElement("IPAddress").addTextNode("127.0.0.1")
        paymentInfo.addChildElement("TrackId").addTextNode("0")
        paymentInfo.addChildElement("PaymentGateway").addTextNode("APICustomer")
        paymentInfo.addChildElement("PaymentModeType").addTextNode("Deposited")

        reqElementTag.addChildElement("NoOfRooms").addTextNode("${hotelBookingVO.noOfRoom1}")
        reqElementTag.addChildElement("Index").addTextNode("${hotelVO.hotelIndex}")
        reqElementTag.addChildElement("HotelCode").addTextNode("${hotelVO.hotelId}")
        reqElementTag.addChildElement("HotelName").addTextNode("${hotelVO.hotelName}")
        def roomDetails = reqElementTag.addChildElement("RoomDetails")
        (1..(hotelBookingVO.noOfRoom1 as Integer)).each {

            def wsRoomDetails = roomDetails.addChildElement("WSHotelRoomsDetails")
            wsRoomDetails.addChildElement("Index").addTextNode("${roomDetailVO.roomIndex}")
            def rate = wsRoomDetails.addChildElement("Rate")
            rate.addChildElement("TotalRate").addTextNode("${roomDetailVO.hotelRateVO.totalRate}")
            rate.addChildElement("TotalTax").addTextNode("${roomDetailVO.hotelRateVO.totalTax}")
            rate.addChildElement("Currency").addTextNode("${roomDetailVO.hotelRateVO.currency}")
            rate.addChildElement("AgentMarkUp").addTextNode("0.000")
            rate.addChildElement("AgentCommission").addTextNode("${0}")

            def roomRate = wsRoomDetails.addChildElement("RoomRate")

            def daysRate = roomRate.addChildElement("DayRates")
            roomDetailVO?.hotelRoomRateVO?.dayRates?.each {
                def wsDayRate = daysRate?.addChildElement("WSDayRates")
                wsDayRate?.addChildElement("Days")?.addTextNode("${it?.dayRate}")
                wsDayRate?.addChildElement("BaseFare")?.addTextNode("${it?.baseFare}")
            }
            roomRate.addChildElement("ExtraGuestCharges").addTextNode("${roomDetailVO?.hotelRoomRateVO?.extraGuestCharges ? roomDetailVO.hotelRoomRateVO.extraGuestCharges : 0}")
            roomRate.addChildElement("DisCount")?.addTextNode("${roomDetailVO?.hotelRoomRateVO?.discount ? roomDetailVO?.hotelRoomRateVO?.discount : 0}")
            roomRate.addChildElement("OtherCharges")?.addTextNode("${roomDetailVO?.hotelRoomRateVO?.otherCharges ? roomDetailVO?.hotelRoomRateVO?.otherCharges : 0}")
            roomRate.addChildElement("ServiceTax").addTextNode("${roomDetailVO?.hotelRoomRateVO?.serviceTax ? roomDetailVO?.hotelRoomRateVO?.serviceTax : 0}")
            roomRate.addChildElement("TotalRoomRate").addTextNode("${roomDetailVO?.hotelRoomRateVO?.totalRoomRate ? roomDetailVO?.hotelRoomRateVO?.totalRoomRate : 0}")
            roomRate.addChildElement("TotalRoomTax").addTextNode("${roomDetailVO?.hotelRoomRateVO?.totalRoomTax ? roomDetailVO?.hotelRoomRateVO?.totalRoomTax : 0}")

            def amenities = wsRoomDetails.addChildElement("Amenities")
            List<String> amen = []
            String tu = ""
            if (roomDetailVO.amenities != []) {
                tu = roomDetailVO?.amenities?.first()?.name
            }
            amenities.addChildElement("string").addTextNode("${tu}")

            println '======MaxADULT===' + roomDetailVO?.hotelWsOccupancyVO?.baseFare
            def occupancy = wsRoomDetails.addChildElement("Occupancy")
            occupancy.addChildElement("MaxAdult").addTextNode("${roomDetailVO?.hotelWsOccupancyVO?.maxAdult ? roomDetailVO?.hotelWsOccupancyVO?.maxAdult : 0}")
            occupancy.addChildElement("MaxChild").addTextNode("${roomDetailVO?.hotelWsOccupancyVO?.maxChild ? roomDetailVO?.hotelWsOccupancyVO?.maxChild : 0}")
            occupancy.addChildElement("MaxInfant").addTextNode("${roomDetailVO?.hotelWsOccupancyVO?.maxInfant ? roomDetailVO?.hotelWsOccupancyVO?.maxInfant : 0}")
            occupancy.addChildElement("MaxGuest").addTextNode("${roomDetailVO?.hotelWsOccupancyVO?.maxGuest ? roomDetailVO?.hotelWsOccupancyVO?.maxGuest : 0}")
            occupancy.addChildElement("BaseAdult").addTextNode("${roomDetailVO?.hotelWsOccupancyVO?.baseAdult ? roomDetailVO?.hotelWsOccupancyVO?.baseAdult : 0}")
            occupancy.addChildElement("BaseChild").addTextNode("${roomDetailVO?.hotelWsOccupancyVO?.baseChild ? roomDetailVO?.hotelWsOccupancyVO?.baseChild : 0}")

            wsRoomDetails.addChildElement("RoomTypeCode").addTextNode("${roomDetailVO.roomTypeCode}")
            wsRoomDetails.addChildElement("RoomTypeName").addTextNode("${roomDetailVO.roomTypeName}")
            wsRoomDetails.addChildElement("RatePlanCode").addTextNode("${roomDetailVO.ratePlanCOde}")
            wsRoomDetails.addChildElement("SequenceNo").addTextNode("${roomDetailVO.roomIndex}")
            wsRoomDetails.addChildElement("PromotionMessage").addTextNode("")
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream()
        soapMsg.writeTo(outputStream)

        String requestNew = "<?xml version='1.0' encoding='utf-16'?>" + outputStream.toString()

        println '---=requestNew ==========' + requestNew

//        def client = new SOAPClient("http://api.tektravels.com/tbohotelapi_v6/hotelservice.asmx")
        def client = new SOAPClient("http://airapi.travelboutiqueonline.com/tbohotelapi_v6/hotelservice.asmx")

        SOAPResponse response = client.send(requestNew)

        println '---=response.text ==========' + response.text

        def ref = new XmlSlurper().parseText(response.text)
        TboBookResponseVO bookResponseVO = new TboBookResponseVO()

        bookResponseVO.bookingId = ref.children().GetHotelBookingResponse.GetHotelBookingResult.BookingDetail.BookingId

        def bookRefId = ref.children().BookResponse.BookResult.BookingId
        println '=========BookRefId==========' + bookRefId
        session["bookingDescription"] = ref.children().GetHotelBookingResponse.GetHotelBookingResult.BookingDetail.Description
        return bookRefId

    }

    Boolean isEnabled = true
    ServiceType serviceType = ServiceType.TBO

}
