package com.travelex.hotelFunctionality

import grails.validation.Validateable

@Validateable
class TboHotelBookingVO {

    String sessionId
    String hotelIndex
    String roomId

    String noOfRoom1
    String child
    String childInRoomTwo
    String childInRoomThree
    String childInRoomFour
    String adult
    String peopleInRoomTwo
    String peopleInRoomThree
    String peopleInRoomFour
    String checkOut
    String checkIn

    String adult1FNameRoom1
    String adult1MNameRoom1
    String adult1LNameRoom1
    String adult1EmailRoom1
    String adult1CityRoom1
    String adult1CityCode1
    String adult1PhoneRoom1
    String adult1CountryRoom1
    String adult1CountryCode1
    String adult1StateRoom1
    String adult1AgeRoom1
    String adult1AddressOneRoom1
    String adult1AddressTwoRoom1
    String adult1ZipRoom1

    String adult2FNameRoom1
    String adult2MNameRoom1
    String adult2LNameRoom1

    String adult3FNameRoom1
    String adult3MNameRoom1
    String adult3LNameRoom1

    String adult4FNameRoom1
    String adult4MNameRoom1
    String adult4LNameRoom1

    String adult5FNameRoom1
    String adult5MNameRoom1
    String adult5LNameRoom1

    String child1Room1FName
    String child1Room1MName
    String child1Room1LName

    String child2Room1FName
    String child2Room1MName
    String child2Room1LName

    String child3Room1FName
    String child3Room1MName
    String child3Room1LName

    String child4Room1FName
    String child4Room1MName
    String child4Room1LName

    String adult1FNameRoom2
    String adult1MNameRoom2
    String adult1LNameRoom2

    String adult2FNameRoom2
    String adult2MNameRoom2
    String adult2LNameRoom2

    String adult3FNameRoom2
    String adult3MNameRoom2
    String adult3LNameRoom2

    String adult4FNameRoom2
    String adult4MNameRoom2
    String adult4LNameRoom2

    String adult5FNameRoom2
    String adult5MNameRoom2
    String adult5LNameRoom2

    String child1Room2FName
    String child1Room2MName
    String child1Room2LName

    String child2Room2FName
    String child2Room2MName
    String child2Room2LName

    String child3Room2FName
    String child3Room2MName
    String child3Room2LName

    String child4Room2FName
    String child4Room2MName
    String child4Room2LName


    String adult1FNameRoom3
    String adult1MNameRoom3
    String adult1LNameRoom3

    String adult2FNameRoom3
    String adult2MNameRoom3
    String adult2LNameRoom3

    String adult3FNameRoom3
    String adult3MNameRoom3
    String adult3LNameRoom3

    String adult4FNameRoom3
    String adult4MNameRoom3
    String adult4LNameRoom3

    String adult5FNameRoom3
    String adult5MNameRoom3
    String adult5LNameRoom3

    String child1Room3FName
    String child1Room3MName
    String child1Room3LName

    String child2Room3FName
    String child2Room3MName
    String child2Room3LName

    String child3Room3FName
    String child3Room3MName
    String child3Room3LName

    String child4Room3FName
    String child4Room3MName
    String child4Room3LName


    String adult1FNameRoom4
    String adult1MNameRoom4
    String adult1LNameRoom4

    String adult2FNameRoom4
    String adult2MNameRoom4
    String adult2LNameRoom4

    String adult3FNameRoom4
    String adult3MNameRoom4
    String adult3LNameRoom4

    String adult4FNameRoom4
    String adult4MNameRoom4
    String adult4LNameRoom4

    String adult5FNameRoom4
    String adult5MNameRoom4
    String adult5LNameRoom4

    String child1Room4FName
    String child1Room4MName
    String child1Room4LName

    String child2Room4FName
    String child2Room4MName
    String child2Room4LName

    String child3Room4FName
    String child3Room4MName
    String child3Room4LName

    String child4Room4FName
    String child4Room4MName
    String child4Room4LName


    String totalAmmount
    String countryName


    static constraints = {

        adult1FNameRoom1(nullable: false, blank: false)
        adult1LNameRoom1(nullable: false, blank: false)
        adult1EmailRoom1(nullable: false, blank: false)
        adult1PhoneRoom1(nullable: false, blank: false)

    }


}
