package com.travelex.hotelFunctionality

import com.travelex.TboApi.CityFromTBO
import grails.validation.Validateable

@Validateable
class HotelSearchParamsVO {

    String place
    String checkIn
    String checkOut
    String range
    String noOfRooms
    String adult
    String children
    String firstChildAgeRoomOne
    String secondChildAgeRoomOne
    String peopleInRoomTwo
    String childInRoomTwo
    String firstChildAgeRoomTwo
    String secondChildAgeRoomTwo
    String peopleInRoomThree
    String childInRoomThree
    String firstChildAgeRoomThree
    String secondChildAgeRoomThree
    String peopleInRoomFour
    String childInRoomFour
    String firstChildAgeRoomFour
    String secondChildAgeRoomFour

    static constraints = {

        place(nullable: false, blank: false)
        checkIn(nullable: false, blank: false)
        checkOut(nullable: false, blank: false)
        noOfRooms(nullable: false, blank: false)

    }


    void passParamsInMethod(Map map) {

        String mnPrice = map.priceRangeMin
        String mxPrice = map.priceRangeMax
        Integer minPrice = mnPrice ? mnPrice?.tokenize("INR")?.first() as Integer : 200
        Integer maxPrice = mxPrice ? mxPrice?.tokenize("INR")?.first() as Integer : 500000
        this.checkIn = map.checkIn
        this.checkOut = map.checkOut
        this.range = maxPrice
        this.noOfRooms = map.noOfRooms
        this.adult = map.adult
        this.children = map.children
        this.firstChildAgeRoomOne = map.firstChildAgeRoomOne
        this.secondChildAgeRoomOne = map.secondChildAgeRoomOne
        this.peopleInRoomTwo = map.peopleInRoomTwo
        this.childInRoomTwo = map.childInRoomTwo
        this.firstChildAgeRoomTwo = map.firstChildAgeRoomTwo
        this.secondChildAgeRoomTwo = map.secondChildAgeRoomTwo
        this.peopleInRoomThree = map.peopleInRoomThree
        this.childInRoomThree = map.childInRoomThree
        this.firstChildAgeRoomThree = map.firstChildAgeRoomThree
        this.secondChildAgeRoomThree = map.secondChildAgeRoomThree
        this.peopleInRoomFour = map.peopleInRoomFour
        this.childInRoomFour = map.childInRoomFour
        this.firstChildAgeRoomFour = map.firstChildAgeRoomFour
        this.secondChildAgeRoomFour = map.secondChildAgeRoomFour
    }

}
