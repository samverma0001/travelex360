package com.travelex.hotelFunctionality


class HotelDetailVO {
    String hotelId
    String hotelInstance
    String sessionId
    String hotelIndex
    String supplierId
    String price
    String noOfRooms
    String adult
    String child
    String checkIn
    String checkOut
    String peopleInRoomTwo
    String peopleInRoomThree
    String peopleInRoomFour
    String childAgeRoomOne
    String childAgeRoomTwo
    String childAgeRoomThree
    String childAgeRoomFour

    String childInRoomTwo
    String childInRoomThree
    String childInRoomFour
}
