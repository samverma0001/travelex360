package com.travelex.flightFunctionality


class FareBreakDown {
    String passengerType
    String passengerCount
    String baseFare
    String tax
    String airlineTransFee
    String additionalTxnFee
    String fuelSurcharge
    String agentServiceCharge
    String agentConvienceCharges
}
