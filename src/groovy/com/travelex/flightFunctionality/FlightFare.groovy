package com.travelex.flightFunctionality


class FlightFare {
    String baseFare
    String tax
    String serviceTax
    String additionalTxnFee
    String agentCommission
    String tdsOnCommission
    String incentiveEarned
    String tdsOnIncentive
    String pLBEarned
    String tdsOnPLB
    String publishedPrice
    String airTransFee
    String currency
    String discount

    String otherCharges
    String fuelSurcharge
    String transactionFee
    String reverseHandlingCharge
    String offeredFare
    String agentServiceCharge
    String agentConvienceCharges


}
