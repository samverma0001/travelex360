package com.travelex.flightFunctionality


class FlightSegmentVO {
    String segmentIndicator
    String airLineCode
    String airLineName
    String airLineRemark
    String flightNumber
    String fareClass

    String originAirPortCode
    String originAirPortName
    String originTerminal
    String originCityCode
    String originCityName
    String originCountryCode
    String originCountryName

    String destinationAirPortCode
    String destinationAirPortName
    String destinationTerminal
    String destinationCityCode
    String destinationCityName
    String destinationCountryCode
    String destinationCountryName

    String departureTime
    String arrivalTime
    String eTicketEligible
    String duration
    String stops
    String craftType
    String status
    String operatingCarrier
}
