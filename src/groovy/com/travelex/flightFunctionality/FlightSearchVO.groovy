package com.travelex.flightFunctionality


class FlightSearchVO {
    String journeyType
    String journeyDate
    String journeyReturnDate
    String flightFromDom
    String flightToDom
    String flightFromInter
    String flightToInter
    String cabinClass
    String location
    String adult
    String children
    String infant
    String preferredCarrier

}
