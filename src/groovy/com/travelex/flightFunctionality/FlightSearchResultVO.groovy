package com.travelex.flightFunctionality

class FlightSearchResultVO {

    String sessionId
    Boolean isDomestic
    Boolean isRoundTrip
    Boolean isLcc
    Boolean isRefundable
    FlightFare flightFare
    List<FlightFareRule> flightFareRule = []
    List<FareBreakDown> fareBreakDown = []
    List<FlightSegmentVO> flightSegmentVO = []
    String tripIndicator
    String origin
    String destination
    String ibDuration
    String obDuration
    String promotionalPlan
    String segmentKey


}
