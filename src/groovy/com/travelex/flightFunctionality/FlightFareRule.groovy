package com.travelex.flightFunctionality


class FlightFareRule {
    String origin
    String destination
    String airline
    String fareRestriction
    String fareBasisCode
    String departureDate
    String returnDate
    String source
}
