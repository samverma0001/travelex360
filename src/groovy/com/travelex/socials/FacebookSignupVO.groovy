package com.travelex.socials

class FacebookSignupVO {

    String name
    String email
    String firstName
    String lastName
    String profileId
    String facebookUserName
    String gender
    String accessToken
    String defaultPassword = UUID.randomUUID().toString()
    String link
    String updateTime
    String ageRange
    String birthday
    String hometown
    String currentLocation

    FacebookSignupVO() {

    }

    FacebookSignupVO(def facebookData) {
        this.name = facebookData.name
        this.link = facebookData.link
        this.email = facebookData.email
        this.firstName = facebookData.first_name
        this.lastName = facebookData.last_name
        this.profileId = facebookData.id
        this.facebookUserName = facebookData.username
        this.gender = facebookData.gender
        this.updateTime = facebookData.updated_time
        this.ageRange = facebookData?.age_range
        this.birthday = facebookData?.birthday
        this.hometown = facebookData?.hometown?.name
        this.currentLocation = facebookData?.location?.name
    }
}
