package com.travelex.socials.hotel

import com.travelex.Hotel.HotelCategory

class HotelVO {

    String hotelId
    String name
    String category
    String location
    String locality
    String state
    String address
    String currency
//    List<HotelCategoryVO> category=[]
    List<GeoDataVO> geoData = []
    List<ContactVO> contacts = []
    List<HotelMediaVO> hotelPictures = []
    List<HotelFacilityVO> facilities = []
    String description
    List<RoomTypeVO> roomType = []
    List<RoomCategoriesNameVO> roomCategory = []
//    List<RoomFacilityVO> roomFacilities = []
//    List<RoomMediaVO> roomPictures = []
     String categoryName
    static transients = ['getMediaInfo', 'getGeoLongitude','getRoomMediaInfo']

    HotelVO(){

    }

    HotelVO(def hotelData) {
        this.hotelId = hotelData.children().id.text()
        this.name = hotelData.children().name.text()
        this.category = hotelData.children().category.text()
        this.location = hotelData.children().location.text()
        this.locality = hotelData.children().locality.text()
        this.state=hotelData.children().state.text()
        this.address = hotelData.children().address.text()
        this.description = hotelData.children().description.text()
        this.categoryName=HotelCategory.findByCategoryId(hotelData.children().category.text())?.name
    }

    public GeoDataVO getGeoLongitude() {
        return geoData.first()
    }

    public HotelMediaVO getMediaInfo() {
        return hotelPictures.first()
    }
    public RoomMediaVO getRoomMediaInfo() {
        return roomCategory.first()
    }

}
