package com.travelex.socials.hotel


class RoomMediaVO {
    String roomMediaTitle
    String roomMediaSrc


    RoomMediaVO(){

    }

    RoomMediaVO(def roomMedia){
         this.roomMediaSrc=roomMedia.attribute("src")
        this.roomMediaTitle=roomMedia.attribute("title")
    }

}
