package com.travelex.socials.hotel


class RoomTypeVO {

    String roomTypeId
    String roomTypeName

    RoomTypeVO(){

    }

    RoomTypeVO(def roomTypeData) {
        this.roomTypeId = roomTypeData.attribute("id")
        this.roomTypeName = roomTypeData.attribute("name")
    }
}
