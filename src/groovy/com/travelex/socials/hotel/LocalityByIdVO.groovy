package com.travelex.socials.hotel


class LocalityByIdVO {
    String id
    String name

    LocalityByIdVO(){

    }

    LocalityByIdVO(def localtyData){
        this.id=localtyData.attribute("id")
        this.name=localtyData.locality.text()
    }
}
