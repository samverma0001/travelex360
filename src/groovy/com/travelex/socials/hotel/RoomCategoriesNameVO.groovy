package com.travelex.socials.hotel


class RoomCategoriesNameVO {
    String roomCategoryId
    String name
    String description

    List<RoomFacilityVO> roomFacilities = []
    List<RoomMediaVO> roomPictures = []

    static transients = ['getRoomMediaInfo']

    RoomCategoriesNameVO(){

    }
    RoomCategoriesNameVO(def category){
      this.roomCategoryId=category.attribute("id")
        this.name=category.attribute("name")
    }


    public RoomMediaVO getRoomMedia() {
        return roomPictures.first()
    }
}
