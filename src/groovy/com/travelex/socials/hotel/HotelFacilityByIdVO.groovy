package com.travelex.socials.hotel


class HotelFacilityByIdVO {
    String facilityId
    String facilityName

    HotelFacilityByIdVO(){

    }

    HotelFacilityByIdVO(def hotelFacilityData) {
        this.facilityId = hotelFacilityData.attribute("id")
        this.facilityName = hotelFacilityData.facility.text()
    }
}
