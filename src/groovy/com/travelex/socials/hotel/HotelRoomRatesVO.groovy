package com.travelex.socials.hotel

import com.travelex.socials.searchHotel.SearchHotelMealPlanVO
import com.travelex.socials.searchHotel.SearchHotelRefundPolicyVO
import com.travelex.socials.searchHotel.SearchRoomRateTypeVO

class HotelRoomRatesVO {
    String availCount
    String roomCategoryId
    String roomTypeId
    String roomType
    String roomIndex

    List<SearchRoomRateTypeVO> roomRateType = []
    List<SearchHotelMealPlanVO> mealPlan = []
    List<SearchHotelRefundPolicyVO> refundPolicy = []

    static transients = ['getRoomMediaInfo']

    HotelRoomRatesVO(){

    }

    HotelRoomRatesVO(def roomRateDate){
        this.availCount=roomRateDate.attribute("availCount")
        this.roomCategoryId=roomRateDate.attribute("roomCategoryId")
        this.roomTypeId=roomRateDate.attribute("roomTypeId")
    }



}
