package com.travelex.socials.hotel


class ContactVO {
    String name
    String email
    String phone
    String mobile

    ContactVO(){

    }
    ContactVO(def contact){
        this.name=contact.name.text()
        this.email=contact.email.text()
        this.phone=contact.phone.text()
        this.mobile=contact.mobile.text()

    }

}
