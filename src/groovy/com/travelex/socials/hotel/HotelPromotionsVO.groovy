package com.travelex.socials.hotel


class HotelPromotionsVO {
    String promotionCode
    String promotionDiscount
    String promotionDiscountType
    String promotionName

    HotelPromotionsVO(){

    }

    HotelPromotionsVO(def promotionData){

    this.promotionCode=promotionData.attribute("code")
    this.promotionDiscount=promotionData.attribute("discount")
    this.promotionDiscountType=promotionData.attribute("discountType")
    this.promotionName=promotionData.text()
}
}