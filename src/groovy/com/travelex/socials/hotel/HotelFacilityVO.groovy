package com.travelex.socials.hotel


class HotelFacilityVO {

    String facilityId
    String facilityName


    HotelFacilityVO(){

    }
    HotelFacilityVO(def hotelFacilityData) {
        this.facilityId = hotelFacilityData.attribute("id")
        this.facilityName = hotelFacilityData.name.text()
    }
}
