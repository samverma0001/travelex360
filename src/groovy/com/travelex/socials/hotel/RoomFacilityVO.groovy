package com.travelex.socials.hotel


class RoomFacilityVO {

    String roomFacilityId
    String roomFacilityName

    RoomFacilityVO(){

    }

    RoomFacilityVO(def roomFacilityData) {
        this.roomFacilityId = roomFacilityData.attribute("id")
        this.roomFacilityName = roomFacilityData.name.text()
    }
}
