package com.travelex.socials.hotel

class HotelMediaVO {
    String title
    String src

    HotelMediaVO(){

    }
    HotelMediaVO(def media){
       this.src=media.attribute("src")
        this.title=media.attribute("title")
    }

}
