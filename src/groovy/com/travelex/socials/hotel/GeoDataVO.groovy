package com.travelex.socials.hotel

class GeoDataVO {
    String longitude
    String latitude


    GeoDataVO(){

    }

    GeoDataVO(def geoData) {
        this.longitude = geoData.geoData['longitude']
        this.latitude = geoData.geoData['latitude']
    }

}
