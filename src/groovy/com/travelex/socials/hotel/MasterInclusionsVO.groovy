package com.travelex.socials.hotel

class MasterInclusionsVO {
    String inclusionId
    String inclusionName

    MasterInclusionsVO(){

    }

    MasterInclusionsVO(def inclusionData){

        this.inclusionId=inclusionData.attribute("id")
        this.inclusionName=inclusionData.text()

    }
}
