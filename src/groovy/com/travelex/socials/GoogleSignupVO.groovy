package com.travelex.socials

class GoogleSignupVO {

    String name
    String email
    String firstName
    String lastName
    String profileId
    String gender
    String accessToken
    String accessSecret
    Long followers
    Long followings
    String defaultPassword = UUID.randomUUID().toString()

    GoogleSignupVO() {

    }
    GoogleSignupVO(def googleData) {
        this.name = googleData?.name
        this.email = googleData?.email
        this.firstName = googleData?.given_name
        this.lastName = googleData?.family_name
        this.profileId = googleData?.id
        this.gender = googleData?.gender
    }
}
