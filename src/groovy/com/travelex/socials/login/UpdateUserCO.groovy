package com.travelex.socials.login

import grails.validation.Validateable

@Validateable
class UpdateUserCO {

    String firstName
    String lastName
    String email
    String mobNo

    static constraints = {
        firstName(nullable: false, blank: false)
        lastName(nullable: false, blank: false)
        email(nullable: false, blank: false)
        mobNo(nullable: false, blank: false)
    }


}
