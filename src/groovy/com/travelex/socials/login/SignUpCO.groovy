package com.travelex.socials.login

import com.travelex.User.User
import org.apache.commons.lang.RandomStringUtils
import org.codehaus.groovy.grails.validation.Validateable

import java.text.SimpleDateFormat

@Validateable
class SignUpCO {

    String firstName
    String lastName
    String username
    String password
    String confirmPassword
    String mobNo
    Boolean accountExpired
    Boolean accountLocked
    Boolean enabled
    Boolean hasVerifiedEmail
    Boolean passwordExpired
    String selectRole


    static constraints = {
        username(email: true, nullable: false, blank: false)

        password(nullable: false, blank: false)
        confirmPassword(nullable: false, blank: false, validator: { val, obj ->
            if (obj.password != val) {
                return "password does not match"
            }
        })
        firstName(nullable: false, blank: false)
        lastName(nullable: false, blank: false)
        mobNo(nullable: true, blank: true)
        accountExpired(nullable: true, blank: true)
        selectRole(nullable: false)
    }


    public static User createUserByAdmin(User newUser, SignUpCO signUpCO) {
        newUser.username = signUpCO.username
        newUser.password = signUpCO.password
        newUser.firstName = signUpCO.firstName
        newUser.lastName = signUpCO.lastName
        newUser.mobNo = signUpCO.mobNo
        newUser.enabled = signUpCO.enabled
        newUser.accountExpired = signUpCO.accountExpired
        newUser.accountLocked = signUpCO.accountLocked
        newUser.passwordExpired = signUpCO.passwordExpired
        newUser.hasVerifiedEmail = signUpCO.hasVerifiedEmail
        String randomAlphanumeric = RandomStringUtils.randomAlphanumeric(8)
        Date today = new Date()
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat('dd/MM/yyyy')
        String[] date = simpleDateFormat.format(today).split('/')
        newUser.coustId = "TR" + "CS" + randomAlphanumeric.toUpperCase() + "${date[0]}" + "${date[1]}" + "${date[2]}"
        return newUser
    }


    public static User updateUserByAdmin(User newUser, SignUpCO signUpCO) {
        newUser.password = signUpCO.password
        newUser.firstName = signUpCO.firstName
        newUser.lastName = signUpCO.lastName
        newUser.mobNo = signUpCO.mobNo
        newUser.enabled = signUpCO.enabled
        newUser.accountExpired = signUpCO.accountExpired
        newUser.accountLocked = signUpCO.accountLocked
        newUser.passwordExpired = signUpCO.passwordExpired
        newUser.hasVerifiedEmail = signUpCO.hasVerifiedEmail
        return newUser
    }


}
