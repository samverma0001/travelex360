package com.travelex.socials.login

import org.codehaus.groovy.grails.validation.Validateable

@Validateable
class SignUpForServiceProviderCO {
    String nameOfHotel
    String city
    String state
    String address
    String propertyType
    String nameOfRepresentee
    String designationOfRepresentee
    String officialEmail
    String website
    String mobNo

    static constraints = {
        nameOfHotel(nullable: false, blank: false)
        city(nullable: false, blank: false)
        state(nullable: false, blank: false)
        address(nullable: true, blank: false)
        propertyType(nullable: true, blank: false)
        nameOfRepresentee(nullable: true, blank: false)
        designationOfRepresentee(nullable: true, blank: false)
        officialEmail(nullable: true, blank: false)
        website(nullable: true, blank: false)
        mobNo(nullable: true, blank: false)

    }
}
