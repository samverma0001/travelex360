package com.travelex.socials.login

import org.codehaus.groovy.grails.validation.Validateable

@Validateable
class ResetPasswordCo {

    String currentPassword
    String newPassword
    String confirmPassword

    static constraints = {

        currentPassword(nullable: false, blank: false)
        newPassword(nullable: false, blank: false)
        confirmPassword(nullable: false, blank: false, validator: { val, obj ->
            if (obj.newPassword != val) {
                return "New password & Confirm password does not match"
            }
        })

    }

}
