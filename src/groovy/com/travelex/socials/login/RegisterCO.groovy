package com.travelex.socials.login

import com.travelex.User.User
import grails.validation.Validateable
import org.apache.commons.lang.RandomStringUtils

import java.text.SimpleDateFormat

/**
 * Created by sumit on 18/9/14.
 */
@Validateable
class RegisterCO {

    String firstName
    String lastName
    String username
    String password
    String confirmPassword
    String mobNo
    String termsCondition
    String captcha


    static constraints = {
        username(email: true, nullable: false, blank: false)
        password(nullable: false, blank: false)
        confirmPassword(nullable: false, blank: false, validator: { val, obj ->
            if (obj.password != val) {
                return "password does not match"
            }
        })
        firstName(nullable: false, blank: false)
        lastName(nullable: false, blank: false)
        mobNo(nullable: false, blank: false)
        termsCondition(nullable: false, blank: false)
        captcha(nullable: false, blank: false)
    }

    public static void createUser(User user, RegisterCO registerCO) {

        user.firstName = registerCO.firstName
        user.lastName = registerCO.lastName
        user.username = registerCO.username
        user.password = registerCO.password
        user.mobNo = registerCO.mobNo

        String randomAlphanumeric = RandomStringUtils.randomAlphanumeric(8)
        Date today = new Date()
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat('dd/MM/yyyy')
        String[] date = simpleDateFormat.format(today).split('/')
        user.coustId = "TR" + "CS" + randomAlphanumeric.toUpperCase() + "${date[0]}" + "${date[1]}" + "${date[2]}"

    }


}
