package com.travelex.socials.login

import com.travelex.User.User

import org.codehaus.groovy.grails.validation.Validateable

@Validateable
class SignInCo {
    String username
    String password
//    Boolean remember


    static constraints = {
        username(nullable: false, blank: false, validator: { val, obj ->
            if (User.findByUsername(obj.username)) {
                return
            }
            else{
                return "username doesnot match"
            }
        })

        password(nullable: false, blank: false, )
        //        remember(nullable: true, blank: true)
    }

}
