package com.travelex.socials.searchHotel


class SearchHotelRoomCategoriesVO {
    String roomCategoryName
    String roomCategoryId

    SearchHotelRoomCategoriesVO() {

    }

    SearchHotelRoomCategoriesVO(def categoryData) {
        this.roomCategoryName = categoryData.attribute("name")
        this.roomCategoryId = categoryData.attribute("id")
    }
}
