package com.travelex.socials.searchHotel

import com.travelex.Hotel.City
import com.travelex.Hotel.HotelCategory
import com.travelex.socials.hotel.HotelPromotionsVO
import com.travelex.socials.hotel.HotelRoomRatesVO
import com.travelex.socials.hotel.MasterInclusionsVO
import wslite.soap.SOAPClient
import wslite.soap.SOAPResponse

class SearchHotelVO {

    String hotelIndex
    String sessionId
    String hotelId
    String hotelName
    String hotelCategoryId
    String hotelLocationId
    String foreignerAllowed
    String hotelRating
    String hotelLocalityId
    String hotelPayAtHotel
    String hotelBookingFlow
    String currency
    String hotelPropertyType
    String category
    String location
    String locality
    List<TboSearchHotelRoomDetailVO> hotelRoomDetailVOArrayList = []
    List<HotelPromotionsVO> promotions = []
    List<MasterInclusionsVO> inclusions = []
    List<SearchHotelMediaListVO> searchHotelMedias = []
    List<SearchHotelRoomCategoriesVO> searchHotelRoomCategory = []
    List<SearchHotelRoomTypeVO> searchHotelRoomType = []
    String shortDescription
    List<SearchHotelFacilitiesVO> hotelFacility = []
    List<HotelRoomRatesVO> hotelRoomRatesVOs = []

    String supplierId

    static transients = ['getMediaInfo']

    SearchHotelVO() {

    }

    SearchHotelVO(def searchData) {

        this.hotelId = searchData.attribute("id")
        this.hotelName = searchData.attribute("name")
        this.hotelCategoryId = searchData.attribute("category")
        this.hotelLocationId = searchData.attribute("location")
        this.foreignerAllowed = searchData.attribute("foreigner_allowed")
        this.hotelRating = searchData.attribute("ratings")
        this.hotelLocalityId = searchData.attribute("locality")
        this.hotelPayAtHotel = searchData.attribute("payathotel")
        this.hotelBookingFlow = searchData.attribute("booking_flow")
        this.currency = searchData.attribute("currency")
        this.hotelPropertyType = searchData.attribute("property_type")
        this.category = HotelCategory.findByCategoryId(searchData.attribute("category"))?.name
        this.location = City.findByLocationId(searchData.attribute("location"))?.name
        this.shortDescription = searchData.shortDescription.text()
        this.supplierId = searchData.attribute("supplier_id")
    }
}
