package com.travelex.socials.searchHotel

class TboSearchHotelRoomDetailVO {
    String roomIndex
    String roomTypeCode
    String roomTypeName
    String ratePlanCOde
    TboSearchHotelRateVO hotelRateVO
    TboSearchHotelRoomRateVO hotelRoomRateVO
    List<TboRoomAminities> amenities=[]
    TboSearchHotelWsOccupancyVO hotelWsOccupancyVO

}
