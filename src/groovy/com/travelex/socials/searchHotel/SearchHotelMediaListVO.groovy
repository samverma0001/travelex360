package com.travelex.socials.searchHotel


class SearchHotelMediaListVO {
    String mediaSrc
    String mediaType
    String mediaTitle

    SearchHotelMediaListVO(){

    }

    SearchHotelMediaListVO(def mediaData){
        this.mediaSrc=mediaData.attribute("src")
        this.mediaType=mediaData.attribute("type")
        this.mediaTitle=mediaData.attribute("title")
    }
}
