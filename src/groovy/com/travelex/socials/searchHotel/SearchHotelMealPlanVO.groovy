package com.travelex.socials.searchHotel


class SearchHotelMealPlanVO {
    String code
    String mealStartDate
    String mealEndDate
    String mealPlan

    SearchHotelMealPlanVO() {

    }

    SearchHotelMealPlanVO(def mealPlan) {
        this.code = mealPlan.attribute("code")
        this.mealStartDate = mealPlan.attribute("start")
        this.mealEndDate = mealPlan.attribute("end")
        this.mealPlan = mealPlan.text()

    }

}
