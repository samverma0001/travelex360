package com.travelex.socials.searchHotel

class SearchHotelRefundPolicyVO {
    String dayMin
    String dayMax
    String deduction
    String unit


    SearchHotelRefundPolicyVO() {

    }

    SearchHotelRefundPolicyVO(def refundPolicy) {
        this.dayMin = refundPolicy.attribute("dayMin")
        this.dayMax = refundPolicy.attribute("dayMax")
        this.deduction = refundPolicy.attribute("deduction")
        this.unit = refundPolicy.attribute("unit")

    }
}
