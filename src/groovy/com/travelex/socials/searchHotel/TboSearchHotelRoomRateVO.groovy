package com.travelex.socials.searchHotel

class TboSearchHotelRoomRateVO {
    List<TboRateVO> dayRates = []
    String extraGuestCharges
    String discount
    String otherCharges
    String serviceTax
    String totalRoomRate
    String totalRoomTax
}
