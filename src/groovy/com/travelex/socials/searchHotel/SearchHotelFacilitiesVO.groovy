package com.travelex.socials.searchHotel


class SearchHotelFacilitiesVO {
    String facilityId

    SearchHotelFacilitiesVO(){

    }

    SearchHotelFacilitiesVO(def facility){
        this.facilityId=facility.attribute("id")
    }
}
