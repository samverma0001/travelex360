package com.travelex.socials.searchHotel


class SearchHotelRoomTypeVO {
    String roomTypeName
    String roomTypeId

    SearchHotelRoomTypeVO() {

    }

    SearchHotelRoomTypeVO(def roomTypeData) {
        this.roomTypeName = roomTypeData.attribute("name")
        this.roomTypeId = roomTypeData.attribute("id")

    }
}
