package com.travelex.socials.searchHotel


class SearchRoomRateTypeVO {
    String totalTax
    String roomRate
    String agentMarkUp

    SearchRoomRateTypeVO() {

    }

    SearchRoomRateTypeVO(def rateData) {
        this.totalTax = rateData.text()
        this.roomRate = rateData.text()
        this.agentMarkUp = rateData.text()

    }
}
