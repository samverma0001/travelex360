package com.travelex.socials.searchHotel

class TboSearchHotelWsOccupancyVO {
    String days
    String baseFare
    String maxAdult
    String maxChild
    String maxInfant
    String maxGuest
    String baseAdult
    String baseChild
}
