package com.travelex.socials


class LinkedInSignupVO {

    String name
    String email
    String firstName
    String lastName
    String profileId
    String gender
    String accessToken
    String accessSecret
    String location
    String summary
    String publicProfileUrl
    String dateOfBirth
    String defaultPassword = UUID.randomUUID().toString()

    LinkedInSignupVO() {

    }

    LinkedInSignupVO(def linkedInData) {
        this.name = linkedInData?.firstName + ' ' + linkedInData?.lastName
        this.email = linkedInData?.emailAddress
        this.firstName = linkedInData?.firstName
        this.lastName = linkedInData?.lastName
        this.profileId = linkedInData?.id
        this.gender = linkedInData?.gender
        this.location = linkedInData?.location?.name
        this.summary = linkedInData?.summary
        this.publicProfileUrl = linkedInData?.publicProfileUrl
        print '-------linkedInData?.publicProfileUrl---------------' + this.publicProfileUrl
        if (linkedInData?.dateOfBirth?.day && linkedInData?.dateOfBirth?.month && linkedInData?.dateOfBirth?.year) {
            this.dateOfBirth = linkedInData.dateOfBirth.month + "/" + linkedInData.dateOfBirth.day + "/" + linkedInData.dateOfBirth.year
        }
    }
}
