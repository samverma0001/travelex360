package com.travelex.socials.Enums

public enum HotelType {

//    RESORTS_AND_PLACES,
//    HERITAGES_HOTELS,
//    HOLIDAY_COTTAGE,
//    BUSINESS_HOTELS,
//    BED_AND_BREAKFAST,
//    MOTELS

    RESORTS_AND_PLACES("Resorts And Places"),
    HERITAGES_HOTELS("Heritage Hotels"),
    HOLIDAY_COTTAGE("Holiday Cottages"),
    BUSINESS_HOTELS("Bussiness Hotels"),
    BED_AND_BREAKFAST("Bed And Breakfast"),
    MOTELS("Motels")


    final String value

    HotelType(String value) {
        this.value = value
    }

    String toString() {
        return value
    }

    public String getKey() {
        return name()
    }

    public String getValue() {
        return value
    }

    public static List<HotelType> list() {

        return [RESORTS_AND_PLACES, HERITAGES_HOTELS, HOLIDAY_COTTAGE, BUSINESS_HOTELS,  BED_AND_BREAKFAST, MOTELS]
    }
}