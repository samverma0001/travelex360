package com.travelex.socials.Enums

public enum ActivityType {

//    EATING_EXOTIC ,
//    ADVENTURE_SPORTS,
//    ARTS_CRAFTS,
//    DISPLAYS_SHOWS,
//    BODY_WELLNESS,
//    SHOPPING
    EATING_EXOTIC ("Eating And Exotic"),
    ADVENTURE_SPORTS("Adventure Sports"),
    ARTS_CRAFTS("Arts And Crafts"),
    DISPLAYS_SHOWS("Display And Shows"),
    BODY_WELLNESS("Body And Wellness"),
    SHOPPING("Shopping")


    final String value

    ActivityType(String value) {
        this.value = value
    }

    String toString() {
        return value
    }

    public String getKey() {
        return name()
    }

    public String getValue() {
        return value
    }

    public static List<ActivityType> list() {

        return [EATING_EXOTIC ,ADVENTURE_SPORTS,  DISPLAYS_SHOWS, BODY_WELLNESS,  SHOPPING]
    }
}