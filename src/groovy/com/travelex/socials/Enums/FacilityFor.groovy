package com.travelex.socials.Enums

public enum FacilityFor {
    HOTEL ("Hotel"),
    HOTEL_ROOM("HtelRoom")


    final String value

    FacilityFor(String value) {
        this.value = value
    }

    String toString() {
        return value
    }

    public String getKey() {
        return name()
    }

    public String getValue() {
        return value
    }

    public static List<FacilityFor> list() {

        return [HOTEL, HOTEL_ROOM]
    }


}