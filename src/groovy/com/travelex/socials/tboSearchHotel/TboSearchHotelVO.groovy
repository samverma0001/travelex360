package com.travelex.socials.tboSearchHotel

class TboSearchHotelVO {
    String hotelCode
    String hotelName
    String hotelPicture
    String hotelDescription
    String hotelMap
    String hotelLocation
    String hotelAddress
    String hotelContactNo
    String hotelRating

    TboSearchHotelVO() {

    }

    TboSearchHotelVO(def searchData) {
        this.hotelCode = searchData.HotelInfo.HotelCode.text()
        this.hotelName = searchData.HotelInfo.HotelName.text()
        this.hotelPicture = searchData.HotelInfo.HotelPicture.text()
        this.hotelDescription = searchData.HotelInfo.HotelDescription.text()
        this.hotelMap = searchData.HotelInfo.HotelMap.text()
        this.hotelLocation = searchData.HotelInfo.HotelLocation.text()
        this.hotelAddress = searchData.HotelInfo.HotelAddress.text()
        this.hotelContactNo = searchData.HotelInfo.HotelContactNo.text()
        this.hotelRating = searchData.HotelInfo.Rating.text()
    }
}
