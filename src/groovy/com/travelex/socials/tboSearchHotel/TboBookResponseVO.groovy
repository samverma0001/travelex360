package com.travelex.socials.tboSearchHotel

class TboBookResponseVO {
    String bookingId
    String confirmationNo
    String referenceNo
    String status
    String statusCode
    String description
    String category
    String tripId

}
