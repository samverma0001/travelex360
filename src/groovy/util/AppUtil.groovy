package util

import grails.util.GrailsUtil
import org.codehaus.groovy.grails.web.context.ServletContextHolder
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsHttpSession
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsWebRequest
import org.springframework.web.context.request.RequestContextHolder

import java.text.SimpleDateFormat

class AppUtil {

    public static final String FILE_500 = '_500';
    public static final String FILE_50 = '_50';
    public static final String FILE_200 = '_200';

    public static String getStaticPath() {
        return ServletContextHolder.getServletContext().getRealPath("/")
    }


    public static String getStaticResourcesDirPath() {
//        GrailsWebRequest request = RequestContextHolder?.currentRequestAttributes()
//        print "--------------request is----------------------" + request
//        GrailsHttpSession session = request.session
//        print "--------------session is----------------------" + session
        String path
        if (GrailsUtil.developmentEnv) {
            print '----env is-----dev------' + GrailsUtil.developmentEnv
            path = ServletContextHolder.getServletContext().getRealPath("/")
            print '--------------paths is-----------------' + path
        } else {
            print '----env is-live----------' + GrailsUtil.developmentEnv
            path = "/home/ubuntu/staticImages"
            print '--------------paths is-----------------' + path
        }
        return path
    }

    public static Integer getHotelRating(String ratingInfo) {
        Integer rating
        if (ratingInfo == "OneStar") {
            rating = 1
        } else if (ratingInfo == "TwoStar") {
            rating = 2
        } else if (ratingInfo == "ThreeStar") {
            rating = 3
        } else if (ratingInfo == "FourStar") {
            rating = 4
        } else if (ratingInfo == "FiveStar") {
            rating = 5
        } else if (ratingInfo == "SixStar") {
            rating = 6
        } else if (ratingInfo == "SevenStar") {
            rating = 7
        } else if (ratingInfo == "0") {
            rating = 0
        } else if (ratingInfo == "1") {
            rating = 1
        } else if (ratingInfo == "2") {
            rating = 2
        } else if (ratingInfo == "3") {
            rating = 3
        } else if (ratingInfo == "4") {
            rating = 4
        } else if (ratingInfo == "5") {
            rating = 5
        } else if (ratingInfo == "6") {
            rating = 6
        } else if (ratingInfo == "7") {
            rating = 7
        }
        return rating
    }


    public static Date convertDateFromStringToDate(String incomingDate) {
        Date returnDate = new Date()
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat('MM/dd/yyyy')
        returnDate = simpleDateFormat.parse(incomingDate)
        returnDate.clearTime()
        return returnDate
    }


}
