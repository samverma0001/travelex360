package com.travelex.User



import grails.test.mixin.*
import spock.lang.*

@TestFor(PaymentInfoController)
@Mock(PaymentInfo)
class PaymentInfoControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.paymentInfoInstanceList
            model.paymentInfoInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.paymentInfoInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            def paymentInfo = new PaymentInfo()
            paymentInfo.validate()
            controller.save(paymentInfo)

        then:"The create view is rendered again with the correct model"
            model.paymentInfoInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            paymentInfo = new PaymentInfo(params)

            controller.save(paymentInfo)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/paymentInfo/show/1'
            controller.flash.message != null
            PaymentInfo.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def paymentInfo = new PaymentInfo(params)
            controller.show(paymentInfo)

        then:"A model is populated containing the domain instance"
            model.paymentInfoInstance == paymentInfo
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def paymentInfo = new PaymentInfo(params)
            controller.edit(paymentInfo)

        then:"A model is populated containing the domain instance"
            model.paymentInfoInstance == paymentInfo
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            controller.update(null)

        then:"A 404 error is returned"
            status == 404

        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def paymentInfo = new PaymentInfo()
            paymentInfo.validate()
            controller.update(paymentInfo)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.paymentInfoInstance == paymentInfo

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            paymentInfo = new PaymentInfo(params).save(flush: true)
            controller.update(paymentInfo)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/paymentInfo/show/$paymentInfo.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            controller.delete(null)

        then:"A 404 is returned"
            status == 404

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def paymentInfo = new PaymentInfo(params).save(flush: true)

        then:"It exists"
            PaymentInfo.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(paymentInfo)

        then:"The instance is deleted"
            PaymentInfo.count() == 0
            response.redirectedUrl == '/paymentInfo/index'
            flash.message != null
    }
}
