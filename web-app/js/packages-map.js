var map;	
var shape;
// var center = new google.maps.LatLng(11.001302, 76.986623);
var center = new google.maps.LatLng(27.067179, -20.192942);

function load() {
	var myMapOptions = {
  	zoom: 3,
		center: center,
		streetViewControl: false,
		styles: [{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]}],
		scaleControl: false,
		// mapTypeId: google.maps.MapTypeId.TERRAIN,
		    mapTypeControl: true,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
    }
	};
	map = new google.maps.Map(document.getElementById("map"),myMapOptions);
	
	var image = new google.maps.MarkerImage(
	  'images/image.png',
		new google.maps.Size(40,35),
		new google.maps.Point(0,0),
		new google.maps.Point(0,35)
	);

	var shadow = new google.maps.MarkerImage(
	  'images/shadow.png',
		new google.maps.Size(62,35),
		new google.maps.Point(0, 0),
		new google.maps.Point(0,35)
	);



//  ----------------- Marker 1

var marker1;
var myInfoWindowOptions;
var infoWindow;
var marker1Position = new google.maps.LatLng(38.820643, -9.254417);

	marker1 = new google.maps.Marker({
		draggable: false,
		raiseOnDrag: false,
		icon: image,
		shadow: shadow,
		shape: shape,
		map: map,
		position: marker1Position
	});
	myInfoWindowOptions = {
		content: '<div class="info-window-content"><h3 class="info-window-title">Portugal <small>Packages</small></h3><div><a href="#"><h4 class="package-title">Lisbon - <small>5 Days <span class="text-muted">(4 Nights)</span></small></h4></a><a href="#"><h4 class="package-title">Caldas da Rainha - <small>2 Days <span class="text-muted">(2 Nights)</span></small></h4></a><a href="#"><h4 class="package-title">Peniche - <small>10 Days <span class="text-muted">(9 Nights)</span></small></h4></a><a href="#"><h4 class="package-title">Lourinhã - <small>5 Days <span class="text-muted">(4 Nights)</span></small></h4></a><p><a href="#" class="btn btn-default">View All Packages</a></p></div></div>',
		maxWidth: 275
	};
	infoWindow = new google.maps.InfoWindow(myInfoWindowOptions);
	google.maps.event.addListener(marker1, 'click', function() {
		infoWindow.open(map,marker1);
	});

//  ----------------- Marker 2

var marker2;
var myInfoWindowOptions2;
var infoWindow2;
var marker2Position = new google.maps.LatLng(15.399649, 73.933247);

	marker2 = new google.maps.Marker({
		draggable: false,
		raiseOnDrag: false,
		icon: image,
		shadow: shadow,
		shape: shape,
		map: map,
		position: marker2Position
	});

	myInfoWindowOptions2 = {
		content: '<div class="info-window-content"><h3 class="info-window-title">Goa <small>Packages</small></h3><div><a href="#"><h4 class="package-title">Margao - <small>5 Days <span class="text-muted">(4 Nights)</span></small></h4></a><a href="#"><h4 class="package-title">Vasco Da Gama - <small>2 Days <span class="text-muted">(2 Nights)</span></small></h4></a><a href="#"><h4 class="package-title">Mapusa - <small>10 Days <span class="text-muted">(9 Nights)</span></small></h4></a><a href="#"><h4 class="package-title">Panjim - <small>5 Days <span class="text-muted">(4 Nights)</span></small></h4></a><p><a href="#" class="btn btn-default">View All Packages</a></p></div></div>',
		maxWidth: 275
	};
	infoWindow2 = new google.maps.InfoWindow(myInfoWindowOptions2);
	google.maps.event.addListener(marker2, 'click', function() {
		infoWindow2.open(map,marker2);
	});



//  ----------------- Marker 3

var marker3;
var myInfoWindowOptions3;
var infoWindow3;
var marker3Position = new google.maps.LatLng(-22.902034, -43.179753);

	marker3 = new google.maps.Marker({
		draggable: false,
		raiseOnDrag: false,
		icon: image,
		shadow: shadow,
		shape: shape,
		map: map,
		position: marker3Position
	});

	myInfoWindowOptions3 = {
		content: '<div class="info-window-content"><h3 class="info-window-title">Rio de Janerio <small>Packages</small></h3><div>    <a href="#"><h4 class="package-title">Pão de Açúcar - <small>5 Days <span class="text-muted">(4 Nights)</span></small></h4></a><a href="#"><h4 class="package-title">Marina da Glória - <small>2 Days <span class="text-muted">(2 Nights)</span></small></h4></a><a href="#"><h4 class="package-title">Copacabana - <small>10 Days <span class="text-muted">(9 Nights)</span></small></h4></a><a href="#"><h4 class="package-title">Parque Garota de Ipanema - <small>5 Days <span class="text-muted">(4 Nights)</span></small></h4></a><p><a href="#" class="btn btn-default">View All Packages</a></p></div></div>',
		maxWidth: 275
	};
	infoWindow3 = new google.maps.InfoWindow(myInfoWindowOptions3);
	google.maps.event.addListener(marker3, 'click', function() {
		infoWindow3.open(map,marker3);
	});


	// google.maps.event.addListener(marker, 'dragstart', function(){
	// 	infoWindow.close();
	// 	infoWindow2.close();
	// });

	// infoWindow.open(map,marker);
}


