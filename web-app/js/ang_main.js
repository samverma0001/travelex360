function book_hotel_control($scope) {
    $scope.TotalAdult = function () {
        return parseInt($scope.room1_adult) + parseInt($scope.room2_adult) + parseInt($scope.room3_adult) + parseInt($scope.room4_adult) + parseInt($scope.room5_adult);
    };
    $scope.TotalChild = function () {
        return parseInt($scope.room1_child) + parseInt($scope.room2_child) + parseInt($scope.room3_child) + parseInt($scope.room4_child) + parseInt($scope.room5_child);
    };

    $scope.TotalGuest = function () {
        return parseInt($scope.room1_child) + parseInt($scope.room2_child) + parseInt($scope.room3_child) + parseInt($scope.room4_child) + parseInt($scope.room5_child) + parseInt($scope.room1_adult) + parseInt($scope.room2_adult) + parseInt($scope.room3_adult) + parseInt($scope.room4_adult) + parseInt($scope.room5_adult);
    };

};
function book_flight_control($scope) {
        $scope.TotalFlightGuest = function () {
            return parseInt($scope.flight_adult) + parseInt($scope.flight_children) + parseInt($scope.flight_infant);
        };
};
function browse_activity_control($scope) {
		$scope.TotalActivityGuest = function () {
			return parseInt($scope.activity_adult) + parseInt($scope.activity_children);
		};
};