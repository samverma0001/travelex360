function showMoreInputField() {
    if ($("#numberOfRooms").val() == "1") {
        $(".peopleInRooms").hide();
        $("#peopleInOneRoom").show();
        $(".firstRoomChildrens").hide();
        $(".peopleInRoom").html("");
        $(".childInRoom").html("");
        for (var i = 1; i <= 3; i++) {
            $("#noOfAdult").append('<option value=' + i + '>' + i + '</option>');
        }
        for (var i = 0; i <= 2; i++) {
            $("#noOfChild").append('<option value=' + i + '>' + i + '</option>');
        }
    }
    else if ($("#numberOfRooms").val() == "2") {
        $(".peopleInRooms").hide();
        $("#peopleInOneRoom").show();
        $(".firstRoomChildrens").hide();
        $("#peopleInTwoRooms").show();
        $(".secondRoomChildrens").hide();
        $(".peopleInRoom").html("");
        $(".childInRoom").html("");
        for (var i = 1; i <= 3; i++) {
            $("#noOfAdult").append('<option value=' + i + '>' + i + '</option>');
            $(".peopleInTwoRooms").append('<option value=' + i + '>' + i + '</option>');
        }
        for (var i = 0; i <= 2; i++) {
            $("#noOfChild").append('<option value=' + i + '>' + i + '</option>');
            $(".childInTwoRooms").append('<option value=' + i + '>' + i + '</option>');
        }
    }
    else if ($("#numberOfRooms").val() == "3") {
        $(".peopleInRooms").hide();
        $("#peopleInOneRoom").show();
        $(".firstRoomChildrens").hide();
        $("#peopleInTwoRooms").show();
        $(".secondRoomChildrens").hide();
        $("#peopleInThreeRooms").show();
        $(".thirdRoomChildrens").hide();
        $(".peopleInRoom").html("");
        $(".childInRoom").html("");
        for (var i = 1; i <= 3; i++) {
            $("#noOfAdult").append('<option value=' + i + '>' + i + '</option>');
            $(".peopleInTwoRooms").append('<option value=' + i + '>' + i + '</option>');
            $(".peopleInThreeRooms").append('<option value=' + i + '>' + i + '</option>');
        }
        for (var i = 0; i <= 2; i++) {
            $("#noOfChild").append('<option value=' + i + '>' + i + '</option>');
            $(".childInTwoRooms").append('<option value=' + i + '>' + i + '</option>');
            $(".childInThreeRooms").append('<option value=' + i + '>' + i + '</option>');
        }
    }
    else if ($("#numberOfRooms").val() == "4") {
        $(".peopleInRooms").hide();
        $("#peopleInOneRoom").show();
        $(".firstRoomChildrens").hide();
        $("#peopleInTwoRooms").show();
        $(".secondRoomChildrens").hide();
        $("#peopleInThreeRooms").show();
        $(".thirdRoomChildrens").hide();
        $("#peopleInFourRooms").show();
        $(".fourthRoomChildrens").hide();
        $(".peopleInRoom").html("");
        $(".childInRoom").html("");
        for (var i = 1; i <= 3; i++) {
            $("#noOfAdult").append('<option value=' + i + '>' + i + '</option>');
            $(".peopleInTwoRooms").append('<option value=' + i + '>' + i + '</option>');
            $(".peopleInThreeRooms").append('<option value=' + i + '>' + i + '</option>');
            $(".peopleInFourRooms").append('<option value=' + i + '>' + i + '</option>');
        }
        for (var i = 0; i <= 2; i++) {
            $("#noOfChild").append('<option value=' + i + '>' + i + '</option>');
            $(".childInTwoRooms").append('<option value=' + i + '>' + i + '</option>');
            $(".childInThreeRooms").append('<option value=' + i + '>' + i + '</option>');
            $(".childInFourRooms").append('<option value=' + i + '>' + i + '</option>');
        }
    }
    else {
        $(".peopleInRooms").hide();
    }
    $("#totalRoom").html($("#numberOfRooms").val());
    var total = 0;
    var adultRoom1 = $("#noOfAdult").val();
    if (adultRoom1) {
        total = parseInt(total)+parseInt(adultRoom1)
    }
    var adultRoom2 = $("#peopleInRoomOne").val();
    if (adultRoom2) {
        total = parseInt(total)+parseInt(adultRoom2)
    }
    var adultRoom3 = $("#peopleInRoomThree").val();
    if (adultRoom3) {
        total = parseInt(total)+parseInt(adultRoom3)
    }
    var adultRoom4 = $("#peopleInRoomFour").val();
    if (adultRoom4) {
        total = parseInt(total)+parseInt(adultRoom4)
    }
    $("#totalAdults").html(total);
    var total1 = 0;
    var childRoom1 = $("#noOfChild").val();
    if (childRoom1) {
        total1 = parseInt(total1)+parseInt(childRoom1)
    }
    var childRoom2 = $("#childInRoomTwo").val();
    if (childRoom2) {
        total1 = parseInt(total1)+parseInt(childRoom2)
    }
    var childRoom3 = $("#childInRoomThree").val();
    if (childRoom3) {
        total1 = parseInt(total1)+parseInt(childRoom3)
    }
    var childRoom4 = $("#childInRoomFour").val();
    if (childRoom4) {
        total = parseInt(total1)+parseInt(childRoom4)
    }
    $("#totalChildren").html(total1);
}
function checkTotalAdultGuest() {
    var total = 0;
    var adultRoom1 = $("#noOfAdult").val();
    if (adultRoom1) {
        total = parseInt(total)+parseInt(adultRoom1)
    }
    var adultRoom2 = $("#peopleInRoomOne").val();
    if (adultRoom2) {
        total = parseInt(total)+parseInt(adultRoom2)
    }
    var adultRoom3 = $("#peopleInRoomThree").val();
    if (adultRoom3) {
        total = parseInt(total)+parseInt(adultRoom3)
    }
    var adultRoom4 = $("#peopleInRoomFour").val();
    if (adultRoom4) {
        total = parseInt(total)+parseInt(adultRoom4)
    }
    $("#totalAdults").html(total);
}

function checkTotalChildrenGuest() {
    var total = 0;
    var childRoom1 = $("#noOfChild").val();
    if (childRoom1) {
        total = parseInt(total)+parseInt(childRoom1)
    }
    var childRoom2 = $("#childInRoomTwo").val();
    if (childRoom2) {
        total = parseInt(total)+parseInt(childRoom2)
    }
    var childRoom3 = $("#childInRoomThree").val();
    if (childRoom3) {
        total = parseInt(total)+parseInt(childRoom3)
    }
    var childRoom4 = $("#childInRoomFour").val();
    if (childRoom4) {
        total = parseInt(total)+parseInt(childRoom4)
    }
    $("#totalChildren").html(total);
}

function checkNoOfAdultForThreeRooms() {
    var firstRoomOccupant = $("#peopleInRoomOneOfThree").val();
    var secondRoomOccupant = $("#peopleInRoomTwoOfThree").val();
    var thirdRoomOccupant = $("#peopleInRoomThreeOfThree").val();
    var dd_val = $("#noOfAdult").val();
    var totalOfAdult = parseInt(firstRoomOccupant) + parseInt(secondRoomOccupant) + parseInt(thirdRoomOccupant);
    if (totalOfAdult >= dd_val) {
        alert("No more number of people available");
        return;
    }
}
function checkNoOfAdultForFourRooms() {
    var firstRoomOccupant = $("#peopleInRoomOneOfFour").val();
    var secondRoomOccupant = $("#peopleInRoomTwoOfFour").val();
    var thirdRoomOccupant = $("#peopleInRoomThreeOfFour").val();
    var forthRoomOccupant = $("#peopleInRoomFourOfFour").val();
    var dd_val = $("#noOfAdult").val();
    var totalOfAdult = parseInt(firstRoomOccupant) + parseInt(secondRoomOccupant) + parseInt(thirdRoomOccupant) + parseInt(forthRoomOccupant);
    if (totalOfAdult >= dd_val) {
        alert("No more number of people available");
        return;
    }
}

function showChildrenAgeRoomOne() {
    var numberOfChild = $("#noOfChild").val();
    $(".ChildrensAgeRoomOne").show();
    if (numberOfChild == "1") {
        $(".firstRoomChildrens").hide();
        $(".firstChildInRoomOne").show();
        $(".childAgeInRoomOne").html("");
        for (var i = 1; i <= 9; i++) {
            $("#firstChildAgeRoomOne").append('<option value=' + i + '>' + i + '</option>');
        }
    }
    else if (numberOfChild == "2") {
        $(".firstRoomChildrens").hide();
        $(".firstChildInRoomOne").show();
        $(".secondChildInRoomOne").show();
        $(".childAgeInRoomOne").html("");

        for (var i = 1; i <= 9; i++) {
            $("#firstChildAgeRoomOne").append('<option value=' + i + '>' + i + '</option>');
            $("#secondChildAgeRoomOne").append('<option value=' + i + '>' + i + '</option>');
        }
    }
    else {
        $(".firstRoomChildrens").hide();
        $(".childAgeInRoomOne").html("");

    }
    $("#totalChildren").html();
}

function showChildrenAgeRoomTwo() {
    var numberOfChild = $("#childInRoomTwo").val();
    if (numberOfChild == "1") {
        $(".secondRoomChildrens").hide();
        $(".firstChildInRoomTwo").show();
        $(".childAgeInRoomTwo").html("");
        for (var i = 1; i <= 9; i++) {
            $("#firstChildAgeRoomTwo").append('<option value=' + i + '>' + i + '</option>');
        }
    }
    else if (numberOfChild == "2") {
        $(".secondRoomChildrens").hide();
        $(".firstChildInRoomTwo").show();
        $(".secondChildInRoomTwo").show();
        $(".childAgeInRoomTwo").html("");
        for (var i = 1; i <= 9; i++) {
            $("#firstChildAgeRoomTwo").append('<option value=' + i + '>' + i + '</option>');
            $("#secondChildAgeRoomTwo").append('<option value=' + i + '>' + i + '</option>');
        }
    }
    else {
        $(".secondRoomChildrens").hide();
        $(".childAgeInRoomTwo").html("");
    }
}

function showChildrenAgeRoomThree() {
    var numberOfChild = $("#childInRoomThree").val();
    if (numberOfChild == "1") {
        $(".thirdRoomChildrens").hide();
        $(".firstChildInRoomThree").show();
        $(".childAgeInRoomThree").html("");
        for (var i = 1; i <= 9; i++) {
            $("#firstChildAgeRoomThree").append('<option value=' + i + '>' + i + '</option>');

        }
    }
    else if (numberOfChild == "2") {
        $(".thirdRoomChildrens").hide();
        $(".firstChildInRoomThree").show();
        $(".secondChildInRoomThree").show();
        $(".childAgeInRoomThree").html("");
        for (var i = 1; i <= 9; i++) {
            $("#firstChildAgeRoomThree").append('<option value=' + i + '>' + i + '</option>');
            $("#secondChildAgeRoomThree").append('<option value=' + i + '>' + i + '</option>');
        }
    }
    else {
        $(".thirdRoomChildrens").hide();
        $(".childAgeInRoomThree").html("");
    }
}

function showChildrenAgeRoomFour() {
    var numberOfChild = $("#childInRoomFour").val();
    if (numberOfChild == "1") {
        $(".fourthRoomChildrens").hide();
        $(".firstChildInRoomFour").show();
        $(".childAgeInRoomFour").html("");
        for (var i = 1; i <= 9; i++) {
            $("#firstChildAgeRoomFour").append('<option value=' + i + '>' + i + '</option>');

        }
    }
    else if (numberOfChild == "2") {
        $(".fourthRoomChildrens").hide();
        $(".firstChildInRoomFour").show();
        $(".secondChildInRoomFour").show();
        $(".childAgeInRoomFour").html("");
        for (var i = 1; i <= 9; i++) {
            $("#firstChildAgeRoomFour").append('<option value=' + i + '>' + i + '</option>');
            $("#secondChildAgeRoomFour").append('<option value=' + i + '>' + i + '</option>');
        }
    }
    else {
        $(".fourthRoomChildrens").hide();
        $(".childAgeInRoomFour").html("");
    }
}