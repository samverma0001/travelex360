$(document).ready(function() {
	$('.input-group.date').datepicker({
    startDate: "today",
    autoclose: true,
    todayHighlight: true
  });


  $("#ex2").slider({});

  $('[data-toggle="tooltip"]').tooltip();

	// var doc_height = $('.page-wrapper').height();
	// console.log(doc_height);
  
  $("select").selectBoxIt();

  $('#select_room_count').change( function(){
  	var current_rooms = $('#select_room_count').val();
	  if (current_rooms == "1") {
	  	$('.room1').show();
	  	$('.room2').hide();
	  	$('.room3').hide();
	  	$('.room4').hide();
	  	$('.room5').hide();
	  }
	  else if (current_rooms == "2") {
	  	$('.room1').show();
	  	$('.room2').show();
	  	$('.room3').hide();
	  	$('.room4').hide();
	  	$('.room5').hide();
	  }
	  else if (current_rooms == "3") {
	  	$('.room1').show();
	  	$('.room2').show();
	  	$('.room3').show();
	  	$('.room4').hide();
	  	$('.room5').hide();
	  }
	  else if (current_rooms == "4") {
	  	$('.room1').show();
	  	$('.room2').show();
	  	$('.room3').show();
	  	$('.room4').show();
	  	$('.room5').hide();
	  }
	  else if (current_rooms == "5") {
	  	$('.room1').show();
	  	$('.room2').show();
	  	$('.room3').show();
	  	$('.room4').show();
	  	$('.room5').show();
	  }
  });
  
  $('.children_count').change(function(){
  	var current_childrens = $(this).val();
  	var	current_child1 = $(this).parents(".room-container").find('.child1');
  	var	current_child2 = $(this).parents(".room-container").find('.child2');
  	
  	// console.log(current_parent);

  	if (current_childrens == "0") {
  		current_child1.hide();
  		current_child2.hide();
	  }
  	if (current_childrens == "1") {
	  	current_child1.show();
  		current_child2.hide();
	  }
	  else if (current_childrens == "2") {
	  	current_child1.show();
  		current_child2.show();
	  }
  });


	$('.show-destination').click(function(){
		$('.destination-box').slideDown('1000');
	});
	$('.hide-destination').click(function(){
		$('.destination-box').slideUp('1000');
	});


	if ($('.flight-single').is(':checked')) {
		$('.return_box').hide();
		$('.multi-trip-container').hide();
	}
	else if ($('.flight-double').is(':checked')){
		$('.return_box').show();
		$('.multi-trip-container').hide();
	}
	else if ($('.flight-multi').is(':checked')){
		$('.single-trip-container').hide();
	}


	$('.flight-single').click(function(){
		$('.single-trip-container').show();
		$('.multi-trip-container').hide();
		$('.return_box').hide();

	});
	$('.flight-double').click(function(){
		$('.single-trip-container').show();
		$('.multi-trip-container').hide();
		$('.return_box').show();
	});
	$('.flight-multi').click(function(){
		$('.multi-trip-container').show();
		$('.single-trip-container').hide();

	});



	


		$('.registered-user').hide();
	$('.show_new_user').click(function(){
		$('.new-user').show();
		$('.registered-user').hide();
	});
	$('.show_registered_user').click(function(){
		$('.new-user').hide();
		$('.registered-user').show();
	});

	$('.room-select-dropdown-container').hide();
	$('.room-select-toggle').click(function(){
		$('.room-select-dropdown-container').slideToggle();
	});

	$('.room-options-toggle span:nth-child(2)').hide();
	$('.room-options-toggle').click(function(){
		$('.aditional-room-options').slideToggle();
		$('.room-options-toggle span').toggle();
	});

      $('.nearby-hotel-slider').owlCarousel({
        items : 6,
        itemsDesktop      : [1199,6],
        itemsDesktopSmall     : [979,5],
        itemsTablet       : [768,2],
        itemsMobile       : [479,1],
        slideSpeed : 1000,
        navigation: false,
        pagination:true,
        afterAction : syncPosition,
        responsiveRefreshRate : 200,
      });


      var flight_slide = $('.flight-listing-multi-slider');
      flight_slide.owlCarousel({
        items : 2,
        itemsDesktop      : [1199,2],
        itemsDesktopSmall     : [979,2],
        itemsTablet       : [768,2],
        itemsMobile       : [479,2],
        slideSpeed : 1000,
        navigation: false,
        rewindNav : false,
        pagination:false,
        mouseDrag: false,
        afterAction : syncPosition,
        responsiveRefreshRate : 200,
        afterAction: function(){
          if ( this.itemsAmount > this.visibleItems.length ) {
            $('.flight-content-nav.next').show();
            $('.flight-content-nav.prev').show();

            $('.flight-content-nav.next').removeClass('disabled');
            $('.flight-content-nav.prev').removeClass('disabled');
            if ( this.currentItem == 0 ) {
              $('.flight-content-nav.prev').addClass('disabled');
            }
            if ( this.currentItem == this.maximumItem ) {
              $('.flight-content-nav.next').addClass('disabled');
            }

          } else {
            $('.flight-content-nav.next').hide();
            $('.flight-content-nav.prev').hide();
          }
        }
      });
      $(".flight-content-nav.next").click(function(){
        flight_slide.trigger('owl.next');
      })
      $(".flight-content-nav.prev").click(function(){
        flight_slide.trigger('owl.prev');
      })

	 var sync1 = $("#slider-large");
      var sync2 = $("#slider-thumb");

      sync1.owlCarousel({
        singleItem : true,
        slideSpeed : 1000,
        navigation: false,
        pagination:false,
        afterAction : syncPosition,
        responsiveRefreshRate : 200,
      });


      sync2.owlCarousel({
        items : 5,
        itemsDesktop      : [1199,4],
        itemsDesktopSmall     : [979,4],
        itemsTablet       : [768,4],
        itemsMobile       : [479,3],
        pagination:false,
        responsiveRefreshRate : 100,
        afterInit : function(el){
          el.find(".owl-item").eq(0).addClass("synced");
        }
      });

      $(".slider-thumb-nav.thumb-next").click(function(){
				sync2.trigger('owl.next');
			})
			$(".slider-thumb-nav.thumb-prev").click(function(){
				sync2.trigger('owl.prev');
			})

      function syncPosition(el){
        var current = this.currentItem;
        $("#slider-thumb")
          .find(".owl-item")
          .removeClass("synced")
          .eq(current)
          .addClass("synced")
        if($("#slider-thumb").data("owlCarousel") !== undefined){
          center(current)
        }

      }

      $("#slider-thumb").on("click", ".owl-item", function(e){
        e.preventDefault();
        var number = $(this).data("owlItem");
        sync1.trigger("owl.goTo",number);
      });

      function center(number){
        var sync2visible = sync2.data("owlCarousel").owl.visibleItems;

        var num = number;
        var found = false;
        for(var i in sync2visible){
          if(num === sync2visible[i]){
            var found = true;
          }
        }

        if(found===false){
          if(num>sync2visible[sync2visible.length-1]){
            sync2.trigger("owl.goTo", num - sync2visible.length+2)
          }else{
            if(num - 1 === -1){
              num = 0;
            }
            sync2.trigger("owl.goTo", num);
          }
        } else if(num === sync2visible[sync2visible.length-1]){
          sync2.trigger("owl.goTo", sync2visible[1])
        } else if(num === sync2visible[0]){
          sync2.trigger("owl.goTo", num-1)
        }
      }

     $('.flight-list-tab-content').hide();
     $('.flight-list-tab-link a').click(function(){
     	var elem = $(this).parents('.flight-list-item');
     	elem.find('.flight-list-tab-content').slideDown();
     });
     $('.tab-content-close').click(function(){
     	var elem = $(this).parents('.flight-list-item');
     	elem.find('.flight-list-tab-content').slideUp();
     	elem.find('.flight-list-tab-link li').removeClass('active');
     });

     $(".countspin").TouchSpin({
     });

     $('.modify-search-toggle').click(function(){
      $('.modify-search-container').slideToggle();
     });

     $(".countspin-room").TouchSpin({
        min: 1,
        initval: '1',
        max: 5,
        buttondown_class: 'btn btn-default',
        buttonup_class: 'btn btn-default',
        step: 1
      });
     $(".countspin-flight-adult").TouchSpin({
        min: 1,
        initval: '1',
        max: 9,
        buttondown_class: 'btn btn-default',
        buttonup_class: 'btn btn-default',
        step: 1
      });
     $(".countspin-flight-children").TouchSpin({
        min: 0,
        initval: '1',
        max: 9,
        buttondown_class: 'btn btn-default',
        buttonup_class: 'btn btn-default',
        step: 1
      });
     $(".countspin-flight-infant").TouchSpin({
        min: 0,
        initval: '1',
        max: 3,
        buttondown_class: 'btn btn-default',
        buttonup_class: 'btn btn-default',
        step: 1
      });

      var countChecked = function() {
        var n = $( ".selectable-box:checked" );
        var m = $( ".selectable-box:not(:checked)" );
        n.parents(".listItem").addClass('selected');
        m.parents(".listItem").removeClass('selected');
      };
      countChecked();
      $( ".selectable-box" ).on( "click", countChecked );

      // $('.multicity-add-container .multicity-add').click(function(){
      //   var city_count = $('.multi-trip-container .multicity-leg').length;
      //   console.log(city_count);
      //   if (city_count == 2) {

      //   }
      // });




     // $('<div/>', {
     //     'class' : 'extraPerson', html: GetHtml()
     // }).appendTo('#container');
     // $('#addRow').click(function () {
     //       $('<div/>', {
     //           'class' : 'extraPerson', html: GetHtml()
     // }).hide().appendTo('#container').slideDown('slow');
         
     // });
     // function GetHtml()
     //  {
     //    var len = $('.extraPerson').length;
     //    var $html = $('.extraPersonTemplate').clone();
     //    $html.find('[name=firstname]')[0].name="firstname" + len;
     //    $html.find('[name=lastname]')[0].name="lastname" + len;
     //    $html.find('[name=gender]')[0].name="gender" + len;
     //    return $html.html();    
     //  }


 $('#addCity').click(function(){

    var city_count = $('.multi-trip-container .multicity-leg').length;
    // console.log(city_count);

    if (city_count < 5) {

      var $person = $('.multicity-leg').last();
      var person = $person.html();
      var index = person.indexOf('fromCity');
      var number = parseInt(person.substring(index+9,1));
      var next = number+1;
      person.replace('fromCity'+number, 'fromCity'+next);
      person.replace('toCity'+number, 'toCity'+next);
      person.replace('depature1'+number, 'depature1'+next);
      $person.after($('<div class="multicity-leg">'+person+'</div>'));            
      var $new = $('.multicity-leg').last();
      // $('.input-group.date').datepicker({
      //   startDate: "today",
      //   autoclose: true,
      //   todayHighlight: true
      // });

    }

 });

  $('#removeCity').click(function(){
    var city_count = $('.multi-trip-container .multicity-leg').length;
    if (city_count > 2) {
      var $person = $('.multicity-leg').last();
      var person = $person.html();
      var $new = $('.multicity-leg').last();
      $new.slideDown();
      $new.remove();
      $new.find('input, select').val('');
    }
  });


    $('#popup_traveller_info').modal({
      backdrop: 'static',
      show: false
    });
    $('.travel-pop:checked').removeAttr('checked');

    $(".travel-pop").change(function (e) {     
        if (this.checked) {               
            e.preventDefault();
            $('#popup_traveller_info').modal('show');
        } else {
            $('#popup_traveller_info').modal('hide');
        }
    });
    $('.cancel-checkbox').click(function(){
      $('.travel-pop:checked').removeAttr('checked');
    });

    $('.passowrd-box').hide();
    $('.toggle-passowrd-box').removeAttr('checked');
    $(".toggle-passowrd-box").change(function (e) {     
        if (this.checked) {               
            e.preventDefault();
            $('.passowrd-box').slideDown();
        } else {
            $('.passowrd-box').slideUp();
        }
    });


    $("#registerForm").validate({
      rules: {
        fullname: "required",
        mobile: "required",
        password: {
          required: true,
          minlength: 5
        },
        confirm_password: {
          required: true,
          minlength: 5,
          equalTo: "#password"
        },
        email: {
          required: true,
          email: true
        },
        topic: {
          required: "#newsletter:checked",
          minlength: 2
        },
        agree: "required"
      },
      messages: {
        firstname: "Please enter your firstname",
        lastname: "Please enter your lastname",
        username: {
          required: "Please enter a username",
          minlength: "Your username must consist of at least 2 characters"
        },
        password: {
          required: "Please provide a password",
          minlength: "Your password must be at least 5 characters long"
        },
        confirm_password: {
          required: "Please provide a password",
          minlength: "Your password must be at least 5 characters long",
          equalTo: "Please enter the same password as above"
        },
        email: "Please enter a valid email address",
        agree: "Please accept our policy"
      }
    });

    $("#bookHotel").validate({
      ignore:'',
      rules: {
        bh_goingto: "required",
        bh_checkin: "required",
        bh_checkout: "required",
        bh_adult1: {
                selectcheck: true
            }
      },
      messages: {
        bh_goingto: "Please enter where you want to go",
        bh_checkin: "Date Required",
        bh_checkout: "Date Required",
        bh_adult1: "Select Adults"
      },
      highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'error',
      errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
          error.insertAfter(element.parent());
        } else {
          error.insertAfter(element);
        }
      }
    });


    $("#bookFlight").validate({
      rules: {
        bf_leavingfrom: "required",
        bf_goingto: "required",
        bf_checkin: "required",
        bf_checkout: "required",
        bf_adult1: {
          selectcheck: true
        }
      },
      messages: {
        bf_leavingfrom: "Data Required",
        bf_goingto: "Data Required",
        bf_checkin: "Date Required",
        bf_checkout: "Date Required",
        bf_adult1: "Select Adults"
      },
      highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'error',
      errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
          error.insertAfter(element.parent());
        } else {
          error.insertAfter(element);
        }
      }
    });

    $("#planTravel").validate({
      rules: {
        pt_goingto: "required",
        pt_when: "required"
      },
      messages: {
        pt_goingto: "Enter where you want to go",
        pt_when: "Select When"
      },
      highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'error',
      errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
          error.insertAfter(element.parent());
        } else {
          error.insertAfter(element);
        }
      }
    });


    $("#planActivity").validate({
      ignore:'',
      rules: {
        ap_location: "required",
        ap_adults: {
          selectcheck: true
        }
      },
      messages: {
        ap_location: "Select Location",
        ap_adults: "Select Trevellers"
      },
      highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'error',
      errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
          error.insertAfter(element.parent());
        } else {
          error.insertAfter(element);
        }
      }
    });


    jQuery.validator.addMethod('selectcheck', function (value) {
        return (value != '0');
    }, "year required");
      

    var availableTags = [
    "Agartala",
    "Agra",
    "Agumbe",
    "Ahmedabad",
    "Aizawl",
    "Ajmer",
    "Alappuzha Beach",
    "Allahabad",
    "Alleppey",
    "Almora",
    "Amarnath",
    "Amritsar",
    "Anantagir",
    "Andaman and Nicobar Islands",
    "Araku valley",
    "Aurangabad",
    "Ayodhya",
    "Jaipur",
    "Jaisalmer",
    "Jabalpur",
    "Jalandhar",
    "Jamshedpur",
    "Jammu",
    "Jodhpur",
    "Sanchi ",
    "Saputara",
    "Sariska Wildlife Sanctuary ",
    "Shillong",
    "Shimla ",
    "Sohna Hills",
    "Sunderbans",
    "Surat",
    "Srinagar ",
    "Badrinath ",
    "Bandhavgarh National Park",
    "Bangalore ",
    "Baroda",
    "Bastar",
    "Bhagalpur",
    "Bhilai",
    "Bhimtal",
    "Bhopal",
    "Bhubaneswar",
    "Bhuj",
    "Bidar",
    "Bilaspur",
    "Bodh Gaya",
    "Kanchipuram",
    "Kanha National Park",
    "Kanpur",
    "Kanyakumari",
    "Kargil",
    "Karwar",
    "Kausani",
    "Kedarnath",
    "Keoladeoghana National Park",
    "Khajuraho",
    "Kochi",
    "Kodaikanal",
    "Kolkata",
    "Kollam",
    "Konark",
    "Kotagiri",
    "Kottakkal and Ayurveda",
    "Kovalam",
    "Kovalam and Ayurveda",
    "Kudremukh",
    "Kullu",
    "Kumarakom",
    "Kumarakom and Ayurveda",
    "Kumarakom Bird Sanctuary",
    "Kumaon",
    "Kurukshetra",
    "Thanjavur",
    "Tezpur",
    "Thrissur",
    "Tirunelveli",
    "Trichy",
    "Tirupati",
    "Thiruvananthapuram",
    "Calicut",
    "Chandigarh",
    "Chail",
    "Chamba",
    "Chennai",
    "Chennai Beaches",
    "Cherai",
    "Cherrapunji",
    "Chopta",
    "Chidambaram",
    "Chikhaldara Hills",
    "Coimbatore",
    "Coonoor",
    "Coorg",
    "Corbett National Park",
    "Cotigao Wild Life Sanctuary",
    "Cuttack",
    "Lakshadweep ",
    "Lucknow",
    "Udaipur",
    "Ujjain",
    "Dadra and Nagar Haveli",
    "Daman and Diu",
    "Dalhousie",
    "Darjeeling",
    "Dehradun ",
    "Delhi",
    "Devikulam",
    "Dharamashala",
    "Dhanaulti",
    "Dindigul",
    "Dudhwa National Park",
    "Dwaraka",
    "Madurai",
    "Mahabalipuram",
    "Malpe Beach",
    "Manas National Park",
    "Mangalore",
    "Mumbai",
    "Margoa",
    "Maravanthe Beach",
    "Mount Abu",
    "Munnar",
    "Mussoorie",
    "Mysore",
    "Vaishali",
    "Valley of Flowers",
    "Varanasi",
    "Varkala and Ayurveda",
    "Vijayawada",
    "Vishakhapatnam",
    "Vrindhavan",
    "Faridabad",
    "Nahsik",
    "Nalanda",
    "NandiHills",
    "Nanda Devi National Park",
    "Netravali Wild Life Sanctuary ",
    "Warangal",
    "Wayanad",
    "Wayanad Wildlife Sanctuary",
    "Gandhinagar",
    "Gangotri",
    "Gangtok",
    "Gir Wildlife Sanctuary",
    "Great Himalayan National Park",
    "Goa",
    "Gwalior",
    "Gulmarg",
    "Gurgaon",
    "Guruvayoor",
    "Guwahati",
    "Ooty",
    "Orchha",
    "Hampi",
    "Haridwar",
    "Hogenakkal",
    "Horsley-Hills",
    "Hyderabad",
    "Pahalgam",
    "Palakkad",
    "Patnitop",
    "Patna",
    "Panchgani",
    "Pattadakkal",
    "Periyar Wildlife Sanctuary",
    "Pithoragarh",
    "Pondicherry",
    "Pune",
    "Puri",
    "Pushkar",
    "Yercaud",
    "Imphal",
    "Itangar",
    "Indore",
    "Idukki",
    "Raipur",
    "Rajgir",
    "Rajaji National Park",
    "Ranchi",
    "Ranganthittu Bird Sanctuary",
    "Ranthambore",
    "Ranikhet",
    "Rameshwaram",
    "Rishikesh",
    "Rourkela",
    "Zanskar"
    ];
    $( ".autocomplete" ).autocomplete({
      source: availableTags
    });


    $('.sub-menu-container').hide();

    $('.category-accordian-container > li > .category-accordian-toggle').click(function(){
      $(this).toggleClass("opened");
      $(this).next('.sub-menu-container').slideToggle();
    });
    $('.sub-menu-container > li > .category-accordian-toggle').click(function(){
      $(this).toggleClass("opened");
      $(this).next().slideToggle();
    });


    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      var that=$($(e.target).attr('href')).find('.map');
      if(!that.find('iframe').length){ 
        that.append($('<iframe/>',{src:that.data('map')})
                      .css({height:'400px',width:'100%',border:'none'}));
      }
    }).first().trigger('shown.bs.tab');



// var Event = function(text, desc) {
//     this.text = text;
//     this.desc = desc;
// };
// var events = {};
// events[new Date("05/5/2015")] = new Event("195. Day of the Year", "best day in the year");
// events[new Date("05/9/2015")] = new Event("Payday", "should pay");
// events[new Date("05/26/2015")] = new Event("Awesome Day", "this day is awesome");

// $(".date_picker").datepicker({
//     beforeShowDay: function(date) {
//         var event = events[date];
//         if (event) {
//             return [true, 'event', event.text];
//         }
//         else {
//             return [true, '', ''];
//         }
//     },
//     numberOfMonths: 2,
//     nextText: '<i class="glyphicon glyphicon-chevron-right"></i>',
//     prevText: '<i class="glyphicon glyphicon-chevron-left"></i>',
//     onSelect: function(dateText) {
//         var event = events[new Date(dateText)];
//         if (event) {
//            // alert(event.text + "\n" + event.desc);
//         }
//     }
// });


var Event = function(text) {
    this.text = text;
};
var events = {};
events[new Date("05/5/2015")] = new Event("195. Day of the Year");
events[new Date("05/9/2015")] = new Event("Payday");
events[new Date("05/26/2015")] = new Event("Awesome Day");


function autoheight() {
    var $document = $(document);
    var windowwidth = $document.width();
    
    if (windowwidth > 600) {

      $(".date_picker").datepicker({
          beforeShowDay: function(date) {
              var event = events[date];
              if (event) {
                  return [true, 'event', event.text];
              }
              else {
                  return [true, '', ''];
              }
              $('.event').tooltip();
            $( ".ui-widget-content" ).append( document.createTextNode( "Hello" ) );
          },
          numberOfMonths: 2,
          nextText: 'Next',
          prevText: 'Prev'
      });
      
    }
    else {
      $(".date_picker").datepicker({
          beforeShowDay: function(date) {
              var event = events[date];
              if (event) {
                  return [true, 'event', event.text];
              }
              else {
                  return [true, '', ''];
              }
              $('.event').tooltip();
            $( ".ui-widget-content" ).append( document.createTextNode( "Hello" ) );
          },
          numberOfMonths: 1,
          nextText: '<i class="glyphicon glyphicon-chevron-right"></i>',
          prevText: '<i class="glyphicon glyphicon-chevron-left"></i>'
      });
    }
  }
  $(window).resize(autoheight);
  autoheight();




});


